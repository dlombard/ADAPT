#!/usr/bin/python
# -*- coding: utf-8 -*-

###########################################
# IMPORT LIBRARY
###########################################

import numpy as np
from numpy import linalg as LA
#import matplotlib.pyplot as plt

'''
Reads all the matrices and constants of the problem.
The matrices are read from .txt files which were created by another code: ErgoSCF
'''


#Function to read the different matrices and constants of the problem
def readmat(path) :
	K = np.zeros((10000,10000))

	fichier = open(path, "r")
	q = 0; 
	p = 0;
	for ligne in fichier:
		# Extraction des données de la ligne séparées par un espace
		donnees = ligne.rstrip('\n\r').split(" ")
		i = int(donnees[0])
		j = int(donnees[1])
		r = float(donnees[2])
		K[i,j] = r; 
		q = max(q,i)
		p = max(p,j)
	fichier.close()
	Ktrue = K[0:q,0:p]

	return Ktrue
