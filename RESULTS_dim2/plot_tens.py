#!/usr/bin/python
# -*- coding: utf-8 -*-

###########################################
# IMPORT LIBRARY
###########################################

import numpy as np
from numpy import linalg as LA
import matplotlib.pyplot as plt
from read2 import *
import random

'''
Reads all the matrices and constants of the problem.
The matrices are read from .txt files which were created by another code: ErgoSCF
'''

numvar = 2; 

dimvar = np.zeros(2); 
dimvar[0] = 1024; 
dimvar[1] = 1024; 

M = np.zeros((dimvar[0], dimvar[1])); 

T = readmat("/home/ehrlache/ADAPT/RESULTS/ResCoulomb_6_dim2_N1024_valuetens.txt");

print(np.amax(T))

#plt.imshow(range(0,dimvar[0]), range(0,dimvar[1]), M)
plt.figure()
plt.matshow(T)
plt.savefig('/home/ehrlache/ADAPT/RESULTS/Gibbs.png')
plt.show()

