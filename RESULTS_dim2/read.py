#!/usr/bin/python
# -*- coding: utf-8 -*-

###########################################
# IMPORT LIBRARY
###########################################

import numpy as np
from numpy import linalg as LA
#import matplotlib.pyplot as plt

'''
Reads all the matrices and constants of the problem.
The matrices are read from .txt files which were created by another code: ErgoSCF
'''


#Function to read the different matrices and constants of the problem
def readfunc(path) :
	K = np.zeros((100000,5))

	fichier = open(path, "r")
	q = 0; 
	for ligne in fichier:
		# Extraction des données de la ligne séparées par un espace
		donnees = ligne.rstrip('\n\r').split(" ")
		for i in range(0,5):
			K[q,i] = int(donnees[i]); 
		q = q+1;
	fichier.close()
	Ktrue = K[0:q,0:5]

	return Ktrue


#Function to read the different matrices and constants of the problem
def readranks(path) :
	K = np.zeros((100000,3))

	fichier = open(path, "r")
	q = 0; 
	for ligne in fichier:
		# Extraction des données de la ligne séparées par un espace
		donnees = ligne.rstrip('\n\r').split(" ")
		for i in range(0,3):
			K[q,i] = int(donnees[i]); 
		q = q+1;
	fichier.close()
	Ktrue = K[0:q,0:3]

	return Ktrue

#Function to read the different matrices and constants of the problem
def readerrors(path) :
	K = np.zeros((100000,3))

	fichier = open(path, "r")
	q = 0; 
	for ligne in fichier:
		# Extraction des données de la ligne séparées par un espace
		donnees = ligne.rstrip('\n\r').split(" ")
		for i in range(0,2):
			K[q,i] = float(donnees[i]); 
		q = q+1;
	fichier.close()
	Ktrue = K[0:q,0:2]

	return Ktrue
