#!/usr/bin/python
# -*- coding: utf-8 -*-

###########################################
# IMPORT LIBRARY
###########################################
##
import numpy as np
from numpy import linalg as LA
import matplotlib.pyplot as plt
from read import *
import random

'''
Reads all the matrices and constants of the problem.
The matrices are read from .txt files which were created by another code: ErgoSCF
'''

numvar = 2; 

dimvar = np.zeros(2); 
dimvar[0] = 2048; 
dimvar[1] = 2048; 

M = np.zeros((dimvar[0], dimvar[1])); 

#nodeleaf = readfunc("/home/ehrlache/ADAPT/RESULTS_dim2/ResCoulomb_err5_7_dim2_N2048_leafs.txt");
nodeleaf = readfunc("/home/ehrlache/ADAPT/RESULTS_dim2/ResGibbs_7_err5_dim2_N2048_leafs.txt");

print(nodeleaf.shape)

for i in range(0, nodeleaf.shape[0]):
	dim1 = int(nodeleaf[i,2]+1 -nodeleaf[i,1])
	dim2 = int(nodeleaf[i,4]+1 -nodeleaf[i,3])
	N = np.ones((dim1,dim2))
	M[int(nodeleaf[i,1]):int((nodeleaf[i,2]+1)),int(nodeleaf[i,3]):int((nodeleaf[i,4]+1))] = random.randint(0, nodeleaf.shape[0])*N


#plt.imshow(range(0,dimvar[0]), range(0,dimvar[1]), M)
plt.figure()
plt.matshow(M)
#plt.savefig('/home/ehrlache/ADAPT/RESULTS_dim2/ResCoulomb_7_dim2_N2048_leafs.png')
plt.savefig('/home/ehrlache/ADAPT/RESULTS_dim2/ResGibbs_7_dim2_N2048_leafs.png')
plt.show()

