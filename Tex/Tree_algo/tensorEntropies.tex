\documentclass[a4paper,12pt]{article}
\usepackage{epsfig}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bbold}
\usepackage{algorithm2e}
\usepackage{color}

\linespread{1.0}
\setlength{\topmargin}{0cm}
\setlength{\oddsidemargin}{0cm}
\setlength{\evensidemargin}{0cm}
\setlength{\textwidth}{16.5cm}

%\thispagestyle{empty}
\title{Tensor Entropies}
\author{Discussion between Virginie Maria and Damiano}
\date{}
\newcommand{\epsBall}[2]{\mathcal{B}(#1 #2)}
\newcommand{\epsBtxt}[1]{$\varepsilon$--ball#1}
\newcommand{\Ker}{\mathrm{ker}}

\newtheorem{mylemma}{}
\newtheorem*{lemNoNum}{}
\newtheorem{myprop}{}
\newtheorem{mytheo}{}
\newtheorem{mydef}{}
\newtheorem{myex}{}


\begin{document}
\maketitle

\section{Introduction}
This work deals with the possibility of discovering, given a tensor $F$ to be compressed, a suitable tensor structure and the approximation. 

\section{Notation.}
Let $d\in\mathbb{N}^*$ denote the tensor order. Let $1\leq i\leq d$ and $\Omega_i$ be an open bounded set and $\mathcal{V}^{(i)}\subseteq L^2(\Omega_i)$ be a Hilbert space. Let $u^{(i)}\in\mathcal{V}^{(i)}$ denote an element of such a space. Remark that it holds: $L^2(\Omega_1\times \ldots \times \Omega_d)=L^2(\Omega_1)\otimes\ldots\otimes L^2(\Omega_d)$. A pure tensor product is a real valued measurable function defined as an element $u\in \mathcal{V}^{(1)}\otimes\ldots\otimes \mathcal{V}^{(d)}$:
\begin{equation}
u : \left\{ \begin{array}{ll}
                                                \Omega_1 \times \ldots \times \Omega_d \ \rightarrow \ \mathbb{R} \\
                                                 (x_1,\ldots,x_d) \ \mapsto u^{(1)}(x1)u^{(2)}(x_2)\ldots u^{(d)}(x_d)\\
                                                 \end{array}\right.
\end{equation}

A tensor $T\in \mathcal{V}^{(1)}\otimes\ldots\otimes \mathcal{V}^{(d)}$ is, in general, a multilinear application defined as:
\begin{equation}
T : \left\{ \begin{array}{ll}
                                                \Omega_1 \times \ldots \times \Omega_d \ \rightarrow \ \mathbb{R} \\
                                                 (x_1,\ldots,x_d) \ \mapsto T(x_1,x_2,\ldots, x_d)\\
                                                 \end{array}\right.
\end{equation}
A density argument in $L^2$ (that is ultimately at the basis of the identification $L^2(\Omega_1\times \ldots \times \Omega_d)=L^2(\Omega_1)\otimes\ldots\otimes L^2(\Omega_d)$ ) makes it possible to state that a tensor $T$ can be seen as a sum of product of fibers in different directions. In order to talk about storage, we need to introduce a discretisation. Let $\mathcal{N}_i\in\mathbb{N}^*$ , $1\leq i \leq d$ be the number of degrees of freedom needed to store an element $u^{(i)}$. 


\section{Motivation.}
In this document we investigate the possibility of defining some entropies related to the linear combinations of pure tensor products. 
The main idea we would like to study is related to the following fact: discovering a tree or a network structure could be related to the process of factorising sums, as illustrated in the following examples.

\subsubsection{Example 1: a small CP.}
Let $T$ be:
\begin{equation}
T = a^{(1)}_1\otimes b^{(2)}_1 \otimes c^{(3)}_1 + a^{(1)}_1\otimes b^{(2)}_2 \otimes c^{(3)}_1 + a^{(1)}_1\otimes b^{(2)}_3 \otimes c^{(3)}_2
\end{equation}
Storing $T$ in this form would mean using a Canonical Polyadic (CP) format, and would have a cost of $M=3(\mathcal{N}_1 + \mathcal{N}_2 + \mathcal{N}_3 )$.

We observe that the term $a^{(1)}_1$ as well as $c^{(3)}_1$  can be factorised, leading to:
\begin{equation}
T = a^{(1)}_1 \otimes \left( c^{(3)}_1 \otimes ( b^{(2)}_1 + b^{(2)}_2 ) +   b^{(2)}_3 \otimes c^{(3)}_2 \right),
\end{equation}
which is absolutely equivalent to the original expression. However, the storage needed in this form is significantly lower: $M=\mathcal{N}_1 + 2 (\mathcal{N}_2 + \mathcal{N}_3)$. A possible interpretation is related to the frequency of appearence of the different fibres within the full CP expression.  If some fibres appear more than once, this means that the sum can be factorised in some way, hence reducing the storage. This is not different, in the spirit, to entropy encoding. 

\subsubsection{Example 2: retrieving a TT.}
Let us consider a third order tensor, whose fibres frequency, when unfolded on the direction $3$ reads:
\begin{equation}
\begin{bmatrix} 
0 & 1 & 0 \\
1 & 1 & 1 \\
0 & 0 & 1 
\end{bmatrix},
\begin{bmatrix} 
1 & 1 & 0 \\
1 & 1 & 0 \\
0 & 0 & 1 
\end{bmatrix},
\begin{bmatrix} 
0 & 0 & 1 \\
1 & 0 & 0 \\
1 & 0 & 0 
\end{bmatrix}
\end{equation}
To read this: the first table represents the terms multiplied by $a^{(3)}_1$, the second by $a^{(3)}_2$ and the third one by $a^{(3)}_3$. So, the first row of the first slice of frequencies contains a $1$ in the second columns, corresponding to a pure tensor term of the form $a^{(1)}_1 a^{(2)}_2 a^{(3)}_1$. If we were about to store this as a CP, we would need a storage of $13(\mathcal{N}_1+\mathcal{N}_2 +\mathcal{N}_3)$. Let us present a naive algorithm to improve the storage. 

Let us consider the marginal frequncies:
\begin{eqnarray}
p^{(1)} = \frac{1}{13}[5,4,4], \\
p^{(2)} = \frac{1}{13}[4, 6, 3], \\
p^{(3)} = \frac{1}{13}[5, 5, 3].
\end{eqnarray}
The term which is appearing the most is the term $a^{(2)}_2$, so we factorise it:
\begin{equation}
a_2^{(2)}\left[ a_1^{(1)} \left( a_1^{(3)}+a_2^{(3)} + a_3^{(3)} \right) + a_2^{(1)}\left( a_1^{(3)} + a_2^{(3)} \right) + a_3^{(1)}a_1^{(3)} \right].
\end{equation}
After we have eliminated this term from the original tensor, what is left to be compressed reads:
\begin{equation}
\begin{bmatrix} 
0 & 1 & 0 \\
0 & 0 & 0 \\
0 & 0 & 1 
\end{bmatrix},
\begin{bmatrix} 
1 & 1 & 0 \\
0 & 0 & 0 \\
0 & 0 & 1 
\end{bmatrix},
\begin{bmatrix} 
0 & 0 & 1 \\
0 & 0 & 0 \\
1 & 0 & 0 
\end{bmatrix},
\end{equation}
which provides relative marginal frequencies of the form:
\begin{eqnarray}
p^{(1)} = \frac{1}{13}[2,2,3], \\
p^{(2)} = \frac{1}{13}[4, 0, 3], \\
p^{(3)} = \frac{1}{13}[2, 3, 2].
\end{eqnarray}
Hence we see that the term which is appearing the most frequently is $a_1^{(2)}$, which provides:
\begin{equation}
a_1^{(2)}\left[ a_2^{(1)} \left( a_1^{(3)}+a_2^{(3)} \right) + a_1^{(1)}a_1^{(3)}  + a_3^{(1)}a_3^{(3)}\right].
\end{equation}
After this second factorisation, the relative frequency unfoldings read:
\begin{equation}
\begin{bmatrix} 
0 & 0 & 0 \\
0 & 0 & 0 \\
0 & 0 & 1 
\end{bmatrix},
\begin{bmatrix} 
0 & 0 & 0 \\
0 & 0 & 0 \\
0 & 0 & 1 
\end{bmatrix},
\begin{bmatrix} 
0 & 0 & 0 \\
0 & 0 & 0 \\
1 & 0 & 0 
\end{bmatrix},
\end{equation}
corresponding to marginal frequencies:
\begin{eqnarray}
p^{(1)} = \frac{1}{13}[1,0,2], \\
p^{(2)} = \frac{1}{13}[0, 0, 3], \\
p^{(3)} = \frac{1}{13}[1, 1, 1].
\end{eqnarray}
The last term reads:
\begin{equation}
a_3^{(2)}\left[ a_3^{(1)}\left( a_1^{(3)}+a_2^{(3)}\right) + a_1^{(1)}a_3^{(1)} \right].
\end{equation}
Let us remark that the factorisation produced corresponds to the following TT:
\begin{equation}
\begin{bmatrix}
a_2^{(2)} & a_1^{(2)} & a_3^{(2)}
\end{bmatrix}
\begin{bmatrix}
a_3^{(1)} & a_1^{(1)} & a_2^{(1)} & 0\\
a_1^{(1)} & 0 & a_2^{(1)} & a_3^{(1)} \\
a_1^{(1)} & 0 & a_3^{(1)} & 0
\end{bmatrix}
\begin{bmatrix}
a_1^{(3)} \\
a_1^{(3)} + a_2^{(3)} + a_3^{(3)} \\
a_1^{(3)} + a_2^{(3)} \\
a_3^{(3)}
\end{bmatrix}
\end{equation}
The storage, even if we duplicate the memory for the terms which are repeated in the core and even if in the last term we introduce two sums, amounts to: $3\mathcal{N}_2 + 8 \mathcal{N}_1 + 4\mathcal{N}_1$, which is largely reduced. 

Let us remove the redundancies in the storage of the cores. The core in the variable $1$ can be rewritten as follows:
\begin{equation}
\begin{bmatrix}
a_3^{(1)} & a_1^{(1)} & a_2^{(1)} & 0\\
a_1^{(1)} & 0 & a_2^{(1)} & a_3^{(1)} \\
a_1^{(1)} & 0 & a_3^{(1)} & 0
\end{bmatrix} = \begin{bmatrix}
[0, 0, 1] & [1, 0, 0] & [0, 1, 0] & 0 \\
[1,0,0] & 0 & [0,1,0] & [0,0,1] \\
[1,0,0] & 0 & [0,0,1] & 0
\end{bmatrix} 
\begin{bmatrix}
a_1^{(1)} \\
a_2^{(1)} \\
a_3^{(1)}
\end{bmatrix}.
\end{equation}
Similarly, the last core takes the following form:
\begin{equation}
\begin{bmatrix}
a_1^{(3)} \\
a_1^{(3)} + a_2^{(3)} + a_3^{(3)} \\
a_1^{(3)} + a_2^{(3)} \\
a_3^{(3)}
\end{bmatrix} = \begin{bmatrix}
1 & 0 & 0 \\
1 & 1 & 1 \\
1 & 1 & 0 \\
0 & 0 & 1
\end{bmatrix} \begin{bmatrix}
a_1^{(3)} \\
a_2^{(3)} \\
a_3^{(3)}
\end{bmatrix}
\end{equation}
After these two last substitution, the storage reduces to: $3(\mathcal{N}_1 + \mathcal{N}_2 + \mathcal{N}_3) + \mathcal{O}(1)$, where the last term stand for the sparse storage of the cores $4\times 3\times 3$ and $4\times 3$ which we introduced.  The interpretation of this factorisation is also reasonable: the fibres repeating in the original CP tensor are just $3$ per direction, hence we need to store them with a cost  $3(\mathcal{N}_1 + \mathcal{N}_2 + \mathcal{N}_3)$; there is also another information that we need to store, and it is related to the tensor structure, to how they are combined together in the original CP expression, which leads to the $\mathcal{O}(1)$ term in the memory. When there are more than 3 fibres per direction and the tensor order increases, considering the sum of the fibres in the cores and the TT storage as a final stage could be advantageous with respect to the last stage proposed. 
Remark that up to this point there is no compression of the degrees of freedom. This method applies as it is if we have fibres in a direction and we apply the first step of the HOSVD. It corresponds to perform an entropy encoding of the core, the advantage being that we are not obliged to actually compute the core. We could derive an equivalent formulation in which we define entropies related to subspaces and we proceed analogously. The idea is then to derive a tensor network architecture as the one that optimise the factorisation (the one which results from an entropy encoding as we were considering the fibres as elements of an alphabet of symbols). 


\section{Entropy for product spaces, entropy for tensors.}
The notion of entropy has been introduced in the study of dynamical systems (Bowen topological entropy for product spaces) and in approximation theory. In this section, we try to introduce several notions of entropy that could be exploited in order to discover an efficient factorisation and compression. 

\subsection{Coverings.}
Let $\mathcal{B}(u,\mathcal{\varepsilon})$ denote a ball of center $u\in  \mathcal{V}^{(1)}\otimes\ldots\otimes \mathcal{V}^{(d)}$ and radius $\varepsilon$. Let us make the hypothesis that $u=u^{(1)}\otimes \ldots\otimes u^{(d)}$ is a pure tensor product. We define radiuses $\delta_1,\delta_2,\ldots, \delta_d >0$ such that: 
$$\mathcal{B}^{(1)}(u^{(1)},\delta_1) \otimes \ldots \otimes \mathcal{B}^{(d)}(u^{(d)},\delta_d) \subseteq  \mathcal{B}(u,\mathcal{\varepsilon}).$$
We try to construct balls in the separated spaces such that the product of the balls is contained in the ball of prescribed radius in the product space. The rationale behind this question is simple: given a pure tensor product, we would like to have a simple way to state if it is possible to approximate it, with an error less than a given $\varepsilon$ by another pure tensor product. Such a criterion would make it possible to work on each fibre separately. 

Let $e^{(i)}\in \mathcal{V}^{(i)}$ be such that: $\| e^{(i)} \|_{\mathcal{V}^{i}}\leq \delta_i$. Moreover, there exist $\alpha>0$ such that:
\begin{equation}
\frac{\delta_i}{\| u^{(i)} \|_{\mathcal{V}^{(i)}} } \leq \alpha, \ \ \forall i, \ \ 1\leq i\leq d.
\end{equation}

Let us consider the following:
\begin{equation}
\tilde{u} = (u^{(1)} + e^{(1)})\otimes \ldots \otimes (u^{(d)} + e^{(d)}),
\end{equation}
and let us try to evaluate:
\begin{equation}
\| \tilde{u} - u\|_{L^2(\Omega)} \leq \sum_{j=1}^d \delta_j \prod_{k\neq j}^d \| u^{(k)} + e^{(k)} \|_{\mathcal{V}^{(k)}}.
\end{equation}
By making use of the triangular inequality we have:
\begin{equation}
\sum_{j=1}^d \delta_j \prod_{k\neq j}^d \| u^{(k)} + e^{(k)} \|_{\mathcal{V}^{(k)}} \leq \sum_{j=1}^d \delta_j \prod_{k\neq j}^d (\| u^{(k)} \|_{\mathcal{V}^{(k)}} + \delta_k) \leq  \sum_{j=1}^d \delta_j \prod_{k\neq j}^d (1+\alpha) \| u^{(k)} \|_{\mathcal{V}^{(k)}} .
\end{equation}
It holds:
\begin{equation}
\prod_{k\neq j}^d (1+\alpha) \| u^{(k)} \|_{\mathcal{V}^{(k)}} = (1+\alpha)^{d-1} \frac{ \| u \|_{L^2(\Omega)} } {\| u^{(j)} \|_{L^2(\Omega_j)}}.
\end{equation}
Henceforth:
\begin{equation}
\| \tilde{u} - u\|_{L^2(\Omega)} \leq (1+\alpha)^{d-1} \| u \|_{L^2}\sum_{j=1}^d \frac{\delta_j}{\| u^{(j)} \|_{L^2(\Omega_j)}} \leq \alpha d (1+\alpha)^{d-1} \| u \|_{L^2(\Omega)}.
\end{equation}
The sufficient condition for the product of the balls to be contained in the $\varepsilon-$ball of the product space is represented by the condition:
\begin{equation}
 \alpha d (1+\alpha)^{d-1} \leq \frac{\varepsilon}{ \| u \|_{L^2(\Omega)} }.
\end{equation}


\paragraph*{Remark:} the auxiliary function $f(t) = (1+\alpha t)^d$ is such that:
\begin{equation}
\frac{\| \tilde{u} - u\|_{L^2(\Omega)}}{\| u \|_{L^2}} \leq \partial_t f |_{t=1}.
\end{equation}
It is saying that, roughly speaking, the relative error on pure tensor products scales like the surface of the product space (the exponent being $d-1$). 

\subsection{Alphabets of fibres subspaces.}
Aiming at introducing a notion of entropy pertinent to describe factorisations, we introduce a set of alphabets, $\mathcal{A}^{(i)}, \ i=1,\ldots,d$, one in each of the directions. The cardinality of the alphabet is $\#\mathcal{A}^{(i)} = n_i$. The choice of the alphabet elements follows a certain rationale. 
Aiming at retrieving a low rank (eventually local) approximation, given two fibres $f^{(i)}_j$ and $\lambda f^{(i)}_j, \lambda\in\mathbb{R}^*$, since they belong to the same subspace it would be reasonable to associate them with the same alphabet element. The subspaces of dimension $r^{i,k_i}$, denoted by $\mathcal{U}^{(i,k_i)}$ are naturally elements of the Grassman manifold, and the orthonormal frames that can be taken as bases for these are elements of the compact Stiefel manifold. 
The elements of the alphabets are henceforth orthonormal bases $U^{(i,k_i)} \in\mathbb{R}^{\mathcal{N}_i\times r^{i,k_i}}, \ 1\leq k_i \leq n_i$. When functions are at hand, these are sets of $r^{i,k_i}$ orthonormal functions of the variable $x_i$. The alphabets read:
\begin{equation}
\mathcal{A}^{(i)} = \left\lbrace U^{(i,k_i)}, 1\leq k_i\leq n_i \right\rbrace.
\end{equation}

A given fibre can henceforth be approximated by one of the alphabet elements. Let $a^{(i)}_j \in\mathbb{R}^{r^{i,k_i}}$, it holds:
\begin{equation}
\| f^{(i)}_j - U^{i,k_i}a^{(i)}_j \|_{L^2(\Omega_i)}\leq \delta_i.
\end{equation}

Let us consider a tensor given in a CP format. It can be approximated as:
\begin{eqnarray}
F = \sum_{i=1}^{R} f^{(1)}_i\otimes\ldots\otimes f^{(d)}_i, \\
\tilde{F} =  \sum_{i=1}^{R} U^{(1,k_1)} a^{(1)}_i \otimes\ldots \otimes  U^{(d,k_d)} a^{(d)}_i, \\
\| F - \tilde{F} \|_{L^2(\Omega)} \leq \varepsilon. 
\end{eqnarray}

Here, for sake of brevity we have written $U^{(1,k_1)}$ for the approximation of a generic element $f^{(1)}_i$. It is clear that, formally, we have to introduce an application associating the indices $i$ with the corresponding subspace $k_1$:
\begin{eqnarray}
k_1(i): [1,R]\subset\mathbb{N}^* \ \rightarrow \ [1,n_1]\subset\mathbb{N}^*, \\
k_1(i) = \arg \inf_{1\leq k\leq n_1} \arg \inf_{a\in\mathbb{R}^{r^{(1,k)}}} \| f_i^{(1)} - U^{(1,k)}a \|^2_{L^2(\Omega_1)}.
\end{eqnarray}
From a practical point of view, we will see that this application would be automatically computed at the moment in which we construct the alphabets. Here we wrote it for the fibres in the first direction. The same holds for the fibres in the other directions. 

\subsection{Definition of entropy and properties.}
Several notions of entropy can be introduced. The first one reads as follows: let the approximation of the tensor in CP format be $\tilde{F}$. Every term of the series is expressed via $d$ symbols $U^{(i,k_i)}$, one for each of the alphabets. It is possible henceforth to introduce the following notions.
Let $0\leq p^{(i)}_{k}\leq 1$ be the probability mass of the $k-$th term of the $i-$th alphabet, which is the frequency with which this term appears in $\tilde{F}$. It holds:
\begin{eqnarray}
\sum_{k=1}^{n_i} p^{(i)}_k = 1, \\
S^{(i)} = \sum_{k=1}^{n_1} p^{(i)}_k \log\left( \frac{1}{p_k^{(i)}} \right).
\end{eqnarray}
The term $S^{(i)}$ is the $i-$th marginal entropy. 
Let the product of symbols be defined as:
\begin{equation}
\bigotimes_{1,\leq i \leq d}U^{(i,k_i)} : \left\{ \begin{array}{ll}
                                                \mathcal{A}^{(1)} \times \ldots \times \mathcal{A}^{(d)} \ \rightarrow \ \mathcal{U}^{(1,k_1)}\otimes \ldots \mathcal{U}^{(d,k_d)} \\
                                                 (k_1,\ldots,k_d) \ \mapsto U^{(1,k_1)} \otimes \ldots \otimes U^{(1,k_1)}\\
                                                 \end{array}\right.
\end{equation}
Given a certain term in the CP expression $\tilde{F}$, there exist indices $k_1,\ldots,k_d$ such that this term belong to the subspace generated by $\bigotimes_{1,\leq i \leq d}U^{(i,k_i)} $. All the possible products of symbols are defined straightforwardly as elements of the cartesian products of the alphabets:
\begin{equation}
\mathcal{A} = \mathcal{A}^{(1)}\times \ldots\times \mathcal{A}^{(d)} = \left\lbrace \bigotimes_{1,\leq i \leq d}U^{(i,k_i)}, U^{(i,k_i)}\in\mathcal{A}^{(i)}   \right\rbrace.
\end{equation}
The cardinality of this set satisfies:
\begin{equation}
\#\mathcal{A} = \prod_{i=1}^d \mathcal{A}^{(i)}.
\end{equation}
Given the CP expression is therefore possible to introduce an entropy function related to the product space. Let $(p_k)_{1\leq k\leq \#\mathcal{A}}$ be the probability mass function of the joint alphabet. It holds:
\begin{equation}
S_{\tilde{F}} = \sum_{j=1}^{\#\mathcal{A}} p_k  \log\left( \frac{1}{p_k} \right).
\end{equation}

A set of natural basic properties are satisfied by the (Shannon) entropies introduced so far. 
The largest possible entropy corresponds to the case in which all the probability masses are equal, providing:
\begin{equation}
p_k = \frac{1}{\#\mathcal{A}} \ \Rightarrow \ S_* = \log(\#\mathcal{A}) = \sum_{i=1}^d \log(\#\mathcal{A}^{(i)}). 
\end{equation}
In the case in which the marginal probability masses are not equal to a constant, it follows that the entropy cannot be the maximal one. In that case, the upper limit is attained where the joint probability density is the product of the marginal densities:
\begin{equation}
S_{\otimes} = \sum_{i=1}^d S^{(i)}.
\end{equation}
It holds henceforth:
\begin{equation}
S_{\tilde{F}}\leq S_{\otimes} \leq S_*.
\end{equation}
This relationship simply gives an idea of the possible levels of compressibility of the tensor $\tilde{F}$. 
The mutual information between the joint and the marginal spaces is:
\begin{equation}
I(p; p^{(1)}, \ldots, p^{(d)}) = \sum_{i=1}^{d} S^{(i)} - S_{\tilde{F}} \geq 0. 
\end{equation}

\textbf{Remark:} it seems that Tucker-HOSVD is a maximal entropy format. Indeed, if all the product of bases are equally probable we will end up with a dense core and a block HOSVD compression. What are the formats we could obtain for $S_{\tilde{F}} = S_{\otimes}<S_*$? And in general? 


\subsection{Tensor entropy as a coarse grain approximation of the Kolmogorov entropy. }
Let us show that there is a link between the above introduced entropies and the entropy introduced by Kolmogorov on metric spaces.  

\section{The method. Version after the discussion of January, the 28th.}
In this section, a first version of the method is detailed. 

\subsection{Extraction of the minimal alphabets of subspaces.}
The first point to be considered is how to extract the alphabets consisting in the smallest possible number of elements such that all the fibres of the tensor can be approximated by one of the alphabets entries.
To this end, the first step consists in extracting, for all the directions, the tensor fibres: let $1\leq j \leq d$, $r_j\in\mathbb{N}^*$, $(f^{(j)}_i)_{1\leq i \leq r_j}$. This can be done by considering the tensor unfoldings or in a simple way for all the formats in which fibres are already separated (typically H-Tucker, TT, CP, sparse).

The first step consists in taking care of the normalisation of the fibres. Let $c_i = \| f_i^{(1)}\otimes\ldots\otimes f^{(d)}_i \|_{L^2(\Omega)}$:
\begin{equation}
f_i^{(1)}\otimes\ldots\otimes f^{(d)}_i  = \frac{c_i}{|c_i|} |c_i|^{1/d} f_i^{(1)} \otimes \ldots \otimes  |c_i|^{1/d} f_i^{(d)}.
\end{equation}
In the following, it is implicitly assumed that this step has been performed, for all the fibres. An adaptive clustering procedure is performed, in which the number of clusters, denoted by $n_{c}^{(j)}\in\mathbb{N^*}$ is not fixed a priori:
\begin{enumerate}
\item At the beginning, the number of clusters is set to $n_{c}=1$, the number of fibres is $R$, the basis is the one we would obtain if we were about to perform an HOSVD, the storage is $M^{(j)} = \mathcal{N}_j r^{(j)} + r^{(j)}R$ . 
\item At the $p-$th step, let the number of clusters be $n^{(j)}_{c}=p$. 
\item We have a set of $n_c^{(j)}$ local bases $U^{(j, k^{(j)})}$, $1\leq k^{(j)}\leq n_c^{(j)}$, consisting in a set of $(r^{(j)}_{k})_{1\leq k \leq n_c^{(j)}}$ modes, making it possible to approximate $R^{(j)}_{k^{(j)}}$ fibres of the cluster up to a prescribed tolerance.  The storage is $M^{(j)} = \sum_{k=1}^{n_c^{(j)}} r^{(j)}_{k} \mathcal{N}_j + R^{(j)}_{k^{(j)}} r^{(j)}_{k}$. 
\item The clustering structure chosen is the one corresponding to the minimal storage. 
\end{enumerate}

The choice of the local basis size can be performed in two ways: by uniformly subdividing the error among the clusters, or by using a parsimony principle. Aiming at finding the cheapest alphabet, we opt for the second strategy. We run over the clusters, see which is the one contributing the most to the global error, we add a term to the local basis of this cluster. We proceed in this way up to the point in which the error is smaller than a prescribed accuracy. 

\subsection{Entropy encoding and final factorisation.}
The Huffman tree in the joint space is introduced, that makes it possible to perform an entropy encoding of the bases. The result of the Huffman tree is a variable length binary code associated to the elements fo the alphabets products appearing in the CP expression. 



\end{document}