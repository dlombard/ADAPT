\documentclass[a4paper,11pt]{article}
\usepackage{epsfig}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bbold}
\usepackage{algorithm2e}
\usepackage{color}
\usepackage [utf8]{inputenc}

\linespread{1.0}
\setlength{\topmargin}{0cm}
\setlength{\oddsidemargin}{0cm}
\setlength{\evensidemargin}{0cm}
\setlength{\textwidth}{16.0cm}
%\thispagestyle{empty}
\title{Papers related with tree construction}
\date{}
\author{Virginie Ehrlacher, Maria Fuente Ruiz, Damiano Lombardi}

\newtheorem{mylemma}{}
\newtheorem*{lemNoNum}{}
\newtheorem{myprop}{}
\newtheorem{mytheo}{}
\newtheorem{mydef}{}
\newtheorem{myex}{}


\def\R{{\mathbb R}}
\def\N{{\mathbb N}}
\def\cI{{\mathcal I}}
\def\cA{{\mathcal A}}
\def\cB{{\mathcal B}}
\def\cF{{\mathcal F}}
\def\cP{{\mathcal P}}
\def\cL{{\mathcal L}}
\def\cV{{\mathcal V}}
\def\cE{{\mathcal E}}
\def\bigtimes{{\text{\Large{$\times$}}}}
\def\BState{\State\hskip-\ALG@thistlm}

\newcommand{\corr}[1]{\textcolor{blue}{#1}}


\begin{document}
\maketitle
\section{Hierarchical SVD of tensors (Lars Grasedyck)}
In this paper it is defined the hierarchical singular value decomposition (SVD) for tensors of order $d \geq 2$ and shows some properties (equal or not to the ones of the standard matrix SVD). In particular, one can find low rank (almost) best approximations in a hierarchical format (H-Tucker). This format is a specialization of the Tucker format and it contains as a special case all rank k tensors. Based on this new concept of a hierarchical SVD, they present algorithms for hierarchical tensor calculations, doing a certain truncation after an error analysis.

Let $f$ be a multivariate function. A way to approximate the multivariate function $f$ is to separate the variables, i.e. to seek for an approximation of the form:
$$f(x_1, \dots, x_d) \approx \tilde f (x_1, \dots, x_d) =\sum^{k}_{i=1}f_{i,1}(x_1)\otimes \ldots \otimes f_{i,d}(x_d)$$

where $k$ is the rank. A necessary basic operation is to truncate representations from larger to smaller rank:
For given $f\in V$ find $\tilde f \in V$ of rank $k$ that $ \| f-\tilde f \| \approx inf_{v\in V, rank(v)=k } \|f - v\|$.
 Where $V$ is the functional space.

The minimization problem could have some difficulties in practice. Thus, for some cases it is known how to construct a low separation rank approximation with high accuracy and stable representation but in order to use this low rank format as a basic format in numerical algorithms one needs a reliable truncation procedure that can be used universally without tuning parameters. In this article it is defined a hierarchical rank of a tensor by singular value decompositions (SVD). The hierarchical format is then characterized by nested subspaces that stem from the SVD. It is also presented a hierarchical SVD and its properties and the truncation procedure used.


The \textbf{Tucker Format} is introduced. 
Let $d \in \mathbb{N}$ and $n_1, \ldots , n_d \in \mathbb{N}$. We consider tensors as vectors over product index sets. For this purpose we introduce the d-fold product index set:
$$\mathcal{I} = \mathcal{I}_1 \times \ldots \times \mathcal{I}_d, \qquad \mathcal{I}_\mu = \{ 1, \ldots, n_\mu\}$$ with $\mu \in \{ 1, \ldots, d\}$.

Let $A \in \mathbb{R}^{\mathcal{I}}$ be the tensor. And the $\mu$-mode matricization (Unfolding): $A^{(\mu)}=\mathcal{M}_{\mu}, \quad \mathcal{M}_{\mu}:  \mathbb{R}^{\mathcal{I}} \rightarrow \mathbb{R}^{\mathcal{I}_{\mu} \times \mathcal{I}^{(\mu)}} $.

Let $U_\mu \in \mathbb{R}^{J_\mu \times \mathcal{I}_\mu}$ be the orthonormal matrices and $C \in \mathbb{R}^{k_1 \times \ldots \times k_d}$ the so called core tensor. The orthogonal Tucker format of a tensor would be:
$$A=(U_1, \ldots, U_d )\times C$$.

If the $U_\mu$ are arbitrary the previous expression is just the Tucker format.
Then the truncation of $A$ to Tucker rank $(k_1, \ldots , k_d)$ is defined by
$$\mathcal{T}_{(k_1, \ldots, k_d)}(A) = (\tilde U_1 \tilde U_1^T \ldots \tilde U_d \tilde U_d^T) \times A$$
where $\tilde U_\mu$ is the matrix of the first $k_\mu$ columns of $U_\mu$.


Let $A \in \mathbb{R}^{\mathcal{I}}$. We denote the best approximation of $A$ in Tucker by $A^{best}$ . The error of the truncation is bounded by:

$$\|A -  \mathcal{T}_{(k_1, \ldots, k_d)}(A)\| \leq   \sqrt{\sum_{\mu=1}^{d} \sum_{i=k_\mu + 1}^{n_\mu} \sigma^2_{\mu,i}} \leq \sqrt{d} \|A - A^{best}\|$$

The \textbf{Hierarchical Tucker} format is a multilevel variant of the Tucker format. \\

A dimension tree or mode cluster tree $\mathcal{T}_\mathcal{I}$ for dimension $d \in \mathbb{N}$ is a tree with root $Root(\mathcal{T}_\mathcal{I}) = \{1,...,d\}$ and depth $p = [log2(d)] := min_{i \in \mathbb{N}_0 | i \geq log2(d)}$ such that each node $t \in T_d$ is either \\
1. a leaf and singleton $t= \mu$ on level $ l \in \{p-1, p\}$  \\
2. the union of two disjoint successors $ S(t) = \{s1, s2\}$ \\
The level $l$ of the tree is defined as the set of all nodes having a distance of exactly $l$ to the root.
The canonical dimension tree is of the form presented where each node has two successors.
For a complete binary tree  the total number of nodes is $2d -1$, the number of leaves is $d$ and the number of interior nodes is $d - 1$. \\

Let $\mathcal{T}_\mathcal{I}$ be a dimension tree. The hierarchical rank $(k_t)_{t\in T_\mathcal{I}}$ of a tensor $A \in \mathbb{R}^{\mathcal{I}}$ is defined by

$$ \forall t \in T_\mathcal{I}, \qquad k_t=rank(A^{(t)}).$$

Let $t \in \mathcal{T}_{\mathcal{I}}$ be a mode cluster with rank $k_t$. We call a matrix $U_t \in \mathbb{R}^{\mathcal{I}_t \times k_t}$ a t-frame. The frame is orthogonal if its columns are, and a frame tree is called orthogonal if each frame except t he root frame is orthogonal. The transfer tensor is defined as the tensor $B_t \in \mathbb{R}^{k_t \times k_{t_1} \times k_{t_2}}$ that reads:
$$ U_{t,i} = \sum^{k_{t_1} }_{j=1} \sum^{k_{t_2} }_{l=1} B_{t,i,j,l} U_{t_1,j} \otimes U_{t_2,l}$$

It is remarkable that the total storage for all transfer tensors and leaf-frames in terms of number of entries is bounded by $(d-1)k^3 + \sum^d_{\mu = 1} n_\mu$, that is linear in the dimension, where $k$ is the max of all $k_t$.


Since $\pi_t A$, where $\pi_t$ is an orthogonal projection, is the best approximation pf A with $\mu-$mode rank $k_t$, we also have for the Tucker approximation that:

$$\|A -  \mathcal{T}_{(k_1, \ldots, k_d)}(A)\| \leq \sqrt{d} \|A - A^{best}\|$$

If we let $A^{best}$ denote the best approximation of $A$ in H-Tucker, and we let $\pi_t$ be the orthogonal frame projection for the t-frame $U_t$ that consists on the left singular values of $A^{(t)}$ the inequality could be rewritten as:

$$\|A - \prod_{t \in \mathcal{T}_\mathcal{I} } \pi_t (A)\| \leq   \sum_{t \in \mathcal{T}_\mathcal{I}} \sum_{i < k_t } \sigma^2_{t,i}  \leq \sqrt{2d-2} \|A - A^{best}\|$$

This result is not optimal. It can be improved if for the root t of the dimension tree and its successors $t_1, t_2$ we combine both projections into a single projection via SVD. This combined projection has the same error as any of the two projection. The error of truncation then holds:

$$\|A - \prod_{t \in \mathcal{T}_\mathcal{I} } \pi_t (A)\| \leq \sqrt{2d-3} \|A - A^{best}\|$$

That for $d=2$ it coincides with the SVD.\\


Let  $\mathcal{T}_\mathcal{I}$ be a dimension tree, $A \in \mathbb{R}^{\mathcal{I}}$, and $\epsilon > 0$. If there exists a tensor $A^{best}$ of hierarchical rank $k_t$ and an error smaller than $\epsilon$, the singular values of $A^{(t)}$ for each node $t$ can be estimated by: $\sqrt{\sum_{i >k_t } \sigma^2_{i}} \leq \epsilon$. On the other hand, if the singular values fulfill $\sqrt{\sum_{i >k_t } \sigma^2_{i}} \leq \epsilon / \sqrt{2d-3}$, the truncation yields an H-Tucker $A_H = \prod_{t \in \mathcal{T}_\mathcal{I} } \pi_t A$ such that $\|A - A_H\| \leq \epsilon$. In \ref{Alg1} a method for the truncation of a tensor to hierarchical rank is provided: \\

\begin{algorithm}[H]
 \KwData{Input tensor $A\in \mathbb{R}^{\mathcal{I}}$, dimension tree $\mathcal{T}_\mathcal{I}$ (depth $p > 0$), target representation rank $k_t$.}
 initialization\;
 \For{Each singleton $t$}{
 Compute the SVD of $A^{(t)}$ and store the dominant $k_t$ left singular vectors in the columns of the t-frame $U_t$\;
 }
  \For{$l= p-1, \ldots, 0$}{
  	\For{Each mode cluster $t$ on level $l$}{
	 Compute the SVD of $A^{(t)}$ and store the dominant $k_t$ left singular vectors in the columns of the t-frame $U_t$\;
	 Let $U_{t_1}, U_{t_2}$ denote the frames for the successors of $t$ on level $l+1$. Compute the entries of the transfer tensor\;
	 $$B_{t,i,j,\nu}=\langle U_{t,i}, U_{t_1,j} \otimes U_{t_2,\nu} \rangle$$

	}
 }
 Compute the entries of the root transfer tensor\;
 $$B_{\{1, \ldots, d \},i,j,\nu}=\langle A, U_{t_1,j} \otimes U_{t_2,\nu} \rangle$$
 
Return H-Tucker representation for $A_H$.\;


 \caption{Root-to-leaves truncation of arbitrary tensors to H-Tucker format}
 \label{Alg1}
\end{algorithm}


Because we want to work with a core tensor that becomes smaller close to the root, we have to computed the SVD not of the original tensor but of a truncated one. We define the hierarchical  leaves-to-root truncation by $A_H = A_{\tilde H, 1}$. In \ref{Alg2}, the algorithm is provided.

\begin{algorithm}[H]
 \KwData{Input tensor $A\in \mathbb{R}^{\mathcal{I}}$, dimension tree $\mathcal{T}_\mathcal{I}$ (depth $p > 0$), target representation rank $k_t$.}
 initialization\;
 \For{Each singleton $t$}{
 Compute the SVD of $A^{(t)}$ and store the dominant $k_t$ left singular vectors in the columns of the t-frame $U_t$\;
 }
 Compute the core tensor $C_p = (U_1^T, \ldots, U_d^T)\otimes A$\;
  \For{$l= p-1, \ldots, 0$}{
  	\For{Each mode cluster $t$ on level $l$}{
	 Compute the SVD of $C_{l+1}^{(t)}$ and store the dominant $k_t$ left singular vectors in the columns of the t-frame $U_t$\;
	 Let $U_{t_1}, U_{t_2}$ denote the frames for the successors of $t$ on level $l+1$. Compute the entries of the transfer tensor\;
	 $$B_{t,i,j,\nu}=\langle U_{t,i}, U_{t_1,j} \otimes U_{t_2,\nu} \rangle$$

	
	Update the core tensor $C_l = U_t^T \otimes C_l$\;
	}
 }
 

Return H-Tucker representation for $A_H$.\;


 \caption{Leaves-to-root truncation of arbitrary tensors to H-Tucker format}
 \label{Alg2}
\end{algorithm} 

The error of the  leaves-to-root truncation $A_{\tilde H}$ is bpunded by:

$$ \|A - A_{\tilde H, p}\|  \leq (2 + \sqrt{2}) \sqrt{d} \|A - A^{best}\|$$

\textbf{Truncation of Hierarchical Tucker tensors}

This section is analogous to the previous section but for the special case that the input tensor is already given in the hierarchical Tucker format.

Let $\mathcal{T}_\mathcal{I}$ be a dimension tree and $t \in \mathcal{T}_\mathcal{I}$ a non-root mode cluster with father $f$. Then we define the unique mode cluster $\bar t \in \mathcal{T}_\mathcal{I}$ as the brother of $t$.

The matricization of a tensor $A$ would be:

$$A^{(t)} = \sum^k_{\nu =1}u_\nu v_\nu^T,$$
where $u_\nu = \sum^{k_1}_{j=1}  \sum^{k_2}_{l=1} c_{\nu,j,l}x_j \otimes y_l, \quad x_j \in \mathbb{R}^{\mathcal{I}_{t_1}}, y_l \in \mathbb{R}^{\mathcal{I}_{t_2}}$ and $\nu = 1, \ldots, k$.	

now, if we denote $\bar U^1, \ldots, \bar U^p$ the frames, $B^0, \ldots, B^{p-1}$ the corresponding transfer tensors and $k_0, \ldots, k_p$ the representation ranks, we have:

$$ U_{t_l,\nu} = \sum_{i} \sum_{j} B^l_{\nu,i,j} \bar U^{l+1}_{i} \otimes U_{t_{l+1},j}$$

And the t-matricization has the form:
$$A^{(t)}=U_t V^T_t$$
where the complementary frame $V_t$ is defined by its columns: 
$$V_{t,j_p} = \left( \sum^{\bar k_1}_{i_1 = 1} \sum^{k_1}_{j_1 = 1}  \ldots \sum^{ k_{p-1}}_{j_{p-1} = 1}  \sum^{\bar k_p}_{i_p = 1} B^0_{1, i_1, j_1} \ldots B^{p-1}_{j_{p-1}, i_p, j_p}\right) \bar U^1_{i_1} \otimes \ldots \bar U^p_{i_p}$$

where $p$ is the length of the path.

For a notation relief, let $k_\nu = k_{t_\nu}$ and  $\bar k_\nu = k_{\bar t_\nu}$. Then we define the accumulated transfer tensor by
$$\hat B^1_{j_1, s_1} = \sum^{\bar k_1}_{i_1=1} B^0_{1, i_1, j_1} B^0_{1,i_1,s_1}$$
$$\hat B^l_{j_l, s_l} = \sum^{k_{l-1}}_{s_{l-1}=1}  \sum^{\bar k_l}_{i_{l}=1}     \left( \sum^{k_{l-1}}_{j_{l-1}=1}  \hat B^{l-1}_{j_{l-1}, s_{l-1}} B^{l-1}_{j_{l-1}, i_l, j_l}  \right) B^{l-1}_{s_{l-1}, i_l, s_l}$$
$$\hat B_t = \hat B^p$$

The first accumulated tensors for the two sons of the root $t$ can be computed in $\mathcal{O}(k_{t_1} k_{t_2}^2 + k_{t_1}^2 k_{t_2} )$. The definition of accumulates transfer tensors involves the bracket a matrix multiplication of complexity $\mathcal{O}(k_{t}^2 k_{t_1} k_{t_2})$ for each son and the outer multiplication of complexity $\mathcal{O}(k_t k_{t_1} k_{t_2}^2 + k_t k_{t_1}^2 k_{t_2} )$. For all nodes this is just the sum for all $t$.\\

We can easily compute the left singular vectors pf $V_t^T$ which are the eigenvectors of the $k_t \times k_t$ matrix $\hat B_t$. The matriz $Q_t$ of singular vectors is the transformation matrix such the $U_tQ_t$is the matrix of the left singular vectors of $A^{(t)}$, the singular values of which are the square roots of the eigenvalues of $\hat B_t$. Thus, one can truncate either to fixed rank or one can determine the rank adaptively in order to guarantee a truncation accuracy of $\epsilon$.\\
The nested mode frames were required to be orthogonal. If this is not yet the case, one has to orthogonalize the frame tree.\\


Let  $t \in \mathcal{T}_\mathcal{I}$ be a mode cluster with t-frame $U_t$, transfer tensor $\hat B_t$ and two sons $t_1, t_2$ with frames $U_{t_1}, U_{t_2}$ such that the columns fulfill

$$U_{t,i} = \sum_{j=1}^{k_1} \sum_{l=1}^{k_2} B_{t,i,j,l}  U_{t_1, j} \otimes U_{t_{2},l}, \qquad i=1, \ldots, k$$ 

Let $X \in \mathbb{R}^{k \times k}, Y \in \mathbb{R}^{k_1 \times k_1}, Z \in \mathbb{R}^{k_2 \times k_2}$ with $Y$ and $Z$ invertible. Then we can rewrite the transformed frames as:
$$U_{t,i}X_i = \sum_{j=1}^{k_1'} \sum_{l=1}^{k_2'} B'_{t,i,j,l}  U_{t_1, j}Y_j \otimes U_{t_{2},l}Z_l, \qquad B'_t = (X^T, Y^{-1}, Z^{-1})B_t.$$

The complexity of the orthogonalization (showed in Alg 3) for a H-Tucker tensor with nested frames is bounded by:  $\mathcal{O}(\sum^d_{\mu=1} n_\mu k_\mu^2 + \sum_{t \in \mathcal{T}_\mathcal{I}, sons(t)= t_1, t_2} k_t^2 k_{t_1} k_{t_2} + k_t k_{t_1}^2 k_{t_2} + k_t k_{t_1} k_{t_2}^2)$. Here the steps: For each interior node we have to compute QR decompositions which are of complexity $\mathcal{O}(k_{t}^2 k_{t_1} k_{t_2})$, and perform two mode multiplications, of complexity  $\mathcal{O}(k_t k_{t_1} k_{t_2}^2 + k_t k_{t_1}^2 k_{t_2} )$. For the leaves, a QR decomposition of complexity $\mathcal{O}(n_\mu k_\mu^2)$. The sum  over all nodes of the tree yields the desired bound. \\

\begin{algorithm}[H]
 \KwData{Input tensor $A_H \in H-Tucker$.}
 initialization\;
 \For{Each singleton $t$}{
 Compute the QR decomposition of the t-frame $U_t$ and define: \\
 $U_t = Q, \quad B_f = (I,I,R)\otimes B_f$ if t is the second successor, or $B_f = (I,R,I)\otimes B_f$ if t is the first successor for the father $f$ of $t$.
  }
  \For{each mode cluster $t$}{
 Compute a QR decomposition of $B_t^{1,2}$, 
 $$ B_t^{1,2} = Q_t^{1,2} R$$
 and set $B_t = Q, \quad B_f = (I,I,R)\otimes B_f$ if t is the second successor, or $B_f = (I,R,I)\otimes B_f$ if t is the first successor for the father $f$ of $t$.
}
 
Return H-Tucker representation for $A_H$.\;


 \caption{Orthogonalization of H-Tucker tensors}
 \label{Alg3}
\end{algorithm} 

\textbf{CP format to H-Tucker format}
Let $A \in \mathbb{R}^{\mathcal{I}}$ be  a tensor represented by an elementary tensor sum:
$$A = \sum_{i=1}^k a_{i,1} \otimes \ldots \otimes a_{i,d}$$
Then, A can immediately be represented in the H-Tucker format by the t-frames:

$$\forall t = \{ \mu \} \in \mathcal{L}(\mathcal{T}_\mathcal{I}): \quad U_{t,i}=a_{i,\mu}, \quad i=1, \ldots, k, k_\mu = k,$$

And the transfer tensors:

$$ B_{t,i,j,l} = 1 \quad if \quad i=j=l$$

and 0 otherwise. The frames are not yet orthogonal, so a subsequent orthogonalization and truncation is advisable to find a reduced representation. In sparse format, the amount of storage needed is $k(d-1) + k\sum^d_{\mu=1}n_\mu$.\\

\subsection{Comparison with other formats}

The sequential unfolding SVD or PARATREE from is defined similarly as the H-Tucker. The first separation is via the SVD of the familiar form:

$$A= \sum^r_{i_1 = 1} U_{1,i_1} \otimes U_{2, i_1}$$

On level 1, $U_{\nu, i_1}$ is then split with SVD leading to a different set of vectors. Each of this vectors is split again on level 2. O level l the frames are indexed by $U_{\nu_1, i_1, \ldots, \nu_l, i_l}$.  Thus, the complexity is no longer linear in the dimension d but scales exponentially in the depth of the tree. \\

Hierarchical MCTDH and $\phi$-system. Both formats are exactly the H-Tucker form. \\

The Tensor Train (TT) format of a tensor is given by a family of integers $k_q$, matrices $G^1, G^d$ and tensors $G^2, \ldots, G^{d-1}$ such that:
$$A_{i_1, \ldots, i_d} = \sum^{k_1}_{j_1=1} \ldots \sum^{k_{d-1}}_{j_{d-1}=1} G^1_{i_1, j_1} G^2_{i_2, j_2, j_2}  \ldots G^{d-1}_{i_{d-1}, j_{d-2}, j_{d-1}}   G^{d}_{i_{d}, j_{d-1}} $$

If the tensor A is given in H-Tucker for a tree with root $t_0$ and successors $t_1, \ldots, t_{d-1}$ that are leaves and $\bar t_1, \ldots,\bar  t_{d-1}$ their respective brothers, then:

$$G^1_{i,j} = \sum^{k_{t_1}}_{l=1} B_{t_0,1,l,j} U_{t_1,i,l}, \qquad G^d_{i,j} = U_{\bar t_{d-1},i,l}, \qquad G^q_{i,j,m} = \sum^{k_{\bar t_q}}_{l=1} B_{\bar t_{q-1},j,l,m} U_{t_q,i,l}$$

for $q=2, \ldots, d-1$. \\


\textbf{Paralelization:} The tree structure for those trees having a depth proportional to $log(d)$, is perfectly suited for parallelization. On each level all operations can be performed in parallel for all nodes on that level. Since the SVDs and QR decompositions in each node are hardly parallelizable and of complexity $\mathcal{O}(k^3)$ and $\mathcal{O}(k^4)$, respectively, the parallelization has to be done with respect to the order $d$ of the tensor. \\

\subsection{Numerical experiments}

Some tests have been made about how to truncate the decomposition. Let A be a dense tensor with entries:
$$A_{i_1, \ldots, i_d} = \left( \sum^d_{\mu =1 } i_\mu^2 \right)^{-1/2}$$
The first example is for $d=5$. The time for the conversion (Algorithm 1) of the dense tensor to H-Tucker format and the storage needed are shown in Table 6.1.\\

The second example is in higher dimension d: The truncation of elementary tensor sums in H- Tucker format. Where the tensor (represented as an elementary tensor sum) is then converted to H-Tucker format.  From this input tensor we compute truncations, $A_{H,\epsilon}$ to lower hierarchical rank by prescribing the (relative) truncation accuracy $\epsilon$. We observe that the accuracy is: $\|A_H - A_{H,\epsilon} \| / \|A_H \| \approx 3 \epsilon$. \\

The third test is not any more concerned with the approximation accuracy, but purely with the computational complexity. Here, we setup an H-Tucker tensor with node-wise ranks $k_t=k$. Then, we vary the rank $k$ and dimension parameter $d$ and measure the storage complexity as well as the complexity for the truncation.

\newpage
\section{Geometry of tree-based tensor formats in tensor Banach spaces (Antonio Falco, Wolfgang Hackbusch, Anthony Nouy)}

In this paper, the authors provide a new geometrical description of manifolds of tensors in tree-based (or hierarchical) format, also known as tree tensor networks, which are intersections of manifolds of tensors in Tucker format associated with different partitions of the set of dimensions. The proposed geometrical description of tensors in tree-based format is compatible with the one of manifolds of tensors in Tucker format. \\

 Sets of tensors in tree-based tensor format are the intersection of a collection of sets of tensors in Tucker format associated with a hierarchy of partitions given by a tree. More precisely, given a dimension partition tree $T_D$ over $D$, we can define a sequence of partitions $\mathcal{P}_1, \ldots ,\mathcal{P}_L$ of $D$, with $L$ the depth of the tree, such that each element in $\mathcal{P}_k$ is a subset of an element of $\mathcal{P}_{k-1}$. For each partition $\mathcal{P}_k$, a tensor in $\textbf{V}_D$ can be identified with a tensor in  $\textbf{V}_{\mathcal{P}_k} = \bigotimes_{\alpha \in \mathcal{P}_k} \textbf{V}_{\alpha}$, that allows to define manifolds of tensors in Tucker format. Here it's defined the set $\mathcal{FT}_r(\textbf{V}_D)$ of tensors in $\textbf{V}_D$ with tree-based rank r. \\
 
 In the paper, some preliminary topological results and properties of tensors in tree-based Tucker and H-Tucker are shown. We proceed directly to the definition of a dimension partition tree.\\
 
 A tree $T_D$ is called \textbf{Dimension partition tree} over D if: \\
 \textbullet All vertices $\alpha \in T_D$ are non-empty subsets of D. \\
 \textbullet D is the root of $T_D$. \\
 \textbullet Every vertex $\alpha \in T_D$ with $\# \alpha \geq 2$ has at least two sons and the set of sons of $\alpha$, denoted $S(\alpha)$, is a non-trivial partition of $\alpha$. \\
 \textbullet Every vertex $\alpha \in T_D$ with $\#\alpha =1$ has no son is called a leaf. \\
 
 The set of leaves is called $\mathcal{L}(T_D)$. The levels of the vertices of a dimension partition tree $T_D$, denoted by $level(\alpha), \alpha \in T_D$ are integers defined such that $level(D)=0$ and for any pair $\alpha, \beta \in T_D$ such that $\beta \in S(\alpha), level(\beta)=level(\alpha) +1$. The depth of the tree is defined as the maximum $level(\alpha)$. Then to each level, is associated a partition of D:
 $$\mathcal{P}_k(T_D) = \{ \alpha \in T_d = level(\alpha)=k\}\cup \{ \alpha \in \mathcal{L}(T_D) : level(\alpha) < k\}$$
 
 
Let $U^{min}_{\alpha} (v) < \infty$ be the minimal subspace of  a certain tensor. For a given dimension partition tree $T_D$ over D, we define the \textbf{tree-based rank} of a tensor $v \in \textbf{V}_D$ by the tuple $rank_{T_D} (v)= (dim U^{min}_{\alpha} (v))_{\alpha \in T_D} : v \in \textbf{V}_D$. \\

Let $T_D$ be a given dimension partition tree and fix some tuple $r \in \mathcal{AD}(\textbf{V}_D,T_D)$. Then the set of tensors of fixed tree-based rank $r$ is defined by:
$$\mathcal{FT}(\textbf{V}_D,T_D) = \{v \in \textbf{V}_D : dim U^{min}_{\alpha}(v) = r_{\alpha}, \quad for \quad all \quad \alpha \in T_D \}$$

and the set of tensors of tree-based rank bounded by $r$ is defined by:
$$\mathcal{FT}(\textbf{V}_D,T_D) = \{v \in \textbf{V}_D : dim U^{min}_{\alpha}(v) \leq r_{\alpha}, \quad for \quad all \quad \alpha \in T_D \}$$

For a dimension partition tree $T_D$ and for $r \in \mathcal{AD}(\textbf{V}_D,T_D)$, we will say that $\mathcal{FT}_r(\textbf{V}_D, T_D)$ is a proper set of tree-based tensors with a fixed tree-based rank $r$ if
$$\mathcal{FT}_r(\textbf{V}_D, T_D) \neq \mathcal{M}(\textbf{V}_D, \mathcal{P}_k(T_D)), \quad holds \quad for \quad 1 \leq k \leq depth(T_D)$$

For a dimension partition tree $T_D$ and for $r \in \mathcal{AD}(\textbf{V}_D,T_D)$, assume that $\mathcal{FT}_r(\textbf{V}_D, T_D)$ is a proper set of tree-based tensors with a fixed tree-based rank $r$ such that
$$\mathcal{FT}_r(\textbf{V}_D, T_D) \cap \mathcal{M}(\textbf{V}_D, \mathcal{P}_k(T_D))$$

We have that for each $1 \leq k \leq depth(T_D)$ the collection $\mathcal{A}_k=\{(\mathcal{U}^k (v), \xi^k_v))\}$ is a $C^{\infty}$-atlas for $\mathcal{M}(\textbf{V}_D, T_D)$ and hence $\mathcal{M}(\textbf{V}_D, T_D)$ is a $C^{\infty}$-Banach manifold. \\

We could also see $\mathcal{FT}_r(\textbf{V}_D, T_D)$ as embedded sub-manifold of $ \mathcal{M}(\textbf{V}_D, T_D)$.


\newpage
\section{Tree based tensor formats (Antonio Falco, Wolfgang Hackbusch, Anthony Nouy)}

The main goal of this paper is to study the topological proper- ties of tensors in tree-based Tucker format. A property of the so-called minimal subspaces is used for obtaining a representation of tensors with either bounded or fixed tree-based rank in the underlying algebraic tensor space. \\

The Tucker and the HT formats are completely characterised by a rooted tree together with a finite sequence of natural numbers associated to each vertex of the tree, denominated the tree-based rank. Each number in the tree-based rank is associated with a class of subspaces of fixed dimension. It can be shown that for a given tree, every element in the tensor space possesses a unique tree-based rank. In consequence, given a tree, a tensor space is a union of sets indexed by the tree-based ranks. It allows to consider for a given tree two kinds of sets in a tensor space: the set of tensors of fixed tree-based rank and the set of tensors of bounded tree-based rank. \\

Let $v \in \textbf{V}_D$. For any $\alpha \in 2^D$ with $\# \alpha \geq 2$ and a non-trivial partition $\mathcal{P}_\alpha$ og $\alpha$, it holds:
$$U^{min}_{\alpha} (v) \in \bigotimes_{\beta \in \mathcal{P}_{\alpha}} U^{min}_{\beta} (v) < \infty$$

Let $\mathcal{P}_D$ be a non-trivial partition of D. The algebraic tensor spave $\textbf{V}_D$ is identified with $\bigotimes_{\alpha \in \mathcal{P}_{D}} \textbf{V}_D$. By definition of the minimal subspaces $U^{min}_{\alpha} (v), \quad \alpha \in \mathcal{P}_D$, we have:
$$v \in \bigotimes_{\alpha \in \mathcal{P}_{D}}  U^{min}_{\alpha} (v)$$




 A tree $T_D$ is called \textbf{Dimension partition tree} over D if: \\
 \textbullet All vertices $\alpha \in T_D$ are non-empty subsets of D. \\
 \textbullet D is the root of $T_D$. \\
 \textbullet Every vertex $\alpha \in T_D$ with $\# \alpha \geq 2$ has at least two sons and the set of sons of $\alpha$, denoted $S(\alpha)$, is a non-trivial partition of $\alpha$. \\
 \textbullet Every vertex $\alpha \in T_D$ with $\#\alpha =1$ has no son is called a leaf. \\
 
 The set of leaves is called $\mathcal{L}(T_D)$. 


\end{document}