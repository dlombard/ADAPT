\documentclass[a4paper,12pt]{article}
\usepackage{epsfig}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bbold}
\usepackage{algorithm2e}

\linespread{1.0}
\setlength{\topmargin}{0cm}
\setlength{\oddsidemargin}{0cm}
\setlength{\evensidemargin}{0cm}
\setlength{\textwidth}{16.0cm}
%\thispagestyle{empty}
\title{Proof of errors in subdomains}
\date{}

\newtheorem{mylemma}{}
\newtheorem{myprop}{}
\newtheorem{mytheo}{}
\newtheorem{mydef}{}
\newtheorem{myex}{}


\begin{document}
The heuristic of the proposed approach is the following: it is not true, in general, that a function belonging to the unit ball of $W^{k,p}$ can be represented in a parsimonious way in a tensor format (in a separate way). However, it is true that it can on a small subset of a domain. 
 
A proof is proposed aiming at making the above heuristics on the tensor approximation of functions precise. 
\subsection*{Notation.}
Let $Q\subset{R}^d$ be a finite measure hypercube, whose measure is denoted by $\mu(Q)$. A function $u:Q \rightarrow \mathbb{R}$ is considered, where $u\in W^{k,p}$ such that $\| u \|_{W^{k,p}(Q)}\leq 1$. The function to be approximated is in the unit ball of a standard Sobolev space. 
Let us consider a congruent partition of the hypercube in $2^{dN}$ sub-cubes $Q_i$, that is, a subdivision such that $Q=\cup_{i=1}^{2^{Nd}} Q_i$ and in which all the sub-cubes are disjoint ($\mu(Q_i\cap Q_j) = 0$). A piecewise polynomial approximation in each of the sub-domains is introduced. Let $\alpha$ be a $d$ multi-index, with the usual notation $|\alpha| = \sum_{i=1}^d \alpha_i$, the monomial $x^{\alpha}=x_1^{\alpha_1}\cdot \ldots \cdot x_d^{\alpha_d}$, and $\alpha! = \prod_{i=1}^d \alpha_i$. Let the weak derivative of order $\alpha$ being denoted by $D^{(\alpha)}$, a projector is defined, based on the Taylor kernel:
\begin{equation}
P^{(k)}_i u = \sum_{|\alpha|<k}\int_{Q_i}\frac{(x-y)^{\alpha}}{\alpha!} D^{(\alpha)} u(y) \ dy.
\end{equation}
The projector $P^{(k)}_i$ defines a polynomial approximation of $u$ of rank $k-1$ on the subdomain $Q_i$. 

The following proof is an adaptation of a proof by Edmunds (1989) deriving entropy numbers asymptothics of the unit ball of Sobolev spaces. 

\begin{myprop} Proposition: Let the domain be $Q\in \mathbb{R^d}$ and the function to be approximated be   $u \in W^{k,p}(Q)$, with $\| u \|_{W^{k,p}(Q)}\leq 1$.  Let $1\leq p \leq q \leq \infty$ and $\lambda = \frac{k}{d} - \frac{1}{p} + \frac{1}{q}>0$. Then, let $\varepsilon>0$, there exist a congruent partition in $2^{dN}$ sub-domains such that a piece-wise finite rank ($R\leq \frac{(k-1+d)!}{(k-1)! d!}$) tensor approximation of $u$ achieves:
\begin{equation}
\| u - \sum_{j=1}^{2^{dN}} T_j \|_{L^q(Q_i)} \leq \varepsilon.
\end{equation}
\begin{proof}
The first remark is that $\lambda>0$ implies, by Rellich-Kondrakov theorem, that the identity $\mathcal{I}: W^{k,p}\rightarrow L^q$ is a compact operator, mapping a bounded set (the unit ball of $W^{k,p}$) into a pre-compact one ($\in L^{q}$), that can therefore be covered by a finite number of balls. This is the geometric fact allowing to build a finite rank local approximation. Let us introduce the polynomial approximation in each of the sub-domains; the error is such that:
\begin{equation}
\| u - P^{k}_i u \|_{L^q(Q_i)} \leq C \mu(Q_i)^{\lambda} \| u \|_{W^{k,p}(Q_i)},
\end{equation}
that follows from the application of a Taylor kernel approximation and by bounding the error using the largest order weak derivatives. 
The global error of the approximation is then:
\begin{equation}
\|u-P^{(k)}u\|_{L^q(Q)}^q = \sum_{i=1}^{2^{dN}} \| u - P^{k}_i u \|_{L^q(Q_i)}^q \leq \sum_{i=1}^{2^{dN}} C^q \mu(Q_i)^{q \lambda} \| u \|_{W^{k,p}(Q_i)}^q,
\end{equation}
where the first equality is due to the congruent subdivision of the domain. Since the sub-domains are all equal, $\mu(Q_i)=2^{-dN}\mu(Q)$, leading to:
\begin{equation}
\|u-P^{(k)}u\|_{L^q(Q)}^q \leq C^q \left(2^{-dN}\mu(Q) \right)^{q\lambda} \sum_{i=1}^{2^{dN}}\| u \|_{W^{k,p}(Q_i)}^q.
\end{equation}
In order to bound the last term, let us consider the following inequality:
\begin{equation}
\sum_{i=1}^{2^{dN}}\| u \|_{W^{k,p}(Q_i)}^q = \sum_{i=1}^{2^{dN}} \left(\| u \|_{W^{k,p}(Q_i)}^p\right)^{\frac{q}{p}} \leq \left( \sum_{i=1}^{2^{dN}} \| u \|_{W^{k,p}(Q_i)}^p \right)^{\frac{q}{p}}\leq 1,
\end{equation}
that holds true because $\| u \|_{W^{k,p}(Q_i)}\leq 1$ and $q\geq p$. 

Putting the estimates together, the global approximation error reads:
\begin{equation}
\|u-P^{(k)}u\|_{L^q(Q)} \leq C \mu(Q)^{\lambda} 2^{-\lambda dN}.
\end{equation}
Since $\lambda>0$ there exists a partition such that the error is smaller than a given $\varepsilon$. To conclude the proof, let us observe that a multi-variate polynomial can be written as a tensor of order at most $R = \frac{(k-1+d)!}{(k-1)! d!} $. 
\end{proof}
\end{myprop}

Few remarks are in order. First, observe that for functions in $H^1(Q)$ a piece-wise local tensor approximation of rank 1 is possible when the error is measured in $L^2$ norm.

Second, this estimate shows that subdivide as a first step is a good strategy for moderate dimensions tensors (a subdivision in $2^{dN}$ is needed). 

The estimate provided gives a precise sense to the statement: \textit{locally there exist a low-rank tensor approximation}.     

\end{document}