\section{Introduction}
\label{sec:intro}

Tensor formats~\cite{hackbusch2007tensor} have proved to be an effective and versatile tool to approximate high-dimensional functions or high-order tensors.
A comprehensive overview is found in \cite{khoromskij2012tensors,kolda2009,hackbusch2012,grasedyck2013literature}. Tensors can be used not only to approximate 
a given datum, but also to efficiently compute the solution of high dimensional problems \cite{bachmayr2016tensor,ballani2015hierarchical,schwab2011sparse,kressner2011low}, 
specified by systems of equations and data. The method which is proposed in the present work is rather general, but was motivated by the computation of the solution of equations arising in Kinetic theory. 
In \cite{EhrlacherLombardi} the solution of the Vlasov-Poisson system was approximated by means of adaptive tensors. The solution of this system of 
partial differential equations at time $t>0$ reads as a function for $x\in \R^d$ and $v\in \R^d$ where $d=1,2,3$ denotes the space dimension. At each time $t>0$, the function $f(t,x,v)$ 
was approximated by a low-rank function as follows
$$
f(t,x,v) \approx \sum_{k=1}^{n_t} r_k(t,x) s_k(v,t),
$$
where the rank $n_t$ was adapted in order to control through the time evolution the error between the true function and its approximation. In several test cases, it was observed in~\cite{EhrlacherLombardi} that the rank of the approximation 
of $f(t,x,v)$ grows linearly with the time $t$, which unfortunately makes the use of traditional tensor formats unfit for long time simulations. However, it was observed that the function $f(t,x,v)$ can be nevertheless very well approximated by low-rank approximations 
in \itshape some large \normalfont regions of the phase space, and has to be approximated by full-order tensors in \itshape some small \normalfont regions.

Henceforth, a parsimonious representation of the solution could be obtained by partitioning the domain in sub-regions and computing a piece-wise tensor approximation. 
In the present work, we start by considering the approximation of a given function (a compression problem), by taking care that the sub-regions and the rank of the tensor 
approximations are not fixed \emph{a priori}. Instead, they are adapted automatically according to a prescribed accuracy. Similar ideas can be found in hierarchical matrices~\cite{bebendorf2008hierarchical,hackbusch2015hierarchical}. 
In this work, we generalise this idea to tensors which \itshape may not \normalfont be the discretisation of asymptotically smooth functions. The idea of hierarchical matrices was adapted to tensors in \cite{khoromskij2007structured} in the context of the discretisation of Boltzmann equations. The method proposed hereafter is somehow similar in the spirit, but it is featured by automatic adaptation on the basis of an error criterion and storage optimisation. 
An adaptation principle along fibers for Tensor Train format is proposed in \cite{gorodetsky2019}, in which the subdomains are splitted, with a top-down approach if an approximation criterion along each fiber is not satisfied. There are two main differences with respect to the strategy proposed in this work: we propose adaptation by splitting in sub-regions (and not along fibers), by using a bottom-up approach.    

This work investigates an adaptive compression method for tensors based on local High-Order Singular Value Decomposition (HOSVD). Let us mention already here that we do not aim at approximating tensors of very high order, but only tensors of moderate order. Two main contributions are presented. The first one is a greedy method to automatically distribute the approximation error in the partitions, in which local HOSVD are computed. To the authors knowledge, no algorithmic procedure has been proposed so far to perform such a task. The second one is a merging strategy aimed at optimising the storage by fusing together sub-regions in which a low-rank (in HOSVD sense) decomposition performed on the union would be beneficial in terms of storage. The outcome of the method, that we call Hierarchical Partitioning Format (HPF) is a non-uniform adapted piece-wise tensor approximation, that guarantees a prescribed accuracy and provides a significant memory compression. This work can be seen as a first step to solve high-dimensional Partial Differential Equations. 

The structure of the work is as follows: in Section \ref{sec:notation} the notation  and some elements of the theoretical analysis for continuous tensors are presented. The method 
proposed in the present work consists of two steps: the first one (a greedy strategy to approximate sub-tensors) is presented in Section \ref{sec:GHOSVD}, the second one, an adaptive merge to 
optimise the storage, is presented in Section \ref{sec:HMerge}. 
%An overview on the linear algebra operations in the proposed format is presented in Section \ref{sec:linAlg}. 
Then, some numerical experiments are detailed in Section \ref{sec:num}. 

Finally, we present the performance of our algorithm in terms of compression rates on several numerical tests, among which the compression of the solution of the Vlasov-Poisson system in a double-stream instability test case.


