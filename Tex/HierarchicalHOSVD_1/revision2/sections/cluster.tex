
\subsection{Partition tree}

To this aim, we introduce here the notion of \itshape partition tree\normalfont. A partition tree may be seen as a generalization of \itshape cluster tree \normalfont as defined in \cite{bebendorf2008hierarchical} for hierarchical matrices. 


Let $T_{\boldsymbol{I}}$ be a tree with vertices (or nodes) $\mathcal{V}(T_{\boldsymbol{I}})$ and edges $\mathcal{E}(T_{\boldsymbol{I}})$. For all $\boldsymbol{J}\in \mathcal{V}(T_{\boldsymbol{I}})$, we denote by 
$$
\mathcal{S}_{\boldsymbol{J}}(T_{\boldsymbol{I}}):= \left\{ \boldsymbol{J}' \in \mathcal{V}(T_{\boldsymbol{I}}), \; (\boldsymbol{J}, \boldsymbol{J}')\in \mathcal{E}(T_{\boldsymbol{I}})\right\}
$$
the set of sons of the vertex $\boldsymbol{J}$. By induction, we define the set of sons of $\boldsymbol{J}$ of the $k^{th}$ generation, denoted by $\mathcal{S}^k_{\boldsymbol{J}}(T_{\boldsymbol{I}})$ with $k\in \mathbb{N}^*$ as follows
$$
\mathcal{S}^1_{\boldsymbol{J}}(T_{\boldsymbol{I}}) = \mathcal{S}_{\boldsymbol{J}}(T_{\boldsymbol{I}}), \quad \mathcal{S}^k_{\boldsymbol{J}}(T_{\boldsymbol{I}})= \left\{ \boldsymbol{J}'' \in \mathcal{V}(T_{\boldsymbol{I}}), \; \exists \boldsymbol{J}'\in \mathcal{S}^{k-1}_{\boldsymbol{J}}(T_{\boldsymbol{I}}), \; (\boldsymbol{J}', \boldsymbol{J}'')\in \mathcal{E}(T_{\boldsymbol{I}})\right\}.
$$


The set of leaves of $T_{\boldsymbol{I}}$ is defined as
$$
\mathcal{L}(T_{\boldsymbol{I}}):=\left\{ \boldsymbol{J}\in \mathcal{V}(T_{\boldsymbol{I}}), \mathcal{S}_{\boldsymbol{J}}(T_{\boldsymbol{I}}) = \emptyset \right\}.
$$
The set of parents of leaves of a tree $T_{\boldsymbol{I}}$ is defined as the set $\mathcal{L}^p(T_{\boldsymbol{I}}) \subset \mathcal{V}(T_{\boldsymbol{I}})$ of vertices of $T_{\boldsymbol{I}}$ which 
have at least one son which is a leaf of $T_{\boldsymbol{I}}$, i.e. 
$$
\mathcal{L}^p(T_{\boldsymbol{I}}):= \left\{ \boldsymbol{J} \in \mathcal{V}(T_{\boldsymbol{I}}), \; \mathcal{S}_{\boldsymbol{J}}(T_{\boldsymbol{I}}) \cap \mathcal{L}(T_{\boldsymbol{I}}) \neq \emptyset\right\}. 
$$
For any $\boldsymbol{J}\in \mathcal{V}(T_{\boldsymbol{I}})$, the set of descendants of $\boldsymbol{J}$ in $T_{\boldsymbol{I}}$ is defined as
$$
\mathcal{D}_{\boldsymbol{J}}(T_{\boldsymbol{I}}):= \left\{ \boldsymbol{J}'\in \mathcal{V}(T_{\boldsymbol{I}}), \; \exists k \in \mathbb{N}^*, \; \boldsymbol{J}' \in \mathcal{S}^k_{\boldsymbol{J}}(T_{\boldsymbol{I}})\right\}.
$$

\begin{mydef}
A tree $T_{\boldsymbol{I}}$ is called a partition tree is called a partition tree for the set $\boldsymbol{I}$ if the following conditions hold:
 \begin{itemize}
  \item $\boldsymbol{I}$ is the root of $T_{\boldsymbol{I}}$; 
  \item For all $\boldsymbol{J}\in \mathcal{V}(T_{\boldsymbol{I}}) \setminus \mathcal{L}(T_{\boldsymbol{I}})$, $\mathcal{S}_{\boldsymbol{J}}(T_{\boldsymbol{I}})$ is an admissible partition of 
  $\boldsymbol{J}$ and $|\mathcal{S}_{\boldsymbol{J}}(T_{\boldsymbol{I}})|\geq 2$; 
  \item For all $\boldsymbol{J}\in \mathcal{V}(T_{\boldsymbol{I}})$, $\boldsymbol{J} \neq \emptyset$.
 \end{itemize}
\end{mydef}


We also introduce here a few definitions, in particular the subtree or the merged tree of a partition tree $T_{\boldsymbol I}$ associated to a vertex $\boldsymbol{J}\in \mathcal{V}(T_{\boldsymbol{I}})$. 

\begin{mydef}
 Let $T_{\boldsymbol{I}}$ be a partition tree  for $\boldsymbol{I}$ with vertices (or nodes) $\mathcal{V}(T_{\boldsymbol{I}})$ and edges $\mathcal{E}(T_{\boldsymbol{I}})$. Let $\boldsymbol{J}\in \mathcal V(T_{\boldsymbol{I}}) 
 \setminus \mathcal{L}(T_{\boldsymbol{I}})$. The merged tree of $T_{\boldsymbol{I}}$ associated to the vertex $\boldsymbol{J}$ is the tree denoted by $T^m_{\boldsymbol{I}}(\boldsymbol{J})$ with root $\boldsymbol{I}$, vertices 
  $\mathcal{V}(T^m_{\boldsymbol{I}}(\boldsymbol{J})):= \mathcal{V}(T_{\boldsymbol{I}}) \setminus \mathcal{D}_{\boldsymbol{J}}(T_{\boldsymbol{I}})$ and edges 
  \begin{align*}
  \mathcal{E}\left(T^m_{\boldsymbol{I}}(\boldsymbol{J})\right) & := \mathcal{E}(T_{\boldsymbol{I}}) \cap \left( \mathcal{V}(T^m_{\boldsymbol{I}}(\boldsymbol{J})) \times \mathcal{V}(T^m_{\boldsymbol{I}}(\boldsymbol{J}))\right)\\
  & = \mathcal{E}(T_{\boldsymbol{I}}) 
  \setminus \left(\mathcal V(T_{\boldsymbol{I}}) \times \mathcal{D}_{\boldsymbol{J}}(T_{\boldsymbol{I}}) \cup \mathcal{D}_{\boldsymbol{J}}(T_{\boldsymbol{I}}) \times \mathcal{V}(T_{\boldsymbol{I}})\right).\\
  \end{align*}
%   \item The subtree of $T_{\boldsymbol{I}}$ associated to the vertex $\boldsymbol{J}$ is the tree denoted by 
%   $T^s_{\boldsymbol{J}}(T_{\boldsymbol{I}}))$ with root $\boldsymbol{J}$, vertices $\mathcal{V}\left(T^s_{\boldsymbol{J}}(T_{\boldsymbol{I}}))\right) \mathcal{D}_{\boldsymbol{J}}(T_{\boldsymbol{I}}))$ and edges 
%   $$
%   \mathcal{E}\left(\boldsymbol{J}\right):= \mathcal{E} \cap \left( \mathcal{D}(\boldsymbol{J}) \times \mathcal{D}(\boldsymbol{J})\right).
%   $$

\end{mydef}





We collect in the following lemma a few useful results that can be easily proved by a recursive argument.

\begin{mylemma}\label{lem:aux}
Let $T_{\boldsymbol{I}}$ be a partition tree for $\boldsymbol{I}$. Let $\boldsymbol{J}\in \mathcal V(T_{\boldsymbol{I}})  \setminus \mathcal{L}(T_{\boldsymbol{I}})$.
 \begin{itemize}
  \item[(i)] For all $\boldsymbol{J}'\in \mathcal{V}(T_{\boldsymbol{I}})$, $\boldsymbol{J}'\subset \boldsymbol{I}$.
  \item[(ii)] The set of leaves $\mathcal{L}(T_{\boldsymbol{I}})$ is an admissible partition of the set $\boldsymbol{I}$.
  \item [(iii)] The set $\mathcal{D}_{\boldsymbol{J}}(T_{\boldsymbol{I}})\cap \mathcal{L}(T_{\boldsymbol{I}})$ forms an admissible partition of the set $\boldsymbol{J}$.
    \item[(iv)] The merged tree $T^m_{\boldsymbol{I}}(\boldsymbol{J})$ is a partition tree for $\boldsymbol{I}$.
       % \item[(iv)] The subtree $T^s_{\boldsymbol{I}}(\boldsymbol{J})$ is a partition tree for $\boldsymbol{J}$.
        \item [(v)] The set of leaves of $T^m_{\boldsymbol{I}}(\boldsymbol{J})$ is 
        $\mathcal{L}\left( T^m_{\boldsymbol{I}}(\boldsymbol{J})\right) = \{ \boldsymbol{J} \} \cup \left( \mathcal{L}(T_{\boldsymbol{I}}) \setminus  \mathcal{D}_{\boldsymbol{J}}(T_{\boldsymbol{I}}) \right)$.
        %\item[(vi)] The set of leaves of $T^s_{\boldsymbol{I}}(\boldsymbol{J})$ is $\mathcal{L}(T^s_{\boldsymbol{I}}(\boldsymbol{J})) = \mathcal{L}(T_{\boldsymbol{I}}) \cap \mathcal{D}(\boldsymbol{J})$.
 \end{itemize}
\end{mylemma}


\subsubsection{Example: dyadic partition tree}

We give here a particular example of partition tree in the particular case when $\boldsymbol{I} = I_1 \times \cdots \times I_d$ with 
$I_1 = \cdots = I_d =: I$ with $I = \left\{ \right\}$.


The dyadic partition tree of level $\ell$ associated to $\boldsymbol{I}$ is defined as follows:


*****  TO DO ****


\subsection{MERGE procedure}


\begin{algorithm}[H]
\caption{MERGE}\label{algo:3}
\begin{algorithmic}[1]
%\Procedure{}{}

\BState \textbf{Input:}

\State $\mathcal{A}\in \R^{\boldsymbol{I}} \gets \text{a tensor of order } d$
\State $T_{\boldsymbol{I}}$ an initial partition tree of $\boldsymbol{I}$
\State A set of leaf errors $(\epsilon^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left(T_{\boldsymbol{I}}\right)}$
\State A set of leaf ranks $(\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}(T_{\boldsymbol{I}})} \subset \N^d$
\State $\boldsymbol{J}_0\in \mathcal{V}(T_{\boldsymbol{I}}) \setminus \mathcal{L}(T_{\boldsymbol{I}})$.


\medskip
\BState \textbf{Output:}
\State $T^{\rm fin}_{\boldsymbol{I}}$ a final partition tree of $\boldsymbol{I}$
\State $merge$ a boolean indicating if the tree has been merged or not 
\State A set of leaf errors $(\epsilon^{\rm fin, \boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}} \right)}$
\State A set of leaf ranks $\left(\boldsymbol{R}^{\rm fin, \boldsymbol{J}}\right)_{\boldsymbol{J} \in \mathcal{L}\left( T^{\rm fin}_{\boldsymbol{I}} \right)} \subset \N^d$


\medskip
\BState \textbf{Begin:}
%\BState \emph{begin}:
\State Set $\mathcal{P}_{\boldsymbol{J}_0}:= \mathcal{D}_{\boldsymbol{J_0}}(T_{\boldsymbol{I}}) \cap \mathcal{L}(T_{\boldsymbol{I}})$. From Lemma~\ref{lem:aux} (iii), $ \mathcal{P}_{\boldsymbol{J}_0}$ is an admissible partition of the set $\boldsymbol{J}_0$.

\State Set $M_{\rm nomerge}:= \sum_{\boldsymbol{J} \in \mathcal{P}_{\boldsymbol{J}_0}} M_{\rm TF}\left( \boldsymbol{J}, \boldsymbol{R}^{\boldsymbol{J}}\right)$

\State Set $\eta:= \sqrt{\sum_{\boldsymbol{J} \in \mathcal{P}_{\boldsymbol{J}_0}} |\epsilon^{\boldsymbol{J}}|^2}$. 

\State Compute $\boldsymbol{R}$ = GREEDY-HOSVD($\cA^{\boldsymbol{J}_0}$, $\eta$) 

\State Compute $M_{\rm merge}:=  M_{\rm TF}\left( \boldsymbol{J}_0, \boldsymbol{R}\right)$.

\If{$M_{\rm merge} < M_{\rm nomerge}$}

\State Set ${merge} = {\rm true}$, $T^{\rm fin}_{\boldsymbol{I}} = T_{\boldsymbol{I}}^m(\boldsymbol{J}_0)$.

\For{$\boldsymbol{J} \in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}}\right)$}

\If{$\boldsymbol{J} = \boldsymbol{J}_0$}

\State Set $\boldsymbol{R}^{\rm fin, \boldsymbol{J}_0}:= \boldsymbol{R}$ and $\epsilon^{\rm fin, \boldsymbol{J}_0}:= \eta$.

\Else 

\State From Lemma~\ref{lem:aux} (v), necessarily $\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})$. 

\State Set $\boldsymbol{R}^{\rm fin, \boldsymbol{J}}:= \boldsymbol{R}^{\boldsymbol{J}}$ and $\epsilon^{\rm fin, \boldsymbol{J}}:= \epsilon^{\boldsymbol{J}}$.

\EndIf

\EndFor

\Else 

\State Set ${merge} = {\rm false}$, $T^{\rm fin}_{\boldsymbol{I}} = T_{\boldsymbol{I}}$.

\For{$\boldsymbol{J} \in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}}\right) = \mathcal{L}(T_{\boldsymbol{I}})$}

\State Set $\boldsymbol{R}^{\rm fin, \boldsymbol{J}}:= \boldsymbol{R}^{\boldsymbol{J}}$ and $\epsilon^{\rm fin, \boldsymbol{J}}:= \epsilon^{\boldsymbol{J}}$.

\EndFor

\EndIf

\Return  $T^{\rm fin}_{\boldsymbol{I}}$, ${merge}$, $(\epsilon^{\rm fin, \boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}} \right)}$, $\left(\boldsymbol{R}^{\rm fin, \boldsymbol{J}}\right)_{\boldsymbol{J}\in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}} \right)}$
%\EndProcedure
\end{algorithmic}
\end{algorithm}


\subsection{HPF-MERGE procedure}

\begin{algorithm}[H]
\caption{HPF-MERGE}\label{algo:4}
\begin{algorithmic}[1]
%\Procedure{}{}

\BState \textbf{Input:}

\State $\mathcal{A}\in \R^{\boldsymbol{I}} \gets \text{a tensor of order } d$
\State $T^{\rm init}_{\boldsymbol{I}}$ an initial partition tree of $\boldsymbol{I}$
\State $\epsilon>0 \gets \text{error tolerance criterion}$

\medskip
\BState \textbf{Output:}
\State $T_{\boldsymbol{I}}$ a final partition tree of $\boldsymbol{I}$
\State A set of leaf errors $(\epsilon^{ \boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left(T_{\boldsymbol{I}} \right)}$
\State A set of leaf ranks $\left(\boldsymbol{R}^{ \boldsymbol{J}}\right)_{\boldsymbol{J} \in \mathcal{L}\left( T_{\boldsymbol{I}} \right)} \subset \N^d$

\medskip
\BState \textbf{Begin:}
%\BState \emph{begin}:

\State Set $T_{\boldsymbol{I}} = T^{\rm init}_{\boldsymbol{I}}$.


\State Compute ($(\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left( T_{\boldsymbol{I}}\right)}$, $(\overline{\epsilon}^{\boldsymbol{J}})_{\boldsymbol{J}\in\mathcal{L}\left( T_{\boldsymbol{I}}\right)}$ )
= HPF-Greedy-HOSVD($\cA$, $\mathcal{L}\left( T_{\boldsymbol{I}}\right)$ , $\epsilon$) 

\State Compute $\eta^2:= \epsilon^2 - \sum_{\boldsymbol{J} \in\mathcal{L}\left( T_{\boldsymbol{I}}\right)} |\overline{\epsilon}^{\boldsymbol{J}}|^2$.

\State For all $\boldsymbol{J}\in \mathcal{L}\left( T_{\boldsymbol{I}}\right)$, define $\epsilon^{\boldsymbol{J}}:= \sqrt{|\overline{\epsilon}^{\boldsymbol{J}}|^2 + \frac{|\boldsymbol{J}|}{|\boldsymbol{I}|}\eta^2}$.

\State Set $\mathcal{N}_{\rm totest} = \mathcal{L}^p(T_{\boldsymbol{I}})$ and $\mathcal{N}_{\rm nomerge} = \emptyset$.

\While{$ \mathcal{N}_{\rm totest} \neq \emptyset$}

\State Choose $\boldsymbol{J}_0 \in \mathcal{N}_{\rm totest}$. 

\State ($T^{\rm fin}_{\boldsymbol{I}}$,${merge}$,$(\epsilon^{\rm fin, \boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}} \right)}$,$\left(\boldsymbol{R}^{\rm fin, \boldsymbol{J}}\right)_{\boldsymbol{J} \in \mathcal{L}\left( T^{\rm fin}_{\boldsymbol{I}} \right)}$) 
= MERGE($\mathcal{A}$,$T_{\boldsymbol{I}}$,$(\epsilon^{\boldsymbol{J}})_{\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})}$,$(\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})}$,  $\boldsymbol{J}_0$)

\If{${merge} = {\rm true}$}

\State $T_{\boldsymbol{I}} = T_{\boldsymbol{I}}^{\rm fin}$
\State $\mathcal{N}_{\rm totest} = \mathcal{L}^p(T_{\boldsymbol{I}}^{\rm new}) \setminus  \mathcal{N}_{\rm nomerge}$.
\State $(\epsilon^{\boldsymbol{J}})_{\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})} = (\epsilon^{\rm fin, \boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}} \right)}$, $(\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})} = \left(\boldsymbol{R}^{\rm fin, \boldsymbol{J}}\right)_{\boldsymbol{J} \in \mathcal{L}\left( T^{\rm fin}_{\boldsymbol{I}} \right)} $

\Else 
\State $\mathcal{N}_{\rm nomerge} = \mathcal{N}_{\rm nomerge} \cup \{ \boldsymbol{J}_0\}$;  $\mathcal{N}_{\rm totest} = \mathcal{N}_{\rm totest}  \setminus \{ \boldsymbol{J}_0\}$. 
\EndIf


\EndWhile


\Return $T_{\boldsymbol{I}}$, $(\epsilon^{\boldsymbol{J}})_{\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})}$, $(\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})}$
%\EndProcedure
\end{algorithmic}
\end{algorithm}




