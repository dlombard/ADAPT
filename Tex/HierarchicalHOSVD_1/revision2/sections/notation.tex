\section{Partitioning for tensors: elements of theoretical analysis}\label{sec:notation}


The aim of this section is to motivate the interest of partitioning a tensor into subtensors with a view 
to represent them in an even sparser way with low-rank approximations. 

To illustrate our point, we first consider in this section \itshape continuous \normalfont tensors. 
Let $d\in \N^*$, $1\leq q \leq +\infty$ and $m_1,\cdots,m_d\in \N^*$ and let $\Omega_1,\cdots,\Omega_d$ be open subsets of $\R^{m_1}, \cdots, \R^{m_d}$ respectively. We denote
$\boldsymbol{\Omega} := \Omega_1\times \cdots\times \Omega_d$.


A tensor $\cF$ of order $d$ defined on $\boldsymbol{\Omega}$ is a function $\cF \in L^q(\boldsymbol{\Omega})$. The tensor $\cF$ is said to be of canonical format with rank $R\in \mathbb{N}$ if
$$
\cF(x_1,\cdots,x_d) = \sum_{r=1}^R F_1^r(x_1) \cdots F_d^r(x_d), \quad \mbox{for}(x_1,\cdots,x_d)\in \boldsymbol{\Omega},
$$
where for all $1\leq j \leq d$ and $1\leq r \leq R$, $F_j^r \in L^q(\Omega_j)$. 


A domain partition $\{\boldsymbol{\Omega}^k\}_{1\leq k \leq K}$ of $\boldsymbol{\Omega}$ is said to be \itshape admissible \normalfont if it satisfies the following properties:
\begin{itemize}
\item for all $1\leq k \leq K$, there exists $\Omega_1^k \subset \Omega_1,\cdots,\Omega_d^k \subset \Omega_d$ open subsets such that $\boldsymbol{\Omega}^k:= \Omega_1^k \times \cdots \times \Omega_d^k$;
\item for all $1\leq k\neq k \leq K$, $\boldsymbol{\Omega}^k \cap \boldsymbol{\Omega}^l = \emptyset$;
\item $\overline{\boldsymbol{\Omega}} = \bigcup_{k=1}^K \overline{\boldsymbol{\Omega}^k}$.
\end{itemize}

A particular case of admissible domain partition of $\boldsymbol{\Omega}$ can be for instance constructed as follows: for all $1\leq j \leq d$, let $K_j \in \N^*$ and let us consider a
collection of subsets $P_j:= \left( \Omega_j^{k_j}\right)_{1\leq k_j \leq K_j}$ 
of $\Omega_j$ such that 
\begin{itemize}
\item $\overline{\Omega_j}:= \bigcup_{1\leq k_j \leq K_j} \overline{\Omega_j^{k_j}}$;
\item $\Omega_j^{k_j} \cap \Omega_j^{l_j} = \emptyset$ for all $1\leq k_j\neq l_j \leq K_j$.
\end{itemize}
Let us denote
$$
\boldsymbol{P}:= \left( \bigtimes_{j=1}^d \Omega_j^{k_j}\right)_{ 1\leq k_1 \leq K_1, \cdots, 1\leq k_d \leq K_d}.
$$
The partition $\boldsymbol{P}$ then defines an admissible domain partition of $\boldsymbol{\Omega}$, and will be called hereafter 
the \itshape tensorized domain partition \normalfont associated to the collection of domain partitions $(P_j)_{1\leq j \leq d}$.


The heuristic of the approach proposed in this article is the following: 
consider a tensor $\cF \in L^q(\boldsymbol{\Omega})$ which is a sufficiently regular function of $(x_1,\cdots,x_d) \in \boldsymbol{\Omega}$. It is of course not true, in general, 
that this function can be represented in a parsimonious way in a given tensor format (by exploiting separation of variable). However, under appropriate assumptions, 
it can be proved that there exists an admissible domain partition $\{\boldsymbol{\Omega}^k\}_{1\leq k \leq K}$ of $\boldsymbol{\Omega}$ such that all the restrictions 
$\cF^k:= \cF|_{\boldsymbol{\Omega}^k}$ can be represented in some tensor formats with low ranks. 

The following result aims at making the above heuristics on the tensor approximation of functions precise, by providing a sufficient condition on which the above statement is true. 

We introduce the following definition.
\begin{mydef}
A tensor $\cF$ defined on $\boldsymbol{\Omega}$ is said to be of Canonical Partitioning Format (CPF) if there exists an admissible partition $\{ \boldsymbol{\Omega}^k\}_{1\leq k \leq K}$ of $\boldsymbol{\Omega}$ such that 
 for all $1\leq k \leq K$, the subtensor $\cF^k$ of $\cF$ 
 associated to $\boldsymbol{\Omega}^k$ is of canonical format on $\boldsymbol{\Omega}^k$. The tensor $\cF$ is said to be of Canonical Partitioning Format (CPF) with rank $R\in \N^*$ 
 if there exists an admissible domain partition $\boldsymbol{P}:= \{ \boldsymbol{\Omega}^k\}_{1\leq k \leq K}$ of $\boldsymbol{\Omega}$ such that for all $1\leq k \leq K$, the subtensor $\cF^k$ of $\cF$ 
 associated to $\boldsymbol{\Omega}^k$ is of canonical format on $\boldsymbol{\Omega}^k$ with rank $R$.
\end{mydef}

For all $d$ multi-index $\alpha\in\mathbb{N}^d$, we denote $|\alpha| := \sum_{i=1}^d \alpha_i$,  $x^{\alpha}=:x_1^{\alpha_1}\cdot \ldots \cdot x_d^{\alpha_d}$ where $x=(x_1,\cdots,x_d)\in \boldsymbol{\Omega}$, 
and $\alpha! := \prod_{i=1}^d \alpha_i$. The weak derivative of order $\alpha$ is denoted by $D^{(\alpha)}$. 

%An admissible domain decomposition is introduced, in which the domain is decomposed into $2^{dN}$ sub-domains $\boldsymbol{\Omega}^{i}$.

The rationale behind the proposition proposed hereafter is the following: we show that there exists a sufficient condition on the function regularity such that, 
if the error is measured in a given norm, there exists an admissible domain partition such that a finite rank tensor approximation in each subdomain achieves a prescribed accuracy on the whole tensor. 


\begin{myprop}\label{prop:Damiano}
 Let $\Omega_1 = \cdots  = \Omega_d = (0,1)$ so that $\boldsymbol{\Omega}:= (0,1)^d$. For all $M\in \mathbb{N}^*$, let us consider $P^M:=\{\left(\frac{m-1}{M}, \frac{m}{M}\right)\}_{1\leq m \leq M}$ be a collection of subsets of $(0,1)$ and let 
$\boldsymbol{P}^M$ be the tensorized domain partition of $\boldsymbol{\Omega}$ associated to the collection of domain partitions $(P_j)_{1\leq j \leq d}$ where $P_j = P^M$ for all $1\leq j \leq d$.   
Let $k\in \mathbb{N}^*$, $1\leq p \leq q \leq \infty$ and $\varepsilon>0$. We denote $\lambda := \frac{k}{d} - \frac{1}{p} + \frac{1}{q}>0$.  Let $\mathcal{F}\in W^{k,p}(\boldsymbol{\Omega})$ such that $\| \mathcal{F} \|_{W^{k,p}(\boldsymbol{\Omega})}\leq 1$. 
 Then, there exists a constant $C>0$ which depends only on $k,p,d,q$ such that for all $M\in \mathbb{N}^*$ such that $\ln M \geq -\frac{1}{d\lambda}\ln \left( \frac{\varepsilon}{C}\right)$, 
 there exists a tensor $\cF^{CPF}$ of Canonical Partitioning Format with domain partition $\boldsymbol{P}^M$ and rank $R\leq \frac{(k-1+d)!}{(k-1)! d!}$ such that
\begin{equation}\label{eq:esteps}
\| \mathcal{F} - \cF^{CPF}\|_{L^q(\boldsymbol{\Omega})} \leq \varepsilon.
\end{equation}

\begin{remark}
 Let us make a simple remark before giving the proof of Proposition~\ref{prop:Damiano}. In the case where the tensor $\cF$ belongs to $H^1(\boldsymbol{\Omega})$. Then, using the same notation as in Proposition~\ref{prop:Damiano}, $k=1$ and $p=2$. 
 Besides, choosing $q=2$, since $ \frac{(k-1+d)!}{(k-1)! d!} = 1$, there exists an admissible partition of $\boldsymbol{\Omega}$ into $\# \boldsymbol{P}^M = M^{d} = \mathcal{O}\left( \varepsilon^{-\frac{1}{\lambda}} \right)$ subdomains such 
 that $\cF$ is approximated in the $L^2(\boldsymbol{\Omega})$ with precision $\varepsilon$ by a tensor in Canonical Partitioning Format with domain partition $\boldsymbol{P}^M$ with rank $1$. 
\end{remark}

\medskip
\normalfont

The proof is mainly based on arguments introduced in~\cite{edmunds1990approximation}.  

\begin{proof}
First, for all $\boldsymbol{m}:=(m_1, \cdots, m_d)\in\{1,\cdots, M\}^d$, let us denote 
$$
\boldsymbol{\Omega}^{\boldsymbol{m}}:= \bigtimes_{j=1}^d \left( \frac{m_j-1}{M}, \frac{m_j}{M}\right).
$$
For all $\boldsymbol{m}\in\{1,\cdots, M\}^d$,for all tensor $\mathcal{G}\in W^{k,p}\left(\boldsymbol{\Omega}^{\boldsymbol{m}}\right)$, we introduce a polynomial approximation of $\mathcal{G}$ on the domain $\boldsymbol{\Omega}^{\boldsymbol{m}}$, 
based on the Taylor kernel, which is defined as follows:
\begin{equation}
\Pi^{\boldsymbol{m}} \mathcal{G} = \sum_{|\alpha|<k}\int_{\boldsymbol{\Omega}^{\boldsymbol{m}}}\frac{(x-y)^{\alpha}}{\alpha!} D^{(\alpha)} \mathcal{G}(y) \ dy.
\end{equation}
The projector $\Pi^{\boldsymbol{m}}\mathcal{G}$ defines a polynomial approximation of $\mathcal{G}$ on the subdomain $\boldsymbol{\Omega}^{\boldsymbol{m}}$ of polynomial degree $k-1$. 

Denoting by $\cF^{\boldsymbol{m}}:= \cF|_{\boldsymbol{\Omega}^{\boldsymbol{m}}}$, we then have 
\begin{equation}
\left\| \mathcal{F}^{\boldsymbol{m}} - \Pi^{\boldsymbol{m}} \mathcal{F}^{\boldsymbol{m}}  \right\|_{L^q(\boldsymbol{\Omega}^{\boldsymbol{m}})} \leq C \left|\boldsymbol{\Omega}^{\boldsymbol{m}}\right|^{\lambda} 
\left\| \mathcal{F}^{\boldsymbol{m}}\right\|_{W^{k,p}(\boldsymbol{\Omega}^{\boldsymbol{m}})},
\end{equation}
where $C>0$ is a constant which only depends on $k$, $d$, $p$ and $q$ (see \cite{edmunds2018spectral}[Lemma V.6.1,p.289] or \cite{edmunds1990approximation}[Lemma 1]). 
Let us denote $\cF^{CPF}\in L^q(\boldsymbol{\Omega})$ defined by $\cF^{CPF}|_{\boldsymbol{\Omega}^{\boldsymbol{m}}} = \Pi^{\boldsymbol{m}}\cF^{\boldsymbol{m}}$. Then, we have:
\begin{equation}
\left\|\mathcal{F}-\mathcal{F}^{CPF}\right\|_{L^q(\boldsymbol{\Omega})}^q = \sum_{\boldsymbol{m}\in \{1,\cdots,M\}^d} \| \mathcal{F}^{\boldsymbol{m}} - \Pi^{\boldsymbol{m}}\mathcal{F}^{\boldsymbol{m}} \|_{L^q(\boldsymbol{\Omega}^{\boldsymbol{m}})}^q 
\leq \sum_{\boldsymbol{m}\in \{1,\cdots,M\}^d} C^q \left|\boldsymbol{\Omega}^{\boldsymbol{m}}\right|^{q \lambda} \left\| \mathcal{F}^{\boldsymbol{m}} \right\|_{W^{k,p}(\boldsymbol{\Omega}^{\boldsymbol{m}})}^q.
\end{equation}
Since $\left| \boldsymbol{\Omega}^{\boldsymbol{m}}\right|  = \frac{1}{M^d}$ for all $\boldsymbol{m}\in \{1,\cdots,M\}^d$, we obtain
\begin{equation}
\left\|\mathcal{F}-\mathcal{F}^{CPF}\right\|_{L^q(\boldsymbol{\Omega})}^q \leq C^q \left(\frac{1}{M^d}\right)^{q\lambda} \sum_{\boldsymbol{m}\in \{1,\cdots,M\}^d} \| \mathcal{F} \|_{W^{k,p}(\boldsymbol{\Omega}^{\boldsymbol{m}})}^q.
\end{equation}
In order to bound the last term, let us consider the following inequality:
\begin{equation}
\sum_{\boldsymbol{m}\in \{1,\cdots,M\}^d}\left\| \mathcal{F}^{\boldsymbol{m}} \right\|_{W^{k,p}(\boldsymbol{\Omega}^{\boldsymbol{m}})}^q = \sum_{\boldsymbol{m}\in \{1,\cdots,M\}^d} \left(\left\| \mathcal{F}^i \right\|_{W^{k,p}(\boldsymbol{\Omega}^{\boldsymbol{m}})}^p\right)^{\frac{q}{p}} \leq \left( \sum_{\boldsymbol{m}\in \{1,\cdots,M\}^d} \| \mathcal{F}^{\boldsymbol{m}} \|_{W^{k,p}(\boldsymbol{\Omega}^{\boldsymbol{m}})}^p \right)^{\frac{q}{p}}\leq 1,
\end{equation}
which holds true because $\left\| \mathcal{F}^{\boldsymbol{m}} \right\|_{W^{k,p}(\boldsymbol{\Omega}^{\boldsymbol{m}})}\leq 1$ and $q\geq p$. Putting the estimates together, we obtain
\begin{equation}
\left\|\mathcal{F}- \mathcal{F}^{CPF}\right\|_{L^q(\boldsymbol{\Omega})} \leq C \left( \frac{1}{M^d} \right)^{\lambda}.
\end{equation}
Since $\lambda>0$, as soon as $M$ is large enough so that $C \left( \frac{1}{M^d} \right)^{\lambda} \leq \varepsilon$, we obtain (\ref{eq:esteps}). 
To conclude the proof, let us observe that a multi-variate polynomial with degree lower than $k-1$ can be written as a tensor of order at most $R = \frac{(k-1+d)!}{(k-1)! d!}$. Hence the result. 
\end{proof}
\end{myprop}
Some remarks are in order. Proposition~\ref{prop:Damiano} does not investigate the tensor approximation \textit{per se}, but it shows a sufficient regularity condition under which an admissible domain partition of a function is such that a given piece-wise finite rank tensor approximation (with a rank that could depend upon the format and be smaller than the polynomial rank) can achieve a prescribed accuracy. The sufficient condition is essentially related to the compactness of the embedding $W^{k,p}\hookrightarrow L^q$ (Rellich-Kondrakov theorem). 
The fact that the admissible domain partition is performed by dividing the domain into $2^{dN}$ subdomains is a reminder that the method, when practically implemented, is suitable to approximate tensors of moderate dimension.  


\medskip

In the light of Proposition~\ref{prop:Damiano}, for a given tensor $\cF \in L^q(\boldsymbol{\Omega})$, the two following issues naturally arise: 
\begin{itemize}
 \item on the one hand, given a particular admissible domain partition $\{\boldsymbol{\Omega}^k\}_{1\leq k \leq K}$, one would like to look for an algorithm which can construct 
 effective low-rank approximations in a given format for all subtensors $\cF^k$ so that 
 (i) the global error between the tensor $\cF$ and the obtained approximation of the tensor is guaranteeed to be lower than an a priori chosen error criterion; 
 (ii) the total memory storage of all the low-rank partitions of the tensor on each subdomain is minimal. This requires a careful strategy to distribute the error over all the different subsets $\boldsymbol{\Omega}^k$.
 The procedure we propose is described in details in Section~\ref{sec:GHOSVD};
 \item on the other hand, one would like to develop a numerical method to find an optimal of quasi-optimal admissible domain partition $\{\boldsymbol{\Omega}^k\}_{1\leq k \leq K}$, so that the total memory storage of 
 the low-rank tensor approximations of each subtensor $\cF^k$ to be minimal, provided that a global error criterion is satified for the whole tensor $\cF$. The procedure we propose is described in details in Section~\ref{sec:HMerge}.
\end{itemize}

The aim of the two following sections is to propose algorithms in order to address these issues from a numerical point of view. 
We make the choice to present the algorithms from now on using discrete tensors, but we stress on the fact that they can be easily generalized to deal with 
the approximation of continuous tensors in the case when $q=2$.

% 
% 
% \subsection{Continuous tensors and subtensors}
% 
% Let us now switch to the case of \itshape continuous \normalfont tensors. 
% \medskip
% 
% A tensor $\cF$ of order $d$ defined on $\boldsymbol{\Omega}$ is a function $\cF \in L^q(\boldsymbol{\Omega})$. 
% 
% 
% The tensor $\cF$ is said to be of canonical format with rank $R\in \mathbb{N}$ if
% $$
% \cF(x_1,\cdots,x_d) = \sum_{r=1}^R F_1^r(x_1) \cdots F_d^r(x_d), \quad \mbox{for a.a. }(x_1,\cdots,x_d)\in \boldsymbol{\Omega},
% $$
% where for all $1\leq j \leq d$ and $1\leq r \leq R$, $F_j^r \in L^q(\Omega_j)$. 
% 
% The tensor $\cF$ is said to be of Tucker Format with rank $\boldsymbol{R} = (R_1,\cdots,R_d)\in (\N)^d$ if 
% $$
% \cF(x_1,\cdots, x_d)  = \sum_{r_1=1}^{R_1} \cdots \sum_{r_d = 1}^{R_d} F_1^{r_1}(x_1) \cdots  F_d^{r_d}(x_d), \quad \mbox{for a.a. }(x_1,\cdots,x_d)\in \boldsymbol{\Omega},
% $$
% where for all $1\leq j \leq d$ and all $1\leq r_j\leq R_j$, $F_j^{r_j} \in L^q(\Omega_j)$.
% 
% Similarly to the discrete tensor case, one can define continuous tensors of Tensor Train or Hierarchical Tensor Tree format. 
% 
% \medskip
% 
% Let now $\widetilde{\Omega}_1\subset \Omega_1$, ..., $\widetilde{\Omega}_d\subset \Omega_d$ be non-empty open subsets and $\widetilde{\boldsymbol{\Omega}}:= \widetilde{\Omega}_1 \times \cdots \times \widetilde{\Omega}_d$. 
% The subtensor of $\cF$ associated to 
% $\widetilde{\boldsymbol{\Omega}}$ is the tensor $\widetilde{\cF} = \cF|_{\widetilde{\boldsymbol{\Omega}}} \in L^q(\widetilde{\boldsymbol{\Omega}})$.
% 
% 
% Let us now consider a collection of subsets $\boldsymbol{P}:= \{\boldsymbol{\Omega}^k\}_{1\leq k \leq K}$ of $\boldsymbol{\Omega}$ such that 
% 
% Such a collection of subsets will be called hereafter an \itshape admissible \normalfont domain decomposition of $\boldsymbol{\Omega}$. 
% Then, the collection of subtensors of $\cF$ associated to the collection $\{\boldsymbol{\Omega}^k\}_{1\leq k \leq K}$ is defined as the set of subtensors $\cF^k$ defined as $\cF^k:= \cF|_{\boldsymbol{\Omega}^k}$. 
% 
% A particular case of admissible domain decomposition of $\boldsymbol{\Omega}$ can be for instance constructed as follows: for all $1\leq j \leq d$, let $K_j \in \N^*$ and let us consider a
% collection of subsets $P_j:= \left( \Omega_j^{k_j}\right)_{1\leq k_j \leq K_j}$ 
% of $\Omega_j$ such that 
% \begin{itemize}
%  \item $\overline{\Omega_j}:= \bigcup_{1\leq k_j \leq K_j} \overline{\Omega_j^{k_j}}$;
%  \item $\Omega_j^{k_j} \cap \Omega_j^{l_j} = \emptyset$ for all $1\leq k_j\neq l_j \leq K_j$.
% \end{itemize}
% Let us denote
% $$
% \boldsymbol{P}:= \left( \bigtimes_{j=1}^d \Omega_j^{k_j}\right)_{ 1\leq k_1 \leq K_1, \cdots, 1\leq k_d \leq K_d}.
% $$
% The partition $\boldsymbol{P}$ then defines an admissible domain decomposition of $\boldsymbol{\Omega}$, and will be called hereafter the \itshape tensorized domain decomposition \normalfont associated to the collection of domain decompositions
% $(P_j)_{1\leq j \leq d}$.
% 
% \medskip
% 
% We introduce the following definitions:
% 
% \begin{mydef}
% $\;$
% 
% \begin{itemize}
%  \item A tensor $\cF$ defined on $\boldsymbol{\Omega}$ is said to be of Canonical Partitioning Format (CPF) if there exists an admissible domain decomposition $\boldsymbol{P}:= \{ \boldsymbol{\Omega}^k\}_{1\leq k \leq K}$ of $\boldsymbol{\Omega}$ such that 
%  for all $1\leq k \leq K$, the subtensor $\cF^k$ of $\cF$ 
%  associated to $\boldsymbol{\Omega}^k$ is of canonical format on $\boldsymbol{\Omega}^k$. The tensor $\cF$ is said to be of Canonical Partitioning Format (CPF) with rank $R\in \N^*$ 
%  if there exists an admissible domain decomposition $\boldsymbol{P}:= \{ \boldsymbol{\Omega}^k\}_{1\leq k \leq K}$ of $\boldsymbol{\Omega}$ such that for all $1\leq k \leq K$, the subtensor $\cF^k$ of $\cF$ 
%  associated to $\boldsymbol{\Omega}^k$ is of canonical format on $\boldsymbol{\Omega}^k$ with rank $R$.
%   \item A tensor $\cF$ defined on $\boldsymbol{\Omega}$ is said to be of Tucker Partitioning Format (TPF) if there exists an admissible partition $\boldsymbol{P}:= \{\boldsymbol{\Omega}^k\}_{1\leq k \leq K}$ of $\boldsymbol{\Omega}$ such that 
%   for all $1\leq k \leq K$, the subtensor $\cF^k$ of $\cF$ 
% associated to $\boldsymbol{\Omega}^k$ is of Tucker format on $\boldsymbol{\Omega}^k$. A tensor $\cF$ defined on $\boldsymbol{\Omega}$ is said to be of Tucker Partitioning Format (TPF) with rank $\boldsymbol{R}\in \N^d$ 
% if there exists an admissible partition $\boldsymbol{P}:= \{\boldsymbol{\Omega}^k\}_{1\leq k \leq K}$ of $\boldsymbol{\Omega}$ such that 
%   for all $1\leq k \leq K$, the subtensor $\cF^k$ of $\cF$ 
% associated to $\boldsymbol{\Omega}^k$ is of Tucker format with rank $\boldsymbol{R}$ on $\boldsymbol{\Omega}^k$. 
% \end{itemize}
% \end{mydef}
