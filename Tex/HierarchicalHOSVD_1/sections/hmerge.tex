\section{Hierarchical low rank tensor approximation}
\label{sec:HMerge}

In this section we describe the main contribution of the present paper, which
is a hierarchical low rank tensor approximation procedure
designed to compress tensors that have overall high ranks, but are
formed by many subtensors of low ranks.

%***** WE MIGHT Delete the following paragraph

The PF-Greedy-HOSVD Algorithm~\ref{algo:2} presented in
Section~\ref{sec:GHOSVD} is based on the assumption that the
partitioning of the tensor format is fixed. In the following, we introduce an algorithm that allows to
identify the low-rank-ness of some potentially large parts of a
tensor, for which a different representation format than the one
prescribed by PF-Greedy-HOSDV Algorithm~\ref{algo:2} is used to obtain
a better compression.

%and thus cannot be adaptively modified to reflect the real rank information of the
%original tensor.  For example, the most efficient storage scheme for a
%tensor of very low rank is to use the ordinary Tucker format with the
%original tensor, since in this case the core tensor is negligibly
%small.  Hence using a partitioned format could lead to unnecessary
%overhead.  However, a tensor could be very low rank in a certain
%subdomain, due to some kind of stationarity of the practical model the
%tensor describes.  Therefore we introduce an algorithm that allows to
%identify the low-rank-ness of some potentially large parts of a
%tensor, for which a different representation format than the one
%prescribed by PF-Greedy-HOSDV Algorithm~\ref{algo:2} is used to obtain
%a better compression.

\subsection{Partition tree}
To present the algorithm,  we first need to introduce the notion of \itshape partition tree\normalfont. 
A partition tree may be seen as a generalization of \itshape cluster tree \normalfont as defined in \cite{bebendorf2008hierarchical} for hierarchical matrices. 


Let $T_{\boldsymbol{I}}$ be a tree with vertices (or nodes) $\mathcal{V}(T_{\boldsymbol{I}})$ and edges $\mathcal{E}(T_{\boldsymbol{I}})$. For all $\boldsymbol{J}\in \mathcal{V}(T_{\boldsymbol{I}})$, we denote
$$
\mathcal{S}_{\boldsymbol{J}}(T_{\boldsymbol{I}}):= \left\{ \boldsymbol{J}' \in \mathcal{V}(T_{\boldsymbol{I}}), \; (\boldsymbol{J}, \boldsymbol{J}')\in \mathcal{E}(T_{\boldsymbol{I}})\right\}
$$
the set of sons of the vertex $\boldsymbol{J}$. By induction, we define the set of sons of $\boldsymbol{J}$ of the $k^{th}$ generation, denoted by $\mathcal{S}^k_{\boldsymbol{J}}(T_{\boldsymbol{I}})$ with $k\in \mathbb{N}^*$ as follows
$$
\mathcal{S}^1_{\boldsymbol{J}}(T_{\boldsymbol{I}}) = \mathcal{S}_{\boldsymbol{J}}(T_{\boldsymbol{I}}), \quad \mathcal{S}^k_{\boldsymbol{J}}(T_{\boldsymbol{I}})= \left\{ \boldsymbol{J}'' \in \mathcal{V}(T_{\boldsymbol{I}}), \; \exists \boldsymbol{J}'\in \mathcal{S}^{k-1}_{\boldsymbol{J}}(T_{\boldsymbol{I}}), \; (\boldsymbol{J}', \boldsymbol{J}'')\in \mathcal{E}(T_{\boldsymbol{I}})\right\}.
$$


The set of leaves of $T_{\boldsymbol{I}}$ is defined as
$$
\mathcal{L}(T_{\boldsymbol{I}}):=\left\{ \boldsymbol{J}\in \mathcal{V}(T_{\boldsymbol{I}}), \mathcal{S}_{\boldsymbol{J}}(T_{\boldsymbol{I}}) = \emptyset \right\}.
$$
The set of parents of leaves of a tree $T_{\boldsymbol{I}}$ is defined as the set $\mathcal{L}^p(T_{\boldsymbol{I}}) \subset \mathcal{V}(T_{\boldsymbol{I}})$ of vertices of $T_{\boldsymbol{I}}$ which 
have at least one son which is a leaf of $T_{\boldsymbol{I}}$, i.e. 
$$
\mathcal{L}^p(T_{\boldsymbol{I}}):= \left\{ \boldsymbol{J} \in \mathcal{V}(T_{\boldsymbol{I}}), \; \mathcal{S}_{\boldsymbol{J}}(T_{\boldsymbol{I}}) \cap \mathcal{L}(T_{\boldsymbol{I}}) \neq \emptyset\right\}. 
$$
For any $\boldsymbol{J}\in \mathcal{V}(T_{\boldsymbol{I}})$, the set of descendants of $\boldsymbol{J}$ in $T_{\boldsymbol{I}}$ is defined as
$$
\mathcal{D}_{\boldsymbol{J}}(T_{\boldsymbol{I}}):= \left\{ \boldsymbol{J}'\in \mathcal{V}(T_{\boldsymbol{I}}), \; \exists k \in \mathbb{N}^*, \; \boldsymbol{J}' \in \mathcal{S}^k_{\boldsymbol{J}}(T_{\boldsymbol{I}})\right\}.
$$

We are now in position to state the definition of a partition tree. 

\begin{mydef}
A tree $T_{\boldsymbol{I}}$ is called a partition tree for the set $\boldsymbol{I}$ if the following conditions hold:
 \begin{itemize}
  \item $\boldsymbol{I}$ is the root of $T_{\boldsymbol{I}}$; 
  \item For all $\boldsymbol{J}\in \mathcal{V}(T_{\boldsymbol{I}}) \setminus \mathcal{L}(T_{\boldsymbol{I}})$, $\mathcal{S}_{\boldsymbol{J}}(T_{\boldsymbol{I}})$ is an admissible partition of 
  $\boldsymbol{J}$ and $|\mathcal{S}_{\boldsymbol{J}}(T_{\boldsymbol{I}})|\geq 2$; 
  \item For all $\boldsymbol{J}\in \mathcal{V}(T_{\boldsymbol{I}})$, $\boldsymbol{J} \neq \emptyset$.
 \end{itemize}
\end{mydef}


\medskip

The goal of a partition tree $T_{\boldsymbol{I}}$, with respect to the adaptivity of the partitioning of a given tensor $\mathcal A$, is twofold:
\begin{itemize}
 \item The current admissible partition of a tensor $\mathcal{A}$ will be given by the set of leaves of the tree $\mathcal{L}(T_{\boldsymbol{I}})$;
 \item The different merging scenarii to be tested will be encoded through the different vertices of the tree that are not leaves $\mathcal{V}(T_{\boldsymbol{I}}) \setminus \mathcal{L}(T_{\boldsymbol{I}})$.
\end{itemize}

\medskip

We also introduce here the definition of the merged tree of a partition tree $T_{\boldsymbol I}$ associated to a vertex $\boldsymbol{J}\in \mathcal{V}(T_{\boldsymbol{I}})\setminus \mathcal L(T_{\boldsymbol{I}})$. 

\begin{mydef}
 Let $T_{\boldsymbol{I}}$ be a partition tree  for $\boldsymbol{I}$ with vertices (or nodes) $\mathcal{V}(T_{\boldsymbol{I}})$ and edges $\mathcal{E}(T_{\boldsymbol{I}})$. Let $\boldsymbol{J}\in \mathcal V(T_{\boldsymbol{I}}) 
 \setminus \mathcal{L}(T_{\boldsymbol{I}})$. The merged tree of $T_{\boldsymbol{I}}$ associated to the vertex $\boldsymbol{J}$ is the tree denoted by $T^m_{\boldsymbol{I}}(\boldsymbol{J})$ with root $\boldsymbol{I}$, vertices 
  $\mathcal{V}(T^m_{\boldsymbol{I}}(\boldsymbol{J})):= \mathcal{V}(T_{\boldsymbol{I}}) \setminus \mathcal{D}_{\boldsymbol{J}}(T_{\boldsymbol{I}})$ and edges 
  \begin{align*}
  \mathcal{E}\left(T^m_{\boldsymbol{I}}(\boldsymbol{J})\right) & := \mathcal{E}(T_{\boldsymbol{I}}) \cap \left( \mathcal{V}(T^m_{\boldsymbol{I}}(\boldsymbol{J})) \times \mathcal{V}(T^m_{\boldsymbol{I}}(\boldsymbol{J}))\right)\\
  & = \mathcal{E}(T_{\boldsymbol{I}}) 
  \setminus \left(\mathcal V(T_{\boldsymbol{I}}) \times \mathcal{D}_{\boldsymbol{J}}(T_{\boldsymbol{I}}) \cup \mathcal{D}_{\boldsymbol{J}}(T_{\boldsymbol{I}}) \times \mathcal{V}(T_{\boldsymbol{I}})\right).\\
  \end{align*}
\end{mydef}

The merged tree $T^m_{\boldsymbol{I}}(\boldsymbol{J})$ is the partition tree which will be associated to a tensor if it is decided, through the merging algorithm, that it is more favorable to merge all the indices subsets of the present partition 
included in $\boldsymbol{J}$ into a single indices subset $\boldsymbol{J}$. Indeed, the set of leaves of the merged tree of $T_{\boldsymbol{I}}$ associated to the vertex $\boldsymbol{J}$ can be characterized as
$$
\mathcal{L}(T^m_{\boldsymbol{I}}(\boldsymbol{J})) = \boldsymbol{J} \cup \bigcup_{
\begin{array}{c}
 \boldsymbol{J}'\in \mathcal L(T_{\boldsymbol{I}}) \\
  \boldsymbol{J}' \cap \boldsymbol{J} = \emptyset\\
\end{array}}
\{ \boldsymbol{J}'\}. 
$$

\medskip

We collect in the following lemma a few useful results that can be easily proved by a recursive argument.

\begin{mylemma}\label{lem:aux}
Let $T_{\boldsymbol{I}}$ be a partition tree for $\boldsymbol{I}$. Let $\boldsymbol{J}\in \mathcal V(T_{\boldsymbol{I}})  \setminus \mathcal{L}(T_{\boldsymbol{I}})$.
 \begin{itemize}
  \item[(i)] For all $\boldsymbol{J}'\in \mathcal{V}(T_{\boldsymbol{I}})$, $\boldsymbol{J}'\subset \boldsymbol{I}$.
  \item[(ii)] The set of leaves $\mathcal{L}(T_{\boldsymbol{I}})$ is an admissible partition of the set $\boldsymbol{I}$.
  \item [(iii)] The set $\mathcal{D}_{\boldsymbol{J}}(T_{\boldsymbol{I}})\cap \mathcal{L}(T_{\boldsymbol{I}})$ forms an admissible partition of the set $\boldsymbol{J}$.
    \item[(iv)] The merged tree $T^m_{\boldsymbol{I}}(\boldsymbol{J})$ is a partition tree for $\boldsymbol{I}$.
       % \item[(iv)] The subtree $T^s_{\boldsymbol{I}}(\boldsymbol{J})$ is a partition tree for $\boldsymbol{J}$.
        \item [(v)] The set of leaves of $T^m_{\boldsymbol{I}}(\boldsymbol{J})$ is 
        $\mathcal{L}\left( T^m_{\boldsymbol{I}}(\boldsymbol{J})\right) = \{ \boldsymbol{J} \} \cup \left( \mathcal{L}(T_{\boldsymbol{I}}) \setminus  \mathcal{D}_{\boldsymbol{J}}(T_{\boldsymbol{I}}) \right)$.
        %\item[(vi)] The set of leaves of $T^s_{\boldsymbol{I}}(\boldsymbol{J})$ is $\mathcal{L}(T^s_{\boldsymbol{I}}(\boldsymbol{J})) = \mathcal{L}(T_{\boldsymbol{I}}) \cap \mathcal{D}(\boldsymbol{J})$.
 \end{itemize}
\end{mylemma}


\subsubsection{Example: dyadic partition tree}

We give here an example of partition tree in the particular case when
$\boldsymbol{I} = I_1 \times \cdots \times I_d$ with $I_1 = \cdots =
I_d =: I$. Let $\ell \in \mathbb{N}^*$ and assume that there exists a
partition $\{I^{\ell, k}\}_{1\leq k \leq 2^\ell}$, $l \geq d$, of the
set of indices $I$ such that for all $1\leq k \leq 2^\ell$, $I^{\ell,
  k} \neq \emptyset$.  For all $1 \leq k \leq 2^{l-1}$, let
$I^{l-1,k}:= \bigcup_{(k-1)\cdot 2^d+1 \leq j \leq k\cdot 2^d}
I^{l,j}$.  A merged dyadic partition tree $T_{\boldsymbol{I}}$ is
obtained by allowing, for each vertex $I^{l-1,k}$, the merging of
$2^d$ indices subsets $\{I^{\ell, k}\}_{(k-1)\cdot 2^d+1 \leq j \leq k\cdot
  2^d}$ into a single domaine $I^{l-1,k}$.  The merge can continue
recursively until depth $\ell$.  The merged tree $T_{\boldsymbol{I}}$
is a full $2^d$-ary tree, that is a tree in which every vertex has
either $0$ or $2^d$ children and its height is at most $\ell$.

\subsection{PF-MERGE procedure}

The proposed hierarchical tensor approximation is presented in
Algorithm~\ref{algo:4}. It takes as input the tensor $\mathcal{A}\in
\R^{\boldsymbol{I}}$ of order $d$, an initial partition tree $T^{\rm
  init}_{\boldsymbol{I}}$, and the error $\varepsilon$ that will be
satisfied by the approximation.  The partition tree $T^{\rm
  init}_{\boldsymbol{I}}$ provides an initial hierarchical
partitioning of the tensor $\mathcal{A}$ into subtensors, where the
root of the tree is associated with the original tensor $\cA$, and
every vertex of the tree is associated with a subtensor.

The PF-MERGE procedure computes an approximation of $\mathcal{A}$ by traversing the
hierarchy of subtensors in a bottom-up approach and adapting
throughout the iterations the initial partition tree while ensuring
that the error of the approximation remains smaller than $\varepsilon$.
It provides as output the final partition tree $T_{\boldsymbol{I}}$,
the errors and the ranks of the approximation in Tucker format of the
subtensors corresponding to the leaves of $T_{\boldsymbol{I}}$,
$(\varepsilon^{ \boldsymbol{J}})_{\boldsymbol{J}\in
  \mathcal{L}\left(T_{\boldsymbol{I}} \right)}$ and
$\left(\boldsymbol{R}^{ \boldsymbol{J}}\right)_{\boldsymbol{J} \in
  \mathcal{L}\left( T_{\boldsymbol{I}} \right)} \subset \N^d$
respectively, and the approximation $\cA^{PTF,
  (\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal P}}$
of the tensor $\cA$ in Partitioned Tucker Format.  The algorithm
ensures that $\left\| \cA - \cA^{PTF,
  (\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal P}}
\right\| \leq \varepsilon$.

The algorithm starts by compressing the subtensors associated with the
leaves of $T^{\rm init}_{\boldsymbol{I}}$, using the PF-Greedy-HOSVD
Algorithm~\ref{algo:2}. This leads to an approximation of
$\mathcal{A}$ in a partitioned Tucker format with a greedy
distribution of the error $\varepsilon$ among subtensors.  Then the
partition tree and the associated hierarchy of subtensors are
traversed in a bottom-up approach.  For this, the algorithm uses two
sets of vertices, a set of vertices that are considered for merging
$\mathcal{N}_{\rm totest}$, which is initialized with
$\mathcal{L}^p(T_{\boldsymbol{I}})$, and a complementary set of
vertices for which no merging is attempted, $\mathcal{N}_{\rm
  nomerge}$, initialized with the empty set.

At each iteration of the algorithm, a vertex $\boldsymbol{J}_0 \in
\mathcal{N}_{\rm totest}$ is chosen and the MERGE procedure determines
whether it is more favorable, in terms of memory consumption, to merge
the subtensors corresponding to the sons of $\boldsymbol{J}_0$ into a
single subtensor and approximate it in Tucker format, or to keep them
splitted.  The MERGE procedure is presented in Algorithm \ref{algo:3}
and its description is postponed to the end of this section.  If the merge is more
favorable, then a new partition tree $T_{\boldsymbol{I}}$ that
reflects the merging is defined. If the parent of $\boldsymbol{J}_0$
is not already a vertex in $\mathcal{N}_{\rm nomerge}$, then it is
added to $\mathcal{N}_{\rm totest}$, and the errors
$(\varepsilon^{\boldsymbol{J}})_{\boldsymbol{J} \in
  \mathcal{L}(T_{\boldsymbol{I}})}$ and the ranks
$(\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J} \in
  \mathcal{L}(T_{\boldsymbol{I}})}$ of the leaves of
$T_{\boldsymbol{I}}$ are updated.  Otherwise, the vertex
$\boldsymbol{J}_0$ is added to the set $\mathcal{N}_{\rm nomerge}$ and
removed from the set $\mathcal{N}_{\rm totest}$.  The algorithm
continues until the set $\mathcal{N}_{\rm totest}$ becomes empty.

\begin{algorithm}[h!]
\caption{PF-MERGE}\label{algo:4}
\begin{algorithmic}[1]

\BState \textbf{Input:}

\State $\mathcal{A}\in \R^{\boldsymbol{I}} \gets \text{a tensor of order } d$
\State $T^{\rm init}_{\boldsymbol{I}}$ an initial partition tree of $\boldsymbol{I}$
\State $\varepsilon>0 \gets \text{error tolerance criterion}$

\medskip
\BState \textbf{Output:}
\State $T_{\boldsymbol{I}}$ a final partition tree of $\boldsymbol{I}$
\State A set of leaf errors $(\varepsilon^{ \boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left(T_{\boldsymbol{I}} \right)}$
\State A set of leaf ranks $\left(\boldsymbol{R}^{ \boldsymbol{J}}\right)_{\boldsymbol{J} \in \mathcal{L}\left( T_{\boldsymbol{I}} \right)} \subset \N^d$

\medskip
\BState \textbf{Begin:}
%\BState \emph{begin}:

\State Set $T_{\boldsymbol{I}} = T^{\rm init}_{\boldsymbol{I}}$.


\State Compute ($(\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left( T_{\boldsymbol{I}}\right)}$, $(\overline{\varepsilon}^{\boldsymbol{J}})_{\boldsymbol{J}\in\mathcal{L}\left( T_{\boldsymbol{I}}\right)}$ )
= PF-Greedy-HOSVD($\cA$, $\mathcal{L}\left( T_{\boldsymbol{I}}\right)$ , $\varepsilon$) 

\State Compute $\eta^2:= \varepsilon^2 - \sum_{\boldsymbol{J} \in\mathcal{L}\left( T_{\boldsymbol{I}}\right)} |\overline{\varepsilon}^{\boldsymbol{J}}|^2$.

\State For all $\boldsymbol{J}\in \mathcal{L}\left( T_{\boldsymbol{I}}\right)$, define $\varepsilon^{\boldsymbol{J}}:= \sqrt{|\overline{\varepsilon}^{\boldsymbol{J}}|^2 + \frac{|\boldsymbol{J}|}{|\boldsymbol{I}|}\eta^2}$.

\State Set $\mathcal{N}_{\rm totest} = \mathcal{L}^p(T_{\boldsymbol{I}})$ and $\mathcal{N}_{\rm nomerge} = \emptyset$.

\While{$ \mathcal{N}_{\rm totest} \neq \emptyset$}

\State Choose $\boldsymbol{J}_0 \in \mathcal{N}_{\rm totest}$. 

\State ($T^{\rm fin}_{\boldsymbol{I}}$,${merge}$,$(\varepsilon^{\rm fin, \boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}} \right)}$,$\left(\boldsymbol{R}^{\rm fin, \boldsymbol{J}}\right)_{\boldsymbol{J} \in \mathcal{L}\left( T^{\rm fin}_{\boldsymbol{I}} \right)}$) 
= MERGE($\mathcal{A}$,$T_{\boldsymbol{I}}$,$(\varepsilon^{\boldsymbol{J}})_{\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})}$,$(\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})}$,  $\boldsymbol{J}_0$)

\If{${merge} = {\rm true}$}

\State $T_{\boldsymbol{I}} = T_{\boldsymbol{I}}^{\rm fin}$
\State $\mathcal{N}_{\rm totest} = \mathcal{L}^p(T_{\boldsymbol{I}}^{\rm fin}) \setminus  \mathcal{N}_{\rm nomerge}$.
\State $(\varepsilon^{\boldsymbol{J}})_{\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})} = (\varepsilon^{\rm fin, \boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}} \right)}$, $(\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})} = \left(\boldsymbol{R}^{\rm fin, \boldsymbol{J}}\right)_{\boldsymbol{J} \in \mathcal{L}\left( T^{\rm fin}_{\boldsymbol{I}} \right)} $

\Else 
\State $\mathcal{N}_{\rm nomerge} = \mathcal{N}_{\rm nomerge} \cup \{ \boldsymbol{J}_0\}$;  $\mathcal{N}_{\rm totest} = \mathcal{N}_{\rm totest}  \setminus \{ \boldsymbol{J}_0\}$. 
\EndIf


\EndWhile


\Return $T_{\boldsymbol{I}}$, $(\varepsilon^{\boldsymbol{J}})_{\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})}$, $(\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})}$
%\EndProcedure
\end{algorithmic}
\end{algorithm}

The MERGE procedure is described in Algorithm~\ref{algo:3}. Given a
vertex $\boldsymbol{J}_0 \in \mathcal{N}_{\rm totest}$ and a partition
tree $T_{\boldsymbol{I}}$, the algorithm computes $M_{\rm nomerge}$,
the memory needed for storing in Tucker format the approximation of
the subtensors corresponding to the sons of $\boldsymbol{J}_0$ in the
partition tree, and $\eta:= \sqrt{\sum_{\boldsymbol{J} \in
    \mathcal{P}_{\boldsymbol{J}_0}} |\varepsilon^{\boldsymbol{J}}|^2}$,
the contribution of the errors of those approximations to the total
approximation error. Then it calls the Greedy-HOSVD
Algorithm~\ref{algo:1} to compute an approximation in Tucker format of
$\cA^{\boldsymbol{J}_0}$, the subtensor associated with the vertex
$\boldsymbol{J}_0$, that satisfies the error tolerance $\eta$.  This
ensures that the contribution to the total approximation error is
preserved if the merge is performed.  If $M_{\rm merge}$, the memory
needed to store the approximation of the subtensor associated to the
vertex $\boldsymbol{J}_0$, is smaller than $M_{\rm nomerge}$, then the
merge is performed. Otherwise, it is not. 

\begin{figure}[htbp]
\label{fig:coulomb3D}
\begin{tabular}{c c}
\hspace{-0.5cm}
\includegraphics[scale=0.3]{figures/3DCoulombErrorsBeforeLowHigh.png} &
\\
(a) & \\
\hspace{-0.5cm}
\includegraphics[scale=0.28]{figures/3DCoulombErrorsAfterLowHigh.png} &
\hspace{-0.5cm} 
\includegraphics[scale=0.28]{figures/Coulomb_After_Cube.png} \\
 (b) & (c)
\end{tabular}
\caption{Coulomb 3D case: a) error distribution per subtensor after
  the greedy step (512 subtensors); b) error distribution per
  subtensor after the first merge step; c) error distribution in the
  final tensor in partitioning format.}
\end{figure}


For the extreme case where the whole original tensor is of very low
rank, the merge stage will eventually choose to merge all the
subtensors, meaning that the final result of the merge stage will be a
trivial approximation in Tucker format of the original very low rank
tensor, which is the desired result. For a typical practical case, the
merge stage will lead to a hierarchical representation of the original
tensor, where the higher rank subtensors have more storage assigned
for their complicated subtree structure, and the low rank subtensors
have relatively simple Tucker format approximations corresponding to
the leaves in a tree.

As described in Algorithm~\ref{algo:4}, once a merge step rejects the
merge and keeps the partitioning in a group of subtensors, no further
merge will be attempted for a subdomain containing this group of
subtensors.  The advantage of this is that the number of merge steps
is reduced and the merge tends to be performed on relatively small
tensors, since it tends to stop when it encounters a high rank
subtensor inside a domain.  Considering that a HOSVD should be
computed for every merge step, decreasing the number of merge steps
reduces the computational cost tremendously.  However, it could also
produce suboptimal results due to the premature rejection of a merge.
A merge several steps afterwards might outperform the current storage,
though in the current step not merging might be better.

\medskip

At this point, we would like to stress on the fact that this problem could be overcome in principle by using an algorithm which could rapidly compute 
the HOSVD decomposition of a tensor, knowing the HOSVD decomposition of its subtensors. For the sake of conciseness, we leave this question for future work and do not adress this issue here. 

\medskip

We illustrate in Figure~\ref{fig:coulomb3D} the compression of the 3D
Coulomb potential obtained by using Algorithm~\ref{algo:3}.  This
function is described in more details in section~\ref{subsec:coulomb}.
The partition tree is an octree, each vertex is associated with a
tensor, which is recursively divided into eight subtensors.  In this
example the recursion stops at depth 2, and the partition tree has 512
leaves and associated subtensors.  Figure~\ref{fig:coulomb3D}~(a)
displays the distribution of the error among subtensors obtained by
using PF-Greedy-HOSVD algorithm, where the subtensors with higher
errors are displayed on the left.  The result of the first merge step
is displayed in Figure~\ref{fig:coulomb3D}~(b), while the error
distribution in the final tensor in partitioned Tucker format is
displayed in Figure~\ref{fig:coulomb3D}~(b).  As expected in this
case, the subtensors along the superdiagonal are not merged since they
have higher ranks, while subtensors further away from the
superdiagonal are merged into larger subtensors since they have
smaller ranks.

\begin{algorithm}[h!]
\caption{MERGE}\label{algo:3}
\begin{algorithmic}[1]
%\Procedure{}{}

\BState \textbf{Input:}

\State $\mathcal{A}\in \R^{\boldsymbol{I}} \gets \text{a tensor of order } d$
\State $T_{\boldsymbol{I}}$ an initial partition tree of $\boldsymbol{I}$
\State A set of leaf errors $(\varepsilon^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left(T_{\boldsymbol{I}}\right)}$
\State A set of leaf ranks $(\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}(T_{\boldsymbol{I}})} \subset \N^d$
\State $\boldsymbol{J}_0\in \mathcal{V}(T_{\boldsymbol{I}}) \setminus \mathcal{L}(T_{\boldsymbol{I}})$.


\medskip
\BState \textbf{Output:}
\State $T^{\rm fin}_{\boldsymbol{I}}$ a final partition tree of $\boldsymbol{I}$
\State $merge$ a boolean indicating if the tree has been merged or not 
\State A set of leaf errors $(\varepsilon^{\rm fin, \boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}} \right)}$
\State A set of leaf ranks $\left(\boldsymbol{R}^{\rm fin, \boldsymbol{J}}\right)_{\boldsymbol{J} \in \mathcal{L}\left( T^{\rm fin}_{\boldsymbol{I}} \right)} \subset \N^d$


\medskip
\BState \textbf{Begin:}
%\BState \emph{begin}:
\State Set $\mathcal{P}_{\boldsymbol{J}_0}:= \mathcal{D}_{\boldsymbol{J_0}}(T_{\boldsymbol{I}}) \cap \mathcal{L}(T_{\boldsymbol{I}})$. From Lemma~\ref{lem:aux} (iii), $ \mathcal{P}_{\boldsymbol{J}_0}$ is an admissible partition of the set $\boldsymbol{J}_0$.

\State Set $M_{\rm nomerge}:= \sum_{\boldsymbol{J} \in \mathcal{P}_{\boldsymbol{J}_0}} M_{\rm TF}\left( \boldsymbol{J}, \boldsymbol{R}^{\boldsymbol{J}}\right)$

\State Set $\eta:= \sqrt{\sum_{\boldsymbol{J} \in \mathcal{P}_{\boldsymbol{J}_0}} |\varepsilon^{\boldsymbol{J}}|^2}$. 

\State Compute $\boldsymbol{R}$ = Greedy-HOSVD($\cA^{\boldsymbol{J}_0}$, $\eta$) 

\State Compute $M_{\rm merge}:=  M_{\rm TF}\left( \boldsymbol{J}_0, \boldsymbol{R}\right)$.

\If{$M_{\rm merge} < M_{\rm nomerge}$}

\State Set ${merge} = {\rm true}$, $T^{\rm fin}_{\boldsymbol{I}} = T_{\boldsymbol{I}}^m(\boldsymbol{J}_0)$.

\For{$\boldsymbol{J} \in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}}\right)$}

\If{$\boldsymbol{J} = \boldsymbol{J}_0$}

\State Set $\boldsymbol{R}^{\rm fin, \boldsymbol{J}_0}:= \boldsymbol{R}$ and $\varepsilon^{\rm fin, \boldsymbol{J}_0}:= \eta$.

\Else 

\State From Lemma~\ref{lem:aux} (v), necessarily $\boldsymbol{J} \in \mathcal{L}(T_{\boldsymbol{I}})$. 

\State Set $\boldsymbol{R}^{\rm fin, \boldsymbol{J}}:= \boldsymbol{R}^{\boldsymbol{J}}$ and $\varepsilon^{\rm fin, \boldsymbol{J}}:= \varepsilon^{\boldsymbol{J}}$.

\EndIf

\EndFor

\Else 

\State Set ${merge} = {\rm false}$, $T^{\rm fin}_{\boldsymbol{I}} = T_{\boldsymbol{I}}$.

\For{$\boldsymbol{J} \in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}}\right) = \mathcal{L}(T_{\boldsymbol{I}})$}

\State Set $\boldsymbol{R}^{\rm fin, \boldsymbol{J}}:= \boldsymbol{R}^{\boldsymbol{J}}$ and $\varepsilon^{\rm fin, \boldsymbol{J}}:= \varepsilon^{\boldsymbol{J}}$.

\EndFor

\EndIf

\Return  $T^{\rm fin}_{\boldsymbol{I}}$, ${merge}$, $(\varepsilon^{\rm fin, \boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}} \right)}$, $\left(\boldsymbol{R}^{\rm fin, \boldsymbol{J}}\right)_{\boldsymbol{J}\in \mathcal{L}\left(T^{\rm fin}_{\boldsymbol{I}} \right)}$
%\EndProcedure
\end{algorithmic}
\end{algorithm}

% \section{Linear Algebra in HPF}
% \label{sec:linAlg}
% 
% Many linear algebra operations are naturally supported by the HPF format. 
% 
% \subsection{Scaling, Addition, and norm}
% 
% Scaling by a number in HPF format can be trivially computed by scaling every subtensor with the same number. However, for addition of two different tensors $\mathcal{A}$ and $\mathcal{B}$, we need some restrictions on the partitioning to carry out effective computation:
% 
% \begin{mydef}
% 
% Let $\mathcal{A}, \mathcal{B}$ be two tensors of the same shape in HPF format. We call them as having compatible partitioning if:
% 
% 	1) one of them is in Tucker format; or
% 	
% 	2) both of them have the same partitioning, and the corresponding subtensors at every position have compatible partitioning.
% 
% \end{mydef}
% 
% If both tensors are obtained by the $HierarchicalMerge$ algorithm above, they are guaranteed to have compatible partitioning.
% 
% Once two tensors have compatible partitioning, they are essentially of the case 1 in the definition of partitioning compatibility, i.e. one of them is already in Tucker format. Because otherwise, we can always consider a smaller subtensor recursively, until case 1 is reached. In the situation of case 1, the tensor addition can be done by considering a Tucker format as a partitioned HPF format where each subtensor has trivial Tucker representation (i.e. a Tucker representation where the core tensor is the same as itself). Thus case 1 can be reduced to the trivial case, where two tensors are both in Tucker format.
% 
% To compute the norm of a HPF tensor $\mathcal{A}$ with partitioning $(m_1, ..., m_d)$, one only needs to compute the squared norm of every of its subtensors and then sum them up:
% 
% $$ || \mathcal{A} ||^2  = \sum_{i_1, ..., i_d} || \mathcal{A}_{(i_1, ..., i_d)} || ^2 $$
% 
% 
% \subsection{Tensor Matrix Multiplication}
% 
% 
% \begin{mytheo}
% Let $\mathcal{A}$ be a tensor of dimension $d$, shape $n_1 \times \cdots \times n_d$ in HPF format, $M$ be a matrix of shape $u \times n_k$ where $1 \leq k \leq d$. Then the TMM(tensor matrix multiplication) $A \times_k M$ along mode $k$ can be computed by the following steps:
% 
% Case 1) If $\mathcal{A}$ is already in Tucker format, use TMM of Tucker format.
% 
% Case 2) If not, $\mathcal{A}$ has a partitioning $(m_1, ..., m_k, ..., m_d)$, together with a collection of small tensors in HPF format $\{ \mathcal{A}_{(s_1, ..., s_d)} \}$ where $1 \leq s_i \leq m_i$ for all $1 \leq i \leq d$. Suppose the partitioning of $\mathcal{A}$ along mode $k$ is at positions $p_1, ..., p_{m_k}$. We then partition matrix $M$ into submatrices $M_1, ..., M_{m_k}$ of shape $u \times p_1, ..., u \times p_k$ respectively. The final result is achieved by replacing every column along mode $k$ of subtensors $ \mathcal{A}_{(s_1, ..., 1,..., s_d)}, ...,, \mathcal{A}_{(s_1, ..., m_k,..., s_d)}  $ by 
% 
% $$ \sum_{k=0}^{m_k} M_k \mathcal{A}_{(s_1, ..., s_d)}, $$ 
% 
% each multiplication of which can be obtained by computing a smaller TMM by the same procedure. 
% 
% \end{mytheo}
% 
% \begin{proof}
% 
% This is derived from matrix multiplication formula:
% 
% $$ [U_1, ..., U_s] [V_1, ..., V_s]^T = \sum_{i = 0}^s U_i V_i^T $$
% 
% 
% \end{proof}
% 
% The same procedure holds for tensor contraction with vector, by regarding a vector as a matrix.


