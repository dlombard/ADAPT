
\section{Greedy-HOSVD for Tucker Partitioned Format (TPF)}\label{sec:GHOSVD}

In this section an algorithm is presented, which constructs an approximation of a given tensor of order $d$, associated to an admissible partition of the set of indices of the tensor, 
where the tensor is approximated by a tensor in Tucker format on each indices subsets. The algorithm relies on a greedy procedure which enables to distribute the error among the subdomains in an optimal way.  

\subsection{Notation and definitions}
We introduce some notation and definitions on discrete tensors and subtensors which are used in the sequel, and are very similar to those used in~\cite{khoromskij2007structured, bebendorf2008}.

\medskip

We consider from now on and in all the rest of the paper the case of \itshape discrete \normalfont tensors. Let $d\in \N^*$ and let $I_1,\cdots,I_d$ be finite discrete sets of indices. For all $1\leq j \leq d$, we denote $n_j:= \# I_j$ the cardinality of the set $I_j$. 
We also denote 
$\boldsymbol{I} := I_1\times \cdots\times I_d$ and $n:= n_1\times \cdots \times n_d$.

\medskip

A tensor $\cA$ of order $d$ defined on $\boldsymbol{I}$ is a collection of $n_1\times \cdots \times n_d$ real numbers $\cA:=\left(a_{\boldsymbol{i}}\right)_{\boldsymbol{i}\in \boldsymbol{I} } \in \R^{\boldsymbol{I}}$. 
Let $A_1:=(a^1_{i_1})_{1\leq i_1 \leq n_1}\in \mathbb{R}^{n_1}, \cdots,A_d:=(a^d_{i_d})_{1\leq i_d \leq n_d}\in \mathbb{R}^{n_d}$, the pure tensor product tensor $A_1\otimes \cdots \otimes A_d= \left(a_{\boldsymbol{i}}\right)_{\boldsymbol{i}\in \boldsymbol{I} } \in \R^{\boldsymbol{I}}$ is the tensor of order $d$ defined 
such that for all ${\boldsymbol{i}}:=(i_1,\cdots,i_d)\in \boldsymbol{I}$, 
$$
a_{\boldsymbol{i}}= \Pi_{j=1}^d a^j_{i_j}.
$$
Given two tensors $\cA = (a_{\boldsymbol{i}})_{\boldsymbol{i}\in \boldsymbol{I}}, \cB = (b_{\boldsymbol{i}})_{\boldsymbol{i}\in \boldsymbol{I}} \in \mathbb{R}^{\boldsymbol{I}}$, the $\ell^2$ scalar product between $\cA$ and $\cB$ in $\mathbb{R}^{\boldsymbol{I}}$ 
is denoted by 
$$
\langle \cA, \cB \rangle:= \sum_{\boldsymbol{i}\in \boldsymbol{I}} a_{\boldsymbol{i}}b_{\boldsymbol{i}}.
$$

\medskip


The tensor $\cA$ is said to be of canonical format with rank $R\in \mathbb{N}$ if
$$
\cA = \sum_{r=1}^R A_1^r \otimes \cdots \otimes A_d^r,
$$
where for all $1\leq j \leq d$ and $1\leq r \leq R$, $A_j^r \in \R^{I_j}$. 

The tensor $\cA$ is said to be of Tucker Format with rank $\boldsymbol{R} = (R_1,\cdots,R_d)\in (\N)^d$ if 
\begin{equation}\label{eq:TF}
\cA  = \sum_{r_1=1}^{R_1} \cdots \sum_{r_d = 1}^{R_d} c_{r_1,\cdots,r_d} A_1^{r_1} \otimes \cdots \otimes A_d^{r_d},
\end{equation}
where for all $1\leq j \leq d$ and all $1\leq r_j\leq R_j$, $A_j^{r_j} \in \R^{I_j}$ and $\left(c_{r_1,\cdots,r_d}\right)_{1\leq r_1\leq R_1,\cdots, 1\leq r_d \leq R_d} \in \R^{R_1\times \cdots \times R_d}$.


It is clear from expression (\ref{eq:TF}) that the memory needed to store a tensor defined on a set of indices $\boldsymbol{I} = I_1\times \cdots \times I_d$ with ranks $\boldsymbol{R}:=(R_1,\cdots,R_d)\in \N^d$ is equal to 
\begin{equation}\label{eq:mem}
M_{TF}(\boldsymbol{I}, \boldsymbol{R}):= \left\{ \begin{array}{ll}
                                                 \Pi_{j=1}^d R_j + \sum_{j=1}^d R_j |I_j| & \; \mbox{ if } R_j>0 \mbox{ for all }1\leq j \leq d.\\
                                                  0 & \; \mbox{ otherwise}.\\
                                                 \end{array}\right.
\end{equation}


Several other tensor formats can be found in the litterature. We refer the reader for instance to \cite{hackbusch2012,grasedyck2013literature,khoromskij2012tensors,oseledets2011tensor} 
for the precise definitions of the Tensor Train and the Hierarchical Tree Tensor formats. For the sake of simplicity, we do not give their definition 
in full details here. 


\medskip


Let now $J_1\subset I_1$, ..., $J_d\subset I_d$ be non-empty subsets of indices and $\boldsymbol{J}:= J_1 \times \cdots \times J_d$. The subtensor of $\cA$ associated to 
$\boldsymbol{J}$ is the tensor $\cA^{\boldsymbol{J}}$ defined as $\cA^{\boldsymbol{J}}:=(a_{\boldsymbol{i}\in \boldsymbol{J}}) \in \R^{\boldsymbol{J}}$. 


Let us now consider a partition $\mathcal{P}$ of $\boldsymbol{I}$ such that for all $\boldsymbol{J}\in \mathcal{P}$, 
there exists $J_1 \subset I_1,\cdots,J_d \subset I_d$ such that $\boldsymbol{J}:= J_1 \times \cdots \times J_d$. 
Recall that the fact that $\mathcal{P}$ is a partition of $\boldsymbol{I}$ implies that $\displaystyle \boldsymbol{I} = \bigcup_{\boldsymbol{J}\in \mathcal{P}} \boldsymbol{J}$ and that for all 
$\boldsymbol{J}_1, \boldsymbol{J}_2 \in \mathcal{P}$ such that $\boldsymbol{J}_1 \neq \boldsymbol{J}_2$, then $\boldsymbol{J}_1 \cap \boldsymbol{J}_2 = \emptyset$. 
Following a denomination already introduced in~\cite{khoromskij2007structured}, 
such a partition will be called hereafter an \itshape admissible \normalfont partition of $\boldsymbol{I}$. 


Then, the collection of subtensors of $\cA$ associated to the partition $\mathcal{P}$ is defined as the set of subtensors $(\cA^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal{P}}$. 

A particular case of admissible partition of $\boldsymbol{I}$ can be for instance constructed as follows: for all $1\leq j \leq d$, let $K_j \in \N^*$ and let us consider a partition $\mathcal{P}_j:= \left\{ I_j^{k_j}\right\}_{1\leq k_j \leq K_j}$ 
of $I_j$ such that $I_j:= \bigcup_{1\leq k_j \leq K_j} I_j^{k_j}$ and $I_j^{k_j} \cap I_j^{l_j} = \emptyset$ for all $1\leq k_j\neq l_j \leq K_j$. Let us denote
$$
\mathcal{P}:= \left( \bigtimes_{j=1}^d I_j^{k_j}\right)_{ 1\leq k_1 \leq K_1, \cdots, 1\leq k_d \leq K_d}.
$$
The partition $\mathcal{P}$ then defines an admissible partition of $\boldsymbol{I}$, and will be called hereafter the \itshape tensorized partition \normalfont associated to the collection of partitions $(\mathcal P_j)_{1\leq j \leq d}$.

\medskip

The following definitions are introduced:

\begin{mydef}
$\;$


\begin{itemize}
 \item A tensor $\cA$ defined on $\boldsymbol{I}$ is said to be of Canonical Partitioning Format (CPF) if there exists an admissible partition $\mathcal{P}$ of $\boldsymbol{I}$ such that for all $\boldsymbol{J}\in \mathcal{P}$, the subtensor $\cA^{\boldsymbol{J}}$ 
 of $\cA$ associated to $\boldsymbol{J}$ is of canonical format on $\boldsymbol{J}$. 
  \item A tensor $\cA$ defined on $\boldsymbol{I}$ is said to be of Tucker Partitioning Format (TPF) if there exists an admissible partition $\mathcal{P}$ of $\boldsymbol{I}$ such that for all $\boldsymbol{J}\in \mathcal{P}$,
  the subtensor $\cA^{\boldsymbol{J}}$ of $\cA$ 
 associated to $\boldsymbol{J}$ is of Tucker format on $\boldsymbol{J}$.  
 \end{itemize}
\end{mydef}




\subsection{Greedy-HOSVD for Tucker format}

Let $\cA:=(a_{\boldsymbol{i}})_{\boldsymbol{i}\in \boldsymbol{I}} \in \R^{\boldsymbol{I}}$ be a discrete tensor of order $d$. The aim of the two following sections is to present an algorithm which, given a particular admissible partition of the set of indices, 
provides effective low-rank approximations in Tucker format for all subtensors of $\cA$. The algorithm presented hereafter guarantees that the global $l^2$ error between the tensor $\cA$ and 
the obtained approximation of the tensor is lower than an a priori chosen error criterion. The main novelty of this algorithm consists in using a greedy algorithm in conjunction with the well-known HOSVD procedure 
which enables to distribute the error in a non-uniform adapted way among the different unfoldings of the tensor $\cA$. This Greedy-HOSVD procedure 
is the starting point of the Hierarchical Merge algorithm which we present in Section~\ref{sec:HMerge}.

\medskip


We recall here some well-known definitions and introduce some notation about unfoldings and singular value decomposition. For all $1\leq j \leq d$, let $\widehat{n}_j:= \Pi_{j'\neq j } n_{j'}$ and $\widehat{\boldsymbol{I}}_j:= I_1 \times \cdots \times I_{j-1} \times I_{j+1} \times \cdots \times I_d$. 
For all $\boldsymbol{i}=(i_1,\cdots,i_d)\in \boldsymbol{I}$, let 
$\widehat{\boldsymbol{i}}_j :=(i_1,\cdots, i_{j-1}, i_{j+1}, \cdots, i_d) \in \widehat{\boldsymbol{I}}_j$. 


The $j^{th}$ unfolding associated to the tensor $\cA$ is the matrix $\cA_j \in \R^{I_j \times \widehat{\boldsymbol{I}}_j}$ which is defined such that 
$$
\forall \boldsymbol{i}:=(i_1,\cdots,i_d) \in \boldsymbol{I}, \quad (\cA_j)_{i_j,\widehat{\boldsymbol{i}}_j}= a_{\boldsymbol{i}}.
$$
The singular values of $\cA_j$ (ranged in decreasing order) are then defined by $\sigma_{j}^1(\cA) \geq \sigma_{j}^2(\cA) \geq \cdots \geq \sigma_{j}^{p_j(\cA)}(\cA)$, where 
$p_j(\cA):= \min(n_j, \widehat{n}_j)$. For all $1\leq q \leq p_j(\cA)$, we denote $U^q_j(\cA)\in \R^{n_j}$ 
a left-hand side singular mode of $\cA_j$ associated to the singular value $\sigma_{j}^q(\cA)$ so that $(U^1_j(\cA), U^2_j(\cA), \cdots, U^{p_j(\cA)}_j(\cA))$ is an orthonormal family of $\R^{n_j}$. 

\medskip

For all $1\leq r_1\leq p_1(\cA)$, ..., $1\leq r_d \leq p_d(\cA)$, let us define
$$
c^\cA_{r_1,\cdots,r_d}:= \left\langle \cA, U_1^{r_1}(\cA) \otimes \cdots \otimes U_d^{r_d}(\cA) \right\rangle.
$$
Let $\boldsymbol{R}:=(R_1,\cdots,R_d)\in (\N^{*})^d$ such that for all $1\leq j \leq d$, $1\leq R_j \leq p_j(\cA)$ and let us define
$$
\cA^{TF,\boldsymbol{R}} := \sum_{1\leq r_1 \leq R_1} \cdots \sum_{1\leq r_d \leq R_d} c^\cA_{r_1,\cdots,r_d} U_1^{r_1}(\cA) \otimes \cdots \otimes U_d^{r_d}(\cA).  
$$
Then, the following inequality holds~\cite{hackbusch2012}
\begin{equation}\label{eq:generrTT}
\left\|\cA - \cA^{TF, \boldsymbol{R}} \right\|\leq \sqrt{\sum_{1\leq j \leq d} \sum_{R_j+1 \leq q_j \leq p_j(\cA)} \left|\sigma_j^{q_j}(\cA)\right|^2}. 
\end{equation}
Note that the tensor $\cA^{TF, \boldsymbol{R}} $ is a tensor of Tucker format with rank $\boldsymbol{R}$. A natural question is then the following: given an \textit{a priori} chosen error tolerance $\epsilon >0$, how should one choose the rank $\boldsymbol{R}$ to ensure that 
\begin{equation}\label{eq:TTerror}
\left\|\cA - \cA^{TF, \boldsymbol{R}} \right\| \leq \epsilon. 
\end{equation}
The most commonly used strategy to choose $\boldsymbol{R}$ in order to guarantee (\ref{eq:TTerror}) is the following~\cite{hackbusch2012}. 
For all $1\leq j \leq d$, the integer $R_j$ is chosen so that 
$$
R_j:=\min\left\{ 1\leq R \leq p_j(\cA), \; \sum_{p_j(\cA) \geq q\geq R+1} |\sigma_j^{q}(\cA)|^2 \leq \epsilon^2/d \right\}.
$$
By construction, using (\ref{eq:generrTT}), it holds that 
$\cA^{TF, \boldsymbol{R}}$ obviously satisfies (\ref{eq:TTerror}). Such a choice implies that the squared error tolerance $\epsilon^2$ is uniformly distributed with respect to each value of $1\leq j \leq d$. 

\medskip

In the present work, it appeared that distributing the error in an appropriate manner with respect to $j$ is a crucial feature for the proposed algorithms (based on partitioning) to be efficient in terms of memory compression. 
The first contribution of this paper consists in suggesting 
an alternative numerical strategy to choose the rank $\boldsymbol{R}$ so that $\cA^{TF, \boldsymbol{R}}$ satisfies (\ref{eq:TTerror}), which appears to yield sparser approximations of the tensor $\cA$ while 
maintaining the same level of accuracy than the strategy presented 
above. The method is based on a greedy algorithm, which is an iterative procedure, whose aim is to compute a set of ranks $\boldsymbol{R} \in (\mathbb{N}^*)^d$ such that 
$$
\left\|\cA - \cA^{TF, \boldsymbol{R}} \right\| \leq \varepsilon,
$$
where $\varepsilon$ is a desired error tolerance, and the principle of the algorithm is the following. 
Assume that we have already an approximation of $\mathcal A$ at hand, given by $\mathcal{A}^{TF, \widetilde{\boldsymbol{R}}}$ for some $\widetilde{\boldsymbol{R}}:= (\widetilde{R}_1, \cdots, \widetilde{R}_d)\in (\mathbb{N}^*)^d$. 
The backbone of the greedy algorithm is to increase the rank corresponding to the variable $1\leq j_0\leq d$ which has the greatest contribution to the error 
$$
\sqrt{\sum_{1\leq j \leq d} \sum_{\widetilde{R}_j+1 \leq q_j \leq p_j(\cA)} \left|\sigma_j^{q_j}(\cA)\right|^2}.
$$
More precisely, we select the integer $1\leq j_0 \leq d$ such that $\displaystyle j_0 \in \mathop{\rm argmax}_{1\leq j \leq d} \sigma_j^{\widetilde{R}_j+1 }(\cA)$ and increase the $j_0^{th}$ rank by one. This procedure is repeated until 
we obtain a set of ranks such that the desired error tolerance is reached.  Algorithm~\ref{algo:1}
summarizes the Greedy-HOSVD procedure.


\begin{algorithm}[h!]
\caption{Greedy-HOSVD}\label{algo:1}
\begin{algorithmic}[1]
%\Procedure{}{}

\BState \textbf{Input:}

\State $\mathcal{A}\in \R^{\boldsymbol{I}} \gets \text{a tensor of order } d$
\State $\varepsilon>0 \gets \text{error tolerance criterion}$

\medskip
\BState \textbf{Output:}
\State Rank $\boldsymbol{R}:=(R_1,\cdots,R_d) \in \N^d$


\medskip
\BState \textbf{Begin:}
%\BState \emph{begin}:

\State Set $\boldsymbol{R}:=(0,\cdots,0)$

\While{$\displaystyle \sum_{1\leq j \leq d} \sum_{R_j+1 \leq q_j \leq p_j(\cA)} \left|\sigma_j^{q_j}(\cA)\right|^2 > \varepsilon^2$}

\State Select $1\leq j_0 \leq d$ such that
$$
j_0 = \mathop{\rm{argmax}}_{1\leq j \leq d} \sigma_j^{R_j+1}(\cA).
$$

\If{$\boldsymbol{R} = (0,\cdots,0)$}

\State Set $\boldsymbol{R}:=(1,\cdots,1)$

\Else

\State $R_{j_0} \gets R_{j_0}+1$.

\EndIf

\EndWhile

\Return $\boldsymbol{R}$
%\EndProcedure
\end{algorithmic}
\end{algorithm}

In view of (\ref{eq:generrTT}), it can be obviously seen that the rank $\boldsymbol{R}$ computed by the Greedy-HOSVD procedure described in Algorithm~\ref{algo:1} necessarily implies that $\cA^{TF,\boldsymbol{R}}$ satisfies (\ref{eq:TTerror}).


\subsection{PF-Greedy-HOSVD for Partitioned Tucker format}

We now present a direct generalization of the Greedy-HOSVD procedure described in Algorithm~\ref{algo:1} in order to construct an approximation of the tensor $\cA$ in a Partitioned Tucker format associated to an a priori fixed admissible partition 
$\mathcal P$ of $\boldsymbol{I}$. 

For all $\boldsymbol{J}\in \mathcal P$, let $\boldsymbol{R}^{\boldsymbol{J}}:=(R_1^{\boldsymbol{J}}, \cdots, R_d^{\boldsymbol{J}}) \in \N^d$ be a set of ranks. 
We define an approximation $\cA^{PTF, (\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal P}}$ of the tensor $\cA$ in Partitioned Tucker Format (PTF) as follows: for all $\boldsymbol{J}_0\in \mathcal P$, 
$$
\left(\cA^{PTF, (\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal P}}\right)^{\boldsymbol{J}_0} = \left(\cA^{\boldsymbol{J}_0}\right)^{TF,\boldsymbol{R}^{\boldsymbol{J}_0}}. 
$$
It then naturally holds, using (\ref{eq:generrTT}), that
$$
\left\| \cA - \cA^{PTF, (\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal P}} \right\| \leq \sqrt{\sum_{\boldsymbol{J}\in \mathcal P} \sum_{1\leq j \leq d} \sum_{R^k_j+1 \leq q_j \leq p_j(\cA^{\boldsymbol{J}})} \left|\sigma_j^{q_j}(\cA^{\boldsymbol{J}})\right|^2}.
$$


For a given error tolerance $\varepsilon>0$, the procedure described in Algorithm~\ref{algo:2} then naturally produces a set of ranks $(\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal P}$ such that 
$$
\left\| \cA - \cA^{PTF, (\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal P}} \right\| \leq \varepsilon,
$$
and is also a greedy algorithm.



\begin{algorithm}[h!]
\caption{PF-Greedy-HOSVD}\label{algo:2}
\begin{algorithmic}[1]
%\Procedure{}{}

\BState \textbf{Input:}

\State $\mathcal{A}\in \R^{\boldsymbol{I}} \gets \text{a tensor of order } d$
\State $\mathcal P \gets \text{an admissible partition of }\boldsymbol{I}$
\State $\varepsilon>0 \gets \text{error tolerance criterion}$

\medskip
\BState \textbf{Output:}
\State Set of Ranks $(\boldsymbol{R}^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal P} \subset \N^d$
\State Set of local errors $(\varepsilon^{\boldsymbol{J}})_{\boldsymbol{J}\in \mathcal P}$ satisfying $\sum_{\boldsymbol{J}\in\mathcal P} |\varepsilon^{\boldsymbol{J}}|^2 < |\varepsilon|^2$.


\medskip
\BState \textbf{Begin:}
%\BState \emph{begin}:

\State Set $\boldsymbol{R}^{\boldsymbol{J}}:=(0,\cdots,0)$ for all $\boldsymbol{J}\in \mathcal P$

\While{$\displaystyle \sum_{\boldsymbol{J}\in \mathcal P} \sum_{1\leq j \leq d} \sum_{R^{\boldsymbol{J}}_j+1 \leq q_j \leq p_j(\cA^{\boldsymbol{J}})} \left|\sigma_j^{q_j}(\cA^{\boldsymbol{J}})\right|^2 \geq \varepsilon^2$}

\State Select $1\leq j_0 \leq d$ and $\boldsymbol{J}_0\in \mathcal P$ such that
$$
(j_0,\boldsymbol{J}_0) = \mathop{\rm{argmax}}_{1\leq j \leq d, \; \boldsymbol{J}\in \mathcal P} \sigma_j^{R^{\boldsymbol{J}}_j+1}(\cA^{\boldsymbol{J}}).
$$

\If{$\boldsymbol{R}^{\boldsymbol{J}_0} = (0,\cdots,0)$}

\State Set $\boldsymbol{R}^{\boldsymbol{J}_0}:=(1,\cdots,1)$

\Else

\State Assume that $\boldsymbol{R}^{\boldsymbol{J}_0} = (R_1^{\boldsymbol{J}_0}, \cdots, R_d^{\boldsymbol{J}_0})$.

\State $R^{\boldsymbol{J}_0}_{j_0} \gets R^{\boldsymbol{J}_0}_{j_0}+1$.

\EndIf

\EndWhile

\State Define $\varepsilon^{\boldsymbol{J}}:= \sqrt{\sum_{1\leq j \leq d} \sum_{R^{\boldsymbol{J}}_j+1 \leq q_j \leq p_j(\cA^{\boldsymbol{J}})} \left|\sigma_j^{q_j}(\cA^{\boldsymbol{J}})\right|^2 }$ for all $\boldsymbol{J}\in \mathcal P$.

\Return $\left(\boldsymbol{R}^{\boldsymbol{J}}\right)_{\boldsymbol{J}\in \mathcal P}$ and $\left(\varepsilon^{\boldsymbol{J}}\right)_{\boldsymbol{J}\in \mathcal P}$
%\EndProcedure
\end{algorithmic}
\end{algorithm}
