import logging
import sys
import time

import numpy as np
import itertools as it
import matplotlib.pyplot as plt
import os.path

from util import *

class GlobalGreedyRes:
    def __init__(self, ns, epss, block_sz, nparts, input_eps):
        self.ns = ns
        self.epss = epss
        self.block_sz = block_sz
        self.storage = np.abs(ns).sum()

        self.nparts = nparts
        self.input_eps = input_eps

class HierarchicalRes:
    def __init__(self, storage, nss):
        self.storage = storage
        self.nss = nss

        self.nparts = None
        self.input_eps = None

    def plot_structure(self, filename):
        import pygraphviz as pgv
        G = pgv.AGraph()
        G.node_attr['shape'] = 'point'

        for i in range(1, len(self.nss)):
            nss = self.nss
            for idx in gen_all_index(nss[i].shape):
                parent = parent_of(idx)
                if nss[i-1][parent] < 0:
                    G.add_edge((i-1, *parent), (i, *idx))

        G.layout(prog='dot')
        G.draw(filename)


class Tensor:
    def __init__(self, ts):
        self.ts = ts

    def global_greedy(self, eps, nparts):
        block_sz = block_sz_for(self.ts.shape, nparts)
    
        sigss = np.zeros((*nparts, 3, max(block_sz)))
        for small_ts_ind in gen_all_index(nparts):
            small_ts = block_at(self.ts, small_ts_ind, block_sz)
            for i, view in enumerate(matrix_views(small_ts)):
                _, sig, _ = np.linalg.svd(view.dot(view.T))
                sigss[(*small_ts_ind, i, slice(0, len(sig)))] = sig
    
        ress = np.zeros((*nparts, 3))
    
        cur_eps = sigss.sum()
    
        next_ind = np.zeros((*nparts, 3), dtype=int)
        leadings = sigss[...,0].copy()
        while cur_eps > eps:
            ind = np.unravel_index(leadings.argmax(), leadings.shape)
            ress[ind] += 1
            if leadings[ind] < 1e-10: break
            cur_eps -= leadings[ind]
    
            try:
                sigss[(*ind, next_ind[ind])] = 0
                leadings[ind] = sigss[(*ind, next_ind[ind]+1)]
            except IndexError:
                leadings[ind] = 0
            next_ind[ind] += 1
    
        ns = np.apply_along_axis(lambda x: compute_storage(x, block_sz), -1, ress)
        storage = ns.sum()
    
        # should normalize epss, it's generally smaller than eps
        epss = sigss.reshape(*nparts, -1).sum(-1)
        total_epss = epss.sum()
        if total_epss > 1e-4:
            epss *= eps/total_epss

        if nparts != (1,1,1): logging.info(f"Global Greedy for {nparts}: {storage}")
    
        return GlobalGreedyRes(ns=ns, epss=epss, block_sz=block_sz, input_eps=eps, nparts=nparts)

    def rank_adaptive_tucker(self, eps):
        storage = self.global_greedy(eps, (1,1,1)).storage
        logging.info(f"Rank Adaptive Tucker: {storage}")
        return storage

    def _hierarchical_merge_impl(self, ns, epss, block_sz, nss):
        if all(s == 1 for s in ns.shape):
            # already at top level, finish
            return HierarchicalRes(np.abs(ns).sum(), list(reversed(nss)))

        new_ns = np.empty(tuple(s//2 or 1 for s in ns.shape))
        new_epss = np.empty(new_ns.shape)
        new_block_sz = tuple(2*b if s != 1 else b for b, s in zip(block_sz, ns.shape))
        for ijk in gen_all_index(new_ns.shape): 
            new_epss[ijk] = sum(epss[child] for child in children_at(ijk, ns.shape))
            old_sum = sum(abs(ns[child]) for child in children_at(ijk, ns.shape))
            if any(ns[child] < 0 for child in children_at(ijk, ns.shape)):
                # ns[i] < 0 indicates already split at this level, abs(ns[i]) is the storage
                new_ns[ijk] = -old_sum
                logging.debug(f"{ijk} @{new_ns.shape}: Already Rejected!!")
            else:
                # compute direct SVD for this block
                this_block = block_at(self.ts, ijk, new_block_sz)
                direct_res = Tensor(this_block).global_greedy(new_epss[ijk], (1,1,1)).storage
                if direct_res >= old_sum:
                    logging.debug(f"{ijk} @{new_ns.shape}: Reject!!")
                    new_ns[ijk] = -old_sum
                else:
                    logging.debug(f"{ijk} @{new_ns.shape}: Merge!!")
                    new_ns[ijk] = direct_res
    
        # this level finished, now next level
        nss.append(new_ns)
        return self._hierarchical_merge_impl(new_ns, new_epss, new_block_sz, nss)

    def hierarchical_merge(self, eps, nparts):
        res = self.global_greedy(eps, nparts)
        final_res = self._hierarchical_merge_impl(res.ns, res.epss, res.block_sz, [res.ns])
        final_res.nparts = nparts
        final_res.input_eps = eps
        logging.info(f'Hierarchical: {final_res.storage}')
        return final_res


class TensorBuilder:
    def build(self):
        return Tensor(self.build_impl())

class TensorBuilderSizable(TensorBuilder):
    def __init__(self):
        super().__init__()
        self._xyz = None

    def set_edge_size(self, *xyz):
        if len(xyz) == 1:
            xyz = (xyz[0],)*3
        
        self._xyz = xyz
        return self

class TensorBuilderFromFile(TensorBuilderSizable):
    def __init__(self, filename):
        super().__init__()
        self._orig = np.load(filename)

    def build_impl(self):
        return self._orig[tuple(slice(0, e) for e in self._xyz)] if self._xyz else self._orig

class TensorBuilderFromFunction(TensorBuilderSizable):
    def __init__(self):
        super().__init__()
        self._edge_sizes = None
        self._func_ranges = None

    def set_func_range(self, *func_ranges):
        if len(func_ranges) == 1:
            func_ranges = (func_ranges[0],)*3

        self._func_ranges = func_ranges
        return self

    def build_impl(self):
        xxyyzz = tuple(np.linspace(st, ed, edge_sz)
                for (st, ed), edge_sz in zip(self._func_ranges, self._xyz))

        return self.func(*np.meshgrid(*xxyyzz))


class TensorBuilderNewton(TensorBuilderFromFunction):
    @staticmethod 
    def func(x, y, z):
        small_eps = 0.1
        return ( 1/(np.abs(x-y) + small_eps)
               + 1/(np.abs(y-z) + small_eps)
               + 1/(np.abs(x-z) + small_eps)
               )


class TensorBuilderExp(TensorBuilderFromFunction):
    @staticmethod
    def func(x, y, z):
        return np.exp( -250*((x-y)**2+(y-z)**2+(z-x)**2) )
        #return np.exp( -520*((x-y)**2+(y-z)**2+(z-x)**2) )

def sizable_plot_result(sizable_builder, rate=0.001):
    l = [32, 48, 64, 96, 128, 160, 192, 256, 320] #, 352, 416, 512, 640] 
    nparts = [4, 8, 16]

    res = np.empty((len(l), len(nparts) + 1))

    for i, il in enumerate(l):
        ts = sizable_builder.set_edge_size(il).build()
        ts_norm = (ts.ts**2).sum()
        eps = ts_norm * rate**2

        # hierarchical
        for j, jnparts in enumerate(nparts):
            res[i, j] = ts.hierarchical_merge(eps, (jnparts,)*3).storage

        # rank adaptive
        res[i, -1] = ts.rank_adaptive_tucker(eps)
        
    for j, jnparts in enumerate(nparts):
        plt.plot(l, res[:, j], label=f'Hierarchical-{jnparts}x{jnparts}x{jnparts}')

    plt.plot(l, res[:, -1], label='Rank Adaptive')
    plt.plot(l, [e*e*e for e in l], label='Original')

    plt.legend() 
    plt.xlabel('Cubic Tensor Size')
    plt.ylabel('Storage')
    plt.savefig(f'{sizable_builder.__class__.__name__}.png')
    plt.clf()

def show_structure(builder):
    ts = builder.build()
    ts_norm = (ts.ts**2).sum()
    logging.info(f'shape:{ts.ts.shape},norm:{ts_norm}')
    rate = .001
    eps = rate**2 * ts_norm
    res = ts.hierarchical_merge(eps, (16,)*3)
    #ts.rank_adaptive_tucker(eps)
    res.plot_structure('hierarchical_structure.png')

def main():
    sizable_plot_result(TensorBuilderNewton().set_func_range((0,1)))
    sizable_plot_result(TensorBuilderExp().set_func_range((0,1)))
    #sizable_plot_result(TensorBuilderFromFile('vid_tensor.data.npy'))
    #show_structure(TensorBuilderFromFile('sim_tensor_total.npy'))

    #show_structure(TensorBuilderNewton().set_func_range((0,1)))

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    #logging.basicConfig(level=logging.DEBUG)
    main()
