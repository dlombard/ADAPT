import time 
import logging
import itertools as it

class timing_cxt:
    def __init__(self, title="No Title"):
        self.title = title

    def __enter__(self):
        print(f"====== NOW {self.title} ======")
        self.stored_time = time.time()

    def __exit__(self, t, v, tb):
        logging.info("time: {}".format(time.time() - self.stored_time))


def plot_sigs(ts):
    for view in matrix_views(ts):
        print("PLOT")
        _, sigs, _ = np.linalg.svd(view.dot(view.T))
        print(sigs)
        plt.plot(sigs)
        plt.show()

def matrix_views(ts):
    shape = ts.shape
    ndims = len(shape)
    for where in range(ndims):
        yield ( ts
                .swapaxes(0, where)
                .reshape(shape[where], ts.size//shape[where])
              )

def parent_of(ijk):
    return tuple(e//2 for e in ijk)

def children_at(ijk, shape):
    # n -> (2n, 2n+1)
    for u in range(2**len(ijk)):
        #yield tuple(2*r+bool(u&(1<<v)) for v, r in enumerate(ijk))
        tp = tuple(2*r+bool(u&(1<<v)) for v, r in enumerate(ijk))
        if all(tp[i] < shape[i] for i in range(len(shape))):
            yield tp

def gen_all_index(tp):
    for r in it.product(*[range(s) for s in tp]):
        yield r

def block_at(ts, ijk, block_sz):
    return ts[ tuple(slice(ijk[i]*block_sz[i], (ijk[i]+1)*block_sz[i]) for i in range(len(ijk))) ]

def block_sz_for(shape, nparts):
    return tuple(shape[i]//nparts[i] for i in range(len(nparts)))


def compute_storage(x, block_sz):
    return x[0]*x[1]*x[2] + sum(x[i]*block_sz[i] for i in range(len(x)))
