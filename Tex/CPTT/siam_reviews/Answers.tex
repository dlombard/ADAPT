\documentclass[a4paper,11pt]{article}
 \usepackage[utf8]{inputenc}
 \usepackage[a4paper, total={6in, 8in}]{geometry}
 
 
 \begin{document}



First of all, we thank the referees for all the comments and suggestions made, which helped in significantly improving the quality of the manuscript. 

In particular, the different remarks of the referees concerning the novelty, made us consider an extension (with respect to the first submission) to a greedy construction of an approximation of a given tensor as a sum of Tensor Trains, for which neither the order of variables nor the ranks are fixed a priori. CP-TT can be seen as the particularization to a fixed rank-1 version of this more general method. Numerous modifications are proposed in the novel version of the manuscript. Please, find hereafter a detailed answer to the questions raised. 

\medskip

\textbf{Answer to referee 1:}

Summary: \textit{The authors propose an iterative TT-style algorithm (CP-TT) to greedily construct the rank-1 components of the CP decomposition for function approximation. The idea of computing the loading vectors in a rank-1 component is simple: CP-TT iteratively chooses a loading vector as the dominating left singular vector of the matrix with largest spectral norm among several matricizations of the dominating right singular vector in the previous sub-iteration. CP-TT is also extended to extract rank-k components. Numerical simulations show that the proposed algorithm outperforms ALS and ASVD in accuracy when the input dimension of a function is sufficiently large.
Overall, this paper is moderately well written with limited novelty and technical contribution. I have the following comments regarding the novelty and technical contributions, the efficiency of the algorithm, and the presentation.}
\medskip

\textbullet \ Comment 1 (Novelty): 
\textit{The novelty of the proposed algorithm seems a bit limited regarding TTr1 [R1]. The proposed method can be viewed as an incremental modification of TTr1 which uses TT to compute the rank-1 components. It is suggested to sufficiently discuss the differences and improvements upon TTr1 (better in both theory and numerical study). 
[R1] Kim Batselier, Haotian Liu, and Ngai Wong. A constructive algorithm for decomposing a tensor into a finite sum of orthonormal rank-1 terms. SIAM Journal on Matrix Analysis and Applications 2015 36:3, 1315-1337}

\vspace{0.2cm}

We thank the referee for having suggested this reference. The TTr1 method is indeed the most similar in the spirit to the CP-TT method proposed in this work. There are two main differences: in CP-TT the order of variables is not fixed a priori but it is the result of an optimization step; moreover, in TTr1 we compute all (or a large number of) the rank-1 terms and then we truncate whereas in CP-TT we compute one term at a time.  This has been pointed out in Section 3. Some numerical tests have been done and commented in Section 4 on random functions of 4 variables. The results are quite similar when compared to the CP-TT ones in terms of accuracy. In terms of computational cost, the CP-TT iteration requires a larger number of operations but a smaller amount of memory. 

Concerning the novelty, we considered in the revised version of the manuscript an extension consisting in defining a sum of TT approximation and we can see CP-TT as a particular case of this more general approach. In the general SoTT (Sum of TT) approach, we do not fix a priori the ranks nor the order of the variables. We provided a proof of the exponential convergence of SoTT in finite dimension. This result is also valid for the CP-TT case.  

\medskip

\textbullet \ Comment 2 (Technical contribution in the analysis of CP-TT):
\textit{Considering the similarity to TTr1, the analysis of properties of CP-TT seems a bit straightforward. The orthogonal properties simply hold due to the POD decomposition. The optimality in the special case for d=2 and the rank-1 case seems a little obvious. The stability of rank-k extension is also a direct result of rank-k POD. Therefore, there seems no interesting points for me in the analysis. So, it is suggested to highlight the technical insights or interesting points in the analysis. }

\vspace{0.2cm}

The properties highlighted in the first version of the manuscript have been removed. In terms of analysis, we added a proof of convergence for the SoTT algorithm, which can be extended to the CP-TT case.

\medskip

\textbullet \ Comment 3 (Efficiency of the proposed algorithm):
\textit{The numerical performance of CP-TT is encouraging in accuracy as compared with ALS and ASVD. However, there are no tables/figures supporting the efficiency of CP-TT. Since the one iteration cost of CP-TT can be significantly heavier than ALS, it is suggested to evaluate the efficiency of CP-TT through simulation study.}

\vspace{0.2cm}

We agree with the referee that one single CP-TT iteration is significantly heavier than the ALS one. However, when we consider a higher order tensor the slow convergence rate of ALS and ASVD make CP-TT competitive or comparable also in terms of computational cost. To asses this we added plots of the computational cost in Section 4.

\medskip


\textbullet \ Comment 4 (The presentation): 
\textit{Although this paper is moderately well written, there is still much room for improvement of the presentation.}

\vspace{0.2cm}

We substantially review the presentation. Concerning the remarks, whenever they are still present in the updated version of the manuscript, we took them into account. 


\vspace{0.75cm}

\textbf{Answer to referee 2:}

\medskip

\textit{This manuscript presents the so-called CP-TT algorithm of the type truncated HOSVD to compute the rank-1 approximation of a given tensor. This algorithm is served for application in the framework of the commonly used greedy method for the construction of the low-rank CP tensor decomposition, based on the sequential rank-1 update of the accumulated residual. The most computationally expensive part of the algorithm (as in the truncated HOSVD methods) is the SVD of the directional unfolding matrices for target accumulated residual tensor.}

\textit{Numerical tests illustrate the comparative convergence features of the greedy method (with three different rank-1 update schemes) applied to the “smooth” input tensor given in the ranks $\leq 6$ orthogonal Tucker format with randomly chosen core coefficient tensor. The Tucker orthogonal vectors are represented by first six Fourier harmonics. The presented numerical results indicate that the accuracy of the order of several percents is achieved with the canonical ranks about 100. All chosen rank-1 update schemes demonstrate the very similar performance in the framework of the simple greedy method applied to the particular “smooth” input data.}

\textit{This contribution to the involved topic on tensor decomposition techniques deserves encouragement. Indeed, any attempt to facilitate the construction of numerical low-rank CP decomposition of higher order tensors may lead to better understanding of the challenge of canonical format. Unfortunately, many of the algorithmic constructions are described in a rather vague way so that the main contribution of the paper remains unclear for the potential readers. It seems that the novelty of the computational scheme presented in the paper is the rank-1 approximation algorithm of a tensor, which can be viewed as the particular case of the standard ALS Tucker algorithm with rank-1 truncation. The main difference is not specified. Moreover, the list of references is not complete, several closely related to the topic publications are missing.}

\vspace{0.2cm}

We agree with the referee and we substantially revised the work. In the new version we proposed a more general scheme, whose fixed rank-1 version is the CP-TT method. We highlighted several properties from an analytical and algorithmic point of view for this new method.

\medskip


\textbullet \ Comment 1: 
\textit{The numerical complexity of the rank-1 update by CP-TT algorithm applied to the full format $n\otimes d$ tensor has not been addressed. It is important to compare it with the cost of the truncated HOSVD method in [6] based on the truncated SVD for rank-1 approximation of matrix unfoldings. The relation and main difference from the HOSVD method should be discussed already in Introduction (currently the reference on [6] appears only in small remark in page 15). An explicit description of input/output data formats in the CP-TT algorithms for rank-1 approximation is recommended, as well as the estimate on computational complexity of the dominating steps.}

\vspace{0.2cm}

In the new version of the manuscript we added a discussion on the computational complexity in the case in which the input tensor is in full format. Moreover, we added an example of approximation  in Section 4. The cost in terms of number of operations is indeed similar to the one of the HOSVD, as we need to compute the truncated SVD for all the tensor unfoldings. The main difference is in terms of the memory used. Indeed, as we evaluate the best unfolding according to the criterion detailed in Section 3, we do not keep in memory the SVD decompositions of unfoldings which are not the optimal one. Moreover, we do not compute nor store the potentially dense core of the HOSVD, but after the modes of the best unfolding are computed we project on them (as it is done in TT-SVD) and we proceed. 


\medskip

\textbullet \ Comment 2: 
\textit{There are only few lines in Introduction concerning the main results and ideas presented in the paper (seven lines in the bottom of page 2). One may expect the discussion 
on the potential advantages of the presented scheme as well as its comparison with the existing algorithms of rank-1 approximation. 
The extensive discussions on the standard notion of “curse of dimensionality” may be reduced, while the description of tensor formats used in the paper can be included in sections.}

\vspace{0.2cm}

The introduction has been significantly modified. The presentation of the CP and TT format has been moven to Section 2.2. Moreover, since we propose now a more general algorithm which constructs a greedy approximation of a tensor as a sum of TTs, 
we present the main advantages of this approach in the introduction. 


\medskip

\textbullet \ Comment 3:
\textit{A multivariate function is not a tensor. A tensor is a multidimensional data array (also when given in the parametrized rank-structured formats) with given entries.
Introducing a “continuous tensor” is questionable and needs additional affirmation. It is not clear why rather standard multilinear algebra is substituted by functional operations, 
which in computational schemes should be again converted (by means of discretization) to the commonly used algebraic form? This way of representation does not help in the reading and 
understanding of the standard notions and definitions of multilinear algebra which have been used since decades.}

\medskip

We are motivated in this work by approximation methods of multivariate functions, solutions of parametric PDEs for instance, and tensors naturally appear as particular discretizations of these multivariate functions. In particular, we try, as much as possible,
to develop methods which could be used in principle for the approximation of such functions independently of the discretization scheme used. This is the reason why we prefer to keep the presentation of the method for ``tensors'', which are defined as real-valued 
applications defined on a cartesian product domain of the form $\Omega_1 \times \cdots \times \Omega_d$. In the case when $\Omega_1, \cdots, \Omega_d$ are discrete sets, then the definition becomes equivalent to the one of a multidimensional data array. 
We have nevertheless indicated in the article that this is indeed an abuse of language.  

\medskip

\textbullet \ Comment 4:
\textit{It is not clear why the presented rank-1 update algorithm is called CP-TT since the main computational steps are basically the same as in the truncated rank-1 HOSVD algorithm, while in the 
description of the scheme the relation to the TT format was not specified at all. Indeed, throughout the paper the relation to the typical TT arithmetic was not explicitly indicated, except the definition 
of the TT format in page 2, and reference to the so-called TT-SVD algorithm (page 2) that was not defined.}
\medskip

We have added a Section which presents the classical TT-SVD algorithm (Procedure~2.1) and highlight more strongly the links between this method and the SoTT algorithm presented in Procedure~3.1). We hope that the links between the two approaches are thus more clearly 
indicated in this new version. 

\medskip

\textbullet \ Comment 5:
\textit{The definition of the POD decomposition is missing (i.e. specification of the input and output of POD). How does it correlate with the well-known truncated HOSVD in the case of rank-1 approximation.}

\vspace{0.2cm}

We added a section with the definition of the POD/SVD decomposition in Section 2.1.

\medskip 

\textbullet \ Comment 6:
\textit{Notice that the detailed convergence theory for the best rank-1 approximation was presented in earlier paper
T. Zhang and G. Golub. Rank-0ne approximation to high order tensors. SIAM J. Matrix Anal. Appl. v. 23 (2001) 534-550.
This basic reference is missing. }

\vspace{0.2cm}

We thank  the referee for the reference, we added it. 

\medskip

\textbullet \ Comment 7:
\textit{For the definition of CP format it would be better to refer to the original paper(s) by F.L. Hitchcock but not to the survey [16].} 

\vspace{0.2cm}

We added this reference. \\


\medskip

\textbullet \ Comment 8:
\textit{Basic analysis of the greedy type algorithms was presented in many papers by V. Temlyakov. The respective citations are recommended. For example,
V.N. Temlyakov. Greedy Algorithms and M-Term Approximation with Regard to Redundant Dictionaries. J. of Approx. Theory, 98 (1999), 117-145.}

\vspace{0.2cm}

We added this reference. 

\medskip

\textbullet \ Comment 9:
\textit{It is not correct to write “we introduce ... ASVD method .. which was proposed in [12]” (page 4, bottom).}

\vspace{0.2cm}

This has been changed in the novel version of the manuscript. 

\medskip

\textbullet \ Comment 10:
\textit{Page 5: “The idea behind this method of tensor approximation is to combine the CP format and the TT-SVD”. So far, the TT-SVD for the rank-1 approximation is not defined or discussed anywhere in the paper. In the context of this paper, the SVD decomposition is related to the standard rank-1 approximation of tensors, hence, it is not clear why it is designated as a special algorithm like CP-SVD. Indeed, rank-1 tensors belong to all standard tensor formats including CP, Tucker, TT, etc. Actually combination with the CP format is realized in the paper by means of greedy algorithm for computing rank-1 updates.}

\vspace{0.2cm}

In the novel version of the manuscript, the TT-SVD is defined in the paper and a more general method is introduced based on it. 

\medskip

\textbullet \ Comment 11:
\textit{Page 15: it is not correct to write “... The function to be compressed is given in CP format: (4.1) ” for the expression (4.1), which is of the Tucker-type form.}

\vspace{0.2cm}

This has been corrected in the novel version of the manuscript. 

\medskip

\textbullet \ Comment 12:
\textit{The rank-1 update in numerics applies to the rank $\leq$ 6 Tucker input tensor, which can be viewed as the special CP tensor with large canonical rank $\leq 6d-1$. Notice that the fast algorithms for the rank truncation in the canonical input tensors with large CP-ranks and large mode size, called reduced HOSVD (RHOSVD), as well as the related error estimates, have been introduced and analyzed in
B. N. Khoromskij and V. Khoromskaia. Multigrid Tensor Approximation of Function Related Arrays. SIAM J. Sci. Comp., 31(4), 3002-3026 (2009).}

\vspace{0.2cm}

We thank the referee for having pointed out this references, we added it to the manuscript. 

\medskip

\textbullet \ Comment 13:
\textit{The presented algorithms are served for the full format tensors, however, in numerical examples the input tensor in (4.1) is given in the special rank $\leq$ 6 orthogonal Tucker tensor format. This input is composed of partial Fourier sum with the first 6 harmonics. It means that independently on the normalized random coefficient, it represents the very smooth tensor which in general is well suited for the low-rank CP approximation. In this concern testing tensors with less regular structure and presented in the full format might be illustrative. Please comment.}

\vspace{0.2cm}

We agree with the referee. We added a test in which we considered a full tensor of order 4 in which we store the approximation of a parametric PDE. In the last section we compare the compression provided by TT-SVD when we consider all the possible permutations of the variables, SoTT and CP-TT which can be seen as a particular case of SoTT.

\medskip 

\textbullet \ Comment 14:
\textit{The rank-R CP approximation of the orthogonal Tucker tensor (which is the case for the presented numerical example) is equivalent to the rank-R canonical approximation of the small size coefficients tensor. Hence, in this case the successive rank-1 approximation should be applied directly to the core tensor. For further details see Lemma 2.5 and Corollary 2.6 in
B. N. Khoromskij and V. Khoromskaia. Low Rank Tucker-Type Tensor Approximation to Classical Potentials. Central European J. of Math., 5(3), pp.523-550, 2007.}

\vspace{0.2cm}

We added a comment and the reference. 

\medskip

\textbullet \ Comment 15:
\textit{The entries of the Tucker core in (4.1) are chosen to decay in the amplitudes. Please comment what is the reason for this choice in the view of tensor decomposition?}

\vspace{0.2cm}

We considered decaying amplitudes to mimic situations in which the functions exhibit low-frequency oscillations but not large high-frequency oscillations. This was more motivated by the applications we have in mind than tensor decomposition per se. 

\medskip

\textbullet \ Comment 16:
\textit{Figures look rather messy. Averaging over random samples for the given method may provide better pictures for comparison. In general, figures need improvement: logarithmic scales for the presented curves could better demonstrate the convergence history. Moreover, the small symbols should be made readable.}

\vspace{0.2cm}

We changed all the figures accordingly. The new plots have been added taking these remarks into account.

\medskip 

\textbullet \ Comment 17:
\textit{As a general remark, I notice that a more relevant literature surveys on tensor decompositions could be included. Indeed, the authors confine themselves only by a short survey [14], however, the recent monographs on tensor methods present more extended list of references on tensor decomposition techniques, describe the basic theory and outline the wide range of applications, see
W. Hackbusch. Tensor spaces and numerical tensor calculus. Springer, Berlin, 2012. and
Boris N. Khoromskij. Tensor Numerical Methods in Scientific Computing. De Gruyter Verlag, Berlin, 2018.}

\vspace{0.2cm}

We added these references. 


\vspace{0.75cm}

\textbf{Answer to referee 3:}

\medskip

\textit{This work proposes a TT-SVD-like algorithm to compute the low-rank CP expansion of a high-dimensional tensor, based on a sequence of projections on orthogonal components (akin to left singular vectors in TT-SVD, which operates on the discrete world). A part of the algorithm is just a reformatting of TT-SVD from the discrete to the continuous realms and is not novel; however, other parts (such as the variable ordering choice in terms of the largest first singular vectors, the insertion of TT-SVD in an incremental rank-1 update iteration) are original as far as I can tell. The arguments presented in the paper are mathematically sound, and the method could be in principle valuable. Unfortunately, this is currently not demonstrated convincingly enough in the evaluation, since I see the following major issues:}

\medskip

\textbullet \ Comment 1:
\textit{Although the method is motivated as being able to decompose a general tensor, the only experiments concern tensors that already have a known low-rank format (sometimes known as the "recompression" use case in the literature). While this is an interesting application, the case of general tensors is left in the air.}

\vspace{0.2cm}

In the novel version of the paper, we present a new algorithm, whose fixed rank-1 version is CP-TT. We have added a discussion on the convergence of the method as well as its computational complexity when the input tensor is in full format. Moreover, in Section 4 we have added some numerical experiments on the compression of a full tensor. In this section, we compare the novel method, namely SoTT with all the possible TT-SVD obtained by permuting the order of the indices, and we compare the general SoTT with the CP-TT method. 

\medskip

\textbullet \ Comment 2:
\textit{There is no comparison of running times or number or iterations/operations. }

\vspace{0.2cm}

We agree with the referee, this information was indeed missing. We have added a discussion on the computational complexity as well as some remarks concerning the comparison with other methods. Moreover, we have added some plots on the computational cost of the CP-TT method compared to ALS and ASVD. 

\medskip

\textbullet \ Comment 3:
\textit{All three algorithms considered in the paper work on incremental rank updates: a low-rank term is progressively added to an incremental solution until the residual is small. However, I would not say this is the most standard way of computing the CP decomposition. I would argue that the most widespread version of CP-ALS in the literature optimizes a full factor matrix at a time (Kolda and Bader 2009: "Tensor Decompositions andApplications"). The authors should at least compare against this (and possibly other methods, see Sorber et al. 2013: "Optimization-Based Algorithms for Tensor Decompositions: Canonical Polyadic Decomposition, Decomposition in Rank-$(L_r,L_r,1)$ Terms, and a New Generalization"). }

\vspace{0.2cm}

We have compared both ways of computing ALS, and we did not observe significant differences. We decided to keep the rank-1 version in order to compare progressive rank one updates. In the revised version of the manuscript, we have added a comparison with TTr1, which is the most similar method in the spirit to CP-TT. In the novel version of the work we have introduced a new method, whose fixed rank-1 version is CP-TT. We have added several other numerical tests. 


\medskip

\textbullet \ Comment 4:
\textit{A significant part of the algorithm consists of revisiting TT-SVD, just ported to the continuous realm. I am not sure how justified this discussion is, given that the tensors of interest in the experiments are discretized after all. In any case, if the continuous TT-SVD is to be kept, the paper should properly introduce the original, discrete TT-SVD algorithm so that the similarities are clear (and any differences should be explicitly highlighted!)}

\vspace{0.2cm}

In the novel version of the paper the TT-SVD is properly defined. We have decided to keep the notation in the continuous realm because we have in mind applications to function parsimonious representation for which this notation is more intuitive. 

\medskip

Concerning the other comments comments, the novel version of the manuscript has been substantially changed. Whenever the comments are related to sentences still present in the novel version, they were taken into account. The plots were completely renewed and the new plots added took the remarks into account. 


\end{document}