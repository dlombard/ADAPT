\documentclass[a4paper,11pt]{article}
\usepackage{epsfig}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bbold}
\usepackage{algorithm2e}
\usepackage{color}

\linespread{1.0}
\setlength{\topmargin}{0cm}
\setlength{\oddsidemargin}{0cm}
\setlength{\evensidemargin}{0cm}
\setlength{\textwidth}{16.0cm}
%\thispagestyle{empty}
\title{Notes about the bibliography suggested}
\date{}
\author{}

\newtheorem{mylemma}{}
\newtheorem*{lemNoNum}{}
\newtheorem{myprop}{}
\newtheorem{mytheo}{}
\newtheorem{mydef}{}
\newtheorem{myex}{}


\begin{document}
\maketitle
\section{ TTr1}
Similar method proposed by referee 1. The whole title: Kim Batselier, Haotian Liu, and Ngai Wong. A constructive algorithm for decomposing a tensor into a finite sum of orthonormal rank-1 terms. SIAM Journal on Matrix Analysis and Applications 2015 36:3, 1315-1337. \\


\textbf{Abstract:} We propose a constructive algorithm that decomposes an arbitrary real tensor into a finite sum of orthonormal rank-1 outer products. The algorithm, called TTr1SVD, works by converting the tensor into a tensor-train rank-1 (TTr1) series via the SVD. TTr1SVD naturally generalizes the SVD to the tensor regime with properties such as uniqueness for a fixed order of indices, orthogonal rank-1 outer product terms, and easy truncation error quantification. Using an outer product column table it also allows, for the first time, a complete characterization of all tensors orthogonal with the original tensor. We also derive a conversion of the TTr1 decomposition into a Tucker decomposition with a sparse core tensor. Numerical examples illustrate each of the favorable properties of the TTr1 decomposition.

\textbullet CP-ALS decomposition: Prescribing the rank $R$.\\

\textbullet Tucker decomposition computed efficiently with HOSVD.\\


The TTr1 decomposition reshapes and factorizes the tensor in a recursive way. One needs to progressively reshape and compute the SVD of each singular vector to produce the TTr1 decomposition. The resulting singular values are constructed into a tree structure whereby the product of each branch is the weight of one orthonormal (rank-1) outer product. Most of the main properties and contributions of the TTr1 decomposition are highly reminiscent of the matrix SVD:

\begin{itemize}
\item An arbitrary tensor is for a fixed order of the indices uniquely decomposed into a linear combination of orthonormal outer products, each associated with a nonnegative TTr1 singular value.
\item The approximation error of an R-term approximation is easily quantified in terms of the singular values.
\item Numerical stability of the algorithm is due to the use of consecutive SVDs.
\item It characterizes the orthogonal complement tensor space that contains all tensors whose inner product is 0 with the original tensor A. This orthogonal complement tensor space is, to our knowledge, new in the literature.
\item 5. It is straightforward to convert the TTr1 decomposition into the Tucker format with a sparse core tensor and orthogonal matrix factors.
\end{itemize}

Let us have the TT decomposition of a certain tensor $\mathcal{A}$:

$$\mathcal{A}_{i_1, \ldots, i_d}=  \mathcal{G}_1(i_1) \ldots \mathcal{G}_d(i_d)$$

where for a fixed $i_k$ each $\mathcal{G}_k (i_k)$ is an $r_{k-1} \times r_k$ matrix, also called the TT core. The TT ranks are $r_k$. Each core $\mathcal{G}_k$ is a third-order tensor with indices $\alpha_{k-1}, i_k, \alpha_k$ and dimensions $r_{k-1}, n_k, r_k$ respectively. We can rewrite:

$$\mathcal{A}_{i_1, \ldots, i_d}= \sum_{\alpha_1, \ldots, \alpha_{d-1}} \mathcal{G}_1(i_1, \alpha_1) \ldots \mathcal{G}_d(\alpha_{d-1}, i_d)$$

The main idea of the TTr1 decomposition is to force the rank for each auxiliary index $\alpha_k$ link to unity, which gives rise to a linear combination of rank-1 outer products. 

Let $\mathcal{A}$ be a third order tensor. The first reshaping of $\mathcal{A}$ into a matrix $\bar{\mathcal{A}}$ that needs to be considered is by grouping the indices $i_2, i_3$ together. We can realize that we can rewrite the SVD of $\bar{\mathcal{A}}$ as a sum of rank-1 terms:

$$\bar{\mathcal{A}} = \sum^{r_1}_{i=1} \sigma_i u_i v_i$$


where $r_1 = min(\mathcal{N}_1, \mathcal{N}_2 \times \mathcal{N}_3)$ and each vector $v_i$ is indexed by $i_2, i_3$. The next step is to  reshape each $v_i$ into an $i_2 \times i_3$ matrix $\bar{ v_i}$ and compute its SVD. This allows us to write it as a sum of rank-1 terms:

$$\bar{v_1} = \sigma_{11}u_{11} v_{11} + \sigma_{12} u_{12} v_{12}$$

The same procedure can be done for $v_2$ and $v_3$. Combining these six rank-1 terms we can finally write $\mathcal{A}$ as:

$$\mathcal{A} = \tilde{\sigma_1}u_1 u_{11} v_{11} + \tilde{\sigma_2}u_1 u_{12} v_{12} 
+ \tilde{\sigma_3}u_2 u_{21} v_{21} + \tilde{\sigma_4}u_2 u_{22} v_{22} 
+ \tilde{\sigma_5}u_3 u_{31} v_{31} + \tilde{\sigma_6}u_3 u_{32} v_{32}$$

With $\tilde{\sigma_1} = \sigma_1 \sigma_{11}, \ldots, \tilde{\sigma_6} = \sigma_3 \sigma_{32}$. The TTr1 decomposition has three main features that render it similar to the matrix SVD:
\begin{itemize}
\item the scalars $\tilde{\sigma_1}, \ldots, \tilde{\sigma_6}$ are the weights of the outer products in the decomposition and can therefore be thought of as the singular values of $\mathcal{A}$.
\item the outer products affiliated with each singular value are tensors of unit Frobenius norm.
\item each outer product in the decomposition is orthogonal to all the others.
\end{itemize}


Computing the TTr1 decomposition requires recursively reshaping the obtained $v$ vectors and computing their SVDs. This recursive procedure gives rise to the formation of a tree, where each SVD generates additional branches of the tree. The number of levels for the TTr1 decomposition of an arbitrary d-way tensor is $d-1$. The final singular values $\tilde{\sigma}'s$ are the product of all $\sigma$'s along a branch.\\

\begin{algorithm}[H]
\SetAlgoLined
\KwResult{$U, S, V$ matrices of each SVD}
 Arbitrary tensor $\mathcal{A}$\;
	$\bar{\mathcal{A}} \leftarrow $ reshape $\mathcal{A}$ into its unfolding\;
  	Do the SVD of $\bar{\mathcal{A}}$ and get $U_1, S_1, V_1$\;
  \For{all remaining nodes in the tree except the leaves ($dim(unfolding) > 2$)}{
   	$\bar{v_i} \leftarrow$ reshape $v_i$\;
     	Do the SVD of $\bar{v_i}$ and get $U_k, S_k, V_k$\;
	add $U_k, S_k, V_k$  to $U,S,V$
      }
 
 \caption{Algorithm of TTr1SVD}
\end{algorithm}


Properties: \\

\textbullet The uniqueness of the TTr1 decomposition follows trivially from the fact that each of the SVDs in Algorithm 1 is unique up to sign.\\

\textbullet  Any two rank-1 terms of the TTr1 decomposition are orthogonal with respect to one another. \\

\textbullet The rank of an arbitrary d-way tensor $\mathcal{A}$, denoted $rank(\mathcal{A})$, is the minimum number of rank-1 tensors that yields $\mathcal{A}$ in a linear combination.
The orthogonal rank, $rank_{\perp}(\mathcal{A})$, is defined as the minimal number of terms in an orthogonal rank-1 decomposition and
$$rank(\mathcal{A}) \leq rank_{\perp}(\mathcal{A})$$
The TTr1 decomposition allows a determination of an upper bound on $rank_{\perp}(\mathcal{A})$. Indeed, this is simply the total number of leaves in the tree $N$, $N = \prod_{k=0}^{d-2} r_k$. For a cubical tensor with $n_1 = \ldots = n_d = n$:

$$rank_{\perp}(\mathcal{A}) \leq \prod_{k=0}^{d-2} n = n^{d-1}$$
The dependency on the ordering of the indices implies that a permutation of the indices can lead to different upper bounds on the orthogonal rank. Computing the upper bounds through all permutation of indices, we can obtain a notion of a minimum upper bound.\\

Let $\mathcal{A}$ be decomposed by TTr1SVD into a linear combination of N orthogonal rank-1 terms:

$$\mathcal{A} = \sum^N_{i =1}\tilde{\sigma_i}u_{1i}u_{2i} \ldots u_{d-1 i} v_i$$

And Let us suppose that $\tilde{\sigma_1} \geq \tilde{\sigma_2} \geq \ldots \geq \tilde{\sigma_N}$.\\

\textbullet An R-term approximation is then computed by truncating the equation above to the first R terms.

$$\tilde{\mathcal{A}} = \sum^R_{i =1}\tilde{\sigma_i}u_{1i}u_{2i} \ldots u_{d-1 i} v_i$$

where: $\| \mathcal{A} - \tilde{\mathcal{A}}\| = \sqrt{\tilde{\sigma_{R+1}^2} + \ldots \tilde{\sigma_N}^2} $.\\

\textbullet It is possible to reduce the total number of required SVDs by taking into account that the final singular values are
the product of the singular values along each branch of the TTr1-tree. This allows us to make a guess about the impact of the singular values at each level on the final rank-1 terms with a certain error. \\

\textbullet The TTr1 decomposition allows us to easily find an orthogonal basis for $opspan(\mathcal{A})^{\perp}$. It is easily seen that the rank-1 terms obtained from the zero entries of the S matrix in the SVD  are orthogonal to $\mathcal{A}$ and are therefore basis vectors of $opspan(\mathcal{A})^{\perp}$. \\

\textbullet After doing the SVD of the tensor and obtaining the modes, we can set a scalar that scales a certain column. The scalar is mobile across various modes. If any two columns differing in only one mode can be added to form a new rank-1 outer product. With these assumptions, we can merge some modes and discard others in terms of obtaining the maximum rank-3 for a $2 \times 2 \times 2$ tensor.\\

\textbullet When an $m\times n$ matrix $A$ is additively perturbed by a matrix $E$ to form $\hat{A} = A + E$, then Weyl’s theorem bounds the absolute perturbations of the corresponding singular values by $| \sigma_i - \hat{\sigma_i}| \leq \| E \|$. We can extend this theorem to the TTr1 decomposition of the perturbed tensor, $\hat{\mathcal{A}} = \mathcal{A} + \mathcal{E}$. We arrive to: 
$$| \sigma_i - \hat{\sigma_i} | \leq \| \mathcal{E} \| + \sigma_1 \left( \sum_{k=1}^{d-3} \|\Delta v_k \| \right)$$

\textbullet Some numerical experiments\.\

\subsection{Conlusions}

The main scheme of the method is very similar to what we do in CPTT. The difference is that they have to select the order of the indices a priori (as in TT) and that the order of them matters. The operations comparing the permutations of the indices increase the cost. \\

They don't select the R terms by picking the largest singular value each time. Instead of that, they compute every term (reordering the tensor) and then they compare the sigmas and select the first R terms.\\

They don't say it explicitly but I think that they compute the whole unfolding, what we have seen it was also very costly.\\

The last thing is that they don't grow the dimensions. Most of the examples and test are for 3d tensors.\\


\section{CP-ALS}
The computation of the ALS method from Kolda and Bader 2009: "Tensor Decompositions and Applications", suggested by referee 3.\\

Pages 28-29 of the paper. Pseudocode included.\\


The main difference in the way that we have computed the ALS and this way is that they do it in a matrix way, considering the $d$ factor matrices:

$$A^{(1)} = [u_1^{(1)}, \ldots, u_R^{(1)}]$$
$$ \ldots $$
$$A^{(d)} = [u_1^{(d)}, \ldots, u_R^{(d)}]$$

Taken from the CP decomposition of a tensor:
$$F = [\lambda, A^{(1)} \ldots A^{(d)}]$$

With these matrices, they compute the fixed point iteration in a similar way,  finding the $\lambda, A^{(1)} \ldots A^{(d)}$ that solve the minimization problem. In this case, we don't depend on the "terms" variable because working with matrices we're already taking them. We only depend on the variables (the superindex of the A's). \\


Instead of that, we take the terms inside these matrices, and the fixed point iterations depends on the variables and also in the terms. First we choose randomly the values for the modes for the term zero. Then, we fix a term and find the mode that minimizes the residual corresponding to every variable and we compare them with the previous term and we see if the terms have changed or not, depending on that we stoped or change the term.\\

Let us remark that, with the method showed in Kolda and Bader, we obtain directly the entries $\lambda, A^{(1)} \ldots A^{(d)}$ of the approximated tensor and with the method showed in CP-TT we obtain the corresponding rank-1 approximation.

\section{TT-SVD}
Introduce the method and show the differences and similarities with it is suggested by referee 2 and 3. \\

The introduction of the method is taken from: "A Randomized Tensor Train Singular Value Decomposition". Benjamin Huber, Reinhold Schneider and Sebastian Wolf.\\

The intuition of the TT-SVD is that in every step a SVD is performed to detach one open mode from the tensor. The TT-SVD starts by calculating an SVD of the matricization of the initial tensor $x$, where all modes but the first one are combined:

$$U_1 S_1 V_1^T = SVD(M_1)$$

This is the analogous step as doing our first unfolding and its SVD. $U_1 \in \mathbb{R}^{n_1 \times r_1}, S_1 \in \mathbb{R}^{r_1 \times r_1}, V_1^T \in \mathbb{R}^{r_1 \times (n_2 \ldots n_d)}$. These resulting matrices are each dematricized. We cal $W_1$ to the dematricization of $U_1$ and $x_1$ to de dematricization of $S_1 V_1^T$. Now that there holds:

$$W_1 x_1 = x$$

In the next step a matricization of the newly acquired tensor $x_1$ is performed. The first dimension of the matricization is formed by the first two modes of $x_1$, corresponding to the new dimension introduced by the prior SVD and the second original dimension. The second dimension of the matricization is formed by all remaining modes of $x_1$. From this matricization another SVD is calculated:

$$U_2 S_2 V_2^T = SVD(M_2)$$

This is the analogous step as doing the unfoldings of the first approximation of the tensor and doing its SVD. As before, we do the dematricization pf the resulting matrices and we get:

$$W_1 W_2 x_2 = x$$

This procedure is continued for a total of $d-2$ steps and in each step the order of $x_i \in \mathbb{R}^{r_i \times n_{i+1} \times \ldots \times n_d}$ shrinks by one. Furthermore there holds

$$W_1 W_2 \ldots W_i x_i = x$$

The $d-1$ step is special sinde the dematricization of $x_{d-1}$ yields and order two tensor that is named $W_d$ instead of $x_d$. Finally: 

$$W_1 W_2 \ldots W_{d-1} W_d = x$$

is a valid TT representation of x with TT-rank $r = (r_1,\ldots ,r_{d-1})$. The same algorithm can also be used to calculate a rank $r^* =(r_1^*,\ldots,r_{d-1}^*)$ approximation of a tensor $x$ with TT-rank $r \geq r^*$. To this end the normal SVDs are replaced by truncated rank $r_i^*$ SVDs, yielding a tensor $x^*$ of TT-rank $r^*$. In contrast to the matrix case, $x^*$ is in general not the best rank $r^*$ approximation of $x$. However is a quasi-optimal approximation. \\

The computational complexity of the TT-SVD is dominated by the $d-1$ matrix singular value decompositions (as in CP-TT). The cost scales as $\mathcal{O}(dn^{d+1})$.\\

Similarities:
\begin{itemize}
\item The scheme based on matricization of the tensor and doing its SVD (this can be done by Eckart-Yung theorem) 
\item Keeping the modes to the final approach
\item The approximation of the new tensor is $SV^T$
\end{itemize}

Differences:
\begin{itemize}
\item They don't select just the modes associated with the largest singular value
\item The dematricization of every term, it's very costly!
\item Higer computational cost (due to the previous differences)
\end{itemize}




The matrizication scheme is inherit from TT-SVD. The intuition of the TT-SVD is that in every step a SVD is performed to detach one open mode from the tensor. 
The TT-SVD starts by calculating an SVD of the matricization of the initial tensor $x$, where all modes but the first one are combined:
$$U_1 S_1 V_1^T = SVD(M_1)$$
This is the analogous step as doing our first unfolding and its SVD. $U_1 \in \mathbb{R}^{n_1 \times r_1}, S_1 \in \mathbb{R}^{r_1 \times r_1}, V_1^T \in \mathbb{R}^{r_1 \times (n_2 \ldots n_d)}$. These resulting matrices are each dematricized. We cal $W_1$ to the dematricization of $U_1$ and $x_1$ to de dematricization of $S_1 V_1^T$. Now that there holds:
$$W_1 x_1 = x$$
In the next step a matricization of the newly acquired tensor $x_1$ is performed, and we proceed in an analogous way. It reads: 
$$W_1 W_2 x_2 = x$$
This procedure is continued for a total of $d-2$ steps and in each step the order of $x_i \in \mathbb{R}^{r_i \times n_{i+1} \times \ldots \times n_d}$ shrinks by one. The $d-1$ step is special since the dematricization of $x_{d-1}$ yields and order two tensor that is named $W_d$ instead of $x_d$. Finally: 
$$W_1 W_2 \ldots W_{d-1} W_d = x$$
is a valid TT representation of x with TT-rank $r = (r_1,\ldots ,r_{d-1})$. The computational complexity of the TT-SVD is dominated by the $d-1$ matrix singular value decompositions (as in CP-TT). The cost scales as $\mathcal{O}(dn^{d+1})$.

The main difference with respect to CP-TT is that the order of the varables in TT-SVD is given. Starting from the first unfolding till the $dth$ unfolding. In CP-TT we select the modes we're keeping by analizing which terms are giving us more information.

\section{TT-SVD better-perfomance}
From paper "Performance of low-rank approximations in tensor train format (TT-SVD) for large dense tensors".

In this paper they mostly focus on the computational part. They don't change the main idea or the main scheme of the TT-SVD method. They propose a ‘tensor-train singular value decomposition’ (TT-SVD) algorithm based on two building blocks: a ‘Q-less tall-skinny QR’ factorization, and a fused tall-skinny matrix- matrix multiplication and reshape operation. In addition, they present performance results for different algorithmic variants for shared-memory as well as distributed- memory architectures.\\

\begin{itemize}
\item The original TT-SVD.
The costly steps in this algorithm are computing the SVD, and evaluating the reduced matrix $X$ for the next iteration. $X$ is very tall and skinny except for the last iterations, where X is much smaller due to the reduction in each step. Therefore, it is advisable to apply the QR trick for calculating the SVD:
$$X = QR$$
$$\bar U \Sigma V^T = R, \quad with \quad U=Q\bar U$$

\item TSQR TT-SVD.
Based on the original TT-SVD algorithm but avoids some unnecessary computations and data transfers. We never compute the large matrix Q of the QR decomposition because it is faster to obtain the new $X$ from $XV_{:,1:r_{i-1}}$ instead of calculating the matrix $Q$ first and then evaluating $Q\bar U_{:,1:r_{i-1}} diag (\sigma_1, \ldots, \sigma_{r_{i-1}})$.\\

They also present the low-level building blocks for implementing a robust, highly-efficient and rank-preserving tall-skinny QR decomposition based on the communication-avoiding QR factorization. The implementation is based on Householder reflections.


\end{itemize}

\end{document}