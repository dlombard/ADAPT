\begin{thebibliography}{10}

\bibitem{batselier2015constructive}
{\sc K.~Batselier, H.~Liu, and N.~Wong}, {\em A constructive algorithm for
  decomposing a tensor into a finite sum of orthonormal rank-1 terms}, SIAM
  Journal on Matrix Analysis and Applications, 36 (2015), pp.~1315--1337.

\bibitem{Beylkin05algorithmsfor}
{\sc G.~Beylkin and M.~J. Mohlenkamp}, {\em Algorithms for numerical analysis
  in high dimensions}, SIAM J. SCI. COMPUT, 26 (2005), pp.~2133--2159.

\bibitem{bigoni2016}
{\sc D.~Bigoni, A.~P. Engsig-Karup, and Y.~M. Marzouk}, {\em Spectral
  tensor-train decomposition}, SIAM Journal on Scientific Computing, 38 (2016),
  pp.~A2405--A2439.

\bibitem{Os16}
{\sc A.~Cichocki, N.~Lee, I.~Oseledets, A.~P. amd Q.~Zhaonand, and D.~Mandic},
  {\em Tensor networks for dimensionality reduction and large-scale
  optimization: Part 1 low-rank tensor decompositions}, Now Publishers Inc., 35
  (2016), \url{https://doi.org/10.1561/2200000059}.

\bibitem{cichocki2016tensor}
{\sc A.~Cichocki, N.~Lee, I.~Oseledets, A.-H. Phan, Q.~Zhao, and D.~P. Mandic},
  {\em Tensor networks for dimensionality reduction and large-scale
  optimization: Part 1 low-rank tensor decompositions}, Foundations and
  Trends{\textregistered} in Machine Learning, 9 (2016), pp.~249--429.

\bibitem{deSilvLim08}
{\sc V.~de~Silva and L.-H. Lim}, {\em Tensor rank and the ill-posedness of the
  best low-rank approximation problem}, SIAM Journal on Matrix Analysis and
  Applications, 30 (2008), p.~1084–1127,
  \url{https://doi.org/10.1137/06066518X}.

\bibitem{Domanov2020OnUA}
{\sc I.~Domanov and L.~Lathauwer}, {\em On uniqueness and computation of the
  decomposition of a tensor into multilinear rank-(1, lr, lr) terms}, SIAM J.
  Matrix Anal. Appl., 41 (2020), pp.~747--803.

\bibitem{DomDeLath14}
{\sc I.~Domanov and L.~D. Lathauwer}, {\em Canonical polyadic decomposition of
  third-order tensors: reduction to generalized eigenvalue decomposition}, SIAM
  Journal on Matrix Analysis and Applications, 35 (2014), pp.~636--660,
  \url{https://doi.org/10.1137/130916084}.

\bibitem{espig2015convergence}
{\sc M.~Espig, W.~Hackbusch, and A.~Khachatryan}, {\em On the convergence of
  alternating least squares optimisation in tensor format representations},
  arXiv preprint arXiv:1506.00062,  (2015).

\bibitem{friedland2013best}
{\sc S.~Friedland, V.~Mehrmann, R.~Pajarola, and S.~K. Suter}, {\em On best
  rank one approximation of tensors}, Numerical Linear Algebra with
  Applications, 20 (2013), pp.~942--955.

\bibitem{Otta14}
{\sc S.~Friedland and G.~Ottaviani}, {\em The number of singular vector tuples
  and uniqueness of best rank-one approximation of tensors}, Found Comput Math,
  14 (2014), p.~1209–1242, \url{https://doi.org/10.1007/s10208-014-9194-z}.

\bibitem{grasedyck2013literature}
{\sc L.~Grasedyck, D.~Kressner, and C.~Tobler}, {\em A literature survey of
  low-rank tensor approximation techniques}, GAMM-Mitteilungen, 36 (2013),
  pp.~53--78.

\bibitem{hackbusch2012tensor}
{\sc W.~Hackbusch}, {\em Tensor spaces and numerical tensor calculus}, vol.~42,
  Springer, 2012.

\bibitem{hitchcock1927expression}
{\sc F.~L. Hitchcock}, {\em The expression of a tensor or a polyadic as a sum
  of products}, Journal of Mathematics and Physics, 6 (1927), pp.~164--189.

\bibitem{khoromskij2007low}
{\sc B.~Khoromskij and V.~Khoromskaia}, {\em Low rank tucker-type tensor
  approximation to classical potentials}, Open Mathematics, 5 (2007),
  pp.~523--550.

\bibitem{khoromskij2018tensor}
{\sc B.~N. Khoromskij}, {\em Tensor numerical methods in scientific computing},
  vol.~19, Walter de Gruyter GmbH \& Co KG, 2018.

\bibitem{khoromskij2009multigrid}
{\sc B.~N. Khoromskij and V.~Khoromskaia}, {\em Multigrid accelerated tensor
  approximation of function related multidimensional arrays}, SIAM Journal on
  Scientific Computing, 31 (2009), pp.~3002--3026.

\bibitem{Kolda2009TensorDA}
{\sc T.~Kolda and B.~Bader}, {\em Tensor decompositions and applications}, SIAM
  Review, 51 (2009), pp.~455--500, \url{https://doi.org/10.1137/07070111X}.

\bibitem{kolda2018tensor}
{\sc T.~G. Kolda}, {\em Tensor decomposition: A mathematical tool for data
  analysis.}, tech. report, Sandia National Lab.(SNL-CA), Livermore, CA (United
  States), 2018.

\bibitem{kolda2008scalable}
{\sc T.~G. Kolda and J.~Sun}, {\em Scalable tensor decompositions for
  multi-aspect data mining}, in 2008 Eighth IEEE international conference on
  data mining, IEEE, 2008, pp.~363--372.

\bibitem{kour2020efficient}
{\sc K.~Kour, S.~Dolgov, M.~Stoll, and P.~Benner}, {\em Efficient
  structure-preserving support tensor train machine}, arXiv preprint
  arXiv:2002.05079,  (2020).

\bibitem{Oseledets2011TensorTrainD}
{\sc I.~Oseledets}, {\em Tensor-train decomposition}, SIAM J. Sci. Comput., 33
  (2011), pp.~2295--2317.

\bibitem{OseTyrt09}
{\sc I.~Oseledets and E.~Tyrtyshnikov}, {\em Breaking the curse of
  dimensionality, or how to use svd in many dimensions}, SIAM Journal on Matrix
  Analysis and Applications, 31 (2009), p.~1084–1127,
  \url{https://doi.org/10.1137/090748330}.

\bibitem{oseledets2018alternating}
{\sc I.~V. Oseledets, M.~V. Rakhuba, and A.~Uschmajew}, {\em Alternating least
  squares as moving subspace correction}, SIAM Journal on Numerical Analysis,
  56 (2018), pp.~3459--3479.

\bibitem{Os20}
{\sc A.-H. Phan, K.~Sobolev, K.~Sozykin, D.~Ermilov, J.~Gusak, P.~Tichavsky,
  V.~Glukhov, I.~Oseledets, and A.~Cichocki}, {\em Stable low-rank tensor
  decomposition for compression of convolutional neural network}, ECCV2020,
  (2020), \url{https://doi.org/arXiv:2008.05441}.

\bibitem{els}
{\sc M.~Rajih, P.~Comon, and R.~Harsman}, {\em Enhanced line search: A novel
  method to accelerate parafac}, SIAM Journal on Matrix Analysis and
  Applications, 30 (2008), \url{https://doi.org/10.1137/06065577}.

\bibitem{rakhuba2016}
{\sc M.~Rakhuba and I.~Oseledets}, {\em Calculating vibrational spectra of
  molecules using tensor train decomposition}, The Journal of Chemical Physics,
  145 (2016), p.~124101.

\bibitem{rohwedder2013local}
{\sc T.~Rohwedder and A.~Uschmajew}, {\em On local convergence of alternating
  schemes for optimization of convex problems in the tensor train format}, SIAM
  Journal on Numerical Analysis, 51 (2013), pp.~1134--1162.

\bibitem{sorber2013optimization}
{\sc L.~Sorber, M.~Van~Barel, and L.~De~Lathauwer}, {\em Optimization-based
  algorithms for tensor decompositions: Canonical polyadic decomposition,
  decomposition in rank-(l\_r,l\_r,1) terms, and a new generalization}, SIAM
  Journal on Optimization, 23 (2013), pp.~695--720.

\bibitem{temlyakov1999greedy}
{\sc V.~N. Temlyakov}, {\em Greedy algorithms andm-term approximation with
  regard to redundant dictionaries}, Journal of Approximation Theory, 98
  (1999), pp.~117--145.

\bibitem{uschmajew2012local}
{\sc A.~Uschmajew}, {\em Local convergence of the alternating least squares
  algorithm for canonical tensor approximation}, SIAM Journal on Matrix
  Analysis and Applications, 33 (2012), pp.~639--652.

\bibitem{8081290}
{\sc M.~{Vandecappelle}, N.~{Vervliet}, and L.~{De Lathauwer}}, {\em Nonlinear
  least squares updating of the canonical polyadic decomposition}, in 2017 25th
  European Signal Processing Conference (EUSIPCO), 2017, pp.~663--667.

\bibitem{wang2015accelerating}
{\sc X.~Wang, C.~Navasca, and S.~Kindermann}, {\em On accelerating the
  regularized alternating least square algorithm for tensors}, arXiv preprint
  arXiv:1507.04721,  (2015).

\bibitem{wang2019distributed}
{\sc X.~Wang, L.~T. Yang, Y.~Wang, X.~Liu, Q.~Zhang, and M.~J. Deen}, {\em A
  distributed tensor-train decomposition method for cyber-physical-social
  services}, ACM Transactions on Cyber-Physical Systems, 3 (2019), pp.~1--15.

\bibitem{zhang2001rank}
{\sc T.~Zhang and G.~H. Golub}, {\em Rank-one approximation to high order
  tensors}, SIAM Journal on Matrix Analysis and Applications, 23 (2001),
  pp.~534--550.

\end{thebibliography}
