\BOOKMARK [1][-]{section.1}{Introduction}{}% 1
\BOOKMARK [1][-]{section.2}{Preliminaries}{}% 2
\BOOKMARK [2][-]{subsection.3}{Notation and Singular Value Decomposition \(SVD\)}{section.2}% 3
\BOOKMARK [2][-]{subsection.6}{Canonical Polyadic \(CP\) and Tensor Train \(TT\) formats}{section.2}% 4
\BOOKMARK [2][-]{subsection.7}{ TT-SVD algorithm}{section.2}% 5
\BOOKMARK [1][-]{section.19}{The Sum of Tensor Trains \(SoTT\) algorithm}{}% 6
\BOOKMARK [2][-]{subsection.20}{Presentation of the SoTT algorithm}{section.19}% 7
\BOOKMARK [2][-]{subsection.43}{Exponential convergence of the SoTT algorithm in finite dimension}{section.19}% 8
\BOOKMARK [2][-]{subsection.50}{Complexity estimate of the SoTT algorithm}{section.19}% 9
\BOOKMARK [3][-]{subsubsection.51}{Case 1: Unbounded ranks}{subsection.50}% 10
\BOOKMARK [3][-]{subsubsection.52}{Case 2: Bounded ranks}{subsection.50}% 11
\BOOKMARK [1][-]{section.54}{CP-TT: fixed-rank SoTT algorithm with rank 1}{}% 12
\BOOKMARK [1][-]{section.92}{Numerical Experiments}{}% 13
\BOOKMARK [2][-]{subsection.93}{Comparison between CP-TT and other rank-one update methods}{section.92}% 14
\BOOKMARK [3][-]{subsubsection.95}{Results for functions with = d2+0.1}{subsection.93}% 15
\BOOKMARK [3][-]{subsubsection.102}{Results for functions with = d2+1.1}{subsection.93}% 16
\BOOKMARK [3][-]{subsubsection.109}{Comparison of the norm of the residual with respect to computational time}{subsection.93}% 17
\BOOKMARK [2][-]{subsection.112}{SoTT for the compression of the solution of a parametric reaction diffusion equation}{section.92}% 18
\BOOKMARK [1][-]{section.120}{Conclusions and perspectives}{}% 19
\BOOKMARK [1][-]{section*.121}{References}{}% 20
