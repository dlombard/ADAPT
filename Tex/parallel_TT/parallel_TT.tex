\documentclass[a4paper,12pt]{article}
\usepackage{epsfig}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bbold}
\usepackage{algorithm2e}
\usepackage{color}

\linespread{1.0}
\setlength{\topmargin}{0cm}
\setlength{\oddsidemargin}{0cm}
\setlength{\evensidemargin}{0cm}
\setlength{\textwidth}{16.5cm}

%\thispagestyle{empty}
\title{Parallel construction of TT tensors.}
\author{Laura, Damiano}
\date{}
\newcommand{\epsBall}[2]{\mathcal{B}(#1 #2)}
\newcommand{\epsBtxt}[1]{$\varepsilon$--ball#1}
\newcommand{\Ker}{\mathrm{ker}}

\newtheorem{mylemma}{}
\newtheorem*{lemNoNum}{}
\newtheorem{myprop}{}
\newtheorem{mytheo}{}
\newtheorem{mydef}{}
\newtheorem{myex}{}


\begin{document}
\maketitle

\section{Introduction}

\section{Notation}
In this work, we deal with discrete tensors. Let $d\in\mathbb{N}^*$ denote the tensor order. The mode size is a vector $n\in\mathbb{N}^{d}$, whose components are $(n_i)_{1\leq i\leq d}\in\mathbb{N}^*$. Let $(I_j)_{1\leq j \leq d}\subset \mathbb{N}^*$, $\#I_j = n_j$ be the set of indices associated to the $j-$th mode (typically $I_j=\left\lbrace 1,\ldots, n_j \right\rbrace$); a real valued tensor reads:
\begin{equation}
T : \left\{ \begin{array}{ll}
                                                 I_i \times \ldots \times I_d  \ \rightarrow \ \mathbb{R} \\
                                                 (i_1,\ldots,i_d) \ \mapsto T(i_1,\ldots, i_d)\\
                                                 \end{array}\right.
\end{equation} 

A pure tensor product is defined. Let $1\leq j \leq d$, $v^{(j)}\in\mathbb{R}^{n_j}$ is a generic vector. A pure tensor product is an application associating to $d$ vectors the tensor:
\begin{equation}
v^{(1)}\otimes \ldots \otimes v^{(d)} : \left\{ \begin{array}{ll}
                                                 I_1 \times \ldots \times I_d  \ \rightarrow \ \mathbb{R} \\
                                                 (i_1,\ldots,i_d)  \ \mapsto  \ v^{(1)}_{i_1},\ldots, v^{(d)}_{i_d}
                                                 \end{array}\right.
\end{equation} 


Several tensor formats were introduced in the literature. In this work, although the main focus is on Tensor Train, we will make use of other formats. 

\section{The method.}
The main idea is the following. To illustrate it, we start considering a tensor $F$, to be compressed, given in CP format:
\begin{equation}
F = \sum_{i=1}^r f_i^{(1)}\otimes\ldots\otimes f_i^{(d)}. 
\end{equation}
Let us split the sum on $p$ processors. Every processor has in memory a part of the CP, the whole tensor be written as:
\begin{equation}
F = \sum_{i=1}^{s_1} f_i^{(1)}\otimes\ldots\otimes f_i^{(d)} + \sum_{i=s_1+1}^{s_2} f_i^{(1)}\otimes\ldots\otimes f_i^{(d)} + \ldots +  \sum_{i=s_{p-1}+1}^{s_p=r} f_i^{(1)}\otimes\ldots\otimes f_i^{(d)}.
\end{equation}
Every processor can do the compression of its partial sum and then a tournament procedure is defined, such that we obtain a final compressed approximation of $F$. 
This idea, which is quite natural when considering a CP format, can be extended to tensors given in: sparse format, Tucker ( and H-Tucker), Tensor-Train. Indeed, for all these formats, we can define a linear map of indices making it possible to decompose the set of the tensor product of fibers into $p$ subsets:
\begin{enumerate}
\item \textbf{Sparse tensor:} $$F = \sum_{i=1}^{N} F_{i \mapsto (i_1,\ldots,i_d)} \ e^{(1)}_{i \mapsto i_1} \otimes\ldots \otimes e^{(d)}_{i\mapsto i_d}.$$
Given $N$ non-zero entries, let $1\leq i\leq N$, there exists an invertible univalued map from $(i_1,\ldots, i_d)\mapsto i$ identifying the elements, the fibers being the pure tensor product of the canonical basis elements $e^{(1)}_{i_1},\ldots, e^{(d)}_{i_d}$. The sum is divided into $p$ sums, every processors having access to a part of the tensor. 
\item \textbf{Tensor-Train:} For TT, we proceed in an analogous way. The tensor is given as:
\begin{equation}
F = \sum_{j_1=1}^{r_1}\ldots\sum_{j_{d-1}=1}^{r_{d-1}} u_{j_1}G^{(2)}_{j_1 j_2} G^{(3)}_{j_2 j_3}\ldots u^{(d)}_{j_{d-1}}. 
\end{equation}
Let $R=\prod_{j=1}^{d-1} r_j$, there exists an invertible univalued map $(j_1,\ldots j_{d-1})\mapsto i$ such that:
\begin{equation}
F = \sum_{i=1}^{R} u_{i\mapsto j_1}G^{(2)}_{i\mapsto (j_1 j_2)} G^{(3)}_{i\mapsto (j_2 j_3)}\ldots u^{(d)}_{i\mapsto (j_{d-1}) }
\end{equation}
This idea is sketched in Fig.\ref{fig:TT_coloring}.
\item \textbf{Tucker:} For tensors given in Tucker format, We have:
\begin{equation}
F = \sum_{j_1}^{r_1}\ldots \sum_{j_d}^{r_d} u_{j_1}^{(1)}\ldots u_{j_d}^d G(j_1, \ldots, j_d). 
\end{equation}
Let $R =\prod_{j=1}^{d} r_j$, It is possible to construct a map $(j_1,\ldots, j_d)\mapsto i$ such that the sum can be recasted as:
\begin{equation}
F = \sum_{i}^{R} u_{i\mapsto j_1}^{(1)}\otimes \ldots \otimes u_{i \mapsto j_d}^d G_{i\mapsto(j_1,\ldots, j_d)}
\end{equation}
\end{enumerate}

\begin{figure}
\label{fig:TT_coloring}
\includegraphics[scale=0.30]{TT_scheme.pdf}
\caption{Representation of the basic idea for a TT tensor.}
\end{figure}

\subsection{Subdivision of a given tensor based on the fibres energy.}
In this section, a method to subdivide a given tensor is proposed. 

\subsection{Tournament Tensor Train compression.}
Once a given tensor is decomposed into a sum of tensors thanks to the maps of indices, a tournament compression is performed. 

\end{document}
