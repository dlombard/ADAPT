// Implementation of the Tucker Tensors
#include "TuckerTensor.h"
using namespace std;


// 1 -- Constructors
/* overloaded to init the tensor given a set of modes and the core tensor
  - input: the modes, the core
  - output: the Tucker tensor is built
*/
TuckerTensor::TuckerTensor(vector<vector<Vec> >& theModes, fullTensor& theCore, MPI_Comm theComm){
  m_theComm = theComm;
  m_nVar = theModes.size();
  m_rank.resize(m_nVar);
  m_nDof_var.resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    m_rank[idVar] = theModes[idVar].size();
    PetscInt thisModeSize;
    VecGetSize((theModes[idVar])[0], &thisModeSize);
    m_nDof_var[idVar] = thisModeSize;
  }
  m_modes = theModes;
  m_core = theCore;
}


// 2 -- COPY Structures and tensor data --

/* Copy the tensor structure from a given fullTensor:
  - input: the tensor from which the structure is copied
  - output: the structure of this tensor is identical
*/
void TuckerTensor::copyTensorStructFrom(TuckerTensor& source){
  m_theComm = source.theComm();
  m_nVar = source.nVar();
  m_nDof_var = source.nDof_var();
  m_isInitialised = true;
}


/* Copy the tensor structure to a given fullTensor:
  - input: the tensor to which the structure has to be copied
  - output: the structure of the target tensor
*/
void TuckerTensor::copyTensorStructTo(TuckerTensor& target){
  target.setTensorComm(m_theComm);
  target.set_nVar(m_nVar);
  target.set_nDof_var(m_nDof_var);
  target.setIsInitialised(true);
}


/* Copy the tensor from a given fullTensor:
  - input: the tensor to be copied
  - output: the tensor (structure + copy of the entries)
*/
void TuckerTensor::copyTensorFrom(TuckerTensor& source){
  copyTensorStructFrom(source);
  fullTensor sourceCore = source.core();
  m_core.copyTensorFrom(sourceCore);
  m_modes.resize(m_nVar);
  m_rank.resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    m_rank[idVar] = source.rank(idVar);
    m_modes[idVar].resize(m_rank[idVar]);
    for(unsigned int idMode=0; idMode<m_rank[idVar]; idMode++){
      Vec thisMode;
      a.initVec(thisMode,m_nDof_var[idVar],m_theComm);
      VecCopy(source.modes(idVar,idMode),thisMode);
      a.finalizeVec(thisMode);
      (m_modes[idVar])[idMode] = thisMode;
    }
  }
  m_isInitialised = true;
}


/* Copy the tensor into a given fullTensor:
  - input: this tensor
  - output: the copy of this tensor (structure + copy of the entries)
*/

void TuckerTensor::copyTensorTo(TuckerTensor& target){

}




// -- OPERATIONS --

/* check if this tensor has the same structure than a given one
  - input: the tensor to compare to
  - output: true if the tensor has the same structure
*/
bool TuckerTensor::hasSameStructure(TuckerTensor& toBeComparedTo){
  return true;
}


/* multiply the tensor by a given scalar
  - input: the double alpha
  - output: this tensor is multiplied by alfa
*/
void TuckerTensor::multiplyByScalar(double alpha){
  m_core.multiplyByScalar(alpha);
}



/* Eval the tensor given the multi-index
  - input: the multi-index
  - output: the tensor value evaluated at the multi-index
*/
void TuckerTensor::eval(const vector<unsigned int>& ind, double& val){
  assert(ind.size()==m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    assert(ind[idVar]<m_nDof_var[idVar]);
  };

  // init the vector vector containing the modes evaluated at ind
  vector<vector<double> > modesAtDofInd;
  modesAtDofInd.resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    modesAtDofInd[idVar].resize(m_rank[idVar]);
    for(unsigned int idMode=0; idMode<m_rank[idVar]; idMode++){
      modesAtDofInd[idVar][idMode] = a.getVecEl(m_modes[idVar][idMode],ind[idVar],m_theComm);
    }
  }

  // n-saturation with the core tensor.
  val = 0.0;
  for(unsigned int l=0; l<m_core.nEntries();l++){
    vector<unsigned int> thisCoreIndex = m_core.lin2sub(l);
    double thisEntry = m_core.tensorEntries(l);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      unsigned int idM = thisCoreIndex[idVar];
      thisEntry = thisEntry * modesAtDofInd[idVar][idM];
    }
    val = val + thisEntry;
  }
}
