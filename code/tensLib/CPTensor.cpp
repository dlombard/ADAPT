// Implementation of CPTensor class

#include "CPTensor.h"
using namespace std;


// -- OVERLOADED CONSTRUCTORS --

/* Initialising an empty CP
  - inputs: dimension, degrees of freedom per direction
  - output: this (empty) CP is initialised with m_rank = 0
*/
CPTensor::CPTensor(unsigned int dim, vector<unsigned int> nDofPerVar, MPI_Comm theComm){
  m_theComm = theComm;
  m_nVar = dim;
  m_nDof_var.resize(dim);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = nDofPerVar[iVar];
  }
  m_rank = 0;
  m_isInitialised = true;
}


/* Initialising a CP
  - inputs: the Terms, the coeffs
  - output: this  CP is initialised
*/
CPTensor::CPTensor(vector<vector<Vec> >& theTerms, vector<double>& theCoeffs, MPI_Comm theComm){
  m_theComm = theComm;
  m_rank = theTerms.size();
  m_nVar = theTerms[0].size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    PetscInt sizeOfVec;
    VecGetSize(theTerms[0][idVar], &sizeOfVec);
    m_nDof_var[idVar] = sizeOfVec;
  }
  m_terms.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_terms[iTerm].resize(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      (m_terms[iTerm])[idVar] = (theTerms[iTerm])[idVar];
    }
  };

  assert(theCoeffs.size() == m_rank);
  m_coeffs = theCoeffs;

  m_isInitialised = true;
}


/* Initialising a CP
  - inputs: the Terms
  - output: this  CP is initialised with coeffs = 1!
*/
CPTensor::CPTensor(vector<vector<Vec> >& theTerms, MPI_Comm theComm){
  m_theComm = theComm;
  m_rank = theTerms.size();
  m_nVar = theTerms[0].size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    PetscInt sizeOfVec;
    VecGetSize(theTerms[0][idVar], &sizeOfVec);
    m_nDof_var[idVar] = sizeOfVec;
  }
  m_terms.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_terms[iTerm].resize(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      (m_terms[iTerm])[idVar] = (theTerms[iTerm])[idVar];
    }
  };
  m_coeffs.resize(m_rank);
  for(unsigned int idTerm=0; idTerm<m_rank; idTerm++){
    m_coeffs[idTerm] = 1.0;
  }

  m_isInitialised = true;
}


/* Initialising a one term CP (pure tensor product)
  - inputs: one single term, a pure tensor product, coefficient
  - output: this  CP is initialised,
*/
CPTensor::CPTensor(vector<Vec>& theTerm, double coeff, MPI_Comm theComm){
  m_theComm = theComm;
  m_rank = 1;
  m_nVar = theTerm.size();
  m_nDof_var.resize(m_nVar);
  m_terms.resize(m_rank);
  m_terms[0].resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    PetscInt sizeOfVec;
    VecGetSize(theTerm[idVar], &sizeOfVec);
    m_nDof_var[idVar] = sizeOfVec;
    (m_terms[0])[idVar] = theTerm[idVar];
  }
  m_coeffs.resize(1);
  m_coeffs[0] = coeff;

  m_isInitialised = true;
}


/* Initialising a one term CP (pure tensor product)
  - inputs: one single term, a pure tensor product, coefficient = 1!
  - output: this  CP is initialised,
*/
CPTensor::CPTensor(vector<Vec>& theTerm, MPI_Comm theComm){
  m_theComm = theComm;
  m_rank = 1;
  m_nVar = theTerm.size();
  m_nDof_var.resize(m_nVar);
  m_terms.resize(m_rank);
  m_terms[0].resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    PetscInt sizeOfVec;
    VecGetSize(theTerm[idVar], &sizeOfVec);
    m_nDof_var[idVar] = sizeOfVec;
    (m_terms[0])[idVar] = theTerm[idVar];
  }
  m_coeffs.resize(1);
  m_coeffs[0] = 1.0;

  m_isInitialised = true;
}


/*  Clear
  - input: none
  - output: clear the current CP
*/
void CPTensor::clear(){
  for(unsigned int idTerm=0; idTerm<m_rank; idTerm++){
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      VecDestroy(&m_terms[idTerm][idVar]);
    }
  }
  m_coeffs.clear();
  m_rank = 0;
}


/* check if a given CP has the same structure as the current one
  - input: the CP to be compared
  - output true or false
*/
inline bool CPTensor::hasSameStructure(CPTensor& toBeCompared){
  bool isItSame = false;
  if(m_nVar==toBeCompared.nVar()){
    bool hasTheSameDof = true;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(m_nDof_var[iVar] != toBeCompared.nDof_var(iVar)){
        hasTheSameDof = false;
        break;
      }
    }
    if(hasTheSameDof){
      isItSame = true;
    }
  }
  return isItSame;
}


// -- COPY functions --

/*  perform a copy of a given tensor
  - inputs: the tensor to be copied
  - output: this CP tensor is the copy of the given one
*/
void CPTensor::copyTensorFrom(CPTensor& toBeCopied){
  m_theComm = toBeCopied.theComm();
  m_nVar = toBeCopied.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    m_nDof_var[idVar] = toBeCopied.nDof_var(idVar);
  }
  m_isInitialised = true;
  m_rank = toBeCopied.rank();
  m_terms.resize(m_rank);
  m_coeffs.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_coeffs[iTerm] = toBeCopied.coeffs(iTerm);
    m_terms[iTerm].resize(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      Vec thisTerm;
      a.initVec(thisTerm, m_nDof_var[idVar], m_theComm);
      VecCopy(toBeCopied.terms(iTerm, idVar), thisTerm);
      a.finalizeVec(thisTerm);
      (m_terms[iTerm])[idVar] = thisTerm;
    }
  }
}



// -- METHODS --

/* multiply by a scalar
  - input: the scalar
  - output: the coefficients are multiplied by the scalar;
*/
void CPTensor::multiplyByScalar(double val){
  for(unsigned int idTerm=0; idTerm<m_rank; idTerm++){
    m_coeffs[idTerm] = m_coeffs[idTerm] * val;
  }
}


/* rescale a term by a given scalar
  - input: the iTerm, iVar and the coefficient alpha
  - output: term[iTerm][iVar] *= alpha
*/
void CPTensor::rescaleTerm(unsigned int iTerm, unsigned int iVar, double alpha){
  VecScale(m_terms[iTerm][iVar], alpha);
}



/* compute the scalar product of two CP terms
  - inputs: the terms, the mass matrices
  - output: the scalar product
*/
double CPTensor::scalarProdTerms(vector<Vec>& term_1, vector<Vec>& term_2, vector<Mat>& mass){
  assert(term_1.size() == m_nVar);
  assert(term_2.size() == m_nVar);
  assert(mass.size() == m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    PetscInt sizeOfVec_1, sizeOfVec_2;
    VecGetSize(term_1[idVar], &sizeOfVec_1);
    VecGetSize(term_2[idVar], &sizeOfVec_2);
    assert(sizeOfVec_1 == m_nDof_var[idVar]);
    assert(sizeOfVec_2 == m_nDof_var[idVar]);
  }

  double scalar = 1.0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    Vec prod;
    a.initVec(prod, m_nDof_var[idVar], m_theComm);
    a.finalizeVec(prod);
    MatMult(mass[idVar], term_1[idVar], prod);
    double quadForm;
    VecDot(prod, term_2[idVar], &quadForm);
    scalar = scalar * quadForm;
    VecDestroy(&prod);
  }
  return scalar;
};


/* compute the scalar product of two CP terms
  - inputs: the terms, the mass matrices
  - output: the scalar product
  REMARK: Masses are identities \ell^2 scalar product
*/
double CPTensor::scalarProdTerms(vector<Vec>& term_1, vector<Vec>& term_2){
  assert(term_1.size() == m_nVar);
  assert(term_2.size() == m_nVar);
  assert(mass.size() == m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    PetscInt sizeOfVec_1, sizeOfVec_2;
    VecGetSize(term_1[idVar], &sizeOfVec_1);
    VecGetSize(term_2[idVar], &sizeOfVec_2);
    assert(sizeOfVec_1 == m_nDof_var[idVar]);
    assert(sizeOfVec_2 == m_nDof_var[idVar]);
  }

  double scalar = 1.0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    double quadForm;
    VecDot(term_1[idVar], term_2[idVar], &quadForm);
    scalar = scalar * quadForm;
  }
  return scalar;
};


/* compute the scalar product between this CP and another one
 - Input: the CP to compute the scalar product with
 - Output: a scalar <this, otherCP>
 REMARK: \ell^2 discrete scalar product !
*/
double CPTensor::scalarProd(CPTensor& otherTens, vector<Mat>& mass){
  assert(hasSameStructure(otherTens));

  double toBeReturned = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int jTerm=0; jTerm<otherTens.rank(); jTerm++){
      vector<Vec> otherTerms = otherTens.terms(jTerm);
      double toBeAdded = scalarProdTerms(m_terms[iTerm], otherTerms, mass);
      toBeReturned += m_coeffs[iTerm]*otherTens.coeffs(jTerm)*toBeAdded;
    }
  }

  return toBeReturned;
};


/* compute the scalar product between this CP and another one
 - Input: the CP to compute the scalar product with
 - Output: a scalar <this, otherCP>
 REMARK: \ell^2 discrete scalar product !
*/
double CPTensor::scalarProd(CPTensor& otherTens){
  assert(hasSameStructure(otherTens));

  double toBeReturned = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int jTerm=0; jTerm<otherTens.rank(); jTerm++){
      vector<Vec> otherTerms = otherTens.terms(jTerm);
      double toBeAdded = scalarProdTerms(m_terms[iTerm], otherTerms);
      toBeReturned += m_coeffs[iTerm]*otherTens.coeffs(jTerm)*toBeAdded;
    }
  }

  return toBeReturned;
};





/* compute the 2 norm squared of a CP tensor
  - inputs: the vector of mass matrices
  - output: the norm of this tensor
*/
double CPTensor::norm2CP(vector<Mat>& mass){
  double toBeReturned = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double tmp = m_coeffs[iTerm] * m_coeffs[iTerm];
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      Vec prod;
      a.initVec(prod, m_nDof_var[idVar], m_theComm);
      a.finalizeVec(prod);
      MatMult(mass[idVar], m_terms[iTerm][idVar], prod);
      double quadForm;
      VecDot(prod, m_terms[iTerm][idVar], &quadForm);
      tmp = tmp * quadForm;
      VecDestroy(&prod);
    }
    toBeReturned = toBeReturned + tmp;
    for(unsigned int jTerm=iTerm+1; jTerm<m_rank; jTerm++){
      double tmpDiffInd = 2.0 * m_coeffs[iTerm] * m_coeffs[jTerm];
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        Vec prod;
        a.initVec(prod, m_nDof_var[idVar], m_theComm);
        a.finalizeVec(prod);
        MatMult(mass[idVar], m_terms[iTerm][idVar], prod);
        double quadForm;
        VecDot(prod, m_terms[jTerm][idVar], &quadForm);
        tmpDiffInd = tmpDiffInd * quadForm;
        VecDestroy(&prod);
      }
      toBeReturned = toBeReturned + tmpDiffInd;
    }
  }
  return toBeReturned;
}


/* compute the 2 norm squared of a CP tensor
  - inputs: mass matrices are identity!!
  - output: the norm of this tensor
*/
double CPTensor::norm2CP(){
  double toBeReturned = 0.0;

  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double tmp = m_coeffs[iTerm] * m_coeffs[iTerm];
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      double quadForm;
      VecDot(m_terms[iTerm][idVar], m_terms[iTerm][idVar], &quadForm);
      tmp = tmp * quadForm;
    }
    if(isnan(tmp)){puts("overflow!");};

    toBeReturned = toBeReturned + tmp;

    for(unsigned int jTerm=iTerm+1; jTerm<m_rank; jTerm++){
      double tmpDiffInd = 2.0 * m_coeffs[iTerm] * m_coeffs[jTerm];
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        double quadForm;
        VecDot(m_terms[iTerm][idVar], m_terms[jTerm][idVar], &quadForm);
        tmpDiffInd = tmpDiffInd * quadForm;
      }
      if(isnan(tmpDiffInd)){puts("overflow!");};
      
      toBeReturned = toBeReturned + tmpDiffInd;
    }
  }
  if(toBeReturned<0.0){toBeReturned  = 0.0;} // fix in case of small roundoffs.
  return toBeReturned;
}


/*  Compute one term of the CPTT algorithm
  - inputs: the residual in full tensor format
  - output: the pure tensor term
*/
void CPTensor::compute_CPTT_Term(fullTensor& residual, vector<Vec>& theTerm, double& coeff){
  theTerm.resize(m_nVar);
  vector<unsigned int>indicesUnfolding(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    indicesUnfolding[iVar] = iVar;
  }

  fullTensor W;
  for(unsigned int iFun=0; iFun<m_nVar-2; iFun++){
    // compute 1-term svd for all the unfoldings m_nVar-iFun == indicesUnfolding.size();


    vector<double> singValues(m_nVar-iFun);
    vector<Vec> modes(m_nVar-iFun);
    vector<Vec> v_modes(m_nVar-iFun);

    for(unsigned int count=0; count<m_nVar-iFun; count++){
       unsigned int iUnfold = indicesUnfolding[count];
       Mat theIthUnfold;

       if(iFun==0){
         residual.computeUnfolding(iUnfold, theIthUnfold);
       }
       else{
         //Reshape w=sigma*v
         W.computeUnfolding(count, theIthUnfold);
       }

       double sigma;
       Vec u,v;
       a.computeOneSVDTerm(theIthUnfold, sigma, u, v, m_theComm);
       modes[count] = u;
       v_modes[count] = v;
       singValues[count] = sigma;

       MatDestroy(&theIthUnfold);
    }
    // ranking of singular values:
    unsigned int iBest=0;
    double largestSingVal = singValues[0];
    for(unsigned int iVal=1; iVal<singValues.size(); iVal++){
      if(singValues[iVal]>largestSingVal){
        iBest = iVal;
        largestSingVal = singValues[iVal];
      }
    }
    unsigned int bestIndex = indicesUnfolding[iBest];
    theTerm[bestIndex] = modes[iBest];

    // update the set of indices
    vector<unsigned int> oldIndices(m_nVar-iFun);
    for(unsigned int iInd=0; iInd<m_nVar-iFun; iInd++){
      oldIndices[iInd] = indicesUnfolding[iInd];
    }
    indicesUnfolding.clear();
    indicesUnfolding.resize(m_nVar-iFun-1);
    unsigned int cc = 0;
    for(unsigned int iInd=0; iInd<m_nVar-iFun; iInd++){
      if(oldIndices[iInd] != bestIndex){
        indicesUnfolding[cc] = oldIndices[iInd];
        cc = cc + 1;
      }
    }

    // free the memory;
    for(unsigned int iInd=0;iInd<m_nVar-iFun; iInd++){
      if(iInd != iBest){
        VecDestroy(&modes[iInd]);
        VecDestroy(&v_modes[iInd]);
      }
    }

    /*  Reshape w = sigma*v^T of the CPTT algorithm
      - inputs: the vector w = sigma*v^T
      - output: the pure tensor term
    */
    Vec w = v_modes[iBest];
    VecScale(w,largestSingVal);

    std::vector<unsigned int> resPerDim(indicesUnfolding.size());
    for (unsigned int iVar = 0; iVar < indicesUnfolding.size(); iVar++) {
      resPerDim[iVar]=m_nDof_var[indicesUnfolding[iVar]];
    }

    W.setTensorComm(m_theComm);
    W.set_nVar(indicesUnfolding.size());
    W.set_nDof_var(resPerDim);
    W.init();
    W.setTensorEntries(w);
    W.finalizeTensor();
    W.setIsInitialised(true);

 }
  // the last term:

Mat lastUnfold;
W.computeUnfolding(0,lastUnfold);

double sigma;
Vec u,v;
a.computeOneSVDTerm(lastUnfold,sigma, u,v, m_theComm);
unsigned int bestIndex_0=indicesUnfolding[0];
unsigned int bestIndex_1=indicesUnfolding[1];

theTerm[bestIndex_0]=u;
theTerm[bestIndex_1]=v;
coeff=sigma;
}


/*  Compute one term of the CPTT algorithm
  - inputs: the residual in CP tensor format
  - output: the pure tensor term
*/
void CPTensor::compute_CPTT_Term(CPTensor& residual, vector<Vec>& theTerm, double& coeff){
  theTerm.resize(m_nVar);
  vector<unsigned int>indicesUnfolding(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    indicesUnfolding[iVar] = iVar;
  }

  CPTensor W;
  //CPTensor Wcopy;
  for(unsigned int iFun=0; iFun<m_nVar-2; iFun++){
    // compute 1-term svd for all the unfoldings m_nVar-iFun == indicesUnfolding.size();
    vector<double> singValues(m_nVar-iFun);
    vector<Vec> modes(m_nVar-iFun);

    for(unsigned int count=0; count<m_nVar-iFun; count++){
       unsigned int iUnfold = indicesUnfolding[count];

       double sigma;
       Vec u;
       if(iFun==0){
         residual.compute_Unfolding_OneSvdTerm(iUnfold, sigma, u);
       }
       else{
         W.compute_Unfolding_OneSvdTerm(count, sigma, u);
       }

       modes[count] = u;
       singValues[count] = sigma;
    }

    // ranking of singular values:
    unsigned int iBest=0;
    double largestSingVal = singValues[0];
    for(unsigned int iVal=1; iVal<singValues.size(); iVal++){
      if(singValues[iVal]>largestSingVal){
        iBest = iVal;
        largestSingVal = singValues[iVal];
      }
    }
    unsigned int bestIndex = indicesUnfolding[iBest];
    theTerm[bestIndex] = modes[iBest];


    // update the set of indices
    vector<unsigned int> oldIndices(m_nVar-iFun);
    for(unsigned int iInd=0; iInd<m_nVar-iFun; iInd++){
      oldIndices[iInd] = indicesUnfolding[iInd];
    }
    indicesUnfolding.clear();
    indicesUnfolding.resize(m_nVar-iFun-1);
    unsigned int cc = 0;
    for(unsigned int iInd=0; iInd<m_nVar-iFun; iInd++){
      if(oldIndices[iInd] != bestIndex){
        indicesUnfolding[cc] = oldIndices[iInd];
        cc = cc + 1;
      }
    }

    // free the memory;
    for(unsigned int iInd=0;iInd<m_nVar-iFun; iInd++){
      if(iInd != iBest){
        VecDestroy(&modes[iInd]);
      }
    }

    /*  Compute W = int_i residual u(x_i) dx_i
      - inputs: the vector w = sigma*v^T
      - output: the pure tensor term
    */
    if(iFun==0){
      residual.modeIContraction(iBest, theTerm[bestIndex], W);
      //Wcopy.copyTensorFrom(residual);
    }
    else{
      W.inplaceModeIContraction(iBest,theTerm[bestIndex]);
      //Wcopy.copyTensorFrom(W);
    }
    //W.clear();
    //Wcopy.modeIContraction(iBest, theTerm[bestIndex], W);
    //Wcopy.clear();
 }
  // the last term:

  double sigma;
  Vec u;
  W.compute_Unfolding_OneSvdTerm(0, sigma, u);
  unsigned int bestIndex_0=indicesUnfolding[0];
  unsigned int bestIndex_1=indicesUnfolding[1];

  // computing v:
  Vec v;
  a.initVec(v, m_nDof_var[bestIndex_1], m_theComm);
  a.finalizeVec(v);
  for(unsigned int iTerm=0; iTerm<W.rank(); iTerm++){
    double scalar;
    //Vec term = W.terms(iTerm,0);
    VecDot(W.terms(iTerm,0), u, &scalar);
    double a_i = W.coeffs(iTerm) / sigma;
    a_i = a_i * scalar;
    VecAXPY(v,a_i,W.terms(iTerm,1));
  }

  theTerm[bestIndex_0]=u;
  theTerm[bestIndex_1]=v;

  coeff=sigma;
  W.clear();
}


/*  Compute one term of the CPTT algorithm (overloaded)
  - inputs: the residual in CP tensor format
  - output: a pure tensor term is added to the current CP
*/
void CPTensor::compute_CPTT_Term(CPTensor& residual){
  assert(hasSameStructure(residual));

  double coeff = 0.0;
  vector<Vec> theTerm;
  theTerm.resize(m_nVar);
  vector<unsigned int>indicesUnfolding(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    indicesUnfolding[iVar] = iVar;
  }

  CPTensor W;
  //CPTensor Wcopy;
  for(unsigned int iFun=0; iFun<m_nVar-2; iFun++){
    // compute 1-term svd for all the unfoldings m_nVar-iFun == indicesUnfolding.size();
    vector<double> singValues(m_nVar-iFun);
    vector<Vec> modes(m_nVar-iFun);

    for(unsigned int count=0; count<m_nVar-iFun; count++){
       unsigned int iUnfold = indicesUnfolding[count];

       double sigma;
       Vec u;
       if(iFun==0){
         residual.compute_Unfolding_OneSvdTerm(iUnfold, sigma, u);
       }
       else{
         W.compute_Unfolding_OneSvdTerm(count, sigma, u);
       }

       modes[count] = u;
       singValues[count] = sigma;
    }

    // ranking of singular values:
    unsigned int iBest=0;
    double largestSingVal = singValues[0];
    for(unsigned int iVal=1; iVal<singValues.size(); iVal++){
      if(singValues[iVal]>largestSingVal){
        iBest = iVal;
        largestSingVal = singValues[iVal];
      }
    }
    unsigned int bestIndex = indicesUnfolding[iBest];
    theTerm[bestIndex] = modes[iBest];


    // update the set of indices
    vector<unsigned int> oldIndices(m_nVar-iFun);
    for(unsigned int iInd=0; iInd<m_nVar-iFun; iInd++){
      oldIndices[iInd] = indicesUnfolding[iInd];
    }
    indicesUnfolding.clear();
    indicesUnfolding.resize(m_nVar-iFun-1);
    unsigned int cc = 0;
    for(unsigned int iInd=0; iInd<m_nVar-iFun; iInd++){
      if(oldIndices[iInd] != bestIndex){
        indicesUnfolding[cc] = oldIndices[iInd];
        cc = cc + 1;
      }
    }

    // free the memory;
    for(unsigned int iInd=0;iInd<m_nVar-iFun; iInd++){
      if(iInd != iBest){
        VecDestroy(&modes[iInd]);
      }
    }

    /*  Compute W = int_i residual u(x_i) dx_i
      - inputs: the vector w = sigma*v^T
      - output: the pure tensor term
    */
    if(iFun==0){
      residual.modeIContraction(iBest, theTerm[bestIndex], W);
      //Wcopy.copyTensorFrom(residual);
    }
    else{
      W.inplaceModeIContraction(iBest,theTerm[bestIndex]);
      //Wcopy.copyTensorFrom(W);
    }
    //W.clear();
    //Wcopy.modeIContraction(iBest, theTerm[bestIndex], W);
    //Wcopy.clear();
 }
  // the last term:

  double sigma;
  Vec u;
  W.compute_Unfolding_OneSvdTerm(0, sigma, u);
  unsigned int bestIndex_0=indicesUnfolding[0];
  unsigned int bestIndex_1=indicesUnfolding[1];

  // computing v:
  Vec v;
  a.initVec(v, m_nDof_var[bestIndex_1], m_theComm);
  a.finalizeVec(v);
  for(unsigned int iTerm=0; iTerm<W.rank(); iTerm++){
    double scalar;
    //Vec term = W.terms(iTerm,0);
    VecDot(W.terms(iTerm,0), u, &scalar);
    double a_i = W.coeffs(iTerm) / sigma;
    a_i = a_i * scalar;
    VecAXPY(v,a_i,W.terms(iTerm,1));
  }

  theTerm[bestIndex_0]=u;
  theTerm[bestIndex_1]=v;

  coeff=sigma;
  W.clear();

  m_terms.push_back(theTerm);
  m_rank = m_rank + 1;
  m_coeffs.push_back(coeff);

}



/* compute first svd term of the unfolding wothout computing the Unfolding!
  - input: the mode-I number
  - output: sigma, u
*/
void CPTensor::compute_Unfolding_OneSvdTerm(unsigned int index, double& sigma, Vec& u){
  assert(index<m_nVar);

  //-- compute svd of the matrix whose columns are the modes in index direction
  Mat T;
  a.initMat(T,m_nDof_var[index], m_rank, m_theComm);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    a.setMatCol(T, iTerm, m_terms[iTerm][index], m_theComm);
  }
  a.finalizeMat(T);

  Mat tildeU, tildeV;
  Vec tildeS;
  a.svdLHC(T, tildeU, tildeV, tildeS, m_theComm);

  Mat transposeV = a.transpose(tildeV, m_theComm);
  Mat S = a.diag(tildeS, m_theComm);

  Mat Z;
  a.initMat(Z, m_rank, m_rank, m_theComm);
  a.finalizeMat(Z);
  MatMatMult(S, transposeV, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &Z);

  // computing the covariance matrix K:
  Mat K;
  a.initMat(K, m_rank, m_rank, m_theComm);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int jTerm=0; jTerm<=iTerm; jTerm++){
      double value = m_coeffs[iTerm] * m_coeffs[jTerm];
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        if(iVar != index){
          double scalar;
          VecDot(m_terms[iTerm][iVar], m_terms[jTerm][iVar], &scalar);
          value = value * scalar;
        }
      }
      a.setMatEl(K, iTerm, jTerm, value, m_theComm);
      a.setMatEl(K, jTerm, iTerm, value, m_theComm);
    }
  }
  a.finalizeMat(K);

  // computing tildeK
  Mat transposedZ = a.transpose(Z,m_theComm);
  Mat tmp;
  a.initMat(tmp, m_rank, m_rank, m_theComm);
  a.finalizeMat(tmp);
  MatMatMult(Z, K, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &tmp);

  Mat tildeK;
  a.initMat(tildeK, m_rank, m_rank, m_theComm);
  a.finalizeMat(tildeK);
  MatMatMult(tmp, transposedZ, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &tildeK);

  // compute the first eigenvector of tildeK and the eigenvalue;
  EPS eps;
  Vec w;
  double lambda;
  a.initEps(eps, tildeK, EPS_HEP, 1, 1e-9, m_theComm);
  a.solveFirstPairHermEPS(eps, tildeK, w, lambda, m_theComm);
  /*PetscInt nconv;
  EPSCreate(m_theComm, &eps);
  EPSSetOperators(eps, tildeK, NULL);
  EPSSetProblemType(eps,EPS_HEP);
  EPSSetType(eps, EPSLANCZOS); // EPSKRYLOVSCHUR
  EPSSetDimensions(eps, 1, PETSC_DEFAULT, PETSC_DEFAULT);
  EPSSetFromOptions(eps);
  EPSSolve(eps);
  EPSGetConverged(eps,&nconv);
  PetscScalar lambda, ki;
  Vec w, xi;
  if(nconv>=1){
    EPSGetEigenpair(eps,0,&lambda,&ki,w,xi);
  }
  else{
    PetscPrintf(m_theComm, "Eps did not converge!");
    exit(1);
  }*/

  // the result:
  a.initVec(u, m_nDof_var[index], m_theComm);
  a.finalizeVec(u);

  sigma = sqrt(lambda);
  MatMult(tildeU, w, u);

  MatDestroy(&T);
  MatDestroy(&K);
  MatDestroy(&Z);
  MatDestroy(&transposeV);
  MatDestroy(&tmp);
  MatDestroy(&transposedZ);
  MatDestroy(&tildeU);
  MatDestroy(&S);
  VecDestroy(&tildeS);
  MatDestroy(&tildeK);
  MatDestroy(&tildeV);
  VecDestroy(&w);
  EPSDestroy(&eps);
}


/*  Add a pure tensor term
  - inputs: the vector of Term of the pure tensor product, the coeff
  - output: this term is added to the current CP tensor
*/
void CPTensor::addPureTensorTerm(vector<Vec>& theTerm, double coeff){
  m_rank = m_rank + 1;
  assert(theTerm.size()==m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    PetscInt vecSize;
    VecGetSize(theTerm[idVar], &vecSize);
    assert(vecSize == m_nDof_var[idVar]);
  }
  m_terms.push_back(theTerm);
  m_coeffs.push_back(coeff);
}


/*  sum another CP, rescaled
  - inputs: CP tensor to be added, the scaling
  - output: this CP is added to the current CP tensor
*/
void CPTensor::sum(CPTensor& toAdd, double scale, bool COPY=false){
  assert(m_nVar == toAdd.nVar());
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    assert(m_nDof_var[idVar] == toAdd.nDof_var(idVar));
  }
  unsigned int currentRank = m_rank;
  m_rank = m_rank + toAdd.rank();
  m_terms.resize(m_rank);
  m_coeffs.resize(m_rank);
  for(unsigned int iTerm=currentRank; iTerm<m_rank; iTerm++){
    m_terms[iTerm].resize(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(!COPY){
        (m_terms[iTerm])[idVar] = toAdd.terms(iTerm-currentRank,idVar);
      }
      else{
        Vec thisTerm;
        a.initVec(thisTerm, toAdd.nDof_var(idVar), m_theComm);
        VecCopy(toAdd.terms(iTerm-currentRank, idVar), thisTerm);
        a.finalizeVec(thisTerm);
        (m_terms[iTerm])[idVar] = thisTerm;
      }
    }
    m_coeffs[iTerm] = scale * toAdd.coeffs(iTerm-currentRank);
  }
}


/* mode I contraction of a CP
  - input: the index of the mode, the vector
  - output: the result in a form of a CP
*/
void CPTensor::modeIContraction(unsigned int index, Vec& u, CPTensor& result){
  result.setTensorComm(m_theComm);
  result.set_nVar(m_nVar-1);
  result.set_rank(m_rank);
  vector<unsigned int> dofPerDim(m_nVar-1);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != index){
      dofPerDim[cc] = m_nDof_var[iVar];
      cc = cc + 1;
    }
  }
  result.set_nDof_var(dofPerDim);

  // compute coefficients:
  vector<double> resultCoeffs(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double scalar;
    VecDot(m_terms[iTerm][index], u, &scalar);
    resultCoeffs[iTerm] = m_coeffs[iTerm] * scalar;
  }
  result.set_coeffs(resultCoeffs);

  // compute the terms:
  vector<vector<Vec> > resultTerms(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    vector<Vec> thisTerm(m_nVar-1);
    unsigned int count = 0;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != index){
        Vec function;
        a.initVec(function, m_nDof_var[iVar], m_theComm);
        VecCopy(m_terms[iTerm][iVar], function);
        a.finalizeVec(function);
        thisTerm[count] = function;
        count = count + 1;
      }
    }
    resultTerms[iTerm] = thisTerm;
  }
  result.set_terms(resultTerms);
}


/*  in place mode I contraction
  - input: the mode and the vector we contract agaist to
  - output: this CP is relaced by the contracted one
*/
void CPTensor::inplaceModeIContraction(unsigned int index, Vec& u){
  // change the coefficients:
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double scalar;
    VecDot(m_terms[iTerm][index], u, &scalar);
    m_coeffs[iTerm] = m_coeffs[iTerm] * scalar;
  }

  // change the terms table
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    VecDestroy(&m_terms[iTerm][index]);
    m_terms[iTerm].erase(m_terms[iTerm].begin()+index);
  }

  vector<unsigned int> dofPerDim(m_nVar-1);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != index){
      dofPerDim[cc] = m_nDof_var[iVar];
      cc = cc + 1;
    }
  }
  m_nVar = m_nVar-1;
  m_nDof_var.clear();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
}


/* contract
  - input: the table of functions we test against, the index to be excluded
  - output: the vector result
*/
void CPTensor::contractEverythingExceptI(unsigned int index, vector<Vec>& uTable, Vec&result){
  assert(index<m_nVar);
  assert(uTable.size()==m_nVar-1);

  a.initVec(result, m_nDof_var[index], m_theComm);
  a.finalizeVec(result);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double a_i = m_coeffs[iTerm];
    unsigned int cc = 0;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != index){
        double scalar;
        VecDot(uTable[cc], m_terms[iTerm][iVar], &scalar);
        a_i = a_i * scalar;
        cc += 1;
      }
    }
    VecAXPY(result, a_i, m_terms[iTerm][index]);
  }
  a.finalizeVec(result);
}


/* greedy to perform approximation of a given CP up to tol
  - inputs: the tensor to be approximated, the mass matrices, the error
  - output: this tensor is the greedy approximation
*/
void CPTensor::greedy_ApproxOf(CPTensor& toBeApprox, vector<Mat>& mass, const double tol, const double tolFP = 1.0e-2){
  ofstream outfile;
  if(!m_CPName.empty()){
    string fileName = string("residual_") + m_CPName + string(".txt");
    outfile.open(fileName.c_str());
  }

  // define tolerance for Fix Point
  const double errFP = tolFP * tol;
  m_rank = 0;

  m_theComm = toBeApprox.theComm();
  m_nVar = toBeApprox.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    m_nDof_var[idVar] = toBeApprox.nDof_var(idVar);
  }
  m_isInitialised = true;

  // init linear solvers:
  vector<KSP>linSolver(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    KSP iKsp;
    a.initLinSolve(iKsp, mass[idVar], m_theComm);
    linSolver[idVar] = iKsp;
  }


  // init residual
  CPTensor residual;
  residual.copyTensorFrom(toBeApprox);
  double norm2Res = residual.norm2CP(mass);

  while(sqrt(norm2Res) > tol){
    PetscPrintf(m_theComm,"residual rank = %d\n", residual.rank());
    PetscPrintf(m_theComm,"residual norm = %f\n", residual.norm2CP(mass));


    // init random terms
    vector<Vec> termsToAdd(m_nVar);

    PetscRandom rctx;
    PetscRandomCreate(m_theComm,&rctx);
    PetscRandomSetSeed(rctx,std::chrono::system_clock::now().time_since_epoch().count());
    PetscRandomSeed(rctx);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      a.initVec(termsToAdd[idVar], m_nDof_var[idVar], m_theComm);
      a.finalizeVec(termsToAdd[idVar]);
      VecSetRandom(termsToAdd[idVar],rctx);
    }

    // copy for fix point check
    vector<Vec> termsToAdd_old(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      Vec thisTerm;
      a.initVec(thisTerm, m_nDof_var[idVar], m_theComm);
      VecCopy(termsToAdd[idVar], thisTerm);
      a.finalizeVec(thisTerm);
      termsToAdd_old[idVar] = thisTerm;
    }
    // starting Fix-Point for ALS iteration
    double testFP = 1e12*errFP;
    while(testFP > errFP){
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        double prod = 1.0;
        for(unsigned int jVar=0; jVar<m_nVar; jVar++){
          if(jVar != iVar){
            Vec prodJ;
            a.initVec(prodJ, m_nDof_var[jVar], m_theComm);
            a.finalizeVec(prodJ);
            MatMult(mass[jVar], termsToAdd[jVar], prodJ);
            double quadForm;
            VecDot(prodJ, termsToAdd[jVar], &quadForm);
            prod = prod * quadForm;
            VecDestroy(&prodJ);
          }
        }

        Vec rhs;
        a.initVec(rhs, m_nDof_var[iVar], m_theComm);
        a.finalizeVec(rhs);
        for(unsigned int kTerm=0; kTerm<residual.rank(); kTerm++){
          double prodK = 1.0;
          for(unsigned int jVar=0; jVar<m_nVar; jVar++){
            if(jVar != iVar){
              Vec prodJ;
              a.initVec(prodJ, m_nDof_var[jVar], m_theComm);
              a.finalizeVec(prodJ);
              MatMult(mass[jVar], termsToAdd[jVar], prodJ);
              double quadForm;
              VecDot(prodJ, residual.terms(kTerm,jVar), &quadForm);
              prodK = prodK * quadForm;
              VecDestroy(&prodJ);
            }
          }
          Vec prodVecI;
          a.initVec(prodVecI, m_nDof_var[iVar], m_theComm);
          a.finalizeVec(prodVecI);
          MatMult(mass[iVar], residual.terms(kTerm,iVar), prodVecI);
          prodK = prodK * residual.coeffs(kTerm);
          VecAXPY(rhs, prodK, prodVecI);
          VecDestroy(&prodVecI);
        }
        VecScale(rhs, 1.0/prod);

        // solve linear System
        a.solveLin(linSolver[iVar], rhs, termsToAdd[iVar], m_theComm, false);
        VecDestroy(&rhs);
      }
      CPTensor thisPureTens(termsToAdd, m_theComm);
      CPTensor oldPureTens(termsToAdd_old, m_theComm);
      oldPureTens.sum(thisPureTens, -1.0);
      double diffNorm2 = oldPureTens.norm2CP(mass);


      // copy termsToAdd
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        VecDestroy(&termsToAdd_old[idVar]);
        Vec thisTerm;
        a.initVec(thisTerm, m_nDof_var[idVar], m_theComm);
        VecCopy(termsToAdd[idVar], thisTerm);
        a.finalizeVec(thisTerm);
        termsToAdd_old[idVar] = thisTerm;
      }
      testFP = sqrt(diffNorm2);
      PetscPrintf(m_theComm, "testFP = %e \n", testFP);
    }
    PetscPrintf(m_theComm, "\n", testFP);

    // update the result:
    addPureTensorTerm(termsToAdd, 1.0);
    PetscPrintf(m_theComm,"Sol rank = %d\n",m_rank);

    // update residual:
    /*vector<Vec> updateResidual(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      a.initVec(updateResidual[idVar], m_nDof_var[idVar], m_theComm);
      a.finalizeVec(updateResidual[idVar]);
      MatMult(mass[idVar], termsToAdd[idVar], updateResidual[idVar]);
    }*/
    residual.addPureTensorTerm(termsToAdd, -1.0);

    PetscRandomDestroy(&rctx);

    norm2Res = residual.norm2CP(mass);
    PetscPrintf(m_theComm, "Norm2res = %e \n", norm2Res);
    PetscPrintf(m_theComm, "\n", testFP);
    PetscPrintf(m_theComm, "\n", testFP);

    // optimise the coefficients:
    Mat scalMat;
    a.initMat(scalMat,m_rank,m_rank,m_theComm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      for(unsigned int jTerm=iTerm; jTerm<m_rank; jTerm++){
        double val = scalarProdTerms(m_terms[iTerm], m_terms[jTerm], mass);
        a.setMatEl(scalMat,iTerm,jTerm,val,m_theComm);
        if(iTerm != jTerm){
          a.setMatEl(scalMat,jTerm,iTerm,val,m_theComm);
        }
      }
    }
    a.finalizeMat(scalMat);

    Vec scalVec;
    a.initVec(scalVec,m_rank, m_theComm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double entry = 0.0;
      for(unsigned int kTerm=0; kTerm<toBeApprox.rank(); kTerm++){
        vector<Vec> kTermVec = toBeApprox.terms(kTerm);
        double val = scalarProdTerms(m_terms[iTerm], kTermVec, mass);
        val = val * toBeApprox.coeffs(kTerm);
        entry = entry + val;
      }
      a.setVecEl(scalVec, iTerm, entry, m_theComm);
    }
    a.finalizeVec(scalVec);

    Vec updateCoeffs;
    a.initVec(updateCoeffs, m_rank, m_theComm);
    a.finalizeVec(updateCoeffs);

    KSP scalSolver;
    a.initLinSolve(scalSolver, scalMat, m_theComm);
    a.solveLin(scalSolver, scalVec, updateCoeffs, m_theComm, false);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double thisCoeff = a.getVecEl(updateCoeffs, iTerm, m_theComm);
      m_coeffs[iTerm] = thisCoeff;
      residual.set_coeffs(residual.rank() - m_rank + iTerm, -1.0*thisCoeff);
    }

    VecDestroy(&scalVec);
    VecDestroy(&updateCoeffs);
    MatDestroy(&scalMat);
    KSPDestroy(&scalSolver);

    norm2Res = residual.norm2CP(mass);
    PetscPrintf(m_theComm, "Corrected Norm2res = %e \n", norm2Res);
    PetscPrintf(m_theComm, "\n", testFP);
    PetscPrintf(m_theComm, "\n", testFP);
    if(!m_CPName.empty()){
      outfile << norm2Res << flush << endl;
    }
  }
  // destroy solvers
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    KSPDestroy(&linSolver[idVar]);
  }
  if(!m_CPName.empty()){
    outfile.close();
  }
}


/* greedy to perform approximation of a given CP up to tol, relaxed FP (!)
  - inputs: the tensor to be approximated, the mass matrices, the error, the relaxation parameter
  - output: this tensor is the greedy approximation of the given target CP
*/
void CPTensor::greedy_ApproxOf(CPTensor& toBeApprox, vector<Mat>& mass, const double tol, const double tolFP = 1.0e-2, const double alpha=1.0){
  ofstream outfile;
  if(!m_CPName.empty()){
    string fileName = string("residual_") + m_CPName + string(".txt");
    outfile.open(fileName.c_str());
  }

  // define tolerance for Fix Point
  const double errFP = tolFP * tol;
  m_rank = 0;

  m_theComm = toBeApprox.theComm();
  m_nVar = toBeApprox.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    m_nDof_var[idVar] = toBeApprox.nDof_var(idVar);
  }
  m_isInitialised = true;

  // init linear solvers:
  vector<KSP>linSolver(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    KSP iKsp;
    a.initLinSolve(iKsp, mass[idVar], m_theComm);
    linSolver[idVar] = iKsp;
  }


  // init residual
  CPTensor residual;
  residual.copyTensorFrom(toBeApprox);
  double norm2Res = residual.norm2CP(mass);
  if(!m_CPName.empty()){
    outfile << norm2Res << flush << endl;
  }

  while(sqrt(norm2Res) > tol){
    PetscPrintf(m_theComm,"residual rank = %d\n", residual.rank());
    PetscPrintf(m_theComm,"residual norm = %f\n", residual.norm2CP(mass));


    // init random terms
    vector<Vec> termsToAdd(m_nVar);

    PetscRandom rctx;
    PetscRandomCreate(m_theComm,&rctx);
    PetscRandomSetSeed(rctx,std::chrono::system_clock::now().time_since_epoch().count());
    PetscRandomSeed(rctx);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      a.initVec(termsToAdd[idVar], m_nDof_var[idVar], m_theComm);
      a.finalizeVec(termsToAdd[idVar]);
      VecSetRandom(termsToAdd[idVar],rctx);
    }

    // copy for fix point check
    vector<Vec> termsToAdd_old(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      Vec thisTerm;
      a.initVec(thisTerm, m_nDof_var[idVar], m_theComm);
      VecCopy(termsToAdd[idVar], thisTerm);
      a.finalizeVec(thisTerm);
      termsToAdd_old[idVar] = thisTerm;
    }
    // starting Fix-Point for ALS iteration
    double testFP = 1e12*errFP;
    while(testFP > errFP){
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        double prod = 1.0;
        for(unsigned int jVar=0; jVar<m_nVar; jVar++){
          if(jVar != iVar){
            Vec prodJ;
            a.initVec(prodJ, m_nDof_var[jVar], m_theComm);
            a.finalizeVec(prodJ);
            MatMult(mass[jVar], termsToAdd[jVar], prodJ);
            double quadForm;
            VecDot(prodJ, termsToAdd[jVar], &quadForm);
            prod = prod * quadForm;
            VecDestroy(&prodJ);
          }
        }

        Vec rhs;
        a.initVec(rhs, m_nDof_var[iVar], m_theComm);
        a.finalizeVec(rhs);
        for(unsigned int kTerm=0; kTerm<residual.rank(); kTerm++){
          double prodK = 1.0;
          for(unsigned int jVar=0; jVar<m_nVar; jVar++){
            if(jVar != iVar){
              Vec prodJ;
              a.initVec(prodJ, m_nDof_var[jVar], m_theComm);
              a.finalizeVec(prodJ);
              MatMult(mass[jVar], termsToAdd[jVar], prodJ);
              double quadForm;
              VecDot(prodJ, residual.terms(kTerm,jVar), &quadForm);
              prodK = prodK * quadForm;
              VecDestroy(&prodJ);
            }
          }
          Vec prodVecI;
          a.initVec(prodVecI, m_nDof_var[iVar], m_theComm);
          a.finalizeVec(prodVecI);
          MatMult(mass[iVar], residual.terms(kTerm,iVar), prodVecI);
          prodK = prodK * residual.coeffs(kTerm);
          VecAXPY(rhs, prodK, prodVecI);
          VecDestroy(&prodVecI);
        }
        VecScale(rhs, 1.0/prod);

        // solve linear System
        Vec tmpSol;
        a.initVec(tmpSol,m_nDof_var[iVar], m_theComm);
        a.solveLin(linSolver[iVar], rhs, tmpSol, m_theComm, false);
        VecScale(termsToAdd[iVar],1.0-alpha);
        VecScale(tmpSol, alpha);
        VecAXPY(termsToAdd[iVar], 1.0, tmpSol);

        VecDestroy(&rhs);
        VecDestroy(&tmpSol);
      }
      CPTensor thisPureTens(termsToAdd, m_theComm);
      CPTensor oldPureTens(termsToAdd_old, m_theComm);
      oldPureTens.sum(thisPureTens, -1.0);
      double diffNorm2 = oldPureTens.norm2CP(mass);


      // copy termsToAdd
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        VecDestroy(&termsToAdd_old[idVar]);
        Vec thisTerm;
        a.initVec(thisTerm, m_nDof_var[idVar], m_theComm);
        VecCopy(termsToAdd[idVar], thisTerm);
        a.finalizeVec(thisTerm);
        termsToAdd_old[idVar] = thisTerm;
      }
      testFP = sqrt(diffNorm2);
      PetscPrintf(m_theComm, "testFP = %e \n", testFP);
    }
    PetscPrintf(m_theComm, "\n", testFP);

    // update the result:
    addPureTensorTerm(termsToAdd, 1.0);
    PetscPrintf(m_theComm,"Sol rank = %d\n",m_rank);

    // update residual:
    /*vector<Vec> updateResidual(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      a.initVec(updateResidual[idVar], m_nDof_var[idVar], m_theComm);
      a.finalizeVec(updateResidual[idVar]);
      MatMult(mass[idVar], termsToAdd[idVar], updateResidual[idVar]);
    }*/
    residual.addPureTensorTerm(termsToAdd, -1.0);

    PetscRandomDestroy(&rctx);

    norm2Res = residual.norm2CP(mass);
    PetscPrintf(m_theComm, "Norm2res = %e \n", norm2Res);
    PetscPrintf(m_theComm, "\n", testFP);
    PetscPrintf(m_theComm, "\n", testFP);

    // optimise the coefficients:
    Mat scalMat;
    a.initMat(scalMat,m_rank,m_rank,m_theComm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      for(unsigned int jTerm=iTerm; jTerm<m_rank; jTerm++){
        double val = scalarProdTerms(m_terms[iTerm], m_terms[jTerm], mass);
        a.setMatEl(scalMat,iTerm,jTerm,val,m_theComm);
        if(iTerm != jTerm){
          a.setMatEl(scalMat,jTerm,iTerm,val,m_theComm);
        }
      }
    }
    a.finalizeMat(scalMat);

    Vec scalVec;
    a.initVec(scalVec,m_rank, m_theComm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double entry = 0.0;
      for(unsigned int kTerm=0; kTerm<toBeApprox.rank(); kTerm++){
        vector<Vec> kTermVec = toBeApprox.terms(kTerm);
        double val = scalarProdTerms(m_terms[iTerm], kTermVec, mass);
        val = val * toBeApprox.coeffs(kTerm);
        entry = entry + val;
      }
      a.setVecEl(scalVec, iTerm, entry, m_theComm);
    }
    a.finalizeVec(scalVec);

    Vec updateCoeffs;
    a.initVec(updateCoeffs, m_rank, m_theComm);
    a.finalizeVec(updateCoeffs);

    KSP scalSolver;
    a.initLinSolve(scalSolver, scalMat, m_theComm);
    a.solveLin(scalSolver, scalVec, updateCoeffs, m_theComm, false);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double thisCoeff = a.getVecEl(updateCoeffs, iTerm, m_theComm);
      m_coeffs[iTerm] = thisCoeff;
      residual.set_coeffs(residual.rank() - m_rank + iTerm, -1.0*thisCoeff);
    }

    VecDestroy(&scalVec);
    VecDestroy(&updateCoeffs);
    MatDestroy(&scalMat);
    KSPDestroy(&scalSolver);

    norm2Res = residual.norm2CP(mass);
    PetscPrintf(m_theComm, "Corrected Norm2res = %e \n", norm2Res);
    PetscPrintf(m_theComm, "\n", testFP);
    PetscPrintf(m_theComm, "\n", testFP);
    if(!m_CPName.empty()){
      outfile << norm2Res << flush << endl;
    }
  }
  // destroy solvers
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    KSPDestroy(&linSolver[idVar]);
  }
  if(!m_CPName.empty()){
    outfile.close();
  }
}


/* CPTT Approximation of full tensor
  - input: the full tensor to be approximated, the tolerance
  - output: this CP tensor is the CPTT aproximation of the tensor
*/
void CPTensor::CPTT_ApproxOf(fullTensor& toBeApprox, double tol){

  m_theComm = toBeApprox.theComm();
  m_nVar = toBeApprox.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = toBeApprox.nDof_var(iVar);
  }
  m_rank = 0;
  m_isInitialised = true;

  fullTensor residual;
  residual.copyTensorFrom(toBeApprox);

  double residualNorm;
  VecNorm(residual.tensorEntries(), NORM_2, &residualNorm);
  PetscPrintf(m_theComm,"Residual l2 norm = %f \n", residualNorm);
  while(residualNorm > tol){
    vector<Vec> kThTerm;
    double kThCoeff;
    compute_CPTT_Term(residual, kThTerm, kThCoeff);

    // update solution (this CP)
    addPureTensorTerm(kThTerm, kThCoeff);

    // update residual:
    fullTensor thisTerm(m_nVar, m_nDof_var, m_theComm);
    thisTerm.init();
    for(unsigned int l=0;l<thisTerm.nEntries(); l++){
      vector<unsigned int> ind = thisTerm.lin2sub(l);
      double value = kThCoeff;
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        double toMultiply = a.getVecEl(kThTerm[iVar], ind[iVar], m_theComm);
        value = value * toMultiply;
      }
      thisTerm.setTensorElement(l, value);
    }

    residual.sum(thisTerm, -1.0);
    VecNorm(residual.tensorEntries(), NORM_2, &residualNorm);
    PetscPrintf(m_theComm, "Residual l2 norm = %f \n", residualNorm);
  }
}


/* CPTT Approximation of a CP
  - input: the full tensor to be approximated, the tolerance
  - output: this CP tensor is the CPTT aproximation of the tensor
*/
void CPTensor::CPTT_ApproxOf(CPTensor& toBeApprox, double tol, bool USE_CORRECTION=false){
  ofstream outfile;
  if(!m_CPName.empty()){
    string fileName = string("residual_") + m_CPName + string(".txt");
    outfile.open(fileName.c_str());
  }

  m_theComm = toBeApprox.theComm();
  m_nVar = toBeApprox.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = toBeApprox.nDof_var(iVar);
  }
  m_rank = 0;
  m_isInitialised = true;

  vector<Mat> mass(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    mass[idVar] = a.eye(m_nDof_var[idVar], PETSC_COMM_WORLD);
  }

  CPTensor residual;
  residual.copyTensorFrom(toBeApprox);

  double residualNorm = residual.norm2CP();
  if(!m_CPName.empty()){
    outfile << residualNorm << flush << endl;
  }
  PetscPrintf(m_theComm,"Residual l2 norm = %f \n", residualNorm);
  while(sqrt(residualNorm) > tol){
    vector<Vec> kThTerm;
    double kThCoeff;
    compute_CPTT_Term(residual, kThTerm, kThCoeff);

    // one cycle correction term:
    if(USE_CORRECTION){
      const double alpha = 0.20;
      double errFP = 1.0e12;
      //for(unsigned int it=0; it<5; it++){
      unsigned int it = 0;
      while( (errFP>1.0e-5) && (it<6)){
        errFP = 0.0;
        for(unsigned int jVar=0; jVar<m_nVar; jVar++){
          vector<Vec> uTable(m_nVar-1);
          unsigned int cc = 0;
          for(unsigned int iVar=0; iVar<m_nVar; iVar++){
            if(iVar!=jVar){
              uTable[cc] = kThTerm[iVar];
              cc += 1;
            }
          }

          Vec copyOfKt;
          a.initVec(copyOfKt, m_nDof_var[jVar], m_theComm);
          VecCopy(kThTerm[jVar], copyOfKt);
          a.finalizeVec(copyOfKt);

          // compute the update (relaxed fix-point)
          Vec u_star;
          residual.contractEverythingExceptI(jVar, uTable, u_star);
          VecScale(u_star, 1.0/kThCoeff);
          VecScale(kThTerm[jVar], (1.0-alpha));
          VecAXPY(kThTerm[jVar], alpha, u_star);

          VecAXPY(copyOfKt, -1.0, kThTerm[jVar]);
          double thisNorm;
          VecNorm(copyOfKt, NORM_2, &thisNorm);
          errFP += thisNorm*thisNorm;
          VecDestroy(&copyOfKt);
        }
        errFP = sqrt(errFP);
        cout << errFP << endl;
        it += 1;
        if(it==100){puts("Maximum number of iteration reached!");}
      }
    }

    // update solution (this CP)
    addPureTensorTerm(kThTerm, kThCoeff);

    // update residual:

    double resCoeff = -kThCoeff;
    residual.addPureTensorTerm(kThTerm, resCoeff);
    residualNorm = residual.norm2CP();
    PetscPrintf(m_theComm,"Residual l2 norm = %f \n", residualNorm);


    // optimise the coefficients:
    Mat scalMat;
    a.initMat(scalMat,m_rank,m_rank,m_theComm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      for(unsigned int jTerm=iTerm; jTerm<m_rank; jTerm++){
        double val = scalarProdTerms(m_terms[iTerm], m_terms[jTerm], mass);
        a.setMatEl(scalMat,iTerm,jTerm,val,m_theComm);
        if(iTerm != jTerm){
          a.setMatEl(scalMat,jTerm,iTerm,val,m_theComm);
        }
      }
    }
    a.finalizeMat(scalMat);

    Vec scalVec;
    a.initVec(scalVec,m_rank, m_theComm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double entry = 0.0;
      for(unsigned int kTerm=0; kTerm<toBeApprox.rank(); kTerm++){
        vector<Vec> kTermVec = toBeApprox.terms(kTerm);
        double val = scalarProdTerms(m_terms[iTerm], kTermVec, mass);
        val = val * toBeApprox.coeffs(kTerm);
        entry = entry + val;
      }
      a.setVecEl(scalVec, iTerm, entry, m_theComm);
    }
    a.finalizeVec(scalVec);

    Vec updateCoeffs;
    a.initVec(updateCoeffs, m_rank, m_theComm);
    a.finalizeVec(updateCoeffs);

    KSP scalSolver;
    a.initLinSolve(scalSolver, scalMat, m_theComm);
    a.solveLin(scalSolver, scalVec, updateCoeffs, m_theComm, false);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double thisCoeff = a.getVecEl(updateCoeffs, iTerm, m_theComm);
      m_coeffs[iTerm] = thisCoeff;
      residual.set_coeffs(residual.rank() - m_rank + iTerm, -1.0*thisCoeff);
    }

    VecDestroy(&scalVec);
    VecDestroy(&updateCoeffs);
    MatDestroy(&scalMat);
    KSPDestroy(&scalSolver);

    residualNorm = residual.norm2CP(mass);
    PetscPrintf(m_theComm, "Corrected Norm2res = %f \n\n", residualNorm);
    if(!m_CPName.empty()){
      outfile << residualNorm << flush << endl;
    }
  }
  if(!m_CPName.empty()){
    outfile.close();
  }
}


/* CPTT Approximation of a CP without optimisation of the coefficients
  - input: the full tensor to be approximated, the tolerance
  - output: this CP tensor is the CPTT aproximation of the tensor
*/
void CPTensor::CPTT_ApproxOf(CPTensor& toBeApprox, double tol){
  ofstream outfile;
  if(!m_CPName.empty()){
    string fileName = string("residual_") + m_CPName + string(".txt");
    outfile.open(fileName.c_str());
  }

  m_theComm = toBeApprox.theComm();
  m_nVar = toBeApprox.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = toBeApprox.nDof_var(iVar);
  }
  m_rank = 0;
  m_isInitialised = true;

  vector<Mat> mass(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    mass[idVar] = a.eye(m_nDof_var[idVar], PETSC_COMM_WORLD);
  }

  CPTensor residual;
  residual.copyTensorFrom(toBeApprox);

  double residualNorm = residual.norm2CP();
  if(!m_CPName.empty()){
    outfile << residualNorm << flush << endl;
  }
  PetscPrintf(m_theComm,"Residual l2 norm = %f \n", residualNorm);
  while(sqrt(residualNorm) > tol){
    vector<Vec> kThTerm;
    double kThCoeff;
    compute_CPTT_Term(residual, kThTerm, kThCoeff);



    // update solution (this CP)
    addPureTensorTerm(kThTerm, kThCoeff);

    // update residual:

    double resCoeff = -kThCoeff;
    residual.addPureTensorTerm(kThTerm, resCoeff);
    residualNorm = residual.norm2CP();
    PetscPrintf(m_theComm,"Residual l2 norm = %f \n", residualNorm);


    if(!m_CPName.empty()){
      outfile << residualNorm << flush << endl;
    }
  }
  if(!m_CPName.empty()){
    outfile.close();
  }
}


/* compute unfolding
  - input: this CP
  - output: the matrix is filled with the unfolding
*/
void CPTensor::computeUnfolding(const unsigned int index, Mat& M){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
	a.initMat(M, nRows, nCols, m_theComm);
  a.finalizeMat(M);

  //define the unfolding increments
  vector<unsigned int> unfoldInc(m_nVar-1);
  unsigned int unfInc = 1;
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar!=index){
      unfoldInc[cc] = unfInc;
      unfInc = unfInc * m_nDof_var[idVar];
      cc = cc + 1;
    }
  }

  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    Mat thisTermUnfold;
    a.initMat(thisTermUnfold, nRows, nCols, m_theComm);
    // filling the pure tensor term unfolding:
    for(unsigned int i=0; i<nRows; i++){
      double term_ij = a.getVecEl(m_terms[iTerm][index], i, m_theComm);
      for(unsigned int j=0; j<nCols; j++){
        unsigned int reminder = j;
        cc = m_nVar-2;
        for (int h=m_nVar-1; h>=0; h--){
            if(h != index){
              unsigned int hThIndex = reminder/unfoldInc[cc];
              term_ij = term_ij * a.getVecEl(m_terms[iTerm][h], hThIndex, m_theComm);
              reminder = reminder - hThIndex * unfoldInc[cc];
              cc = cc - 1;
            }
        };
        a.setMatEl(thisTermUnfold, i, j, term_ij, m_theComm);
      }
    }
    a.finalizeMat(thisTermUnfold);
    MatAXPY(M, m_coeffs[iTerm], thisTermUnfold , DIFFERENT_NONZERO_PATTERN);
    MatDestroy(&thisTermUnfold);
  }
	a.finalizeMat(M);
}


/* eval function
  - input: the multi-index
  - output: the value
*/
void CPTensor::eval(const vector<unsigned int>& ind, double& val){
  val = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double toAdd = m_coeffs[iTerm];
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      toAdd = toAdd * a.getVecEl(m_terms[iTerm][idVar], ind[idVar], m_theComm);
    }
    val = val + toAdd;
  }
}



/* Save CPTT

*/
void CPTensor::save(string& fileName){
  ofstream coeffFile;
  string coeffName = fileName + string("_coeffs.txt");
  coeffFile.open(coeffName.c_str());
  for (unsigned int iTerm = 0; iTerm < m_rank; iTerm++) {
    coeffFile << m_coeffs[iTerm] << endl;
  }
  coeffFile.close();

  for (unsigned int iTerm = 0; iTerm < m_rank; iTerm++) {
    for (unsigned int iVar = 0; iVar < m_nVar; iVar++) {
      ofstream modeFile;
      string modeName = fileName + string("_modes_") + to_string(iTerm) + string("_") + to_string(iVar) + string(".txt");
      modeFile.open(modeName.c_str());
      Vec thisTerm = m_terms[iTerm][iVar];
      for (unsigned int iDof = 0; iDof < m_nDof_var[iVar]; iDof++) {
        modeFile << a.getVecEl(thisTerm, iDof, m_theComm) << endl;
      }
      modeFile.close();
    }
  }


}
