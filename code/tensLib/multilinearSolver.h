// Class of multi-linear solvers

#ifndef multilinearSolver_h
#define multilinearSolver_h

#include "tensor.h"
#include "CPTensor.h"
#include "genericInclude.h"
#include "operatorTensor.h"
#include "fullTensor.h"

using namespace std;

class multilinearSolver{

private:
   auxiliary a;
public:
  multilinearSolver(){};
  ~multilinearSolver(){};

  // list of methods, actual solvers:
  void CG(operatorTensor&, CPTensor&, CPTensor&, double);
  void CG(operatorTensor&, CPTensor&, CPTensor&, CPTensor&, double);
  void CG_recompress(operatorTensor&, CPTensor&, CPTensor&, double);
  void fixPoint(operatorTensor&, CPTensor&, CPTensor&, double);
  void fixPoint(operatorTensor&, CPTensor&, CPTensor&, CPTensor&, double);
  void fixPoint_compress(operatorTensor&, CPTensor&, CPTensor&, double);

  // Solving with CPTT:
  void solvewithCPTT(operatorTensor&, CPTensor&, CPTensor&, double);
  void solvewithCPTT(operatorTensor&, CPTensor&, CPTensor&, CPTensor&, double);
  void solvewithCPTT_recompress(operatorTensor&, CPTensor&, CPTensor&, double);
  void solvewithCPTT_precond(operatorTensor&, operatorTensor&, CPTensor&, CPTensor&, double);
  void solveCPTT_CG(operatorTensor&, CPTensor&, CPTensor&, double, bool);
  void solveCPTT_CG(operatorTensor&, CPTensor&, CPTensor&, CPTensor&, double, bool);
  void solveCPTT_CG_precond(operatorTensor&, operatorTensor&, CPTensor&, CPTensor&, double);
  void solveCPTT_BiCGStab(operatorTensor&, CPTensor&, CPTensor&, double);
  void solve_CPTT_fixPoint(operatorTensor&, CPTensor&, CPTensor&, double);
};



#endif
