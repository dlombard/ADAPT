// implementation of the mather class tensor. Header
// tensor is a generic class, containing the attributes of all kinds of tensors.

#ifndef tensor_h
#define tensor_h

#include "genericInclude.h"
#include "auxiliary.h"


using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:

class tensor{

	protected:

  MPI_Comm m_theComm;
	int m_nVar; // Number of variables = the tensor dimension
	vector<unsigned int> m_nDof_var; //Dimension of the discretised spaces in each variable
	auxiliary a; // class containing all the auxiliary operations;

	public:

	// -- CONSTRUCTORS and DESTRUCTOR --
	tensor(){};
	~tensor(){};


	// -- ACCESS FUNCTIONS --
	inline MPI_Comm theComm(){return m_theComm;};
  inline  int nVar(){return m_nVar;};
  inline  vector<unsigned int> nDof_var(){return m_nDof_var;};
  inline  int nDof_var(int j){assert(j<m_nVar); return m_nDof_var[j];};

  // -- SETTERS --
	inline void setTensorComm(MPI_Comm theComm){m_theComm = theComm;};
  inline void set_nVar(int nVar){m_nVar = nVar;};
  inline void set_nDof_var(vector<unsigned int>& vec){assert(vec.size()==m_nVar);
    m_nDof_var.resize(vec.size());
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      m_nDof_var[idVar] = vec[idVar];
    }
  }
	// if already initialised!
  inline void set_nDof_var(int j, unsigned int val){assert(j<m_nVar); m_nDof_var[j]=val;};

  // -- METHODS --

	// - Compute unfolding -
	virtual void computeUnfolding(const unsigned int, Mat&){};
	//vector<int> compute_multi_index(const int&, const int&, const int&);

	//-- Evaluation of the tensor entries --
	virtual void eval(const vector<unsigned int>&, double&){};
};


#endif
