//implementation of operatorTensor class:

#include "operatorTensor.h"
using namespace std;



// Overloaded Constructor:

// -- empty resized operator --
operatorTensor::operatorTensor(unsigned int nTerms, unsigned int dim, MPI_Comm theComm){
    m_numTerms = nTerms;
    m_nVar = dim;
    m_theComm = theComm;

    m_op.resize(nTerms);
    for(unsigned int iTerm=0; iTerm<m_numTerms; iTerm++){
      m_op[iTerm].resize(m_nVar);
    }
}

// -- one term tensorised operator --
operatorTensor::operatorTensor(vector<Mat>& oneTermOp, MPI_Comm theComm){
  m_numTerms = 1;
  m_nVar = oneTermOp.size();
  m_theComm = theComm;

  m_op.push_back(oneTermOp);

}

// -- assign the operator --
operatorTensor::operatorTensor(vector<vector<Mat> >& Op, MPI_Comm theComm){
  m_numTerms = Op.size();
  m_nVar = Op[0].size();
  m_theComm = theComm;

  m_op = Op;
}


// -- apply the operator --
// input => non-empty CP;
// output => an empty CP;
void operatorTensor::applyOperator(CPTensor& input, CPTensor& result){
  assert(m_nVar == input.nVar());
  MPI_Comm theComm = input.theComm();
  unsigned int nTermsResult = input.rank() * m_numTerms;

  result.setTensorComm(theComm);
  result.set_rank(nTermsResult);
  result.set_nVar(m_nVar);
  result.setIsInitialised(true);

  vector<unsigned int> dofPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    int nRows, nCols;
    MatGetSize(m_op[0][iVar], &nRows, &nCols);
    assert(nCols == input.nDof_var(iVar));
    dofPerDim[iVar] = nRows;
  }
  result.set_nDof_var(dofPerDim);

  // init result fields:
  vector<double> coeffs(nTermsResult);
  vector<vector<Vec> > terms(nTermsResult);
  unsigned int cc = 0;
  for(unsigned int iTerm=0; iTerm<input.rank(); iTerm++){

    for(unsigned int lTerm=0; lTerm<m_numTerms; lTerm++){

      vector<Vec> thisTerm(m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        Vec prod;
        a.initVec(prod, result.nDof_var(iVar), theComm);
        a.finalizeVec(prod);
        MatMult(m_op[lTerm][iVar], input.terms(iTerm,iVar), prod);
        thisTerm[iVar] = prod;
      }
      coeffs[cc] = input.coeffs(iTerm);
      terms[cc] = thisTerm;
      cc = cc + 1;
    }
  }
  result.set_terms(terms);
  result.set_coeffs(coeffs);
}



// -- apply the operator, overloaded function
// input => non-empty CP;
// output => the resulting CP;
CPTensor operatorTensor::applyOperator(CPTensor& input){
  assert(m_nVar == input.nVar());
  MPI_Comm theComm = input.theComm();
  unsigned int nTermsResult = input.rank() * m_numTerms;

  CPTensor result;
  result.setTensorComm(theComm);
  result.set_rank(nTermsResult);
  result.set_nVar(m_nVar);
  result.setIsInitialised(true);

  vector<unsigned int> dofPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    int nRows, nCols;
    MatGetSize(m_op[0][iVar], &nRows, &nCols);
    assert(nCols == input.nDof_var(iVar));
    dofPerDim[iVar] = nRows;
  }
  result.set_nDof_var(dofPerDim);

  // init result fields:
  vector<double> coeffs(nTermsResult);
  vector<vector<Vec> > terms(nTermsResult);

  unsigned int cc = 0;
  for(unsigned int iTerm=0; iTerm<input.rank(); iTerm++){

    for(unsigned int lTerm=0; lTerm<m_numTerms; lTerm++){

      vector<Vec> thisTerm(m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        Vec prod;
        a.initVec(prod, result.nDof_var(iVar), theComm);
        a.finalizeVec(prod);
        MatMult(m_op[lTerm][iVar], input.terms(iTerm,iVar), prod);
        thisTerm[iVar] = prod;
      }

      terms[cc] = thisTerm;
      coeffs[cc] = input.coeffs(iTerm);
      cc = cc + 1;
    }
  }
  result.set_terms(terms);
  result.set_coeffs(coeffs);
  return result;
}


//Adjoint of the OperatorTensor itself
operatorTensor operatorTensor::adjoint(){
  operatorTensor adjA(m_numTerms, m_nVar, m_theComm);
  for (unsigned int iTerm = 0; iTerm < m_numTerms; iTerm++) {
    for (unsigned int iVar = 0; iVar < m_nVar; iVar++) {
       Mat adj = a.transpose(m_op[iTerm][iVar], m_theComm);
       adjA.set_term(iTerm, iVar, adj);
    }
  }
  return adjA;
};
