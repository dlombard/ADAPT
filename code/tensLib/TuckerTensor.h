// class Tucker tensor, daughter of tensor, header file.
#ifndef TuckerTensor_h
#define TuckerTensor_h

#include "tensor.h"
#include "fullTensor.h"
using namespace std;

class TuckerTensor : public tensor{
private:
  vector<vector<Vec> > m_modes;
  vector<unsigned int> m_rank;
  fullTensor m_core;
  bool m_isInitialised;
public:

  // Constructor and Destructor
  TuckerTensor(){};
  TuckerTensor(vector<vector<Vec> >&, fullTensor&, MPI_Comm);
  ~TuckerTensor(){};

  // ACCESS FUNCTIONS:
  inline vector<vector<Vec> > modes(){return m_modes;};
  inline vector<Vec> modes(unsigned int idVar){assert( idVar<m_nVar); return m_modes[idVar];};
  inline Vec modes(unsigned int idVar, unsigned int idMode){
    assert( idVar<m_nVar);
    assert(idMode<m_rank[idVar]);
    return (m_modes[idVar])[idMode];
  }
  inline vector<unsigned int> rank(){return m_rank;};
  inline unsigned int rank(int idVar){assert(idVar<m_nVar); return m_rank[idVar];};
  inline fullTensor core(){return m_core;};
  inline bool isInitialised(){return m_isInitialised;};

  // SETTERS:
  inline void set_rank(vector<unsigned int>& tuckerRank){
    assert(tuckerRank.size()<m_nVar);
    m_rank = tuckerRank;
  }

  inline void setIsInitialised(bool state){
		m_isInitialised = state;
	}

  // COPY Tensor Structure:
	void copyTensorStructFrom(TuckerTensor&);
	void copyTensorStructTo(TuckerTensor&);

	// COPY the full tensor:
	void copyTensorFrom(TuckerTensor&);
	void copyTensorTo(TuckerTensor&);


  // OPERATIONS:
  bool hasSameStructure(TuckerTensor&);
  void multiplyByScalar(double);
  void HOSVD_ApproxOf(fullTensor&);

  // Eval function:
  void eval(const vector<unsigned int>& ind, double& val);


};

#endif
