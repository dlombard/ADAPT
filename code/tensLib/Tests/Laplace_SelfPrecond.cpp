// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "auxiliary.h"
#include "fullTensor.h"
#include "CPTensor.h"
#include "multilinearSolver.h"
using namespace std;

/* 4 -- saving field in VTK:
  - input: a 3d tensor
  - output: a vtk file
*/
void saveField(fullTensor& T, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = T.nDof_var(0);
 unsigned int ny = T.nDof_var(1);
 unsigned int nz = T.nDof_var(2);
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;
 for(unsigned int h=0; h<T.nEntries(); h++){
     double value = T.tensorEntries(h);
     if(fabs(value) < 1.0e-9){value = 0.0;};
     outfile << value << endl;
 }
 outfile.close();
}


// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

    // define auxiliary Petsc operations
    auxiliary a;

    //Random numbers
    unsigned long int testIdSeed = (unsigned long int) args[1];
    unsigned long int theSeed = std::chrono::system_clock::now().time_since_epoch().count();
    theSeed = theSeed + testIdSeed;
    std::default_random_engine coeffGenerator;
    coeffGenerator.seed(theSeed);
    std::uniform_real_distribution<double> distribution(-1.0,1.0);
    srand(time(NULL));




    //CPapprox  in d-dimension
    unsigned int dim=4;
    vector<unsigned int> resPerDim(dim);
    for (unsigned int i = 0; i < dim; i++) {
      resPerDim[i]=64;
    }

    // Defining the Laplacian in weak form, H^1_0
    vector<Mat> Masses(dim);
    vector<Mat> Stiffneses(dim);

    //Compute mass matrices
    for (unsigned int iVar = 0; iVar < dim; iVar++) {
      Mat thisMatrix;
      const double h = static_cast<double>(1.0/(resPerDim[iVar]-1));
      a.initMat(thisMatrix, resPerDim[iVar]-2 , resPerDim[iVar]-2, PETSC_COMM_WORLD);
      for (unsigned int iDof = 0; iDof < resPerDim[iVar]-2; iDof++) {
        a.setMatEl(thisMatrix, iDof, iDof, (2.0 * h)/3.0, PETSC_COMM_WORLD);
        if(iDof > 0) {
          a.setMatEl(thisMatrix, iDof, iDof-1, h/6.0, PETSC_COMM_WORLD);
        }
        if (iDof < resPerDim[iVar]-3){
        a.setMatEl(thisMatrix, iDof, iDof+1, h/6.0, PETSC_COMM_WORLD);
      }
      }
      a.finalizeMat(thisMatrix);
      Masses[iVar]= thisMatrix;
    }

    //Compute stiffness Matrices
    for (unsigned int iVar = 0; iVar < dim; iVar++) {
      Mat thisMatrix;
      const double h = static_cast<double>(1.0/(resPerDim[iVar]-1));
      a.initMat(thisMatrix, resPerDim[iVar]-2 , resPerDim[iVar]-2, PETSC_COMM_WORLD);
      for (unsigned int iDof = 0; iDof < resPerDim[iVar]-2; iDof++) {
        a.setMatEl(thisMatrix, iDof, iDof, 2.0/h, PETSC_COMM_WORLD);
        if(iDof > 0) {
          a.setMatEl(thisMatrix, iDof, iDof-1, -1.0/h, PETSC_COMM_WORLD);
        }
        if (iDof < resPerDim[iVar]-3){
        a.setMatEl(thisMatrix, iDof, iDof+1, -1.0/h, PETSC_COMM_WORLD);
      }
      }
      a.finalizeMat(thisMatrix);
      Stiffneses[iVar]= thisMatrix;
    }


    //Constructing the Laplacian
    vector<vector<Mat> > LaplacianTerms(dim);
    for (unsigned int iTerm = 0; iTerm < dim; iTerm++) {
      vector<Mat> thisTerm(dim);
      for(unsigned int iVar = 0; iVar < dim; iVar++ ){
        if (iVar == iTerm){
          thisTerm[iVar] = Stiffneses[iVar];
        } else {
          thisTerm[iVar] = Masses[iVar];
        }
      }
      LaplacianTerms[iTerm] = thisTerm;
    }
    operatorTensor Laplacian(LaplacianTerms, PETSC_COMM_WORLD);
    PetscPrintf(PETSC_COMM_WORLD,"Laplacian op has been computed \n\n");

    // Computation of the rhs
    vector<unsigned int> kappaMax(dim);
    unsigned int kMax=6;
    for (unsigned int i = 0; i < dim; i++) {
      kappaMax[i]=kMax;
    }

    std::default_random_engine coeffGeneratorL;
    coeffGeneratorL.seed(theSeed);
    uniform_int_distribution<int> distributionL(1.0,kMax);
    srand(time(NULL));

    fullTensor K(dim, kappaMax, PETSC_COMM_WORLD);

    unsigned int nOfTerms = 100;
    const double beta = 3.1;
    const double pi = std::atan(1.0)*4.0;


    CPTensor F(dim, resPerDim, PETSC_COMM_WORLD);

    vector<double> waveAmplitudes(nOfTerms);
    vector<double> kappa(dim);
    vector<vector<Vec> > terms(nOfTerms);

    for (unsigned int iTerm = 0; iTerm < nOfTerms ; iTerm++) {
      double kappasq = 0.0;
      vector<unsigned int> ind = K.lin2sub(iTerm);
      for(unsigned int iVar=0;iVar<dim;iVar++){
        kappa[iVar] = distributionL(coeffGeneratorL);
        kappasq += kappa[iVar] * kappa[iVar];
      }
      double lambda_tilde=sqrt(kappasq);
      double lambda= lambda_tilde/sqrt(dim);
      double alpha = distribution(coeffGenerator);
      double aCoef = alpha/(pow(lambda,beta));
      waveAmplitudes[iTerm] = pi*pi*kappasq*aCoef;

      vector<Vec> thisTerm(dim);
      for(unsigned int iVar=0; iVar<dim; iVar++){
        Vec fun;
        a.initVec(fun, resPerDim[iVar]-2, PETSC_COMM_WORLD);
        for(unsigned int jDof=1; jDof<resPerDim[iVar]-1; jDof++){
          double x = 1.0* jDof/(resPerDim[iVar]-1);
          double value = sin(pi*kappa[iVar]*x);
          a.setVecEl(fun, jDof-1, value, PETSC_COMM_WORLD);
        }
        a.finalizeVec(fun);
        thisTerm[iVar] = fun;
      }
      terms[iTerm] = thisTerm;
    }
    F.set_rank(nOfTerms);
    F.set_coeffs(waveAmplitudes);
    F.set_terms(terms);
    PetscPrintf(PETSC_COMM_WORLD,"Tensor has been built \n\n");
    double residualNorm = sqrt(F.norm2CP());
    cout << "Norm of F " << residualNorm << '\n';

    //Computing the right part
    vector<vector<Mat> > MassOpTerm(1);
      vector<Mat> thisTerm(dim);
      for(unsigned int iVar = 0; iVar < dim; iVar++ ){
          thisTerm[iVar] = Masses[iVar];
        }
      MassOpTerm[0] = thisTerm;

    operatorTensor MassOp(MassOpTerm,PETSC_COMM_WORLD);

    CPTensor FOp = MassOp.applyOperator(F);
    PetscPrintf(PETSC_COMM_WORLD,"FOp has been computed. Solving... \n\n");
    double residualNormOp = sqrt(FOp.norm2CP());


    //Solving with CPTT
    multilinearSolver mysolver;
    CPTensor CPsol(dim, resPerDim, PETSC_COMM_WORLD);
    string outFileName = "solvewithCPTT_Laplace64_4_3.1_" + string(args[1]); //solvewithCPTT_dim_beta_
    CPsol.set_CPName(outFileName);

    cout << "Starting self-preconditioned\n";
    //mysolver.solvewithCPTT(Laplacian, FOp, CPsol, residualNormOp * 1.0e-3);
    //mysolver.solve_CPTT_fixPoint(Laplacian, FOp, CPsol, residualNormOp * 1.0e-3);
    mysolver.fixPoint_compress(Laplacian, FOp, CPsol, residualNormOp * 1.0e-3);

    /*ofstream chara;
    string charaName = string("Characteristics_") + outFileName + string(".log");
    chara.open(charaName.c_str());
    chara << "Dim= " << dim << endl;
    chara << "Degrees of freedom= " << resPerDim[0] <<endl;
    chara << "Beta= " << beta << endl;
    chara << "Seed= " << theSeed << endl;
    chara.close(); */
/*
    //Solving with CG and approximating with CPTT
    CPTensor CGsol;
    multilinearSolver myCGsolver;
    myCGsolver.CG(Laplacian, FOp, CGsol, 1.0e-9);
    //CPTT method
    PetscPrintf(PETSC_COMM_WORLD,"Starting CPTT: \n\n");
    string outFileName = "approxwithCPTT_8_5.1_" + string(args[1]); //solvewithCPTT_dim_beta_
    CGsol.set_CPName(outFileName);
    CGsol.CPTT_ApproxOf(CGsol, 1.0e-2, false);

  */  // Finalize the MPI environment.
    SlepcFinalize();
    return 0;
}
