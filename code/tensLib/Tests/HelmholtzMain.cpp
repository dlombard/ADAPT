// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "auxiliary.h"
#include "fullTensor.h"
#include "CPTensor.h"
#include "multilinearSolver.h"
using namespace std;

/* 4 -- saving field in VTK:
  - input: a 3d tensor
  - output: a vtk file
*/
void saveField(fullTensor& T, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = T.nDof_var(0);
 unsigned int ny = T.nDof_var(1);
 unsigned int nz = T.nDof_var(2);
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;
 for(unsigned int h=0; h<T.nEntries(); h++){
     double value = T.tensorEntries(h);
     if(fabs(value) < 1.0e-9){value = 0.0;};
     outfile << value << endl;
 }
 outfile.close();
}





/* Importing full tensor from FROSTT repository.
  - Input: file name, reference to an empty full tensor, number of entries per line
  - Output: initialise and fill the full tensor.
*/
void importFrostt(fullTensor& T, string& fName, unsigned int nInLine, MPI_Comm theComm){
  T.setTensorComm(theComm);
  auxiliary a;
  unsigned int dim = nInLine - 1;

  ifstream inputFile;
  inputFile.open(fName.c_str());
  char output[128];
  unsigned int IthMode;
  double tensorValue;

  unsigned int cc = 0;

  vector<double>* values;
  values = new vector<double>;

  vector<vector<int> >* tab;
  tab = new vector<vector<int> >;

  vector<int> maxModes(nInLine-1, 0); // assuming modes are positive in the tensor!!
  vector<int> minModes(nInLine-1, 16777216);  // 2^24

  if (inputFile.is_open()) {
    cout << "Reading...";
    while (!inputFile.eof()) {
      inputFile >> output;
      stringstream str;
      str << output;
      vector<int> thisLine(nInLine-1);
      if(cc%nInLine<nInLine-1){
        unsigned int iVar = cc%nInLine;
        // convert it to unsigned int
        str >> IthMode;
        thisLine[iVar] = IthMode;
        if(IthMode>maxModes[iVar]){
          maxModes[iVar] = IthMode;
        }
        if(IthMode<minModes[iVar]){
          minModes[iVar] = IthMode;
        }
      }
      else{
        // it is the tensor value, convert it to double
        str >> tensorValue;
        values->push_back(tensorValue);
      }
      tab->push_back(thisLine);
      cc +=1;
     }
     cout << "done.\n";
  }
  else{
    puts("Unable to open file!");
    exit(1);
  }
  inputFile.close();

  unsigned int nOfNonZeros = values->size();

  /* analize the tab to get the resolution.
  assuming implicitly that the step in each mode is 1 (otherwise a map is required!)
  */
  vector<unsigned int> resPerDim(nInLine-1);
  for(unsigned int iVar = 0; iVar< nInLine-1; iVar++){
    assert(maxModes[iVar] > minModes[iVar]);
    resPerDim[iVar] = maxModes[iVar] - minModes[iVar];
  }

  T.set_nVar(nInLine-1);
  T.set_nDof_var(resPerDim);
  T.init();

  // tensor is ready to be filled with the data collected.
  for(unsigned int iEntry=0; iEntry<nOfNonZeros; iEntry++){
    vector<unsigned int> indices(nInLine-1);
    for(unsigned int iVar=0; iVar<nInLine-1; iVar++){
      indices[iVar] = (*tab)[iEntry][iVar] - minModes[iVar];
    }
    T.setTensorElement(indices, (*values)[iEntry]);
  }
  T.finalizeTensor();

  delete values;
  delete tab;
}

/* load matrix from freefem++
  - format I J VAL
  - rows are in increasing order
*/
Mat loadFreefemMatrix(string& fName, MPI_Comm theComm){
  auxiliary a;
  Mat toBeReturned;

  ifstream inputFile;
  inputFile.open(fName.c_str());
  char output[128];

  unsigned int iThRow;
  unsigned int jThCol;
  double val;

  vector<unsigned int> I;
  vector<unsigned int> J;
  vector<double> values;

  if (inputFile.is_open()) {
    cout << "Reading..." << endl;
    unsigned int cc = 0;
    while (!inputFile.eof()) {
      inputFile >> output;
      stringstream str;
      str << output;

      if(cc%3==0){
        str >> iThRow;
        I.push_back(iThRow);
      }
      else if(cc%3==1){
        str >> jThCol;
        J.push_back(jThCol);
      }
      else if(cc%3==2){
        str >> val;
        values.push_back(val);
      }
      cc += 1;
    }
    cout << "done.\n";
  }
  else{
    puts("Unable to read file!");
    exit(1);
  }

  I.pop_back();
  unsigned int nRows = I[I.size()-1] + 1;
  a.initMat(toBeReturned, nRows, nRows, theComm);
  for(unsigned int iEl=0; iEl<I.size(); iEl++){
    a.setMatEl(toBeReturned, I[iEl], J[iEl], values[iEl], theComm);
  }
  a.finalizeMat(toBeReturned);

  return toBeReturned;
}



/* load vector from freefem++
  - format: simple sequence of values
*/
Vec loadFreefemVector(string& fName, MPI_Comm theComm){
  auxiliary a;
  Vec toBeReturned;
  vector<double> nonZerosList;
  vector<unsigned int> idList;
  double thisEntry = 0.0;

  ifstream inputFile;
  inputFile.open(fName.c_str());
  char output[128];
  unsigned int cc = 0;
  if (inputFile.is_open()) {
    cout << "Reading..." << endl;
    while (!inputFile.eof()) {
      inputFile >> output;
      stringstream str;
      str << output;
      str >> thisEntry;
      if(fabs(thisEntry)>1.0e-16){
        idList.push_back(cc);
        nonZerosList.push_back(thisEntry);
      }
      cc += 1;
    }
    cout << "done.\n";
  }
  else{
    puts("Unable to read file!");
    exit(1);
  }
  cc = cc -1;
  a.initVec(toBeReturned, cc, theComm);
  for(unsigned int iEl=0; iEl<nonZerosList.size(); iEl++){
    a.setVecEl(toBeReturned, idList[iEl], nonZerosList[iEl], theComm);
  }
  a.finalizeVec(toBeReturned);
  return toBeReturned;
}


// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

    // define auxiliary Petsc operations
    auxiliary a;

    //dimension
    unsigned int dim=10;

    //Start assembling the operator
 string stifName  = "Data/Helmholtz/L.txt";
 Mat K1 = loadFreefemMatrix(stifName, PETSC_COMM_WORLD);

 string massName  ="Data/Helmholtz/M.txt";
 Mat M1 = loadFreefemMatrix(massName, PETSC_COMM_WORLD);

//size of matrix
int nRows, nCols;
MatGetSize(K1, &nRows, &nCols);
vector<unsigned int> resPerDim(10);
resPerDim[0]=nRows;
//resolution in the frequency parameter
resPerDim[1]=40; //we've set the frequency from 1 to 50
for (unsigned int iVar = 2; iVar < dim; iVar++){
resPerDim[iVar]=32;
}


string HelmName  ="../Exercises/Preconditioner.txt";
Mat Helm = loadFreefemMatrix(HelmName, PETSC_COMM_WORLD);
//MatView(Helm, PETSC_VIEWER_STDOUT_WORLD);


//identity matrices
Mat I2=a.eye(resPerDim[1], PETSC_COMM_WORLD);
Mat I3d = a.eye(resPerDim[2], PETSC_COMM_WORLD);

Mat X2;
a.initMat(X2, resPerDim[1], resPerDim[1], PETSC_COMM_WORLD);
for (unsigned int jDof=0; jDof <resPerDim[1];  jDof++) {
  double value = -1.0-9.0/(resPerDim[1]-1)*jDof;
  a.setMatEl(X2, jDof, jDof, value, PETSC_COMM_WORLD);
}
a.finalizeMat(X2);

vector<vector<Mat> > opTerms(2);
opTerms[0].resize(dim);
opTerms[1].resize(dim);

opTerms[0][0]=K1;
opTerms[0][1]= I2;
for (unsigned int iVar = 2; iVar < dim; iVar++) {
  opTerms[0][iVar]= I3d;
}

opTerms[1][0]= M1;
opTerms[1][1]= X2;
for (unsigned int iVar = 2; iVar < dim; iVar++) {
  opTerms[1][iVar]= I3d;
}

operatorTensor A(opTerms, PETSC_COMM_WORLD);


//Building the preconditioner;
vector<vector<Mat> > pTerms(1);
pTerms[0].resize(dim);
pTerms[0][0] = Helm;
pTerms[0][1] = I2;
for(unsigned int iVar=2; iVar<dim; iVar++){
  pTerms[0][iVar] = I3d;
}

operatorTensor P(pTerms, PETSC_COMM_WORLD);



//Assembling the right hand side. The source term.

string source1Name  ="Data/Helmholtz/C_1.txt";
Vec C1 = loadFreefemVector(source1Name, PETSC_COMM_WORLD);
string source2Name  ="Data/Helmholtz/C_2.txt";
Vec C2 = loadFreefemVector(source2Name, PETSC_COMM_WORLD);
string source3Name  ="Data/Helmholtz/C_3.txt";
Vec C3 = loadFreefemVector(source3Name, PETSC_COMM_WORLD);
string source4Name  ="Data/Helmholtz/C_4.txt";
Vec C4 = loadFreefemVector(source4Name, PETSC_COMM_WORLD);

string ssource1Name  ="Data/Helmholtz/S_1.txt";
Vec S1 = loadFreefemVector(ssource1Name, PETSC_COMM_WORLD);
string ssource2Name  ="Data/Helmholtz/S_2.txt";
Vec S2 = loadFreefemVector(ssource2Name, PETSC_COMM_WORLD);
string ssource3Name  ="Data/Helmholtz/S_3.txt";
Vec S3 = loadFreefemVector(ssource3Name, PETSC_COMM_WORLD);
string ssource4Name  ="Data/Helmholtz/S_4.txt";
Vec S4 = loadFreefemVector(ssource4Name, PETSC_COMM_WORLD);



// TESTING KSP

/*KSP ksp;
//a.initLinSolve(ksp, Helm, 1.0e-8, 1.0e-10, PETSC_COMM_WORLD, KSPPREONLY);
KSPCreate(PETSC_COMM_WORLD,&ksp);
KSPSetOperators(ksp, Helm, Helm); // => use A to build preconditioner
KSPSetType(ksp, KSPGMRES);
//KSPSetTolerances(ksp, 1.0e-12, 1.0e-12, 1.0e-12, PETSC_DEFAULT);
KSPSetFromOptions(ksp);
Vec thisSol;
a.initVec(thisSol,nRows,PETSC_COMM_WORLD);
a.finalizeVec(thisSol);
KSPSolve(ksp,S4,thisSol);
//a.solveLin(ksp, C1, thisSol, PETSC_COMM_WORLD, true);
KSPDestroy(&ksp);
string testName = "KSP_SOLVE.vtk";

ofstream modeFile;
modeFile.open(testName.c_str());
for (unsigned int iDof = 0; iDof < 1424; iDof++) {
  modeFile << a.getVecEl(thisSol, iDof, PETSC_COMM_WORLD) << endl;
}
modeFile.close();*/




vector<vector<Vec> > opTerms2(dim-2);
for (unsigned int iTerm=0; iTerm < dim-2; iTerm++) {
  opTerms2[iTerm].resize(dim);
}

opTerms2[0][0]=C1;
opTerms2[1][0]=C2;
opTerms2[2][0]=C3;
opTerms2[3][0]=C4;
opTerms2[4][0]=S1;
opTerms2[5][0]=S2;
opTerms2[6][0]=S3;
opTerms2[7][0]=S4;


//Vector of ones for the second and the rest of the elements
Vec one3d;
a.initVec(one3d, resPerDim[2], PETSC_COMM_WORLD);
for(unsigned int jDof=0; jDof < resPerDim[2]; jDof ++){
  a.setVecEl(one3d, jDof, 1.0, PETSC_COMM_WORLD);
}
a.finalizeVec(one3d);

Vec one2;
a.initVec(one2, resPerDim[1], PETSC_COMM_WORLD);
for(unsigned int jDof=0; jDof < resPerDim[1]; jDof ++){
  a.setVecEl(one2, jDof, 1.0, PETSC_COMM_WORLD);
}
a.finalizeVec(one2);

vector<double> freqWeights = {1.0, 1.0/4.0, 1.0/9.0, 1.0/16.0, 1.0, 1.0/4.0, 1.0/9.0, 1.0/16.0};

for (unsigned int iTerm=0; iTerm < dim-2; iTerm++) {
  opTerms2[iTerm][1]= one2;
}
for (unsigned int iTerm = 0; iTerm < dim-2; iTerm++) {
  for (unsigned int iVar = 2; iVar < dim; iVar++) {
    if (iVar == iTerm + 2){
      Vec values;
      a.initVec(values, resPerDim[iVar], PETSC_COMM_WORLD);
      for (unsigned int jDof = 0; jDof < resPerDim[iVar]; jDof++) {
        double val = -1.0 + jDof * 2.0 * freqWeights[iVar-2]/(static_cast<double>(resPerDim[iVar]-1));
        a.setVecEl(values, jDof, val, PETSC_COMM_WORLD);
      }
      a.finalizeVec(values);
    opTerms2[iTerm][iVar]= values;
  }else{
    opTerms2[iTerm][iVar]=one3d;
  }
  }
}





CPTensor b(opTerms2, PETSC_COMM_WORLD);

double normb = sqrt(b.norm2CP());

//Solving
CPTensor solution(dim, resPerDim, PETSC_COMM_WORLD);
multilinearSolver solver;
string outFileName = "Helmholtz";
solution.set_CPName(outFileName);
double resNorm = normb;

const double kappa = 0.8;
//solver.solvewithCPTT(A, b, solution, kappa * normb);
solver.CG(A, b, solution, kappa*normb);
unsigned int itSolve = 1;
while(resNorm>1.0e-2*normb){
  double tol = pow(kappa, itSolve+1) * normb;
  CPTensor sol_0(dim, resPerDim, PETSC_COMM_WORLD);
  sol_0.CPTT_ApproxOf(solution, 1.0e-3*tol);
  solution.clear();
  //solver.solvewithCPTT(A, b, sol_0, solution, tol);
  solver.CG(A, b, sol_0, solution, tol);
  itSolve +=1;
}


//solver.solvewithCPTT_precond(A, P, b, solution, 1.0e-2 * normb);
//solver.solveCPTT_CG(A, b, solution, 1.0e-2 * normb, true);
//solver.solveCPTT_CG_precond(A, P, b, solution, 1.0e-2 * normb);
//solver.CG(A, b, solution, 1.0e-2 * normb);



// Destroying objects:
b.clear();
for(unsigned int iTerm=0; iTerm<A.numTerms(); iTerm++){
  for(unsigned int iVar=0; iVar<dim; iVar++){
    Mat toBeDestroyed = A.op(iTerm,iVar);
    MatDestroy(&toBeDestroyed);
  }
}

for(unsigned int iTerm=0; iTerm<P.numTerms(); iTerm++){
  for(unsigned int iVar=0; iVar<dim; iVar++){
    Mat toBeDestroyed = P.op(iTerm,iVar);
    MatDestroy(&toBeDestroyed);
  }
}


SlepcFinalize();
return 0;

}
