// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include "genericInclude.h"
#include "auxiliary.h"
#include "fullTensor.h"

using namespace std;

// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

    // Init a Full Tensor:
    fullTensor A;

    // init a Full Tensor given the dimension and the resolution:
    unsigned int dim = 3;
    vector<unsigned int> resPerDim(dim);
    resPerDim[0] = 2;
    resPerDim[1] = 4;
    resPerDim[2] = 3;

    fullTensor B(dim, resPerDim, PETSC_COMM_WORLD);
    B.printTensorStruct();

    // set tensor elements:
    vector<unsigned int> I = {0,0,1};
    B.setTensorElement(I,57.0);
    B.finalizeTensor();
    VecView(B.tensorEntries(),PETSC_VIEWER_STDOUT_WORLD);

    unsigned int linInd = B.sub2lin(I);
    PetscPrintf(PETSC_COMM_WORLD, "l = %d \n", linInd);
    PetscPrintf(PETSC_COMM_WORLD, "T(I) = %f \n", B.tensorEntries(linInd) );

    // copy the structure of B into A;
    A.copyTensorStructFrom(B);
    PetscPrintf(PETSC_COMM_WORLD,"A:\n");
    A.printTensorStruct();

    // tensor C and copy structure from B into C:
    fullTensor C;
    B.copyTensorStructTo(C);
    PetscPrintf(PETSC_COMM_WORLD,"C:\n");
    C.printTensorStruct();

    // Finalize the MPI environment.
    SlepcFinalize();
    return 0;
}
