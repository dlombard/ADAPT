// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "auxiliary.h"
#include "fullTensor.h"
#include "CPTensor.h"
using namespace std;

/* 4 -- saving field in VTK:
  - input: a 3d tensor
  - output: a vtk file
*/
void saveField(fullTensor& T, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = T.nDof_var(0);
 unsigned int ny = T.nDof_var(1);
 unsigned int nz = T.nDof_var(2);
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;
 for(unsigned int h=0; h<T.nEntries(); h++){
     double value = T.tensorEntries(h);
     if(fabs(value) < 1.0e-9){value = 0.0;};
     outfile << value << endl;
 }
 outfile.close();
}


// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

    // define auxiliary Petsc operations
    auxiliary a;

    //Random numbers
    unsigned long int testIdSeed = (unsigned long int) args[1];
    unsigned long int theSeed = std::chrono::system_clock::now().time_since_epoch().count();
    theSeed = theSeed + testIdSeed;
    std::default_random_engine coeffGenerator;
    coeffGenerator.seed(theSeed);
    std::uniform_real_distribution<double> distribution(-1.0,1.0);
    srand(time(NULL));



    //CPapprox  in d-dimension
    unsigned int dim=3;
    vector<unsigned int> resPerDim(dim);
    for (unsigned int i = 0; i < dim; i++) {
      resPerDim[i]=25;
    }

    vector<unsigned int> kappaMax(dim);
    unsigned int kMax=6;
    for (unsigned int i = 0; i < dim; i++) {
      kappaMax[i]=kMax;
    }

    std::default_random_engine coeffGeneratorL;
    coeffGeneratorL.seed(theSeed);
    uniform_int_distribution<int> distributionL(1.0,kMax);
    srand(time(NULL));

    fullTensor K(dim, kappaMax, PETSC_COMM_WORLD);

    unsigned int nOfTerms = 50;
    const double beta = 3;
    const double pi = std::atan(1.0)*4.0;

    CPTensor F(dim, resPerDim, PETSC_COMM_WORLD);

    vector<double> waveAmplitudes(nOfTerms);
    vector<double> kappa(dim);
    vector<vector<Vec> > terms(nOfTerms);

    for (unsigned int iTerm = 0; iTerm < nOfTerms ; iTerm++) {
      double kappasq = 0.0;
      vector<unsigned int> ind = K.lin2sub(iTerm);
      for(unsigned int iVar=0;iVar<dim;iVar++){
        kappa[iVar] = distributionL(coeffGeneratorL);
        kappasq += kappa[iVar] * kappa[iVar];
      }
      double lambda_tilde=sqrt(kappasq);
      double lambda= lambda_tilde/sqrt(dim);
      double alpha = distribution(coeffGenerator);
      double aCoef = alpha/(pow(lambda,beta));
      waveAmplitudes[iTerm] = aCoef;

      vector<Vec> thisTerm(dim);
      for(unsigned int iVar=0; iVar<dim; iVar++){
        Vec fun;
        a.initVec(fun, resPerDim[iVar], PETSC_COMM_WORLD);
        for(unsigned int jDof=0; jDof<resPerDim[iVar]; jDof++){
          double x = 1.0* jDof/(resPerDim[iVar]-1);
          double value = sin(pi*kappa[iVar]*x);
          a.setVecEl(fun, jDof, value, PETSC_COMM_WORLD);
        }
        a.finalizeVec(fun);
        thisTerm[iVar] = fun;
      }
      terms[iTerm] = thisTerm;
    }
    F.set_rank(nOfTerms);
    F.set_coeffs(waveAmplitudes);
    F.set_terms(terms);
    PetscPrintf(PETSC_COMM_WORLD,"Tensor has been built \n\n");

    // ALS:
    vector<Mat> massMat(dim);
      for(unsigned int idVar=0; idVar<dim; idVar++){
        massMat[idVar] = a.eye(resPerDim[idVar], PETSC_COMM_WORLD);
      }

    PetscPrintf(PETSC_COMM_WORLD,"Starting ALS: \n\n");
    CPTensor ALS_approx;
    string outFileName = "ALS_3_3.5_" + string(args[1]); //ALS_dim_beta_
    ALS_approx.set_CPName(outFileName);

    ofstream chara;
    string charaName = string("Characteristics_") + outFileName + string(".log");
    chara.open(charaName.c_str());
    chara << "Dim= " << dim << endl;
    chara << "Degrees of freedom= " << resPerDim[0] <<endl;
    chara << "Beta= " << beta << endl;
    chara << "Seed= " << theSeed << endl;
    chara.close();

    //ALS_approx.greedy_ApproxOf(F, massMat, 1.0e-2, 1.0e-4);
    ALS_approx.greedy_ApproxOf(F, massMat, 1.0e-1, 1.0e-4, 0.5); //relaxed fixed point

    //CPTT method
    PetscPrintf(PETSC_COMM_WORLD,"Starting CPTT: \n\n");
    CPTensor approx;
    outFileName = "CPTT_3_3.5_" + string(args[1]);
    approx.set_CPName(outFileName);
    approx.CPTT_ApproxOf(F, 1.0e-1, false);


    /* ofstream kmode;
    string kmode_name = string("Modes_") + outFileName + string(".txt");
    kmode.open(kmode_name.c_str());
    kmode << kThTerm << endl;
    kmode.close(); */


    // Finalize the MPI environment.
    SlepcFinalize();
    return 0;
}
