// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>

using namespace std;

// Simple Hello World program to test MPI implementation

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    MPI_Init(NULL,NULL);

    // Number of procs
    int nOfProcs;
    MPI_Comm_size(MPI_COMM_WORLD, &nOfProcs);

    // Get the rank of the process
    int idProc;
    MPI_Comm_rank(MPI_COMM_WORLD, &idProc);

    // Get the name of the processor
    char procName[MPI_MAX_PROCESSOR_NAME];
    int lenName;
    MPI_Get_processor_name(procName, &lenName);

    // Print off a hello world message
    printf("Hello world from processor %s, rank %d out of %d processors\n",
           procName, idProc, nOfProcs);

    // Finalize the MPI environment.
    MPI_Finalize();
    return 0;
}
