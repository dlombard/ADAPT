// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include "genericInclude.h"
#include "auxiliary.h"
#include "fullTensor.h"

using namespace std;

// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


    // init a Full Tensor given the dimension and the resolution:
    unsigned int dim = 3;
    vector<unsigned int> resPerDim(dim);
    resPerDim[0] = 2;
    resPerDim[1] = 4;
    resPerDim[2] = 3;

    fullTensor B(dim, resPerDim, PETSC_COMM_WORLD);
    B.printTensorStruct();

    // set tensor elements:
    vector<unsigned int> I = {0,0,0};
    B.setTensorElement(I,57.0);
    I = {0,1,0};
    B.setTensorElement(I,1.0);

    I = {1,1,1};
    B.setTensorElement(I,-6.0);
    I = {0,1,1};
    B.setTensorElement(I,-7.0);
    I = {1,2,1};
    B.setTensorElement(I,-8.0);

    I = {0,2,2};
    B.setTensorElement(I,3.14);
    I = {1,3,2};
    B.setTensorElement(I,2.71);

    B.finalizeTensor();


    double valTest = 0.0;

    // evaluate unfolding wothout assembling it !
    B.evalUnfolding(0,0,10,valTest);
    PetscPrintf(PETSC_COMM_WORLD, "U_0(0,10) = %f \n", valTest);

    B.evalUnfolding(1,2,4,valTest);
    PetscPrintf(PETSC_COMM_WORLD, "U_1(2,4) = %f \n", valTest);

    B.evalUnfolding(2,2,4,valTest);
    PetscPrintf(PETSC_COMM_WORLD, "U_2(2,4) = %f \n", valTest);


    // Finalize the MPI environment.
    SlepcFinalize();
    return 0;
}
