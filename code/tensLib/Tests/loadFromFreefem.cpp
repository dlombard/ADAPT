// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "auxiliary.h"
#include "fullTensor.h"
#include "CPTensor.h"
#include "multilinearSolver.h"
#include "operatorTensor.h"
using namespace std;

/* 4 -- saving field in VTK:
  - input: a 3d tensor
  - output: a vtk file
*/
void saveField(fullTensor& T, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = T.nDof_var(0);
 unsigned int ny = T.nDof_var(1);
 unsigned int nz = T.nDof_var(2);
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;
 for(unsigned int h=0; h<T.nEntries(); h++){
     double value = T.tensorEntries(h);
     if(fabs(value) < 1.0e-9){value = 0.0;};
     outfile << value << endl;
 }
 outfile.close();
}

/* Importing full tensor from FROSTT repository.
  - Input: file name, reference to an empty full tensor, number of entries per line
  - Output: initialise and fill the full tensor.
*/
void importFrostt(fullTensor& T, string& fName, unsigned int nInLine, MPI_Comm theComm){
  T.setTensorComm(theComm);
  auxiliary a;
  unsigned int dim = nInLine - 1;

  ifstream inputFile;
  inputFile.open(fName.c_str());
  char output[128];
  unsigned int IthMode;
  double tensorValue;

  unsigned int cc = 0;

  vector<double>* values;
  values = new vector<double>;

  vector<vector<int> >* tab;
  tab = new vector<vector<int> >;

  vector<int> maxModes(nInLine-1, 0); // assuming modes are positive in the tensor!!
  vector<int> minModes(nInLine-1, 16777216);  // 2^24
  vector<int> thisLine(nInLine-1);

  if (inputFile.is_open()) {
    cout << "Reading...";
    while (!inputFile.eof()) {
      inputFile >> output;
      stringstream str;
      str << output;

      if(cc%nInLine<nInLine-1){
        unsigned int iVar = cc%nInLine;
        // convert it to unsigned int
        str >> IthMode;
        thisLine[iVar] = IthMode;
        if(IthMode>maxModes[iVar]){
          maxModes[iVar] = IthMode;
        }
        if(IthMode<minModes[iVar]){
          minModes[iVar] = IthMode;
        }
      }
      else{
        // it is the tensor value, convert it to double
        str >> tensorValue;
        values->push_back(tensorValue);
        tab->push_back(thisLine);
        thisLine.clear();
        thisLine.resize(nInLine-1);
      }
      cc +=1;
     }
     cout << "done.\n";
  }
  else{
    puts("Unable to open file!");
    exit(1);
  }
  inputFile.close();

  unsigned int nOfNonZeros = values->size();

  /* analize the tab to get the resolution.
  assuming implicitly that the step in each mode is 1 (otherwise a map is required!)
  */
  vector<unsigned int> resPerDim(nInLine-1);
  for(unsigned int iVar = 0; iVar< nInLine-1; iVar++){
    assert(maxModes[iVar] > minModes[iVar]);
    resPerDim[iVar] = maxModes[iVar] - minModes[iVar];
  }

  T.set_nVar(nInLine-1);
  T.set_nDof_var(resPerDim);
  T.init();

  // tensor is ready to be filled with the data collected.
  for(unsigned int iEntry=0; iEntry<nOfNonZeros; iEntry++){
    vector<unsigned int> indices(nInLine-1);
    for(unsigned int iVar=0; iVar<nInLine-1; iVar++){
      indices[iVar] = (*tab)[iEntry][iVar] - minModes[iVar];
    }
    T.setTensorElement(indices, (*values)[iEntry]);
  }
  T.finalizeTensor();

  delete values;
  delete tab;
}



/* load matrix from freefem++
  - format I J VAL
  - frefem is saving indices from 1, first elem A_11!
  - rows are in increasing order, commentary is 28 words long
  - after 24 lines of comment we have the matrix size
*/
Mat loadFreefemMatrix(string& fName, MPI_Comm theComm){
  auxiliary a;
  Mat toBeReturned;

  ifstream inputFile;
  inputFile.open(fName.c_str());
  char output[128];

  unsigned int iThRow;
  unsigned int jThCol;
  double val;

  unsigned int nRows;
  unsigned int nCols;

  if (inputFile.is_open()) {
    cout << "Reading..." << endl;
    unsigned int cc = 0;
    while (!inputFile.eof()) {
      inputFile >> output;
      stringstream str;
      str << output;
      if(cc>24 && cc<27){
        if(cc==25){
          str >> nRows;
        }
        if(cc==26){
          str >> nCols;
          a.initMat(toBeReturned,nRows,nCols,theComm);
        }
      }

      // filling the matrix
      if(cc>28){
        if(cc%3==2){
          str >> iThRow;
          iThRow = iThRow - 1;
        }
        else if(cc%3==0){
          str >> jThCol;
          jThCol = jThCol - 1;
        }
        else if(cc%3==1){
          str >> val;
          a.setMatEl(toBeReturned, iThRow, jThCol, val, theComm);
        }
      }
      cc += 1;
    }
    cout << "done.\n";
  }
  else{
    puts("Unable to read file!");
    exit(1);
  }
  a.finalizeMat(toBeReturned);

  return toBeReturned;
}


/* load vector from freefem++
  - format: simple sequence of values
*/
Vec loadFreefemVector(string& fName, MPI_Comm theComm){
  auxiliary a;
  Vec toBeReturned;
  vector<double> nonZerosList;
  vector<unsigned int> idList;
  double thisEntry = 0.0;

  ifstream inputFile;
  inputFile.open(fName.c_str());
  char output[128];
  unsigned int cc = 0;
  if (inputFile.is_open()) {
    cout << "Reading..." << endl;
    while (!inputFile.eof()) {
      inputFile >> output;
      stringstream str;
      str << output;
      str >> thisEntry;
      if(fabs(thisEntry)>1.0e-16){
        idList.push_back(cc);
        nonZerosList.push_back(thisEntry);
      }
      cc += 1;
    }
    cout << "done.\n";
  }
  else{
    puts("Unable to read file!");
    exit(1);
  }
  a.initVec(toBeReturned, cc, theComm);
  for(unsigned int iEl=0; iEl<nonZerosList.size(); iEl++){
    a.setVecEl(toBeReturned, idList[iEl], nonZerosList[iEl], theComm);
  }
  a.finalizeVec(toBeReturned);
  return toBeReturned;
}


// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

    // define auxiliary Petsc operations
    auxiliary a;

    // load freefem++ Matrix;
    string stiffName = "../Exercises/L.txt";
    Mat K = loadFreefemMatrix(stiffName, PETSC_COMM_WORLD);
    MatView(K, PETSC_VIEWER_STDOUT_WORLD);

    string S_1_Name = "../Exercises/S_1.txt";
    Vec s1 = loadFreefemVector(S_1_Name, PETSC_COMM_WORLD);
    VecView(s1, PETSC_VIEWER_STDOUT_WORLD);

    SlepcFinalize();
    return 0;
}
