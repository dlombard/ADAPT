// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>

using namespace std;

// Simple Hello World program to test MPI implementation and groups contruction

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    MPI_Init(NULL,NULL);

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(MPI_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(MPI_COMM_WORLD, &idProc);

    // Group of processes in MPI_COMM_WORLD
    MPI_Group worldGroup;
    MPI_Comm_group(MPI_COMM_WORLD, &worldGroup);

    // Create two groups, even ranks, odd ranks.
    int nEven, nOdd;
    if (nOfProcs%2==0) {
      nEven = nOfProcs / 2;
      nOdd = nOfProcs / 2;
    }
    else{
      nEven = (nOfProcs + 1)/2;
      nOdd = nOfProcs - nEven;
    }

    int evenRanks[nEven];
    int oddRanks[nOdd];
    int ccEven=0;
    int ccOdd=0;
    for(int iProc=0; iProc<nOfProcs; iProc++){
      if(iProc%2==0){
        evenRanks[ccEven] = iProc;
        ccEven += 1;
      }
      else{
        oddRanks[ccOdd] = iProc;
        ccOdd += 1;
      }
    }

    // Initialise the group even:
    MPI_Group evenGroup;
    MPI_Group_incl(worldGroup, nEven, evenRanks, &evenGroup);

    // Create a new communicator based on the even group ( 0 is the TAG!)
    MPI_Comm evenComm;
    MPI_Comm_create_group(MPI_COMM_WORLD, evenGroup, 0, &evenComm);


    // Initialise the group odd:
    MPI_Group oddGroup;
    MPI_Group_incl(worldGroup, nOdd, oddRanks, &oddGroup);

    // Create a new communicator based on the odd group ( 1 is the TAG!)
    MPI_Comm oddComm;
    MPI_Comm_create_group(MPI_COMM_WORLD, oddGroup, 1, &oddComm);


    // printing routines:
    if(evenComm != MPI_COMM_NULL){
      int sizeEven, iEven;
      MPI_Comm_size(evenComm, &sizeEven);
      MPI_Comm_rank(evenComm, &iEven);
      cout << "Proc " << iEven << ", global " << idProc << " in a group of " << sizeEven << endl;
    }

    if(oddComm != MPI_COMM_NULL){
      int sizeOdd, iOdd;
      MPI_Comm_size(oddComm, &sizeOdd);
      MPI_Comm_rank(oddComm, &iOdd);
      cout << "Proc " << iOdd << ", global " << idProc << " in a group of " << sizeOdd << endl;
    }



    // Destroy the objects:
    MPI_Group_free(&worldGroup);
    MPI_Group_free(&evenGroup);
    MPI_Group_free(&oddGroup);
    if(evenComm != MPI_COMM_NULL){MPI_Comm_free(&evenComm);}
    if(oddComm != MPI_COMM_NULL){MPI_Comm_free(&oddComm);}


    // Finalize the MPI environment.
    MPI_Finalize();
    return 0;
}
