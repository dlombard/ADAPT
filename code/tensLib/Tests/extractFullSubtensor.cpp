// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include "genericInclude.h"
#include "auxiliary.h"
#include "fullTensor.h"

using namespace std;

// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


    // init a Full Tensor given the dimension and the resolution:
    unsigned int dim = 3;
    vector<unsigned int> resPerDim(dim);
    resPerDim[0] = 2;
    resPerDim[1] = 4;
    resPerDim[2] = 3;

    fullTensor B(dim, resPerDim, PETSC_COMM_WORLD);
    B.printTensorStruct();

    // set tensor elements:
    vector<unsigned int> I = {0,0,2};
    B.setTensorElement(I,57.0);
    I = {0,1,2};
    B.setTensorElement(I,1.0);
    I = {0,2,2};
    B.setTensorElement(I,3.14);
    B.finalizeTensor();
    VecView(B.tensorEntries(),PETSC_VIEWER_STDOUT_WORLD);

    PetscPrintf(PETSC_COMM_WORLD,"\n");
    PetscPrintf(PETSC_COMM_WORLD,"Extracting subtensor...\n");
    PetscPrintf(PETSC_COMM_WORLD,"\n");

    // Init a Full Tensor to compute the subtensor of B:
    fullTensor A;
    vector<unsigned int> indicesBound(2*dim);
    indicesBound[0] = 0; indicesBound[1] = 1;
    indicesBound[2] = 0; indicesBound[3] = 3;
    indicesBound[4] = 2; indicesBound[5] = 3;
    B.extractSubtensor(indicesBound,A);
    A.printTensorStruct();
    VecView(A.tensorEntries(),PETSC_VIEWER_STDOUT_WORLD);


    // Finalize the MPI environment.
    SlepcFinalize();
    return 0;
}
