// Implementation of the class multilinearSolver


#include "multilinearSolver.h"

using namespace std;


/* Conjugate Gradient solvers
  - Input: operator tensor, rhs, tolerance
  - Output: solution
    REMARK: in this function there is no initial guess: implicitly set to 0.
*/

void multilinearSolver::CG(operatorTensor& A, CPTensor& rhs, CPTensor& sol, double tol){

  unsigned int nVar = rhs.nVar();
  vector<unsigned int> dofPerDim = rhs.nDof_var();
  MPI_Comm theComm = rhs.theComm();

  // Initialising the solution fields:
  sol.set_nVar(nVar);
  sol.set_nDof_var(dofPerDim);
  sol.setTensorComm(theComm);

  // Creating residual andconjugate direction tensors:
  CPTensor residual;
  CPTensor conjDir;
  residual.copyTensorFrom(rhs);
  conjDir.copyTensorFrom(rhs);

  // compute the norm of the residual
  double norm2Res = residual.norm2CP();
  PetscPrintf(rhs.theComm(), "Residual norm = %e \n", sqrt(norm2Res));

  while(sqrt(norm2Res)>tol){
    double alphaNum = norm2Res;
    CPTensor krilovTens;
    A.applyOperator(conjDir, krilovTens);
    double alphaDen = conjDir.scalarProd(krilovTens);
    double alpha = alphaNum/alphaDen;


    // solution update (copy the terms conjDir will be destroyed);
    sol.sum(conjDir, alpha, true);
    residual.sum(krilovTens, -alpha, true);

    norm2Res = residual.norm2CP();
    PetscPrintf(rhs.theComm(), "Residual norm = %e \n", sqrt(norm2Res));

    // updating the conjugate direction
    double betaDen = alphaNum;
    double betaNum = norm2Res;
    double beta = betaNum/betaDen;

    conjDir.multiplyByScalar(beta);
    conjDir.sum(residual, 1.0, true);
    krilovTens.clear();
  }

  conjDir.clear();
  residual.clear();
}


/* Conjugate Gradient solvers, with initial guess
  - Input: operator tensor, rhs, tolerance, initial solution
  - Output: solution
    REMARK: in this function there is no initial guess: implicitly set to 0.
*/

void multilinearSolver::CG(operatorTensor& A, CPTensor& rhs, CPTensor& sol_0, CPTensor& sol, double tol){

  unsigned int nVar = rhs.nVar();
  vector<unsigned int> dofPerDim = rhs.nDof_var();
  MPI_Comm theComm = rhs.theComm();

  // Initialising the solution fields:
  sol.set_nVar(nVar);
  sol.set_nDof_var(dofPerDim);
  sol.setTensorComm(theComm);
  sol.copyTensorFrom(sol_0);

  // Creating residual andconjugate direction tensors:
  CPTensor residual;
  CPTensor conjDir;
  residual.copyTensorFrom(rhs);
  CPTensor AsolUpdated;
  A.applyOperator(sol_0, AsolUpdated);
  residual.sum(AsolUpdated, -1.0, true);
  AsolUpdated.clear();

  conjDir.copyTensorFrom(rhs);

  // compute the norm of the residual
  double norm2Res = residual.norm2CP();
  PetscPrintf(rhs.theComm(), "Residual norm = %e \n", sqrt(norm2Res));

  while(sqrt(norm2Res)>tol){
    double alphaNum = norm2Res;
    CPTensor krilovTens;
    A.applyOperator(conjDir, krilovTens);
    double alphaDen = conjDir.scalarProd(krilovTens);
    double alpha = alphaNum/alphaDen;


    // solution update (copy the terms conjDir will be destroyed);
    sol.sum(conjDir, alpha, true);
    residual.sum(krilovTens, -alpha, true);

    norm2Res = residual.norm2CP();
    PetscPrintf(rhs.theComm(), "Residual norm = %e \n", sqrt(norm2Res));

    // updating the conjugate direction
    double betaDen = alphaNum;
    double betaNum = norm2Res;
    double beta = betaNum/betaDen;

    conjDir.multiplyByScalar(beta);
    conjDir.sum(residual, 1.0, true);
    krilovTens.clear();
  }

  conjDir.clear();
  residual.clear();
}

/*
  CG with progressive recompression
*/
void multilinearSolver::CG_recompress(operatorTensor& A, CPTensor& rhs, CPTensor& sol, double tol){
  unsigned int nVar = rhs.nVar();
  vector<unsigned int> dofPerDim = rhs.nDof_var();
  MPI_Comm theComm = rhs.theComm();

  // Initialising the solution fields:
  sol.set_nVar(nVar);
  sol.set_nDof_var(dofPerDim);
  sol.setTensorComm(theComm);

  // Creating residual andconjugate direction tensors:
  CPTensor residual;
  CPTensor conjDir;
  residual.copyTensorFrom(rhs);
  conjDir.copyTensorFrom(rhs);

  // compute the norm of the residual
  double norm2Res = residual.norm2CP();
  PetscPrintf(rhs.theComm(), "Residual norm = %e \n", sqrt(norm2Res));

  while(sqrt(norm2Res)>tol){
    double alphaNum = norm2Res;
    CPTensor krilovTens;
    A.applyOperator(conjDir, krilovTens);
    double alphaDen = conjDir.scalarProd(krilovTens);
    double alpha = alphaNum/alphaDen;


    // solution update (copy the terms conjDir will be destroyed);
    sol.sum(conjDir, alpha, true);
    residual.sum(krilovTens, -alpha, true);

    norm2Res = residual.norm2CP();
    PetscPrintf(rhs.theComm(), "Residual norm = %e \n", sqrt(norm2Res));

    // updating the conjugate direction
    double betaDen = alphaNum;
    double betaNum = norm2Res;
    double beta = betaNum/betaDen;

    conjDir.multiplyByScalar(beta);
    conjDir.sum(residual, 1.0, true);
    krilovTens.clear();
  }
  CPTensor updatedSol;
  updatedSol.copyTensorFrom(sol);
  sol.clear();
  double normSol = sqrt(updatedSol.norm2CP());
  sol.CPTT_ApproxOf(updatedSol, normSol*1.0e-3, false);

  conjDir.clear();
  residual.clear();
}




/*Solving with CPTT
  - gradient of the residual with CPTT compression and exact line search
*/
void multilinearSolver::solvewithCPTT(operatorTensor& A, CPTensor& F, CPTensor& sol, double tol){
// Initialise residual R=F:
CPTensor R;
R.copyTensorFrom(F);
ofstream outfile;
if(!sol.CPName().empty()){
  string fileName = string("residual_") + sol.CPName() + string(".txt");
  outfile.open(fileName.c_str());
}

// Computing the Adjoint of the operator:
operatorTensor adjA = A.adjoint();

// Initialising an empty solution:
sol.setTensorComm(F.theComm());
sol.set_nVar(F.nVar());
vector<unsigned int> resPerDim = F.nDof_var();
sol.set_nDof_var(resPerDim);

// start looping
double residualNorm = sqrt(R.norm2CP());

PetscPrintf(PETSC_COMM_WORLD, "Residual norm %f \n", residualNorm);


unsigned int it = 0;
while ( residualNorm > tol) {

  CPTensor t(R.nVar(), R.nDof_var(), R.theComm());
  t.compute_CPTT_Term(R);
  cout << "t computed" << endl;
  cout << t.coeffs(0) << endl;


  CPTensor y = A.applyOperator(R);

  CPTensor w = A.applyOperator(t);

  double T1= y.scalarProd(w);
  cout << "T1 = " << T1 << endl;


  CPTensor RtoBeApprox;
  if(T1<0){
    RtoBeApprox.copyTensorFrom(R);
    RtoBeApprox.sum(t,-1.0, true);
  }
  while (T1 < 0) {
    cout << "entering the loop" << endl;
    CPTensor tToAdd(R.nVar(), R.nDof_var(), R.theComm());
    tToAdd.compute_CPTT_Term(RtoBeApprox);

    CPTensor wToAdd = A.applyOperator(tToAdd);
    w.sum(wToAdd, 1.0, true);
    t.sum(tToAdd, 1.0, false);
    T1 = T1 + y.scalarProd(wToAdd);
    RtoBeApprox.sum(tToAdd,-1.0, true);
  }
  // compute T2 =  <AA^* t, AA^* t>;
  //CPTensor update = adjA.applyOperator(t);
  CPTensor update;
  adjA.applyOperator(t, update);

  //CPTensor z=A.applyOperator(update);
  CPTensor z;
  A.applyOperator(update, z);

  double T2 = z.norm2CP();
  cout << "T2 = " << T2 << endl;
  cout << "alpha = " << T1/T2 << endl;

  // free the memory:
  w.clear();
  y.clear();
  z.clear();

  // exact line search:
  double alpha = T1 / T2;

  cout << "BEFORE UPDATE" << endl;
  for (unsigned int iTerm=0; iTerm < sol.rank(); iTerm++) {
    /*for (unsigned int iVar = 0; iVar < sol.nVar(); iVar++) {
      VecView(sol.terms(iTerm, iVar), PETSC_VIEWER_STDOUT_WORLD);
    }*/
    //cout << sol.coeffs(iTerm)<<endl;
  }
  cout << endl;




  // update the solution:
  sol.sum(update, alpha, false);
  //update.clear();

  cout << "AFTER UPDATE" << endl;

  //for (unsigned int iTerm=0; iTerm < sol.rank(); iTerm++) {
    /*for (unsigned int iVar = 0; iVar < sol.nVar(); iVar++) {
      VecView(sol.terms(iTerm, iVar), PETSC_VIEWER_STDOUT_WORLD);
    }*/
    //cout << sol.coeffs(iTerm) << endl;
  //}
  cout << endl;

  //if(it == 5){exit(1);}


  // renormalise the terms
  double exponent = 1.0/static_cast<double>(sol.nVar());
  for(unsigned int iTerm=0; iTerm<sol.rank(); iTerm++){
    double c_i = sol.coeffs(iTerm);
    double sign = 1;
    if(c_i<0.0){
      c_i = -1.0*c_i;
      sign = -1.0;
    }
    double scaling = pow(c_i, exponent);
    for(unsigned int iVar=0; iVar<sol.nVar(); iVar++){
      sol.rescaleTerm(iTerm, iVar, scaling);
    }
    sol.set_coeffs(iTerm, sign);
  }



  cout << "Sol computed" << endl;
  /*for (unsigned int iTerm=0; iTerm < sol.rank(); iTerm++) {
    for (unsigned int iVar = 0; iVar < sol.nVar(); iVar++) {
      VecView(sol.terms(iTerm, iVar), PETSC_VIEWER_STDOUT_WORLD);
    }
  }*/
/*
// coefficients optimisation:
  double maxEntry = -1e15;
  double minEntry = 1e15;
  Mat scalMat;
  a.initMat(scalMat, sol.rank(), sol.rank(),F.theComm());
  for(unsigned int iTerm=0; iTerm<sol.rank(); iTerm++){
    vector<Vec> I_term = sol.terms(iTerm);
    CPTensor It(I_term, 1.0, F.theComm());
    CPTensor AIt = A.applyOperator(It);
    for(unsigned int jTerm=iTerm; jTerm<sol.rank(); jTerm++){
      vector<Vec> J_term = sol.terms(jTerm);
      CPTensor Jt(J_term, 1.0, F.theComm());
      CPTensor AJt = A.applyOperator(Jt);

      double val = AIt.scalarProd(AJt);
      if(val>maxEntry){
        maxEntry = val;
      }
      if(val<minEntry){
        minEntry = val;
      }

      a.setMatEl(scalMat,iTerm,jTerm,val,F.theComm());
      if(iTerm != jTerm){
        a.setMatEl(scalMat,jTerm,iTerm,val,F.theComm());
      }
  //    AJt.clear();
    //  Jt.clear();
    }
  //  AIt.clear();
  //  It.clear();
  }
  a.finalizeMat(scalMat);
  double rescalingCoeff = 1.0/(maxEntry - minEntry);
  MatScale(scalMat, rescalingCoeff);

  //MatView(scalMat, PETSC_VIEWER_STDOUT_WORLD);

  Vec scalVec;
  a.initVec(scalVec,sol.rank(), F.theComm());
  for(unsigned int iTerm=0; iTerm < sol.rank(); iTerm++){
    double entry = 0.0;
    vector<Vec> I_term = sol.terms(iTerm);
    CPTensor It(I_term, 1.0, F.theComm());
    CPTensor AIt = A.applyOperator(It);

    for(unsigned int kTerm=0; kTerm < F.rank(); kTerm++){
      vector<Vec> K_term = F.terms(kTerm);
      CPTensor Kt(K_term, 1.0, F.theComm());
      double val = AIt.scalarProd(Kt);
      val = val * F.coeffs(kTerm);
      entry = entry + val;
    //  Kt.clear();
    }
    //AIt.clear();
    //It.clear();
    a.setVecEl(scalVec, iTerm, entry, F.theComm());
  }
  a.finalizeVec(scalVec);
  VecScale(scalVec, rescalingCoeff);
  //VecView(scalVec, PETSC_VIEWER_STDOUT_WORLD);

  Vec updateCoeffs;
  a.initVec(updateCoeffs, sol.rank(), F.theComm());
  a.finalizeVec(updateCoeffs);

  cout << "Printing new coefficients" << endl;
  KSP scalSolver;
  //KSPType type = KSPCG;
  KSPType type = KSPPREONLY;
  a.initLinSolve(scalSolver, scalMat, F.theComm(), type);
  a.solveLin(scalSolver, scalVec, updateCoeffs, F.theComm(), false);
  for(unsigned int iTerm=0; iTerm<sol.rank(); iTerm++){
    double thisCoeff = a.getVecEl(updateCoeffs, iTerm, F.theComm());

    cout << thisCoeff << endl;

    sol.set_coeffs(iTerm, thisCoeff);
  }
  cout << endl;

  VecDestroy(&scalVec);
  VecDestroy(&updateCoeffs);
  MatDestroy(&scalMat);
  KSPDestroy(&scalSolver);
*/


  //CPTensor AsolUpdated = A.applyOperator(sol);
  CPTensor AsolUpdated;
  A.applyOperator(sol, AsolUpdated);

  R.clear();
  R.copyTensorFrom(F);
  R.sum(AsolUpdated,-1.0, false);
  residualNorm = sqrt(R.norm2CP());
  PetscPrintf(PETSC_COMM_WORLD, "Residual norm %f \n", residualNorm);
  if(!sol.CPName().empty()){
    outfile << residualNorm << flush << endl;
  }
  it = it + 1;

  if(it%1==0){
    string solname = "Sol";
    sol.save(solname);
  }

  /*for (unsigned int iTerm=0; iTerm < sol.rank(); iTerm++) {
    for (unsigned int iVar = 0; iVar < sol.nVar(); iVar++) {
      VecView(sol.terms(iTerm, iVar), PETSC_VIEWER_STDOUT_WORLD);
    }
  }*/

}
if(!sol.CPName().empty()){
  outfile.close();
}

}


/*Solving with CPTT
  - gradient of the residual with CPTT compression and exact line search
  - overloaded to accept an initial guess
*/
void multilinearSolver::solvewithCPTT(operatorTensor& A, CPTensor& F, CPTensor& sol_0, CPTensor& sol, double tol){
// Initialise residual R=F - A x_0:
CPTensor AsolUpdated;
A.applyOperator(sol_0, AsolUpdated);
CPTensor R;
R.copyTensorFrom(F);
R.sum(AsolUpdated, -1.0, true);
AsolUpdated.clear();

ofstream outfile;
if(!sol.CPName().empty()){
  string fileName = string("residual_") + sol.CPName() + string(".txt");
  outfile.open(fileName.c_str());
}

// Computing the Adjoint of the operator:
operatorTensor adjA = A.adjoint();

// Initialising a solution equal to sol_0:
sol.setTensorComm(F.theComm());
sol.set_nVar(F.nVar());
vector<unsigned int> resPerDim = F.nDof_var();
sol.set_nDof_var(resPerDim);
sol.copyTensorFrom(sol_0);

// start looping
double residualNorm = sqrt(R.norm2CP());

PetscPrintf(PETSC_COMM_WORLD, "Residual norm %f \n", residualNorm);


unsigned int it = 0;
while ( residualNorm > tol) {

  CPTensor t(R.nVar(), R.nDof_var(), R.theComm());
  t.compute_CPTT_Term(R);
  cout << "t computed" << endl;
  cout << t.coeffs(0) << endl;


  CPTensor y = A.applyOperator(R);

  CPTensor w = A.applyOperator(t);

  double T1= y.scalarProd(w);
  cout << "T1 = " << T1 << endl;


  CPTensor RtoBeApprox;
  if(T1<0){
    RtoBeApprox.copyTensorFrom(R);
    RtoBeApprox.sum(t,-1.0, true);
  }
  while (T1 < 0) {
    cout << "entering the loop" << endl;
    CPTensor tToAdd(R.nVar(), R.nDof_var(), R.theComm());
    tToAdd.compute_CPTT_Term(RtoBeApprox);

    CPTensor wToAdd = A.applyOperator(tToAdd);
    w.sum(wToAdd, 1.0, true);
    t.sum(tToAdd, 1.0, false);
    T1 = T1 + y.scalarProd(wToAdd);
    RtoBeApprox.sum(tToAdd,-1.0, true);
  }
  // compute T2 =  <AA^* t, AA^* t>;
  //CPTensor update = adjA.applyOperator(t);
  CPTensor update;
  adjA.applyOperator(t, update);

  //CPTensor z=A.applyOperator(update);
  CPTensor z;
  A.applyOperator(update, z);

  double T2 = z.norm2CP();
  cout << "T2 = " << T2 << endl;
  cout << "alpha = " << T1/T2 << endl;

  // free the memory:
  w.clear();
  y.clear();
  z.clear();

  // exact line search:
  double alpha = T1 / T2;

  cout << "BEFORE UPDATE" << endl;

  // update the solution:
  sol.sum(update, alpha, false);
  //update.clear();

  cout << "AFTER UPDATE" << endl;

  cout << endl;

  //if(it == 5){exit(1);}


  // renormalise the terms
  double exponent = 1.0/static_cast<double>(sol.nVar());
  for(unsigned int iTerm=0; iTerm<sol.rank(); iTerm++){
    double c_i = sol.coeffs(iTerm);
    double sign = 1;
    if(c_i<0.0){
      c_i = -1.0*c_i;
      sign = -1.0;
    }
    double scaling = pow(c_i, exponent);
    for(unsigned int iVar=0; iVar<sol.nVar(); iVar++){
      sol.rescaleTerm(iTerm, iVar, scaling);
    }
    sol.set_coeffs(iTerm, sign);
  }



  cout << "Sol computed" << endl;
  /*for (unsigned int iTerm=0; iTerm < sol.rank(); iTerm++) {
    for (unsigned int iVar = 0; iVar < sol.nVar(); iVar++) {
      VecView(sol.terms(iTerm, iVar), PETSC_VIEWER_STDOUT_WORLD);
    }
  }*/
/*
// coefficients optimisation:
  double maxEntry = -1e15;
  double minEntry = 1e15;
  Mat scalMat;
  a.initMat(scalMat, sol.rank(), sol.rank(),F.theComm());
  for(unsigned int iTerm=0; iTerm<sol.rank(); iTerm++){
    vector<Vec> I_term = sol.terms(iTerm);
    CPTensor It(I_term, 1.0, F.theComm());
    CPTensor AIt = A.applyOperator(It);
    for(unsigned int jTerm=iTerm; jTerm<sol.rank(); jTerm++){
      vector<Vec> J_term = sol.terms(jTerm);
      CPTensor Jt(J_term, 1.0, F.theComm());
      CPTensor AJt = A.applyOperator(Jt);

      double val = AIt.scalarProd(AJt);
      if(val>maxEntry){
        maxEntry = val;
      }
      if(val<minEntry){
        minEntry = val;
      }

      a.setMatEl(scalMat,iTerm,jTerm,val,F.theComm());
      if(iTerm != jTerm){
        a.setMatEl(scalMat,jTerm,iTerm,val,F.theComm());
      }
  //    AJt.clear();
    //  Jt.clear();
    }
  //  AIt.clear();
  //  It.clear();
  }
  a.finalizeMat(scalMat);
  double rescalingCoeff = 1.0/(maxEntry - minEntry);
  MatScale(scalMat, rescalingCoeff);

  //MatView(scalMat, PETSC_VIEWER_STDOUT_WORLD);

  Vec scalVec;
  a.initVec(scalVec,sol.rank(), F.theComm());
  for(unsigned int iTerm=0; iTerm < sol.rank(); iTerm++){
    double entry = 0.0;
    vector<Vec> I_term = sol.terms(iTerm);
    CPTensor It(I_term, 1.0, F.theComm());
    CPTensor AIt = A.applyOperator(It);

    for(unsigned int kTerm=0; kTerm < F.rank(); kTerm++){
      vector<Vec> K_term = F.terms(kTerm);
      CPTensor Kt(K_term, 1.0, F.theComm());
      double val = AIt.scalarProd(Kt);
      val = val * F.coeffs(kTerm);
      entry = entry + val;
    //  Kt.clear();
    }
    //AIt.clear();
    //It.clear();
    a.setVecEl(scalVec, iTerm, entry, F.theComm());
  }
  a.finalizeVec(scalVec);
  VecScale(scalVec, rescalingCoeff);
  //VecView(scalVec, PETSC_VIEWER_STDOUT_WORLD);

  Vec updateCoeffs;
  a.initVec(updateCoeffs, sol.rank(), F.theComm());
  a.finalizeVec(updateCoeffs);

  cout << "Printing new coefficients" << endl;
  KSP scalSolver;
  //KSPType type = KSPCG;
  KSPType type = KSPPREONLY;
  a.initLinSolve(scalSolver, scalMat, F.theComm(), type);
  a.solveLin(scalSolver, scalVec, updateCoeffs, F.theComm(), false);
  for(unsigned int iTerm=0; iTerm<sol.rank(); iTerm++){
    double thisCoeff = a.getVecEl(updateCoeffs, iTerm, F.theComm());

    cout << thisCoeff << endl;

    sol.set_coeffs(iTerm, thisCoeff);
  }
  cout << endl;

  VecDestroy(&scalVec);
  VecDestroy(&updateCoeffs);
  MatDestroy(&scalMat);
  KSPDestroy(&scalSolver);
*/


  //CPTensor AsolUpdated = A.applyOperator(sol);
  CPTensor AsolUpdated;
  A.applyOperator(sol, AsolUpdated);

  R.clear();
  R.copyTensorFrom(F);
  R.sum(AsolUpdated,-1.0, false);
  residualNorm = sqrt(R.norm2CP());
  PetscPrintf(PETSC_COMM_WORLD, "Residual norm %f \n", residualNorm);
  if(!sol.CPName().empty()){
    outfile << residualNorm << flush << endl;
  }
  it = it + 1;

  if(it%1==0){
    string solname = "Sol";
    sol.save(solname);
  }

  /*for (unsigned int iTerm=0; iTerm < sol.rank(); iTerm++) {
    for (unsigned int iVar = 0; iVar < sol.nVar(); iVar++) {
      VecView(sol.terms(iTerm, iVar), PETSC_VIEWER_STDOUT_WORLD);
    }
  }*/

}
if(!sol.CPName().empty()){
  outfile.close();
}

}


/*Solving with CPTT with recompression
  - gradient of the residual with CPTT compression and exact line search
*/
void multilinearSolver::solvewithCPTT_recompress(operatorTensor& A, CPTensor& F, CPTensor& sol, double tol){
// Initialise residual R=F:
CPTensor R;
R.copyTensorFrom(F);
ofstream outfile;
if(!sol.CPName().empty()){
  string fileName = string("residual_") + sol.CPName() + string(".txt");
  outfile.open(fileName.c_str());
}

// Computing the Adjoint of the operator:
operatorTensor adjA = A.adjoint();

// Initialising an empty solution:
sol.setTensorComm(F.theComm());
sol.set_nVar(F.nVar());
vector<unsigned int> resPerDim = F.nDof_var();
sol.set_nDof_var(resPerDim);

unsigned int dim = F.nVar();
MPI_Comm theComm = F.theComm();
CPTensor solToRecompress(dim, resPerDim, theComm);

// start looping
double residualNorm = sqrt(R.norm2CP());

PetscPrintf(PETSC_COMM_WORLD, "Residual norm %f \n", residualNorm);

unsigned int compressTol = 20;
unsigned int it = 0;
while ( residualNorm > tol) {

  CPTensor t(R.nVar(), R.nDof_var(), R.theComm());
  t.compute_CPTT_Term(R);
  cout << "t computed" << endl;
  cout << t.coeffs(0) << endl;


  CPTensor y = A.applyOperator(R);

  CPTensor w = A.applyOperator(t);

  double T1= y.scalarProd(w);
  cout << "T1 = " << T1 << endl;


  CPTensor RtoBeApprox;
  if(T1<0){
    RtoBeApprox.copyTensorFrom(R);
    RtoBeApprox.sum(t,-1.0, true);
  }
  while (T1 < 0) {
    cout << "entering the loop" << endl;
    CPTensor tToAdd(R.nVar(), R.nDof_var(), R.theComm());
    tToAdd.compute_CPTT_Term(RtoBeApprox);

    CPTensor wToAdd = A.applyOperator(tToAdd);
    w.sum(wToAdd, 1.0, true);
    t.sum(tToAdd, 1.0, false);
    T1 = T1 + y.scalarProd(wToAdd);
    RtoBeApprox.sum(tToAdd,-1.0, true);
  }
  // compute T2 =  <AA^* t, AA^* t>;
  //CPTensor update = adjA.applyOperator(t);
  CPTensor update;
  adjA.applyOperator(t, update);

  //CPTensor z=A.applyOperator(update);
  CPTensor z;
  A.applyOperator(update, z);

  double T2 = z.norm2CP();
  cout << "T2 = " << T2 << endl;
  cout << "alpha = " << T1/T2 << endl;

  // free the memory:
  w.clear();
  y.clear();
  z.clear();

  // exact line search:
  double alpha = T1 / T2;

  cout << "BEFORE UPDATE" << endl;


  // update the solution:
  solToRecompress.sum(update, alpha, true);
  update.clear();

  cout << "AFTER UPDATE" << endl;



  // renormalise the terms
  double exponent = 1.0/static_cast<double>(sol.nVar());
  for(unsigned int iTerm=0; iTerm<sol.rank(); iTerm++){
    double c_i = sol.coeffs(iTerm);
    double sign = 1;
    if(c_i<0.0){
      c_i = -1.0*c_i;
      sign = -1.0;
    }
    double scaling = pow(c_i, exponent);
    for(unsigned int iVar=0; iVar<sol.nVar(); iVar++){
      sol.rescaleTerm(iTerm, iVar, scaling);
    }
    sol.set_coeffs(iTerm, sign);
  }



  cout << "Sol computed" << endl;

  if(solToRecompress.rank()>compressTol){
    cout << "Recompress solution" << endl;
    double normSol = sqrt(solToRecompress.norm2CP());
    sol.CPTT_ApproxOf(solToRecompress, 1.0e-3*residualNorm);
    solToRecompress.clear();
    solToRecompress.copyTensorFrom(sol);
    cout << "Solution rank after recompression = " << sol.rank() << endl;
    compressTol = compressTol + sol.rank() + A.numTerms();
  }



  /*for (unsigned int iTerm=0; iTerm < sol.rank(); iTerm++) {
    for (unsigned int iVar = 0; iVar < sol.nVar(); iVar++) {
      VecView(sol.terms(iTerm, iVar), PETSC_VIEWER_STDOUT_WORLD);
    }
  }*/
/*
// coefficients optimisation:
  double maxEntry = -1e15;
  double minEntry = 1e15;
  Mat scalMat;
  a.initMat(scalMat, sol.rank(), sol.rank(),F.theComm());
  for(unsigned int iTerm=0; iTerm<sol.rank(); iTerm++){
    vector<Vec> I_term = sol.terms(iTerm);
    CPTensor It(I_term, 1.0, F.theComm());
    CPTensor AIt = A.applyOperator(It);
    for(unsigned int jTerm=iTerm; jTerm<sol.rank(); jTerm++){
      vector<Vec> J_term = sol.terms(jTerm);
      CPTensor Jt(J_term, 1.0, F.theComm());
      CPTensor AJt = A.applyOperator(Jt);

      double val = AIt.scalarProd(AJt);
      if(val>maxEntry){
        maxEntry = val;
      }
      if(val<minEntry){
        minEntry = val;
      }

      a.setMatEl(scalMat,iTerm,jTerm,val,F.theComm());
      if(iTerm != jTerm){
        a.setMatEl(scalMat,jTerm,iTerm,val,F.theComm());
      }
  //    AJt.clear();
    //  Jt.clear();
    }
  //  AIt.clear();
  //  It.clear();
  }
  a.finalizeMat(scalMat);
  double rescalingCoeff = 1.0/(maxEntry - minEntry);
  MatScale(scalMat, rescalingCoeff);

  //MatView(scalMat, PETSC_VIEWER_STDOUT_WORLD);

  Vec scalVec;
  a.initVec(scalVec,sol.rank(), F.theComm());
  for(unsigned int iTerm=0; iTerm < sol.rank(); iTerm++){
    double entry = 0.0;
    vector<Vec> I_term = sol.terms(iTerm);
    CPTensor It(I_term, 1.0, F.theComm());
    CPTensor AIt = A.applyOperator(It);

    for(unsigned int kTerm=0; kTerm < F.rank(); kTerm++){
      vector<Vec> K_term = F.terms(kTerm);
      CPTensor Kt(K_term, 1.0, F.theComm());
      double val = AIt.scalarProd(Kt);
      val = val * F.coeffs(kTerm);
      entry = entry + val;
    //  Kt.clear();
    }
    //AIt.clear();
    //It.clear();
    a.setVecEl(scalVec, iTerm, entry, F.theComm());
  }
  a.finalizeVec(scalVec);
  VecScale(scalVec, rescalingCoeff);
  //VecView(scalVec, PETSC_VIEWER_STDOUT_WORLD);

  Vec updateCoeffs;
  a.initVec(updateCoeffs, sol.rank(), F.theComm());
  a.finalizeVec(updateCoeffs);

  cout << "Printing new coefficients" << endl;
  KSP scalSolver;
  //KSPType type = KSPCG;
  KSPType type = KSPPREONLY;
  a.initLinSolve(scalSolver, scalMat, F.theComm(), type);
  a.solveLin(scalSolver, scalVec, updateCoeffs, F.theComm(), false);
  for(unsigned int iTerm=0; iTerm<sol.rank(); iTerm++){
    double thisCoeff = a.getVecEl(updateCoeffs, iTerm, F.theComm());

    cout << thisCoeff << endl;

    sol.set_coeffs(iTerm, thisCoeff);
  }
  cout << endl;

  VecDestroy(&scalVec);
  VecDestroy(&updateCoeffs);
  MatDestroy(&scalMat);
  KSPDestroy(&scalSolver);
*/


  //CPTensor AsolUpdated = A.applyOperator(sol);
  CPTensor AsolUpdated;
  A.applyOperator(solToRecompress, AsolUpdated);

  R.clear();
  R.copyTensorFrom(F);
  R.sum(AsolUpdated,-1.0, false);
  residualNorm = sqrt(R.norm2CP());
  PetscPrintf(PETSC_COMM_WORLD, "Residual norm at the end of iteration %f \n", residualNorm);
  if(!sol.CPName().empty()){
    outfile << residualNorm << flush << endl;
  }
  it = it + 1;

  if(it%1==0){
    string solname = "Sol";
    sol.save(solname);
  }

  /*for (unsigned int iTerm=0; iTerm < sol.rank(); iTerm++) {
    for (unsigned int iVar = 0; iVar < sol.nVar(); iVar++) {
      VecView(sol.terms(iTerm, iVar), PETSC_VIEWER_STDOUT_WORLD);
    }
  }*/

}
if(!sol.CPName().empty()){
  outfile.close();
}

}


/*  Solving with CPTT, preconditionned version
  - provide a 1 rank tensorised preconditioner
*/

void multilinearSolver::solvewithCPTT_precond(operatorTensor& A, operatorTensor& P, CPTensor& F, CPTensor& sol, double tol){

if(P.numTerms() != 1){
  puts("Define a rank-1 preconditioner!");
  exit(1);
}

CPTensor unprecondR;
unprecondR.copyTensorFrom(F);
double unprecResidualNorm = sqrt(unprecondR.norm2CP());
PetscPrintf(PETSC_COMM_WORLD, "Unpreconditionned Residual norm %f \n", unprecResidualNorm);
unprecondR.clear();


// 1- compute P^-1 F
unsigned int dim_rhs = F.nVar();
vector<unsigned int> resPerDim_rhs = F.nDof_var();
unsigned int rank_rhs = F.rank();
MPI_Comm theComm_rhs = F.theComm();


vector<vector<Vec> > residualTerms(rank_rhs);
vector<double> residualCoeffs(rank_rhs);
for(unsigned int iTerm=0; iTerm<rank_rhs; iTerm++){
  vector<Vec> thisTerm(dim_rhs);
  for(unsigned int iVar=0; iVar<dim_rhs; iVar++){
    KSP ksp;
    Mat matP = P.op(0,iVar);
    KSPCreate(PETSC_COMM_WORLD,&ksp);
    KSPSetOperators(ksp, matP, matP); // => use A to build preconditioner
    KSPSetType(ksp, KSPGMRES);
    KSPSetFromOptions(ksp);
    Vec thisSol;
    a.initVec(thisSol,resPerDim_rhs[iVar],PETSC_COMM_WORLD);
    a.finalizeVec(thisSol);
    Vec rhsF = F.terms(iTerm,iVar);
    // solving the system:
    KSPSolve(ksp, rhsF, thisSol);
    thisTerm[iVar] = thisSol;
    KSPDestroy(&ksp);
  }
  residualTerms[iTerm] = thisTerm;
  residualCoeffs[iTerm] = F.coeffs(iTerm);
}
CPTensor R(residualTerms, residualCoeffs, theComm_rhs);
cout << "Residual has been defined" << endl;

// start the iteration
ofstream outfile;
if(!sol.CPName().empty()){
  string fileName = string("residual_") + sol.CPName() + string(".txt");
  outfile.open(fileName.c_str());
}

// Computing the Adjoint of the operator:
operatorTensor adjA = A.adjoint();
operatorTensor adjP = P.adjoint();

// Initialising an empty solution:
sol.setTensorComm(theComm_rhs);
sol.set_nVar(dim_rhs);
sol.set_nDof_var(resPerDim_rhs);

// start looping
double residualNorm = sqrt(R.norm2CP());

PetscPrintf(PETSC_COMM_WORLD, "Residual norm %f \n", residualNorm);


unsigned int it = 0;
while ( residualNorm > tol) {

  CPTensor t(R.nVar(), R.nDof_var(), R.theComm());
  t.compute_CPTT_Term(R);
  cout << "t computed" << endl;
  cout << t.coeffs(0) << endl;

  // compute the preconditionned update:
  vector<vector<Vec> > qTerms(t.rank());
  vector<double> qCoeffs(t.rank());
  for(unsigned int iTerm=0; iTerm<t.rank(); iTerm++){
    vector<Vec> thisTerm(dim_rhs);
    for(unsigned int iVar=0; iVar<dim_rhs; iVar++){
      KSP ksp;
      Mat matP = adjP.op(0,iVar);
      KSPCreate(PETSC_COMM_WORLD,&ksp);
      KSPSetOperators(ksp, matP, matP); // => use A to build preconditioner
      KSPSetType(ksp, KSPGMRES);
      KSPSetFromOptions(ksp);
      Vec thisSol;
      a.initVec(thisSol,resPerDim_rhs[iVar],PETSC_COMM_WORLD);
      a.finalizeVec(thisSol);
      Vec rhsF = t.terms(iTerm,iVar);
      // solving the system:
      KSPSolve(ksp, rhsF, thisSol);
      thisTerm[iVar] = thisSol;
      KSPDestroy(&ksp);
    }
    qTerms[iTerm] = thisTerm;
    qCoeffs[iTerm] = t.coeffs(iTerm);
  }
  CPTensor q(qTerms, qCoeffs, theComm_rhs);


  // compute exact line search:
  CPTensor y = adjA.applyOperator(q);
  CPTensor w = A.applyOperator(y);
  vector<vector<Vec> > zTerms(w.rank());
  vector<double> zCoeffs(w.rank());
  for(unsigned int iTerm=0; iTerm<w.rank(); iTerm++){
    vector<Vec> thisTerm(dim_rhs);
    for(unsigned int iVar=0; iVar<dim_rhs; iVar++){
      KSP ksp;
      Mat matP = P.op(0,iVar);
      KSPCreate(PETSC_COMM_WORLD,&ksp);
      KSPSetOperators(ksp, matP, matP); // => use A to build preconditioner
      KSPSetType(ksp, KSPGMRES);
      KSPSetFromOptions(ksp);
      Vec thisSol;
      a.initVec(thisSol,resPerDim_rhs[iVar],PETSC_COMM_WORLD);
      a.finalizeVec(thisSol);
      Vec rhsF = w.terms(iTerm,iVar);
      // solving the system:
      KSPSolve(ksp, rhsF, thisSol);
      thisTerm[iVar] = thisSol;
      KSPDestroy(&ksp);
    }
    zTerms[iTerm] = thisTerm;
    zCoeffs[iTerm] = w.coeffs(iTerm);
  }
  CPTensor z(zTerms, zCoeffs, theComm_rhs);

  double T1= z.scalarProd(R);
  cout << "T1 = " << T1 << endl;

  // If T1<0 provide a warning, see unpreconditionned function for implementation

  // compute T2 =  <AA^* t, AA^* t>;
  //CPTensor update = adjA.applyOperator(t);
  double T2 = z.norm2CP();
  cout << "T2 = " << T2 << endl;
  cout << "alpha = " << T1/T2 << endl;

  // free the memory:
  w.clear();
  y.clear();

  // exact line search:
  double alpha = T1 / T2;

  cout << "BEFORE UPDATE" << endl;
  CPTensor update;
  adjA.applyOperator(q, update);

  // update the solution:
  sol.sum(update, alpha, false);
  //update.clear();

  cout << "AFTER UPDATE" << endl;
  cout << endl;


  //CPTensor AsolUpdated = A.applyOperator(sol);
  R.sum(z, -alpha, true);
  z.clear();
  t.clear();
  q.clear();

  residualNorm = sqrt(R.norm2CP());
  PetscPrintf(PETSC_COMM_WORLD, "Residual norm %f \n", residualNorm);

  CPTensor AsolUpdated;
  A.applyOperator(sol, AsolUpdated);

  CPTensor unprecondR;
  unprecondR.copyTensorFrom(F);
  unprecondR.sum(AsolUpdated,-1.0, true);
  double unprecResidualNorm = sqrt(unprecondR.norm2CP());
  PetscPrintf(PETSC_COMM_WORLD, "Unpreconditionned Residual norm %f \n", unprecResidualNorm);
  unprecondR.clear();
  AsolUpdated.clear();

  if(!sol.CPName().empty()){
    outfile << residualNorm << flush << endl;
  }
  it = it + 1;

  if(it%1==0){
    string solname = "Sol";
    sol.save(solname);
  }

  /*for (unsigned int iTerm=0; iTerm < sol.rank(); iTerm++) {
    for (unsigned int iVar = 0; iVar < sol.nVar(); iVar++) {
      VecView(sol.terms(iTerm, iVar), PETSC_VIEWER_STDOUT_WORLD);
    }
  }*/

}
if(!sol.CPName().empty()){
  outfile.close();
}

}


/* CPTT and conjugate gradient method
  - Input: operator tensor, rhs, tolerance
  - Output: solution
    REMARK: in this function there is no initial guess: implicitly set to 0.
  - 2 inner iterations of CG
*/
void multilinearSolver::solveCPTT_CG(operatorTensor& A, CPTensor& F, CPTensor& sol, double tol, bool RECOMP=false){
    unsigned int nVar = F.nVar();
    vector<unsigned int> dofPerDim = F.nDof_var();
    MPI_Comm theComm = F.theComm();

    // Initialising the solution fields:
    sol.set_nVar(nVar);
    sol.set_nDof_var(dofPerDim);
    sol.setTensorComm(theComm);

    // Copy the right hand side:
    // Initialise residual R=F:
    CPTensor R;
    R.copyTensorFrom(F);

    // compute the residual norm:
    double norm2Res = R.norm2CP();
    double residualNorm = sqrt(norm2Res);
    PetscPrintf(F.theComm(), "Intial Residual norm = %e \n\n", residualNorm);
    if(residualNorm<tol){
      PetscPrintf(F.theComm(), "Best solution is 0, nothing to be done \n");
      return;
    }


    while(residualNorm>tol){
      // First step: compress R and retrieve one CPTT term
      CPTensor compF;
      compF.CPTT_ApproxOf(R,residualNorm*0.99, false);
      R.sum(compF, -1.0, true);
      PetscPrintf(F.theComm(), "Rank of compF = %d \n", compF.rank());

      // Second step: compute the CG approximation.
      PetscPrintf(F.theComm(), "Entering CG step...\n");
      CPTensor CGsol(nVar, dofPerDim, theComm);
      CPTensor CGResidual;
      CPTensor conjDir;
      CGResidual.copyTensorFrom(compF);
      conjDir.copyTensorFrom(compF);

      // compute the norm of the residual
      double CGnorm2Res = CGResidual.norm2CP();
      const double initialNorm = sqrt(CGnorm2Res);
      PetscPrintf(F.theComm(), "CG Residual norm = %e \n", sqrt(CGnorm2Res));

      unsigned int it=0;
      while(sqrt(CGnorm2Res)>1.0e-4*initialNorm && (it<1)){
        double alphaNum = CGnorm2Res;
        CPTensor krilovTens;
        A.applyOperator(conjDir, krilovTens);
        double alphaDen = conjDir.scalarProd(krilovTens);
        double alpha = alphaNum/alphaDen;


        // solution update (copy the terms conjDir will be destroyed);
        CGsol.sum(conjDir, alpha, true);
        CGResidual.sum(krilovTens, -alpha, true);

        CGnorm2Res = CGResidual.norm2CP();
        PetscPrintf(F.theComm(), "CG Residual norm = %e \n", sqrt(CGnorm2Res));

        // updating the conjugate direction
        double betaDen = alphaNum;
        double betaNum = norm2Res;
        double beta = betaNum/betaDen;

        conjDir.multiplyByScalar(beta);
        conjDir.sum(CGResidual, 1.0, true);
        krilovTens.clear();
        it +=1;
      }

      conjDir.clear();
      CGResidual.clear();
      compF.clear();


      // sum this sol to CGSol;
      if(RECOMP){
        CPTensor updateSol;
        double normUp = sqrt(CGsol.norm2CP());
        cout << "Recompressing the solution\n";
        updateSol.CPTT_ApproxOf(CGsol, 1.0e-2*normUp, false);
        sol.sum(updateSol, 1.0, true);
        updateSol.clear();
      }
      else{
        sol.sum(CGsol,1.0,true);
      }
      PetscPrintf(F.theComm(), "Done, solution rank = %d\n", sol.rank());

      CGsol.clear();
      compF.clear();

      if(it%1==0){
        string solname = "Sol";
        sol.save(solname);
      }


      // compute the new residual:
      CPTensor AsolUpdated;
      A.applyOperator(sol, AsolUpdated);

      R.clear();
      R.copyTensorFrom(F);
      R.sum(AsolUpdated,-1.0, false);
      residualNorm = sqrt(R.norm2CP());
      PetscPrintf(F.theComm(), "Residual norm at the end of the Iteration  = %e \n\n", residualNorm);
    }
}


/* CPTT and conjugate gradient method with initial guess, overloaded
  - Input: operator tensor, rhs, tolerance
  - Output: solution
    REMARK: in this function there is no initial guess: implicitly set to 0.
  - 2 inner iterations of CG
*/
void multilinearSolver::solveCPTT_CG(operatorTensor& A, CPTensor& F, CPTensor& sol_0, CPTensor& sol, double tol, bool RECOMP=false){
    unsigned int nVar = F.nVar();
    vector<unsigned int> dofPerDim = F.nDof_var();
    MPI_Comm theComm = F.theComm();

    // Initialising the solution fields:
    sol.set_nVar(nVar);
    sol.set_nDof_var(dofPerDim);
    sol.setTensorComm(theComm);
    sol.copyTensorFrom(sol_0);

    // Copy the right hand side:
    // Initialise residual R=F:
    CPTensor R;
    R.copyTensorFrom(F);
    CPTensor AsolUpdated;
    A.applyOperator(sol, AsolUpdated);
    R.sum(AsolUpdated, -1.0, true);
    AsolUpdated.clear();

    // compute the residual norm:
    double norm2Res = R.norm2CP();
    double residualNorm = sqrt(norm2Res);
    PetscPrintf(F.theComm(), "Intial Residual norm = %e \n\n", residualNorm);
    if(residualNorm<tol){
      PetscPrintf(F.theComm(), "Best solution is 0, nothing to be done \n");
      return;
    }


    while(residualNorm>tol){
      // First step: compress R and retrieve one CPTT term
      CPTensor compF;
      compF.CPTT_ApproxOf(R,residualNorm*0.99, false);
      R.sum(compF, -1.0, true);
      PetscPrintf(F.theComm(), "Rank of compF = %d \n", compF.rank());

      // Second step: compute the CG approximation.
      PetscPrintf(F.theComm(), "Entering CG step...\n");
      CPTensor CGsol(nVar, dofPerDim, theComm);
      CPTensor CGResidual;
      CPTensor conjDir;
      CGResidual.copyTensorFrom(compF);
      conjDir.copyTensorFrom(compF);

      // compute the norm of the residual
      double CGnorm2Res = CGResidual.norm2CP();
      const double initialNorm = sqrt(CGnorm2Res);
      PetscPrintf(F.theComm(), "CG Residual norm = %e \n", sqrt(CGnorm2Res));

      unsigned int it=0;
      while(sqrt(CGnorm2Res)>1.0e-4*initialNorm && (it<1)){
        double alphaNum = CGnorm2Res;
        CPTensor krilovTens;
        A.applyOperator(conjDir, krilovTens);
        double alphaDen = conjDir.scalarProd(krilovTens);
        double alpha = alphaNum/alphaDen;


        // solution update (copy the terms conjDir will be destroyed);
        CGsol.sum(conjDir, alpha, true);
        CGResidual.sum(krilovTens, -alpha, true);

        CGnorm2Res = CGResidual.norm2CP();
        PetscPrintf(F.theComm(), "CG Residual norm = %e \n", sqrt(CGnorm2Res));

        // updating the conjugate direction
        double betaDen = alphaNum;
        double betaNum = norm2Res;
        double beta = betaNum/betaDen;

        conjDir.multiplyByScalar(beta);
        conjDir.sum(CGResidual, 1.0, true);
        krilovTens.clear();
        it +=1;
      }

      conjDir.clear();
      CGResidual.clear();
      compF.clear();


      // sum this sol to CGSol;
      if(RECOMP){
        CPTensor updateSol;
        double normUp = sqrt(CGsol.norm2CP());
        cout << "Recompressing the solution\n";
        updateSol.CPTT_ApproxOf(CGsol, 1.0e-2*normUp, false);
        sol.sum(updateSol, 1.0, true);
        updateSol.clear();
      }
      else{
        sol.sum(CGsol,1.0,true);
      }
      PetscPrintf(F.theComm(), "Done, solution rank = %d\n", sol.rank());

      CGsol.clear();
      compF.clear();

      if(it%1==0){
        string solname = "Sol";
        sol.save(solname);
      }


      // compute the new residual:
      CPTensor AsolUpdated;
      A.applyOperator(sol, AsolUpdated);

      R.clear();
      R.copyTensorFrom(F);
      R.sum(AsolUpdated,-1.0, false);
      residualNorm = sqrt(R.norm2CP());
      PetscPrintf(F.theComm(), "Residual norm at the end of the Iteration  = %e \n\n", residualNorm);
    }
}


/* CPTT and precondionned conjugate gradient method
  - Input: operator tensor, rhs, tolerance
  - Output: solution
    REMARK: in this function there is no initial guess: implicitly set to 0.
  - 2 inner iterations of CG
*/
void multilinearSolver::solveCPTT_CG_precond(operatorTensor& A, operatorTensor& P, CPTensor& F, CPTensor& sol, double tol){
    unsigned int nVar = F.nVar();
    vector<unsigned int> dofPerDim = F.nDof_var();
    MPI_Comm theComm = F.theComm();

    // Initialising the solution fields:
    sol.set_nVar(nVar);
    sol.set_nDof_var(dofPerDim);
    sol.setTensorComm(theComm);

    // Copy the right hand side:
    // Initialise residual R=F:
    CPTensor R;
    R.copyTensorFrom(F);

    // compute the residual norm:
    double norm2Res = R.norm2CP();
    double residualNorm = sqrt(norm2Res);
    PetscPrintf(F.theComm(), "Intial Residual norm = %e \n\n", residualNorm);
    if(residualNorm<tol){
      PetscPrintf(F.theComm(), "Best solution is 0, nothing to be done \n");
      return;
    }


    while(residualNorm>tol){
      // First step: compress R and retrieve one CPTT term
      CPTensor compF;
      compF.CPTT_ApproxOf(R,residualNorm*0.99, false);
      R.sum(compF, -1.0, true);
      PetscPrintf(F.theComm(), "Rank of compF = %d \n", compF.rank());

      // Second step: compute the preconditionnned CG approximation.
      PetscPrintf(F.theComm(), "Entering CG step...\n");
      CPTensor CGsol(nVar, dofPerDim, theComm);
      CPTensor CGResidual;
      CPTensor conjDir;
      CGResidual.copyTensorFrom(compF);

      vector<vector<Vec> > residualTerms(compF.rank());
      vector<double> resCoeffs(compF.rank());
      for(unsigned int iTerm=0; iTerm<compF.rank(); iTerm++){
        vector<Vec> thisTerm(compF.nVar());
        for(unsigned int iVar=0; iVar<compF.nVar(); iVar++){
          KSP ksp;
          Mat matP = P.op(0,iVar);
          KSPCreate(PETSC_COMM_WORLD,&ksp);
          KSPSetOperators(ksp, matP, matP); // => use A to build preconditioner
          KSPSetType(ksp, KSPGMRES);
          KSPSetFromOptions(ksp);
          Vec thisSol;
          a.initVec(thisSol,compF.nDof_var(iVar),PETSC_COMM_WORLD);
          a.finalizeVec(thisSol);
          Vec rhsF = compF.terms(iTerm,iVar);
          // solving the system:
          KSPSolve(ksp, rhsF, thisSol);
          thisTerm[iVar] = thisSol;
          KSPDestroy(&ksp);
        }
        residualTerms[iTerm] = thisTerm;
        resCoeffs[iTerm] = compF.coeffs(iTerm);
      }
      CPTensor Z(residualTerms, resCoeffs, compF.theComm());
      conjDir.copyTensorFrom(Z);

      // compute the norm of the residual
      double CGnorm2Res = CGResidual.norm2CP();
      const double initialNorm = sqrt(CGnorm2Res);
      PetscPrintf(F.theComm(), "CG Residual norm = %e \n", sqrt(CGnorm2Res));

      unsigned int it=0;
      while(sqrt(CGnorm2Res)>1.0e-4*initialNorm && (it<1)){
        unsigned int dimRes = CGResidual.nVar();
        vector<unsigned int> resPerDimRes = CGResidual.nDof_var();
        MPI_Comm theCommRes = CGResidual.theComm();
        double alphaNum = Z.scalarProd(CGResidual);
        CPTensor krilovTens(dimRes, resPerDimRes, theCommRes);
        A.applyOperator(conjDir, krilovTens);
        double alphaDen = conjDir.scalarProd(krilovTens);
        double alpha = alphaNum/alphaDen;
        cout << "Alpha num = " << alphaNum << " Alpha den = " << alphaDen << " Alpha = " << alpha << endl;

        // solution update (copy the terms conjDir will be destroyed);
        CGsol.sum(conjDir, alpha, true);
        CGResidual.sum(krilovTens, -alpha, true);

        CGnorm2Res = CGResidual.norm2CP();
        PetscPrintf(F.theComm(), "CG Residual norm = %e \n", sqrt(CGnorm2Res));

        // updating the conjugate direction

        // pre-conditioning the residual;
        vector<vector<Vec> > residualTerms(krilovTens.rank());
        vector<double> resCoeffs(krilovTens.rank());
        for(unsigned int iTerm=0; iTerm<krilovTens.rank(); iTerm++){
          vector<Vec> thisTerm(krilovTens.nVar());
          for(unsigned int iVar=0; iVar<krilovTens.nVar(); iVar++){
            KSP ksp;
            Mat matP = P.op(0,iVar);
            KSPCreate(PETSC_COMM_WORLD,&ksp);
            KSPSetOperators(ksp, matP, matP); // => use A to build preconditioner
            KSPSetType(ksp, KSPGMRES);
            KSPSetFromOptions(ksp);
            Vec thisSol;
            a.initVec(thisSol,compF.nDof_var(iVar),PETSC_COMM_WORLD);
            a.finalizeVec(thisSol);
            Vec rhsF = krilovTens.terms(iTerm,iVar);
            // solving the system:
            KSPSolve(ksp, rhsF, thisSol);
            thisTerm[iVar] = thisSol;
            KSPDestroy(&ksp);
          }
          residualTerms[iTerm] = thisTerm;
          resCoeffs[iTerm] = krilovTens.coeffs(iTerm);
        }
        CPTensor toAddZ(residualTerms, resCoeffs, CGResidual.theComm());
        Z.sum(toAddZ, -alpha, true);
        toAddZ.clear();

        double betaDen = alphaNum;
        double betaNum = Z.scalarProd(CGResidual);
        double beta = betaNum/betaDen;

        conjDir.multiplyByScalar(beta);
        conjDir.sum(Z, 1.0, true);
        krilovTens.clear();
        it +=1;
      }

      conjDir.clear();
      CGResidual.clear();
      Z.clear();
      compF.clear();

      PetscPrintf(F.theComm(), "Done, solution rank = %d\n", CGsol.rank());

      // sum this sol to CGSol;
      sol.sum(CGsol,1.0,true);
      CGsol.clear();
      compF.clear();

      // compute the new residual:
      CPTensor AsolUpdated;
      A.applyOperator(sol, AsolUpdated);

      R.clear();
      R.copyTensorFrom(F);
      R.sum(AsolUpdated,-1.0, false);
      residualNorm = sqrt(R.norm2CP());
      PetscPrintf(F.theComm(), "Residual norm at the end of the Iteration  = %e \n\n", residualNorm);
    }

}



/* CPTT and bi-conjugate gradient stabilized method
  - Input: operator tensor, rhs, tolerance
  - Output: solution
    REMARK: in this function there is no initial guess: implicitly set to 0.
  - 2 inner iterations of BiCGStab (unpreconditionned)
*/
void multilinearSolver::solveCPTT_BiCGStab(operatorTensor& A, CPTensor& F, CPTensor& sol, double tol){
    unsigned int nVar = F.nVar();
    vector<unsigned int> dofPerDim = F.nDof_var();
    MPI_Comm theComm = F.theComm();

    // Initialising the solution fields:
    sol.set_nVar(nVar);
    sol.set_nDof_var(dofPerDim);
    sol.setTensorComm(theComm);

    // Copy the right hand side:
    // Initialise residual R=F:
    CPTensor R;
    R.copyTensorFrom(F);

    // compute the residual norm:
    double norm2Res = R.norm2CP();
    double residualNorm = sqrt(norm2Res);
    PetscPrintf(F.theComm(), "Intial Residual norm = %e \n\n", residualNorm);
    if(residualNorm<tol){
      PetscPrintf(F.theComm(), "Best solution is 0, nothing to be done \n");
      return;
    }


    while(residualNorm>tol){
      // First step: compress R and retrieve one CPTT term
      CPTensor compF;
      compF.CPTT_ApproxOf(R,residualNorm*0.99, false);
      //R.sum(compF, -1.0, true);
      PetscPrintf(F.theComm(), "Rank of compF = %d \n", compF.rank());

      // Second step: compute the Bi-CGStab approximation.
      PetscPrintf(F.theComm(), "Entering BiCG step...\n");
      CPTensor CGsol(nVar, dofPerDim, theComm);
      CPTensor CGResidual;
      CPTensor conjDir;
      CPTensor krilovTens;
      CPTensor initRes;
      CGResidual.copyTensorFrom(compF);
      conjDir.copyTensorFrom(compF);
      krilovTens.copyTensorFrom(compF);
      initRes.copyTensorFrom(compF);
      double alpha = 1.0;
      double omega = 1.0;
      double rho_0 = 1.0;
      double rho_i = 1.0;

      // compute the norm of the residual
      double CGnorm2Res = CGResidual.norm2CP();
      const double initialNorm = sqrt(CGnorm2Res);

      PetscPrintf(F.theComm(), "BiCG Residual norm = %e \n", sqrt(CGnorm2Res));

      unsigned int it=0;
      while(sqrt(CGnorm2Res)>1.0e-4*initialNorm && (it<1)){
        rho_0 = rho_i;
        rho_i = initRes.scalarProd(CGResidual);
        double beta = (rho_i/rho_0) * (alpha/omega);

        if(it>0){
          conjDir.sum(krilovTens,-omega,true);
          conjDir.multiplyByScalar(beta);
          conjDir.sum(CGResidual, 1.0, true);
        }
        krilovTens.clear();
        A.applyOperator(conjDir, krilovTens);

        double alphaDen = initRes.scalarProd(krilovTens);
        alpha = rho_i / alphaDen;
        CGsol.sum(conjDir, alpha, true);

        CPTensor sTens;
        sTens.copyTensorFrom(CGResidual);
        sTens.sum(krilovTens, -alpha, true);

        CPTensor tTens;
        A.applyOperator(sTens);

        double omega_num = tTens.scalarProd(sTens);
        double omega_den = tTens.norm2CP();
        if(omega_den<1.0e-12){
          omega = 0.01;
        }
        else{
          omega = omega_num/omega_den;
        }


        CGsol.sum(sTens, omega, true);
        CGResidual.clear();
        CGResidual.copyTensorFrom(sTens);
        CGResidual.sum(tTens, -omega, true);

        CGnorm2Res = CGResidual.norm2CP();
        PetscPrintf(F.theComm(), "BiCG Residual norm = %e \n", sqrt(CGnorm2Res));

        sTens.clear();
        tTens.clear();

        it +=1;
      }
      initRes.clear();
      conjDir.clear();
      CGResidual.clear();



      PetscPrintf(F.theComm(), "Done, solution rank = %d\n", CGsol.rank());

      // sum this sol to CGSol;
      sol.sum(CGsol,1.0,true);
      CGsol.clear();
      compF.clear();

      // compute the new residual:
      CPTensor AsolUpdated;
      A.applyOperator(sol, AsolUpdated);

      R.clear();
      R.copyTensorFrom(F);
      R.sum(AsolUpdated,-1.0, false);
      residualNorm = sqrt(R.norm2CP());
      PetscPrintf(F.theComm(), "Residual norm at the end of the Iteration  = %e \n\n", residualNorm);
    }

}



/*  Fix point, based on the fact that a rank-one operator can be easily solved.
  - input: operator tensor, rhs, tol
  - output: the solution
*/
void multilinearSolver::fixPoint(operatorTensor& A, CPTensor& F, CPTensor& sol, double tol){
  // init the resolutions
  unsigned int nVar = F.nVar();
  vector<unsigned int> dofPerDim = F.nDof_var();
  MPI_Comm theComm = F.theComm();
  unsigned int numTerms_A = A.numTerms();

  // Decomposing A as a vector of operator tensors of rank 1
  vector<operatorTensor> opA(numTerms_A);
  for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
    vector<Mat> thisTerm = A.op(lTerm);
    operatorTensor thisOp(thisTerm, theComm);
    opA[lTerm] = thisOp;
  }

  // Initialising the solution fields:
  sol.set_nVar(nVar);
  sol.set_nDof_var(dofPerDim);
  sol.setTensorComm(theComm);

  // Copy the right hand side:
  // Initialise residual R=F:
  CPTensor R;
  R.copyTensorFrom(F);

  double normRes = sqrt(R.norm2CP());
  PetscPrintf(theComm, "Residual norm = %e \n", normRes);

  unsigned int it = 0;
  while(normRes > tol){

    vector<CPTensor> y(numTerms_A);
    // Computing the terms:
    for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
      CPTensor thisRes;
      thisRes.copyTensorFrom(F);
      for(unsigned int mTerm=0; mTerm<numTerms_A; mTerm++){
        if(mTerm != lTerm){
          CPTensor toAdd;
          opA[mTerm].applyOperator(sol, toAdd);
          thisRes.sum(toAdd, -1.0, false);
        }
      }
      // solving the system for the rank one operator A_l
      vector<vector<Vec> > this_terms(thisRes.rank());
      vector<double> this_coeff(thisRes.rank());
      for(unsigned int iTerm=0; iTerm<thisRes.rank(); iTerm++){
        this_terms[iTerm].resize(F.nVar());
      }

      for(unsigned int iVar=0; iVar<F.nVar(); iVar++){
        Mat matP = A.op(lTerm,iVar);
        KSP ksp;
        KSPCreate(PETSC_COMM_WORLD,&ksp);
        KSPSetOperators(ksp, matP, matP); // => use A to build preconditioner
        KSPSetType(ksp, KSPGMRES);
        KSPSetFromOptions(ksp);
        for(unsigned int iTerm=0; iTerm<thisRes.rank(); iTerm++){
          Vec thisSol;
          a.initVec(thisSol,dofPerDim[iVar],PETSC_COMM_WORLD);
          a.finalizeVec(thisSol);
          Vec rhsF = thisRes.terms(iTerm,iVar);
          // solving the system:
          KSPSolve(ksp, rhsF, thisSol);
          this_terms[iTerm][iVar] = thisSol;
        }
        KSPDestroy(&ksp);
      }
      for(unsigned int iTerm=0; iTerm<thisRes.rank(); iTerm++){
        this_coeff[iTerm] = thisRes.coeffs(iTerm);
      }
      CPTensor this_y(this_terms, this_coeff, theComm);
      y[lTerm] = this_y;
    }

    // Exact line search stage.
    PetscPrintf(theComm, "Exact line search...");
    if(it==0){
      vector<CPTensor> z(numTerms_A);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        CPTensor z_entry = A.applyOperator(y[lTerm]);
        z[lTerm] = z_entry;
      }

      Mat lineSearchMat;
      a.initMat(lineSearchMat, numTerms_A, numTerms_A, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        for(unsigned int mTerm=0; mTerm<=lTerm; mTerm++){
          double matrixEntry = z[lTerm].scalarProd(z[mTerm]);
          a.setMatEl(lineSearchMat, lTerm, mTerm, matrixEntry, theComm);
          if(lTerm != mTerm){
            a.setMatEl(lineSearchMat, mTerm, lTerm, matrixEntry, theComm);
          }
        }
      }
      a.finalizeMat(lineSearchMat);

      Vec lineSearchVec;
      a.initVec(lineSearchVec, numTerms_A, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double vecEntry = z[lTerm].scalarProd(R);
        a.setVecEl(lineSearchVec, lTerm, vecEntry, theComm);
      }
      a.finalizeVec(lineSearchVec);

      // solving the linear system:
      Vec thisSol;
      a.initVec(thisSol, numTerms_A, theComm);
      a.finalizeVec(thisSol);

      KSP ksp;
      KSPCreate(PETSC_COMM_WORLD,&ksp);
      KSPSetOperators(ksp, lineSearchMat, lineSearchMat); // => use A to build preconditioner
      KSPSetType(ksp, KSPGMRES);
      KSPSetFromOptions(ksp);
      KSPSolve(ksp, lineSearchVec, thisSol);
      //VecView(thisSol, PETSC_VIEWER_STDOUT_WORLD);

      KSPDestroy(&ksp);
      MatDestroy(&lineSearchMat);
      VecDestroy(&lineSearchVec);
      PetscPrintf(theComm, "done.\n");

      // updating the solution and the residual:
      CPTensor update(F.nVar(), dofPerDim, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double coeff = a.getVecEl(thisSol, lTerm, theComm);
        update.sum(y[lTerm], coeff, true);
        R.sum(z[lTerm], -coeff, true);
      }

      sol.sum(update, 1.0, true);
      update.clear();
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        y[lTerm].clear();
        z[lTerm].clear();
      }
    }
    else{
      vector<CPTensor> z(numTerms_A+1);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        CPTensor z_entry = A.applyOperator(y[lTerm]);
        z[lTerm] = z_entry;
      }
      CPTensor AsolUpdated = A.applyOperator(sol);
      z[numTerms_A] = AsolUpdated;

      Mat lineSearchMat;
      a.initMat(lineSearchMat, numTerms_A+1, numTerms_A+1, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        for(unsigned int mTerm=0; mTerm<=lTerm; mTerm++){
          double matrixEntry = z[lTerm].scalarProd(z[mTerm]);
          a.setMatEl(lineSearchMat, lTerm, mTerm, matrixEntry, theComm);
          if(lTerm != mTerm){
            a.setMatEl(lineSearchMat, mTerm, lTerm, matrixEntry, theComm);
          }
        }
      }
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double matrixEntry = z[lTerm].scalarProd(z[numTerms_A]);
        a.setMatEl(lineSearchMat, lTerm, numTerms_A, matrixEntry, theComm);
        a.setMatEl(lineSearchMat, numTerms_A, lTerm, matrixEntry, theComm);
      }
      double matrixEntry = z[numTerms_A].scalarProd(z[numTerms_A]);
      a.setMatEl(lineSearchMat, numTerms_A, numTerms_A, matrixEntry, theComm);
      a.finalizeMat(lineSearchMat);

      Vec lineSearchVec;
      a.initVec(lineSearchVec, numTerms_A+1, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double vecEntry = z[lTerm].scalarProd(F); // bacause the current sol is included in the optimisation
        a.setVecEl(lineSearchVec, lTerm, vecEntry, theComm);
      }
      double vecEntry = z[numTerms_A].scalarProd(F); // bacause the current sol is included in the optimisation
      a.setVecEl(lineSearchVec, numTerms_A, vecEntry, theComm);
      a.finalizeVec(lineSearchVec);

      // solving the linear system:
      Vec thisSol;
      a.initVec(thisSol, numTerms_A+1, theComm);
      a.finalizeVec(thisSol);

      KSP ksp;
      KSPCreate(PETSC_COMM_WORLD,&ksp);
      KSPSetOperators(ksp, lineSearchMat, lineSearchMat); // => use A to build preconditioner
      KSPSetType(ksp, KSPGMRES);
      KSPSetFromOptions(ksp);
      KSPSolve(ksp, lineSearchVec, thisSol);
      //VecView(thisSol, PETSC_VIEWER_STDOUT_WORLD);

      KSPDestroy(&ksp);
      MatDestroy(&lineSearchMat);
      VecDestroy(&lineSearchVec);
      PetscPrintf(theComm, "done.\n");

      // updating the solution and the residual:
      CPTensor update(F.nVar(), dofPerDim, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double coeff = a.getVecEl(thisSol, lTerm, theComm);
        update.sum(y[lTerm], coeff, true);
      }
      double rescaleCoeff = a.getVecEl(thisSol, numTerms_A, theComm);
      sol.multiplyByScalar(rescaleCoeff);
      sol.sum(update, 1.0, true);
      update.clear();
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        y[lTerm].clear();
        z[lTerm].clear();
      }
      z[numTerms_A].clear();
      R.clear();
      R.copyTensorFrom(F);
      CPTensor AtimesSol = A.applyOperator(sol);
      R.sum(AtimesSol, -1.0, true);
      AtimesSol.clear();
    }

    if(it%10==0){
      string solname = "Sol";
      sol.save(solname);
    }

    normRes = sqrt(R.norm2CP());
    PetscPrintf(theComm, "Solution rank = %d \n", sol.rank());
    PetscPrintf(theComm, "Residual norm = %e \n", normRes);
    it += 1;
  }
  // end of function
}


/*  Fix point, based on the fact that a rank-one operator can be easily solved.
  - input: operator tensor, rhs, tol
  - output: the solution
  - overloaded with initial guess
*/
void multilinearSolver::fixPoint(operatorTensor& A, CPTensor& F, CPTensor& sol_0, CPTensor& sol, double tol){
  // init the resolutions
  unsigned int nVar = F.nVar();
  vector<unsigned int> dofPerDim = F.nDof_var();
  MPI_Comm theComm = F.theComm();
  unsigned int numTerms_A = A.numTerms();

  // Decomposing A as a vector of operator tensors of rank 1
  vector<operatorTensor> opA(numTerms_A);
  for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
    vector<Mat> thisTerm = A.op(lTerm);
    operatorTensor thisOp(thisTerm, theComm);
    opA[lTerm] = thisOp;
  }

  // Initialising the solution fields:
  sol.set_nVar(nVar);
  sol.set_nDof_var(dofPerDim);
  sol.setTensorComm(theComm);
  sol.copyTensorFrom(sol_0);

  // Copy the right hand side:
  // Initialise residual R=F:
  CPTensor R;
  R.copyTensorFrom(F);
  CPTensor AsolUpdated = A.applyOperator(sol);
  R.sum(AsolUpdated, -1.0, true);
  AsolUpdated.clear();

  double normRes = sqrt(R.norm2CP());
  PetscPrintf(theComm, "Residual norm = %e \n", normRes);

  unsigned int it = 0;
  while(normRes > tol){

    vector<CPTensor> y(numTerms_A);
    // Computing the terms:
    for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
      CPTensor thisRes;
      thisRes.copyTensorFrom(F);
      for(unsigned int mTerm=0; mTerm<numTerms_A; mTerm++){
        if(mTerm != lTerm){
          CPTensor toAdd;
          opA[mTerm].applyOperator(sol, toAdd);
          thisRes.sum(toAdd, -1.0, false);
        }
      }
      // solving the system for the rank one operator A_l
      vector<vector<Vec> > this_terms(thisRes.rank());
      vector<double> this_coeff(thisRes.rank());
      for(unsigned int iTerm=0; iTerm<thisRes.rank(); iTerm++){
        this_terms[iTerm].resize(F.nVar());
      }

      for(unsigned int iVar=0; iVar<F.nVar(); iVar++){
        Mat matP = A.op(lTerm,iVar);
        KSP ksp;
        KSPCreate(PETSC_COMM_WORLD,&ksp);
        KSPSetOperators(ksp, matP, matP); // => use A to build preconditioner
        KSPSetType(ksp, KSPGMRES);
        KSPSetFromOptions(ksp);
        for(unsigned int iTerm=0; iTerm<thisRes.rank(); iTerm++){
          Vec thisSol;
          a.initVec(thisSol,dofPerDim[iVar],PETSC_COMM_WORLD);
          a.finalizeVec(thisSol);
          Vec rhsF = thisRes.terms(iTerm,iVar);
          // solving the system:
          KSPSolve(ksp, rhsF, thisSol);
          this_terms[iTerm][iVar] = thisSol;
        }
        KSPDestroy(&ksp);
      }
      for(unsigned int iTerm=0; iTerm<thisRes.rank(); iTerm++){
        this_coeff[iTerm] = thisRes.coeffs(iTerm);
      }
      CPTensor this_y(this_terms, this_coeff, theComm);
      y[lTerm] = this_y;
    }

    // Exact line search stage.
    PetscPrintf(theComm, "Exact line search...");
    if(it==0){
      vector<CPTensor> z(numTerms_A);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        CPTensor z_entry = A.applyOperator(y[lTerm]);
        z[lTerm] = z_entry;
      }

      Mat lineSearchMat;
      a.initMat(lineSearchMat, numTerms_A, numTerms_A, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        for(unsigned int mTerm=0; mTerm<=lTerm; mTerm++){
          double matrixEntry = z[lTerm].scalarProd(z[mTerm]);
          a.setMatEl(lineSearchMat, lTerm, mTerm, matrixEntry, theComm);
          if(lTerm != mTerm){
            a.setMatEl(lineSearchMat, mTerm, lTerm, matrixEntry, theComm);
          }
        }
      }
      a.finalizeMat(lineSearchMat);

      Vec lineSearchVec;
      a.initVec(lineSearchVec, numTerms_A, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double vecEntry = z[lTerm].scalarProd(R);
        a.setVecEl(lineSearchVec, lTerm, vecEntry, theComm);
      }
      a.finalizeVec(lineSearchVec);

      // solving the linear system:
      Vec thisSol;
      a.initVec(thisSol, numTerms_A, theComm);
      a.finalizeVec(thisSol);

      KSP ksp;
      KSPCreate(PETSC_COMM_WORLD,&ksp);
      KSPSetOperators(ksp, lineSearchMat, lineSearchMat); // => use A to build preconditioner
      KSPSetType(ksp, KSPGMRES);
      KSPSetFromOptions(ksp);
      KSPSolve(ksp, lineSearchVec, thisSol);
      //VecView(thisSol, PETSC_VIEWER_STDOUT_WORLD);

      KSPDestroy(&ksp);
      MatDestroy(&lineSearchMat);
      VecDestroy(&lineSearchVec);
      PetscPrintf(theComm, "done.\n");

      // updating the solution and the residual:
      CPTensor update(F.nVar(), dofPerDim, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double coeff = a.getVecEl(thisSol, lTerm, theComm);
        update.sum(y[lTerm], coeff, true);
        R.sum(z[lTerm], -coeff, true);
      }

      sol.sum(update, 1.0, true);
      update.clear();
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        y[lTerm].clear();
        z[lTerm].clear();
      }
    }
    else{
      vector<CPTensor> z(numTerms_A+1);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        CPTensor z_entry = A.applyOperator(y[lTerm]);
        z[lTerm] = z_entry;
      }
      CPTensor AsolUpdated = A.applyOperator(sol);
      z[numTerms_A] = AsolUpdated;

      Mat lineSearchMat;
      a.initMat(lineSearchMat, numTerms_A+1, numTerms_A+1, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        for(unsigned int mTerm=0; mTerm<=lTerm; mTerm++){
          double matrixEntry = z[lTerm].scalarProd(z[mTerm]);
          a.setMatEl(lineSearchMat, lTerm, mTerm, matrixEntry, theComm);
          if(lTerm != mTerm){
            a.setMatEl(lineSearchMat, mTerm, lTerm, matrixEntry, theComm);
          }
        }
      }
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double matrixEntry = z[lTerm].scalarProd(z[numTerms_A]);
        a.setMatEl(lineSearchMat, lTerm, numTerms_A, matrixEntry, theComm);
        a.setMatEl(lineSearchMat, numTerms_A, lTerm, matrixEntry, theComm);
      }
      double matrixEntry = z[numTerms_A].scalarProd(z[numTerms_A]);
      a.setMatEl(lineSearchMat, numTerms_A, numTerms_A, matrixEntry, theComm);
      a.finalizeMat(lineSearchMat);

      Vec lineSearchVec;
      a.initVec(lineSearchVec, numTerms_A+1, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double vecEntry = z[lTerm].scalarProd(F); // bacause the current sol is included in the optimisation
        a.setVecEl(lineSearchVec, lTerm, vecEntry, theComm);
      }
      double vecEntry = z[numTerms_A].scalarProd(F); // bacause the current sol is included in the optimisation
      a.setVecEl(lineSearchVec, numTerms_A, vecEntry, theComm);
      a.finalizeVec(lineSearchVec);

      // solving the linear system:
      Vec thisSol;
      a.initVec(thisSol, numTerms_A+1, theComm);
      a.finalizeVec(thisSol);

      KSP ksp;
      KSPCreate(PETSC_COMM_WORLD,&ksp);
      KSPSetOperators(ksp, lineSearchMat, lineSearchMat); // => use A to build preconditioner
      KSPSetType(ksp, KSPGMRES);
      KSPSetFromOptions(ksp);
      KSPSolve(ksp, lineSearchVec, thisSol);
      //VecView(thisSol, PETSC_VIEWER_STDOUT_WORLD);

      KSPDestroy(&ksp);
      MatDestroy(&lineSearchMat);
      VecDestroy(&lineSearchVec);
      PetscPrintf(theComm, "done.\n");

      // updating the solution and the residual:
      CPTensor update(F.nVar(), dofPerDim, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double coeff = a.getVecEl(thisSol, lTerm, theComm);
        update.sum(y[lTerm], coeff, true);
      }
      double rescaleCoeff = a.getVecEl(thisSol, numTerms_A, theComm);
      sol.multiplyByScalar(rescaleCoeff);
      sol.sum(update, 1.0, true);
      update.clear();
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        y[lTerm].clear();
        z[lTerm].clear();
      }
      z[numTerms_A].clear();
      R.clear();
      R.copyTensorFrom(F);
      CPTensor AtimesSol = A.applyOperator(sol);
      R.sum(AtimesSol, -1.0, true);
      AtimesSol.clear();
    }

    if(it%10==0){
      string solname = "Sol";
      sol.save(solname);
    }

    normRes = sqrt(R.norm2CP());
    PetscPrintf(theComm, "Solution rank = %d \n", sol.rank());
    PetscPrintf(theComm, "Residual norm = %e \n", normRes);
    it += 1;
  }
  // end of function
}



/* Solve CPTT-fix point
  - compress the rhs, then fix point
*/
void multilinearSolver::solve_CPTT_fixPoint(operatorTensor& A, CPTensor& F, CPTensor& sol, double tol){
  // init the resolutions
  unsigned int nVar = F.nVar();
  vector<unsigned int> dofPerDim = F.nDof_var();
  MPI_Comm theComm = F.theComm();
  unsigned int numTerms_A = A.numTerms();

  // Decomposing A as a vector of operator tensors of rank 1
  vector<operatorTensor> opA(numTerms_A);
  for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
    vector<Mat> thisTerm = A.op(lTerm);
    operatorTensor thisOp(thisTerm, theComm);
    opA[lTerm] = thisOp;
  }

  // Initialising the solution fields:
  sol.set_nVar(nVar);
  sol.set_nDof_var(dofPerDim);
  sol.setTensorComm(theComm);

  // Copy the right hand side:
  // Initialise residual R=F:
  CPTensor R;
  R.copyTensorFrom(F);

  double normRes = sqrt(R.norm2CP());
  PetscPrintf(theComm, "Residual norm = %e \n", normRes);

  unsigned int it = 0;
  while(normRes > tol){
    // First step: compress R and retrieve one CPTT term
    CPTensor compF;
    compF.CPTT_ApproxOf(R,normRes*0.80);
    PetscPrintf(F.theComm(), "Rank of compF = %d \n", compF.rank());
    double normCompF = sqrt(compF.norm2CP());
    // Second step: compute the Bi-CGStab approximation.
    PetscPrintf(F.theComm(), "Entering Self-preconditioned step...\n");
    unsigned int dim = compF.nVar();
    vector<unsigned int> resPerDim = compF.nDof_var();
    CPTensor thisFPSol(dim, resPerDim, compF.theComm());
    fixPoint(A, compF, thisFPSol, 0.5*normCompF);

    sol.sum(thisFPSol, 1.0, true);
    thisFPSol.clear();
    double normSol = sqrt(sol.norm2CP());
    // Compress the solution and update the residual:
    CPTensor compressedSol;
    compressedSol.CPTT_ApproxOf(sol, 5e-2*normSol);
    sol.clear();
    sol.copyTensorFrom(compressedSol);
    compressedSol.clear();

    R.clear();
    R.copyTensorFrom(F);
    CPTensor AsolUpdated = A.applyOperator(sol);
    R.sum(AsolUpdated, -1.0, true);
    AsolUpdated.clear();
    normRes = sqrt(R.norm2CP());

    PetscPrintf(theComm, "Solution rank at the end of the iteration = %d \n", sol.rank());
    PetscPrintf(theComm, "Residual norm at the end of the iteration = %e \n", normRes);

  }

}


/* Solve fix point compress
  - fix point and compress the solution
*/
void multilinearSolver::fixPoint_compress(operatorTensor& A, CPTensor& F, CPTensor& sol, double tol){
  // init the resolutions
  unsigned int nVar = F.nVar();
  vector<unsigned int> dofPerDim = F.nDof_var();
  MPI_Comm theComm = F.theComm();
  unsigned int numTerms_A = A.numTerms();

  // Decomposing A as a vector of operator tensors of rank 1
  vector<operatorTensor> opA(numTerms_A);
  for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
    vector<Mat> thisTerm = A.op(lTerm);
    operatorTensor thisOp(thisTerm, theComm);
    opA[lTerm] = thisOp;
  }

  // Initialising the solution fields:
  sol.set_nVar(nVar);
  sol.set_nDof_var(dofPerDim);
  sol.setTensorComm(theComm);

  // Copy the right hand side:
  // Initialise residual R=F:
  CPTensor R;
  R.copyTensorFrom(F);

  double normRes = sqrt(R.norm2CP());
  PetscPrintf(theComm, "Residual norm = %e \n", normRes);
  const double normRes_0 = normRes;
  unsigned int it = 0;
  while(normRes > tol){
    vector<CPTensor> y(numTerms_A);
    // Computing the terms:
    for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
      CPTensor thisRes;
      thisRes.copyTensorFrom(F);
      for(unsigned int mTerm=0; mTerm<numTerms_A; mTerm++){
        if(mTerm != lTerm){
          CPTensor toAdd;
          opA[mTerm].applyOperator(sol, toAdd);
          thisRes.sum(toAdd, -1.0, false);
        }
      }
      // solving the system for the rank one operator A_l
      vector<vector<Vec> > this_terms(thisRes.rank());
      vector<double> this_coeff(thisRes.rank());
      for(unsigned int iTerm=0; iTerm<thisRes.rank(); iTerm++){
        this_terms[iTerm].resize(F.nVar());
      }

      for(unsigned int iVar=0; iVar<F.nVar(); iVar++){
        Mat matP = A.op(lTerm,iVar);
        KSP ksp;
        KSPCreate(PETSC_COMM_WORLD,&ksp);
        KSPSetOperators(ksp, matP, matP); // => use A to build preconditioner
        KSPSetType(ksp, KSPGMRES);
        KSPSetFromOptions(ksp);
        for(unsigned int iTerm=0; iTerm<thisRes.rank(); iTerm++){
          Vec thisSol;
          a.initVec(thisSol,dofPerDim[iVar],PETSC_COMM_WORLD);
          a.finalizeVec(thisSol);
          Vec rhsF = thisRes.terms(iTerm,iVar);
          // solving the system:
          KSPSolve(ksp, rhsF, thisSol);
          this_terms[iTerm][iVar] = thisSol;
        }
        KSPDestroy(&ksp);
      }
      for(unsigned int iTerm=0; iTerm<thisRes.rank(); iTerm++){
        this_coeff[iTerm] = thisRes.coeffs(iTerm);
      }
      CPTensor this_y(this_terms, this_coeff, theComm);
      y[lTerm] = this_y;
    }

    // Exact line search stage.
    PetscPrintf(theComm, "Exact line search...");
    if(it==0){
      vector<CPTensor> z(numTerms_A);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        CPTensor z_entry = A.applyOperator(y[lTerm]);
        z[lTerm] = z_entry;
      }

      Mat lineSearchMat;
      a.initMat(lineSearchMat, numTerms_A, numTerms_A, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        for(unsigned int mTerm=0; mTerm<=lTerm; mTerm++){
          double matrixEntry = z[lTerm].scalarProd(z[mTerm]);
          a.setMatEl(lineSearchMat, lTerm, mTerm, matrixEntry, theComm);
          if(lTerm != mTerm){
            a.setMatEl(lineSearchMat, mTerm, lTerm, matrixEntry, theComm);
          }
        }
      }
      a.finalizeMat(lineSearchMat);

      Vec lineSearchVec;
      a.initVec(lineSearchVec, numTerms_A, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double vecEntry = z[lTerm].scalarProd(R);
        a.setVecEl(lineSearchVec, lTerm, vecEntry, theComm);
      }
      a.finalizeVec(lineSearchVec);

      // solving the linear system:
      Vec thisSol;
      a.initVec(thisSol, numTerms_A, theComm);
      a.finalizeVec(thisSol);

      KSP ksp;
      KSPCreate(PETSC_COMM_WORLD,&ksp);
      KSPSetOperators(ksp, lineSearchMat, lineSearchMat); // => use A to build preconditioner
      KSPSetType(ksp, KSPGMRES);
      KSPSetFromOptions(ksp);
      KSPSolve(ksp, lineSearchVec, thisSol);
      //VecView(thisSol, PETSC_VIEWER_STDOUT_WORLD);

      KSPDestroy(&ksp);
      MatDestroy(&lineSearchMat);
      VecDestroy(&lineSearchVec);
      PetscPrintf(theComm, "done.\n");

      // updating the solution and the residual:
      CPTensor update(F.nVar(), dofPerDim, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double coeff = a.getVecEl(thisSol, lTerm, theComm);
        update.sum(y[lTerm], coeff, true);
        R.sum(z[lTerm], -coeff, true);
      }

      sol.sum(update, 1.0, true);
      update.clear();
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        y[lTerm].clear();
        z[lTerm].clear();
      }
    }
    else{
      vector<CPTensor> z(numTerms_A+1);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        CPTensor z_entry = A.applyOperator(y[lTerm]);
        z[lTerm] = z_entry;
      }
      CPTensor AsolUpdated = A.applyOperator(sol);
      z[numTerms_A] = AsolUpdated;

      Mat lineSearchMat;
      a.initMat(lineSearchMat, numTerms_A+1, numTerms_A+1, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        for(unsigned int mTerm=0; mTerm<=lTerm; mTerm++){
          double matrixEntry = z[lTerm].scalarProd(z[mTerm]);
          a.setMatEl(lineSearchMat, lTerm, mTerm, matrixEntry, theComm);
          if(lTerm != mTerm){
            a.setMatEl(lineSearchMat, mTerm, lTerm, matrixEntry, theComm);
          }
        }
      }
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double matrixEntry = z[lTerm].scalarProd(z[numTerms_A]);
        a.setMatEl(lineSearchMat, lTerm, numTerms_A, matrixEntry, theComm);
        a.setMatEl(lineSearchMat, numTerms_A, lTerm, matrixEntry, theComm);
      }
      double matrixEntry = z[numTerms_A].scalarProd(z[numTerms_A]);
      a.setMatEl(lineSearchMat, numTerms_A, numTerms_A, matrixEntry, theComm);
      a.finalizeMat(lineSearchMat);

      Vec lineSearchVec;
      a.initVec(lineSearchVec, numTerms_A+1, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double vecEntry = z[lTerm].scalarProd(F); // bacause the current sol is included in the optimisation
        a.setVecEl(lineSearchVec, lTerm, vecEntry, theComm);
      }
      double vecEntry = z[numTerms_A].scalarProd(F); // bacause the current sol is included in the optimisation
      a.setVecEl(lineSearchVec, numTerms_A, vecEntry, theComm);
      a.finalizeVec(lineSearchVec);

      // solving the linear system:
      Vec thisSol;
      a.initVec(thisSol, numTerms_A+1, theComm);
      a.finalizeVec(thisSol);

      KSP ksp;
      KSPCreate(PETSC_COMM_WORLD,&ksp);
      KSPSetOperators(ksp, lineSearchMat, lineSearchMat); // => use A to build preconditioner
      KSPSetType(ksp, KSPGMRES);
      KSPSetFromOptions(ksp);
      KSPSolve(ksp, lineSearchVec, thisSol);
      //VecView(thisSol, PETSC_VIEWER_STDOUT_WORLD);

      KSPDestroy(&ksp);
      MatDestroy(&lineSearchMat);
      VecDestroy(&lineSearchVec);
      PetscPrintf(theComm, "done.\n");

      // updating the solution and the residual:
      CPTensor update(F.nVar(), dofPerDim, theComm);
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        double coeff = a.getVecEl(thisSol, lTerm, theComm);
        update.sum(y[lTerm], coeff, true);
      }
      double rescaleCoeff = a.getVecEl(thisSol, numTerms_A, theComm);
      sol.multiplyByScalar(rescaleCoeff);
      sol.sum(update, 1.0, true);
      update.clear();
      for(unsigned int lTerm=0; lTerm<numTerms_A; lTerm++){
        y[lTerm].clear();
        z[lTerm].clear();
      }
      z[numTerms_A].clear();
      R.clear();
      R.copyTensorFrom(F);
      CPTensor AtimesSol = A.applyOperator(sol);
      R.sum(AtimesSol, -1.0, true);
      AtimesSol.clear();
    }

    normRes = sqrt(R.norm2CP());

    if(it%10==0){
      string solname = "Sol";
      sol.save(solname);
    }
    double normSol = sqrt(sol.norm2CP());
    PetscPrintf(theComm, "Solution rank before compression = %d \n", sol.rank());
    PetscPrintf(theComm, "Residual norm before compression = %e \n", normRes);

    CPTensor compressSol(sol.nVar(), sol.nDof_var(), sol.theComm());
    double ratioRes = normRes/normRes_0;
    double tolCompression = 0.05*ratioRes*normSol;
    compressSol.CPTT_ApproxOf(sol, tolCompression);
    sol.clear();
    sol.copyTensorFrom(compressSol);
    compressSol.clear();

    R.clear();
    R.copyTensorFrom(F);
    CPTensor AsolUpdated = A.applyOperator(sol);
    R.sum(AsolUpdated, -1.0, true);
    AsolUpdated.clear();

    normRes = sqrt(R.norm2CP());
    PetscPrintf(theComm, "Solution rank at the end of iteration = %d \n", sol.rank());
    PetscPrintf(theComm, "Residual norm at the end of iteration = %e \n\n", normRes);
    it += 1;
  }
  // end of function

}
