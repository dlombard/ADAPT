// Implementation of full Tensor
#include "fullTensor.h"
using namespace std;


// 1 -- Overloaded Constructors and Initialisers --

/* input:
   - tensor dimension
   - number of degrees of freedom per dimension
   - communicator
   output:
   - build and init all the structures for data manipulation.
*/

fullTensor::fullTensor(unsigned int dim, vector<unsigned int>& nDofPerDir, MPI_Comm theComm){
  // assigning the tensor dimension
  m_theComm = theComm;
  m_nVar = dim;
  m_nEntries = 1;
  m_nDof_var.resize(m_nVar);
  m_inc.resize(m_nVar);
  for(unsigned int idVar = 0; idVar < m_nVar; idVar++){
    m_nDof_var[idVar] = nDofPerDir[idVar];
    m_inc[idVar] = 1;
    for (unsigned int n=0; n<idVar; n++){
        m_inc[idVar] = m_inc[idVar] * m_nDof_var[n];
    };
    m_nEntries = m_nEntries * m_nDof_var[idVar];
  }
  a.initVec(m_tensorEntries, m_nEntries, m_theComm);
  m_isInitialised = true;
}


// Init the fields once the nVar and nDof_var are set:
void fullTensor::init(){
  m_inc.resize(m_nVar);
  m_nEntries = 1;
  for(unsigned int idVar = 0; idVar < m_nVar; idVar++){
    m_inc[idVar] = 1;
    for (unsigned int n=0; n<idVar; n++){
        m_inc[idVar] = m_inc[idVar] * m_nDof_var[n];
    };
    m_nEntries = m_nEntries * m_nDof_var[idVar];
  }
  a.initVec(m_tensorEntries, m_nEntries, m_theComm);
  m_isInitialised = true;
}


// Print the tensor structure:
void fullTensor::printTensorStruct(){
		PetscPrintf(m_theComm,"Tensor dimension: %d \n", m_nVar);
    PetscPrintf(m_theComm,"n Dof per dimension:\n");
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      PetscPrintf(m_theComm,"dof for var_%d  = %d \n", idVar, m_nDof_var[idVar]);
    }
}



// 2 -- COPY Structures and tensor data --

/* Copy the tensor structure from a given fullTensor:
  - input: the tensor from which the structure is copied
  - output: the structure of this tensor is identical
*/
void fullTensor::copyTensorStructFrom(fullTensor& source){
  m_theComm = source.theComm();
  m_nVar = source.nVar();
  m_nDof_var = source.nDof_var();

  m_nEntries = 1;
  m_inc.resize(m_nVar);
  for(unsigned int idVar = 0; idVar < m_nVar; idVar++){
    m_inc[idVar] = 1;
    for (unsigned int n=0; n<idVar; n++){
        m_inc[idVar] = m_inc[idVar] * m_nDof_var[n];
    };
    m_nEntries = m_nEntries * m_nDof_var[idVar];
  }
  a.initVec(m_tensorEntries, m_nEntries, m_theComm);
  m_isInitialised = true;
}


/* Copy the tensor structure to a given fullTensor:
  - input: the tensor to which the structure has to be copied
  - output: the structure of the target tensor
*/
void fullTensor::copyTensorStructTo(fullTensor& target){
  target.setTensorComm(m_theComm);
  target.set_nVar(m_nVar);
  target.set_nDof_var(m_nDof_var);
  target.init();
  target.setIsInitialised(true);
}


/* Copy the tensor from a given fullTensor:
  - input: the tensor to be copied
  - output: the tensor (structure + copy of the entries)
*/
void fullTensor::copyTensorFrom(fullTensor& source){
  copyTensorStructFrom(source);
  VecCopy(source.tensorEntries(),m_tensorEntries);
  finalizeTensor();
}


/* Copy the tensor into a given fullTensor:
  - input: this tensor
  - output: the copy of this tensor (structure + copy of the entries)
*/

void fullTensor::copyTensorTo(fullTensor& target){
  copyTensorStructTo(target);
  Vec theEntries;
  a.initVec(theEntries,m_tensorEntries);
  VecCopy(m_tensorEntries, theEntries);
  a.finalizeVec(theEntries);
  target.setTensorEntries(theEntries);
  finalizeTensor();
}


// 3 -- OPERATIONS --

/* check if this tensor has the same structure of a given full tensor
  - input: the tensor to compare with
  - output: true if if has the same structure, false otherwise
*/

bool fullTensor::hasSameStructure(fullTensor& toBeComparedTo){
    bool toBeReturned = true;
    if(m_nVar != toBeComparedTo.nVar()){
      return false;
    }
    else{
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        if(m_nDof_var[idVar] != toBeComparedTo.nDof_var(idVar)){
          return false;
        }
      }
    }
    return toBeReturned;
}


/* check if this tensor is equal a given full tensor
  - input: the tensor to compare with
  - output: true if it is equal, false otherwise
*/
bool fullTensor::isEqualTo(fullTensor& toBeComparedTo){
    bool toBeReturned = hasSameStructure(toBeComparedTo);
    if(toBeReturned){
      for(unsigned int l=0; l<m_nEntries; l++){
        double thisVal = a.getVecEl(m_tensorEntries, l, m_theComm);
        if(toBeComparedTo.tensorEntries(l) != thisVal){
          toBeReturned = false;
          break;
        }
      }
    }
    return toBeReturned;
}


/* sum a given tensor times alpha to the current tensor
  - input: the tensor to be summed
  - output: the present tensor T <- T + alpha S
*/

void fullTensor::sum(fullTensor& toBeAdded, double alpha){
  if(hasSameStructure(toBeAdded)){
    VecAXPY(m_tensorEntries, alpha, toBeAdded.tensorEntries());
  }
  else{
    PetscPrintf(m_theComm, "Tensor cannot be summed!\n");
  }
}


/* multiply this tensor by a scalar
  - input: the scalar
  - output: the present tensor T <- alpha T
*/
void fullTensor::multiplyByScalar(double alpha){
  VecScale(m_tensorEntries, alpha);
}


/* shift this tensor entries by a scalar
  - input: the scalar
  - output: the present tensor T <- alpha + T
*/
void fullTensor::shiftByScalar(double alpha){
  VecShift(m_tensorEntries, alpha);
}


/* Extract a subtensor and put it into a given full tensor
  - input: the indices bounds (C notation [lower_1,upper_1, lower2, upper_2,...]) and an empty full tensor
  - output: the subtensor
*/
void fullTensor::extractSubtensor(vector<unsigned int>& indicesBounds, fullTensor& target){
  assert(indicesBounds.size()==2*m_nVar);
  target.setTensorComm(m_theComm);
  target.set_nVar(m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  unsigned int N = 1;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    resPerDim[idVar] = indicesBounds[2*idVar+1] - indicesBounds[2*idVar];
    N = N * resPerDim[idVar];
  }
  target.set_nDof_var(resPerDim);
  target.init();
  for(unsigned int l=0; l<N; l++){
      vector<unsigned int> trgLocalInd = target.lin2sub(l);
      vector<unsigned int> thisIndices(m_nVar);
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        thisIndices[idVar] = trgLocalInd[idVar] + indicesBounds[2*idVar];
      }
      unsigned int linInd = sub2lin(thisIndices);
      double value = a.getVecEl(m_tensorEntries, linInd, m_theComm);
      target.setTensorElement(trgLocalInd,value);
  }
  target.finalizeTensor();
}


/* Put a given subtensor into this full tensor, at specified positions
  - input: the indices bounds (C notation [lower_1,upper_1, lower2, upper_2,...]) and the full subtensor
  - output: the subtensor is put into the current tensor
*/
void fullTensor::assignSubtensor(vector<unsigned int>& indicesBounds, fullTensor& source){
  assert(indicesBounds.size()==2*m_nVar);
  assert(source.theComm()==m_theComm);
  assert(source.nVar()==m_nVar);
  unsigned int N  = 1;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    assert(indicesBounds[2*idVar+1]-indicesBounds[2*idVar]<m_nDof_var[idVar]);
    N = N* (indicesBounds[2*idVar+1]-indicesBounds[2*idVar]);
  }
  for(unsigned int l=0; l<N; l++){
    vector<unsigned int> sourceIndices = source.lin2sub(l);
    vector<unsigned int> thisIndices(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      thisIndices[idVar] = sourceIndices[idVar] + indicesBounds[2*idVar];
    }
    unsigned int linInd = sub2lin(thisIndices);
    double value = source.tensorEntries(l);
    a.setVecEl(m_tensorEntries, linInd, value, m_theComm);
  }
  finalizeTensor();
}


/* reshape the current tensor into a new one (pointer given)
  - input: the set of new resolution per variables, COPY flag (to actually copy the entries)
  - output: result is the tensor reshaped.
  REMARK: could be use to have unfoldings as fullTensors ( without copy ) => fast
*/
void fullTensor::reshape(vector<unsigned int>& nDofPerVar, fullTensor& result, bool COPY = true){
  assert(nDofPerVar.size()==m_nVar);
  unsigned int nDof = nDofPerVar[0];
  for(unsigned int idVar=1; idVar<m_nVar; idVar++){
    nDof = nDof * nDofPerVar[idVar];
  }
  assert(nDof == m_nEntries);
  result.setTensorComm(m_theComm);
  result.set_nVar(m_nVar);
  result.set_nDof_var(nDofPerVar);
  result.init();
  if(!COPY){
    result.setTensorEntries(m_tensorEntries); // without copy
  }
  else{
    Vec theEntries;
    a.initVec(theEntries,m_tensorEntries);
    VecCopy(m_tensorEntries, theEntries);
    a.finalizeVec(theEntries);
    result.setTensorEntries(theEntries);
  }
  result.finalizeTensor();
}


/* Left Kronecker product of tensors
  - input: tensors A and B
  - output: this tensor (empty) is the Kronecker product of the tensors
*/
void fullTensor::leftKronecker(fullTensor& A, fullTensor& B){
  assert(A.nVar()==B.nVar());
  assert(A.theComm()==B.theComm());
  m_theComm = A.theComm();
  m_nVar = A.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    m_nDof_var[idVar] = A.nDof_var(idVar) * B.nDof_var(idVar);
  }
  init();
  for(unsigned int lB=0; lB<B.nEntries(); lB++){
    vector<unsigned int> indicesB = B.lin2sub(lB);
    double valB = B.tensorEntries(lB);
    for(unsigned int lA=0; lA<A.nEntries(); lA++){
      vector<unsigned int> indicesA = A.lin2sub(lA);
      double valA = A.tensorEntries(lA);
      vector<unsigned int> ind(m_nVar);
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        ind[idVar] = indicesB[idVar]*A.nDof_var(idVar) + indicesA[idVar];
      }
      unsigned int lin = sub2lin(ind);
      double val = valA*valB;
      a.setVecEl(m_tensorEntries,lin,val,m_theComm);
    }
  }
  finalizeTensor();
}


/* Right Kronecker product of tensors
  - input: tensors A and B
  - output: this tensor (empty) is the right Kronecker product of the tensors
*/
void fullTensor::rightKronecker(fullTensor& A, fullTensor& B){
  assert(A.nVar()==B.nVar());
  assert(A.theComm()==B.theComm());
  m_theComm = A.theComm();
  m_nVar = A.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    m_nDof_var[idVar] = A.nDof_var(idVar) * B.nDof_var(idVar);
  }
  init();
  for(unsigned int lB=0; lB<B.nEntries(); lB++){
    vector<unsigned int> indicesB = B.lin2sub(lB);
    double valB = B.tensorEntries(lB);
    for(unsigned int lA=0; lA<A.nEntries(); lA++){
      vector<unsigned int> indicesA = A.lin2sub(lA);
      double valA = A.tensorEntries(lA);
      vector<unsigned int> ind(m_nVar);
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        ind[idVar] = indicesA[idVar]*B.nDof_var(idVar) + indicesB[idVar];
      }
      unsigned int lin = sub2lin(ind);
      double val = valA*valB;
      a.setVecEl(m_tensorEntries,lin,val,m_theComm);
    }
  }
  finalizeTensor();
}


/* Implicit left Kronecker product of the present tensor with a given fullTensor.
  - input: the full tensor B, the indices of this tensor and B
  - output: the indices and the value of the Kronecker product
*/
void fullTensor::implicitLeftKronecker(fullTensor& B, vector<unsigned int>& indThis, vector<unsigned int>& indB, vector<unsigned int>& ind, double& val){
  assert(m_nVar==B.nVar());
  assert(m_theComm==B.theComm());
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    ind[idVar] = indB[idVar]*m_nDof_var[idVar] + indThis[idVar];
  }
  unsigned int lThis = sub2lin(indThis);
  double valThis = a.getVecEl(m_tensorEntries, lThis, m_theComm);
  unsigned int lB = B.sub2lin(indB);
  double valB = B.tensorEntries(lB);
  val = valThis * valB;
}


/* Implicit right Kronecker product of the present tensor with a given fullTensor.
  - input: the full tensor B, the indices of this tensor and B
  - output: the indices and the value of the right Kronecker product
*/
void fullTensor::implicitRightKronecker(fullTensor& B, vector<unsigned int>& indThis, vector<unsigned int>& indB, vector<unsigned int>& ind, double& val){
  assert(m_nVar==B.nVar());
  assert(m_theComm==B.theComm());
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    ind[idVar] = indThis[idVar]*B.nDof_var(idVar) + indB[idVar];
  }
  unsigned int lThis = sub2lin(indThis);
  double valThis = a.getVecEl(m_tensorEntries, lThis, m_theComm);
  unsigned int lB = B.sub2lin(indB);
  double valB = B.tensorEntries(lB);
  val = valThis * valB;
}


/* mode I Khatri-Rao product of two given full Tensors.
  - input: the full tensors A and B, the mode I
  - output: this (empty) tensor becomes the mode I Khatri-Rao product of A and B
*/
void fullTensor::modeIKhatriRao(unsigned int modeI, fullTensor& A, fullTensor& B){
  assert(A.nVar()==B.nVar());
  assert(A.theComm()==B.theComm());
  assert(A.nDof_var(modeI) == B.nDof_var(modeI));
  assert(modeI < A.nVar());

  m_theComm = A.theComm();
  m_nVar = A.nVar();
  m_nDof_var.resize(m_nVar);

  vector<unsigned int>indicesBoundsA(2*m_nVar);
  vector<unsigned int>indicesBoundsB(2*m_nVar);
  vector<unsigned int>indicesBoundsThis(2*m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar != modeI){
      m_nDof_var[idVar] = A.nDof_var(idVar) * B.nDof_var(idVar);
      indicesBoundsA[2*idVar] = 0;
      indicesBoundsA[2*idVar+1] = A.nDof_var(idVar);
      indicesBoundsB[2*idVar] = 0;
      indicesBoundsB[2*idVar+1] = B.nDof_var(idVar);
      indicesBoundsThis[2*idVar] = 0;
      indicesBoundsThis[2*idVar+1] = m_nDof_var[idVar];
    }
    else{
      m_nDof_var[idVar] = A.nDof_var(modeI);
    }
  }
  init();

  for(unsigned int idDof=0; idDof<m_nDof_var[modeI]; idDof++){
    fullTensor subA;
    indicesBoundsA[2*modeI] = idDof;
    indicesBoundsA[2*modeI+1] = idDof+1;
    A.extractSubtensor(indicesBoundsA,subA);
    fullTensor subB;
    indicesBoundsB[2*modeI] = idDof;
    indicesBoundsB[2*modeI+1] = idDof+1;
    B.extractSubtensor(indicesBoundsB,subB);

    fullTensor subKronProd;
    subKronProd.rightKronecker(subA,subB);
    indicesBoundsThis[2*modeI] = idDof;
    indicesBoundsThis[2*modeI+1] = idDof+1;
    assignSubtensor(indicesBoundsThis,subKronProd);
  }
  finalizeTensor();
}


/* mode I concatenation of two full order tensors
  - input: tensor A and tensor B in full tensor format
  - output: this tensor is the concatenation
*/
void fullTensor::modeIConcatenate(unsigned int modeI, fullTensor& A, fullTensor& B){
  assert(A.nVar()==B.nVar());
  assert(A.theComm()==B.theComm());
  assert(modeI < A.nVar());
  for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
    if(idVar!=modeI){
      assert(A.nDof_var(idVar)==B.nDof_var(idVar));
    }
  }
  m_theComm = A.theComm();
  m_nVar = A.nVar();
  m_nDof_var.resize(m_nVar);
  vector<unsigned int>indicesBoundsA(2*m_nVar);
  vector<unsigned int>indicesBoundsB(2*m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar != modeI){
      m_nDof_var[idVar] = A.nDof_var(idVar);
      indicesBoundsA[2*idVar] = 0;
      indicesBoundsA[2*idVar+1] = A.nDof_var(idVar);
      indicesBoundsB[2*idVar] = 0;
      indicesBoundsB[2*idVar+1] = B.nDof_var(idVar);
    }
    else{
      m_nDof_var[idVar] = A.nDof_var(idVar) + B.nDof_var(idVar);
      indicesBoundsA[2*idVar] = 0;
      indicesBoundsA[2*idVar+1] = A.nDof_var(idVar);
      indicesBoundsB[2*idVar] = A.nDof_var(idVar);
      indicesBoundsB[2*idVar+1] = m_nDof_var[idVar];
    }
  }
  init();
  assignSubtensor(indicesBoundsA,A);
  assignSubtensor(indicesBoundsB,B);
  finalizeTensor();
}


/* mode I contraction of a tensor and a given vector
  - input: the vector b, an empty result tensor
  - output: this tensor contracted by b to provide result
*/
void fullTensor::modeIContraction(unsigned int modeI, Vec& b, fullTensor& result){
  PetscInt bSize;
  VecGetSize(b, &bSize);
  assert(bSize == m_nDof_var[modeI]);

  result.setTensorComm(m_theComm);
  result.set_nVar(m_nVar-1);
  vector<unsigned int> dofPerVar(m_nVar-1);
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar != modeI){
      dofPerVar[cc] = m_nDof_var[idVar];
      cc = cc + 1;
    }
  }
  result.set_nDof_var(dofPerVar);
  result.init();
  for(unsigned int l=0; l<result.nEntries(); l++){
    double value = 0.0;
    vector<unsigned int> indResult =  result.lin2sub(l);
    vector<unsigned int> thisInd(m_nVar);
    unsigned int count = 0;
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(idVar != modeI){
        thisInd[idVar] = indResult[count];
        count = count + 1;
      }
    }
    for(unsigned int idDof=0; idDof<m_nDof_var[modeI]; idDof++){
      thisInd[modeI] = idDof;
      unsigned int thisLin = sub2lin(thisInd);
      double tEnt = a.getVecEl(m_tensorEntries, thisLin, m_theComm);
      double bEnt = a.getVecEl(b, idDof, m_theComm);
      value = value + tEnt * bEnt;
    }
    result.setTensorElement(indResult, value);
  }
  result.finalizeTensor();
};


/* function that performs matrix tensor multiplication along mode I
 - inputs: the mode I, the matrix
 - outputs: the result (passed as empty fullTensor)
*/
void fullTensor::modeIMatMult(unsigned int modeI, Mat& A, fullTensor& result){
  PetscInt nRows,nCols;
  MatGetSize(A, &nRows, &nCols);
  assert(nCols == m_nDof_var[modeI]);
  Mat IthUnfold;
  computeUnfolding(modeI, IthUnfold);
  PetscInt nUnfRows, nUnfCols;
  MatGetSize(IthUnfold, &nUnfRows, &nUnfCols);
  Mat unfoldingResult;
  a.initMat(unfoldingResult, nRows, nUnfCols, m_theComm);
  a.finalizeMat(unfoldingResult);
  MatMatMult(A, IthUnfold, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &unfoldingResult);
  //fromUnfolding2FullTensor(unfoldingResult, result); !!! TO_BE_CODED !!!
}


/* outer Product of two given tensors A and B
  - input: the tensors A and B (full tensors)
  - output: this (empty) tensor is the outer product of the two
*/
void fullTensor::outerProduct(fullTensor& A, fullTensor&B){
  assert(A.theComm() == B.theComm());
  m_theComm = A.theComm();
  m_nVar = A.nVar() + B.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar<A.nVar()){
      m_nDof_var[idVar] = A.nDof_var(idVar);
    }
    else{
      unsigned int idB = idVar - A.nVar();
      m_nDof_var[idVar] = B.nDof_var(idB);
    }
  }
  init();
  for(unsigned int lA=0; lA<A.nEntries(); lA++){
    vector<unsigned int> indA = A.lin2sub(lA);
    double valA = A.tensorEntries(lA);
    vector<unsigned int> ind(m_nVar);
    for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
      ind[idVar] = indA[idVar];
    }
    for(unsigned int lB=0; lB<B.nEntries(); lB++){
      vector<unsigned int> indB = B.lin2sub(lB);
      double valB = B.tensorEntries(lB);
      double val = valA*valB;
      for(unsigned int idVar=0; idVar<B.nVar(); idVar++){
          unsigned int idThis = idVar + A.nVar();
          ind[idThis] = indB[idVar];
      }
      unsigned int thisLin = sub2lin(ind);
      a.setVecEl(m_tensorEntries,thisLin,val,m_theComm);
    }
  }
  finalizeTensor();
}


/* Compute unfolding and put it into a given matrix
  - input: the index along which the unfolding has to be computed
  - output: the unfolding into the Matrix
*/
void fullTensor::computeUnfolding(const unsigned int index, Mat& M){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
	a.initMat(M, nRows, nCols, m_theComm);

  for(unsigned int l=0; l<m_nEntries; l++){
    vector<unsigned int> ind = lin2sub(l);
    unsigned int iRow = ind[index];
    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(idVar != index){
        jCol = jCol + unfoldInc*ind[idVar];
        unfoldInc = unfoldInc * m_nDof_var[idVar];
      }
    }
    double value = a.getVecEl(m_tensorEntries, l, m_theComm);
    a.setMatEl(M,iRow,jCol, value, m_theComm);
  }
	a.finalizeMat(M);
}


/* Compute unfolding and put it into a set of vectors (get the fibers)
  - input: the index along which the unfolding has to be computed
  - output: the set of fibers
*/
void fullTensor::computeUnfolding(const unsigned int index, vector<Vec>& fibers){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nFibers = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nFibers = nFibers*m_nDof_var[idVar];
		}
	}

  fibers.resize(nFibers);
  for(unsigned int jFiber=0; jFiber<nFibers; jFiber++){
    a.initVec(fibers[jFiber], nRows, m_theComm);
  }

  for(unsigned int l=0; l<m_nEntries; l++){
    vector<unsigned int> ind = lin2sub(l);
    unsigned int iRow = ind[index];
    unsigned int jFiber = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(idVar != index){
        jFiber = jFiber + unfoldInc*ind[idVar];
        unfoldInc = unfoldInc * m_nDof_var[idVar];
      }
    }
    double value = a.getVecEl(m_tensorEntries, l, m_theComm);
    a.setVecEl(fibers[jFiber], iRow, value, m_theComm);
  }

  for(unsigned int jFiber=0; jFiber<nFibers; jFiber++){
    a.finalizeVec(fibers[jFiber]);
  }
}


/* Evaluate unfolding without assembling it!
  - input: the index along which the unfolding has to be computed, (I,J) of the unfolding
  - output: the value of the unfolding_[index] in I,J
*/
void fullTensor::evalUnfolding(const unsigned int index, const unsigned int I, const unsigned int J, double& value){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}

  vector<unsigned int> ind(m_nVar);
  ind[index] = I;
  //define the unfolding increments
  vector<unsigned int> unfoldInc(m_nVar-1);
  unsigned int unfInc = 1;
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar!=index){
      unfoldInc[cc] = unfInc;
      unfInc = unfInc * m_nDof_var[idVar];
      cc = cc + 1;
    }
  }
  // compute the tensor indices
  unsigned int reminder = J;
  cc = m_nVar-2;
  for (int h=m_nVar-1; h>=0; h--){
      if(h != index){
        ind[h] = reminder/unfoldInc[cc];
        reminder = reminder - ind[h] * unfoldInc[cc];
        cc = cc - 1;
      }
  };
  // get the element
  unsigned int linInd = sub2lin(ind);
  value = a.getVecEl(m_tensorEntries, linInd, m_theComm);
}


/* given an unfolding as a matrix, compute the full tensor
  - inputs: the matrix, the mode
  - outputs: this (empty) full tensor is the tensor whose unfolding is the matrix
*/
void fullTensor::fromUnfold2full(const unsigned int modeI, vector<unsigned int>& nDof, Mat& Unfold, MPI_Comm theComm){


}
