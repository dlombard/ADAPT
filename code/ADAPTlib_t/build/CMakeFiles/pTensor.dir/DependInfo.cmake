
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/Users/dlombard/Work/ADAPT/code/ADAPTlib_t/linAlg/linAlgOp_real.cpp" "CMakeFiles/pTensor.dir/linAlg/linAlgOp_real.o" "gcc" "CMakeFiles/pTensor.dir/linAlg/linAlgOp_real.o.d"
  "/Users/dlombard/Work/ADAPT/code/ADAPTlib_t/linAlg/vec_petsc.cpp" "CMakeFiles/pTensor.dir/linAlg/vec_petsc.o" "gcc" "CMakeFiles/pTensor.dir/linAlg/vec_petsc.o.d"
  "/Users/dlombard/Work/ADAPT/code/ADAPTlib_t/linAlg/vec_real.cpp" "CMakeFiles/pTensor.dir/linAlg/vec_real.o" "gcc" "CMakeFiles/pTensor.dir/linAlg/vec_real.o.d"
  "/Users/dlombard/Work/ADAPT/code/ADAPTlib_t/main.cpp" "CMakeFiles/pTensor.dir/main.o" "gcc" "CMakeFiles/pTensor.dir/main.o.d"
  )

# Targets to which this target links which contain Fortran sources.
set(CMAKE_Fortran_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
