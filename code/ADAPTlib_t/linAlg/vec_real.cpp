// Implementation of the class vec_real
#include "vec.h"

using namespace std;

/* I.1 - Constructors for real vectors:
  - input: the vector size
  - output: the vector is allocated, size is set
*/
vec_real::vec_real(unsigned int N){
  m_size = N;
  m_v = (double*) fftw_malloc(sizeof(double)*m_size);
}


/* I.2 - Init (for objects already created):
  - input: the vector size
  - output: the vector is allocated, size is set
*/
void vec_real::init(unsigned int N){
  m_size = N;
  m_v = (double*) fftw_malloc(sizeof(double)*m_size);
}


// Methods:

/* I.2 - Copy a vector
  - input: the vector to be copied
  - output: the current vector is a copy of toBeCopied
*/
void vec_real::copyVecFrom(vec_real& toBeCopied){
  unsigned int N = toBeCopied.size();
  m_size = N;
  m_v = (double*) fftw_malloc(sizeof(double)*m_size);
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof] = toBeCopied(iDof);
  }
}

// the associated operator:
void vec_real::operator << (vec_real& toBeCopied){copyVecFrom(toBeCopied);}


/* I.3 - set the m_v pointer to v
  - input: a pointer to a vector
  - output: the current m_v is set to v
  !! Size is not provided and has to be set !!
*/
void vec_real::setVector(double*& v){
  m_v = v;
}

// the associated operator
void vec_real::operator << (double*& v){
  m_v = v;
}


// II - OPERATIONS:

/* II.1 - axpy
  - input: a vec_real and a scalar
  - output: m_v += alpha * v
*/
void vec_real::axpy(vec_real& v, double alpha){
  assert(m_size==v.size());
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof] += alpha*v(iDof);
  }
}

// += operator
void vec_real::operator += (vec_real& toBeAdded){
  unsigned int N = toBeAdded.size();
  assert(N == m_size);
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof] += toBeAdded(iDof);
  }
}

// -= operator
void vec_real::operator -= (vec_real& toBeAdded){
  unsigned int N = toBeAdded.size();
  assert(N == m_size);
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof] -= toBeAdded(iDof);
  }
}

// *= operator
void vec_real::operator *= (double alpha){
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof] *= alpha;
  }
}


// norm and norm squared:
double vec_real::norm(){
  double out = 0.0;
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    out += m_v[iDof] * m_v[iDof];
  }
  return sqrt(out);
}

double vec_real::norm_squared(){
  double out = 0.0;
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    out += m_v[iDof] * m_v[iDof];
  }
  return out;
}
