// Implementation of linear algebra operations for vec_real objects:

#include "linearAlgebraOperations.h"

using namespace std;


/* I.1 - scalar product -
  - input: two vec_real
  - output: their scalar product
*/
double dot(vec_real& u, vec_real& v){
  assert(u.size()==v.size());
  double out = 0.0;
  for(unsigned int iDof=0; iDof<u.size(); iDof++){
    out += u(iDof)*v(iDof);
  }
  return out;
}


/* I.2 - sum and difference of two vectors -
  - input: two vec_real
  - output: their sum or difference
*/

//sum:
vec_real  operator + (vec_real& u, vec_real& v){
  assert(u.size()==v.size());
  vec_real out(u.size());
  for(unsigned int iDof=0; iDof<u.size(); iDof++){
    double val = u(iDof) + v(iDof);
    out.setVecEl(iDof,val);
  }
  return out;
}

// difference:
vec_real  operator - (vec_real& u, vec_real& v){
  assert(u.size()==v.size());
  vec_real out(u.size());
  for(unsigned int iDof=0; iDof<u.size(); iDof++){
    double val = u(iDof) - v(iDof);
    out.setVecEl(iDof,val);
  }
  return out;
}


/* I.3 - linear combination of vectors:
  - input: a set of vectors, and the coefficients of the linear combination
  - output: out = a_1 * v_1 + ... + a_n * v_n
*/
void lin_comb(vector<vec_real>& U, vector<double>& a, vec_real& out){
  assert(U.size()== a.size());
  const unsigned int nMod = U.size();
  out << U[0];
  out *= a[0];
  for(unsigned int iMod=1; iMod<nMod; iMod++){
    out.axpy(U[iMod],a[iMod]);
  }
}

// associated overloaded operator:
vec_real operator * (vecor<vec_real>& U, vector<double>& a){
  vec_real out;
  lin_comb(U, a, out);
  return out;
}
