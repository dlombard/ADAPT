/* header of the vec mother class:
all other types of vectors will be derivate classes all vectors will contain the same operators,
enabling the template definition of tensors:
*/

#ifndef vec_h
#define vec_h

#include "../generic_include.h"

using namespace std;

 // mother class: vector
class vec{
protected:
  unsigned int m_size;
public:
  vec(){};
  ~vec(){};
  virtual void init(unsigned int){};

  // ACCESS FUNCTION:
  inline unsigned int size(){return m_size;}
};


/* daughter class: real vectors.
  - storage: double array
  - type: sequential
*/
class vec_real : public vec{
private:
  double *m_v;
public:
  vec_real(){};
  vec_real(unsigned int);
  ~vec_real(){
    if(m_v != NULL){
      delete m_v;
      m_v = NULL;
    }
  }
  void init(unsigned int);
  void clear(){
    if(m_v != NULL){
      delete m_v;
      m_v = NULL;
    }
    m_size = 0;
  }

  // Methods:
  void copyVecFrom(vec_real&);
  void operator << (vec_real&);
  void setVector(double*&);
  void operator << (double*&);
  void operator += (vec_real&);
  void operator -= (vec_real&);
  void operator *= (double);
  double norm();
  double norm_squared();
  void axpy(vec_real&, double);

  // Setters:
  inline void set_size(unsigned int i){m_size = i;};
  inline void setVecEl(unsigned int i, double val){m_v[i]=val;}
  void operator ()(unsigned int i, double val){setVecEl(i,val);}

  // Access functions:
  inline double* v(){return m_v;}
  inline double v(unsigned int i){return m_v[i];}
  inline double operator()(unsigned int i){return m_v[i];}

  // print functions:
  inline void print_size(){cout<< "Real vec of size: " << m_size << endl;}
  inline void print(){
    print_size();
    for(unsigned int iDof=0; iDof<m_size; iDof++){
      cout<< m_v[iDof] << endl;
    }
    cout << endl;
  }
};






/* daughter class: real vectors.
  - storage: complex array, fftw3 type and storage
  - type: sequential
*/
class vec_comp : public vec{
private:
  fftw_complex *m_v;

public:
  vec_comp(){};
  vec_comp(unsigned int);
  vec_comp(fftw_complex*&, unsigned int);
  ~vec_comp(){};//fftw_free(m_v);}
  void init(unsigned int);
  void clear(){
    fftw_free(m_v);
    m_size = 0;
  }

  // methods:
  void copyVecFrom(vec_comp&);
  void operator << (vec_comp& toBeCopied){copyVecFrom(toBeCopied);}
  void operator << (fftw_complex*&);
  void conj();
  void operator += (vec_comp&);
  void operator -= (vec_comp&);
  void operator *= (double);
  void operator *= (fftw_complex*);
  double norm();
  double norm_squared();
  void axpy(vec_comp&, fftw_complex*);

  // Setters:
  inline void set_size(unsigned int i){m_size = i;};
  inline void setVecEl(unsigned int i, fftw_complex* val){m_v[i][0]=val[0][0]; m_v[i][1]=val[0][1];}
  inline void setVecReal(unsigned int i, double val){m_v[i][0]=val;}
  inline void setVecImag(unsigned int i, double val){m_v[i][1]=val;}
  inline void set_ptr(fftw_complex*& new_ptr){// call a clear before.
    m_v = new_ptr;
  }

  // Access functions:
  inline fftw_complex* v(){return m_v;}
  inline fftw_complex* v(unsigned int i){
    fftw_complex* a;
    a = (fftw_complex*) fftw_malloc(sizeof(fftw_complex));
    a[0][0] = m_v[i][0];
    a[0][1] = m_v[i][1];
    return a;
  }
  inline double real(unsigned int i){return m_v[i][0];}
  inline double imag(unsigned int i){return m_v[i][1];}
  inline fftw_complex* operator()(unsigned int i){return this->v(i);}

  // printing:
  inline void print_size(){cout<< "Complex vec of size: " << m_size << endl;}
  inline void print(){
    print_size();
    for(unsigned int iDof=0; iDof<m_size; iDof++){
      cout<< m_v[iDof][0] << " + i " << m_v[iDof][1] << endl;
    }
    cout << endl;
  }
};



/* daughter class: petsc real vectors.
  - storage: petsc Vec
  - type: parallel
*/
class vec_petsc : public vec{
private:
  Vec m_x;
  MPI_Comm m_comm;

public:

  // I - CONSTRUCTORS and DESTRUCTORS
  vec_petsc(){};
  ~vec_petsc(){};
  void clear(){VecDestroy(&m_x);}

  /* I.1 - overloaded constructors:
    - inputs: vector size, MPI communicator
    - output: the fields m_x, m_comm, m_size are initialised
  */
  vec_petsc(int, MPI_Comm=PETSC_COMM_WORLD);


  // -- INIT, in case of existing object --
  // I.2 - Initialize a Petsc Vec given its size and a communicator:
  void init(unsigned int, MPI_Comm=PETSC_COMM_WORLD);

  // I.2 - Overloaded, copying the structure of another vector, b:
  void init(vec_petsc&);

  // II - Setting the element of the vector
  void setVecEl(int, double);
  void setVecEl_seq(int, double);

  // adding values to existing entries for unassembled objects
  void addVecEl(int, double);
  void addVecEl_seq(int, double);

  // I.0) Finalize the Assembly
  void finalize(){
    VecAssemblyBegin(m_x);
    VecAssemblyEnd(m_x);
  }

  // set the whole x:
  void setVector(Vec&);

  // set Communicator
  void setComm(MPI_Comm);

  // get vector element:
  double getVecEl(int) const;

  // parallel version:
  double getVecEl_par(int) const;

  // Get the size (in case of resizing...)
  unsigned int getSize();

  // norm-2 of a vector:
  double norm() const;

  // infty-norm of a vector:
  double normInfty()const;

  // ones() to set all the vector elements to 1.0:
  void ones();

  // copy vector from a given vec:
  void copyVecFrom(const vec_petsc&);
  void operator << (const vec_petsc&);


  // II -- Operations: --

  // scale:
  void scale(const double);
  void operator *= (const double);

  // VecAXPY
  void axpy(const vec_petsc&, double);
  void operator += (vec_petsc);
  void operator -= (vec_petsc);

  // check if a vector is empty
  bool isEmpty();

  // checking equalities between vecs:
  bool operator == (vec_petsc);

  // add to all the elements a constant:
  void operator += (double);

  // extract sub-vector and put it into a vec: indices C convention
  vec_petsc extractSubvector(unsigned int, unsigned int) const;


  // III -  I/O operations:

  // saving a vector in binary format:
  void save(string);

  // loading a vector in binary format:
  void load(string);

  // loading a vector in binary format:
  void load(const string&, const MPI_Comm&);

  // save vector in ASCII:
  void saveASCII(string);

  // load from ASCII: -- useful for freefem++
  void loadASCII(string);


  // IV -- ACCESS FUNCTIONS:
  inline const Vec x() const {return m_x;}
  inline Vec x() {return m_x;}
  inline Vec& xPt() {return m_x;}
  inline unsigned int size() const {
    return m_size;
  }
  inline MPI_Comm comm() const {return m_comm;}
  inline void print(){
    VecView(m_x, PETSC_VIEWER_STDOUT_WORLD);
  }


};


#endif
