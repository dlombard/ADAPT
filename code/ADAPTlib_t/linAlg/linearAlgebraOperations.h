// linear algebra operations header:

# ifndef linearAlgebraOperations_h
# define linearAlgebraOperations_h

# include "../generic_include.h"
# include  "vec.h"

// I -- OPERATIONS between VECTORS:


// I.1 -- OPERATIONS between vec_real type of vectors:

// scalar product between two vectors:
double dot(vec_real&, vec_real&);

// sum and difference of two vectors:
vec_real operator + (vec_real&, vec_real&);
vec_real operator - (vec_real&, vec_real&);

// linear combination of a set of vec_real:
void lin_comb(vector<vec_real>&, vector<double>&, vec_real&);
vec_real operator * (vector<vec_real>&, vector<double>&);


# endif
