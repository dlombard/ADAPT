// header with generic include:

#ifndef GENERIC_INCLUDE_H
#define GENERIC_INCLUDE_H

// Standard lib:
#include <stdio.h>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ostream>
#include <string>
#include <vector>
#include <map>
#include <complex>
#include <math.h>
#include <ios>
#include <algorithm>
#include <random>
#include <chrono>
#include <cfloat>
#include <iomanip>

// mpi, petsc, slepc:
#include <mpi.h>
#include <petsc.h>
#include <slepc.h>

// fftw:
#include <fftw3.h>


#endif
