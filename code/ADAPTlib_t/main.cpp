// Parallel tensor implementation
#include "generic_include.h"
#include "linAlg/vec.h"

using namespace std;


extern "C" {
  extern int dgeev_(char*,char*,int*,double*,int*,double*, double*, double*, int*, double*, int*, double*, int*, int*);
  extern int dsyev_(char*,char*,int*,double*,int*,double*,double*,int*,int*);
  extern int dgesv_(int*, int*, double*, int*, int*, double*, int*, int*);
}


int main(int argc, char **args){

  cout << "Testing..." << endl;
  const char help = 'a';
  PetscInitialize(&argc,&args,(char *)0, &help);

  vec_petsc a(10);
  a.setVecEl(0,2.5);
  a.setVecEl(4,1.0);
  a.finalize();

  vec_petsc b; b << a;

  b.axpy(a, 2.0);
  b.print();


  // free the memory:
  a.clear();
  b.clear();
  // finalize
  PetscFinalize();
  return 0;

}
