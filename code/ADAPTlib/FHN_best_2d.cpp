// Parallel tensor implementation
#include "genericInclude.h"
#include "linAlg/linAlg.h"
#include "CP/CP.h"
#include "fullTensor/fullTens.h"
#include "TensorTrain/SoTT.h"
#include "compression/CPTensorCompression.hpp"
#include "Applications/Network/Network.h"
#include "Applications/Network/locHOSVD.h"
#include <string>

using namespace std;


/* Linspace function: */
vector<double> linspace(double start, double end, int num){

  vector<double> linspaced(num);


  if (num == 0) { return linspaced; }
  if (num == 1) {
      linspaced.push_back(start);
      return linspaced;
    }

  double delta = (end - start) / (num - 1.0);

  for(unsigned int i=0; i < num; ++i){
    linspaced[i] = start + delta * i;
  }
  //linspaced.push_back(end);  I want to ensure that start and end are exactly the same as the input
  return linspaced;
}


void saveField_2d_txt(vec& sol, unsigned int Nx, unsigned int Ny, string& fName){
 string saveName = fName + ".txt";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

  for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();
}


// Simple test on vectors

int main(int argc, char **args){

  static char help[] = "Testing\n\n";

  SlepcInitialize(&argc,&args,(char*)0,help);

  if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

  // Number of procs, and the ranks
  int nOfProcs;
  MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
  int idProc;
  MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

  unsigned int begining = 0;
  unsigned int end = 20;
  unsigned int t = end-begining;
  unsigned int Dofsx = 256 * 256;
  unsigned int nTheta = 2;


  //UNFOLDINGS X, T*THETA
  vector<vector<double>*>u(t*nTheta);
  for (size_t i = 0; i < t*nTheta; i++) {
    u[i] = new vector<double>(Dofsx);
  }
  //t
  vector<vector<double>*>u_t(Dofsx*nTheta);
  for (size_t i = 0; i < Dofsx*nTheta; i++){
    u_t[i] = new vector<double>(t);
  }


  double normData = 0.0;
  //Linear mapping t

  for (size_t iT = 0; iT < t; iT++) {
    if (iT%1 == 0){
      string uName = string("Applications/FitzHughNagumo/Outputs/FHNu_spiral/FHNu/u_") + to_string(iT) + string(".txt");
      if (iT%20 == 0){cout << uName << endl;}
      vec tmp = loadVecASCII(uName, PETSC_COMM_WORLD);
      for (size_t ix = 0; ix < Dofsx; ix++) {
        double val = tmp.getVecEl(ix);
        (*u[iT])[ix] = val;
        (*u_t[ix])[iT] = val;
        normData += val * val;
      }
      tmp.clear();
    }
  }

  normData = sqrt(normData);


//Clustering method

    vector<mat> Basis;
    vector<vector<unsigned int>> indOnBasis;
    vector<double> locErrBudget;
    vector<double> memory;
    double globalBudget;
    Network net;
    string x_name = "x";
    double error = 1.0e-4 * normData; // * sqrt(normalization);

    //Obtaining the basis with respect to the x
    net.lowMem_ComputeBases(u, error, Basis, indOnBasis, locErrBudget, globalBudget);
    cout << "Basis size " << Basis.size() << endl;
    unsigned int nfiles_x = indOnBasis.size();
    if (Basis.size() > 1) {
      net.lowMem_Merge2dBases(x_name, u, Basis, indOnBasis, locErrBudget, globalBudget, memory);
      cout << "Basis merged" << endl;
    }


    vector<mat> Basis_t;
    vector<vector<unsigned int>> indOnBasis_t;
    vector<double> locErrBudget_t;
    vector<double> memory_t;
    double globalBudget_t;
    string t_name = "t";


    //Obtaining the basis with respect to the t
    net.lowMem_ComputeBases(u_t, error, Basis_t, indOnBasis_t, locErrBudget_t, globalBudget_t);
    cout << "Basis_t computed and its size is " << Basis_t.size() << endl;
    unsigned int nfiles_t = indOnBasis_t.size();
    if (Basis_t.size() > 1) {
      net.lowMem_Merge2dBases(t_name, u_t, Basis_t, indOnBasis_t, locErrBudget_t, globalBudget_t, memory_t);
      cout << "Basis_t merged "<< endl;
    }

    cout << "number of files in t = " << nfiles_t << endl;
    cout << "number of files in x = " << nfiles_x << endl;


    indOnBasis.clear();
    indOnBasis_t.clear();


    Basis.clear();
    Basis_t.clear();


    /*
    outFileName = string("Applications/Network/memory_t_")+ to_string(t.size())+string("_") + string(".txt") ;
    outfile.open(outFileName.c_str());
    for (size_t iBas = 0; iBas < memory_t.size(); iBas++) {
      outfile << memory_t[iBas] << flush << endl;
    }
    outfile.close();*/

    unsigned int total_memory = 0;
    unsigned int total_memory_t = 0;


    /*
    //Computing basis and core for the HOSVD:
    mat Ux=lowMem_computeHOSVDbasis(u, error * error);
    mat Ut=lowMem_computeHOSVDbasis(u_t, error * error);

    vector<mat> basisHOSVD(2);
    basisHOSVD[0] = Ux;
    basisHOSVD[1] = Ut;

    mat core = lowMem_computeHOSVDcore(u, basisHOSVD);
    cout << "HOSVD core computed" << endl;
    unsigned int memory_pod = Ux.nCols() * (x.size() + t[iT].size());
    cout << "memory pod = " << memory_pod << endl;
    double compression_ratio_POD = (memory_pod * 1.0)/(x.size()*t[iT].size());

    double error_sum_HOSVD = 0.0;
    double error_sum_localHOSVD = 0.0;*/

    //unsigned int memory_locpod = 0;
    unsigned int best_memory = 1.0e8;
    vector<unsigned int> best_pair_clusters(2);

    double compression_ratio_locPOD = 0.0;


    for (size_t ix = 0; ix < nfiles_x; ix++) {
      unsigned int iob0_x;
      vector<unsigned int> iobList_x;
      vector<vector<unsigned int>> indOnBasis;
      string outFileName_x = string("indOnBasis_x_") + to_string(ix) + (".txt") ;
      net.load_indOnBasis(outFileName_x, indOnBasis, iobList_x, iob0_x);

      for (size_t it = 0; it < nfiles_t; it++) {
        unsigned int memory_locpod = 0;
        unsigned int iob0_t;
        vector<unsigned int> iobList_t;
        vector<vector<unsigned int>> indOnBasis_t;
        string outFileName_t = string("indOnBasis_t_") + to_string(it) + (".txt") ;
        net.load_indOnBasis(outFileName_t, indOnBasis_t, iobList_t, iob0_t);

        cout << indOnBasis.size() << endl;
        cout << indOnBasis_t.size() << endl;

        vector<vector<bool>> isItNon0(indOnBasis.size());

        for (unsigned int iBase = 0; iBase < indOnBasis.size(); iBase++) {
          isItNon0[iBase].resize(indOnBasis_t.size());
          for (unsigned int jBase = 0; jBase < indOnBasis_t.size(); jBase++) {
            vector<vec> theFibers(indOnBasis[iBase].size());

            for (unsigned int iFib = 0; iFib < indOnBasis[iBase].size(); iFib++) {
              theFibers[iFib].init(indOnBasis_t[jBase].size(), PETSC_COMM_WORLD);
              for (unsigned int jFib = 0; jFib < indOnBasis_t[jBase].size(); jFib++) {
                double val = (*u[indOnBasis[iBase][iFib]])[indOnBasis_t[jBase][jFib]];
                theFibers[iFib].setVecEl(jFib, val);
              }
              theFibers[iFib].finalize();
            }

            cout << "computed fibers" << endl;
            double l2 = 0.0;
            for (unsigned int iFib = 0; iFib < theFibers.size(); iFib++) {
              for (unsigned int jFib = 0; jFib < theFibers[0].size(); jFib++) {
                double val = theFibers[iFib](jFib)*theFibers[iFib](jFib);
                l2 += val;
              }
            }

            if (l2 > (error * error)/(indOnBasis.size() * indOnBasis_t.size() * indOnBasis_th.size())) {
              isItNon0[iBase][jBase][kBase] = true;
              unsigned int rank = estimate_basisRank(theFibers, (error * error)/(indOnBasis.size() * indOnBasis_t.size() * indOnBasis_th.size()));
              memory_locpod += rank * (theFibers.size() + theFibers[0].size());
            } else{isItNon0[iBase][jBase][kBase] = false;}

            for (size_t iFib = 0; iFib < indOnBasis[iBase].size(); iFib++) {
              theFibers[iFib].clear();
            }
            theFibers.clear();
        }
      }
      cout << "ix = " << ix << " and it = " << it << "and its memory = " << memory_locpod << endl;
      cout << endl;

      if (memory_locpod < best_memory) {
        best_memory = memory_locpod;
        best_pair_clusters[0] = ix;
        best_pair_clusters[1]= it;
      }
      indOnBasis_t.clear();
    }
    indOnBasis.clear();
  }
  cout << "the best memory = " << best_memory << " and the best pair " << best_pair_clusters[0] << " and " << best_pair_clusters[1] << endl;
  cout <<"the best number of merges in x = " << nfiles_x - best_pair_clusters[0] << " and in t = " << nfiles_t - best_pair_clusters[1]<<endl;

  SlepcFinalize();
  return 0;
}
