// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorCompression.hpp"
#include "Tucker.h"
#include "TensorTrain.h"
#include "Node.h"
#include "HPFTucker.h"
#include "HPFTucker_parallel.h"
#include "TuckerCompression.hpp"
#include "HPFTuckerCompression.hpp"
#include "operatorTensor.h"
#include "conversions.hpp"
#include "HPFCP.h"
#include "multilinearSolver.h"
#include "nearest.h"
#include "clustering.h"

using namespace std;


// function to save 3D fields:
/* 4 -- saving field in VTK:
  - input: a 3d tensor
  - output: a vtk file
*/
void saveField(fullTensor& T, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = T.nDof_var(0);
 unsigned int ny = T.nDof_var(1);
 unsigned int nz = T.nDof_var(2);
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;
 for(unsigned int h=0; h<T.nEntries(); h++){
     double value = T.tensorEntries(h);
     if(fabs(value) < 1.0e-9){value = 0.0;};
     outfile << value << endl;
 }
 outfile.close();
}



// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing nearest neighbors routines:
    const unsigned int nOfPts = 6;
    vector<vector<double> > pts(nOfPts);

    vector<double> pt_0(2);
    pt_0[0] = 0.0;
    pt_0[1] = 0.0;
    pts[0] = pt_0;

    vector<double> pt_1(2);
    pt_1[0] = 0.01;
    pt_1[1] = 0.01;
    pts[1] = pt_1;

    vector<double> pt_2(2);
    pt_2[0] = 0.0;
    pt_2[1] = 0.01;
    pts[2] = pt_2;

    vector<double> pt_3(2);
    pt_3[0] = 10.0;
    pt_3[1] = 10.0;
    pts[3] = pt_3;

    vector<double> pt_4(2);
    pt_4[0] = 9.99;
    pt_4[1] = 10.0;
    pts[4] = pt_4;

    vector<double> pt_5(2);
    pt_5[0] = 10.0;
    pt_5[1] = 9.99;
    pts[5] = pt_5;


    // clustering test:
    clustering test(pts, "l2", "centroid");
    for(unsigned int iCh=0; iCh<test.node()->children().size(); iCh++){
      cout << "Child " << iCh << ":\n";
      Node* child = test.node()->children(iCh);
      child->print_leafsTable();
      cout << endl;
    }

    // finilize:
    SlepcFinalize();
    return 0;

}
