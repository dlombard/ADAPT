// Parallel tensor implementation
#include "genericInclude.h"
// Include linear algebra headers:
#include "linAlg/linAlg.h"

using namespace std;

#include <mpi.h>



// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

    // Use the class vec
    vec myVec(6, PETSC_COMM_WORLD);
    myVec(0) = 1.0;
    myVec(5) = 3.0;
    myVec.finalize();

    myVec(0) = myVec(0) * 2.0;

    // compute the norm of myVec:
    double l2_norm = myVec.norm();
    double li_norm = myVec.normInfty();

    // normalise:
    double factor = 1./l2_norm;
    myVec *= factor;
    myVec.print();


    // define a matrix:
    mat A(6, 6, PETSC_COMM_WORLD);
    A(0,0) = 1.0;
    A(1,1) = 2.0;
    A(5,5) = 3.0;
    A.finalize();
    // finalize before using these commands in parallel:
    A(5,5) = A(5,5) + 1.0;
    A.print();

    // Adding the identity matrix to A:
    mat I = eye(6, PETSC_COMM_WORLD);
    mat B = A + I;
    // As an alternative: A += I;
    A += eye(6, PETSC_COMM_WORLD);
    A.print();
    B.print();

    // multiply the Matrix by a vector:
    vec y = B * myVec;
    y.print();

    // solving linear system with standard options:
    vec sol = B / y;
    PetscPrintf(PETSC_COMM_WORLD, "The system solution is:\n");
    sol.print();
    PetscPrintf(PETSC_COMM_WORLD, "\n");

    // reshape it:
    mat K = reshape(I, 4, 9);
    K.print();


    // free the memory:
    myVec.clear();
    y.clear();
    sol.clear();
    A.clear();
    B.clear();
    K.clear();
    I.clear();

    SlepcFinalize();
    return 0;

}
