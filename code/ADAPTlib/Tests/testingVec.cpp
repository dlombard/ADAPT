// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

    // Use the class vec
    vec myVec(6, PETSC_COMM_WORLD);
    myVec(0) = 1.0;
    myVec(5) = 3.0;
    myVec.finalize();

    myVec(0) = myVec(0) * 2.0;
    myVec.print();

    vec toTestSum(6, PETSC_COMM_WORLD);
    toTestSum(0) = 2.0;
    toTestSum.finalize();

    vec sumOfTwo = myVec + toTestSum;
    sumOfTwo.print();

    double alpha = scalProd(myVec, toTestSum);
    PetscPrintf(PETSC_COMM_WORLD, "alpha = %f \n", alpha);

    vec point = pointWiseMult(myVec, toTestSum);
    point.print();


    // add to myVec 1.0
    myVec += 1.0;
    myVec.print();

    // compute the norm of myVec:
    double l2_norm = myVec.norm();
    double li_norm = myVec.normInfty();

    // normalise:
    double factor = 1./l2_norm;
    myVec *= factor;
    myVec.print();


    myVec.clear();
    toTestSum.clear();
    sumOfTwo.clear();
    point.clear();

    SlepcFinalize();
    return 0;

}
