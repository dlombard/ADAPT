// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorCompression.hpp"
#include "Tucker.h"
#include "TensorTrain.h"
#include "Node.h"
#include "HPFTucker.h"
#include "HPFTucker_parallel.h"
#include "TuckerCompression.hpp"
#include "HPFTuckerCompression.hpp"
#include "operatorTensor.h"
#include "conversions.hpp"
#include "HPFCP.h"
#include "multilinearSolver.h"
#include "nearest.h"
#include "clustering.h"
#include "pointCloud.h"
#include "mesh.h"


using namespace std;

inline void writeCSVField(vector<double> field, vector<vector<double> > pts, string fName){
  assert(field.size() == pts.size());
  ofstream outFile(fName.c_str());
  outFile << "x coord, y coord, z coord, scalar\n";
  for(unsigned int i=0; i<field.size(); i++){
    double value = field[i];
    if(fabs(value)<1.0e-9){
      value = 0.0;
    }
    vector<double> currentPt = pts[i];
    outFile << currentPt[0] << ", " << currentPt[1] <<", " << currentPt[2] << ", " <<value << endl;
  }
  outFile.close();
}



// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing mesh reading:
    mesh test("data/cube.msh", "gmsh");
    test.print();

    // construct a hierarchical partitioning of the mesh:
    vector<vector<double> > dofs = test.pts();
    clustering meshPartitioning(dofs, "l2", "linkage");

    // printing:
    vector<double> coloring(dofs.size());

    unsigned int depth = 3;
    vector<vector<vector<unsigned int> > > exTable = meshPartitioning.indTableAtDepth(depth);
    cout << "number of subdomains = " << exTable.size() << endl;
    for(unsigned int iSub=0; iSub<exTable.size(); iSub++){
      cout << "Sub " << iSub << "  number of dof = " << exTable[iSub][0].size() <<".\n";
      for(unsigned int iPt=0; iPt<exTable[iSub][0].size(); iPt++){
        //cout << exTable[iSub][0][iPt] << endl;
        unsigned int ind = exTable[iSub][0][iPt];
        coloring[ind] = iSub;
      }
      cout << endl;
      cout << endl;
    }

    test.clear();
    writeCSVField(coloring, dofs, "data/coloring.csv");

    // finalize:
    SlepcFinalize();
    return 0;

}
