// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing the full tensor class:
    sparseTensor T({3,4,2}, PETSC_COMM_WORLD);


    // setting the tensor elements:
    cout << "linear index = " << T.sub2lin({1,0,0}) << endl;

    // operator ():
    T(1,0,0) = 2.0;
    T(1,0,0) = 2.5* T(1,0,0) - 1.0;
    T(0,0,1) = 7.0;
    T(2,0,1) = 1.0;
    T(0,1,0) = 5.0;

    T.finalize();
    cout << " Number of non-zeros: "  <<T.nonZeros() << endl;

    T.tensorEntries().print();
    cout << endl;

    // copy it:
    sparseTensor S;
    S.copyTensorFrom(T);

    // add another element
    S(1,1,0) = 4.5;
    S.finalize();
    S.tensorEntries().print();
    cout << endl;

    // outer product:
    sparseTensor L = outerProduct(S,T);
    L.tensorEntries().print();

    // sum:
    sparseTensor R = S + T;

    // scalar product:
    double val = scalProd(S,T);
    PetscPrintf(PETSC_COMM_WORLD, "Scalar Product = %e\n", val);

    // free the memory:
    T.clear();
    S.clear();
    L.clear();
    R.clear();

    // finilize:
    SlepcFinalize();
    return 0;

}
