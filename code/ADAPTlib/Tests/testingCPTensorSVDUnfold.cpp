// Parallel tensor implementation
#include "genericInclude.h"
#include "linAlg/linAlg.h"
#include "CP/CP.h"
#include "linAlg/svd.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing the full tensor class:
    CPTensor T({3,4,2}, PETSC_COMM_WORLD);


    // setting the tensor elements:
    PetscPrintf(PETSC_COMM_WORLD, "linear index = %d \n",T.sub2lin({1,0,0}));

    //T.set_tensorElement({0,0,0}, 1.0);
    T(0,0,0) = 1.0;
    T(1,0,0) = 1.0;
    T(2,0,0) = 1.0;
    T.finalize();

    cout << "The rank of T is: " << T.rank() << endl;

    // computing a tensor S:
    CPTensor S;
    S.copyTensorFrom(T);
    S *= 2.0;
    S += 1.0;

    cout << "The rank of S is: " << S.rank() << endl;


    // compute one term svd unfolding:
    vec u;
    double sigma;
    T.compute_Unfolding_OneSvdTerm(0, sigma, u);
    u.print();
    cout << endl;
    PetscPrintf(PETSC_COMM_WORLD, "Sigma = %e \n", sigma);

    // compute unfolding:
    mat M_0 = T.computeUnfolding(0);
    M_0.print();


    // computing the SVD;
    svd mySvd(M_0);
    mySvd.Umat().print();
    mySvd.Svec().print();
    mySvd.Vmat().print();



    // free the memory:
    T.clear();
    S.clear();
    mySvd.clear();



    // finilize:
    SlepcFinalize();
    return 0;

}
