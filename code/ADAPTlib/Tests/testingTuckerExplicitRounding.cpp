// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorCompression.hpp"
#include "Tucker.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing the full tensor class:
    Tucker T({3,4,2}, PETSC_COMM_WORLD);

    T(0,0,0) = 1.0;

    // command to manipulate entries:
    T(0,0,0) = 2.0 * T(0,0,0) + 1.0;
    cout << T(0,0,0) << endl;

    // adding another entry:
    T(1,0,0) = 5.0;
    cout << T(1,0,0) << endl;


    cout << "R = [" << T.ranks(0) << ", " << T.ranks(1) << ", " << T.ranks(2) << "]" << endl;


    cout << "\n";
    T.core().tensorEntries().print();

    for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
      cout << "iVar = " << iVar << endl;
      for(unsigned int iMod=0; iMod<T.ranks(iVar); iMod++){
        T.modes(iVar, iMod).print();
      }
      cout << "\n\n";
    }

    // svd, direction per direction, of the modes:
    cout << "SVD, direction 0" << endl;

    svd sDir_0(T.modes(0));
    mat U0 = sDir_0.Umat();
    mat V0 = sDir_0.Vmat();
    mat S0 = sDir_0.Smat();
    mat Vt0 = transpose(V0);
    mat W0 = S0 * Vt0;

    U0.print();
    cout << endl;
    W0.print();

    cout << "\n";
    cout << "SVD, direction 1" << endl;

    svd sDir_1(T.modes(1));
    mat U1 = sDir_1.Umat();
    mat V1 = sDir_1.Vmat();
    mat S1 = sDir_1.Smat();
    mat Vt1 = transpose(V1);
    mat W1 = S1 * Vt1;

    U1.print();
    cout << endl;
    W1.print();


    cout << "\n";
    cout << "SVD, direction 2" << endl;

    svd sDir_2(T.modes(2));
    mat U2 = sDir_2.Umat();
    mat V2 = sDir_2.Vmat();
    mat S2 = sDir_2.Smat();
    mat Vt2 = transpose(V2);
    mat W2 = S2 * Vt2;

    U2.print();
    cout << endl;
    W2.print();


    // try to contract the core:
    fullTensor test_0 = T.core().matTensProd(0,W0);
    fullTensor test_1 = test_0.matTensProd(1,W1);
    fullTensor test_2 = test_1.matTensProd(2,W2);

    test_2.print();
    test_2.tensorEntries().print();


    // try to compress T:
    /*T.rounding(1.0e-3);
    cout << "Ranks after rounding:\n";
    cout << "R = [" << T.ranks(0) << ", " << T.ranks(1) << ", " << T.ranks(2) << "]" << endl;
    cout << T(1,0,0) << endl;*/

    // free the memory
    T.clear();


    // finilize:
    SlepcFinalize();
    return 0;

}
