// Parallel tensor implementation
#include "genericInclude.h"
#include "linAlg/linAlg.h"
#include "Applications/Schroedinger/fft.h"

using namespace std;





// MAIN:
int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    const double pi = 4 * atan(1.0);

    //signals:
    unsigned int N = 128;
    vec sinus(N, PETSC_COMM_WORLD);
    vec empty(N, PETSC_COMM_WORLD);

    for(unsigned int iDof=0; iDof<N; iDof++){
      double x = iDof * (2.0*pi/N);
      sinus(iDof) = sin(x);
      empty(iDof) = 0.0;
    }
    sinus.finalize();
    empty.finalize();

    // testing fftw:
    vec f_real, f_imag;
    fft_1d(sinus, empty, f_real, f_imag);

    f_imag.print();

    // computing auto-correlation:
    vec conv_real, conv_imag;
    convolution_1d(sinus, empty, sinus, empty, conv_real, conv_imag);
    conv_real.print();


    SlepcFinalize();
    return 0;

}
