// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing the full tensor class:
    CPTensor T({3,4,2}, PETSC_COMM_WORLD);


    // setting the tensor elements:
    PetscPrintf(PETSC_COMM_WORLD, "linear index = %d \n",T.sub2lin({1,0,0}));

    //T.set_tensorElement({0,0,0}, 1.0);
    T(0,0,0) = 1.0;
    T(1,0,0) = 2.0;
    T(2,0,0) = 5.0;
    T.finalize();

    cout << "The rank of T is: " << T.rank() << endl;

    // computing a tensor S:
    CPTensor S;
    S.copyTensorFrom(T);
    S *= 2.0;
    S += 1.0;

    cout << "The rank of S is: " << S.rank() << endl;

    // extracting a subtensor
    cout << endl;
    CPTensor R = S.extractSubtensor({0,2,0,2,0,1});

    cout << "The rank of R is: " << R.rank() << endl;
    R.print();

    // checking all terms...
    /*for(unsigned int iTerm=0; iTerm<R.rank(); iTerm++){
      for(unsigned int iVar=0; iVar<R.nVar(); iVar++){
        R.terms(iTerm,iVar).print();
        cout << endl;
      }
    }*/

    // assign subtensor:
    T.assignSubtensor({0,2,0,2,0,1}, R);
    cout << "The rank of T is: " << T.rank() << endl;
    
    // free the memory:
    T.clear();
    S.clear();
    R.clear();


    // finilize:
    SlepcFinalize();
    return 0;

}
