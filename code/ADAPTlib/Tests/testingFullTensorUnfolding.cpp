// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing the full tensor class:
    fullTensor T({3,4,2}, PETSC_COMM_WORLD);


    // setting the tensor elements:
    cout << "linear index = " << T.sub2lin({1,0,0}) << endl;
    T.tensorEntries().setVecEl(1, 1.0);

    // operator ():
    T(1,0,0) = 2.0;
    T(1,0,0) = 2.5* T(1,0,0) - 1.0;
    T(0,0,1) = 7.0;
    T(2,0,1) = 1.0;
    T(0,1,0) = 5.0;
    T.finalize();

    T.tensorEntries().print();
    cout << endl;

    // testing the unfolding:
    mat M_0 = T.computeUnfolding(0);
    M_0.print();

    cout << endl;
    mat M_1 = T.computeUnfolding(1);
    M_1.print();

    cout << endl;
    mat M_2 = T.computeUnfolding(2);
    M_2.print();

    // getting fibers:
    vector<vec> Tfibers = T.computeFibers(0);
    cout << endl;
    Tfibers[4].print();



    // free the memory:
    T.clear();
    M_0.clear();
    M_1.clear();
    M_2.clear();

    for(unsigned int iFiber = 0; iFiber< Tfibers.size(); iFiber++){
      Tfibers[iFiber].clear();
    }

    // finilize:
    SlepcFinalize();
    return 0;

}
