// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

    // Group of processes in MPI_COMM_WORLD
    MPI_Group worldGroup;
    MPI_Comm_group(PETSC_COMM_WORLD, &worldGroup);

    // Create two groups, even ranks, odd ranks.
    int nEven, nOdd;
    if (nOfProcs%2==0) {
      nEven = nOfProcs / 2;
      nOdd = nOfProcs / 2;
    }
    else{
      nEven = (nOfProcs + 1)/2;
      nOdd = nOfProcs - nEven;
    }

    int evenRanks[nEven];
    int oddRanks[nOdd];
    int ccEven=0;
    int ccOdd=0;
    for(int iProc=0; iProc<nOfProcs; iProc++){
      if(iProc%2==0){
        evenRanks[ccEven] = iProc;
        ccEven += 1;
      }
      else{
        oddRanks[ccOdd] = iProc;
        ccOdd += 1;
      }
    }

    // Initialise the group even:
    MPI_Group evenGroup;
    MPI_Group_incl(worldGroup, nEven, evenRanks, &evenGroup);

    // Create a new communicator based on the even group ( 0 is the TAG!)
    MPI_Comm evenComm;
    MPI_Comm_create_group(PETSC_COMM_WORLD, evenGroup, 0, &evenComm);


    // Initialise the group odd:
    MPI_Group oddGroup;
    MPI_Group_incl(worldGroup, nOdd, oddRanks, &oddGroup);

    // Create a new communicator based on the odd group ( 1 is the TAG!)
    MPI_Comm oddComm;
    MPI_Comm_create_group(PETSC_COMM_WORLD, oddGroup, 1, &oddComm);


    // printing routines:
    if(evenComm != MPI_COMM_NULL){
      int sizeEven, iEven;
      MPI_Comm_size(evenComm, &sizeEven);
      MPI_Comm_rank(evenComm, &iEven);

      vec v1(4, evenComm);
      v1(0) = 1.0;
      v1(1) = 1.0;
      v1.finalize();
      vec v2(4, evenComm);
      v2(1) = 1.0;
      v2(2) = 2.0;
      v2.finalize();

      mat A(4,4, evenComm);
      A(0,0) = 3.5;
      A(1,1) = 3.5;
      A(2,2) = 3.5;
      A(3,3) = 3.5;
      A.finalize();

      vec w = A * v1;

      cout << "Proc " << iEven << ", global " << idProc << " in a group of " << sizeEven << endl;
      cout << "Value = " << w(0) << endl;

      qr orth({v1,v2});
      PetscPrintf(evenComm, "v1[0] = %f \n", v1.getVecEl(0));
      PetscPrintf(evenComm, "Even team, done.\n");

      // free the memory:
      v1.clear();
      v2.clear();
    }

    if(oddComm != MPI_COMM_NULL){
      int sizeOdd, iOdd;
      MPI_Comm_size(oddComm, &sizeOdd);
      MPI_Comm_rank(oddComm, &iOdd);

      vec v3(4, oddComm);
      v3(2) = 3.0;
      v3(3) = 4.0;
      v3.finalize();
      vec v4(4, oddComm);
      v4(0) = 2.0;
      v4(3) = 5.0;
      v4.finalize();

      cout << "Proc " << iOdd << ", global " << idProc << " in a group of " << sizeOdd << endl;

      qr orth({v3,v4});

      PetscPrintf(oddComm, "v4[0] = %f \n", v4.getVecEl(0));
      PetscPrintf(oddComm, "Odd team, done.\n");

      // free the memory:
      v3.clear();
      v4.clear();
    }



    // Destroy the objects:
    MPI_Group_free(&worldGroup);
    MPI_Group_free(&evenGroup);
    MPI_Group_free(&oddGroup);
    if(evenComm != MPI_COMM_NULL){MPI_Comm_free(&evenComm);}
    if(oddComm != MPI_COMM_NULL){MPI_Comm_free(&oddComm);}


    // finilize:
    SlepcFinalize();
    return 0;

}
