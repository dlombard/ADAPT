// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorCompression.hpp"
#include "Tucker.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing the full tensor class:
    Tucker T({3,4,2}, PETSC_COMM_WORLD);

    T(0,0,0) = 1.0;

    // command to manipulate entries:
    T(0,0,0) = 2.0 * T(0,0,0) + 1.0;
    cout << T(0,0,0) << endl;

    // adding another entry:
    T(1,0,0) = 5.0;
    cout << T(1,0,0) << endl;


    cout << "R = [" << T.ranks(0) << ", " << T.ranks(1) << ", " << T.ranks(2) << "]" << endl;


    cout << "\n";
    T.core().tensorEntries().print();

    for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
      cout << "iVar = " << iVar << endl;
      for(unsigned int iMod=0; iMod<T.ranks(iVar); iMod++){
        T.modes(iVar, iMod).print();
      }
      cout << "\n\n";
    }

    // try to compress T:
    T.rounding(1.0e-3);
    cout << "Ranks after rounding:\n";
    cout << "R = [" << T.ranks(0) << ", " << T.ranks(1) << ", " << T.ranks(2) << "]" << endl;
    cout << T(0,0,0) << " " << T(1,0,0) << endl;

    // free the memory
    T.clear();


    // finilize:
    SlepcFinalize();
    return 0;

}
