// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorCompression.hpp"
#include "Tucker.h"
#include "TensorTrain.h"
#include "Node.h"
#include "HPFTucker.h"
#include "HPFTucker_parallel.h"
#include "TuckerCompression.hpp"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // tree width:
    const unsigned int width = 2;

    // Construct a full tensor:
    fullTensor T({8,16,4}, PETSC_COMM_WORLD);
    T(0,0,0) = 1.0;
    T(1,0,0) = 2.0;
    T(2,0,0) = 3.0;
    T.finalize();

    // polymorphism:
    tensor* target = &T;

    // Compute its HOSVD:
    TuckerCompression Approx;

    // function that produces a Tucker approximation of a fullTensor:
    Approx.HOSVD(T);
    Tucker tildeT = Approx.result();

    // Generic computation of HOSVD:
    Approx.HOSVD(*target);

    cout << "Number of modes:\n";
    cout << Approx.HOSVD_modes(0).size() << endl;
    cout << Approx.HOSVD_modes(1).size() << endl;
    cout << Approx.HOSVD_modes(2).size() << endl;

    cout << endl;
    cout << "Comparing modes:\n";
    cout << "tildeT, Mode 0-0:\n";
    tildeT.modes(0,0).print();
    cout << "HOSVD table, Mode 0-0:\n";
    Approx.HOSVD_modes(0,0).print();

    // testing subtensor syntax:
    fullTensor subT = T.extractSubtensor({0,2,0,2,0,2});

    tensor* subTens = &subT;
    cout << endl;
    cout << subTens->eval({0,0,0}) << endl;
    subTens->computeUnfolding(0).print();

    cout << endl;
    cout << subT.eval({0,0,0}) << endl;

    // polymorphic via const tensor&:
    const tensor& test = T.extractSubtensor({0,2,0,2,0,2});

    // testing unfoldings:
    target->computeUnfolding(0).print();

    // testing:
    TuckerCompression Approx_sub;

    Approx_sub.HOSVD(*subTens);
    cout << "Number of modes:\n";
    cout << Approx_sub.HOSVD_modes(0).size() << endl;
    cout << Approx_sub.HOSVD_modes(1).size() << endl;
    cout << Approx_sub.HOSVD_modes(2).size() << endl;
    Approx_sub.HOSVD_modes(0,0).print();

    // free the memory:
    T.clear();
    tildeT.clear();
    subT.clear();

    // finilize:
    SlepcFinalize();
    return 0;

}
