// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"

using namespace std;

// first test on a parallel CP tensor:
class parCPTens{
private:
  unsigned int m_nVar;
  vector<unsigned int> m_nDof_var;
  unsigned int m_nOfTeams;

  vector<vector<vec> > m_terms; // every team has its own...
  vector<double> m_coeffs;
  unsigned int m_rank;

  MPI_Comm m_comm;
  MPI_Group m_worldGroup;
  vector<MPI_Group> m_teams;
  vector<MPI_Comm> m_comm_teams;

public:
  parCPTens(){};
  ~parCPTens(){

  };

  void clear(){
    // Destroy the MPI objects:
    MPI_Group_free(&m_worldGroup);
    for(unsigned int iTeam=0; iTeam<m_nOfTeams; iTeam++){
      MPI_Group_free(&m_teams[iTeam]);
      if(m_comm_teams[iTeam] != MPI_COMM_NULL){MPI_Comm_free(&m_comm_teams[iTeam]);}
    }
  }

  // overloaded constructor:
  parCPTens(unsigned int nOfTeams, MPI_Comm theComm){
    m_comm = theComm;
    m_nOfTeams = nOfTeams;
    int nOfProcs;
    MPI_Comm_size(m_comm, &nOfProcs);
    int idProc;
    MPI_Comm_rank(m_comm, &idProc);
    MPI_Comm_group(m_comm, &m_worldGroup);

    m_teams.resize(m_nOfTeams);
    m_comm_teams.resize(m_nOfTeams);

    // Create m_nOfTeams groups, based on the proc ranks:
    vector<vector<int> > idProcs_inTeams(m_nOfTeams);
    for(int iProc=0; iProc<nOfProcs; iProc++){
      int iTeam = iProc%m_nOfTeams;
      idProcs_inTeams[iTeam].push_back(iProc);
    }

    // Creating the teams:
    for(unsigned int iTeam=0; iTeam< m_nOfTeams; iTeam++){
      int nThis = idProcs_inTeams[iTeam].size();
      int thisRanks[nThis];
      for(unsigned int iP=0; iP<nThis; iP++){
        thisRanks[iP] = idProcs_inTeams[iTeam][iP];
      }

      MPI_Group thisGroup;
      MPI_Group_incl(m_worldGroup, nThis, thisRanks, &thisGroup);
      m_teams[iTeam] = thisGroup;

      // Create a new communicator based on the groups ( 0 is the TAG!)
      MPI_Comm thisComm;
      MPI_Comm_create_group(m_comm, thisGroup, 0, &thisComm);
      m_comm_teams[iTeam] = thisComm;
    }

    // testing the teams:
    for(unsigned int iTeam=0; iTeam<m_nOfTeams; iTeam++){
      int locSize, locProc;
      MPI_Comm_size(m_comm_teams[iTeam], &locSize);
      MPI_Comm_rank(m_comm_teams[iTeam], &locProc);
      if(m_comm_teams[iTeam] != MPI_COMM_NULL){
          cout << "Proc " << locProc << ", global " << idProc << " in the team " << iTeam << endl;
      }
    }

  }

  // checking teams by printing:
  inline void checkTeams(){
    int nOfProcs;
    MPI_Comm_size(m_comm, &nOfProcs);
    int idProc;
    MPI_Comm_rank(m_comm, &idProc);

    for(unsigned int iTeam=0; iTeam<m_nOfTeams; iTeam++){
      int locSize, locProc;
      MPI_Comm_size(m_comm_teams[iTeam], &locSize);
      MPI_Comm_rank(m_comm_teams[iTeam], &locProc);
      if(m_comm_teams[iTeam] != MPI_COMM_NULL){
          cout << "Proc " << locProc << ", global " << idProc << " in the team " << iTeam << endl;
      }
    }
    //end of function
  }


  // ACCESS FUNCTIONS:
  inline unsigned int nVar(){return m_nVar;}
  inline vector<unsigned int> nDof_var(){return m_nDof_var;}
  inline unsigned int nDof_var(unsigned int j){return m_nDof_var[j];}

};



// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // init a CP on 2 teams of procs:
    parCPTens myCP(2, PETSC_COMM_WORLD);


    myCP.clear();

    // finilize:
    SlepcFinalize();
    return 0;

}
