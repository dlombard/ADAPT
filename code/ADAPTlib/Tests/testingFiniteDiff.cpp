// Parallel tensor implementation
#include "genericInclude.h"
// Include linear algebra headers:
#include "linAlg/linAlg.h"
// Include discretisation:
#include "Discretisations/Discretisations.h"

using namespace std;

#include <mpi.h>



// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing built in discretisation routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

    // Use the class finiteDifferences:
    finiteDifferences testSpace(3, {10,4,8}, PETSC_COMM_WORLD);
    testSpace.print();

    // Compute the mass matrix:
    testSpace.compute_mass();

    // Test a Laplacian matrix assembly, without boundaries:
    const unsigned int nDof = testSpace.nDof();
    mat Laplacian(nDof, nDof, PETSC_COMM_WORLD);
    for(unsigned int iDof=0; iDof<nDof; iDof++){
      // Assemble the bulk:
      if(!testSpace.isItOnBoundary(iDof)){
        double diagVal = 0.0;
        for(unsigned int iVar=0; iVar<testSpace.dim(); iVar++){
          diagVal += -2.0/(testSpace.dx(iVar)*testSpace.dx(iVar));
        }
        Laplacian(iDof,iDof) = diagVal;

        // number of "star points" = 2*n with increments [0,..,±1,...]
        for(unsigned int iVar=0; iVar<testSpace.dim(); iVar++){
          vector<int> inc_plus(testSpace.dim(),0);
          inc_plus[iVar] = 1;
          unsigned int lin_plus = testSpace.linIndIncrement(iDof,inc_plus);
          Laplacian(iDof,lin_plus) = 1.0/(testSpace.dx(iVar)*testSpace.dx(iVar));
          vector<int> inc_minus(testSpace.dim(),0);
          inc_minus[iVar] = -1;
          unsigned int lin_minus = testSpace.linIndIncrement(iDof,inc_minus);
          Laplacian(iDof,lin_minus) = 1.0/(testSpace.dx(iVar)*testSpace.dx(iVar));
        }
      }
      // Assemble the boundary:
      //...
    }
    Laplacian.finalize();
    Laplacian.print();


    SlepcFinalize();
    return 0;

}
