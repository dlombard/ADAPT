// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorCompression.hpp"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing the full tensor class:
    CPTensor T({3,4,2}, PETSC_COMM_WORLD);


    // setting the tensor elements:
    PetscPrintf(PETSC_COMM_WORLD, "linear index = %d \n",T.sub2lin({1,0,0}));

    //T.set_tensorElement({0,0,0}, 1.0);
    T(0,0,0) = 1.0;
    T(1,0,0) = 1.0;
    T(2,0,0) = 1.0;
    T.finalize();

    cout << "The rank of T is: " << T.rank() << endl;

    // computing a tensor S:
    CPTensor S;
    S.copyTensorFrom(T);
    S *= 2.0;
    S += 1.0;

    cout << "The rank of S is: " << S.rank() << endl;

    CPTensor R;
    R.copyTensorFrom(S);
    R += -1.0;

    double normT = sqrt(T.norm2CP());
    double normR = sqrt(R.norm2CP());
    double normS = sqrt(S.norm2CP());

    PetscPrintf(PETSC_COMM_WORLD, " |T| = %e , |S| = %e, |R| = %e \n", normT, normS, normR);



    // the tensor R is rank 1: one CP-TT term is sufficient to compress it:
    CPTensorCompression CPComp;

    // one CP-TT term:
    vector<vec> term;
    double coeff;
    CPComp.compute_CPTT_Term(R, term, coeff);
    term[0].print();
    cout << endl;
    term[1].print();
    cout << endl;
    term[2].print();

    // computing new R:
    R.addPureTensorTerm(term, -1.0*coeff);
    normR = sqrt(R.norm2CP());

    cout << endl;
    PetscPrintf(PETSC_COMM_WORLD, " |R| = %e \n", normR);



    // free the memory:
    T.clear();
    S.clear();
    R.clear();


    // finilize:
    SlepcFinalize();
    return 0;

}
