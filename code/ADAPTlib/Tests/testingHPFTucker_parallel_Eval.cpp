// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorCompression.hpp"
#include "Tucker.h"
#include "TensorTrain.h"
#include "Node.h"
#include "HPFTucker.h"
#include "HPFTucker_parallel.h"
#include "TuckerCompression.hpp"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // tree width:
    const unsigned int width = 2;

    // Creation of HPF:
    HPFTucker T({8,16,4}, width, PETSC_COMM_WORLD);

    // filling the tensor:
    T(0,0,0) = 1.0;
    T(5,3,1) = 2.0;



    // initialisation of HPFTucker parallel
    PetscPrintf(PETSC_COMM_WORLD,"Building its parallel version.\n");

    unsigned int nTeams = 2;
    HPFTucker_parallel S({8,16,4}, width, nTeams, PETSC_COMM_WORLD);
    //S.checkTeams();
    //S.print_leafsTable();

    S.set_tensorElement({0,0,0}, 4.0);
    double val = S.eval({0,0,0});
    cout << "Val = " << val << endl;

    // testing the variadic operator ():
    S(0,0,0) = 2.0;
    cout << "Value after operation: " << S(0,0,0) << endl;

    // finilize:
    SlepcFinalize();
    return 0;

}
