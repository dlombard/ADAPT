// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing the full tensor class:
    fullTensor T({3,4,2}, PETSC_COMM_WORLD);


    // setting the tensor elements:
    cout << "linear index = " << T.sub2lin({1,0,0}) << endl;
    T.tensorEntries().setVecEl(1, 1.0);

    // operator ():
    T(1,0,0) = 2.0;
    T(1,0,0) = 2.5* T(1,0,0) - 1.0;
    T(0,0,1) = 7.0;
    T(2,0,1) = 1.0;
    T.finalize();

    T.tensorEntries().print();


    // testing the copy:
    fullTensor S;
    S.copyTensorFrom(T);

    S.print();
    S.tensorEntries().print();


    // testing the sum: S.sum(T, 1.0);
    S += T;
    cout << endl;
    S.tensorEntries().print();

    // free the memory:
    T.clear();
    S.clear();

    // finilize:
    SlepcFinalize();
    return 0;

}
