// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorCompression.hpp"
#include "Tucker.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing the full tensor class:
    Tucker T({3,4,2}, PETSC_COMM_WORLD);

    // setting a tensor element:
    T.set_tensorElement({0,0,0}, 1.0);

    // evaluating it:
    PetscPrintf(PETSC_COMM_WORLD, "the element T(0,0,0) = %e\n", T.eval({0,0,0}));

    // testing the operator ()
    double val = T(0,0,0);
    PetscPrintf(PETSC_COMM_WORLD, "the element T(0,0,0) = %e\n", val);

    // command to manipulate entries:
    T(0,0,0) = 2.0 * T(0,0,0) + 1.0;
    cout << T(0,0,0) << endl;

    // adding another entry:
    T(1,0,0) = 5.0;
    cout << T(1,0,0) << endl;

    cout << "R = [" << T.ranks(0) << ", " << T.ranks(1) << ", " << T.ranks(2) << "]" << endl;

    // Tensor S:
    Tucker S;
    S.copyTensorFrom(T);

    // shifting the whole tensor by 2.5 and multiplying by 2.0:
    S += 2.5;
    S *= 2.0;
    cout << S(2,0,0) << endl;
    cout << "R = [" << S.ranks(0) << ", " << S.ranks(1) << ", " << S.ranks(2) << "]" << endl;




    // free the memory
    T.clear();
    S.clear();

    // finilize:
    SlepcFinalize();
    return 0;

}
