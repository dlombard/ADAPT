// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


    // define a matrix:
    mat A(6, 6, PETSC_COMM_WORLD);
    A(0,0) = 1.0;
    A(1,1) = 2.0;
    A(5,5) = 3.0;
    A.finalize();

    A(5,5) = A(5,5) + 1.0;
    A.finalize();

    // Adding the identity matrix to A:
    mat I = eye(6, PETSC_COMM_WORLD);
    mat B = A + I;
    B.print();

    // defining eig object
    eigenSolver myEig;
    /* // Extended syntax //
    myEig.init(B, EPS_HEP);
    myEig.solveFirstPairHermEPS(B);
    vec v = myEig.v();
    v.print();
    double lambda = myEig.lambda();
    cout << "lambda = " << lambda << endl;*/
    myEig.solveOneTermHermitian(B);
    myEig.v().print();
    PetscPrintf(PETSC_COMM_WORLD, "lambda = %f \n", myEig.lambda()) ;

    // testing the overloaded constructor: directly compute all elements
    eigenSolver Eig(B);
    PetscPrintf(PETSC_COMM_WORLD, "lambda = %f \n", Eig.eigenVals(0));
    Eig.eigenVecs(0).print();

    // free the memory:
    myEig.clear();
    Eig.clear();
    A.clear();
    B.clear();
    I.clear();

    SlepcFinalize();
    return 0;

}
