// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorOperations.hpp"
#include "CPTensorCompression.hpp"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing more linear algebra, solving 1D - laplacian.
    unsigned int nDof = 101;
    const double factor = (nDof-1);
    mat L(nDof, nDof, PETSC_COMM_WORLD);
    for(unsigned int i=1; i<nDof-1; i++){
      L(i,i-1) = -factor * factor;
      L(i,i) = 2.0 * factor * factor;
      L(i,i+1) = -factor * factor;
    }
    L(0,0) = 1.0;
    L(nDof-1, nDof-1) = 1.0;
    L.finalize();

    // right hand side: x(1-x);
    vec fun(nDof, PETSC_COMM_WORLD);
    for(unsigned int i=0; i<nDof; i++){
      double x_i = i * 1.0/(nDof-1);
      fun(i) = x_i * (1.0 - x_i);
    }
    fun.finalize();
    saveVec(fun, "rhs.dat");

    // testing the loading:
    vec check = loadVec("rhs.dat", PETSC_COMM_WORLD);

    if(check == fun){PetscPrintf(PETSC_COMM_WORLD, "Vector loaded correctly.\n");}

    // create a constant vector:
    vec rhs(nDof, PETSC_COMM_WORLD);
    rhs.ones();
    rhs(0) = 0.0;
    rhs(nDof-1) = 0.0;
    rhs.finalize();

    // Solving the laplacian;
    vec sol = L / rhs;

    // Solution must be 1/2 * fun, computing the error
    fun *= 0.5;
    fun.sum(sol, -1.0);
    double err = fun.norm();
    PetscPrintf(PETSC_COMM_WORLD, "The error norm is: %e.\n", err);

    // finilize:
    SlepcFinalize();
    return 0;

}
