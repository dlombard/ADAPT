// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorCompression.hpp"
#include "Tucker.h"
#include "TensorTrain.h"
#include "Node.h"
#include "HPFTucker.h"
#include "HPFTucker_parallel.h"
#include "TuckerCompression.hpp"
#include "HPFTuckerCompression.hpp"
#include "operatorTensor.h"
#include "conversions.hpp"
#include "HPFCP.h"
#include "multilinearSolver.h"

using namespace std;


// function to save 3D fields:
/* 4 -- saving field in VTK:
  - input: a 3d tensor
  - output: a vtk file
*/
void saveField(fullTensor& T, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = T.nDof_var(0);
 unsigned int ny = T.nDof_var(1);
 unsigned int nz = T.nDof_var(2);
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;
 for(unsigned int h=0; h<T.nEntries(); h++){
     double value = T.tensorEntries(h);
     if(fabs(value) < 1.0e-9){value = 0.0;};
     outfile << value << endl;
 }
 outfile.close();
}



// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);


    // Construct a full tensor:
    /* testing a gaussian, center around the diagonal
      - domain: unit cube
    */

    // parameters:
    const double alpha = 1.0; // singularity parameters of the covariance
    const double beta = 0.01;
    const double one_over_sigma2 = 500; // inverse of the squared convariance
    vector<unsigned int> dofPerDim = {16,16,16}; // resolution

    fullTensor T(dofPerDim, PETSC_COMM_WORLD);
    for(unsigned int iDof_x=0; iDof_x<dofPerDim[0]; iDof_x++){
      double x = (1.0*iDof_x)/(dofPerDim[0]-1);
      for(unsigned int iDof_y=0; iDof_y<dofPerDim[1]; iDof_y++){
        double y = (1.0*iDof_y)/(dofPerDim[1]-1);
        for(unsigned int iDof_z=0; iDof_z<dofPerDim[2]; iDof_z++){
          double z = (1.0*iDof_z)/(dofPerDim[2]-1);
          double argVal = (x-0.5)*(x-0.5) + (y-0.5)*(y-0.5) + (z-0.5)*(z-0.5);
          argVal *= alpha;
          double singPart = ((x-0.5)+(y-0.5)+(z-0.5)) * ((x-0.5)+(y-0.5)+(z-0.5));
          singPart *= (beta-alpha)/3.0;
          argVal += singPart;
          argVal *= one_over_sigma2;

          vector<unsigned int> ind = {iDof_x, iDof_y, iDof_z};
          T.set_tensorElement(ind, exp(-0.5*argVal));
        }
      }
    }
    T.finalize();
    string fieldName = "gaussianField";
    saveField(T, fieldName);


    // TOLERANCE:
    double tol = 1.0e-3;

    // testing classical HOSVD:
    TuckerCompression appT;
    appT.HOSVD(T);
    for(unsigned int iVar=0; iVar<dofPerDim.size(); iVar++){
      const unsigned int nOfSingVal = appT.HOSVD_sigmas(iVar).size();
      cout << "N of singular values in var " << iVar << ": " << nOfSingVal << "\n";
      for(unsigned int iS = 0; iS<nOfSingVal; iS++){
        cout << appT.HOSVD_sigmas(iVar, iS) << endl;
      }
      cout << endl << endl;
    }

    // Tucker rounding to get compression up to tolerance:
    /*Tucker app = appT.result();
    app.recompress(tol);
    cout << app.ranks(0) << " " << app.ranks(1) << " " << app.ranks(2) << endl;
    exit(1);*/

    cout << "HPF compression: \n\n";

    // compressing HPFTucker:
    unsigned int width = 2;
    Node* tree; tree = new Node(dofPerDim, width);
    //tree->print_leafsTable(); // check the leafs

    // Init the compression:
    HPFTuckerCompression ApproxT(T, tree, tol);
    ApproxT.set_verbosity(2);

    /*for(unsigned int iSub=0; iSub<ApproxT.subApprox().size(); iSub++){
      const unsigned int nOfS = ApproxT.subApprox(iSub).HOSVD_sigmas(0).size();
      cout << "Sub " << iSub << "  n of sigmas = " << nOfS << endl;
      if(nOfS>0){
        for(unsigned int iVar=0; iVar<3; iVar++){
          for(unsigned int iMod=0; iMod<nOfS; iMod++){
            cout << ApproxT.subApprox(iSub).HOSVD_sigmas(iVar,iMod) << endl;
          }
        }
      }
      cout << endl;
    }*/


    // Checking the greedy step:
    ApproxT.compute_greedy();

    cout << endl << "Greedy step:" << endl;
    cout << "Total error = " << ApproxT.totalError() << endl;
    cout << "Total memory = " << ApproxT.totalMemory() << endl;
    cout << endl;


    ApproxT.optimise_tree();

    ApproxT.assembleResult();

    // reconstruct the approximation and the error, save it:
    fullTensor approxRec(dofPerDim, PETSC_COMM_WORLD);
    fullTensor errorField(dofPerDim, PETSC_COMM_WORLD);
    for(unsigned int iDof_x=0; iDof_x<dofPerDim[0]; iDof_x++){
      for(unsigned int iDof_y=0; iDof_y<dofPerDim[1]; iDof_y++){
        for(unsigned int iDof_z=0; iDof_z<dofPerDim[2]; iDof_z++){
          double value = ApproxT.result().eval(iDof_x,iDof_y,iDof_z);
          approxRec(iDof_x,iDof_y,iDof_z) = value;
          errorField(iDof_x,iDof_y,iDof_z) = T(iDof_x,iDof_y,iDof_z) - value;
        }
      }
    }
    approxRec.finalize();
    errorField.finalize();

    string appName = "fieldApprox";
    saveField(approxRec, appName);

    string errName = "errApprox";
    saveField(errorField, errName);

    exit(1);


    /*
    TuckerCompression parentApprox;
    bool shouldIMerge = ApproxT.shouldMerge(parent, parentApprox);
    if(shouldIMerge){cout << "Yes, you should merge.\n";}

    // Tucker result for parent:
    parentApprox.result().print();
    cout << parentApprox.result().ranks(0) << " x " <<  parentApprox.result().ranks(1) << " x " << parentApprox.result().ranks(2) << endl;

    // replace the set of leafs in the tree by the parent.
    parent->removeChildren();
    tree_leafs = tree->getLeafs();
    tree->print_leafsTable();*/

    // free the memory:
    T.clear();
    ApproxT.clear();

    // finilize:
    SlepcFinalize();
    return 0;

}
