// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorCompression.hpp"
#include "Tucker.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);



    // fullTensor to be contracted by a matrix:
    fullTensor S({2,3,4}, PETSC_COMM_WORLD);
    S(0,0,0) = 1.0;
    S(1,1,1) = 1.0;
    S(1,2,2) = 1.0;
    S(0,0,3) = 5.0;
    S.finalize();

    mat A(1,4, PETSC_COMM_WORLD);
    A(0,0) = 2.0;
    A(0,1) = 3.0;
    A(0,2) = 4.0;
    A(0,3) = 5.0;
    A.finalize();

    mat testUnfold = S.computeUnfolding(2);
    testUnfold.print();

    fullTensor R = S.matTensProd(2, A);
    for(unsigned int l=0; l<R.nEntries(); l++){
      vector<unsigned int> ind = R.lin2sub(l);
      cout << ind[0] << " - " << ind[1] << " - " << ind[2] << " => " << R.tensorEntries(l) << endl;
    }


    // free the memory
    S.clear();
    R.clear();
    A.clear();

    // finilize:
    SlepcFinalize();
    return 0;

}
