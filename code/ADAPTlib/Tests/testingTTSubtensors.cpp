// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorCompression.hpp"
#include "Tucker.h"
#include "TensorTrain.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing the tensor train class:
    TensorTrain tt({3,4,2}, PETSC_COMM_WORLD);

    tt(0,0,0) = 2.0;
    tt(1,0,0) = 1.0;
    tt(1,1,1) = 5.0;

    // doing an operation on one of the entries:
    tt(0,0,0) = 2.0 * tt(1,0,0) + 1.0;

    // printing the size:
    tt.print_ttSize();

    cout << "Checking variadic operator ():\n";
    cout << tt(0,0,0) << endl;
    cout << tt(1,0,0) << endl;
    cout << tt(1,1,1) << endl;


    // extract sub-tensor:
    TensorTrain S = tt.extractSubtensor({0,2,0,2,0,2});
    S.print_ttSize();

    cout << "Checking subtensor:\n";
    cout << S(0,0,0) << endl;
    cout << S(1,0,0) << endl;
    cout << S(1,1,1) << endl;

    S *= 2.0;

    // re-assigning sub-tensor:
    tt.assignSubtensor({0,2,0,2,0,2}, S);
    tt.print_ttSize();

    cout << "Checking assignment:\n";
    cout << tt(0,0,0) << endl;
    cout << tt(1,0,0) << endl;
    cout << tt(1,1,1) << endl;


    // free the memory:
    tt.clear();
    S.clear();


    // finilize:
    SlepcFinalize();
    return 0;

}
