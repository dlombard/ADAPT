// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // testing the full tensor class:
    CPTensor T({3,4,2}, PETSC_COMM_WORLD);


    // setting the tensor elements:
    PetscPrintf(PETSC_COMM_WORLD, "linear index = %d \n",T.sub2lin({1,0,0}));

    //T.set_tensorElement({0,0,0}, 1.0);
    T(0,0,0) = 1.0;
    T.finalize();
    double p = T.eval(0,0,0);
    PetscPrintf(PETSC_COMM_WORLD, "p = %e \n", p);

    T(1,0,0) = 2.0;
    T(0,0,0) = T(1,0,0) + 3*T(0,0,0);

    p = T(0,0,0);
    PetscPrintf(PETSC_COMM_WORLD, "p = %e \n", p);




    // finilize:
    SlepcFinalize();
    return 0;

}
