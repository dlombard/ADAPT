// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorCompression.hpp"
#include "Tucker.h"
#include "TensorTrain.h"
#include "Node.h"
#include "HPFTucker.h"
#include "HPFTucker_parallel.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // space dimension
    unsigned int dim = 2;
    vector<unsigned int> resPerDim = {16,16};

    // testing the tensor train class:
    unsigned int width = 2;
    vector<vector<unsigned int> > indexTable(dim);
    for(unsigned int iVar=0; iVar<dim; iVar++){
      indexTable[iVar].resize(resPerDim[iVar]);
      for(unsigned int jDof=0; jDof<resPerDim[iVar]; jDof++){
        indexTable[iVar][jDof] = jDof;
      }
    }

    // create a tree:
    Node* tree;
    tree = new Node(indexTable);
    tree->createDyadicTree(width);

    // the leafs:
    vector<Node*> leafs = tree->getLeafs();
    for(unsigned int iLeaf=0; iLeaf< tree->nOfLeafs(); iLeaf++){
      vector<vector<unsigned int> > i_vec_ind = leafs[iLeaf]->indices();
      cout << "Printing the index table for Leaf: " << iLeaf << "  pointer: " << tree->getLeafs()[iLeaf] << endl;
      for(unsigned int iVar=0; iVar<dim; iVar++){
        for(unsigned int jDof=0; jDof<i_vec_ind[iVar].size(); jDof++){
          cout << i_vec_ind[iVar][jDof] << "  ";
        }
        cout << endl;
      }
      cout << endl;
    }

    // checking in which leaf there is a given index:
    vector<unsigned int> ind = {2,5};
    vector<unsigned int> ind_shifted;
    Node* theLeaf;

    tree->whichLeaf(ind, theLeaf, ind_shifted);
    cout << "Pointer = " << theLeaf << endl;
    cout << "Id Leaf = " << tree->findIdLeaf(theLeaf) << endl;
    cout << ind_shifted[0] << " " << ind_shifted[1] << "\n";

    // Creation of HPF:
    HPFTucker T({8,16,4}, width, PETSC_COMM_WORLD);

    // filling the tensor:
    T(0,0,0) = 1.0;
    T(5,3,1) = 2.0;

    // Checking:
    cout << "T(0,0,0) = " << T(0,0,0) << endl;
    cout << "T(5,3,1) = " << T(5,3,1) << endl;

    // operation:
    T(0,0,0) = 2.5 * T(5,3,1) - 1.0;

    cout << "T(0,0,0) = " << T(0,0,0) << endl;

    // copy the tensor T:
    HPFTucker S;
    S.copyTensorFrom(T);
    // printing the leafs index table:
    S.print_leafsTable();

    cout << "S(0,0,0) = " << S(0,0,0) << endl;

    if(T == S){cout << "Indeed, they are equal...\n";}

    // summing up:
    // S.sum(T, 1.0, true);
    S += T;
    cout << "S(0,0,0) = " << S(0,0,0) << endl;

    // multiply by scalar:
    S *= 0.25;
    cout << "S(0,0,0) = " << S(0,0,0) << endl;

    // shift by scalar:
    S += 3.0;
    cout << "S(0,0,0) = " << S(0,0,0) << endl;

    // finilize:
    SlepcFinalize();
    return 0;

}
