// Parallel tensor implementation
#include "genericInclude.h"
#include "linAlg/linAlg.h"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


    // define a set of vecs:
    vec v1(4, PETSC_COMM_WORLD);
    v1(0) = 1.0;
    v1(1) = 1.0;
    v1.finalize();
    vec v2(4, PETSC_COMM_WORLD);
    v2(1) = 1.0;
    v2(2) = 2.0;
    v2.finalize();
    vec v3(4, PETSC_COMM_WORLD);
    v3(2) = 3.0;
    v3(3) = 4.0;
    v3.finalize();

    // computing the svd of a vector set:
    svd vecSvd({v1,v2,v3});
    vecSvd.Umat().print();
    vecSvd.Svec().print();


    // using the matrix version:
    mat A(4, 3, PETSC_COMM_WORLD);
    A(0,0) = 1.0;
    A(1,0) = 1.0;
    A(1,1) = 1.0;
    A(2,1) = 2.0;
    A(2,2) = 3.0;
    A(3,2) = 4.0;
    A.finalize();

    // computing the SVD;
    svd mySvd(A);
    mySvd.Umat().print();
    mySvd.Svec().print();
    mySvd.Vmat().print();

    // computing the first triplet:
    svd testFirst;
    testFirst.oneSVDTerm(A);
    PetscPrintf(PETSC_COMM_WORLD, "The first singular value is %f \n", testFirst.sigma());
    testFirst.u().print();


    // free the memory:
    mySvd.clear();
    vecSvd.clear();
    testFirst.clear();
    A.clear();

    SlepcFinalize();
    return 0;

}
