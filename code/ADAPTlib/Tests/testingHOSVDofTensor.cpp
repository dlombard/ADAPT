// Parallel tensor implementation
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"
#include "eigenSolver.h"
#include "orthogonalization.h"
#include "svd.h"
#include "tensor.h"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "sparseTensorOperations.hpp"
#include "CPTensor.h"
#include "CPTensorCompression.hpp"
#include "Tucker.h"
#include "TensorTrain.h"
#include "Node.h"
#include "HPFTucker.h"
#include "HPFTucker_parallel.h"
#include "TuckerCompression.hpp"

using namespace std;





// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // tree width:
    const unsigned int width = 2;

    // Construct a full tensor:
    fullTensor T({3,4,2}, PETSC_COMM_WORLD);
    T(0,0,0) = 1.0;
    T(1,0,0) = 2.0;
    T.finalize();

    // polymorphism:
    tensor* target = &T;

    // Compute its HOSVD:
    TuckerCompression Approx;

    // function that produces a Tucker approximation of a fullTensor:
    Tucker tildeT = Approx.HOSVD(T);

    // Generic computation of HOSVD:
    Approx.HOSVD(*target);

    cout << "Number of modes:\n";
    cout << Approx.HOSVD_modes(0).size() << endl;
    cout << Approx.HOSVD_modes(1).size() << endl;
    cout << Approx.HOSVD_modes(2).size() << endl;

    cout << endl;
    cout << "Comparing modes:\n";
    cout << "tildeT, Mode 0-0:\n";
    tildeT.modes(0,0).print();
    cout << "HOSVD table, Mode 0-0:\n";
    Approx.HOSVD_modes(0,0).print();


    // finilize:
    SlepcFinalize();
    return 0;

}
