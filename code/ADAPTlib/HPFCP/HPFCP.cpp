// Implementation of the class HPFCP

#include "HPFCP.h"


// -- CONSTRUCTORS --

/* constructor:
  - input: the degrees of freedom per direction, the communicator
  - output: tensor is init
*/
HPFCP::HPFCP(vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = dofPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  compute_minc();
}


/* constructor:
  - input: number of variables, the degrees of freedom per direction, the communicator
  - output: tensor is init
*/
HPFCP::HPFCP(unsigned int dim, vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = dim;
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  compute_minc();
}


/* constructor:
  - input:  the degrees of freedom per direction, the tree width, the communicator
  - output: tensor is init, uniform dyadic tree
*/
HPFCP::HPFCP(vector<unsigned int> dofPerDim, unsigned int width, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = dofPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  compute_minc();
  // creating the Node:
  vector<vector<unsigned int> > indexTable(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    indexTable[iVar].resize(m_nDof_var[iVar]);
    for(unsigned int jDof=0; jDof<m_nDof_var[iVar]; jDof++){
      indexTable[iVar][jDof] = jDof;
    }
  }
  m_node = new Node(indexTable);
  m_node->createDyadicTree(width);
  m_leafs = m_node->getLeafs();
  m_nOfSub = m_leafs.size();
  m_subTensors.resize(m_nOfSub);
  for(unsigned int iLeaf=0; iLeaf<m_nOfSub; iLeaf++){
    vector<vector<unsigned int> > vec_iSub = m_leafs[iLeaf]->indices();
    vector<unsigned int> resPerDim_iSub(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      resPerDim_iSub[iVar] = vec_iSub[iVar].size();
    }
    // init the Tucker Compression:
    m_subTensors[iLeaf].init(resPerDim_iSub, m_comm);
  }
}


/* constructor:
  - input:  a node, the set of CP tensors
  - output: tensor is init
*/
HPFCP::HPFCP(vector<unsigned int> dofPerDim, Node*& tree, vector<CPTensor>& subTens){
  m_nVar = tree->nVar();
  assert(m_nVar == subTens[0].nVar());
  assert(dofPerDim.size() == m_nVar);
  m_comm = subTens[0].comm();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  compute_minc();
  m_node = tree;
  m_subTensors = subTens;
}


/* constructor: init when empty object is used
  - input: the degrees of freedom per direction, the communicator
  - output: tensor is init
*/
void HPFCP::init(vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = dofPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  compute_minc();
}


// -- METHODS implementing generic operations on HPFCP --


/* Copy the tensor structure:
  - input: the HPFCP to be copied
  - output: the structure of this tensor is copied.
*/
void HPFCP::copyTensorStructFrom(HPFCP& source){
  if(m_isInitialised){clear();}

  m_comm = source.comm();
  m_nVar = source.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = source.nDof_var(iVar);
  }
  compute_minc();
}


/* Copy the tensor from a given HPFCP:
  - input: the tensor to be copied
  - output: the tensor (structure + copy of the entries)
*/
void HPFCP::copyTensorFrom(HPFCP& source){
  copyTensorStructFrom(source);

  m_node = source.node()->createNodeCopy();
  m_leafs = m_node->getLeafs();
  m_nOfSub = source.subTensors().size();
  m_subTensors.resize(m_nOfSub);
  for(unsigned int iSub = 0; iSub<m_nOfSub; iSub++){
    CPTensor subTens = source.subTensors(iSub);
    unsigned int subRank = subTens.rank();
    bool isItEmpty = false;
    if(subRank == 0){
      isItEmpty = true;
    }
    if(!isItEmpty){
      m_subTensors[iSub].copyTensorFrom(subTens);
    }
    else{
      m_subTensors[iSub].copyTensorStructFrom(subTens);
    }
  }
}


/* check if this tensor has the same structure of a given tensor
  - input: the tensor to compare with
  - output: true if if has the same structure, false otherwise
*/
bool HPFCP::hasSameStructure(HPFCP& toBeComparedTo){
    bool toBeReturned = true;
    if(m_nVar != toBeComparedTo.nVar()){
      return false;
    }
    else{
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        if(m_nDof_var[idVar] != toBeComparedTo.nDof_var(idVar)){
          return false;
        }
      }
    }
    return toBeReturned;
}


/* check if this HPFCP is equal to a given one:
  - input: a HPFCP to be compared
  - output: a bool
*/
bool HPFCP::isEqualTo(HPFCP& source){
  bool isIt = hasSameStructure(source);
  if(isIt){
    if(m_nOfSub != source.nOfSub()){
      isIt = false;
      if(isIt){
        for(unsigned int iSub = 0; iSub<m_nOfSub; iSub++){
          bool isThisEqual = source.subTensors(iSub).isEqualTo(m_subTensors[iSub]);
          if(!isThisEqual){
            isIt = false;
          }
        }
      }
    }
  }
  return isIt;
}

// overloaded operator:
bool HPFCP::operator == (HPFCP& source){
  return isEqualTo(source);
}


/* sum to the current HPFCP another one
- input: the HPFCP to be added, rescaled by alpha
- output: T <-- T + alpha S
*/
void HPFCP::sum(HPFCP& toBeAdded, double alpha, bool COPY){
  if(hasSameStructure(toBeAdded)){
    // if they have the same partitioning: use CP
    if(hasSamePartition(toBeAdded)){
      for(unsigned int iSub=0; iSub<m_nOfSub; iSub++){
        CPTensor subToAdd = toBeAdded.subTensors(iSub);
        bool isItEmpty = false;
        unsigned int subRank = subToAdd.rank();
        if(subRank== 0){
          isItEmpty = true;
        }
        if(!isItEmpty){
          m_subTensors[iSub].sum(subToAdd, alpha, COPY);
        }
      }
    }
    else{
      for(unsigned int iSub=0; iSub<m_nOfSub; iSub++){
        unsigned int nDofSub = 1;
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          nDofSub = nDofSub * m_subTensors[iSub].nDof_var(iVar);
        }
        vector<vector<unsigned int> > indGlob = m_leafs[iSub]->indices();
        vector<vector<unsigned int> > indLoc(m_nVar);
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          indLoc.resize(indLoc[iVar].size());
          for(unsigned int jDof=0; jDof<indLoc[iVar].size(); jDof++){
            indLoc[iVar][jDof] = indGlob[iVar][jDof] - indGlob[iVar][0];
          }
        }
        for(unsigned int l=0; l<nDofSub; l++){
          vector<unsigned int> ind = m_subTensors[iSub].lin2sub(l);
          vector<unsigned int> glo(m_nVar);
          for(unsigned int iVar=0; iVar<m_nVar; iVar++){
            glo[iVar] = ind[iVar] + indGlob[iVar][0];
          }
          double thisVal = eval(ind);
          double toSum = toBeAdded.eval(glo);
          thisVal += alpha * toSum;
          m_subTensors[iSub].set_tensorElement(ind, thisVal);
        }
      }
    }
  }
  else{
    PetscPrintf(m_comm, "Cannot be summed!\n");
    exit(1);
  }
}


// overloaded to sum with coefficient 1:
void HPFCP::operator += (HPFCP& toBeAdded){
  sum(toBeAdded, 1.0, true);
}


/* multiply the given HPFCP by a scalar:
- input: the scalar
- output: this HPFCP is multiplied by the given scalar
*/
void HPFCP::multiplyByScalar(double alpha){
  for(unsigned int iSub=0; iSub<m_nOfSub; iSub++){
    if(!isEmpty(iSub)){
      m_subTensors[iSub] *= alpha;
    }
  }
}

// overloaded to multiply by a scalar:
void HPFCP::operator *= (double alpha){
  for(unsigned int iSub=0; iSub<m_nOfSub; iSub++){
    if(!isEmpty(iSub)){
      m_subTensors[iSub] *= alpha;
    }
  }
}


/* shift the whole tensor by a scalar
 - input: scalar
 - output: tensor is shifted
*/
void HPFCP::shiftByScalar(double alpha){
  for(unsigned int iSub=0; iSub<m_nOfSub; iSub++){
      m_subTensors[iSub].shiftByScalar(alpha);
  }
}

// overloaded operator:
void HPFCP::operator += (double alpha){
  for(unsigned int iSub=0; iSub<m_nOfSub; iSub++){
    m_subTensors[iSub].shiftByScalar(alpha);
  }
}


/* Extract a subtensor, contiguous indices:
 - input: the indices bounds, C convention [lower, upper)_1,..., [lower, upper)_d
 - output: a given empty pointer is filled with a subtensor.
REMARK: for this format, the output has the same size, and it is zero outside the requested subtensor
*/
void HPFCP::extractSubtensor(vector<unsigned int> indBounds, HPFCP& subTens){
  assert(indBounds.size() == 2*m_nVar);
  subTens.init(m_nDof_var, m_comm);
  Node* subTensNode = m_node->createNodeCopy();
  subTens.set_node(subTensNode);
  // the vector of Tucker tensors:
  vector<CPTensor> subTensVec(m_nOfSub);
  for(unsigned int iSub=0; iSub<m_nOfSub; iSub++){
    subTensVec[iSub].init(m_subTensors[iSub].nDof_var(), m_subTensors[iSub].comm());
    // check if there is an intersection of the indices
    vector<vector<unsigned int> > subIndTable = m_leafs[iSub]->indices();
    vector<unsigned int> globIndBounds(2*m_nVar);
    bool isThereIntersection = true;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      globIndBounds[2*iVar] = subIndTable[iVar][0];
      globIndBounds[2*iVar+1] = subIndTable[iVar][1];
      if( (globIndBounds[2*iVar+1]-1<indBounds[2*iVar]) || (globIndBounds[2*iVar]>=indBounds[2*iVar+1]) ){
        isThereIntersection = false;
        break;
      }
    }
    // if there is intersection extract the subtensor:
    if(isThereIntersection){
      vector<unsigned int> interBounds(2*m_nVar);
      vector<unsigned int> interLocBounds(2*m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        interBounds[2*iVar] = globIndBounds[2*iVar] > indBounds[2*iVar] ? globIndBounds[2*iVar] : indBounds[2*iVar];
        interBounds[2*iVar+1] = globIndBounds[2*iVar+1] < indBounds[2*iVar + 1] ? globIndBounds[2*iVar+1] : indBounds[2*iVar+1];
        // local indices:
        interLocBounds[2*iVar] = interBounds[2*iVar] - subIndTable[iVar][0];
        interLocBounds[2*iVar+1] = interBounds[2*iVar+1] - subIndTable[iVar][0];
      }
      // assigning the subtensor:
      CPTensor localSub = m_subTensors[iSub].extractSubtensor(interLocBounds);
      subTensVec[iSub].assignSubtensor(interLocBounds, localSub);
    }

  }
  subTens.set_subTensors(subTensVec);
}



// -- II -- METHODS SPECIFIC TO HPFCP:

/* check if the subdomain partitioning is the same (same order also):
- input: an HPFCP
- output: a bool
*/
bool HPFCP::hasSamePartition(HPFCP& source){
  bool haveIt = (m_nOfSub == source.nOfSub());
  if(haveIt){
    vector<Node*> thisLeafs = m_node->getLeafs();
    vector<Node*> sourceLeafs = source.node()->getLeafs();
    for(unsigned int iSub = 0; iSub<m_nOfSub; iSub++){
      vector<vector<unsigned int> > thisInd = thisLeafs[iSub]->indices();
      vector<vector<unsigned int> > sourceInd = sourceLeafs[iSub]->indices();
      bool sameSize = true;
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        if(thisInd[iVar].size()!=sourceInd[iVar].size()){
          sameSize = false;
          haveIt = false;
          break;
        }
      }
      if(sameSize){
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          for(unsigned int jDof=0; jDof<thisInd[iVar].size(); jDof++){
            if(thisInd[iVar][jDof] != sourceInd[iVar][jDof]){
              haveIt = false;
              break;
            }
          }
        }
      }
      else{
        haveIt = false;
        break;
      }
    }
  }
  return haveIt;
}
