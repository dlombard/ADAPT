// Header for the CPTensor2Var class

#ifndef CPTensor2Var_hpp
#define CPTensor2Var_hpp

#include "../linAlg/vec.h"
#include "../linAlg/mat.h"
#include "../linAlg/linearAlgebraOperations.h"
#include "../linAlg/svd.h"

class CPTensor2Var {

private:

  unsigned int m_rank;
  unsigned int m_nDof_x;
  unsigned int m_nDof_y;
  std::vector<double> m_coeffs;
  std::vector<vec> m_terms_x;
  std::vector<vec> m_terms_y;
  MPI_Comm m_comm;

  static inline bool check_terms(const unsigned int rank, const unsigned int nDof, const std::vector<vec>& terms);


public:

  CPTensor2Var();
  CPTensor2Var(const unsigned int nDof_x, const unsigned int nDof_y, const MPI_Comm& comm=PETSC_COMM_WORLD);
  CPTensor2Var(const unsigned int nDof_x, const unsigned int nDof_y, const std::vector<double>& coeffs, const std::vector<vec>& terms_x, const std::vector<vec>& terms_y, const MPI_Comm& comm=PETSC_COMM_WORLD);
  CPTensor2Var(const CPTensor2Var& tens);
  ~CPTensor2Var();

  void init(const unsigned int nDof_x, const unsigned int nDof_y, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const unsigned int nDof_x, const unsigned int nDof_y, const std::vector<double>& coeffs, const std::vector<vec>& terms_x, const std::vector<vec>& terms_y, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const CPTensor2Var& tens);
  void clear();

  inline const unsigned int rank() const;
  inline const unsigned int nDof_x() const;
  inline const unsigned int nDof_y() const;
  inline const double coeffs(const unsigned int iTerm) const;
  inline const std::vector<double>& coeffs() const;
  inline const vec& terms_x(const unsigned int iTerm) const;
  inline const std::vector<vec>& terms_x() const;
  inline const vec& terms_y(const unsigned int iTerm) const;
  inline const std::vector<vec>& terms_y() const;
  inline const MPI_Comm& comm() const;

  void rank(const unsigned int rank);
  void coeffs(const unsigned int iTerm, const double theCoeffs);
  void coeffs(const std::vector<double>& coeffs);
  void terms_x(const unsigned int iTerm, const vec& theTerms);
  void terms_x(const std::vector<vec>& terms_x);
  void terms_y(const unsigned int iTerm, const vec& theTerms);
  void terms_y(const std::vector<vec>& terms_y);

  vec eval(const unsigned int iTerm) const;
  std::vector<vec> eval() const;
  CPTensor2Var extract_subtensor(const unsigned int iLow, const unsigned int iUp) const;
  void add(const CPTensor2Var& tens, const double scale=1.0);
  void truncate(const double tol);
  void solve(const KSP& ksp);
  void print_coeffs() const;
  void print_terms_x() const;
  void print_terms_y() const;
  void print(const unsigned int iTerm) const;
  void print() const;
  void save_coeffs(const std::string& filename) const;
  void save_terms_x(const std::string& filename) const;
  void save_terms_y(const std::string& filename) const;
  void save(const std::string& filename, const unsigned int iTerm) const;
  void save(const std::string& filename) const;
  
  double operator () (const unsigned int ind_x, const unsigned int ind_y) const;
  CPTensor2Var& operator += (const CPTensor2Var& tens);
  CPTensor2Var& operator -= (const CPTensor2Var& tens);  
  CPTensor2Var& operator *= (const double scale);  
  CPTensor2Var& operator /= (const double scale); 
  CPTensor2Var& operator = (const CPTensor2Var& tens);

  friend double dot(const CPTensor2Var& tens1, const CPTensor2Var& tens2);
  friend double norm(const CPTensor2Var& tens);

};

#include "CPTensor2Var.tpp"

#endif
