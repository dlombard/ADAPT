// Header file for CPTensor operations:

#ifndef CPTensorOperations_hpp
#define CPTensorOperations_hpp

#include "CPTensor.h"

/* Left Kronecker product of tensors
  - input: tensors A and B
  - output: a tensor which is the Kronecker product of the given tensors
*/
CPTensor leftKronecker(CPTensor& A, CPTensor& B);


/* Right Kronecker product of tensors
  - input: tensors A and B
  - output: this tensor (empty) is the right Kronecker product of the tensors
*/
CPTensor rightKronecker(CPTensor& A, CPTensor& B);


/* mode I Khatri-Rao product of two given full Tensors.
  - input: the full tensors A and B, the mode I
  - output: full tensor which is the mode I Khatri-Rao product of A and B
*/
CPTensor modeIKhatriRao(unsigned int modeI, CPTensor& A, CPTensor& B);



/* mode I concatenation of two full order tensors
  - input: tensor A and tensor B in full tensor format
  - output: this tensor is the concatenation
*/
CPTensor modeIConcatenate(unsigned int modeI, CPTensor& A, CPTensor& B);


/* outer Product of two given tensors A and B
  - input: the tensors A and B (full tensors)
  - output: the tensor which is the outer product of the two
*/
CPTensor outerProduct(CPTensor& A, CPTensor& B);


/* mode I contraction of a tensor and a given vector
  - input: the vector b, an empty result tensor
  - output: this tensor contracted by b to provide result
*/
CPTensor modeIContraction(unsigned int modeI, CPTensor& A, vec& b);


/* operator +, to sum two Tensors
  - input: tensor A and B
  - output A + B
*/
CPTensor operator + (CPTensor& A, CPTensor& B);


/* scalar Product of Full Tensors:
  - input: two Full tensors
  - output: A:B
*/
double scalProd(CPTensor& A, CPTensor& B);


/* scalar Product of terms:
  - input: two terms
  - output: A:B
*/
double scalarProdTerms(vector<vec>& A, vector<vec>& B, vector<mat>& mass);

// overloaded without vector of mass matrix
double scalarProdTerms(vector<vec>& A, vector<vec>& B);


// I/O operations
CPTensor loadCP(string, MPI_Comm);


#endif
