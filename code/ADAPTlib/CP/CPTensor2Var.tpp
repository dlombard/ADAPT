/* 
*  Getters/Setters
*/

inline const unsigned int CPTensor2Var::rank() const {
  return m_rank;
}

inline const unsigned int CPTensor2Var::nDof_x() const {
  return m_nDof_x;
}

inline const unsigned int CPTensor2Var::nDof_y() const {
  return m_nDof_y;
}

inline const double CPTensor2Var::coeffs(const unsigned int iTerm) const {
  assert(0<=iTerm && iTerm<m_rank);
  return m_coeffs[iTerm];
}

inline const std::vector<double>& CPTensor2Var::coeffs() const {
  return m_coeffs;
}

inline const vec& CPTensor2Var::terms_x(const unsigned int iTerm) const {
  assert(0<=iTerm && iTerm<m_rank);
  return m_terms_x[iTerm];
}

inline const std::vector<vec>& CPTensor2Var::terms_x() const {
  return m_terms_x;
}

inline const vec& CPTensor2Var::terms_y(const unsigned int iTerm) const {
  assert(0<=iTerm && iTerm<m_rank);
  return m_terms_y[iTerm];
}

inline const std::vector<vec>& CPTensor2Var::terms_y() const {
  return m_terms_y;
}

inline const MPI_Comm& CPTensor2Var::comm() const {
  return m_comm;
}


/* 
*  Static methods
*/

inline bool CPTensor2Var::check_terms(const unsigned int rank, const unsigned int nDof, const std::vector<vec>& terms) {
  if (terms.size()!=rank) {
    return false;
  }
  for (unsigned int iTerm=0; iTerm<rank; ++iTerm) {
    if (terms[iTerm].size()!=nDof) {
      return false;
    }
  }
  return true;
}