// Implementation of the CPTensor class:

#include "CPTensor.h"
#include "../linAlg/linearAlgebraOperations.h"
#include "../linAlg/svd.h"
#include "../linAlg/eigenSolver.h"


/* Initialising an empty CP
  - inputs: dimension, degrees of freedom per direction
  - output: this (empty) CP is initialised with m_rank = 0
*/
CPTensor::CPTensor(unsigned int dim, vector<unsigned int> nDofPerVar, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = dim;
  m_nDof_var.resize(dim);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = nDofPerVar[iVar];
  }
  m_rank = 0;
  compute_minc();
  m_isInitialised = true;
}


/* Initialising an empty CP
  - inputs: degrees of freedom per direction
  - output: this (empty) CP is initialised with m_rank = 0
*/
CPTensor::CPTensor(vector<unsigned int> nDofPerVar, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = nDofPerVar.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = nDofPerVar[iVar];
  }
  m_rank = 0;
  compute_minc();
  m_isInitialised = true;
}


/* Initialising a CP
  - inputs: the Terms, the coeffs
  - output: this  CP is initialised
*/
CPTensor::CPTensor(vector<vector<vec> >& theTerms, vector<double>& theCoeffs, MPI_Comm theComm){
  m_comm = theComm;
  m_rank = theTerms.size();
  m_nVar = theTerms[0].size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    m_nDof_var[idVar] = theTerms[0][idVar].size();
  }
  m_terms.resize(m_rank);
  m_coeffs.resize(m_rank);
  assert(theCoeffs.size() == m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_terms[iTerm].resize(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      (m_terms[iTerm])[idVar] = (theTerms[iTerm])[idVar];
    }
    m_coeffs[iTerm] = theCoeffs[iTerm];
  };
  compute_minc();
  m_isInitialised = true;
}


/* Initialising a CP
  - inputs: the Terms
  - output: this  CP is initialised with coeffs = 1!
*/
CPTensor::CPTensor(vector<vector<vec> >& theTerms, MPI_Comm theComm){
  m_comm = theComm;
  m_rank = theTerms.size();
  m_nVar = theTerms[0].size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    m_nDof_var[idVar] = theTerms[0][idVar].size();
  }
  m_terms.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_terms[iTerm].resize(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      (m_terms[iTerm])[idVar] = (theTerms[iTerm])[idVar];
    }
  };
  m_coeffs.resize(m_rank);
  for(unsigned int idTerm=0; idTerm<m_rank; idTerm++){
    m_coeffs[idTerm] = 1.0;
  }
  compute_minc();
  m_isInitialised = true;
}


/* Initialising a one term CP (pure tensor product)
  - inputs: one single term, a pure tensor product, coefficient
  - output: this  CP is initialised,
*/
CPTensor::CPTensor(vector<vec>& theTerm, double coeff, MPI_Comm theComm){
  m_comm = theComm;
  m_rank = 1;
  m_nVar = theTerm.size();
  m_nDof_var.resize(m_nVar);
  m_terms.resize(m_rank);
  m_terms[0].resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    m_nDof_var[idVar] = theTerm[idVar].size();
    (m_terms[0])[idVar] = theTerm[idVar];
  }
  m_coeffs.resize(1);
  m_coeffs[0] = coeff;
  compute_minc();
  m_isInitialised = true;
}


/* Initialising a one term CP (pure tensor product)
  - inputs: one single term, a pure tensor product, coefficient = 1!
  - output: this  CP is initialised,
*/
CPTensor::CPTensor(vector<vec>& theTerm, MPI_Comm theComm){
  m_comm = theComm;
  m_rank = 1;
  m_nVar = theTerm.size();
  m_nDof_var.resize(m_nVar);
  m_terms.resize(m_rank);
  m_terms[0].resize(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    m_nDof_var[idVar] = theTerm[idVar].size();
    (m_terms[0])[idVar] = theTerm[idVar];
  }
  m_coeffs.resize(1);
  m_coeffs[0] = 1.0;
  compute_minc();
  m_isInitialised = true;
}


/* Initialising an empty CP
  - inputs: degrees of freedom per direction
  - output: this (empty) CP is initialised with m_rank = 0
*/
void CPTensor::init(vector<unsigned int> nDofPerVar, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = nDofPerVar.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = nDofPerVar[iVar];
  }
  m_rank = 0;
  compute_minc();
  m_isInitialised = true;
}


/*  copy tensor structure from a given tensor:
  - input: the tensor whose structure is copied
  - output: the structure of this tensor is shaped
*/
void CPTensor::copyTensorStructFrom(CPTensor& source){

  if(m_isInitialised){clear();}

  m_comm = source.comm();
  m_nVar = source.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = source.nDof_var(iVar);
  }

  compute_minc();
  m_isInitialised = true;
}


/* Copy the tensor from a given fullTensor:
  - input: the tensor to be copied
  - output: the tensor (structure + copy of the entries)
*/
void CPTensor::copyTensorFrom(CPTensor& source){
  copyTensorStructFrom(source);
  m_rank = source.rank();
  m_terms.resize(m_rank);
  m_coeffs.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_coeffs[iTerm] = source.coeffs(iTerm);
    vector<vec> thisTerm(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      vec termToCopy = source.terms(iTerm,iVar);
      thisTerm[iVar].copyVecFrom(termToCopy);
    }
    m_terms[iTerm] = thisTerm;
  }
  finalize();
}


/* check if this tensor has the same structure of a given full tensor
  - input: the tensor to compare with
  - output: true if if has the same structure, false otherwise
*/

bool CPTensor::hasSameStructure(CPTensor& toBeComparedTo){
    bool toBeReturned = true;
    if(m_nVar != toBeComparedTo.nVar()){
      return false;
    }
    else{
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        if(m_nDof_var[idVar] != toBeComparedTo.nDof_var(idVar)){
          return false;
        }
      }
    }
    return toBeReturned;
}


/* check if this tensor is equal to a given CP tensor
  - input: the tensor to compare with
  - output: true if it is equal, false otherwise
*/
bool CPTensor::isEqualTo(CPTensor& toBeCompared){
    bool toBeReturned = hasSameStructure(toBeCompared);
    if(toBeReturned){
      if(m_rank == toBeCompared.rank()){
        for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
          for(unsigned int iVar=0; iVar<m_nVar; iVar++){
            if(! (m_terms[iTerm][iVar] == toBeCompared.terms(iTerm,iVar)) ){
              toBeReturned = false;
              break;
            }
          }
        }
      }
      else{
        toBeReturned = false;
      }
    }
    return toBeReturned;
}


/* check if this tensor is equal a given full tensor (overloaded operator)
  - input: the tensor to compare with
  - output: true if it is equal, false otherwise
*/
bool CPTensor::operator == (CPTensor& toBeCompared){
  bool isItEqual = isEqualTo(toBeCompared);
  return isItEqual;
}


/*  sum another CP, rescaled
  - inputs: CP tensor to be added, the scaling
  - output: this CP is added to the current CP tensor
*/
void CPTensor::sum(CPTensor& toAdd, double scale, bool COPY=false){
  assert(m_nVar == toAdd.nVar());
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    assert(m_nDof_var[idVar] == toAdd.nDof_var(idVar));
  }
  unsigned int currentRank = m_rank;
  m_rank = m_rank + toAdd.rank();
  m_terms.resize(m_rank);
  m_coeffs.resize(m_rank);
  for(unsigned int iTerm=currentRank; iTerm<m_rank; iTerm++){
    m_terms[iTerm].resize(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(!COPY){
        (m_terms[iTerm])[idVar] = toAdd.terms(iTerm-currentRank,idVar);
      }
      else{
        vec thisTerm;
        thisTerm.copyVecFrom(toAdd.terms(iTerm-currentRank, idVar));
        (m_terms[iTerm])[idVar] = thisTerm;
      }
    }
    m_coeffs[iTerm] = scale * toAdd.coeffs(iTerm-currentRank);
  }
}


/*  sum another CP
  - inputs: CP tensor to be added
  - output: this CP is added to the current CP tensor (and copy of the terms is made)
*/
void CPTensor::operator += (CPTensor& toBeAdded){
  sum(toBeAdded, 1.0, true);
  finalize();
}


/* multiply by a scalar
  - input: the scalar
  - output: the coefficients are multiplied by the scalar;
*/
void CPTensor::multiplyByScalar(double val){
  for(unsigned int idTerm=0; idTerm<m_rank; idTerm++){
    m_coeffs[idTerm] = m_coeffs[idTerm] * val;
  }
  finalize();
}


/* operator to multiply by a scalar
- input: the scalar
- output: the coefficients are multiplied by the scalar;
*/
void CPTensor::operator *= (double val){
  multiplyByScalar(val);
}


/* shift by a scalar
  - input: the scalar
  - output T <- T + scalar
*/
void CPTensor::shiftByScalar(double val){
  vector<vec> termToAdd(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    termToAdd[iVar].init(m_nDof_var[iVar], m_comm);
    termToAdd[iVar].ones();
    termToAdd[iVar].finalize();
  }
  addPureTensorTerm(termToAdd, val);
  finalize();
}


/* shift by a scalar (overloaded operator)
  - input: the scalar
  - output T <- T + scalar
*/
void CPTensor::operator += (double val){
  shiftByScalar(val);
}


/*  extract subtensor and put it into a given CP
  - input: the indices bounds (supposed to be contiguous)
  - output: the subtensor
*/
void CPTensor::extractSubtensor(vector<unsigned int> indicesBounds, CPTensor& target){
  assert(indicesBounds.size()==2*m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    resPerDim[idVar] = indicesBounds[2*idVar+1] - indicesBounds[2*idVar];
  }
  target.init(resPerDim, m_comm);
  target.set_rank(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    vector<vec> subtensTerm(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      vec iVterm(resPerDim[iVar], m_comm);
      for(unsigned int jDof=0; jDof<resPerDim[iVar]; jDof++){
        unsigned int globalInd = jDof + indicesBounds[2*iVar];
        double value = m_terms[iTerm][iVar].getVecEl(globalInd);
        iVterm.setVecEl(jDof, value);
      }
      iVterm.finalize();
      subtensTerm[iVar] = iVterm;
    }
    target.set_terms(iTerm, subtensTerm);
  }
  target.set_coeffs(m_coeffs);
  target.finalize();
};


/*  extract subtensor and put it into a given CP (overloaded)
  - input: the indices bounds (supposed to be contiguous)
  - output: the subtensor
*/
CPTensor CPTensor::extractSubtensor(vector<unsigned int> indicesBounds){
  assert(indicesBounds.size()==2*m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    resPerDim[idVar] = indicesBounds[2*idVar+1] - indicesBounds[2*idVar];
  }
  CPTensor target(resPerDim, m_comm);
  target.set_rank(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    vector<vec> subtensTerm(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      vec iVterm(resPerDim[iVar], m_comm);
      for(unsigned int jDof=0; jDof<resPerDim[iVar]; jDof++){
        unsigned int globalInd = jDof + indicesBounds[2*iVar];
        double value = m_terms[iTerm][iVar].getVecEl(globalInd);
        iVterm.setVecEl(jDof, value);
      }
      iVterm.finalize();
      subtensTerm[iVar] = iVterm;
    }
    target.set_terms(iTerm, subtensTerm);
  }
  target.set_coeffs(m_coeffs);
  target.finalize();
  return target;
};


/*  extract subtensor and put it into a given CP (overloaded)
  - input: the indices list (NON CONTIGUOUS!)
  - output: the subtensor
  - REAMRK: outer index => iVar, inner index, the list for iVar (!!!)
    this is different than what is done for sparse and full tensor (due to the format)
*/
void CPTensor::extractSubtensor(vector<vector<unsigned int> > listOfIndices, CPTensor& target){
  assert(listOfIndices.size() == m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    resPerDim[iVar] = listOfIndices[iVar].size();
  }
  target.init(resPerDim, m_comm);
  target.set_rank(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    vector<vec> subtensTerm(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      vec iVterm(resPerDim[iVar], m_comm);
      for(unsigned int jDof=0; jDof<resPerDim[iVar]; jDof++){
        unsigned int globalInd = listOfIndices[iVar][jDof];
        double value = m_terms[iTerm][iVar].getVecEl(globalInd);
        iVterm.setVecEl(jDof, value);
      }
      iVterm.finalize();
      subtensTerm[iVar] = iVterm;
    }
    target.set_terms(iTerm, subtensTerm);
  }
  target.set_coeffs(m_coeffs);
  target.finalize();
}


/*  extract subtensor and put it into a given CP (overloaded)
  - input: the indices list (NON CONTIGUOUS!)
  - output: the subtensor
  - REAMRK: outer index => iVar, inner index, the list for iVar (!!!)
    this is different than what is done for sparse and full tensor (due to the format)
*/
CPTensor CPTensor::extractSubtensor(vector<vector<unsigned int> > listOfIndices){
  assert(listOfIndices.size() == m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    resPerDim[iVar] = listOfIndices[iVar].size();
  }
  CPTensor target(resPerDim, m_comm);
  target.set_rank(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    vector<vec> subtensTerm(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      vec iVterm(resPerDim[iVar], m_comm);
      for(unsigned int jDof=0; jDof<resPerDim[iVar]; jDof++){
        unsigned int globalInd = listOfIndices[iVar][jDof];
        double value = m_terms[iTerm][iVar].getVecEl(globalInd);
        iVterm.setVecEl(jDof, value);
      }
      iVterm.finalize();
      subtensTerm[iVar] = iVterm;
    }
    target.set_terms(iTerm, subtensTerm);
  }
  target.set_coeffs(m_coeffs);
  target.finalize();
  return target;
}


/* Assign subtensor:
 - input: indices bounds
 - output: the given subtensor is put into the tensor
*/
void CPTensor::assignSubtensor(vector<unsigned int> indicesBounds, CPTensor& source){
  assert(indicesBounds.size()==2*m_nVar);
  assert(source.comm()==m_comm);
  assert(source.nVar()==m_nVar);
  assert(source.rank()==m_rank);

  // computing term to add.
  CPTensor actualSubtens = extractSubtensor(indicesBounds);
  source.sum(actualSubtens, -1.0, true);

  // extend to zero:
  CPTensor toAdd(m_nDof_var, m_comm);
  toAdd.set_rank(source.rank());
  for(unsigned int iTerm=0; iTerm<source.rank(); iTerm++){
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      vec iTVterm(m_nDof_var[iVar],m_comm);
      const unsigned int sizeOfSub = indicesBounds[2*iVar+1] - indicesBounds[2*iVar];
      for(unsigned int jDof=0; jDof<sizeOfSub; jDof++){
        unsigned int globInd = jDof + indicesBounds[2*iVar];
        double value = source.terms(iTerm,iVar).getVecEl(jDof);
        iTVterm.setVecEl(globInd, value);
      }
      iTVterm.finalize();
      toAdd.set_terms(iTerm, iVar, iTVterm);
    }
    toAdd.set_coeffs(iTerm, source.coeffs(iTerm));
  }

  sum(toAdd, 1.0, false);

  // free the memory:
  actualSubtens.clear();
}


/* Reshape:
 - input: dof per dimension
 - output: a reshaped tensor
*/
void CPTensor::reshape(vector<unsigned int> dofPerDim, CPTensor& target, bool COPY=true){
  assert(dofPerDim.size()==m_nVar);
  unsigned int Ntarget;
  unsigned int Nthis;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    Ntarget *= dofPerDim[iVar];
    Nthis *= m_nDof_var[iVar];
  }
  assert(Nthis == Ntarget);

  // start reshaping:
  target.init(dofPerDim, m_comm);
  target.set_rank(m_rank);
  // TO BE CODED

}


/* Reshape:
 - input: dof per dimension
 - output: a reshaped tensor
*/
CPTensor CPTensor::reshape(vector<unsigned int> dofPerDim, bool COPY=true){
  assert(dofPerDim.size()==m_nVar);
  unsigned int Ntarget;
  unsigned int Nthis;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    Ntarget *= dofPerDim[iVar];
    Nthis *= m_nDof_var[iVar];
  }
  assert(Nthis == Ntarget);

  // start reshaping:
  CPTensor target(dofPerDim, m_comm);
  target.set_rank(m_rank);
  // TO BE CODED

  target.finalize();
  return target;
}


/* mode I contraction:
  - input: the mode (id of the variable) and the vector
  - output: the contraction
*/
CPTensor CPTensor::modeIContraction(unsigned int iMode, vec u){
  vector<unsigned int> dofPerDim(m_nVar-1);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != iMode){
      dofPerDim[cc] = m_nDof_var[iVar];
      cc = cc + 1;
    }
  }

  CPTensor result(dofPerDim, m_comm);
  result.set_rank(m_rank);

  // compute coefficients:
  vector<double> resultCoeffs(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double scalar = scalProd(m_terms[iTerm][iMode], u);
    resultCoeffs[iTerm] = m_coeffs[iTerm] * scalar;
  }
  result.set_coeffs(resultCoeffs);

  // compute the terms:
  vector<vector<vec> > resultTerms(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    vector<vec> thisTerm(m_nVar-1);
    unsigned int count = 0;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != iMode){
        vec function;
        function.copyVecFrom(m_terms[iTerm][iVar]);
        thisTerm[count] = function;
        count = count + 1;
      }
    }
    resultTerms[iTerm] = thisTerm;
  }
  result.set_terms(resultTerms);
  result.finalize();
  return result;
}


/* mode I contraction: (overloaded to modify an existing tensor)
  - input: the mode (id of the variable) and the vector
  - output: the contraction
*/
void CPTensor::modeIContraction(unsigned int iMode, vec u, CPTensor& result){
  vector<unsigned int> dofPerDim(m_nVar-1);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != iMode){
      dofPerDim[cc] = m_nDof_var[iVar];
      cc = cc + 1;
    }
  }

  result.init(dofPerDim, m_comm);
  result.set_rank(m_rank);

  // compute coefficients:
  vector<double> resultCoeffs(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double scalar = scalProd(m_terms[iTerm][iMode], u);
    resultCoeffs[iTerm] = m_coeffs[iTerm] * scalar;
  }
  result.set_coeffs(resultCoeffs);

  // compute the terms:
  vector<vector<vec> > resultTerms(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    vector<vec> thisTerm(m_nVar-1);
    unsigned int count = 0;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != iMode){
        vec function;
        function.copyVecFrom(m_terms[iTerm][iVar]);
        thisTerm[count] = function;
        count = count + 1;
      }
    }
    resultTerms[iTerm] = thisTerm;
  }
  result.set_terms(resultTerms);
  result.finalize();
}




// OPERATIONS specific to the CP format:

/*  Add a pure tensor term
  - inputs: the vector of Term of the pure tensor product, the coeff
  - output: this term is added to the current CP tensor
*/
void CPTensor::addPureTensorTerm(vector<vec>& theTerm, double coeff){
  assert(theTerm.size()==m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    assert(theTerm[iVar].size() == m_nDof_var[iVar]);
  }
  m_terms.push_back(theTerm);
  m_coeffs.push_back(coeff);
  m_rank = m_rank + 1;
}


/*  in place mode I contraction
  - input: the mode and the vector we contract agaist to
  - output: this CP is relaced by the contracted one
*/
void CPTensor::inplaceModeIContraction(unsigned int iMode, vec& u){
  // change the coefficients:
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double scalar = scalProd(m_terms[iTerm][iMode], u);
    m_coeffs[iTerm] = m_coeffs[iTerm] * scalar;
  }

  // change the terms table
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_terms[iTerm][iMode].clear();
    m_terms[iTerm].erase(m_terms[iTerm].begin()+iMode);
  }

  vector<unsigned int> dofPerDim(m_nVar-1);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != iMode){
      dofPerDim[cc] = m_nDof_var[iVar];
      cc = cc + 1;
    }
  }
  m_nVar = m_nVar-1;
  m_nDof_var.clear();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
}


/* contract
  - input: the table of functions we test against, the index to be excluded
  - output: the vector result
*/
void CPTensor::contractEverythingExceptI(unsigned int iMode, vector<vec>& uTable, vec&result){
  assert(iMode<m_nVar);
  assert(uTable.size()==m_nVar-1);

  result.init(m_nDof_var[iMode], m_comm);
  result.finalize();
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double a_i = m_coeffs[iTerm];
    unsigned int cc = 0;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != iMode){
        double scalar = scalProd(uTable[cc], m_terms[iTerm][iVar]);
        a_i = a_i * scalar;
        cc += 1;
      }
    }
    result.sum(m_terms[iTerm][iMode], a_i);
  }
  result.finalize();
}


/* compute the scalar product of two CP terms
  - inputs: the terms, the mass matrices
  - output: the scalar product
*/
double CPTensor::scalarProdTerms(vector<vec>& term_1, vector<vec>& term_2, vector<mat>& mass){
  assert(term_1.size() == m_nVar);
  assert(term_2.size() == m_nVar);
  assert(mass.size() == m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    assert(term_1[idVar].size() == m_nDof_var[idVar]);
    assert(term_2[idVar].size() == m_nDof_var[idVar]);
  }

  double scalar = 1.0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    vec prod = mass[idVar] * term_1[idVar];
    double quadForm = scalProd(prod, term_2[idVar]);
    scalar *= quadForm;
    prod.clear();
  }
  return scalar;
};


/* compute the scalar product of two CP terms
  - inputs: the terms, the mass matrices
  - output: the scalar product
  REMARK: Masses are identities \ell^2 scalar product
*/
double CPTensor::scalarProdTerms(vector<vec>& term_1, vector<vec>& term_2){
  assert(term_1.size() == m_nVar);
  assert(term_2.size() == m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    assert(term_1[idVar].size() == m_nDof_var[idVar]);
    assert(term_2[idVar].size() == m_nDof_var[idVar]);
  }

  double scalar = 1.0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    double quadForm = scalProd(term_1[idVar], term_2[idVar]);
    scalar *= quadForm;
  }
  return scalar;
};


/* compute the scalar product between this CP and another one
 - Input: the CP to compute the scalar product with
 - Output: a scalar <this, otherCP>
 REMARK: \ell^2 discrete scalar product !
*/
double CPTensor::scalarProd(CPTensor& otherTens, vector<mat>& mass){
  assert(hasSameStructure(otherTens));

  double toBeReturned = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int jTerm=0; jTerm<otherTens.rank(); jTerm++){
      vector<vec> otherTerms = otherTens.terms(jTerm);
      double toBeAdded = scalarProdTerms(m_terms[iTerm], otherTerms, mass);
      toBeReturned += m_coeffs[iTerm]*otherTens.coeffs(jTerm)*toBeAdded;
    }
  }

  return toBeReturned;
};


/* compute the scalar product between this CP and another one
 - Input: the CP to compute the scalar product with
 - Output: a scalar <this, otherCP>
 REMARK: \ell^2 discrete scalar product !
*/
double CPTensor::scalarProd(CPTensor& otherTens){
  assert(hasSameStructure(otherTens));

  double toBeReturned = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int jTerm=0; jTerm<otherTens.rank(); jTerm++){
      vector<vec> otherTerms = otherTens.terms(jTerm);
      double toBeAdded = scalarProdTerms(m_terms[iTerm], otherTerms);
      toBeReturned += m_coeffs[iTerm]*otherTens.coeffs(jTerm)*toBeAdded;
    }
  }

  return toBeReturned;
};


/* reorder the variables as prescribed by a vector<unsigned int>
 - Input: the new variable order
 - Output: the same CP with varied variables order;
*/
void CPTensor::reorderVars(vector<unsigned int> order){
  assert(order.size()==m_nVar);
  // copy m_nDof_var:
  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    unsigned int id_var = order[iVar];
    resPerDim[iVar] = m_nDof_var[id_var];
  }
  // change the m_nDof_var:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = resPerDim[iVar];
  }
  // updating the maps:
  compute_minc();

  // copy the terms:
  vector<vector<vec> > reorderedTerms(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    reorderedTerms[iTerm].resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      unsigned int id_var = order[iVar];
      reorderedTerms[iTerm][iVar] = m_terms[iTerm][id_var];
    }
  }
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_terms[iTerm][iVar] = reorderedTerms[iTerm][iVar];
    }
  }

};




/* compute the 2 norm squared of a CP tensor
  - inputs: the vector of mass matrices
  - output: the norm of this tensor
*/
double CPTensor::norm2CP(vector<mat>& mass){
  double toBeReturned = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double tmp = m_coeffs[iTerm] * m_coeffs[iTerm];
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      vec prod = mass[idVar] * m_terms[iTerm][idVar]; // matrix vector product
      double quadForm = scalProd(prod, m_terms[iTerm][idVar]);
      tmp = tmp * quadForm;
      prod.clear();
    }
    toBeReturned = toBeReturned + tmp;
    for(unsigned int jTerm=iTerm+1; jTerm<m_rank; jTerm++){
      double tmpDiffInd = 2.0 * m_coeffs[iTerm] * m_coeffs[jTerm];
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        vec prod = mass[idVar] * m_terms[iTerm][idVar];
        double quadForm = scalProd(prod, m_terms[jTerm][idVar]);
        prod.clear();
      }
      toBeReturned = toBeReturned + tmpDiffInd;
    }
  }
  return toBeReturned;
}


/* compute the 2 norm squared of a CP tensor
  - inputs: mass matrices are identity!!
  - output: the norm of this tensor
*/
double CPTensor::norm2CP(){
  double toBeReturned = 0.0;

  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double tmp = m_coeffs[iTerm] * m_coeffs[iTerm];
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      double quadForm = scalProd(m_terms[iTerm][idVar], m_terms[iTerm][idVar]);
      tmp = tmp * quadForm;
    }
    if(isnan(tmp)){puts("overflow!");};

    toBeReturned = toBeReturned + tmp;

    for(unsigned int jTerm=iTerm+1; jTerm<m_rank; jTerm++){
      double tmpDiffInd = 2.0 * m_coeffs[iTerm] * m_coeffs[jTerm];
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        double quadForm = scalProd(m_terms[iTerm][idVar], m_terms[jTerm][idVar]);
        tmpDiffInd = tmpDiffInd * quadForm;
      }
      if(isnan(tmpDiffInd)){puts("overflow!");};

      toBeReturned = toBeReturned + tmpDiffInd;
    }
  }
  if(toBeReturned<0.0){toBeReturned  = 0.0;} // fix in case of small roundoffs.
  return toBeReturned;
}


/* compute first svd term of the unfolding wothout computing the Unfolding!
  - input: the mode-I number
  - output: sigma, u
*/
void CPTensor::compute_Unfolding_OneSvdTerm(unsigned int index, double& sigma, vec& u){
  assert(index<m_nVar);

  //-- compute svd of the matrix whose columns are the modes in index direction
  mat T(m_nDof_var[index], m_rank, m_comm);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    T.setCol(iTerm, m_terms[iTerm][index]);
  }
  T.finalize();

  svd Tsvd(T);
  mat Vt = transpose(Tsvd.Vmat());
  mat Z = Tsvd.Smat() * Vt;

  // computing the covariance matrix K:
  mat K(m_rank, m_rank, m_comm);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int jTerm=0; jTerm<=iTerm; jTerm++){
      double value = m_coeffs[iTerm] * m_coeffs[jTerm];
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        if(iVar != index){
          double scalar = scalProd(m_terms[iTerm][iVar],m_terms[jTerm][iVar]);
          value = value * scalar;
        }
      }
      K.setMatEl(iTerm, jTerm, value);
      K.setMatEl(jTerm, iTerm, value);
    }
  }
  K.finalize();


  mat Zt = transpose(Z);
  mat tmp = Z * K;
  mat tildeK = tmp * Zt;

  // compute first eigenvalue:
  eigenSolver eps;
  eps.solveOneTermHermitian(tildeK);


  // the result:
  u.init(m_nDof_var[index], m_comm);
  u.finalize();

  sigma = sqrt(eps.lambda());
  vec weights = eps.v();
  mat U = Tsvd.Umat();
  matVecProd(U, weights, u);


  // free the memory:
  T.clear();
  K.clear();
  Z.clear();
  Vt.clear();
  tmp.clear();
  Zt.clear();
  tildeK.clear();
  Tsvd.clear();
  eps.clear();
}


/* compute first svd term of the unfolding wothout computing the Unfolding!
  - input: the mode-I number
  - output: sigma, u
*/
void CPTensor::compute_Unfolding_Svd(unsigned int index, vector<double>& sigma, vector<vec>& u){
  assert(index<m_nVar);

  if(m_rank < m_nDof_var[index]){
    //-- compute svd of the matrix whose columns are the modes in index direction
    mat T(m_nDof_var[index], m_rank, m_comm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      T.setCol(iTerm, m_terms[iTerm][index]);
    }
    T.finalize();

    svd Tsvd(T);
    mat Vt = transpose(Tsvd.Vmat());
    mat Z = Tsvd.Smat() * Vt;

    // computing the covariance matrix K:
    mat K(m_rank, m_rank, m_comm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      for(unsigned int jTerm=0; jTerm<=iTerm; jTerm++){
        double value = m_coeffs[iTerm] * m_coeffs[jTerm];
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          if(iVar != index){
            double scalar = scalProd(m_terms[iTerm][iVar],m_terms[jTerm][iVar]);
            value = value * scalar;
          }
        }
        K.setMatEl(iTerm, jTerm, value);
        K.setMatEl(jTerm, iTerm, value);
      }
    }
    K.finalize();

    mat Zt = transpose(Z);
    mat tmp = Z * K;
    mat tildeK = tmp * Zt;

    // compute the eigenvalue:
    eigenSolver eps;
    eps.init(tildeK, EPS_HEP, tildeK.nRows(), 1.0e-12);
    eps.solveHermEPS(tildeK);
    vector<vec> vv = eps.eigenVecs();
    vector<double> lambda = eps.eigenVals();

    // the result:
    sigma.resize(lambda.size());
    for(unsigned int iS=0; iS<lambda.size(); iS++){
      sigma[iS] = sqrt(fabs(lambda[iS]));
    }

    u.resize(lambda.size());
    mat U = Tsvd.Umat();
    for(unsigned int iS=0; iS<lambda.size(); iS++){
      u[iS].init(m_nDof_var[index], m_comm);
      u[iS].finalize();
      matVecProd(U, vv[iS], u[iS]);
    }

    // free the memory:
    T.clear();
    K.clear();
    Z.clear();
    Vt.clear();
    tmp.clear();
    Zt.clear();
    tildeK.clear();
    Tsvd.clear();
    eps.clear();
  }
  else{
    // computing the covariance:
    mat C(m_nDof_var[index], m_nDof_var[index], m_comm);
    for(unsigned int iDof=0; iDof<m_nDof_var[index]; iDof++){
      for(unsigned int jDof=0; jDof<=iDof; jDof++){
        double val = 0.0;
        for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
          double t1 = m_terms[iTerm][index].getVecEl(iDof);
          for(unsigned int jTerm=0; jTerm<m_rank; jTerm++){
            double t2 = m_terms[jTerm][index].getVecEl(jDof);
            double tmp = m_coeffs[iTerm] * m_coeffs[jTerm];
            for(unsigned int iVar=0; iVar<m_nVar; iVar++){
              if(iVar != index){
                double scalar = scalProd(m_terms[iTerm][iVar],m_terms[jTerm][iVar]);
                tmp = tmp * scalar;
              }
            }
            val += t1 * t2 * tmp;
          }
        }
        C(iDof,jDof) = val;
        if(iDof != jDof){
          C(jDof, iDof) = val;
        }
      }
    }
    C.finalize();
    //C.print();
    eigenSolver eps;
    //cout << m_nDof_var[index] << endl;
    eps.init(C, EPS_HEP, m_nDof_var[index], 1.0e-12);
    eps.solveHermEPS(C);
    vector<vec> vv = eps.eigenVecs();
    vector<double> lambda = eps.eigenVals();
    //cout << lambda.size() << endl;
    u.resize(vv.size());
    sigma.resize(lambda.size());
    for(unsigned int iMod=0; iMod<vv.size(); iMod++){
      u[iMod].copyVecFrom(vv[iMod]);
      sigma[iMod] = sqrt(fabs(lambda[iMod]));
    }
    // free the memory;
    eps.clear();
    C.clear();
    // end of case
  }
  // end of function
}


/* compute first svd term of the unfolding wothout computing the Unfolding!
  - input: the mode-I number
  - output: sigma, u
*/
void CPTensor::compute_Approx_Unfolding_Svd(unsigned int index, vector<double>& sigma, vector<vec>& u){
  assert(index<m_nVar);

  // normalising the terms:
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double normTerm = 1.0;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      double normVec = m_terms[iTerm][iVar].norm();
      m_terms[iTerm][iVar] *= 1.0/normVec;
      normTerm *= normVec;
    }
    m_coeffs[iTerm] *= normTerm;
  }
  // list of important terms:
  vector<unsigned int> toRetain;
  const double threshold = sqrt(norm2CP()/(5.0*m_rank));
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    if(fabs(m_coeffs[iTerm])>threshold){
      toRetain.push_back(iTerm);
    }
  }
  const unsigned int N = toRetain.size();
  cout << "Number of relevant terms: " << N << endl;

  if(10000*N < m_nDof_var[index]){
    //-- compute svd of the matrix whose columns are the modes in index direction
    mat T(m_nDof_var[index], N, m_comm);
    for(unsigned int iT=0; iT<N; iT++){
      unsigned int iTerm = toRetain[iT];
      T.setCol(iT, m_terms[iTerm][index]);
    }
    T.finalize();

    svd Tsvd(T);
    mat Vt = transpose(Tsvd.Vmat());
    mat Z = Tsvd.Smat() * Vt;

    // computing the covariance matrix K:
    mat K(N, N, m_comm);
    for(unsigned int iT=0; iT<N; iT++){
      unsigned int iTerm = toRetain[iT];
      for(unsigned int jT=0; jT<=iT; jT++){
        unsigned int jTerm = toRetain[jT];
        double value = m_coeffs[iTerm] * m_coeffs[jTerm];
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          if(iVar != index){
            double scalar = scalProd(m_terms[iTerm][iVar],m_terms[jTerm][iVar]);
            value = value * scalar;
          }
        }
        K.setMatEl(iT, jT, value);
        K.setMatEl(jT, iT, value);
      }
    }
    K.finalize();

    mat Zt = transpose(Z);
    mat tmp = Z * K;
    mat tildeK = tmp * Zt;

    // compute the eigenvalue:
    eigenSolver eps;
    eps.init(tildeK, EPS_HEP, tildeK.nRows(), 1.0e-12);
    eps.solveHermEPS(tildeK);
    vector<vec> vv = eps.eigenVecs();
    vector<double> lambda = eps.eigenVals();

    // the result:
    sigma.resize(lambda.size());
    for(unsigned int iS=0; iS<lambda.size(); iS++){
      sigma[iS] = sqrt(fabs(lambda[iS]));
    }

    u.resize(lambda.size());
    mat U = Tsvd.Umat();
    for(unsigned int iS=0; iS<lambda.size(); iS++){
      u[iS].init(m_nDof_var[index], m_comm);
      u[iS].finalize();
      matVecProd(U, vv[iS], u[iS]);
    }

    // free the memory:
    T.clear();
    K.clear();
    Z.clear();
    Vt.clear();
    tmp.clear();
    Zt.clear();
    tildeK.clear();
    Tsvd.clear();
    eps.clear();
  }
  else{
    // computing the covariance:
    mat C(m_nDof_var[index], m_nDof_var[index], m_comm);
    for(unsigned int iDof=0; iDof<m_nDof_var[index]; iDof++){
      for(unsigned int jDof=0; jDof<=iDof; jDof++){
        double val = 0.0;
        for(unsigned int iT=0; iT<N; iT++){
          unsigned int iTerm = toRetain[iT];
          double t1 = m_terms[iTerm][index].getVecEl(iDof);
          for(unsigned int jT=0; jT<N; jT++){
            unsigned int jTerm = toRetain[jT];
            double t2 = m_terms[jTerm][index].getVecEl(jDof);
            double tmp = m_coeffs[iTerm] * m_coeffs[jTerm];
            for(unsigned int iVar=0; iVar<m_nVar; iVar++){
              if(iVar != index){
                double scalar = scalProd(m_terms[iTerm][iVar],m_terms[jTerm][iVar]);
                tmp = tmp * scalar;
              }
            }
            val += t1 * t2 * tmp;
          }
        }
        C(iDof,jDof) = val;
        if(iDof != jDof){
          C(jDof, iDof) = val;
        }
      }
    }
    C.finalize();
    //C.print();
    eigenSolver eps;
    //cout << m_nDof_var[index] << endl;
    eps.init(C, EPS_HEP, m_nDof_var[index], 1.0e-12);
    eps.solveHermEPS(C);
    vector<vec> vv = eps.eigenVecs();
    vector<double> lambda = eps.eigenVals();
    //cout << lambda.size() << endl;
    u.resize(vv.size());
    sigma.resize(lambda.size());
    for(unsigned int iMod=0; iMod<vv.size(); iMod++){
      u[iMod].copyVecFrom(vv[iMod]);
      sigma[iMod] = sqrt(fabs(lambda[iMod]));
    }
    // free the memory;
    eps.clear();
    C.clear();
    // end of case
  }
  // end of function
}



/* Compute Unfolding for CP
  -input: the mode,
  -output: the Unfolding:
*/
void CPTensor::computeUnfolding(const unsigned int index, mat& M){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  M.init(nRows, nCols, m_comm);
  M.finalize();

  //define the unfolding increments
  vector<unsigned int> unfoldInc(m_nVar-1);
  unsigned int unfInc = 1;
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar!=index){
      unfoldInc[cc] = unfInc;
      unfInc = unfInc * m_nDof_var[idVar];
      cc = cc + 1;
    }
  }

  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    mat thisTermUnfold(nRows, nCols, m_comm);

    // filling the pure tensor term unfolding:
    for(unsigned int i=0; i<nRows; i++){
      double term_ij = m_terms[iTerm][index].getVecEl(i);
      double term_ij_0 = term_ij;
      for(unsigned int j=0; j<nCols; j++){
        term_ij = term_ij_0;
        unsigned int reminder = j;
        cc = m_nVar-2;
        for (int h=m_nVar-1; h>=0; h--){
            if(h != index){
              unsigned int hThIndex = reminder/unfoldInc[cc];
              term_ij = term_ij * m_terms[iTerm][h].getVecEl(hThIndex);
              reminder = reminder - hThIndex * unfoldInc[cc];
              cc = cc - 1;
            }
        };
        if(term_ij != 0.0){
          thisTermUnfold.setMatEl(i, j, term_ij);
        }
      }
    }
    thisTermUnfold.finalize();
    //thisTermUnfold.print();
    M.sum(thisTermUnfold, m_coeffs[iTerm]);
    thisTermUnfold.clear();
  }
	M.finalize();
}


/* Compute Unfolding for CP
  -input: the mode,
  -output: the Unfolding:
*/
mat CPTensor::computeUnfolding(const unsigned int index){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  mat M(nRows, nCols, m_comm);
  M.finalize();


  //define the unfolding increments
  vector<unsigned int> unfoldInc(m_nVar-1);
  unsigned int unfInc = 1;
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar!=index){
      unfoldInc[cc] = unfInc;
      unfInc = unfInc * m_nDof_var[idVar];
      cc = cc + 1;
    }
  }

  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    mat thisTermUnfold(nRows, nCols, m_comm);

    // filling the pure tensor term unfolding:
    for(unsigned int i=0; i<nRows; i++){
      double term_ij = m_terms[iTerm][index].getVecEl(i);
      double term_ij_0 = term_ij;
      for(unsigned int j=0; j<nCols; j++){
        term_ij = term_ij_0;
        unsigned int reminder = j;
        cc = m_nVar-2;
        for (int h=m_nVar-1; h>=0; h--){
            if(h != index){
              unsigned int hThIndex = reminder/unfoldInc[cc];
              term_ij = term_ij * m_terms[iTerm][h].getVecEl(hThIndex);
              reminder = reminder - hThIndex * unfoldInc[cc];
              cc = cc - 1;
            }
        };
        if(term_ij != 0.0){
          thisTermUnfold.setMatEl(i, j, term_ij);
        }
      }
    }
    thisTermUnfold.finalize();
    M.sum(thisTermUnfold, m_coeffs[iTerm]);
    thisTermUnfold.clear();
  }
	M.finalize();
  return M;
}


/* Compute unfolding (overloaded)
  - input: the mode along which to compute the unfolding
  - output: the fibers
*/
void CPTensor::computeFibers(const unsigned int index, vector<vec>& fibers){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  fibers.resize(nCols);
  for(unsigned int iFib=0; iFib<nCols; iFib++){
    fibers[iFib].init(nRows, m_comm);
  }


  //define the unfolding increments
  vector<unsigned int> unfoldInc(m_nVar-1);
  unsigned int unfInc = 1;
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar!=index){
      unfoldInc[cc] = unfInc;
      unfInc = unfInc * m_nDof_var[idVar];
      cc = cc + 1;
    }
  }

  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){


    // filling the pure tensor term unfolding:
    for(unsigned int i=0; i<nRows; i++){
      double term_ij = m_terms[iTerm][index].getVecEl(i);
      double term_ij_0 = term_ij;
      for(unsigned int j=0; j<nCols; j++){
        term_ij = term_ij_0;
        unsigned int reminder = j;
        cc = m_nVar-2;
        for (int h=m_nVar-1; h>=0; h--){
            if(h != index){
              unsigned int hThIndex = reminder/unfoldInc[cc];
              term_ij = term_ij * m_terms[iTerm][h].getVecEl(hThIndex);
              reminder = reminder - hThIndex * unfoldInc[cc];
              cc = cc - 1;
            }
        };
        double val = fibers[j].getVecEl(i);
        val = val + term_ij * m_coeffs[iTerm];
        fibers[j].setVecEl(i,val);
        //fibers[j](i) = fibers[j](i) +  term_ij * m_coeffs[iTerm]; // synthetic line:
      }
    }
  }

  for(unsigned int iFib=0; iFib<nCols; iFib++){
    fibers[iFib].finalize();
  }
}


/* Compute unfolding (overloaded)
  - input: the mode along which to compute the unfolding
  - output: the fibers
*/
vector<vec> CPTensor::computeFibers(const unsigned int index){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  vector<vec> fibers(nCols);
  for(unsigned int iFib=0; iFib<nCols; iFib++){
    fibers[iFib].init(nRows, m_comm);
  }


  //define the unfolding increments
  vector<unsigned int> unfoldInc(m_nVar-1);
  unsigned int unfInc = 1;
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar!=index){
      unfoldInc[cc] = unfInc;
      unfInc = unfInc * m_nDof_var[idVar];
      cc = cc + 1;
    }
  }

  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){


    // filling the pure tensor term unfolding:
    for(unsigned int i=0; i<nRows; i++){
      double term_ij = m_terms[iTerm][index].getVecEl(i);
      double term_ij_0 = term_ij;
      for(unsigned int j=0; j<nCols; j++){
        term_ij = term_ij_0;
        unsigned int reminder = j;
        cc = m_nVar-2;
        for (int h=m_nVar-1; h>=0; h--){
            if(h != index){
              unsigned int hThIndex = reminder/unfoldInc[cc];
              term_ij = term_ij * m_terms[iTerm][h].getVecEl(hThIndex);
              reminder = reminder - hThIndex * unfoldInc[cc];
              cc = cc - 1;
            }
        };
        double val = fibers[j].getVecEl(i);
        val = val + term_ij * m_coeffs[iTerm];
        fibers[j].setVecEl(i,val);
        //fibers[j](i) = fibers[j](i) +  term_ij * m_coeffs[iTerm]; // synthetic line:
      }
    }
  }

  for(unsigned int iFib=0; iFib<nCols; iFib++){
    fibers[iFib].finalize();
  }
  return fibers;
}


// I/O routines:

/* save a CP
  - save a file .log containing rank, dimension, and degrees of freedom per dimension
  - save modes and coefficients
*/
void CPTensor::save(string fileName){
  string logName = fileName + ".log";
  // saving tensor rank, dim, dof per dim:
  ofstream logfile;
  logfile.open(logName.c_str());
  logfile << m_rank << endl;
  logfile << m_nVar << endl;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    logfile << m_nDof_var[iVar] << endl;
  }
  logfile.close();
  // saving modes:
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      string modeName = fileName + "_" + to_string(iTerm) + "_" + to_string(iVar) + ".dat";
      m_terms[iTerm][iVar].save(modeName);
    }
  }
  // saving coefficients:
  string coeffName = fileName + "_coeffs.dat";
  ofstream coeffFile(coeffName.c_str());
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    coeffFile << m_coeffs[iTerm] << endl;
  }
  coeffFile.close();
}


/* load a CP
  - load a file .log containing rank, dimension, and degrees of freedom per dimension
  - load modes and coefficients
*/
void CPTensor::load(string fileName){
  //load the log file:
  string logName = fileName + ".log";
  ifstream logFile(logName.c_str());
  vector<unsigned int> data;
  unsigned int datum;
  while(logFile >> datum){
    data.push_back(datum);
  }
  m_rank = data[0];
  m_nVar = data[1];
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = data[iVar+2];
  }
  logFile.close();
  compute_minc();
  //load the modes:
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      string modeName = fileName + "_" + to_string(iTerm) + "_" + to_string(iVar) + ".dat";
      m_terms[iTerm][iVar].load(modeName);
    }
  }
  // load the coefficients:
  string coeffName = fileName + "_coeffs.dat";
  ifstream coeffFile(coeffName.c_str());
  double coeff;
  m_coeffs.resize(m_rank);
  unsigned int cc = 0;
  while(coeffFile >> coeff){
    m_coeffs[cc] = coeff;
    cc += 1;
  }
  coeffFile.close();
}
