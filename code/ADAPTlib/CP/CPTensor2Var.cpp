#include "CPTensor2Var.hpp"


/* 
*  Constructors/Destructors
*/

CPTensor2Var::CPTensor2Var() : m_rank(0), m_nDof_x(0), m_nDof_y(0), m_comm(PETSC_COMM_WORLD) {}

CPTensor2Var::CPTensor2Var(const unsigned int nDof_x, const unsigned int nDof_y, const MPI_Comm& comm) : m_rank(0), m_nDof_x(nDof_x), m_nDof_y(nDof_y), m_comm(comm) {}

CPTensor2Var::CPTensor2Var(const unsigned int nDof_x, const unsigned int nDof_y, const std::vector<double>& coeffs, const std::vector<vec>& terms_x, const std::vector<vec>& terms_y, const MPI_Comm& comm) : m_rank(coeffs.size()), m_nDof_x(nDof_x), m_nDof_y(nDof_y), m_coeffs(coeffs), m_terms_x(terms_x), m_terms_y(terms_y), m_comm(comm) {
  assert(CPTensor2Var::check_terms(coeffs.size(), nDof_x, terms_x) && CPTensor2Var::check_terms(coeffs.size(), nDof_y, terms_y));
  // for (unsigned int iTerm=0; iTerm<m_terms_x.size(); ++iTerm) {
  //   m_terms_x[iTerm].convert(terms_x[iTerm]);
  // }
  // for (unsigned int iTerm=0; iTerm<m_terms_y.size(); ++iTerm) {
  //   m_terms_y[iTerm].convert(terms_y[iTerm]);
  // }
}

CPTensor2Var::CPTensor2Var(const CPTensor2Var& tens) : m_rank(tens.m_rank), m_nDof_x(tens.m_nDof_x), m_nDof_y(tens.m_nDof_y), m_coeffs(tens.m_coeffs), m_terms_x(tens.m_terms_x), m_terms_y(tens.m_terms_y), m_comm(tens.m_comm) {}

CPTensor2Var::~CPTensor2Var() {}

void CPTensor2Var::init(const unsigned int nDof_x, const unsigned int nDof_y, const MPI_Comm& comm) {
  m_rank = 0;
  m_nDof_x = nDof_x;
  m_nDof_y = nDof_y;
  m_comm = comm;
}

void CPTensor2Var::init(const unsigned int nDof_x, const unsigned int nDof_y, const std::vector<double>& coeffs, const std::vector<vec>& terms_x, const std::vector<vec>& terms_y, const MPI_Comm& comm) {
  assert(CPTensor2Var::check_terms(coeffs.size(), nDof_x, terms_x) && CPTensor2Var::check_terms(coeffs.size(), nDof_y, terms_y));
  m_rank = coeffs.size();
  m_nDof_x = nDof_x;
  m_nDof_y = nDof_y;
  m_coeffs = coeffs;
  m_terms_x = terms_x;
  m_terms_y = terms_y;
  // m_terms_x.resize(terms_x.size());
  // for (unsigned int iTerm=0; iTerm<m_terms_x.size(); ++iTerm) {
  //   m_terms_x[iTerm].convert(terms_x[iTerm]);
  // }
  // m_terms_y.resize(terms_y.size());
  // for (unsigned int iTerm=0; iTerm<m_terms_y.size(); ++iTerm) {
  //   m_terms_y[iTerm].convert(terms_y[iTerm]);
  // }
  m_comm = comm;
}

void CPTensor2Var::init(const CPTensor2Var& tens) {
  m_rank = tens.m_rank;
  m_nDof_x = tens.m_nDof_x;
  m_nDof_y = tens.m_nDof_y;
  m_coeffs = tens.m_coeffs;
  m_terms_x = tens.m_terms_x;
  m_terms_y = tens.m_terms_y;
  // m_terms_x.resize(tens.m_rank);
  // for (unsigned int iTerm=0; iTerm<m_terms_x.size(); ++iTerm) {
  //   m_terms_x[iTerm].convert(tens.m_terms_x[iTerm]);
  // }
  // m_terms_y.resize(tens.m_rank);
  // for (unsigned int iTerm=0; iTerm<m_terms_y.size(); ++iTerm) {
  //   m_terms_y[iTerm].convert(tens.m_terms_y[iTerm]);
  // }
  m_comm = tens.m_comm;
}

void CPTensor2Var::clear() {
  m_coeffs.clear();
  for(unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
    m_terms_x[iTerm].clear();
  }
  m_terms_x.clear();
  for(unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
    m_terms_y[iTerm].clear();
  }
  m_terms_y.clear();
  m_rank = 0;
}


/* 
*  Getters/Setters
*/

void CPTensor2Var::rank(const unsigned int rank) {
  assert(rank<=m_rank);
  m_rank = rank;
  m_coeffs.resize(m_rank);
  m_terms_x.resize(m_rank);
  m_terms_y.resize(m_rank);
}

void CPTensor2Var::coeffs(const unsigned int iTerm, const double theCoeffs) {
  assert(iTerm<m_rank);
  m_coeffs[iTerm] = theCoeffs;
}

void CPTensor2Var::coeffs(const std::vector<double>& coeffs) {
  assert(coeffs.size()==m_rank);
  m_coeffs = coeffs;
}

void CPTensor2Var::terms_x(const unsigned int iTerm, const vec& theTerms) {
  assert(iTerm<m_rank && theTerms.size()==m_nDof_x);
  m_terms_x[iTerm] = theTerms;
  // m_terms_x[iTerm].copy(theTerms);
}

void CPTensor2Var::terms_x(const std::vector<vec>& terms_x) {
  assert(CPTensor2Var::check_terms(m_rank, m_nDof_x, terms_x));
  m_terms_x = terms_x;
  // for (unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
  //   m_terms_x[iTerm].copy(terms_x[iTerm]);
  // }
}

void CPTensor2Var::terms_y(const unsigned int iTerm, const vec& theTerms) {
  assert(iTerm<m_rank && theTerms.size()==m_nDof_y);
  m_terms_y[iTerm] = theTerms;
  // m_terms_y[iTerm].copy(theTerms);
}

void CPTensor2Var::terms_y(const std::vector<vec>& terms_y) {
  assert(CPTensor2Var::check_terms(m_rank, m_nDof_y, terms_y));
  m_terms_y = terms_y;
  // for (unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
  //   m_terms_y[iTerm].copy(terms_y[iTerm]);
  // }
}


/* 
*  Methods
*/

vec CPTensor2Var::eval(const unsigned int iTerm) const {
  assert(iTerm<m_nDof_y);
  return lin_comb(m_nDof_x, m_nDof_y, m_terms_x, m_coeffs, m_terms_y, m_comm, iTerm);
}

std::vector<vec> CPTensor2Var::eval() const {
  std::vector<vec> tens(m_rank);
  for (unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
    tens[iTerm] = this->eval(iTerm);
    // tens[iTerm].convert(this->eval(iTerm));
  }
  return tens;
}

CPTensor2Var CPTensor2Var::extract_subtensor(const unsigned int iLow, const unsigned int iUp) const {
  assert(iLow<=iUp && iUp<=m_nDof_x);
  std::vector<vec> subterms_x(m_rank);
  for (unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
    subterms_x[iTerm] = m_terms_x[iTerm].extractSubvector(iLow, iUp);
    // subterms_x[iTerm].convert(m_terms_x[iTerm].extractSubvector(iLow, iUp));
  }
  return CPTensor2Var(iUp-iLow, m_nDof_y, m_coeffs, subterms_x, m_terms_y, m_comm);
}


void CPTensor2Var::add(const CPTensor2Var& tens, const double scale) {
  assert(m_nDof_x==tens.m_nDof_x && m_nDof_y==tens.m_nDof_y);

  if (fabs(scale)!=0.0) {

    m_coeffs.resize(m_rank+tens.m_rank);
    for(unsigned int iTerm=0; iTerm<tens.m_rank; ++iTerm) {
      m_coeffs[m_rank+iTerm] = scale*tens.m_coeffs[iTerm];    
    }

    m_terms_x.resize(m_rank+tens.m_rank);
    for(unsigned int iTerm=0; iTerm<tens.m_rank; ++iTerm) {
      m_terms_x[m_rank+iTerm].convert(tens.m_terms_x[iTerm]);    
    }

    m_terms_y.resize(m_rank+tens.m_rank);
    for(unsigned int iTerm=0; iTerm<tens.m_rank; ++iTerm) {
      m_terms_y[m_rank+iTerm].convert(tens.m_terms_y[iTerm]);    
    }

    m_rank += tens.m_rank;
  }
}


void CPTensor2Var::truncate(const double tol) {
  assert(0.0<=tol && tol<=1.0);

  if (m_rank>1) {

    // QR
    qr terms_x_qr(m_terms_x, m_comm), terms_y_qr(m_terms_y, m_comm);

    // SVD
    mat K = terms_x_qr.R()*vector2mat(m_coeffs, m_comm)*transpose(terms_y_qr.R());

    // Check if K corresponds to the null matrix
    if (K.norm()<1e-15) {

      m_rank = 0;
      m_coeffs.resize(m_rank);
      m_terms_x.resize(m_rank);
      m_terms_y.resize(m_rank);

    } else {

      svd tmp(K);
      mat U = tmp.Umat();
      vec S = tmp.Svec();
      mat V = tmp.Vmat();

      // Find the rank satisfying the tolerance condition
      m_rank = 0;
      double error1 = tol, error2 = 1.0;
      m_coeffs = vec2vector(S);
      while (m_rank<m_coeffs.size() && error1<error2) {
        ++m_rank;
        error1 = 0.0;
        for (unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
          error1 += (tol*m_coeffs[iTerm])*(tol*m_coeffs[iTerm]);
        }
        error2 = 0.0;
        for (unsigned int iTerm=m_rank; iTerm<m_coeffs.size(); ++iTerm) {
          error2 += (1.0-tol*tol)*m_coeffs[iTerm]*m_coeffs[iTerm];
        }
      }

      // Truncated tensor
      m_coeffs.resize(m_rank);

      m_terms_x.resize(m_rank);
      for (unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
        m_terms_x[iTerm] = lin_comb(terms_x_qr.Q(), U, iTerm);
        // m_terms_x[iTerm].copy(lin_comb(terms_x_qr.Q(), U, iTerm));
      }

      m_terms_y.resize(m_rank);
      for (unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
        m_terms_y[iTerm] = lin_comb(terms_y_qr.Q(), V, iTerm);
        // m_terms_y[iTerm].copy(lin_comb(terms_y_qr.Q(), V, iTerm));
      }
    }
  }
}

void CPTensor2Var::solve(const KSP& ksp) {
  for (unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
    KSPSolve(ksp, m_terms_x[iTerm].x(), m_terms_x[iTerm].x());
  }
}

void CPTensor2Var::print_coeffs() const {
  vec coeffs = vector2vec(m_coeffs, m_comm);
  // vec coeffs;
  // coeffs.convert(vector2vec(m_coeffs, m_comm));
  coeffs.print();
}

void CPTensor2Var::print_terms_x() const {
  mat terms_x = vector2mat(m_terms_x, m_comm);
  // mat terms_x;
  // terms_x.convert(vector2mat(m_terms_x, m_comm));
  terms_x.print();
}

void CPTensor2Var::print_terms_y() const {
  mat terms_y = vector2mat(m_terms_y, m_comm);
  // mat terms_y;
  // terms_y.convert(vector2mat(m_terms_y, m_comm));
  terms_y.print();
}

void CPTensor2Var::print(const unsigned int iTerm) const {
  vec v = this->eval(iTerm);
  // vec v;
  // v.convert(this->eval(iTerm));
  v.print();
}

void CPTensor2Var::print() const {
  mat tens = vector2mat(this->eval(), m_comm);
  // mat tens;
  // tens.convert(vector2mat(this->eval(), m_comm));
  tens.print();
}

void CPTensor2Var::save_coeffs(const std::string& filename) const {
  vec coeffs = vector2vec(m_coeffs, m_comm);
  // vec coeffs;
  // coeffs.convert(vector2vec(m_coeffs, m_comm));
  coeffs.save(filename);
}

void CPTensor2Var::save_terms_x(const std::string& filename) const {
  mat terms_x = vector2mat(m_terms_x, m_comm);
  // mat terms_x;
  // terms_x.convert(vector2mat(m_terms_x, m_comm));
  terms_x.save(filename);
}

void CPTensor2Var::save_terms_y(const std::string& filename) const {
  mat terms_y = vector2mat(m_terms_y, m_comm);
  // mat terms_y;
  // terms_y.convert(vector2mat(m_terms_y, m_comm));
  terms_y.save(filename);
}

void CPTensor2Var::save(const std::string& filename, const unsigned int iTerm) const {
  assert(iTerm<m_nDof_y);
  vec tens = this->eval(iTerm);
  // vec tens;
  // tens.convert(this->eval(iTerm));
  tens.save(filename);
}

void CPTensor2Var::save(const std::string& filename) const {
  mat tens = vector2mat(this->eval(), m_comm);
  // mat tens;
  // tens.convert(vector2mat(this->eval(), m_comm));
  tens.save(filename);
}


/* 
*  Operators
*/

double CPTensor2Var::operator () (const unsigned int ind_x, const unsigned int ind_y) const {
  assert(ind_x<m_nDof_x && ind_y<m_nDof_y);
  double value = 0.;
  for(unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
    value += m_coeffs[iTerm]*m_terms_x[iTerm].getVecEl(ind_x)*m_terms_y[iTerm].getVecEl(ind_y);
  }
  return value;
}

CPTensor2Var& CPTensor2Var::operator += (const CPTensor2Var& tens) {
  assert(m_nDof_x==tens.m_nDof_x && m_nDof_y==tens.m_nDof_y);
  this->add(tens, 1.0);
  return *this;
}

CPTensor2Var& CPTensor2Var::operator -= (const CPTensor2Var& tens) {
  assert(m_nDof_x==tens.m_nDof_x && m_nDof_y==tens.m_nDof_y);
  this->add(tens, -1.0);
  return *this;
} 

CPTensor2Var& CPTensor2Var::operator *= (const double scale) {
  for(unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
    m_coeffs[iTerm] *= scale;    
  }
  return *this;
}

CPTensor2Var& CPTensor2Var::operator /= (const double scale) {
  assert(fabs(scale)>DBL_EPSILON);
  for(unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
    m_coeffs[iTerm] /= scale;    
  }
  return *this;
}

CPTensor2Var& CPTensor2Var::operator = (const CPTensor2Var& tens) {
  m_rank = tens.m_rank;
  m_nDof_x = tens.m_nDof_x;
  m_nDof_y = tens.m_nDof_y;
  m_coeffs = tens.m_coeffs;
  m_terms_x = tens.m_terms_x;
  m_terms_y = tens.m_terms_y;
  // m_terms_x.resize(tens.m_rank);
  // for (unsigned int iTerm=0; iTerm<m_terms_x.size(); ++iTerm) {
  //   m_terms_x[iTerm].copyVecFrom(tens.m_terms_x[iTerm]);
  // }
  // m_terms_y.resize(tens.m_rank);
  // for (unsigned int iTerm=0; iTerm<m_terms_y.size(); ++iTerm) {
  //   m_terms_y[iTerm].copyVecFrom(tens.m_terms_y[iTerm]);
  // }
  m_comm = tens.m_comm;
  return *this;
}


/*
*  Friend methods
*/

double dot(const CPTensor2Var& tens1, const CPTensor2Var& tens2) {
  assert(tens1.m_nDof_x==tens2.m_nDof_x && tens1.m_nDof_y==tens2.m_nDof_y);

  double value = 0.0;
  for(unsigned int iTerm=0; iTerm<tens1.m_rank; ++iTerm) {
    for(unsigned int jTerm=0; jTerm<tens2.m_rank; ++jTerm) {
      value += tens1.m_coeffs[iTerm]*tens2.m_coeffs[jTerm]*scalProd(tens1.m_terms_x[iTerm], tens2.m_terms_x[jTerm])*scalProd(tens1.m_terms_y[iTerm], tens2.m_terms_y[jTerm]);
    }
  }
  return value;
}

double norm(const CPTensor2Var& tens) {
  double value = 0.0;
  for(unsigned int iTerm=0; iTerm<tens.m_rank; ++iTerm) {
    value += tens.m_coeffs[iTerm]*tens.m_coeffs[iTerm]*scalProd(tens.m_terms_x[iTerm], tens.m_terms_x[iTerm])*scalProd(tens.m_terms_y[iTerm], tens.m_terms_y[iTerm]);
    for(unsigned int jTerm=iTerm+1; jTerm<tens.m_rank; ++jTerm) {
      value += 2.0*tens.m_coeffs[iTerm]*tens.m_coeffs[jTerm]*scalProd(tens.m_terms_x[iTerm], tens.m_terms_x[jTerm])*scalProd(tens.m_terms_y[iTerm], tens.m_terms_y[jTerm]);
    }
  }
  return sqrt(fmax(0.0, value));
}

