// Header for the CPTensor class

#ifndef CPTensor_h
#define CPTensor_h

#include "../tensor.h"
#include "../linAlg/vec.h"
#include "../linAlg/mat.h"

class CPTensor: public tensor{
private:
  unsigned int m_rank;
  vector<vector<vec> > m_terms;
  vector<double> m_coeffs;
  bool m_isInitialised;

public:
  CPTensor(){};
  ~CPTensor(){};
  void clear(){
    m_coeffs.clear();
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        m_terms[iTerm][iVar].clear();
      }
    }
  }

  // overloaded constructors and init:
  CPTensor(unsigned int, vector<unsigned int>, MPI_Comm);
  CPTensor(vector<unsigned int>, MPI_Comm);
  CPTensor(vector<vector<vec> >&, vector<double>&, MPI_Comm);
  CPTensor(vector<vector<vec> >&, MPI_Comm);
  CPTensor(vector<vec>&, double, MPI_Comm);
  CPTensor(vector<vec>&, MPI_Comm);

  void init(vector<unsigned int>, MPI_Comm); // constructor after empty
  void finalize(){
    get_rank();
    m_isInitialised=true;
  }

  // -- ACCESS FUNCTIONS --
  unsigned int rank(){return m_rank;}
  vector<double> coeffs(){return m_coeffs;}
  double coeffs(unsigned int iTerm){return m_coeffs[iTerm];}
  vector<vector<vec> > terms(){return m_terms;}
  vector<vec> terms(unsigned int iTerm){return m_terms[iTerm];}
  vec terms(unsigned int iTerm, unsigned int iVar){return m_terms[iTerm][iVar];}


  // -- SETTERS --
  inline void set_rank(unsigned int theRank){
    m_rank = theRank;
    m_coeffs.resize(m_rank);
    m_terms.resize(m_rank);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      m_terms[iTerm].resize(m_nVar);
    }
  };
  inline void set_terms(vector<vector<vec> >& theTerms){
    m_terms = theTerms;
  }
  // if already initialised!
  inline void set_terms(unsigned int iTerm, vector<vec>& iThPureTensProd){
    assert(iTerm<m_rank);
    m_terms[iTerm] = iThPureTensProd;
  }
  inline void set_terms(unsigned int iTerm, unsigned int idVar, vec& theTerm){
    assert(iTerm<m_rank);
    assert(idVar<m_nVar);
    (m_terms[iTerm])[idVar] = theTerm;
  }

  inline void replaceTerm(unsigned int iTerm, vector<vec> theTerm){
    assert(iTerm<m_rank);
    for(unsigned int iVar=0; iVar<m_terms[iTerm].size(); iVar++){
      (m_terms[iTerm])[iVar].clear();
    }
    m_terms[iTerm] = theTerm;
  }

  inline void replaceTerm(unsigned int iTerm, unsigned int idVar, vec& theTerm){
    assert(iTerm<m_rank);
    assert(idVar<m_nVar);
    (m_terms[iTerm])[idVar].clear();
    (m_terms[iTerm])[idVar] = theTerm;
  }

  inline void set_coeffs(vector<double>& theCoeffs){m_coeffs = theCoeffs;};
  inline void set_coeffs(unsigned int iTerm, double val){m_coeffs[iTerm] = val;};
  inline void setIsInitialised(bool state){
		m_isInitialised = state;
	}

  // set one tensor element:
  inline void set_tensorElement(vector<unsigned int> ind, double value){
    vector<vec> termToAdd(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      termToAdd[iVar].init(m_nDof_var[iVar], m_comm);
      termToAdd[iVar].setVecEl(ind[iVar], 1.0);
      termToAdd[iVar].finalize();
    }
    // evaluate the current value of the tensor in ind:
    double actualValue = eval(ind);
    double coeffToAdd = value - actualValue;
    m_coeffs.push_back(coeffToAdd);
    m_terms.push_back(termToAdd);
    m_rank += 1;
  }


  // get rank:
  inline void get_rank(){m_rank = m_terms.size();}

  /* eval function (base version)
    - input: the multi-index
    - output: the value
  */
  void eval(const vector<unsigned int>& ind, double& val){
    get_rank();
    val = 0.0;
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double toAdd = m_coeffs[iTerm];
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        toAdd = toAdd * m_terms[iTerm][idVar].getVecEl(ind[idVar]);
      }
      val = val + toAdd;
    }
  }


  /* eval function (overloaded to return double)
    - input: the multi-index
    - output: the value
  */
  double eval(const vector<unsigned int>& ind){
    get_rank();
    double val = 0.0;
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double toAdd = m_coeffs[iTerm];
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        toAdd = toAdd * m_terms[iTerm][idVar].getVecEl(ind[idVar]);
      }
      val = val + toAdd;
    }
    return val;
  }


  /* eval function (overloaded to be variadic)
    - input: the multi-index
    - output: the value
  */
  double eval(unsigned int iComp,...){
    get_rank();
    vector<unsigned int> indices(m_nVar);
    va_list ap;
    va_start(ap, iComp);
    indices[0] = iComp;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      indices[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);
    double val = 0.0;
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double toAdd = m_coeffs[iTerm];
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        toAdd = toAdd * m_terms[iTerm][idVar].getVecEl(indices[idVar]);
      }
      val = val + toAdd;
    }
    return val;
  }


  // Nested proxy class for accessing and manipulating tensor elements
  // overloading operator () in assignement through a proxy class:
  class Proxy : public tensor
  {
    vector<vector<vec> >* x;
    vector<double>* c;
    vector<unsigned int> idx;
    unsigned int x_rank;
  public:

      inline void get_rank(){x_rank = x->size();}

      // Set Tensor Elements:
      inline void set_tensorElement(vector<unsigned int> ind, double val){
        vector<vec> termToAdd(m_nVar);
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          termToAdd[iVar].init(m_nDof_var[iVar], m_comm);
          termToAdd[iVar].setVecEl(ind[iVar], 1.0);
          termToAdd[iVar].finalize();
        }
        // evaluate the current value of the tensor in ind:
        double actualValue = eval(ind);
        double coeffToAdd = val - actualValue;
        c->push_back(coeffToAdd);
        x->push_back(termToAdd);
        x_rank = x_rank + 1;
      }

      // overloaded to return a double:
      double eval(const vector<unsigned int>& ind){
        get_rank();
        double val = 0.0;
        for(unsigned int iTerm=0; iTerm< x_rank; iTerm++){
          double toAdd = (*c)[iTerm];
          for(unsigned int idVar=0; idVar<m_nVar; idVar++){
            toAdd = toAdd * (*x)[iTerm][idVar].getVecEl(ind[idVar]);
          }
          val = val + toAdd;
        }
        return val;
      }

      // Constructor of the proxy class:
      Proxy(vector<unsigned int> idx, vector<unsigned int> dofPerDim, vector<vector<vec> >* x, vector<double>* c, unsigned int x_rank, MPI_Comm theComm) : idx(idx), x(x), c(c), x_rank(x_rank){
        m_comm = theComm;
        m_nVar = idx.size();
        m_nDof_var.resize(m_nVar);
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          m_nDof_var[iVar] = dofPerDim[iVar];
        }
        compute_minc();
      }

      // equal operator overload:
      inline double operator= (double value) {
        set_tensorElement(idx, value);
        return value;
      }

      // Overloading the double operator for assignement:
      operator double(){
        double toBeReturned = eval(idx);
        return toBeReturned;
      }
  };

  // Calling the proxy object through variadic:
  Proxy operator() (unsigned int I,...) {

    vector<unsigned int> ind(m_nVar);
    va_list ap;
    va_start(ap, I);
    ind[0] = I;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      ind[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);
    return Proxy(ind, m_nDof_var, &m_terms, &m_coeffs, m_rank, m_comm);
  }


  // Methods and operations for CPTensors:
  // COPY Tensor Structure:
	void copyTensorStructFrom(CPTensor&);

	// COPY the full tensor:
	void copyTensorFrom(CPTensor&);

  // OPERATIONS on CP tensor (generic):
	bool hasSameStructure(CPTensor&);
	bool isEqualTo(CPTensor&);
  bool operator == (CPTensor&);
	void sum(CPTensor&, double, bool);
  void operator += (CPTensor&);
	void multiplyByScalar(double);
  void operator *= (double);
	void shiftByScalar(double);
  void operator += (double);
	void extractSubtensor(vector<unsigned int>, CPTensor&);
  CPTensor extractSubtensor(vector<unsigned int>);
  void extractSubtensor(vector<vector<unsigned int> >, CPTensor&);
  CPTensor extractSubtensor(vector<vector<unsigned int> >);
	void assignSubtensor(vector<unsigned int>, CPTensor&);
	void reshape(vector<unsigned int>, CPTensor&, bool);
  CPTensor reshape(vector<unsigned int>, bool);
  CPTensor modeIContraction(unsigned int, vec);
  void modeIContraction(unsigned int, vec, CPTensor&);

  // OPERATIONS on CP tensor (specific):
  void reorderVars(vector<unsigned int>);
  void addPureTensorTerm(vector<vec>&, double coeff);
  void inplaceModeIContraction(unsigned int, vec&);
  void contractEverythingExceptI(unsigned int, vector<vec>&, vec&);
  double scalarProdTerms(vector<vec>&, vector<vec>&, vector<mat>&);
  double scalarProdTerms(vector<vec>&, vector<vec>&);
  double scalarProd(CPTensor&, vector<mat>&);
  double scalarProd(CPTensor&);
  double norm2CP(vector<mat>&);
  double norm2CP();
  void compute_Unfolding_OneSvdTerm(unsigned int, double&, vec&);
  void compute_Unfolding_Svd(unsigned int, vector<double>&, vector<vec>&);
  void compute_Approx_Unfolding_Svd(unsigned int, vector<double>&, vector<vec>&);

	// - Compute unfolding -
	void computeUnfolding(const unsigned int, mat&);
  mat computeUnfolding(const unsigned int);
	void computeFibers(const unsigned int, vector<vec>&);
  vector<vec> computeFibers(const unsigned int);
	void evalUnfolding(const unsigned int,const unsigned int,const unsigned int,double&);
  double evalUnfolding(const unsigned int,const unsigned int,const unsigned int);

  // - I/O for CP Tensor -
  void save(string);
  void load(string);

};

#endif
