// CPTensorOperations Implementation
#include "CPTensorOperations.hpp"
#include "../linAlg/linearAlgebraOperations.h"


/* implementation of contraction with a vector */
CPTensor modeIContraction(unsigned int modeI, CPTensor& A, vec& b){
  CPTensor result = A.modeIContraction(modeI, b);
  result.finalize();
  return result;
}



/* implementation of sum operator*/
CPTensor operator + (CPTensor& A, CPTensor& B){
  assert(A.hasSameStructure(B));
  CPTensor result(A.nDof_var(), A.comm());
  result.copyTensorFrom(A);
  result.sum(B, 1.0, true);
  result.finalize();
  return result;
}

/* implementation of scalar product */
double scalProd(CPTensor& A, CPTensor&B){
  double val = A.scalarProd(B);
  return val;
}


/* compute the scalar product of two CP terms
  - inputs: the terms, the mass matrices
  - output: the scalar product
*/
double scalarProdTerms(vector<vec>& term_1, vector<vec>& term_2, vector<mat>& mass){
  assert(term_1.size() == term_2.size());
  const unsigned int m_nVar = term_1.size();
  assert(mass.size() == m_nVar);

  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    assert(term_1[idVar].size() == term_2[idVar].size());
  }

  double scalar = 1.0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    vec prod = mass[idVar] * term_1[idVar];
    double quadForm = scalProd(prod, term_2[idVar]);
    prod.clear();
  }
  return scalar;
};


/* compute the scalar product of two CP terms
  - inputs: the terms, the mass matrices
  - output: the scalar product
  REMARK: Masses are identities \ell^2 scalar product
*/
double scalarProdTerms(vector<vec>& term_1, vector<vec>& term_2){
  assert(term_1.size() == term_2.size());
  const unsigned int m_nVar = term_1.size();
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    assert(term_1[idVar].size() == term_2[idVar].size());
  }

  double scalar = 1.0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    double quadForm = scalProd(term_1[idVar], term_2[idVar]);
  }
  return scalar;
};


// I/O operations:

/* define a CP by loading a file
  - input: fileName
  - output: a CP tensor
*/
CPTensor loadCP(string fileName, MPI_Comm theComm){
  //load the log file:
  string logName = fileName + ".log";
  ifstream logFile(logName.c_str());
  vector<unsigned int> data;
  unsigned int datum;
  while(logFile >> datum){
    data.push_back(datum);
  }
  unsigned int m_rank = data[0];
  unsigned int m_nVar = data[1];
  vector<unsigned int> m_nDof_var(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = data[iVar+2];
  }
  logFile.close();

  // defining the tensor:
  CPTensor toBeReturned(m_nDof_var, theComm);
  toBeReturned.set_rank(m_rank);

  //load the modes:
  vector<vector<vec> > tensTerms(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    tensTerms[iTerm].resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      string modeName = fileName + "_" + to_string(iTerm) + "_" + to_string(iVar) + ".dat";
      tensTerms[iTerm][iVar].load(modeName);
    }
  }
  toBeReturned.set_terms(tensTerms);

  // load the coefficients:
  string coeffName = fileName + "_coeffs.dat";
  ifstream coeffFile(coeffName.c_str());
  double coeff;
  vector<double> m_coeffs(m_rank);
  unsigned int cc = 0;
  while(coeffFile >> coeff){
    m_coeffs[cc] = coeff;
    cc += 1;
  }
  coeffFile.close();
  toBeReturned.set_coeffs(m_coeffs);

  return toBeReturned;
}
