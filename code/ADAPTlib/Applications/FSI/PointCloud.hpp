// Header file for PointCloud

#ifndef PointCloud_hpp
#define PointCloud_hpp

#include "../../Applications/FSI/Include.hpp"

class PointCloud {

private:

  unsigned int m_dim;
  unsigned int m_nOfPt;
  std::vector<std::vector<double>> m_pts;

  void uniform(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt);

public:

  PointCloud();
  PointCloud(const unsigned int dim, const unsigned int nOfPt=0);
  PointCloud(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt);
  PointCloud(const std::string& filename);
  PointCloud(const PointCloud& pts);
  ~PointCloud();

  void init(const unsigned int dim, const unsigned int nOfPt=0);
  void init(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt);
  void init(const std::string& filename);
  void init(const PointCloud& pts);
  void clear();

  inline const unsigned int dim() const;
  inline const unsigned int nOfPt() const;
  inline double& operator () (const unsigned int i, const unsigned int j);
  inline const double& operator () (const unsigned int i, const unsigned int j) const;
  inline std::vector<double>& operator () (const unsigned int i);
  inline const std::vector<double>& operator () (const unsigned int i) const;

  void load(const std::string& filename);
  void print() const;
  void save(const std::string& filename) const;
  void resize(const unsigned int nOfPt);
  void erase(const unsigned int i);
  void insert(const unsigned int i, const std::vector<double>& pt);
  void pop_back();
  void push_back(const std::vector<double>& pt);

  PointCloud& operator = (const PointCloud& pts);

};

inline const unsigned int PointCloud::dim() const {
  return m_dim;
}

inline const unsigned int PointCloud::nOfPt() const {
  return m_nOfPt;
}

inline double& PointCloud::operator () (const unsigned int i, const unsigned int j) {
  assert(i<m_nOfPt && j<m_dim);
  return m_pts[i][j];
}

inline const double& PointCloud::operator () (const unsigned int i, const unsigned int j) const {
  assert(i<m_nOfPt && j<m_dim);
  return m_pts[i][j];
}

inline std::vector<double>& PointCloud::operator () (const unsigned int i) {
  assert(i<m_nOfPt);
  return m_pts[i];
}

inline const std::vector<double>& PointCloud::operator() (const unsigned int i) const {
  assert(i<m_nOfPt);
  return m_pts[i];
}

#endif


// // Header file for PointCloud

// #ifndef PointCloud_hpp
// #define PointCloud_hpp

// #include "../../Applications/FSI/Include.hpp"

// class PointCloud {

// private:

//   unsigned int m_dim;
//   unsigned int m_nOfPt;
//   std::vector<std::vector<double>> m_pts;

//   void uniform_sampling(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt);
//   void uniform_box(const unsigned int nOfPt, std::vector<unsigned int>& box);

// public:

//   PointCloud();
//   PointCloud(const unsigned int dim, const unsigned int nOfPt=0);
//   PointCloud(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt);
//   PointCloud(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt, std::vector<unsigned int>& box);
//   PointCloud(const std::string& filename);
//   PointCloud(const PointCloud& pts);
//   ~PointCloud();

//   void init(const unsigned int dim, const unsigned int nOfPt=0);
//   void init(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt);
//   void init(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt, std::vector<unsigned int>& box);
//   void init(const std::string& filename);
//   void init(const PointCloud& pts);
//   void clear();

//   inline const unsigned int dim() const;
//   inline const unsigned int nOfPt() const;
//   inline double& operator () (const unsigned int i, const unsigned int j);
//   inline const double& operator () (const unsigned int i, const unsigned int j) const;
//   inline std::vector<double>& operator () (const unsigned int i);
//   inline const std::vector<double>& operator () (const unsigned int i) const;

//   void load(const std::string& filename);
//   void print() const;
//   void save(const std::string& filename) const;
//   void resize(const unsigned int nOfPt);
//   void erase(const unsigned int i);
//   void insert(const unsigned int i, const std::vector<double>& pt);
//   void pop_back();
//   void push_back(const std::vector<double>& pt);

//   PointCloud& operator = (const PointCloud& pts);

// };

// inline const unsigned int PointCloud::dim() const {
//   return m_dim;
// }

// inline const unsigned int PointCloud::nOfPt() const {
//   return m_nOfPt;
// }

// inline double& PointCloud::operator () (const unsigned int i, const unsigned int j) {
//   assert(i<m_nOfPt && j<m_dim);
//   return m_pts[i][j];
// }

// inline const double& PointCloud::operator () (const unsigned int i, const unsigned int j) const {
//   assert(i<m_nOfPt && j<m_dim);
//   return m_pts[i][j];
// }

// inline std::vector<double>& PointCloud::operator () (const unsigned int i) {
//   assert(i<m_nOfPt);
//   return m_pts[i];
// }

// inline const std::vector<double>& PointCloud::operator() (const unsigned int i) const {
//   assert(i<m_nOfPt);
//   return m_pts[i];
// }

// #endif
