/* 
*  Header for the Include
*/

#ifndef Include_hpp
#define Include_hpp

#include "../../genericInclude.h"

extern "C" {  
  void dgesvd_(const char* jobu, const char* jobvt, const int* m, const int* n, double* A, const int* lda, double* S, double* U, const int* ldu, double* Vt, const int* ldvt, double* work, const int* lwork, int* info);
}


template <typename T>
inline T max(const std::vector<T>& v) {
  assert(0<v.size());
  return *std::max_element(v.begin(), v.end());
};

template<typename T>
inline T sum(const std::vector<T>& v) {
  return std::accumulate(v.begin(), v.end(), (T)0);
}

inline std::vector<int> linspace(const int first, const unsigned int n) {
  std::vector<int> v(n);
  for (int i=0; i<n; ++i) {
    v[i] = first+i;
  }
  return v;
}

inline std::vector<double> linspace(const double a, const double b, const unsigned int n) {
  std::vector<double> v(n);
  if (n==1) {
    v[0] = (a+b)/2.;
  } else {
    for (unsigned int i=0; i<n; ++i) {
      v[i] = a+i*(b-a)/(n-1.0);
    }
  }
  return v;
}

#endif
