/* 
*  Header for the VectorSet class
*/

#ifndef VectorSet_hpp
#define VectorSet_hpp

#include "../../Applications/FSI/Include.hpp"
#include "../../Applications/FSI/Vector.hpp"

class VectorSet {

  friend double dot(const VectorSet& M1, const VectorSet& M2);
  friend std::vector<double> vecnorm(const VectorSet& M);
  friend double norm(const VectorSet& M);

private:

  std::vector<Vector> m_data;
  unsigned int m_nRows;
  unsigned int m_nCols;
  MPI_Comm m_comm;
  bool m_isInitialised;

public:

  VectorSet();
  VectorSet(const unsigned int nRows, const unsigned int nCols, const MPI_Comm& comm=PETSC_COMM_WORLD);
  VectorSet(const unsigned int nRows, const unsigned int nCols, const double value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  VectorSet(const VectorSet& M, const unsigned int low, const unsigned int high);
  // VectorSet(const VectorSet& M, const Matrix& P);
  VectorSet(const VectorSet& M);
  ~VectorSet();

  void init(const unsigned int nRows, const unsigned int nCols, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const unsigned int nRows, const unsigned int nCols, const double value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const VectorSet& M, const unsigned int low, const unsigned int high);
  // void init(const VectorSet& M, const Matrix& P);
  void init(const VectorSet& M);
  void clear();

  inline Vector& operator () (const unsigned int j);
  inline const Vector& operator () (const unsigned int j) const;
  inline unsigned int& nRows();
  inline const unsigned int& nRows() const;
  inline const unsigned int nCols() const;
  inline const MPI_Comm& comm() const;
  inline const bool isInitialised() const;
  inline const bool isAssembled() const;

  double getMatEl(const unsigned int i, const unsigned int j) const;
  void setMatEl(const unsigned int i, const unsigned int j, const double value, const bool PAR=true);
  void finalize();
  void add(const VectorSet& M, const double scale=1.0);
  void extract(const unsigned int low, const unsigned int high, VectorSet& M) const;
  void extract(const unsigned int low1, const unsigned int high1, const unsigned int low2, const unsigned int high2, VectorSet& M) const;
  void copy(const VectorSet& M);
  void print() const;
  void resize(const unsigned int nCols);
  void push_back(const Vector& v);
  void push_back(const VectorSet& M);
  void erase(const unsigned int j);

  VectorSet& operator += (const VectorSet& M);
  VectorSet& operator -= (const VectorSet& M);
  VectorSet& operator *= (const double scale);
  VectorSet& operator /= (const double scale);
  VectorSet& operator = (const double value);
  VectorSet& operator = (const VectorSet& M);

};

inline Vector& VectorSet::operator () (const unsigned int j) {
  return m_data[j];
};

inline const Vector& VectorSet::operator () (const unsigned int j) const {
  return m_data[j];
};

inline unsigned int& VectorSet::nRows() {
  return m_nRows;
};

inline const unsigned int& VectorSet::nRows() const {
  return m_nRows;
};

inline const unsigned int VectorSet::nCols() const {
  return m_nCols;
};

inline const MPI_Comm& VectorSet::comm() const {
  return m_comm;
};

inline const bool VectorSet::isInitialised() const {
  return m_isInitialised;
};

inline const bool VectorSet::isAssembled() const {
  if (!m_isInitialised || m_data.size()!=m_nCols) {
    return false;
  }
  for (unsigned int i=0; i<m_nCols; ++i) {
    if (!m_data[i].isAssembled() || m_data[i].size()!=m_nRows) {
      return false;
    }
  }
  return true;
};

#endif
