#include "VectorSet.hpp"


/* 
*  Constructors/Destructors
*/

VectorSet::VectorSet() : m_nRows(0), m_nCols(0), m_comm(PETSC_COMM_WORLD), m_isInitialised(false) {}

VectorSet::VectorSet(const unsigned int nRows, const unsigned int nCols, const MPI_Comm& comm) : m_isInitialised(false) {
  this->init(nRows, nCols, comm);
}

VectorSet::VectorSet(const unsigned int nRows, const unsigned int nCols, const double value, const MPI_Comm& comm) : m_isInitialised(false) {
  this->init(nRows, nCols, value, comm);
}

VectorSet::VectorSet(const VectorSet& M, const unsigned int low, const unsigned int high) : m_isInitialised(false) {
  assert(M.isAssembled() && low<=high && high<=M.m_nRows);
  this->init(M, low, high);
}

// VectorSet::VectorSet(const VectorSet& M, const Matrix& P) : m_isInitialised(false) {
//   assert(M.isAssembled() && P.isAssembled() && M.m_nRows==P.nCols());
//   this->init(M, P);
// }

VectorSet::VectorSet(const VectorSet& M) : m_isInitialised(false) {
  assert(M.isAssembled());
  this->init(M);
}

VectorSet::~VectorSet() {
  this->clear();
}

void VectorSet::init(const unsigned int nRows, const unsigned int nCols, const MPI_Comm& comm) {
  m_nRows = nRows;
  m_nCols = nCols;
  m_comm = comm;
  m_data.resize(m_nCols);
  for (unsigned int j=0; j<m_nCols; ++j) {
    m_data[j].init(m_nRows, m_comm);
  }
  m_isInitialised = true;
}

void VectorSet::init(const unsigned int nRows, const unsigned int nCols, const double value, const MPI_Comm& comm) {
  m_nRows = nRows;
  m_nCols = nCols;
  m_comm = comm;
  m_data.resize(m_nCols);
  for (unsigned int j=0; j<m_nCols; ++j) {
    m_data[j].init(m_nRows, value, m_comm);
  }
  m_isInitialised = true;
}

void VectorSet::init(const VectorSet& M, const unsigned int low, const unsigned int high) {
  assert(M.isAssembled() && low<=high && high<=M.m_nRows);
  m_nRows = high-low;
  m_nCols = M.m_nCols;
  m_comm = M.m_comm;
  m_data.resize(m_nCols);
  for (unsigned int j=0; j<m_nCols; ++j) {
    m_data[j].init(M.m_data[j], low, high);
  }
  m_isInitialised = true;
}

// void VectorSet::init(const VectorSet& M, const Matrix& P) {
//   assert(M.isAssembled() && P.isAssembled() && M.m_nRows==P.nCols());
//   m_nRows = P.nRows();
//   m_nCols = M.m_nCols;
//   m_comm = M.m_comm;
//   m_data.resize(m_nCols);
//   for (unsigned int j=0; j<m_nCols; ++j) {
//     m_data[j].init(M.m_data[j], low, high);
//   }
//   m_isInitialised = true;
// }

void VectorSet::init(const VectorSet& M) {
  assert(M.isAssembled());
  m_nRows = M.m_nRows;
  m_nCols = M.m_nCols;
  m_comm = M.m_comm;
  m_data.resize(m_nCols);
  for (unsigned int j=0; j<m_nCols; ++j) {
    m_data[j].init(M.m_data[j]);
  }
  m_isInitialised = true;
}

void VectorSet::clear() {
  if (m_isInitialised) {
    for (unsigned int j=0; j<m_nCols; ++j) {
      m_data[j].clear();
    }
    m_data.clear();
    m_nRows = 0;
    m_nCols = 0;
    m_isInitialised = false;
  }
}


/* 
*  Methods
*/

double VectorSet::getMatEl(const unsigned int i, const unsigned int j) const {
  assert(this->isAssembled() && i<m_nRows && j<m_nCols);
  return m_data[j].getVecEl(i);
}

void VectorSet::setMatEl(const unsigned int i, const unsigned int j, const double value, const bool PAR) {
  assert(m_isInitialised && i<m_nRows && j<m_nCols);
  m_data[j].setVecEl(i, value, PAR);
}

void VectorSet::finalize() {
  assert(m_isInitialised);
  for (unsigned int j=0; j<m_nCols; ++j) {
    m_data[j].finalize();
  }
}

void VectorSet::add(const VectorSet& M, const double scale) {
  assert(this->isAssembled() && M.isAssembled() && m_nRows==M.m_nRows && m_nCols==M.m_nCols && m_comm==M.m_comm);
  if (fabs(scale)!=0.0) {
    for (unsigned int j=0; j<m_nCols; ++j) {
      m_data[j].add(M.m_data[j], scale);
    }
  }
}

void VectorSet::extract(const unsigned int low, const unsigned int high, VectorSet& M) const {
  assert(this->isAssembled() && M.isAssembled() && low<=high && high<=m_nRows && M.m_nRows==(high-low) && m_nCols==M.m_nCols);
  for (unsigned int j=0; j<M.m_nCols; ++j) {
    m_data[j].extract(low, high, M.m_data[j]);
  }
}

void VectorSet::extract(const unsigned int low1, const unsigned int high1, const unsigned int low2, const unsigned int high2, VectorSet& M) const {
  assert(this->isAssembled() && M.isAssembled() && low1<=high1 && high1<=m_nRows && M.m_nRows==(high1-low1) && low2<=high2 && high2<=m_nCols && M.m_nCols==(high2-low2));
  for (unsigned int j=0; j<M.m_nCols; ++j) {
    m_data[j+low2].extract(low1, high1, M.m_data[j]);
  }
}

void VectorSet::copy(const VectorSet& M) {
  assert(this->isAssembled() && M.isAssembled() && m_nRows==M.m_nRows && m_nCols==M.m_nCols);
  for (unsigned int j=0; j<m_nCols; ++j) {
    m_data[j].copy(M.m_data[j]);
  }
}

void VectorSet::print() const {
  assert(this->isAssembled());
  for (unsigned int j=0; j<m_nCols; ++j) {
    m_data[j].print();
  }
}

void VectorSet::resize(const unsigned int nCols) {
  assert(this->isAssembled());
  m_data.resize(nCols);
  for (unsigned int j=m_nCols; j<nCols; ++j) {
    m_data[j].init(m_nRows, 0.0, m_comm);
  }
  m_nCols = nCols;
}

void VectorSet::push_back(const Vector& v) {
  assert(this->isAssembled() && v.isAssembled());
  m_data.resize(m_nCols+1);
  m_data[m_nCols].init(v);
  m_nCols += 1;
}

void VectorSet::push_back(const VectorSet& M) {
  assert(this->isAssembled() && M.isAssembled());
  m_data.resize(m_nCols+M.m_nCols);
  for (unsigned int j=0; j<M.m_nCols; ++j) {
    m_data[m_nCols+j].init(M.m_data[j]);
  }
  m_nCols += M.m_nCols;
}

void VectorSet::erase(const unsigned int j) {
  assert(this->isAssembled() && j<m_nCols);
  m_data.erase(std::next(m_data.begin(), j));
  m_nCols -= 1;
}


/* 
*  Operators
*/

VectorSet& VectorSet::operator += (const VectorSet& M) {
  assert(this->isAssembled() && M.isAssembled() && m_nRows==M.m_nRows && m_nCols==M.m_nCols && m_comm==M.m_comm);
  this->add(M, 1.0);
  return *this;
}

VectorSet& VectorSet::operator -= (const VectorSet& M) {
  assert(this->isAssembled() && M.isAssembled() && m_nRows==M.m_nRows && m_nCols==M.m_nCols && m_comm==M.m_comm);
  this->add(M, -1.0);
  return *this;
}

VectorSet& VectorSet::operator *= (const double scale) {
  assert(this->isAssembled());
  for (unsigned int j=0; j<m_nCols; ++j) {
    m_data[j] *= scale;
  }
  return *this;
}

VectorSet& VectorSet::operator /= (const double scale) {
  assert(this->isAssembled() && fabs(scale)>DBL_EPSILON);
  for (unsigned int j=0; j<m_nCols; ++j) {
    m_data[j] /= scale;
  }
  return *this;
}

VectorSet& VectorSet::operator = (const double value) {
  assert(this->isAssembled());
  for (unsigned int j=0; j<m_nCols; ++j) {
    m_data[j] = value;
  }
  return *this;
}

VectorSet& VectorSet::operator = (const VectorSet& M) {
  assert(M.isAssembled());
  m_nRows = M.m_nRows;
  m_nCols = M.m_nCols;
  m_comm = M.m_comm;
  m_data.resize(M.m_nCols);
  for (unsigned int j=0; j<m_nCols; ++j) {
    m_data[j] = M.m_data[j];
  }
  m_isInitialised = true;
  return *this;
}


/*
*  Friend methods
*/

double dot(const VectorSet& M1, const VectorSet& M2) {
  assert(M1.isAssembled() && M2.isAssembled() && M1.nRows()==M2.nRows() && M1.nCols()==M2.nCols());
  double value = 0.0;
  for (unsigned int j=0; j<M1.nCols(); ++j) {
    value += dot(M1(j), M2(j));
  }
  return value;
}

std::vector<double> vecnorm(const VectorSet& M) {
  assert(M.isAssembled());
  std::vector<double> v(M.nCols());
  for (unsigned int j=0; j<M.nCols(); ++j) {
    v[j] += norm(M(j));
  }
  return v;
}

double norm(const VectorSet& M) {
  assert(M.isAssembled());
  double value = 0.0;
  for (unsigned int j=0; j<M.nCols(); ++j) {
    value += pow(norm(M(j)), 2);
  }
  return value;
}
