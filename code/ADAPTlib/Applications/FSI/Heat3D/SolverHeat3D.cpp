#include "SolverHeat3D.hpp"


/* 
*  Constructors/Destructors
*/

SolverHeat3D::SolverHeat3D() : m_isInitialised(false) {}

SolverHeat3D::SolverHeat3D(const Testcase& testcase, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm) : m_isInitialised(false) {
  assert(lbParameter.size()==ubParameter.size());
  this->init(testcase, lbParameter, ubParameter, comm);
}

SolverHeat3D::~SolverHeat3D() {
	this->clear();
}

void SolverHeat3D::init(const Testcase& testcase, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm) {
	assert(lbParameter.size()==ubParameter.size());
	m_comm = comm;
	m_testcase.init(testcase);
  this->load_data(lbParameter, ubParameter);
	m_isInitialised = true;
}

void SolverHeat3D::clear() {
	if (m_isInitialised) {
		m_testcase.clear();
		m_operatorA.clear();
	  m_operatorB.clear();
	  m_tensC_coeffs.clear();
	  m_tensC_terms_x.clear();
	  m_P.clear();
	  m_isInitialised = false;
	}
}


/* 
*  Methods
*/

void SolverHeat3D::load_data(const std::vector<double>& lbParameter, const std::vector<double>& ubParameter) {

	// Assemble m_operatorA
	m_operatorA.init({m_testcase.nFluid()}, {m_testcase.nFluid()}, 0, 0, 3, m_comm);
	m_operatorA(0, 0).init(m_testcase.folderpath_input_data() + "K.dat", m_comm);
	m_operatorA(1, 0).init(m_testcase.folderpath_input_data() + "R.dat", m_comm);
	m_operatorA(2, 0).init(m_testcase.folderpath_input_data() + "M.dat", m_comm);
	m_operatorA(2, 0) *= (1/m_testcase.dt());

	// Assemble m_operatorB
	m_operatorB.init({m_testcase.nFluid()}, {m_testcase.nFluid()}, 0, 0, 1, m_comm);
	m_operatorB(0, 0).init(m_testcase.folderpath_input_data() + "M.dat", m_comm);
	m_operatorB(0, 0) *= (1/m_testcase.dt());

	// Assemble m_tensC
	m_tensC_coeffs.resize(5, 1.0);
	m_tensC_terms_x.init(m_testcase.nFluid(), 5, m_comm);
	m_tensC_terms_x(0).init(m_testcase.folderpath_input_data() + "b10.dat", m_comm);
	m_tensC_terms_x(1).init(m_testcase.folderpath_input_data() + "b20.dat", m_comm);
	m_tensC_terms_x(2).init(m_testcase.folderpath_input_data() + "b30.dat", m_comm);
	m_tensC_terms_x(3).init(m_testcase.folderpath_input_data() + "b40.dat", m_comm);
	m_tensC_terms_x(4).init(m_testcase.folderpath_input_data() + "b50.dat", m_comm);
	m_tensC_terms_x.finalize();

	// Preconditioner matrix
	m_P.init(m_operatorA(2, 0));
	m_P.add(m_operatorA(0, 0), (lbParameter[0]+ubParameter[0])/2.0);
	m_P.add(m_operatorA(1, 0), (lbParameter[1]+ubParameter[1])/2.0);
	m_P.inv();

}


void SolverHeat3D::solve(const PointCloud& parameter, const double t, const CPTensor2Vars& sol_old, CPTensor2Vars& sol) {

	assert(m_isInitialised && sol_old.isAssembled());

	// Matrices containing the parameter values on the diagonal.
	std::vector<Matrix> theta(3);
	for (unsigned int i=0; i<2; ++i) {
		theta[i].init(parameter.nOfPt(), parameter.nOfPt(), m_comm);
		for (unsigned int j=0; j<parameter.nOfPt(); ++j) {
			theta[i].setMatEl(j, j, parameter(j, i));
		}
		theta[i].finalize();
	}
	theta[2].init(eye(parameter.nOfPt(), parameter.nOfPt(), m_comm));

	CPTensor2Vars rhs({m_testcase.nFluid()}, parameter.nOfPt(), {sol_old.terms_x().nCols()*m_operatorB.nTerms()}, m_comm);
	rhs.finalize();

	m_operatorB.nDof_y_in() = parameter.nOfPt();
	m_operatorB.nDof_y_out() = parameter.nOfPt();
	m_operatorB(0, 1).init(theta[2]);
	m_operatorB.apply(sol_old, rhs);

	CPTensor2Vars tensC({m_testcase.nFluid()}, parameter.nOfPt(), {5}, m_comm);
	tensC.coeffs() = m_tensC_coeffs;
	tensC.terms_x() = m_tensC_terms_x;
	for (unsigned int i=0; i<tensC.terms_y().nRows(); ++i) {
		for (unsigned int j=0; j<tensC.terms_y().nCols(); ++j) {
			tensC.terms_y().setMatEl(i, j, parameter(i, j+2));
		}
	}
	tensC.finalize();
	rhs.add(tensC, 1.0);

	m_operatorA.nDof_y_in() = parameter.nOfPt();
	m_operatorA.nDof_y_out() = parameter.nOfPt();
	m_operatorA(0, 1).init(theta[0]);
	m_operatorA(1, 1).init(theta[1]);
	m_operatorA(2, 1).init(theta[2]);
	sol = sol_old;
	unsigned int it = gmres(m_operatorA, sol, rhs, m_testcase.tolr_gmres(), m_testcase.itmax_gmres(), m_P, m_testcase.tolc_gmres(), m_comm);

	PetscPrintf(m_comm, "Time: %f, number of iteration: %d, rank: %d\n", t, it, sol.rank(0));

}
