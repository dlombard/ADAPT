#ifndef FSIHeat3D_hpp
#define FSIHeat3D_hpp

#include "../../../Applications/FSI/Vector.hpp"
#include "../../../Applications/FSI/VectorSet.hpp"
#include "../../../Applications/FSI/Matrix.hpp"
#include "../../../Applications/FSI/Operations.hpp"
#include "../../../Applications/FSI/CPTensor2Vars.hpp"
#include "../../../Applications/FSI/OperatorCPTensor2Vars.hpp"
#include "../../../Applications/FSI/Testcase.hpp"
#include "../../../Applications/FSI/PointCloud.hpp"


class FSIHeat3D {

private:

  Testcase m_testcase;
  PointCloud m_parameter;
  std::vector<unsigned int> m_perm;
  MPI_Comm m_comm;
  OperatorCPTensor2Vars m_operatorA;
  OperatorCPTensor2Vars m_operatorB;
  CPTensor2Vars m_tensC;
  Matrix m_P;
  CPTensor2Vars m_sol;
  bool m_isInitialised;

  void load_data(const std::vector<double>& lbParameter, const std::vector<double>& ubParameter);
  void save_solution(const unsigned int k) const;

public:

  FSIHeat3D();
  FSIHeat3D(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm=PETSC_COMM_WORLD);
  ~FSIHeat3D();

  void init(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void solve(const bool write_solution=false);
  void check() const;
  void clear();

};

// unsigned int gmres(const OperatorCPTensor2Vars& operatorA, CPTensor2Vars& sol, const CPTensor2Vars& rhs, const double tolr, const unsigned int itmax, const Matrix& P, const double tolc, const MPI_Comm& comm);

#endif
