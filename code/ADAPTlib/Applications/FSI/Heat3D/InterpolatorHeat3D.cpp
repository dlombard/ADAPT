#include "InterpolatorHeat3D.hpp"


/* 
*  Constructors/Destructors
*/

InterpolatorHeat3D::InterpolatorHeat3D() : m_isInitialised(false) {}

InterpolatorHeat3D::InterpolatorHeat3D(const Testcase& testcase, const MPI_Comm& comm) {
  this->init(testcase, comm);
}

InterpolatorHeat3D::~InterpolatorHeat3D() {
	this->clear();
}

void InterpolatorHeat3D::init(const Testcase& testcase, const MPI_Comm& comm) {
	m_isInitialised = false;
	m_testcase.init(testcase);
	m_comm = comm;
	this->load_data();
}


void InterpolatorHeat3D::clear() {

	m_testcase.clear();
	m_sol_coeffs.clear();
	m_sol_terms_x.clear();
	m_U.clear();
	m_B.clear();
	m_BB.clear();

	for (unsigned int i=0; i<m_A.size(); ++i) {
		m_A[i].clear();
	}
	m_A.clear();

	for (unsigned int i=0; i<m_c.size(); ++i) {
		m_c[i].clear();
	}
	m_c.clear();

	for (unsigned int i=0; i<m_AA.size(); ++i) {
		m_AA[i].clear();
	}
	m_AA.clear();

	for (unsigned int i=0; i<m_cc.size(); ++i) {
		m_cc[i].clear();
	}
	m_cc.clear();

  m_isInitialised = false;

}


/* 
*  Methods
*/

void InterpolatorHeat3D::load_data() {

	m_AA.resize(3);
	m_AA[0].init(m_testcase.folderpath_input_data() + "M.dat", m_comm);
	m_AA[0] *= (1/m_testcase.dt());
  m_AA[1].init(m_testcase.folderpath_input_data() + "K.dat", m_comm);
  m_AA[2].init(m_testcase.folderpath_input_data() + "R.dat", m_comm);

  m_BB.init(m_testcase.folderpath_input_data() + "M.dat", m_comm);
  m_BB *= (1/m_testcase.dt());

  m_cc.resize(5);
  m_cc[0].init(m_testcase.folderpath_input_data() + "b10.dat", m_comm);
  m_cc[1].init(m_testcase.folderpath_input_data() + "b20.dat", m_comm);
  m_cc[2].init(m_testcase.folderpath_input_data() + "b30.dat", m_comm);
  m_cc[3].init(m_testcase.folderpath_input_data() + "b40.dat", m_comm);
  m_cc[4].init(m_testcase.folderpath_input_data() + "b50.dat", m_comm);

	m_A.resize(m_AA.size());
	m_c.resize(m_cc.size());

}


void InterpolatorHeat3D::update_precomputed_quantities() {

	// Compute terms_x
  m_sol_terms_x = m_U;

	// Compute coeffs
	m_sol_coeffs.resize(m_sol_terms_x.nCols(), 1.0);

	// Compute matrices A
	for (unsigned int i=0; i<m_A.size(); ++i) {
		m_A[i].init(m_sol_terms_x.nCols(), m_sol_terms_x.nCols(), 0.0, m_comm);
		MatTransposeMatMult(m_sol_terms_x, MatMatMult(m_AA[i], m_sol_terms_x), m_A[i]);
	}

	// Compute matrix B
	m_B.init(m_sol_terms_x.nCols(), m_sol_terms_x.nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_sol_terms_x, MatMatMult(m_BB, m_sol_terms_x), m_B);

	// Compute vectors c
	for (unsigned int i=0; i<m_c.size(); ++i) {
		m_c[i].init(m_sol_terms_x.nCols(), 0.0, m_comm);
		MatMultTranspose(m_sol_terms_x, m_cc[i], m_c[i]);
	}

	m_isInitialised = true;

	PetscPrintf(m_comm, "Dimension of the reduced basis: %d\n", m_U.nCols());

}


void InterpolatorHeat3D::update(const CPTensor2Vars& sol) {

	unsigned int rank = 0;
	double error1 = m_testcase.tolc_interpol(), error2 = 1.0;
	while (rank<sol.rank(0) && error1<error2) {
    rank += 1;
    error1 = 0.0;
    for (unsigned int iTerm=0; iTerm<rank; ++iTerm) {
      error1 += pow(m_testcase.tolc_interpol()*sol.coeffs(iTerm), 2);
    }
    error2 = 0.0;
    for (unsigned int iTerm=rank; iTerm<sol.rank(0); ++iTerm) {
      error2 += (1.0-pow(m_testcase.tolc_interpol(), 2))*pow(sol.coeffs(iTerm), 2);
    }
  }

  m_U.init(sol.nDof_x(0), rank, m_comm);
  for (unsigned int j=0; j<rank; ++j) {
  	m_U(j) = sol.terms_x(j);
  }

	this->update_precomputed_quantities();

}


void InterpolatorHeat3D::update(const CPTensor2Vars& sol, const VectorSet& sol_terms_y) {

	brand(m_U, sol_terms_y, sol.terms_x(), sol.coeffs(), m_testcase.tolc_interpol(), m_comm);
	this->update_precomputed_quantities();

}


void InterpolatorHeat3D::solve(const PointCloud& parameter, const unsigned int nStep, VectorSet& sol_terms_y) const {

	assert(m_isInitialised);

	sol_terms_y.init(m_sol_terms_x.nCols(), parameter.nOfPt()*(nStep+1), m_comm);
	
	for (unsigned int i=0; i<parameter.nOfPt(); ++i) {

		Matrix A(m_A[0]);
		A.add(m_A[1], parameter(i, 0));
		A.add(m_A[2], parameter(i, 1));
		A.inv();

		Vector c(m_c[0]);
		c *= parameter(i, 2);
		c.add(m_c[1], parameter(i, 3));
		c.add(m_c[2], parameter(i, 4));
		c.add(m_c[3], parameter(i, 5));
		c.add(m_c[4], parameter(i, 6));

		Vector y(m_sol_terms_x.nCols(), 0.0, m_comm);
		Vector rhs(m_sol_terms_x.nCols(), 0.0, m_comm);
		sol_terms_y(i*(nStep+1)) = y;

	  for (unsigned int k=0; k<nStep; ++k) {

			MatMultAdd(m_B, y, c, rhs);
			Solve(A, rhs, y);
	  	sol_terms_y(k+1+i*(nStep+1)) = y;

	  }

	}

	sol_terms_y.finalize();

}


void InterpolatorHeat3D::solve(const PointCloud& parameter, const unsigned int nStep, VectorSet& sol_terms_y, CPTensor2Vars& sol) const {

	assert(m_isInitialised);

	sol_terms_y.init(m_sol_terms_x.nCols(), parameter.nOfPt()*(nStep+1), m_comm);

	sol.init({m_testcase.nFluid()}, parameter.nOfPt(), {m_sol_terms_x.nCols()}, m_comm);
	sol.terms_x() = m_sol_terms_x;
	sol.coeffs() = m_sol_coeffs;
	
	for (unsigned int i=0; i<parameter.nOfPt(); ++i) {

		Matrix A(m_A[0]);
		A.add(m_A[1], parameter(i, 0));
		A.add(m_A[2], parameter(i, 1));
		A.inv();

		Vector c(m_c[0]);
		c *= parameter(i, 2);
		c.add(m_c[1], parameter(i, 3));
		c.add(m_c[2], parameter(i, 4));
		c.add(m_c[3], parameter(i, 5));
		c.add(m_c[4], parameter(i, 6));

		Vector y(m_sol_terms_x.nCols(), 0.0, m_comm);
		Vector rhs(m_sol_terms_x.nCols(), 0.0, m_comm);
		sol_terms_y(i*(nStep+1)) = y;

	  for (unsigned int k=0; k<nStep; ++k) {

			MatMultAdd(m_B, y, c, rhs);
			Solve(A, rhs, y);
	  	sol_terms_y(k+1+i*(nStep+1)) = y;

	  }

	  for (unsigned int j=0; j<sol.terms_y().nCols(); ++j) {
			sol.terms_y().setMatEl(i, j, y.getVecEl(j));
		}

	}

	sol_terms_y.finalize();
	sol.finalize();

}
