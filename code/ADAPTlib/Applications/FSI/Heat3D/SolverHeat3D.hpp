#ifndef SolverHeat3D_hpp
#define SolverHeat3D_hpp

// Including headers:
#include "../../../Applications/FSI/Vector.hpp"
#include "../../../Applications/FSI/VectorSet.hpp"
#include "../../../Applications/FSI/Matrix.hpp"
#include "../../../Applications/FSI/Operations.hpp"
#include "../../../Applications/FSI/CPTensor2Vars.hpp"
#include "../../../Applications/FSI/OperatorCPTensor2Vars.hpp"
#include "../../../Applications/FSI/Testcase.hpp"
#include "../../../Applications/FSI/PointCloud.hpp"
#include "../../../Applications/FSI/Heat3D/InterpolatorHeat3D.hpp"


class SolverHeat3D {

private:

  Testcase m_testcase;
  MPI_Comm m_comm;
  OperatorCPTensor2Vars m_operatorA;
  OperatorCPTensor2Vars m_operatorB;
  std::vector<double> m_tensC_coeffs;
  VectorSet m_tensC_terms_x;
  Matrix m_P;
  bool m_isInitialised;

  void load_data(const std::vector<double>& lbParameter, const std::vector<double>& ubParameter);

public:

  SolverHeat3D();
  SolverHeat3D(const Testcase& testcase, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm=PETSC_COMM_WORLD);
  ~SolverHeat3D();

  void init(const Testcase& testcase, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void solve(const PointCloud& parameter, const double t, const CPTensor2Vars& sol_old, CPTensor2Vars& sol);
  void clear();

};

#endif
