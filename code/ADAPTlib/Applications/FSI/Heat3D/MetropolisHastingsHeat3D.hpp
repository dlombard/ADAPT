#ifndef MetropolisHastingsHeat3D_hpp
#define MetropolisHastingsHeat3D_hpp

// Including headers:
#include "../../../Applications/FSI/Vector.hpp"
#include "../../../Applications/FSI/VectorSet.hpp"
#include "../../../Applications/FSI/Matrix.hpp"
#include "../../../Applications/FSI/Operations.hpp"
#include "../../../Applications/FSI/CPTensor2Vars.hpp"
#include "../../../Applications/FSI/OperatorCPTensor2Vars.hpp"
#include "../../../Applications/FSI/Testcase.hpp"
#include "../../../Applications/FSI/PointCloud.hpp"
#include "../../../Applications/FSI/Heat3D/InterpolatorHeat3D.hpp"
#include "../../../Applications/FSI/Heat3D/SolverHeat3D.hpp"


class MetropolisHastingsHeat3D {

private:

  Testcase m_testcase;
  PointCloud m_parameter;
  MPI_Comm m_comm;
  VectorSet m_HtX;
  Matrix m_HtH;
  std::vector<double> m_h1;
  VectorSet m_h2;
  VectorSet m_h3;
  VectorSet m_observation;
  Matrix m_H;
  VectorSet m_sol_terms_y;
  CPTensor2Vars m_sol_old;
  CPTensor2Vars m_sol;
  InterpolatorHeat3D m_interpol;
  SolverHeat3D m_solver;
  std::default_random_engine m_generator;
  std::uniform_real_distribution<double> m_uniform_distribution;
  std::vector<std::normal_distribution<double>> m_normal_distribution;
  std::vector<double> m_lbParameter;
  std::vector<double> m_ubParameter;
  bool m_isInitialised;

  void init_observation();
  void update_solution(const PointCloud& candidate, const unsigned int k, const bool update_interpol=false);
  std::vector<double> update_error(const unsigned int k, const bool update_interpol=false);
  void update_basis(const unsigned int k);
  void save_solution(const PointCloud& parameter, const unsigned int k) const;

public:

  MetropolisHastingsHeat3D();
  MetropolisHastingsHeat3D(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm=PETSC_COMM_WORLD);
  ~MetropolisHastingsHeat3D();

  void init(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void solve();
  void clear();

};

#endif
