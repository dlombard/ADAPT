#ifndef InterpolatorHeat3D_hpp
#define InterpolatorHeat3D_hpp

// Including headers:
#include "../../../Applications/FSI/Vector.hpp"
#include "../../../Applications/FSI/VectorSet.hpp"
#include "../../../Applications/FSI/Matrix.hpp"
#include "../../../Applications/FSI/Operations.hpp"
#include "../../../Applications/FSI/CPTensor2Vars.hpp"
#include "../../../Applications/FSI/OperatorCPTensor2Vars.hpp"
#include "../../../Applications/FSI/Testcase.hpp"
#include "../../../Applications/FSI/PointCloud.hpp"


class InterpolatorHeat3D {

private:

  bool m_isInitialised;
  Testcase m_testcase;
  VectorSet m_U;
  VectorSet m_sol_terms_x;
  std::vector<double> m_sol_coeffs;
  std::vector<Matrix> m_A;
  Matrix m_B;
  std::vector<Vector> m_c;
  std::vector<Matrix> m_AA;
  Matrix m_BB;
  std::vector<Vector> m_cc;
  MPI_Comm m_comm;

  void load_data();
  void update_precomputed_quantities();

public:

  InterpolatorHeat3D();
  InterpolatorHeat3D(const Testcase& testcase, const MPI_Comm& comm=PETSC_COMM_WORLD);
  ~InterpolatorHeat3D();

  void init(const Testcase& testcase, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void update(const CPTensor2Vars& sol);
  void update(const CPTensor2Vars& sol, const VectorSet& sol_terms_y);
  void solve(const PointCloud& parameter, const unsigned int nStep, VectorSet& sol_terms_y) const;
  void solve(const PointCloud& parameter, const unsigned int nStep, VectorSet& sol_terms_y, CPTensor2Vars& sol) const;
  void clear();

  inline const VectorSet& terms_x() const;

};


/*
*  Inline methods
*/

inline const VectorSet& InterpolatorHeat3D::terms_x() const {
  return m_sol_terms_x;
};

#endif
