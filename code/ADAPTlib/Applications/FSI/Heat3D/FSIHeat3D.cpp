#include "FSIHeat3D.hpp"


/* 
*  Constructors/Destructors
*/

FSIHeat3D::FSIHeat3D() : m_isInitialised(false) {}

FSIHeat3D::FSIHeat3D(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm) {
  assert(lbParameter.size()==ubParameter.size());
  this->init(filename, lbParameter, ubParameter, comm);
}

FSIHeat3D::~FSIHeat3D() {
	this->clear();
}

void FSIHeat3D::init(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm) {
	assert(lbParameter.size()==ubParameter.size());
	m_testcase.init(filename);
	m_parameter.init(lbParameter, ubParameter, m_testcase.nParameter(), m_perm);
	std::cout << m_perm.size() << " " << m_parameter.nOfPt() << std::endl;
	m_comm = comm;
  this->load_data(lbParameter, ubParameter);
  m_sol.init({m_testcase.nFluid()}, m_parameter.nOfPt(), {0}, m_comm);
	m_isInitialised = true;
}

void FSIHeat3D::clear() {
	m_testcase.clear();
  m_parameter.clear();
  m_perm.clear();
  m_operatorA.clear();
  m_operatorB.clear();
  m_tensC.clear();
  m_P.clear();
  m_sol.clear();
  m_isInitialised = false;
}


/* 
*  Methods
*/

void FSIHeat3D::load_data(const std::vector<double>& lbParameter, const std::vector<double>& ubParameter) {
	assert(lbParameter.size()==ubParameter.size());

	// Matrices containing the parameter values on the diagonal.
	std::vector<Matrix> theta(m_parameter.dim());
	for (unsigned int i=0; i<theta.size(); ++i) {
		theta[i].init(m_parameter.nOfPt(), m_parameter.nOfPt(), m_comm);
		for (unsigned int j=0; j<m_parameter.nOfPt(); ++j) {
			theta[i].setMatEl(j, j, m_parameter(j, i));
		}
		theta[i].finalize();
	}
	
	// Assemble m_operatorA
	m_operatorA.init({m_testcase.nFluid()}, {m_testcase.nFluid()}, m_parameter.nOfPt(), m_parameter.nOfPt(), 3, m_comm);
	m_operatorA(0, 0).init(m_testcase.folderpath_input_data() + "K.dat", m_comm);
	m_operatorA(0, 1).init(theta[0]);
	m_operatorA(1, 0).init(m_testcase.folderpath_input_data() + "R.dat", m_comm);
	m_operatorA(1, 1).init(theta[1]);
	m_operatorA(2, 0).init(m_testcase.folderpath_input_data() + "M.dat", m_comm);
	m_operatorA(2, 0) *= (1/m_testcase.dt());
	m_operatorA(2, 1).init(eye(m_parameter.nOfPt(), m_parameter.nOfPt(), m_comm));	

	// Assemble m_operatorB
	m_operatorB.init({m_testcase.nFluid()}, {m_testcase.nFluid()}, m_parameter.nOfPt(), m_parameter.nOfPt(), 1, m_comm);
	m_operatorB(0, 0).init(m_testcase.folderpath_input_data() + "M.dat", m_comm);
	m_operatorB(0, 0) *= (1/m_testcase.dt());
	m_operatorB(0, 1).init(eye(m_parameter.nOfPt(), m_parameter.nOfPt(), m_comm));	
	
	// Assemble m_tensC
	m_tensC.init({m_testcase.nFluid()}, m_parameter.nOfPt(), {5}, m_comm);
	m_tensC.coeffs(0) = 1.0;
	m_tensC.coeffs(1) = 1.0;
	m_tensC.coeffs(2) = 1.0;
	m_tensC.coeffs(3) = 1.0;
	m_tensC.coeffs(4) = 1.0;
	m_tensC.terms_x(0).init(m_testcase.folderpath_input_data() + "b10.dat", m_comm);
	m_tensC.terms_x(1).init(m_testcase.folderpath_input_data() + "b20.dat", m_comm);
	m_tensC.terms_x(2).init(m_testcase.folderpath_input_data() + "b30.dat", m_comm);
	m_tensC.terms_x(3).init(m_testcase.folderpath_input_data() + "b40.dat", m_comm);
	m_tensC.terms_x(4).init(m_testcase.folderpath_input_data() + "b50.dat", m_comm);
	for (unsigned int i=0; i<m_tensC.terms_y().nRows(); ++i) {
		for (unsigned int j=0; j<m_tensC.terms_y().nCols(); ++j) {
			m_tensC.terms_y().setMatEl(i, j, m_parameter(i, j+2));
		}
	}
	m_tensC.finalize();

	// Preconditioner matrix
	m_P.init(m_operatorA(2, 0));
	for (unsigned int i=0; i<2; ++i) {
		m_P.add(m_operatorA(i, 0), (lbParameter[i]+ubParameter[i])/2.0);
	}
	m_P.inv(PCCHOLESKY);
}


void FSIHeat3D::save_solution(const unsigned int k) const {
	assert(m_isInitialised);
	for (unsigned int j=0; j<m_perm.size(); ++j) {
		m_sol.save(m_testcase.folderpath_output_sol() + std::to_string(j) + "/sol_" + std::to_string(k+1) + ".dat", m_perm[j]);
	}
}


void FSIHeat3D::solve(const bool write_solution) {

	assert(m_isInitialised);

	unsigned int it;

	for (unsigned int k=0; k<m_testcase.nStep(); ++k) {

		// Compute rhs
		CPTensor2Vars rhs({m_testcase.nFluid()}, m_parameter.nOfPt(), {((unsigned int) m_sol.coeffs().size())*m_operatorB.nTerms()}, m_comm);
		rhs.finalize();
		m_operatorB.apply(m_sol, rhs);
		rhs.add(m_tensC, 1.0);

		// Update solution
		it = gmres(m_operatorA, m_sol, rhs, m_testcase.tolr_gmres(), m_testcase.itmax_gmres(), m_P, m_testcase.tolc_gmres(), m_comm);

		// Compute res
		CPTensor2Vars res(rhs);
	  res -= m_operatorA.apply(m_sol);
		std::cout << k << ": " << it << ", " << m_sol.rank(0) << ", " << norm(res) << std::endl;

		if (write_solution) {
			this->save_solution(k);
		}
	}
}


void FSIHeat3D::check() const {

	assert(m_isInitialised);

	for (unsigned int j=0; j<m_perm.size(); ++j) {
		double error1 = 0.0, error2 = 0.0;
		for (unsigned int k=0; k<m_testcase.nStep(); ++k) {
			Vector sol(m_testcase.folderpath_output_sol() + std::to_string(j) + "/sol_" + std::to_string(k+1) + ".dat", m_comm);
			Vector ref(m_testcase.folderpath_input_observation() + std::to_string(j) + "/sol_" + std::to_string(k+1) + ".dat", m_comm);
			error1 += pow(norm(ref), 2);
			error2 += pow(norm(sol-ref), 2);
		}
		std::cout << "Error " << j << " : " << 100.0*sqrt(error2)/sqrt(error1) << std::endl;
	}
}


/*
*  Non-member function
*/

// unsigned int gmres(const OperatorCPTensor2Vars& operatorA, CPTensor2Vars& X, const CPTensor2Vars& B, const double tolr, const unsigned int itmax, const Matrix& P, const double tolc, const MPI_Comm& comm) {

//   CPTensor2Vars X0(X), R(B);
//   R -= operatorA.apply(X);
//   R.truncate(tolc);
//   double resnorm = norm(R);
//   R.solve(P);

//   double tol = sqrt(1.0-pow(tolc, 2))*tolr*norm(B), tmp;
//   std::vector<double> sn(itmax), cs(itmax), e1(itmax+1, 0.0), H((itmax+1)*itmax, 0.0), beta(itmax+1, 0.0);
//   e1[0] = 1.0;
//   beta[0] = norm(R);
//   std::vector<CPTensor2Vars> Q(itmax+1);
//   Q[0].init(R);
//   Q[0] /= beta[0];
  
//   unsigned int it = 0;
//   while (it<itmax && resnorm>tol) {

//     // Arnoldi iteration
//     Q[it+1].init({Q[it].nDof_x(0)}, Q[it].nDof_y(), {Q[it].rank()*operatorA.nTerms()}, comm);
//     Q[it+1].finalize();
//     operatorA.apply(Q[it], Q[it+1]);
//     Q[it+1].truncate(tolc);
//     Q[it+1].solve(P);
//     for (unsigned int subit=0; subit<=it; ++subit) {
//     	H[it*(itmax+1)+subit] = dot(Q[subit], Q[it+1]);
//     	if (fabs(H[it*(itmax+1)+subit])!=0.0) {
// 	      Q[it+1].add(Q[subit], -H[it*(itmax+1)+subit]);
// 	      Q[it+1].truncate(tolc);
// 	    }
//     }
//     H[it*(itmax+1)+it+1] = norm(Q[it+1]);
//     if (fabs(H[it*(itmax+1)+it+1])==0.0) {
//     	std::cout << "Warning gmres stops prematurely: " << tol << "<" << resnorm << std::endl;
//     	break;
//     }
//     Q[it+1] /= H[it*(itmax+1)+it+1];
    
//     // Givens rotation
//     for (unsigned int subit=0; subit<it; ++subit) {
//     	tmp = sn[subit]*H[it*(itmax+1)+subit+1]+cs[subit]*H[it*(itmax+1)+subit];
//       H[it*(itmax+1)+subit+1] = cs[subit]*H[it*(itmax+1)+subit+1]-sn[subit]*H[it*(itmax+1)+subit];
//       H[it*(itmax+1)+subit] = tmp;
//     }
//     tmp = sqrt(H[it*(itmax+1)+it]*H[it*(itmax+1)+it]+H[it*(itmax+1)+it+1]*H[it*(itmax+1)+it+1]);
//     cs[it] = H[it*(itmax+1)+it]/tmp;
//     sn[it] = H[it*(itmax+1)+it+1]/tmp;
//     H[it*(itmax+1)+it] = sn[it]*H[it*(itmax+1)+it+1]+cs[it]*H[it*(itmax+1)+it];
//     H[it*(itmax+1)+it+1] = 0.0;
    
//     // Residual
//     beta[it+1] = -sn[it]*beta[it];
//     beta[it] = cs[it]*beta[it];
//     ++it;
//     std::vector<double> y(it);
// 	  for (unsigned int i=it-1; i<it; --i) {
// 	    y[i] = beta[i];
// 	    for (unsigned int j=it-1; i<j; --j) {
// 	      y[i] -= H[j*(itmax+1)+i]*y[j];
// 	    }
// 	    y[i] /= H[i*(itmax+1)+i];
// 	  }
// 	  X = X0;
// 	  for (unsigned int subit=0; subit<it; ++subit) {
// 	    X.add(Q[subit], y[subit]);
// 	  }
// 	  X.truncate(tolc);
// 	  R = B;
// 	  R -= operatorA.apply(X);
// 	  R.truncate(tolc);
// 	  resnorm = norm(R);    
//   }

//   if (it==itmax) {
//   	std::cout << "Warning gmres does not converge: " << tol << "<" << resnorm << std::endl;
//   }

//   return it;
// }

// unsigned int gmres(const OperatorCPTensor2Vars& operatorA, CPTensor2Vars& X, const CPTensor2Vars& B, const double tolr, const unsigned int itmax, const Matrix& P, const double tolc, const MPI_Comm& comm) {

//   CPTensor2Vars X0(X), R(B);
//   R -= operatorA.apply(X);
//   R.truncate(tolc);
//   double resnorm = norm(R);
//   R.solve(P);

//   double tol = sqrt(1.0-pow(tolc, 2))*tolr*norm(B), tmp;
//   std::vector<double> sn(itmax), cs(itmax), e1(itmax+1, 0.0), H((itmax+1)*itmax, 0.0), beta(itmax+1, 0.0);
//   e1[0] = 1.0;
//   beta[0] = norm(R);
//   std::vector<CPTensor2Vars> Q(itmax+1);
//   Q[0].init(R);
//   Q[0] /= beta[0];
  
//   unsigned int it = 0;
//   while (it<itmax && resnorm>tol) {

//     // Arnoldi iteration
//     Q[it+1].init({Q[it].nDof_x(0)}, Q[it].nDof_y(), {Q[it].rank()*operatorA.nTerms()}, comm);
//     Q[it+1].finalize();
//     operatorA.apply(Q[it], Q[it+1]);
//     Q[it+1].truncate(tolc);
//     Q[it+1].solve(P);
//     for (unsigned int subit=0; subit<=it; ++subit) {
//     	H[it*(itmax+1)+subit] = dot(Q[subit], Q[it+1]);
//     	if (fabs(H[it*(itmax+1)+subit])!=0.0) {
// 	      Q[it+1].add(Q[subit], -H[it*(itmax+1)+subit]);
// 	    }
//     }
//     Q[it+1].truncate(tolc);
//     H[it*(itmax+1)+it+1] = norm(Q[it+1]);
//     if (fabs(H[it*(itmax+1)+it+1])==0.0) {
//     	std::cout << "Warning gmres stops prematurely: " << tol << "<" << resnorm << std::endl;
//     	break;
//     }
//     Q[it+1] /= H[it*(itmax+1)+it+1];
    
//     // Givens rotation
//     for (unsigned int subit=0; subit<it; ++subit) {
//     	tmp = sn[subit]*H[it*(itmax+1)+subit+1]+cs[subit]*H[it*(itmax+1)+subit];
//       H[it*(itmax+1)+subit+1] = cs[subit]*H[it*(itmax+1)+subit+1]-sn[subit]*H[it*(itmax+1)+subit];
//       H[it*(itmax+1)+subit] = tmp;
//     }
//     tmp = sqrt(H[it*(itmax+1)+it]*H[it*(itmax+1)+it]+H[it*(itmax+1)+it+1]*H[it*(itmax+1)+it+1]);
//     cs[it] = H[it*(itmax+1)+it]/tmp;
//     sn[it] = H[it*(itmax+1)+it+1]/tmp;
//     H[it*(itmax+1)+it] = sn[it]*H[it*(itmax+1)+it+1]+cs[it]*H[it*(itmax+1)+it];
//     H[it*(itmax+1)+it+1] = 0.0;
    
//     // Residual
//     beta[it+1] = -sn[it]*beta[it];
//     beta[it] = cs[it]*beta[it];
//     ++it;
//     std::vector<double> y(it);
// 	  for (unsigned int i=it-1; i<it; --i) {
// 	    y[i] = beta[i];
// 	    for (unsigned int j=it-1; i<j; --j) {
// 	      y[i] -= H[j*(itmax+1)+i]*y[j];
// 	    }
// 	    y[i] /= H[i*(itmax+1)+i];
// 	  }
// 	  X = X0;
// 	  for (unsigned int subit=0; subit<it; ++subit) {
// 	    X.add(Q[subit], y[subit]);
// 	  }
// 	  X.truncate(tolc);
// 	  R = B;
// 	  R -= operatorA.apply(X);
// 	  R.truncate(tolc);
// 	  resnorm = norm(R);    
//   }

//   if (it==itmax) {
//   	std::cout << "Warning gmres does not converge: " << tol << "<" << resnorm << std::endl;
//   }

//   return it;
// }

