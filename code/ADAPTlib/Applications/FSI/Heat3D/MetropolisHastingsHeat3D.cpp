#include "MetropolisHastingsHeat3D.hpp"


/* 
*  Constructors/Destructors
*/

MetropolisHastingsHeat3D::MetropolisHastingsHeat3D() : m_isInitialised(false) {}

MetropolisHastingsHeat3D::MetropolisHastingsHeat3D(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm) : m_isInitialised(false) {
  assert(lbParameter.size()==ubParameter.size());
  this->init(filename, lbParameter, ubParameter, comm);
}

MetropolisHastingsHeat3D::~MetropolisHastingsHeat3D() {
	this->clear();
}

void MetropolisHastingsHeat3D::init(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm) {
	
	assert(lbParameter.size()==ubParameter.size());
	
	m_testcase.init(filename);
	m_comm = comm;
	m_lbParameter = lbParameter;
	m_ubParameter = ubParameter;

	m_parameter.init(m_lbParameter, m_ubParameter, m_testcase.nParameter());
	PetscPrintf(m_comm, "Number of particles: %d\n", m_parameter.nOfPt());
	this->init_observation();

  // m_sol.init({m_testcase.nFluid()}, m_parameter.nOfPt(), {0}, m_comm);
  m_interpol.init(m_testcase, m_comm);
  m_solver.init(m_testcase, m_lbParameter, m_ubParameter, m_comm);

  // unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  // m_generator = std::default_random_engine(seed);
  m_uniform_distribution = std::uniform_real_distribution<double>(0.0, 1.0);
  m_normal_distribution.resize(m_parameter.dim());
  for (unsigned int iComp=0; iComp<m_normal_distribution.size(); ++iComp) {
    m_normal_distribution[iComp] = std::normal_distribution<double>(0.0, m_testcase.step_mcmc()*(m_ubParameter[iComp]-m_lbParameter[iComp]));
  }
  
	m_isInitialised = true;
}


void MetropolisHastingsHeat3D::clear() {
	if (!m_isInitialised) {
		m_testcase.clear();
		m_parameter.clear();
		m_HtX.clear();
		m_HtH.clear();
		m_h1.clear();
		m_h2.clear();
		m_h3.clear();
		m_observation.clear();
		m_H.clear();
		m_sol_terms_y.clear();
		m_sol_old.clear();
		m_sol.clear();
		m_interpol.clear();
		m_solver.clear();
		m_normal_distribution.clear();
		m_lbParameter.clear();
		m_ubParameter.clear();
	  m_isInitialised = false;
	}
}


/* 
*  Methods
*/

void MetropolisHastingsHeat3D::init_observation() {

	Vector indx(m_testcase.folderpath_input_data() + "h.dat", m_comm);
  
  m_H.init(m_testcase.nFluid(), m_testcase.nFluid(), m_comm);
	for (unsigned int i=0; i<indx.size(); ++i) {
		unsigned int j = indx.getVecEl(i)-1;
    m_H.setMatEl(j, j, 1.0);
  }
  m_H.finalize();

  VectorSet observation(m_testcase.nFluid(), m_testcase.nStep()+1, PETSC_COMM_WORLD);
  for (unsigned int k=0; k<observation.nCols(); ++k) {
    observation(k).init(m_testcase.folderpath_input_observation() + "sol_" + std::to_string(k) + ".dat", PETSC_COMM_WORLD);
  }
  observation.finalize();
  m_observation.init(m_H.nRows(), observation.nCols(), 0.0, m_comm);
	MatMatMult(m_H, observation, m_observation);

	m_HtX.init(m_H.nCols(), m_observation.nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_H, m_observation, m_HtX);

	m_HtH.init(m_H.nCols(), m_H.nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_H, m_H, m_HtH);

	m_h1 = vecnorm(m_observation);

}


void MetropolisHastingsHeat3D::update_solution(const PointCloud& parameter, const unsigned int k, const bool update_interpol) {

	assert(m_isInitialised && k<m_testcase.nStep());

  if (update_interpol) {
  	if (k==0) {
  		m_sol_old.init({m_testcase.nFluid()}, parameter.nOfPt(), {0}, m_comm);
  	} else {
  		m_interpol.solve(parameter, k, m_sol_terms_y, m_sol_old);
  	}
  	m_solver.solve(parameter, (k+1)*m_testcase.dt(), m_sol_old, m_sol);
  } else {
  	m_interpol.solve(parameter, k+1, m_sol_terms_y);
  }
}


std::vector<double> MetropolisHastingsHeat3D::update_error(const unsigned int k, const bool update_interpol) {
	assert(m_isInitialised && k<m_testcase.nStep());

	std::vector<double> error(m_parameter.nOfPt(), 0.0);

	if (update_interpol) {

		if (k==0) {

			CPTensor2Vars observation_old(m_H, m_sol_old);
			double observation_norm2 = pow(norm(m_observation(0)), 2);
		  std::vector<double> observation_norm = vecnorm(observation_old);
		  for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
				error[iPt] += observation_norm2+pow(observation_norm[iPt], 2);
			}
		  for (unsigned int iTerm=0; iTerm<observation_old.rank(0); ++iTerm) {
		  	double value = 2.0*observation_old.coeffs(iTerm)*dot(observation_old.terms_x(iTerm), m_observation(0));
		  	for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
					error[iPt] -= value*observation_old.terms_y().getMatEl(iPt,iTerm);
				}
		  }

		}	else {

		  double value1 = 0.0;
		  for (unsigned int l=0; l<k+1; ++l) {
	  		value1 += pow(m_h1[l], 2);
		  }
		  for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
		  	double value2 = 0.0;
		  	for (unsigned int l=0; l<k+1; ++l) {
		  		value2 += dot(m_h2(l), m_sol_terms_y(l+(k+1)*iPt)); 
				}
				double value3 = 0.0;
		  	for (unsigned int l=0; l<k+1; ++l) {
		  		value3 += dot(MatMult(m_h3, m_sol_terms_y(l+(k+1)*iPt)), m_sol_terms_y(l+(k+1)*iPt)); 
				}
				error[iPt] += value1-2.0*value2+value3;
		  }

		}

	  CPTensor2Vars observation(m_H, m_sol);
	  double observation_norm2 = pow(norm(m_observation(k+1)), 2);
	  std::vector<double> observation_norm = vecnorm(observation);
	  for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
			error[iPt] += observation_norm2+pow(observation_norm[iPt], 2);
		}
	  for (unsigned int iTerm=0; iTerm<observation.rank(0); ++iTerm) {
	  	double value = 2.0*observation.coeffs(iTerm)*dot(observation.terms_x(iTerm), m_observation(k+1));
	  	for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
				error[iPt] -= value*observation.terms_y().getMatEl(iPt,iTerm);
			}
	  }

  } else {

	  double value1 = 0.0;
	  for (unsigned int l=0; l<k+2; ++l) {
  		value1 += pow(m_h1[l], 2);
	  }
	  for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
	  	double value2 = 0.0;
	  	for (unsigned int l=0; l<k+2; ++l) {
	  		value2 += dot(m_h2(l), m_sol_terms_y(l+(k+2)*iPt)); 
			}
			double value3 = 0.0;
	  	for (unsigned int l=0; l<k+2; ++l) {
	  		value3 += dot(MatMult(m_h3, m_sol_terms_y(l+(k+2)*iPt)), m_sol_terms_y(l+(k+2)*iPt)); 
			}
			error[iPt] += value1-2.0*value2+value3;
	  }

  }

  return error;
}


void MetropolisHastingsHeat3D::update_basis(const unsigned int k) {

	assert(m_isInitialised && k<m_testcase.nStep());

	if (k==0) {
		m_interpol.update(m_sol);
	} else {
		m_interpol.update(m_sol, m_sol_terms_y);
	}

	m_h2.init(m_interpol.terms_x().nCols(), k+2, 0.0, m_comm);
	for (unsigned int l=0; l<m_h2.nCols(); ++l) {
		MatMultTranspose(m_interpol.terms_x(), m_HtX(l), m_h2(l));
	}

	m_h3.init(m_interpol.terms_x().nCols(), m_interpol.terms_x().nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_interpol.terms_x(), MatMatMult(m_HtH, m_interpol.terms_x()), m_h3);

}


void MetropolisHastingsHeat3D::save_solution(const PointCloud& parameter, const unsigned int k) const {
	assert(m_isInitialised);

	VectorSet sol_terms_y;
	CPTensor2Vars sol;
	m_interpol.solve(parameter, k, sol_terms_y);

	Matrix terms_x(m_interpol.terms_x());
	terms_x.save(m_testcase.folderpath_output_sol() + "sol_terms_x.dat");
	Matrix terms_y(sol_terms_y);
	terms_y.save(m_testcase.folderpath_output_sol() + "sol_terms_y.dat");
}


void MetropolisHastingsHeat3D::solve() {

	assert(m_isInitialised);

	m_parameter.save(m_testcase.folderpath_output_parameter() + "theta_0.dat");
	for (unsigned int k=0; k<m_testcase.nStep(); ++k) {
		this->update_solution(m_parameter, k, true);
		std::vector<double> parameter_error = this->update_error(k, true);
		this->update_basis(k);
		for (unsigned int it=0; it<m_testcase.itmax_mcmc(); ++it) {
			PointCloud m_candidate(m_parameter.dim(), m_parameter.nOfPt());
			for (unsigned int iPt=0; iPt<m_candidate.nOfPt(); ++iPt) {
				for(unsigned int iComp=0; iComp<m_candidate.dim(); ++iComp) {
	        m_candidate(iPt, iComp) = std::max(m_lbParameter[iComp], std::min(m_ubParameter[iComp], m_parameter(iPt, iComp)+m_normal_distribution[iComp](m_generator)));
	      }
			}
			this->update_solution(m_candidate, k, false);
			std::vector<double> candidate_error = this->update_error(k, false);
			for (unsigned int iPt=0; iPt<parameter_error.size(); ++iPt) {
				double p = std::max(1.0e-16, m_uniform_distribution(m_generator));
				if (std::log(p)<=(parameter_error[iPt]-candidate_error[iPt])) {
					m_parameter(iPt) = m_candidate(iPt);
		    	parameter_error[iPt] = candidate_error[iPt];
		    }
		  }
		}
		m_parameter.save(m_testcase.folderpath_output_parameter() + "theta_" + std::to_string(k+1) + ".dat");
	}
	this->save_solution(m_parameter, m_testcase.nStep());
}


// void MetropolisHastingsHeat3D::solve() {

// 	assert(m_isInitialised);

// 	// m_parameter.save(m_testcase.folderpath_output_parameter() + "theta_0.dat");
// 	unsigned int ll = 0;
// 	for (unsigned int k=0; k<m_testcase.nStep(); ++k) {
// 		m_parameter.load(m_testcase.folderpath_output_parameter() + "theta_" + std::to_string(ll) + ".dat");
// 		++ll;
// 		this->update_solution(m_parameter, k, true);
// 		std::vector<double> parameter_error = this->update_error(k, true);
// 		for (unsigned int i=0; i<parameter_error.size(); ++i) {
// 			std::cout << parameter_error[i] << " ";
// 		}
// 		std::cout << std::endl;
// 		this->update_basis(k);
// 		for (unsigned int it=0; it<m_testcase.itmax_mcmc(); ++it) {
// 			PointCloud m_candidate(m_parameter.dim(), m_parameter.nOfPt());
// 			for (unsigned int iPt=0; iPt<m_candidate.nOfPt(); ++iPt) {
// 				for(unsigned int iComp=0; iComp<m_candidate.dim(); ++iComp) {
// 	        m_candidate(iPt, iComp) = std::max(m_lbParameter[iComp], std::min(m_ubParameter[iComp], m_parameter(iPt, iComp)+m_normal_distribution[iComp](m_generator)));
// 	      }
// 			}
// 			m_candidate.load(m_testcase.folderpath_output_parameter() + "theta_" + std::to_string(ll) + ".dat");
// 			++ll;
// 			this->update_solution(m_candidate, k, false);
// 			std::vector<double> candidate_error = this->update_error(k, false);
// 			for (unsigned int i=0; i<candidate_error.size(); ++i) {
// 				std::cout << candidate_error[i] << " ";
// 			}
// 			std::cout << std::endl;
// 			for (unsigned int iPt=0; iPt<parameter_error.size(); ++iPt) {
// 				double p = std::max(1.0e-16, m_uniform_distribution(m_generator));
// 				if (std::log(p)<=(parameter_error[iPt]-candidate_error[iPt])) {
// 					m_parameter(iPt) = m_candidate(iPt);
// 		    	parameter_error[iPt] = candidate_error[iPt];
// 		    }
// 		  }
// 		}
// 		m_parameter.save(m_testcase.folderpath_output_parameter() + "theta_" + std::to_string(k+1) + ".dat");
// 	}
// 	this->save_solution(m_parameter, m_testcase.nStep());
// }
