#include "Operations.hpp"


void Vec2vector(const Vector& u, std::vector<double>& v) {
  assert(u.isAssembled());
  v.resize(u.size());
  for (unsigned int i=0; i<v.size(); ++i) {
    v[i] = u.getVecEl(i);
  }
}

std::vector<double> Vec2vector(const Vector& u) {
  std::vector<double> v;
  Vec2vector(u, v);
  return v;
}

void MatMult(const VectorSet& M, const Vector& u, Vector& v) {
  assert(M.isAssembled() && u.isAssembled() && v.isAssembled() && M.nRows()==v.size() && M.nCols()==u.size());
  v = 0.0;
  for (unsigned int j=0; j<M.nCols(); ++j) {
    v.add(M(j), u.getVecEl(j));
  }
}

void MatMult(const Matrix& M, const Vector& u, Vector& v) {
  assert(M.m_isAssembled && u.m_isAssembled && v.m_isAssembled && M.m_nRows==v.m_size && M.m_nCols==u.m_size);
  MatMult(M.m_data, u.m_data, v.m_data);
}

Vector MatMult(const VectorSet& M, const Vector& u) {
  assert(M.isAssembled() && u.isAssembled() && M.nCols()==u.size());
  Vector v(M.nRows(), 0.0, M.comm());
  MatMult(M, u, v);
  return v;
}

Vector MatMult(const Matrix& M, const Vector& u) {
  assert(M.isAssembled() && u.isAssembled() && M.nCols()==u.size());
  Vector v(M.nRows(), 0.0, M.comm());
  MatMult(M, u, v);
  return v;
}

void MatMultAdd(const Matrix& M, const Vector& u, const Vector& v, Vector& w) {
  assert(M.m_isAssembled && u.m_isAssembled && v.m_isAssembled && w.m_isAssembled && M.m_nRows==v.m_size && M.m_nRows==w.m_size && M.m_nCols==u.m_size);
  MatMultAdd(M.m_data, u.m_data, v.m_data, w.m_data);
}

void MatMultTranspose(const VectorSet& M, const Vector& u, Vector& v) {
  assert(M.isAssembled() && u.isAssembled() && v.isAssembled() && M.nRows()==u.size() && M.nCols()==v.size());
  for (unsigned int j=0; j<M.nCols(); ++j) {
    v.setVecEl(j, dot(M(j), u));
  }
  v.finalize();
}

void MatMultTranspose(const Matrix& M, const Vector& u, Vector& v) {
  assert(M.m_isAssembled && u.m_isAssembled && v.m_isInitialised && M.m_nRows==u.m_size && M.m_nCols==v.m_size);
  MatMultTranspose(M.m_data, u.m_data, v.m_data);
}

// void MatMult(const VectorSet& M, const Vector& u, Vector& v) {
//   assert(M.m_isAssembled && u.m_isAssembled && M.m_nCols==u.m_size);
//   if (!v.m_isAssembled || M.m_nRows!=v.m_size) {
//     v.init(M.m_nRows, 0.0, M.m_comm);
//   }
//   if (u.m_size>0) {
//     VecAXPBY(v.m_x, u.getVecEl(0), (PetscScalar) 0.0, M(0).m_x);
//     for (unsigned int j=1; j<M.m_nCols; ++j) {
//       VecAXPY(v.m_x, u.getVecEl(j), M(j).m_x);
//     }
//   }
// }

// Vector MatMult(const VectorSet& M, const Vector& u) {
//   assert(M.m_isAssembled && u.m_isAssembled && M.m_nCols==u.m_size);
//   Vector v(M.m_nRows, 0.0, M.m_comm);
//   MatMult(M, u, v);
//   return v;
// }

// void MatMultTranspose(const VectorSet& M, const Vector& u, Vector& v) {
//   assert(M.m_isAssembled && u.m_isAssembled && M.m_nRows==u.m_size);
//   if (!v.m_isInitialised || M.m_nCols!=v.m_size) {
//     v.init(M.m_nCols, M.m_comm);
//   }
//   for (unsigned int j=0; j<M.m_nCols; ++j) {
//     v.setVecEl(j, dot(M(j), u));
//   }
//   v.finalize();
// }

void MatMatMult(const VectorSet& A, const VectorSet& B, VectorSet& C) {
  assert(A.isAssembled() && B.isAssembled() && C.isAssembled() && A.nRows()==C.nRows() && A.nCols()==B.nRows() && B.nCols()==C.nCols());
  for (unsigned int j=0; j<C.nCols(); ++j) {
    MatMult(A, B(j), C(j));
  }
}

void MatMatMult(const Matrix& A, const VectorSet& B, VectorSet& C) {
  assert(A.isAssembled() && B.isAssembled() && C.isAssembled() && A.nRows()==C.nRows() && A.nCols()==B.nRows() && B.nCols()==C.nCols());
  for (unsigned int j=0; j<C.nCols(); ++j) {
    MatMult(A, B(j), C(j));
  }
}

void MatMatMult(const Matrix& A, const Matrix& B, Matrix& C) {
  assert(A.m_isAssembled && B.m_isAssembled && C.m_isAssembled && A.m_nRows==C.m_nRows && A.m_nCols==B.m_nRows && B.m_nCols==C.m_nCols);
  MatMatMult(A.m_data, B.m_data, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &C.m_data);
}

VectorSet MatMatMult(const VectorSet& A, const VectorSet& B) {
  assert(A.isAssembled() && B.isAssembled() && A.nCols()==B.nRows());
  VectorSet C(A.nRows(), B.nCols(), 0.0, A.comm());
  MatMatMult(A, B, C);
  return C;
}

VectorSet MatMatMult(const Matrix& A, const VectorSet& B) {
  assert(A.isAssembled() && B.isAssembled() && A.nCols()==B.nRows());
  VectorSet C(A.nRows(), B.nCols(), 0.0, A.comm());
  MatMatMult(A, B, C);
  return C;
}

Matrix MatMatMult(const Matrix& A, const Matrix& B) {
  assert(A.isAssembled() && B.isAssembled() && A.nCols()==B.nRows());
  Matrix C(A.nRows(), B.nCols(), 0.0, A.comm());
  MatMatMult(A, B, C);
  return C;
}

void MatTransposeMatMult(const VectorSet& A, const VectorSet& B, VectorSet& C) {
  assert(A.isAssembled() && B.isAssembled() && C.isAssembled() && A.nRows()==B.nRows() && A.nCols()==C.nRows() && B.nCols()==C.nCols());
  for (unsigned int i=0; i<C.nRows(); ++i) {
    for (unsigned int j=0; j<C.nCols(); ++j) {
      C.setMatEl(i, j, dot(A(i), B(j)));
    }
  }
  C.finalize();
}

void MatTransposeMatMult(const VectorSet& A, const VectorSet& B, Matrix& C) {
  assert(A.isAssembled() && B.isAssembled() && C.isAssembled() && A.nRows()==B.nRows() && A.nCols()==C.nRows() && B.nCols()==C.nCols());
  for (unsigned int i=0; i<C.nRows(); ++i) {
    for (unsigned int j=0; j<C.nCols(); ++j) {
      C.setMatEl(i, j, dot(A(i), B(j)));
    }
  }
  C.finalize();
}

void MatTransposeMatMult(const Matrix& A, const VectorSet& B, VectorSet& C) {
  assert(A.isAssembled() && B.isAssembled() && C.isAssembled() && A.nRows()==B.nRows() && A.nCols()==C.nRows() && B.nCols()==C.nCols());
  for (unsigned int j=0; j<C.nCols(); ++j) {
    MatMultTranspose(A, B(j), C(j));
  }
}

void MatTransposeMatMult(const Matrix& A, const Matrix& B, Matrix& C) {
  assert(A.m_isAssembled && B.m_isAssembled && C.m_isAssembled && A.m_nRows==B.m_nRows && A.m_nCols==C.m_nRows && B.m_nCols==C.m_nCols);
  MatTransposeMatMult(A.m_data, B.m_data, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &C.m_data);
}

void MatMatTransposeMult(const Matrix& A, const Matrix& B, Matrix& C) {
  assert(A.m_isAssembled && B.m_isAssembled && C.m_isAssembled && A.m_nRows==C.m_nRows && A.m_nCols==B.m_nCols && B.m_nRows==C.m_nCols);
  MatMatTransposeMult(A.m_data, B.m_data, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &C.m_data);
}

Matrix MatMatTransposeMult(const Matrix& A, const Matrix& B) {
  assert(A.isAssembled() && B.isAssembled() && A.nCols()==B.nCols());
  Matrix C(A.nRows(), B.nRows(), 0.0, A.comm());
  MatMatTransposeMult(A, B, C);
  return C;
}

void Solve(const Matrix& M, const Vector& u, Vector& v) {
  assert(M.m_isInversed && u.m_isAssembled && v.m_isAssembled && M.m_nRows==u.m_size && M.m_nCols==v.m_size);
  KSPSolve(M.m_ksp, u.m_data, v.m_data);
}

// void MatMatMult(const Matrix& A, const VectorSet& B, VectorSet& C) {
//   assert(A.isAssembled() && B.isAssembled() && A.nCols()==B.nRows());
//   if (!C.isAssembled() || C.nRows()!=A.nRows() || C.nCols()!=B.nCols()) {
//     C.init(A.nRows(), B.nCols(), 0.0, A.comm());
//   }
//   for (unsigned int j=0; j<C.nCols(); ++j) {
//     MatMult(A, B(j), C(j));
//   }
// }

// VectorSet MatMatMult(const Matrix& A, const VectorSet& B) {
//   assert(A.isAssembled() && B.isAssembled() && A.nCols()==B.nRows());
//   VectorSet C(A.nRows(), B.nCols(), 0.0, A.comm());
//   MatMatMult(A, B, C);
//   return C;
// }

// void MatTransposeMatMult(const VectorSet& A, const VectorSet& B, Matrix& C) {
//   assert(A.isAssembled() && B.isAssembled() && A.nRows()==B.nRows());
//   if (!C.isInitialised() || C.nRows()!=A.nCols() || C.nCols()!=B.nCols()) {
//     C.init(A.nCols(), B.nCols(), A.comm());
//   }
//   for (unsigned int i=0; i<C.nRows(); ++i) {
//     for (unsigned int j=0; j<C.nCols(); ++j) {
//       C.setMatEl(i, j, dot(A(i), B(j)));
//     }
//   }
//   C.finalize();
// }


void qr(const VectorSet& V, VectorSet& Q, Matrix& R, const double tol) {

  assert(V.isAssembled() && 0.0<tol);

  unsigned int k=0;
  double tmp;
  std::vector<bool> isZero(V.nCols(), false);
  std::vector<unsigned int> I, J;
  std::vector<double> val;
  Q.init(V);
  
  for (unsigned int i=0; i<Q.nCols(); ++i) {
    tmp = norm(Q(i));
    if (tmp>tol) {
      I.push_back(k);
      J.push_back(i);
      val.push_back(tmp);
      Q(i) *= (1.0/tmp);
      for (unsigned int j=i+1; j<Q.nCols(); ++j) {
        tmp = dot(Q(i), Q(j));
        if (fabs(tmp)!=0.0) {
          I.push_back(k);
          J.push_back(j);
          val.push_back(tmp);
          Q(j).add(Q(i), -tmp);
        }
      }
      ++k;
    } else {
      isZero[i] = true;
    }
  }

  for (unsigned int i=Q.nCols()-1; i<Q.nCols(); --i) {
    if (isZero[i]) {
      Q.erase(i);
    }
  }
  if (Q.nCols()==0) {
    Q.push_back(e(0, Q.nRows(), Q.comm()));
  }
  R.init(Q.nCols(), V.nCols(), I, J, val, V.comm());

}


void svd(const Matrix& A, VectorSet& U, std::vector<double>& S, VectorSet& V) {

  assert(A.isAssembled());

  int n = A.nRows(), m = A.nCols(), rank = std::min(n, m), numProc;
  MPI_Comm_rank(A.comm(), &numProc);

  std::vector<double> At_;
  IS isrow, iscol;
  if (numProc==0) {
    ISCreateStride(PETSC_COMM_WORLD, n, 0, 1, &isrow);
    ISCreateStride(PETSC_COMM_WORLD, m, 0, 1, &iscol);
  } else {
    ISCreateStride(PETSC_COMM_WORLD, 0, 0, 1, &isrow);
    ISCreateStride(PETSC_COMM_WORLD, 0, 0, 1, &iscol);
  }
  Matrix tmp(A, isrow, iscol);
  ISDestroy(&isrow);
  ISDestroy(&iscol);
  if (numProc==0) {
    std::vector<int> indi = linspace(0, n), indj = linspace(0, m);
    At_.resize(n*m);
    tmp.getValues(indi, indj, At_);
  }

  std::vector<double> Ut_, V_;
  if (numProc==0) {
    char cS = 'S';
    int lwork = -1, info;
    double w;
    Ut_.resize(n*n);
    S.resize(rank);
    V_.resize(m*rank);
    dgesvd_(&cS, &cS, &m, &n, At_.data(), &m, S.data(), V_.data(), &m, Ut_.data(), &n, &w, &lwork, &info);
    lwork = (int) w;
    std::vector<double> work(lwork);
    dgesvd_(&cS, &cS, &m, &n, At_.data(), &m, S.data(), V_.data(), &m, Ut_.data(), &n, work.data(), &lwork, &info);
    if (info!=0) {
      std::cerr << "The algorithm failed to compute SVD : " << info << std::endl;
      exit(0);
    }
  }

  if (!U.isInitialised() || U.nRows()!=n || U.nCols()!=rank) {
    U.init(n, rank, A.comm());
  }
  if (numProc==0) {
    std::vector<double> U_(n*n);
    for (unsigned int i=0; i<n; ++i) {
      for (unsigned int j=0; j<n; ++j) {
        U_[j*n+i] = Ut_[i*n+j];
      }
    }
    std::vector<int> idrow = linspace(0, n);
    for (unsigned int i=0; i<rank; ++i) {
      U(i).setValues(idrow, U_.data()+i*n);
    }
  }
  U.finalize();

  // if (!S.isInitialised() || S.size()!=rank) {
  //   S.init(rank, A.comm());
  // }
  // if (numProc==0) {
  //   std::vector<int> idrow = linspace(0, rank);
  //   S.setValues(idrow, S_.data());
  // }
  // S.finalize();

  if (!V.isInitialised() || V.nRows()!=m || V.nCols()!=rank) {
    V.init(m, rank, A.comm());
  }
  if (numProc==0) {
    std::vector<int> idrow = linspace(0, m);
    for (unsigned int i=0; i<rank; ++i) {
      V(i).setValues(idrow, V_.data()+i*m);
    }
  }
  V.finalize();
  
}


// void svd(const Matrix& A, VectorSet& U, Vector& S, VectorSet& V) {

//   assert(A.isAssembled());

//   int n = A.nRows(), m = A.nCols(), rank = std::min(n, m);
//   std::vector<double> A_(n*m);
//   for (unsigned int j=0; j<m; ++j) {
//     for (unsigned int i=0; i<n; ++i) {
//       A_[i+n*j] = A.getMatEl(i, j);
//     }
//   }

//   char cS = 'S';
//   int lwork = -1, info;
//   double w;
//   std::vector<double> U_(n*rank), S_(rank), Vt_(m*m);
//   dgesvd_(&cS, &cS, &n, &m, A_.data(), &n, S_.data(), U_.data(), &n, Vt_.data(), &m, &w, &lwork, &info);
//   lwork = (int) w;
//   std::vector<double> work(lwork);
//   dgesvd_(&cS, &cS, &n, &m, A_.data(), &n, S_.data(), U_.data(), &n, Vt_.data(), &m, work.data(), &lwork, &info);
//   if (info!=0) {
//     std::cerr << "The algorithm failed to compute SVD : " << info << std::endl;
//     exit(0);
//   }

//   if (!U.isInitialised() || U.nRows()!=n || U.nCols()!=rank) {
//     U.init(n, rank, A.comm());
//   }
//   for (unsigned int j=0; j<rank; ++j) {
//     for (unsigned int i=0; i<n; ++i) {
//       U.setMatEl(i, j, U_[i+n*j]);
//     }
//   }
//   U.finalize();

//   if (!S.isInitialised() || S.size()!=rank) {
//     S.init(rank, A.comm());
//   }
//   for (unsigned int k=0; k<rank; ++k) {
//     S.setVecEl(k, S_[k]);
//   }
//   S.finalize();

//   if (!V.isInitialised() || V.nRows()!=m || V.nCols()!=rank) {
//     V.init(m, rank, A.comm());
//   }
//   for (unsigned int j=0; j<rank; ++j) {
//     for (unsigned int i=0; i<m; ++i) {
//       V.setMatEl(i, j, Vt_[j+m*i]);
//     }
//   }
//   V.finalize();
  
// }


void brand(VectorSet& U, const VectorSet& Y, const VectorSet& Uk, const std::vector<double>& Sk, const double tol, const MPI_Comm& comm) {

  VectorSet UktU(Uk.nCols(), U.nCols(), 0.0, comm);
  MatTransposeMatMult(Uk, U, UktU);
  VectorSet A(U);
  A -= MatMatMult(Uk, UktU);

  VectorSet Q;
  Matrix R;
  qr(A, Q, R);

  Matrix K(UktU.nRows()+R.nRows(), UktU.nRows()+R.nRows(), comm);
  Matrix UktUmat(UktU);
  Matrix Ymat(Y);
  Matrix YYt(Ymat.nRows(), Ymat.nRows(), 0.0, comm);
  MatMatTransposeMult(Ymat, Ymat, YYt);
  Matrix RYYt(R.nRows(), Y.nRows(), 0.0, comm);
  MatMatMult(R, YYt, RYYt);
  
  Matrix subK(UktUmat.nRows(), UktUmat.nRows(), comm);
  for (unsigned int i=0; i<subK.nRows(); ++i) {
    subK.setMatEl(i, i, Sk[i]*Sk[i]);
  }
  subK.finalize();
  subK += MatMatMult(UktUmat, MatMatTransposeMult(YYt, UktUmat));
  for (unsigned int i=0; i<subK.nRows(); ++i) {
    for (unsigned int j=0; j<subK.nCols(); ++j) {
      K.setMatEl(i, j, subK.getMatEl(i, j));
    }
  }

  subK.init(R.nRows(), UktUmat.nRows(), 0.0, comm);
  MatMatTransposeMult(RYYt, UktUmat, subK);
  for (unsigned int i=0; i<subK.nRows(); ++i) {
    for (unsigned int j=0; j<subK.nCols(); ++j) {
      K.setMatEl(i+UktUmat.nRows(), j, subK.getMatEl(i, j));
    }
  }
  for (unsigned int j=0; j<subK.nRows(); ++j) {
    for (unsigned int i=0; i<subK.nCols(); ++i) {
      K.setMatEl(i, j+UktUmat.nRows(), subK.getMatEl(j, i));
    }
  }

  subK.init(R.nRows(), R.nRows(), 0.0, comm);
  MatMatTransposeMult(RYYt, R, subK);
  for (unsigned int i=0; i<subK.nRows(); ++i) {
    for (unsigned int j=0; j<subK.nCols(); ++j) {
      K.setMatEl(i+UktUmat.nRows(), j+UktUmat.nRows(), subK.getMatEl(i, j));
    }
  }

  K.finalize();

  std::vector<double> SS;
  VectorSet UU, VV;
  svd(K, UU, SS, VV);

  unsigned int rank = 0;
  double error1 = tol, error2 = 1.0;
  while (rank<SS.size() && error1<error2) {
    ++rank;
    error1 = 0.0;
    for (unsigned int iTerm=0; iTerm<rank; ++iTerm) {
      error1 += tol*tol*SS[iTerm];
    }
    error2 = 0.0;
    for (unsigned int iTerm=rank; iTerm<SS.size(); ++iTerm) {
      error2 += (1.0-tol*tol)*SS[iTerm];
    }
  }

  VectorSet UUU(Uk);
  UUU.push_back(Q);
  UU.resize(rank);
  U.resize(rank);
  MatMatMult(UUU, UU, U);

}
