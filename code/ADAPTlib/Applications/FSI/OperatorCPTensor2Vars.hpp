// Header file for operator tensor

#ifndef OperatorCPTensor2Vars_hpp
#define OperatorCPTensor2Vars_hpp

#include "../../Applications/FSI/Vector.hpp"
#include "../../Applications/FSI/VectorSet.hpp"
#include "../../Applications/FSI/Matrix.hpp"
#include "../../Applications/FSI/Operations.hpp"
#include "../../Applications/FSI/CPTensor2Vars.hpp"


class OperatorCPTensor2Vars {

private:

  std::vector<unsigned int> m_nDof_x_in;
  std::vector<unsigned int> m_nDof_x_out;
  unsigned int m_nDof_y_in;
  unsigned int m_nDof_y_out;
  unsigned int m_nTerms;
  unsigned int m_nVar;
  std::vector<std::vector<Matrix>> m_data;
  MPI_Comm m_comm;
  bool m_isInitialised;

public:

  OperatorCPTensor2Vars();
  OperatorCPTensor2Vars(const std::vector<unsigned int>& nDof_x_in, const std::vector<unsigned int>& nDof_x_out, const unsigned int nDof_y_in, const unsigned int nDof_y_out, const unsigned int nTerms, const MPI_Comm& comm=PETSC_COMM_WORLD);
  ~OperatorCPTensor2Vars();

  void init(const std::vector<unsigned int>& nDof_x_in, const std::vector<unsigned int>& nDof_x_out, const unsigned int nDof_y_in, const unsigned int nDof_y_out, const unsigned int nTerms, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void clear();

  inline unsigned int& nDof_x_in(const unsigned int i);
  inline const unsigned int& nDof_x_in(const unsigned int i) const;
  inline unsigned int& nDof_x_out(const unsigned int i);
  inline const unsigned int& nDof_x_out(const unsigned int i) const;
  inline unsigned int& nDof_y_in();
  inline const unsigned int& nDof_y_in() const;
  inline unsigned int& nDof_y_out();
  inline const unsigned int& nDof_y_out() const;
  inline const unsigned int nTerms() const;
  inline const unsigned int nVar() const;
  inline Matrix& operator () (const unsigned int iTerm, const unsigned int iVar);
  inline const Matrix& operator () (const unsigned int iTerm, const unsigned int iVar) const;
  inline const MPI_Comm& comm() const;
  inline const bool isInitialised() const;
  inline const bool isAssembled() const;

  void finalize();
  void apply(const CPTensor2Vars& input, CPTensor2Vars& output) const;
  CPTensor2Vars apply(const CPTensor2Vars& input) const;

};

unsigned int gmres(const OperatorCPTensor2Vars& operatorA, CPTensor2Vars& sol, const CPTensor2Vars& rhs, const double tolr, const unsigned int itmax, const Matrix& P, const double tolc, const MPI_Comm& comm);


/*
*  Inline methods
*/

inline unsigned int& OperatorCPTensor2Vars::nDof_x_in(const unsigned int i) {
  assert(i<m_nDof_x_in.size());
  return m_nDof_x_in[i];
};

inline const unsigned int& OperatorCPTensor2Vars::nDof_x_in(const unsigned int i) const {
  assert(i<m_nDof_x_in.size());
  return m_nDof_x_in[i];
};

inline unsigned int& OperatorCPTensor2Vars::nDof_x_out(const unsigned int i) {
  assert(i<m_nDof_x_out.size());
  return m_nDof_x_out[i];
};

inline const unsigned int& OperatorCPTensor2Vars::nDof_x_out(const unsigned int i) const {
  assert(i<m_nDof_x_out.size());
  return m_nDof_x_out[i];
};

inline unsigned int& OperatorCPTensor2Vars::nDof_y_in() {
  return m_nDof_y_in;
};

inline const unsigned int& OperatorCPTensor2Vars::nDof_y_in() const {
  return m_nDof_y_in;
};

inline unsigned int& OperatorCPTensor2Vars::nDof_y_out() {
  return m_nDof_y_out;
};

inline const unsigned int& OperatorCPTensor2Vars::nDof_y_out() const {
  return m_nDof_y_out;
};

inline const unsigned int OperatorCPTensor2Vars::nTerms() const {
  return m_nTerms;
};

inline const unsigned int OperatorCPTensor2Vars::nVar() const {
  return m_nVar;
};

inline Matrix& OperatorCPTensor2Vars::operator () (const unsigned int iTerm, const unsigned int iVar) {
  assert(iTerm<m_nTerms && iVar<m_nVar);
  return m_data[iTerm][iVar];
};

inline const Matrix& OperatorCPTensor2Vars::operator () (const unsigned int iTerm, const unsigned int iVar) const {
  assert(iTerm<m_nTerms && iVar<m_nVar);
  return m_data[iTerm][iVar];
};

inline const MPI_Comm& OperatorCPTensor2Vars::comm() const {
  return m_comm;
};

inline const bool OperatorCPTensor2Vars::isInitialised() const {
  return m_isInitialised;
};

inline const bool OperatorCPTensor2Vars::isAssembled() const {
  if (!m_isInitialised || m_data.size()!=m_nTerms || m_nVar!=2) {
    return false;
  }
  for (unsigned int i=0; i<m_nTerms; ++i) {
    if (m_data[i].size()!=m_nVar || !m_data[i][0].isAssembled() || m_data[i][0].nRows()!=sum(m_nDof_x_out) || m_data[i][0].nCols()!=sum(m_nDof_x_in) || !m_data[i][1].isAssembled() || m_data[i][1].nRows()!=m_nDof_y_out || m_data[i][1].nCols()!=m_nDof_y_in) {
      return false;
    }
  }
  return true;
};

#endif

// // Header file for operator tensor

// #ifndef OperatorCPTensor2Vars_hpp
// #define OperatorCPTensor2Vars_hpp

// #include "../../Applications/FSI/Vector.hpp"
// #include "../../Applications/FSI/VectorSet.hpp"
// #include "../../Applications/FSI/Matrix.hpp"
// #include "../../Applications/FSI/Operations.hpp"
// #include "../../Applications/FSI/TriCPTensor2Vars.hpp"
// #include "../../Applications/FSI/CPTensor2Vars.hpp"


// class OperatorCPTensor2Vars {

// private:

//   unsigned int m_nDof_x_u_in;
//   unsigned int m_nDof_x_u_out;
//   unsigned int m_nDof_x_p_in;
//   unsigned int m_nDof_x_p_out;
//   unsigned int m_nDof_x_d_in;
//   unsigned int m_nDof_x_d_out;
//   unsigned int m_nDof_x_in;
//   unsigned int m_nDof_x_out;
//   unsigned int m_nDof_y_in;
//   unsigned int m_nDof_y_out;
//   unsigned int m_nTerms;
//   unsigned int m_nVar;
//   std::vector<std::vector<Matrix>> m_data;
//   MPI_Comm m_comm;
//   bool m_isInitialised;

// public:

//   OperatorCPTensor2Vars();
//   OperatorCPTensor2Vars(const unsigned int nDof_x_in, const unsigned int nDof_x_out, const unsigned int nDof_y_in, const unsigned int nDof_y_out, const unsigned int nTerms, const MPI_Comm& comm=PETSC_COMM_WORLD);
//   OperatorCPTensor2Vars(const unsigned int nDof_x_u_in, const unsigned int nDof_x_u_out, const unsigned int nDof_x_p_in, const unsigned int nDof_x_p_out, const unsigned int nDof_x_d_in, const unsigned int nDof_x_d_out, const unsigned int nDof_y_in, const unsigned int nDof_y_out, const unsigned int nTerms, const MPI_Comm& comm=PETSC_COMM_WORLD);
//   ~OperatorCPTensor2Vars();

//   void init(const unsigned int nDof_x_in, const unsigned int nDof_x_out, const unsigned int nDof_y_in, const unsigned int nDof_y_out, const unsigned int nTerms, const MPI_Comm& comm=PETSC_COMM_WORLD);
//   void init(const unsigned int nDof_x_u_in, const unsigned int nDof_x_u_out, const unsigned int nDof_x_p_in, const unsigned int nDof_x_p_out, const unsigned int nDof_x_d_in, const unsigned int nDof_x_d_out, const unsigned int nDof_y_in, const unsigned int nDof_y_out, const unsigned int nTerms, const MPI_Comm& comm=PETSC_COMM_WORLD);
//   void clear();

//   inline unsigned int& nDof_x_in();
//   inline const unsigned int& nDof_x_in() const;
//   inline unsigned int& nDof_x_out();
//   inline const unsigned int& nDof_x_out() const;
//   inline unsigned int& nDof_y_in();
//   inline const unsigned int& nDof_y_in() const;
//   inline unsigned int& nDof_y_out();
//   inline const unsigned int& nDof_y_out() const;
//   inline const unsigned int nTerms() const;
//   inline const unsigned int nVar() const;
//   inline Matrix& operator () (const unsigned int iTerm, const unsigned int iVar);
//   inline const Matrix& operator () (const unsigned int iTerm, const unsigned int iVar) const;
//   inline const MPI_Comm& comm() const;
//   inline const bool isInitialised() const;
//   inline const bool isAssembled() const;

//   void finalize();
//   void apply(const CPTensor2Vars& tens1, CPTensor2Vars& tens2) const;
//   CPTensor2Vars apply(const CPTensor2Vars& tens1) const;
//   void apply(const TriCPTensor2Vars& tens1, TriCPTensor2Vars& tens2) const;
//   TriCPTensor2Vars apply(const TriCPTensor2Vars& tens1) const;

// };

// /*
// *  Inline methods
// */

// inline unsigned int& OperatorCPTensor2Vars::nDof_x_in() {
//   return m_nDof_x_in;
// };

// inline const unsigned int& OperatorCPTensor2Vars::nDof_x_in() const {
//   return m_nDof_x_in;
// };

// inline unsigned int& OperatorCPTensor2Vars::nDof_x_out() {
//   return m_nDof_x_out;
// };

// inline const unsigned int& OperatorCPTensor2Vars::nDof_x_out() const {
//   return m_nDof_x_out;
// };

// inline unsigned int& OperatorCPTensor2Vars::nDof_y_in() {
//   return m_nDof_y_in;
// };

// inline const unsigned int& OperatorCPTensor2Vars::nDof_y_in() const {
//   return m_nDof_y_in;
// };

// inline unsigned int& OperatorCPTensor2Vars::nDof_y_out() {
//   return m_nDof_y_out;
// };

// inline const unsigned int& OperatorCPTensor2Vars::nDof_y_out() const {
//   return m_nDof_y_out;
// };

// inline const unsigned int OperatorCPTensor2Vars::nTerms() const {
//   return m_nTerms;
// };

// inline const unsigned int OperatorCPTensor2Vars::nVar() const {
//   return m_nVar;
// };

// inline Matrix& OperatorCPTensor2Vars::operator () (const unsigned int iTerm, const unsigned int iVar) {
//   assert(iTerm<m_nTerms && iVar<m_nVar);
//   return m_data[iTerm][iVar];
// };

// inline const Matrix& OperatorCPTensor2Vars::operator () (const unsigned int iTerm, const unsigned int iVar) const {
//   assert(iTerm<m_nTerms && iVar<m_nVar);
//   return m_data[iTerm][iVar];
// };

// inline const MPI_Comm& OperatorCPTensor2Vars::comm() const {
//   return m_comm;
// };

// inline const bool OperatorCPTensor2Vars::isInitialised() const {
//   return m_isInitialised;
// };

// inline const bool OperatorCPTensor2Vars::isAssembled() const {
//   if (!m_isInitialised || m_data.size()!=m_nTerms || m_nVar!=2 || m_nDof_x_in!=(m_nDof_x_u_in+m_nDof_x_p_in+m_nDof_x_d_in) || m_nDof_x_out!=(m_nDof_x_u_out+m_nDof_x_p_out+m_nDof_x_d_out)) {
//     return false;
//   }
//   for (unsigned int i=0; i<m_nTerms; ++i) {
//     if (m_data[i].size()!=m_nVar || !m_data[i][0].isAssembled() || m_data[i][0].nRows()!=m_nDof_x_out || m_data[i][0].nCols()!=m_nDof_x_in || !m_data[i][1].isAssembled() || m_data[i][1].nRows()!=m_nDof_y_out || m_data[i][1].nCols()!=m_nDof_y_in) {
//       return false;
//     }
//   }
//   return true;
// };

// #endif


