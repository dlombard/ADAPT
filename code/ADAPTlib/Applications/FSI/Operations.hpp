/* 
*  Header for the Operations method
*/

#ifndef Operations_hpp
#define Operations_hpp

#include "../../Applications/FSI/Vector.hpp"
#include "../../Applications/FSI/VectorSet.hpp"
#include "../../Applications/FSI/Matrix.hpp"


void Vec2vector(const Vector& u, std::vector<double>& v);
std::vector<double> Vec2vector(const Vector& u);

void MatMult(const VectorSet& M, const Vector& u, Vector& v);
void MatMult(const Matrix& M, const Vector& u, Vector& v);
Vector MatMult(const VectorSet& M, const Vector& u);
Vector MatMult(const Matrix& M, const Vector& u);

void MatMultAdd(const Matrix& M, const Vector& u, const Vector& v, Vector& w);

void MatMultTranspose(const VectorSet& M, const Vector& u, Vector& v);
void MatMultTranspose(const Matrix& M, const Vector& u, Vector& v);

void MatMatMult(const VectorSet& A, const VectorSet& B, VectorSet& C);
void MatMatMult(const Matrix& A, const VectorSet& B, VectorSet& C);
void MatMatMult(const Matrix& A, const Matrix& B, Matrix& C);
VectorSet MatMatMult(const VectorSet& A, const VectorSet& B);
VectorSet MatMatMult(const Matrix& A, const VectorSet& B);
Matrix MatMatMult(const Matrix& A, const Matrix& B);

void MatTransposeMatMult(const VectorSet& A, const VectorSet& B, VectorSet& C);
void MatTransposeMatMult(const VectorSet& A, const VectorSet& B, Matrix& C);
void MatTransposeMatMult(const Matrix& A, const VectorSet& B, VectorSet& C);
void MatTransposeMatMult(const Matrix& A, const Matrix& B, Matrix& C);

void MatMatTransposeMult(const Matrix& A, const Matrix& B, Matrix& C);
Matrix MatMatTransposeMult(const Matrix& A, const Matrix& B);

void Solve(const Matrix& M, const Vector& u, Vector& v);

void qr(const VectorSet& V, VectorSet& Q, Matrix& R, const double tol=1.0e-16);
void svd(const Matrix& A, VectorSet& U, std::vector<double>& S, VectorSet& V);
void brand(VectorSet& U, const VectorSet& Y, const VectorSet& Uk, const std::vector<double>& Sk, const double tol, const MPI_Comm& comm);

#endif