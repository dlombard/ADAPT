#include "Vector.hpp"


/* 
*  Constructors/Destructors
*/

Vector::Vector() : m_data(nullptr), m_size(0), m_comm(PETSC_COMM_WORLD), m_isInitialised(false), m_isAssembled(false) {}

Vector::Vector(const unsigned int size, const MPI_Comm& comm) : m_isInitialised(false), m_isAssembled(false) {
  this->init(size, comm);
}

Vector::Vector(const unsigned int size, const double value, const MPI_Comm& comm) : m_isInitialised(false), m_isAssembled(false) {
  this->init(size, value, comm);
}

Vector::Vector(const std::vector<double>& value, const MPI_Comm& comm) : m_isInitialised(false), m_isAssembled(false) {
  this->init(value, comm);
}

Vector::Vector(const unsigned int size, const std::vector<unsigned int>& ind, const std::vector<double>& value, const MPI_Comm& comm) : m_isInitialised(false), m_isAssembled(false) {
  assert((ind.size()==0 || max(ind)<size) && ind.size()==value.size());
  this->init(size, ind, value, comm);
}

Vector::Vector(const Vector& v, const unsigned int low, const unsigned int high) : m_isInitialised(false), m_isAssembled(false) {
  assert(v.m_isAssembled && low<=high && high<=v.m_size);
  this->init(v, low, high);
}

Vector::Vector(const Vector& v) : m_isInitialised(false), m_isAssembled(false) {
  assert(v.m_isAssembled);
  this->init(v);
}

Vector::Vector(const std::string& filename, const MPI_Comm& comm) : m_isInitialised(false), m_isAssembled(false) {
  this->init(filename, comm);
}

Vector::~Vector() {
  this->clear();
}

void Vector::init(const unsigned int size, const MPI_Comm& comm) {
  this->clear();
  m_size = size;
  m_comm = comm;
  VecCreate(m_comm, &m_data);
  VecSetSizes(m_data, PETSC_DECIDE, m_size);
  VecSetFromOptions(m_data);
  m_isInitialised = true;
  m_isAssembled = false;
}

void Vector::init(const unsigned int size, const double value, const MPI_Comm& comm) {
  this->init(size, comm);
  this->finalize();
  if (fabs(value)!=0.0) {
    this->operator=(value);
  }
}

void Vector::init(const std::vector<double>& value, const MPI_Comm& comm) {
  this->init(value.size(), comm);
  for (unsigned int i=0; i<value.size(); ++i) {
    this->setVecEl(i, value[i]);
  }
  this->finalize();
}

void Vector::init(const unsigned int size, const std::vector<unsigned int>& ind, const std::vector<double>& value, const MPI_Comm& comm) {
  assert((ind.size()==0 || max(ind)<size) && ind.size()==value.size());
  this->init(size, comm);
  for (unsigned int i=0; i<ind.size(); ++i) {
    this->setVecEl(ind[i], value[i]);
  }
  this->finalize();
}

void Vector::init(const Vector& v, const unsigned int low, const unsigned int high) {
  assert(v.m_isAssembled && low<=high && high<=v.m_size);
  this->init(high-low, 0.0, v.m_comm);
  v.extract(low, high, *this);
}

void Vector::init(const Vector& v) {
  assert(v.m_isAssembled);
  this->init(v.m_size, 0.0, v.m_comm);
  this->copy(v);
}

void Vector::init(const std::string& filename, const MPI_Comm& comm) {
  this->clear();
  m_comm = comm;
  PetscViewer viewer;
  PetscViewerBinaryOpen(m_comm, filename.c_str(), FILE_MODE_READ, &viewer);
  VecCreate(m_comm, &m_data);
  VecLoad(m_data, viewer);
  PetscViewerDestroy(&viewer);
  PetscInt size;
  VecGetSize(m_data, &size);
  m_size = size;
  m_isInitialised = true;
  m_isAssembled = false;
  this->finalize();
}

void Vector::clear() {
  if (m_isInitialised) {
    this->finalize();
    VecDestroy(&m_data);
    m_data = nullptr;
    m_size = 0;
    m_isInitialised = false;
    m_isAssembled = false;
  }
}


/* 
*  Methods
*/

void Vector::getOwnershipRange(unsigned int& low, unsigned int& high) const {
  assert(m_isAssembled);
  int low_, high_;
  VecGetOwnershipRange(m_data, &low_, &high_);
  low = low_;
  high = high_;
}

double Vector::getVecEl(const unsigned int i) const {
  assert(m_isAssembled && i<m_size);
  int rank;
  MPI_Comm_rank(m_comm, &rank);
  int startInd, endInd;
  VecGetOwnershipRange(m_data, &startInd, &endInd);
  int ind = i, amIRoot = 0;
  double value = 0.0;
  if (ind >=startInd && ind<endInd) {
    VecGetValues(m_data, 1, &ind, &value);
    amIRoot = 1;
  }
  int consensus = amIRoot*rank, root = 0;
  MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, m_comm);
  MPI_Bcast(&value, 1, MPI_DOUBLE, root, m_comm);
  return value;
}

void Vector::setValues(const std::vector<int>& ind, double* value) {
  assert(m_isInitialised && (ind.size()==0 || max(ind)<m_size));
  VecSetValues(m_data, ind.size(), ind.data(), value, INSERT_VALUES);
}

void Vector::setVecEl(const unsigned int i, const double value, const bool PAR) {
  assert(m_isInitialised && i<m_size);
  int ind = i;
  if (!PAR) {
    int rank;
    MPI_Comm_rank(m_comm, &rank);
    if (rank==0){
      VecSetValues(m_data, 1, &ind, &value, INSERT_VALUES);
    }
  } else { // Parallel non-communicating way
    int startInd, endInd;
    VecGetOwnershipRange(m_data, &startInd ,&endInd);
    if (ind>=startInd && ind<endInd) {
      VecSetValues(m_data, 1, &ind, &value, INSERT_VALUES);
    }
  }
  m_isAssembled = false;
}

void Vector::finalize() {
  assert(m_isInitialised);
  if (!m_isAssembled) {
    VecAssemblyBegin(m_data);
    VecAssemblyEnd(m_data);
    m_isAssembled = true;
  }
}

void Vector::add(const Vector& v, const double scale) {
  assert(m_isAssembled && v.m_isAssembled && m_size==v.m_size && m_comm==v.m_comm);
  if (fabs(scale)!=0.0) {
    VecAXPY(m_data, scale, v.m_data);
  }
}

void Vector::add(const Vector& v, const unsigned int low, const unsigned int high, const double scale) {
  assert(m_isAssembled && v.m_isAssembled && low<=high && high<=m_size && v.m_size==(high-low));

  if (fabs(scale)!=0.0) {
    // Vector u(v.m_size, v.m_comm);
    // for (unsigned int i=0; i<v.m_size; ++i) {
    //   u.setVecEl(i, this->getVecEl(i+low));
    // }
    // u.finalize();
    // for (unsigned int i=0; i<v.m_size; ++i) {
    //   this->setVecEl(i+low, u.getVecEl(i)+scale*v.getVecEl(i));
    // }
    // this->finalize();
    IS is;
    ISCreateStride(m_comm, v.m_size, low, 1, &is);
    VecISAXPY(m_data, is, scale, v.m_data);
    ISDestroy(&is);
  }

}

void Vector::extract(const unsigned int low, const unsigned int high, Vector& v) const {
  assert(m_isAssembled && v.m_isAssembled && low<=high && high<=m_size && v.m_size==(high-low) && m_comm==v.m_comm);
  // for (unsigned int i=0; i<v.m_size; ++i) {
  //   v.setVecEl(i, this->getVecEl(i+low));
  // }
  // v.finalize();
  unsigned int start, end;
  v.getOwnershipRange(start, end);
  PetscInt *indices;
  PetscMalloc1(end-start, &indices);
  for (unsigned int i=0; i<v.m_size; ++i) {
    if (start<=i && i<end) {
      indices[i-start] = i+low;
    }
  }
  IS is;
  ISCreateGeneral(m_comm, end-start, indices, PETSC_COPY_VALUES, &is);
  PetscFree(indices);
  VecScatter ctx;
  VecScatterCreate(m_data, is, v.m_data, NULL, &ctx);
  ISDestroy(&is);
  VecScatterBegin(ctx, m_data, v.m_data, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(ctx, m_data, v.m_data, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterDestroy(&ctx);
}

void Vector::copy(const Vector& v) {
  assert(m_isAssembled && v.m_isAssembled && m_size==v.m_size && m_comm==v.m_comm);
  VecCopy(v.m_data, m_data);
}

void Vector::print() const {
  assert(m_isAssembled);
  VecView(m_data, PETSC_VIEWER_STDOUT_WORLD);
}

void Vector::save(const std::string& filename) const {
  assert(m_isAssembled);
  PetscViewer viewer;
  PetscViewerBinaryOpen(m_comm, filename.c_str(), FILE_MODE_WRITE, &viewer);
  VecView(m_data, viewer);
  PetscViewerDestroy(&viewer);
}


/* 
*  Operators
*/

Vector& Vector::operator += (const double value) {
  assert(m_isAssembled);
  VecShift(m_data, value);
  return *this;
}

Vector& Vector::operator += (const Vector& v) {
  assert(m_isAssembled && v.m_isAssembled && m_size==v.m_size && m_comm==v.m_comm);
  this->add(v, 1.0);
  return *this;
}

Vector& Vector::operator -= (const double value) {
  assert(m_isAssembled);
  this->operator+=(-value);
  return *this;
}

Vector& Vector::operator -= (const Vector& v) {
  assert(m_isAssembled && v.m_isAssembled && m_size==v.m_size && m_comm==v.m_comm);
  this->add(v, -1.0);
  return *this;
}

Vector& Vector::operator *= (const double scale) {
  assert(m_isAssembled);
  VecScale(m_data, scale);
  return *this;
}

Vector& Vector::operator *= (const Vector& v) {
  assert(m_isAssembled && v.m_isAssembled);
  VecPointwiseMult(m_data, v.m_data , m_data);
  return *this;
}

Vector& Vector::operator /= (const double scale) {
  assert(m_isAssembled && fabs(scale)>DBL_EPSILON);
  this->operator*=(1.0/scale);
  return *this;
}

Vector& Vector::operator = (const double value) {
  assert(m_isAssembled);
  VecSet(m_data, value);
  return *this;
}

Vector& Vector::operator = (const Vector& v) {
  assert(v.m_isAssembled);
  if (!m_isAssembled || m_size!=v.m_size || m_comm!=v.m_comm) {
    this->init(v.m_size, 0.0, v.m_comm);
  }
  this->copy(v);
  return *this;
}


/*
*  Friend methods
*/

double dot(const Vector& v1, const Vector& v2) {
  assert(v1.m_isAssembled && v2.m_isAssembled && v1.m_size==v2.m_size && v1.m_comm==v2.m_comm);
  double value;
  VecDot(v1.m_data, v2.m_data, &value);
  return value;
}

double norm(const Vector& v) {
  assert(v.m_isAssembled);
  double value;
  VecNorm(v.m_data, NORM_2, &value);
  return value;
}


/*
*  Non-member methods
*/

Vector e(const unsigned int i, const unsigned int size, const MPI_Comm& comm) {
  assert(i<size);
  Vector v(size, comm);
  v.setVecEl(i, 1.0);
  v.finalize();
  return v;
}

Vector operator + (const Vector& v1, const Vector& v2) {
  assert(v1.isAssembled() && v2.isAssembled() && v1.size()==v2.size());
  Vector v3(v1);
  v3 += v2;
  return v3;
}

Vector operator - (const Vector& v1, const Vector& v2) {
  assert(v1.isAssembled() && v2.isAssembled() && v1.size()==v2.size());
  Vector v3(v1);
  v3 -= v2;
  return v3;
}
