#include "Testcase.hpp"


/* 
*  Constructors/Destructors
*/

Testcase::Testcase() : m_isInitialised(false) {}

Testcase::Testcase(const std::string& filename) {
  this->init(filename);
}

Testcase::Testcase(const Testcase& test) {
	assert(test.m_isInitialised);
  this->init(test);
}

Testcase::~Testcase() {}

void Testcase::init(const std::string& filename) {
  m_isInitialised = this->load(filename);
}

void Testcase::init(const Testcase& test) {
	assert(test.m_isInitialised);
	this->operator=(test);
}

void Testcase::clear() {
	m_folderpath_input_data.clear();
	m_folderpath_input_observation.clear();
	m_folderpath_output_parameter.clear();
	m_folderpath_output_sol.clear();
  m_isInitialised = false;
}


/* 
*  Methods
*/

bool Testcase::load(const std::string& filename) {

	bool isValid_nFluid(false), isValid_nSolid(false), isValid_dt(false), isValid_Press(false), isValid_Tstar(false), isValid_c(false), isValid_nStep(false), isValid_folderpath_input_data(false), isValid_folderpath_input_observation(false), isValid_folderpath_output_parameter(false), isValid_folderpath_output_sol(false), isValid_tolr_gmres(false), isValid_itmax_gmres(false), isValid_tolc_gmres(false), isValid_tolc_interpol(false), isValid_itmax_mcmc(false), isValid_step_mcmc(false), isValid_nParameter(false);
	std::ifstream file(filename.c_str(), std::ifstream::in);

	if (!file) {

		std::cerr << "Error opening file: " << filename << std::endl;
		return false;

	} else {

		std::string label;
		while (!file.eof()) {

			file >> label;
			if (label=="nFluid") {
			  file >> m_nFluid;
			  isValid_nFluid = (m_nFluid>=0);
			} else if (label=="nSolid") {
			  file >> m_nSolid;
			  isValid_nSolid = (m_nSolid>=0);
			} else if (label=="nStep") {
			  file >> m_nStep;
			  isValid_nStep = (m_nStep>=0);
			} else if (label=="dt") {
			  file >> m_dt;
			  isValid_dt = (m_dt>0.0);
			} else if (label=="Press") {
			  file >> m_Press;
			  isValid_Press = (m_Press>=0.0);
			} else if (label=="Tstar") {
			  file >> m_Tstar;
			  isValid_Tstar = (m_Tstar>=0.0);
			} else if (label=="c") {
			  file >> m_c;
			  isValid_c = (m_c>=0.0);
			} else if (label=="folderpath_input_data") {
			  file >> m_folderpath_input_data;
			  isValid_folderpath_input_data = (m_folderpath_input_data.size()>0);
			} else if (label=="folderpath_input_observation") {
			  file >> m_folderpath_input_observation;
			  isValid_folderpath_input_observation = (m_folderpath_input_observation.size()>0);
			} else if (label=="folderpath_output_parameter") {
			  file >> m_folderpath_output_parameter;
			  isValid_folderpath_output_parameter = (m_folderpath_output_parameter.size()>0);
			} else if (label=="folderpath_output_sol") {
			  file >> m_folderpath_output_sol;
			  isValid_folderpath_output_sol = (m_folderpath_output_sol.size()>0);
			} else if (label=="tolr_gmres") {
			  file >> m_tolr_gmres;
			  isValid_tolr_gmres = (m_tolr_gmres>0.0);
			} else if (label=="itmax_gmres") {
			  file >> m_itmax_gmres;
			  isValid_itmax_gmres = (m_itmax_gmres>=0);
			} else if (label=="tolc_gmres") {
			  file >> m_tolc_gmres;
			  isValid_tolc_gmres = (0.0<=m_tolc_gmres && m_tolc_gmres<1.0);
			} else if (label=="tolc_interpol") {
			  file >> m_tolc_interpol;
			  isValid_tolc_interpol = (0.0<=m_tolc_interpol && m_tolc_interpol<=1.0);
			} else if (label=="itmax_mcmc") {
			  file >> m_itmax_mcmc;
			  isValid_itmax_mcmc = (m_itmax_mcmc>=0);
			} else if (label=="step_mcmc") {
        file >> m_step_mcmc;
        isValid_step_mcmc = (m_step_mcmc>0);
			} else if (label=="nParameter") {
			  file >> m_nParameter;
			  isValid_nParameter = (m_nParameter>=1);
			}
		}
		file.close();	

	}

	if (!(isValid_nFluid && isValid_nSolid && isValid_dt && isValid_Press && isValid_Tstar && isValid_c && isValid_nStep && isValid_folderpath_input_data && isValid_folderpath_input_observation && isValid_folderpath_output_parameter && isValid_folderpath_output_sol && isValid_tolr_gmres && isValid_itmax_gmres && isValid_tolc_gmres && isValid_tolc_interpol && isValid_itmax_mcmc && isValid_step_mcmc && isValid_nParameter)) {
		std::cerr << "Missing or invalid input value(s) " << std::endl;
		return false;
	}
	return true;

}


/* 
*  Operators
*/

Testcase& Testcase::operator = (const Testcase& test) {
	assert(test.m_isInitialised);
  m_nFluid = test.m_nFluid;
  m_nSolid = test.m_nSolid;
  m_nStep = test.m_nStep;
  m_dt = test.m_dt;
  m_Press = test.m_Press;
  m_Tstar = test.m_Tstar;
  m_c = test.m_c;
  m_folderpath_input_data = test.m_folderpath_input_data;
  m_folderpath_input_observation = test.m_folderpath_input_observation;
  m_folderpath_output_parameter = test.m_folderpath_output_parameter;
  m_folderpath_output_sol = test.m_folderpath_output_sol;
  m_tolr_gmres = test.m_tolr_gmres;
  m_itmax_gmres = test.m_itmax_gmres;
  m_tolc_gmres = test.m_tolc_gmres;
  m_tolc_interpol = test.m_tolc_interpol;
  m_itmax_mcmc = test.m_itmax_mcmc;
  m_step_mcmc = test.m_step_mcmc;
  m_nParameter = test.m_nParameter;
  m_isInitialised = true;
  return *this;
}

