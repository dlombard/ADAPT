#include "MetropolisHastingsStokes2D.hpp"


/* 
*  Constructors/Destructors
*/

MetropolisHastingsStokes2D::MetropolisHastingsStokes2D() : m_isInitialised(false) {}

MetropolisHastingsStokes2D::MetropolisHastingsStokes2D(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm) : m_isInitialised(false) {
  assert(lbParameter.size()==ubParameter.size());
  this->init(filename, lbParameter, ubParameter, comm);
}

MetropolisHastingsStokes2D::~MetropolisHastingsStokes2D() {
	this->clear();
}

void MetropolisHastingsStokes2D::init(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm) {
	
	assert(lbParameter.size()==ubParameter.size());
	
	m_testcase.init(filename);
	m_comm = comm;
	m_lbParameter = lbParameter;
	m_ubParameter = ubParameter;

	m_parameter.init(m_lbParameter, m_ubParameter, m_testcase.nParameter());
	PetscPrintf(m_comm, "Number of particles: %d\n", m_parameter.nOfPt());
	this->init_observation();

  m_interpol.init(m_testcase, m_comm);
  m_solver.init(m_testcase, m_lbParameter, m_ubParameter, m_comm);

  // unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  // m_generator = std::default_random_engine(seed);
  m_uniform_distribution = std::uniform_real_distribution<double>(0.0, 1.0);
  m_normal_distribution.resize(m_parameter.dim());
  for (unsigned int iComp=0; iComp<m_normal_distribution.size(); ++iComp) {
    m_normal_distribution[iComp] = std::normal_distribution<double>(0.0, m_testcase.step_mcmc()*(m_ubParameter[iComp]-m_lbParameter[iComp]));
  }
  
	m_isInitialised = true;
}


void MetropolisHastingsStokes2D::clear() {
	if (!m_isInitialised) {
		m_testcase.clear();
		m_parameter.clear();
		m_HtX1.clear();
		m_HtX2.clear();
		m_HtH1.clear();
		m_HtH2.clear();
		m_h11.clear();
		m_h12.clear();
		m_h21.clear();
		m_h22.clear();
		m_h31.clear();
		m_h32.clear();
		m_observation1.clear();
		m_observation2.clear();
		m_H1.clear();
		m_H2.clear();
		m_sol1_terms_y.clear();
		m_sol2_terms_y.clear();
		m_sol1_old.clear();
		m_sol2_old.clear();
		m_sol1.clear();
		m_sol2.clear();
		m_interpol.clear();
		m_solver.clear();
		m_normal_distribution.clear();
		m_lbParameter.clear();
		m_ubParameter.clear();
	  m_isInitialised = false;
	}
}


/* 
*  Methods
*/

void MetropolisHastingsStokes2D::init_observation() {
	
	m_H1.init(3*m_testcase.nFluid()+2*m_testcase.nSolid(), 3*m_testcase.nFluid()+2*m_testcase.nSolid(), m_comm);
	for (unsigned int i=0; i<2*m_testcase.nFluid(); ++i) {
    m_H1.setMatEl(i, i, 1.0);
  }
	m_H1.finalize();
  
  m_H2.init(2*m_testcase.nSolid(), 2*m_testcase.nSolid(), 0.0, m_comm);

  VectorSet observation1(3*m_testcase.nFluid()+2*m_testcase.nSolid(), m_testcase.nStep()+1, PETSC_COMM_WORLD);
  for (unsigned int k=0; k<observation1.nCols(); ++k) {
    observation1(k).init(m_testcase.folderpath_input_observation() + "sol1_" + std::to_string(k) + ".dat", PETSC_COMM_WORLD);
  }
  observation1.finalize();
  m_observation1.init(m_H1.nRows(), observation1.nCols(), 0.0, m_comm);
	MatMatMult(m_H1, observation1, m_observation1);

  VectorSet observation2(2*m_testcase.nSolid(), m_testcase.nStep()+1, PETSC_COMM_WORLD);
  for (unsigned int k=0; k<observation1.nCols(); ++k) {
    observation2(k).init(m_testcase.folderpath_input_observation() + "sol2_" + std::to_string(k) + ".dat", PETSC_COMM_WORLD);
  }
  observation2.finalize();
	m_observation2.init(m_H2.nRows(), observation2.nCols(), 0.0, m_comm);
	MatMatMult(m_H2, observation2, m_observation2);

	m_HtX1.init(m_H1.nCols(), m_observation1.nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_H1, m_observation1, m_HtX1);

	m_HtX2.init(m_H2.nCols(), m_observation2.nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_H2, m_observation2, m_HtX2);

	m_HtH1.init(m_H1.nCols(), m_H1.nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_H1, m_H1, m_HtH1);

	m_HtH2.init(m_H2.nCols(), m_H2.nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_H2, m_H2, m_HtH2);

	m_h11 = vecnorm(m_observation1);

	m_h12 = vecnorm(m_observation2);

}


void MetropolisHastingsStokes2D::update_solution(const PointCloud& parameter, const unsigned int k, const bool update_interpol) {

	assert(m_isInitialised && k<m_testcase.nStep());

  if (update_interpol) {
  	if (k==0) {
  		m_sol1_old.init({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, parameter.nOfPt(), {0, 0, 0}, m_comm);
    	m_sol2_old.init({2*m_testcase.nSolid()}, parameter.nOfPt(), {0}, m_comm);
  	} else {
  		m_interpol.solve(parameter, k, m_sol1_terms_y, m_sol2_terms_y, m_sol1_old, m_sol2_old);
  	}
  	m_solver.solve(parameter, (k+1)*m_testcase.dt(), m_sol1_old, m_sol2_old, m_sol1, m_sol2);
  } else {
  	m_interpol.solve(parameter, k+1, m_sol1_terms_y, m_sol2_terms_y);
  }
}


std::vector<double> MetropolisHastingsStokes2D::update_error(const unsigned int k, const bool update_interpol) {
	assert(m_isInitialised && k<m_testcase.nStep());

	std::vector<double> error(m_parameter.nOfPt(), 0.0);

	if (update_interpol) {

		if (k==0) {

			CPTensor2Vars observation1_old(m_H1, m_sol1_old), observation2_old(m_H2, m_sol2_old);
			double observation1_norm2 = pow(norm(m_observation1(0)), 2), observation2_norm2 = pow(norm(m_observation2(0)), 2);
		  std::vector<double> observation1_norm = vecnorm(observation1_old), observation2_norm = vecnorm(observation2_old);
		  for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
				error[iPt] += observation1_norm2+pow(observation1_norm[iPt], 2);
			}
			for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
				error[iPt] += observation2_norm2+pow(observation2_norm[iPt], 2);
			}
		  for (unsigned int iTerm=0; iTerm<observation1_old.rank(0); ++iTerm) {
		  	double value = 2.0*observation1_old.coeffs(iTerm)*dot(observation1_old.terms_x(iTerm), m_observation1(0));
		  	for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
					error[iPt] -= value*observation1_old.terms_y().getMatEl(iPt,iTerm);
				}
		  }
		  for (unsigned int iTerm=0; iTerm<observation2_old.rank(0); ++iTerm) {
		  	double value = 2.0*observation2_old.coeffs(iTerm)*dot(observation2_old.terms_x(iTerm), m_observation2(0));
		  	for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
					error[iPt] -= value*observation2_old.terms_y().getMatEl(iPt,iTerm);
				}
		  }

		}	else {

		  double value1 = 0.0;
		  for (unsigned int l=0; l<k+1; ++l) {
	  		value1 += pow(m_h11[l], 2);
		  }
		  for (unsigned int l=0; l<k+1; ++l) {
	  		value1 += pow(m_h12[l], 2);
		  }
		  for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
		  	double value2 = 0.0;
		  	for (unsigned int l=0; l<k+1; ++l) {
		  		value2 += dot(m_h21(l), m_sol1_terms_y(l+(k+1)*iPt)); 
				}
				for (unsigned int l=0; l<k+1; ++l) {
		  		value2 += dot(m_h22(l), m_sol2_terms_y(l+(k+1)*iPt)); 
				}
				double value3 = 0.0;
		  	for (unsigned int l=0; l<k+1; ++l) {
		  		value3 += dot(MatMult(m_h31, m_sol1_terms_y(l+(k+1)*iPt)), m_sol1_terms_y(l+(k+1)*iPt)); 
				}
				for (unsigned int l=0; l<k+1; ++l) {
		  		value3 += dot(MatMult(m_h32, m_sol2_terms_y(l+(k+1)*iPt)), m_sol2_terms_y(l+(k+1)*iPt)); 
				}
				error[iPt] += value1-2.0*value2+value3;
		  }

		}

	  CPTensor2Vars observation1(m_H1, m_sol1), observation2(m_H2, m_sol2);
	  double observation1_norm2 = pow(norm(m_observation1(k+1)), 2), observation2_norm2 = pow(norm(m_observation2(k+1)), 2);
	  std::vector<double> observation1_norm = vecnorm(observation1), observation2_norm = vecnorm(observation2);
	  for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
			error[iPt] += observation1_norm2+pow(observation1_norm[iPt], 2);
		}
		for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
			error[iPt] += observation2_norm2+pow(observation2_norm[iPt], 2);
		}
	  for (unsigned int iTerm=0; iTerm<observation1.rank(0); ++iTerm) {
	  	double value = 2.0*observation1.coeffs(iTerm)*dot(observation1.terms_x(iTerm), m_observation1(k+1));
	  	for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
				error[iPt] -= value*observation1.terms_y().getMatEl(iPt,iTerm);
			}
	  }
	  for (unsigned int iTerm=0; iTerm<observation2.rank(0); ++iTerm) {
	  	double value = 2.0*observation2.coeffs(iTerm)*dot(observation2.terms_x(iTerm), m_observation2(k+1));
	  	for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
				error[iPt] -= value*observation2.terms_y().getMatEl(iPt,iTerm);
			}
	  }

  } else {

	  double value1 = 0.0;
	  for (unsigned int l=0; l<k+2; ++l) {
  		value1 += pow(m_h11[l], 2);
	  }
	  for (unsigned int l=0; l<k+2; ++l) {
  		value1 += pow(m_h12[l], 2);
	  }
	  for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
	  	double value2 = 0.0;
	  	for (unsigned int l=0; l<k+2; ++l) {
	  		value2 += dot(m_h21(l), m_sol1_terms_y(l+(k+2)*iPt)); 
			}
			for (unsigned int l=0; l<k+2; ++l) {
	  		value2 += dot(m_h22(l), m_sol2_terms_y(l+(k+2)*iPt)); 
			}
			double value3 = 0.0;
	  	for (unsigned int l=0; l<k+2; ++l) {
	  		value3 += dot(MatMult(m_h31, m_sol1_terms_y(l+(k+2)*iPt)), m_sol1_terms_y(l+(k+2)*iPt)); 
			}
			for (unsigned int l=0; l<k+2; ++l) {
	  		value3 += dot(MatMult(m_h32, m_sol2_terms_y(l+(k+2)*iPt)), m_sol2_terms_y(l+(k+2)*iPt)); 
			}
			error[iPt] += value1-2.0*value2+value3;
	  }

  }

  return error;

}


void MetropolisHastingsStokes2D::update_basis(const unsigned int k) {

	assert(m_isInitialised && k<m_testcase.nStep());

	if (k==0) {
		m_interpol.update(m_sol1, m_sol2);
	} else {
		m_interpol.update(m_sol1, m_sol2, m_sol1_terms_y, m_sol2_terms_y);
	}

	m_h21.init(m_interpol.terms1_x().nCols(), k+2, 0.0, m_comm);
	for (unsigned int l=0; l<m_h21.nCols(); ++l) {
		MatMultTranspose(m_interpol.terms1_x(), m_HtX1(l), m_h21(l));
	}

	m_h22.init(m_interpol.terms2_x().nCols(), k+2, 0.0, m_comm);
	for (unsigned int l=0; l<m_h22.nCols(); ++l) {
		MatMultTranspose(m_interpol.terms2_x(), m_HtX2(l), m_h22(l));
	}

	m_h31.init(m_interpol.terms1_x().nCols(), m_interpol.terms1_x().nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_interpol.terms1_x(), MatMatMult(m_HtH1, m_interpol.terms1_x()), m_h31);

	m_h32.init(m_interpol.terms2_x().nCols(), m_interpol.terms2_x().nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_interpol.terms2_x(), MatMatMult(m_HtH2, m_interpol.terms2_x()), m_h32);

}


void MetropolisHastingsStokes2D::save_solution(const PointCloud& parameter, const unsigned int k) const {
	assert(m_isInitialised);

	VectorSet sol1_terms_y, sol2_terms_y;
	CPTensor2Vars sol1, sol2;
	m_interpol.solve(parameter, k, sol1_terms_y, sol2_terms_y);

	Matrix terms1_x(m_interpol.terms1_x());
	terms1_x.save(m_testcase.folderpath_output_sol() + "sol1_terms_x.dat");
	Matrix terms1_y(sol1_terms_y);
	terms1_y.save(m_testcase.folderpath_output_sol() + "sol1_terms_y.dat");
	Matrix terms2_x(m_interpol.terms2_x());
	terms2_x.save(m_testcase.folderpath_output_sol() + "sol2_terms_x.dat");
	Matrix terms2_y(sol2_terms_y);
	terms2_y.save(m_testcase.folderpath_output_sol() + "sol2_terms_y.dat");
}


void MetropolisHastingsStokes2D::solve() {

	assert(m_isInitialised);

	m_parameter.save(m_testcase.folderpath_output_parameter() + "theta_0.dat");
	for (unsigned int k=0; k<m_testcase.nStep(); ++k) {
		this->update_solution(m_parameter, k, true);
		std::vector<double> parameter_error = this->update_error(k, true);
		this->update_basis(k);
		for (unsigned int it=0; it<m_testcase.itmax_mcmc(); ++it) {
			PointCloud m_candidate(m_parameter.dim(), m_parameter.nOfPt());
			for (unsigned int iPt=0; iPt<m_candidate.nOfPt(); ++iPt) {
				for(unsigned int iComp=0; iComp<m_candidate.dim(); ++iComp) {
	        m_candidate(iPt, iComp) = std::max(m_lbParameter[iComp], std::min(m_ubParameter[iComp], m_parameter(iPt, iComp)+m_normal_distribution[iComp](m_generator)));
	      }
			}
			this->update_solution(m_candidate, k, false);
			std::vector<double> candidate_error = this->update_error(k, false);
			for (unsigned int iPt=0; iPt<parameter_error.size(); ++iPt) {
				double p = std::max(1.0e-16, m_uniform_distribution(m_generator));
				if (std::log(p)<=(parameter_error[iPt]-candidate_error[iPt])) {
					m_parameter(iPt) = m_candidate(iPt);
		    	parameter_error[iPt] = candidate_error[iPt];
		    }
		  }
		}
		m_parameter.save(m_testcase.folderpath_output_parameter() + "theta_" + std::to_string(k+1) + ".dat");
	}
	this->save_solution(m_parameter, m_testcase.nStep());
}
