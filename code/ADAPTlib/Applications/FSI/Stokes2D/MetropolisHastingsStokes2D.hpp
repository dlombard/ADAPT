#ifndef MetropolisHastingsStokes2D_hpp
#define MetropolisHastingsStokes2D_hpp

// Including headers:
#include "../../../Applications/FSI/Vector.hpp"
#include "../../../Applications/FSI/VectorSet.hpp"
#include "../../../Applications/FSI/Matrix.hpp"
#include "../../../Applications/FSI/Operations.hpp"
#include "../../../Applications/FSI/CPTensor2Vars.hpp"
#include "../../../Applications/FSI/OperatorCPTensor2Vars.hpp"
#include "../../../Applications/FSI/Testcase.hpp"
#include "../../../Applications/FSI/PointCloud.hpp"
#include "../../../Applications/FSI/Stokes2D/InterpolatorStokes2D.hpp"
#include "../../../Applications/FSI/Stokes2D/SolverStokes2D.hpp"


class MetropolisHastingsStokes2D {

private:

  Testcase m_testcase;
  PointCloud m_parameter;
  MPI_Comm m_comm;
  VectorSet m_HtX1;
  VectorSet m_HtX2;
  Matrix m_HtH1;
  Matrix m_HtH2;
  std::vector<double> m_h11;
  std::vector<double> m_h12;
  VectorSet m_h21;
  VectorSet m_h22;
  VectorSet m_h31;
  VectorSet m_h32;
  VectorSet m_observation1;
  VectorSet m_observation2;
  Matrix m_H1;
  Matrix m_H2;
  VectorSet m_sol1_terms_y;
  VectorSet m_sol2_terms_y;
  CPTensor2Vars m_sol1_old;
  CPTensor2Vars m_sol2_old;
  CPTensor2Vars m_sol1;
  CPTensor2Vars m_sol2;
  InterpolatorStokes2D m_interpol;
  SolverStokes2D m_solver;
  std::default_random_engine m_generator;
  std::uniform_real_distribution<double> m_uniform_distribution;
  std::vector<std::normal_distribution<double>> m_normal_distribution;
  std::vector<double> m_lbParameter;
  std::vector<double> m_ubParameter;
  bool m_isInitialised;

  void init_observation();
  void update_solution(const PointCloud& candidate, const unsigned int k, const bool update_interpol=false);
  std::vector<double> update_error(const unsigned int k, const bool update_interpol=false);
  void update_basis(const unsigned int k);
  void save_solution(const PointCloud& parameter, const unsigned int k) const;

public:

  MetropolisHastingsStokes2D();
  MetropolisHastingsStokes2D(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm=PETSC_COMM_WORLD);
  ~MetropolisHastingsStokes2D();

  void init(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void solve();
  void clear();

};

#endif
