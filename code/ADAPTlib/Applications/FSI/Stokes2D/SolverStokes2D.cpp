#include "SolverStokes2D.hpp"


/* 
*  Constructors/Destructors
*/

SolverStokes2D::SolverStokes2D() : m_isInitialised(false) {}

SolverStokes2D::SolverStokes2D(const Testcase& testcase, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm) : m_isInitialised(false) {
  this->init(testcase, lbParameter, ubParameter, comm);
}

SolverStokes2D::~SolverStokes2D() {
	this->clear();
}

void SolverStokes2D::init(const Testcase& testcase, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm) {
	m_comm = comm;
	m_testcase.init(testcase);
  this->load_data(lbParameter, ubParameter);
	m_isInitialised = true;
}

void SolverStokes2D::clear() {
	if (m_isInitialised) {
		m_testcase.clear();
		m_operatorA.clear();
	  m_operatorB.clear();
	  m_operatorC.clear();
	  m_tensD.clear();
	  m_P.clear();
	  m_isInitialised = false;
	}
}


/* 
*  Methods
*/

void SolverStokes2D::load_data(const std::vector<double>& lbParameter, const std::vector<double>& ubParameter) {

	// Assemble m_operatorA
	m_operatorA.init({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, {2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, 0, 0, 7, m_comm);
	m_operatorA(0, 0).init(m_testcase.folderpath_input_data() + "Arhof.dat", m_comm);
	m_operatorA(1, 0).init(m_testcase.folderpath_input_data() + "Amu.dat", m_comm);
	m_operatorA(2, 0).init(m_testcase.folderpath_input_data() + "Arhos.dat", m_comm);
	m_operatorA(3, 0).init(m_testcase.folderpath_input_data() + "Alambda1.dat", m_comm);
	m_operatorA(4, 0).init(m_testcase.folderpath_input_data() + "Alambda2.dat", m_comm);
	m_operatorA(5, 0).init(m_testcase.folderpath_input_data() + "Ainvmu.dat", m_comm);
	m_operatorA(6, 0).init(m_testcase.folderpath_input_data() + "Ac.dat", m_comm);

	// Assemble m_operatorB
	m_operatorB.init({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, {2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, 0, 0, 6, m_comm);
	m_operatorB(0, 0).init(m_testcase.folderpath_input_data() + "Brhof.dat", m_comm);
	m_operatorB(1, 0).init(m_testcase.folderpath_input_data() + "Bmu.dat", m_comm);
	m_operatorB(2, 0).init(m_testcase.folderpath_input_data() + "Brhos.dat", m_comm);
	m_operatorB(3, 0).init(m_testcase.folderpath_input_data() + "Blambda1.dat", m_comm);
	m_operatorB(4, 0).init(m_testcase.folderpath_input_data() + "Blambda2.dat", m_comm);
	m_operatorB(5, 0).init(m_testcase.folderpath_input_data() + "Bc.dat", m_comm);

	// Assemble m_operatorC
	m_operatorC.init({2*m_testcase.nSolid()}, {3*m_testcase.nFluid()+2*m_testcase.nSolid()}, 0, 0, 1, m_comm);
	m_operatorC(0, 0).init(m_testcase.folderpath_input_data() + "Brhos_aux.dat", m_comm);

	// Assemble m_tensD
	m_tensD.init({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, 0, {1, 0, 0}, m_comm);
	m_tensD.coeffs(0) = 1.0;
	m_tensD.terms_x(0).init(m_testcase.folderpath_input_data() + "B1c.dat", m_comm);

	// Preconditioner matrix
	m_P.init(m_operatorA(6, 0));
	m_P.add(m_operatorA(0, 0), (lbParameter[0]+ubParameter[0])/2.0);
	m_P.add(m_operatorA(1, 0), (lbParameter[1]+ubParameter[1])/2.0);
	m_P.add(m_operatorA(2, 0), (lbParameter[2]+ubParameter[2])/2.0);
	m_P.add(m_operatorA(3, 0), (lbParameter[3]+ubParameter[3])/2.0);
	m_P.add(m_operatorA(4, 0), (lbParameter[4]+ubParameter[4])/2.0);
	m_P.add(m_operatorA(5, 0), 2.0/(lbParameter[1]+ubParameter[1]));
	m_P.inv();
}


void SolverStokes2D::solve(const PointCloud& parameter, const double t, const CPTensor2Vars& sol1_old, const CPTensor2Vars& sol2_old, CPTensor2Vars& sol1, CPTensor2Vars& sol2) {

	assert(m_isInitialised && sol1_old.isAssembled() && sol2_old.isAssembled());

	// Matrices containing the parameter values on the diagonal.
	std::vector<Matrix> theta(parameter.dim()+2);
	for (unsigned int i=0; i<theta.size()-1; ++i) {
		theta[i].init(parameter.nOfPt(), parameter.nOfPt(), m_comm);
		if (i<parameter.dim()) {
			for (unsigned int j=0; j<parameter.nOfPt(); ++j) {
				theta[i].setMatEl(j, j, parameter(j, i));
			}
		} else {
			for (unsigned int j=0; j<parameter.nOfPt(); ++j) {
				theta[i].setMatEl(j, j, 1./parameter(j, 1));
			}
		}
		theta[i].finalize();
	}
	theta[theta.size()-1].init(eye(parameter.nOfPt(), parameter.nOfPt(), m_comm));

	CPTensor2Vars rhs({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, parameter.nOfPt(), {sol1_old.terms_x().nCols()*m_operatorB.nTerms(), 0, 0}, m_comm);
	rhs.finalize();

	m_operatorB.nDof_y_in() = parameter.nOfPt();
	m_operatorB.nDof_y_out() = parameter.nOfPt();
	m_operatorB(0, 1).init(theta[0]);
	m_operatorB(1, 1).init(theta[1]);
	m_operatorB(2, 1).init(theta[2]);
	m_operatorB(3, 1).init(theta[3]);
	m_operatorB(4, 1).init(theta[4]);
	m_operatorB(5, 1).init(theta[6]);
	m_operatorB.apply(sol1_old, rhs);

	m_operatorC.nDof_y_in() = parameter.nOfPt();
	m_operatorC.nDof_y_out() = parameter.nOfPt();
	m_operatorC(0, 1).init(theta[2]);
	rhs += m_operatorC.apply(sol2_old);

	if (t<=m_testcase.Tstar()) {
		m_tensD.terms_y().nRows() = parameter.nOfPt();
		m_tensD.terms_y(0).init(parameter.nOfPt(), 1.0, m_comm);
		rhs.add(m_tensD, m_testcase.Press()*(1.0-cos(2.0*t*M_PI/m_testcase.Tstar()))/2.0);
	}

	m_operatorA.nDof_y_in() = parameter.nOfPt();
	m_operatorA.nDof_y_out() = parameter.nOfPt();
	m_operatorA(0, 1).init(theta[0]);
	m_operatorA(1, 1).init(theta[1]);
	m_operatorA(2, 1).init(theta[2]);
	m_operatorA(3, 1).init(theta[3]);
	m_operatorA(4, 1).init(theta[4]);
	m_operatorA(5, 1).init(theta[5]);
	m_operatorA(6, 1).init(theta[6]);
	sol1 = sol1_old;
	unsigned int it = gmres(m_operatorA, sol1, rhs, m_testcase.tolr_gmres(), m_testcase.itmax_gmres(), m_P, m_testcase.tolc_gmres(), m_comm);

	sol2 = sol2_old;
	sol2 *= (-m_testcase.c());
	CPTensor2Vars sol1_d(sol1, 3*m_testcase.nFluid(), 3*m_testcase.nFluid()+2*m_testcase.nSolid());
	sol2.add(sol1_d, (1.0+m_testcase.c())/m_testcase.dt());
	CPTensor2Vars sol1_old_d(sol1_old, 3*m_testcase.nFluid(), 3*m_testcase.nFluid()+2*m_testcase.nSolid());
	sol2.add(sol1_old_d, -(1.0+m_testcase.c())/m_testcase.dt());
	sol2.truncate(m_testcase.tolc_gmres());

	PetscPrintf(m_comm, "Time: %f, number of iteration: %d, rank: %d %d %d %d\n", t, it, sol1.rank(0), sol1.rank(1), sol1.rank(2), sol2.rank(0));

}
