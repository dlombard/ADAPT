#ifndef InterpolatorStokes2D_hpp
#define InterpolatorStokes2D_hpp

// Including headers:
#include "../../../Applications/FSI/Vector.hpp"
#include "../../../Applications/FSI/VectorSet.hpp"
#include "../../../Applications/FSI/Matrix.hpp"
#include "../../../Applications/FSI/Operations.hpp"
#include "../../../Applications/FSI/CPTensor2Vars.hpp"
#include "../../../Applications/FSI/Testcase.hpp"
#include "../../../Applications/FSI/PointCloud.hpp"


class InterpolatorStokes2D {

private:

  bool m_isInitialised;
  Testcase m_testcase;
  std::vector<VectorSet> m_U;
  VectorSet m_sol1_terms_x;
  std::vector<double> m_sol1_coeffs;
  VectorSet m_sol2_terms_x;
  std::vector<double> m_sol2_coeffs;
  Matrix m_P;
  std::vector<Matrix> m_A;
  std::vector<Matrix> m_B;
  Matrix m_C;
  Vector m_d;
  Matrix m_E;
  std::vector<Matrix> m_AA;
  std::vector<Matrix> m_BB;
  Matrix m_CC;
  Vector m_dd;
  MPI_Comm m_comm;

  void load_data();
  void update_precomputed_quantities();

public:

  InterpolatorStokes2D();
  InterpolatorStokes2D(const Testcase& testcase, const MPI_Comm& comm=PETSC_COMM_WORLD);
  ~InterpolatorStokes2D();

  void init(const Testcase& testcase, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void update(const CPTensor2Vars& sol1, const CPTensor2Vars& sol2);
  void update(const CPTensor2Vars& sol1, const CPTensor2Vars& sol2, const VectorSet& sol1_terms_y, const VectorSet& sol2_terms_y);
  void solve(const PointCloud& parameter, const unsigned int nStep, VectorSet& sol1_terms_y, VectorSet& sol2_terms_y) const;
  void solve(const PointCloud& parameter, const unsigned int nStep, VectorSet& sol1_terms_y, VectorSet& sol2_terms_y, CPTensor2Vars& sol1, CPTensor2Vars& sol2) const;
  void clear();

  inline const VectorSet& terms1_x() const;
  inline const VectorSet& terms2_x() const;

};


/*
*  Inline methods
*/

inline const VectorSet& InterpolatorStokes2D::terms1_x() const {
  return m_sol1_terms_x;
};

inline const VectorSet& InterpolatorStokes2D::terms2_x() const {
  return m_sol2_terms_x;
};

#endif
