#ifndef SolverStokes2D_hpp
#define SolverStokes2D_hpp

// Including headers:
#include "../../../Applications/FSI/Vector.hpp"
#include "../../../Applications/FSI/VectorSet.hpp"
#include "../../../Applications/FSI/Matrix.hpp"
#include "../../../Applications/FSI/Operations.hpp"
#include "../../../Applications/FSI/CPTensor2Vars.hpp"
#include "../../../Applications/FSI/OperatorCPTensor2Vars.hpp"
#include "../../../Applications/FSI/Testcase.hpp"
#include "../../../Applications/FSI/PointCloud.hpp"
#include "../../../Applications/FSI/Stokes2D/InterpolatorStokes2D.hpp"


class SolverStokes2D {

private:

  Testcase m_testcase;
  MPI_Comm m_comm;
  OperatorCPTensor2Vars m_operatorA;
  OperatorCPTensor2Vars m_operatorB;
  OperatorCPTensor2Vars m_operatorC;
  CPTensor2Vars m_tensD;
  Matrix m_P;
  bool m_isInitialised;

  void load_data(const std::vector<double>& lbParameter, const std::vector<double>& ubParameter);

public:

  SolverStokes2D();
  SolverStokes2D(const Testcase& testcase, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm=PETSC_COMM_WORLD);
  ~SolverStokes2D();

  void init(const Testcase& testcase, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void solve(const PointCloud& parameter, const double t, const CPTensor2Vars& sol1_old, const CPTensor2Vars& sol2_old, CPTensor2Vars& sol1, CPTensor2Vars& sol2);
  void clear();

};

#endif
