#include "InterpolatorStokes2D.hpp"


/* 
*  Constructors/Destructors
*/

InterpolatorStokes2D::InterpolatorStokes2D() : m_isInitialised(false) {}

InterpolatorStokes2D::InterpolatorStokes2D(const Testcase& testcase, const MPI_Comm& comm) {
  this->init(testcase, comm);
}

InterpolatorStokes2D::~InterpolatorStokes2D() {
	this->clear();
}

void InterpolatorStokes2D::init(const Testcase& testcase, const MPI_Comm& comm) {
	m_isInitialised = false;
	m_testcase.init(testcase);
	m_comm = comm;
	this->load_data();
}


void InterpolatorStokes2D::clear() {

	m_testcase.clear();
	m_sol1_coeffs.clear();
	m_sol1_terms_x.clear();
	m_sol2_coeffs.clear();
	m_sol2_terms_x.clear();
	m_P.clear();
	m_C.clear();
	m_d.clear();
	m_E.clear();
	m_CC.clear();
	m_dd.clear();

	for (unsigned int i=0; i<m_U.size(); ++i) {
		m_U[i].clear();
	}
  m_U.clear();

	for (unsigned int i=0; i<m_A.size(); ++i) {
		m_A[i].clear();
	}
	m_A.clear();

	for (unsigned int i=0; i<m_B.size(); ++i) {
		m_B[i].clear();
	}
	m_B.clear();

	for (unsigned int i=0; i<m_AA.size(); ++i) {
		m_AA[i].clear();
	}
	m_AA.clear();

	for (unsigned int i=0; i<m_BB.size(); ++i) {
		m_BB[i].clear();
	}
	m_BB.clear();

  m_isInitialised = false;

}


/* 
*  Methods
*/

void InterpolatorStokes2D::load_data() {

	m_AA.resize(7);
	m_AA[0].init(m_testcase.folderpath_input_data() + "Ac.dat", m_comm);
  m_AA[1].init(m_testcase.folderpath_input_data() + "Arhof.dat", m_comm);
  m_AA[2].init(m_testcase.folderpath_input_data() + "Amu.dat", m_comm);
  m_AA[3].init(m_testcase.folderpath_input_data() + "Arhos.dat", m_comm);
  m_AA[4].init(m_testcase.folderpath_input_data() + "Alambda1.dat", m_comm);
  m_AA[5].init(m_testcase.folderpath_input_data() + "Alambda2.dat", m_comm);
  m_AA[6].init(m_testcase.folderpath_input_data() + "Ainvmu.dat", m_comm);

  m_BB.resize(6);
  m_BB[0].init(m_testcase.folderpath_input_data() + "Bc.dat", m_comm);
  m_BB[1].init(m_testcase.folderpath_input_data() + "Brhof.dat", m_comm);
  m_BB[2].init(m_testcase.folderpath_input_data() + "Bmu.dat", m_comm);
  m_BB[3].init(m_testcase.folderpath_input_data() + "Brhos.dat", m_comm);
  m_BB[4].init(m_testcase.folderpath_input_data() + "Blambda1.dat", m_comm);
  m_BB[5].init(m_testcase.folderpath_input_data() + "Blambda2.dat", m_comm);
  m_CC.init(m_testcase.folderpath_input_data() + "Brhos_aux.dat", m_comm);
  m_dd.init(m_testcase.folderpath_input_data() + "B1c.dat", m_comm);

  m_P.init(2*m_testcase.nSolid(), 3*m_testcase.nFluid()+2*m_testcase.nSolid(), m_comm);
  for (unsigned int i=0; i<m_P.nRows(); ++i) {
  	m_P.setMatEl(i, 3*m_testcase.nFluid()+i, 1.0);
  }
	m_P.finalize();

	m_U.resize(4);
	m_A.resize(m_AA.size());
	m_B.resize(m_BB.size());

}


void InterpolatorStokes2D::update_precomputed_quantities() {

	// Compute terms_x
	m_sol1_terms_x.init(m_U[0].nRows()+m_U[1].nRows()+m_U[2].nRows(), m_U[0].nCols()+m_U[1].nCols()+m_U[2].nCols(), 0.0, m_comm);
  for (unsigned int i=0; i<m_U[0].nCols(); ++i) {
    m_sol1_terms_x(i).add(m_U[0](i), 0, m_U[0].nRows());
  }
  for (unsigned int i=0; i<m_U[1].nCols(); ++i) {
    m_sol1_terms_x(m_U[0].nCols()+i).add(m_U[1](i), m_U[0].nRows(), m_U[0].nRows()+m_U[1].nRows());
  }
  for (unsigned int i=0; i<m_U[2].nCols(); ++i) {
    m_sol1_terms_x(m_U[0].nCols()+m_U[1].nCols()+i).add(m_U[2](i), m_U[0].nRows()+m_U[1].nRows(), m_U[0].nRows()+m_U[1].nRows()+m_U[2].nRows());
  }
  m_sol2_terms_x = m_U[3];

	// Compute coeffs
	m_sol1_coeffs.resize(m_sol1_terms_x.nCols(), 1.0);
	m_sol2_coeffs.resize(m_sol2_terms_x.nCols(), 1.0);

	// Compute matrices A
	for (unsigned int i=0; i<m_A.size(); ++i) {
		m_A[i].init(m_sol1_terms_x.nCols(), m_sol1_terms_x.nCols(), 0.0, m_comm);
		MatTransposeMatMult(m_sol1_terms_x, MatMatMult(m_AA[i], m_sol1_terms_x), m_A[i]);
	}

	// Compute matrices B
	for (unsigned int i=0; i<m_B.size(); ++i) {
		m_B[i].init(m_sol1_terms_x.nCols(), m_sol1_terms_x.nCols(), 0.0, m_comm);
		MatTransposeMatMult(m_sol1_terms_x, MatMatMult(m_BB[i], m_sol1_terms_x), m_B[i]);
	}

	// Compute matrix C
	m_C.init(m_sol1_terms_x.nCols(), m_sol2_terms_x.nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_sol1_terms_x, MatMatMult(m_CC, m_sol2_terms_x), m_C);

	// Compute vector d
	m_d.init(m_sol1_terms_x.nCols(), 0.0, m_comm);
	MatMultTranspose(m_sol1_terms_x, m_dd, m_d);
	
	// Compute matrix E
	m_E.init(m_sol2_terms_x.nCols(), m_sol1_terms_x.nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_sol2_terms_x, MatMatMult(m_P, m_sol1_terms_x), m_E);
	m_E *= (-(1.0+m_testcase.c())/m_testcase.dt());

	m_isInitialised = true;

	PetscPrintf(m_comm, "Dimension of the reduced basis: %d %d %d %d\n", m_U[0].nCols(), m_U[1].nCols(), m_U[2].nCols(), m_U[3].nCols());

}


void InterpolatorStokes2D::update(const CPTensor2Vars& sol1, const CPTensor2Vars& sol2) {

	unsigned int offset=0, start=0, end=0;
	for (unsigned int i=0; i<3; ++i) {
		unsigned int rank = 0;
		double error1 = m_testcase.tolc_interpol(), error2 = 1.0;
		while (rank<sol1.rank(i) && error1<error2) {
	    rank += 1;
	    error1 = 0.0;
	    for (unsigned int iTerm=0; iTerm<rank; ++iTerm) {
	      error1 += pow(m_testcase.tolc_interpol()*sol1.coeffs(offset+iTerm), 2);
	    }
	    error2 = 0.0;
	    for (unsigned int iTerm=rank; iTerm<sol1.rank(i); ++iTerm) {
	      error2 += (1.0-pow(m_testcase.tolc_interpol(), 2))*pow(sol1.coeffs(offset+iTerm), 2);
	    }
	  }

	  m_U[i].init(sol1.nDof_x(i), rank, 0.0, m_comm);
	  end += sol1.nDof_x(i);
	  for (unsigned int j=0; j<rank; ++j) {
	  	sol1.terms_x(offset+j).extract(start, end, m_U[i](j));
	  }
	  start = end;
	  offset += sol1.rank(i);
	}

	unsigned int rank = 0;
	double error1 = m_testcase.tolc_interpol(), error2 = 1.0;
	while (rank<sol2.rank(0) && error1<error2) {
    rank += 1;
    error1 = 0.0;
    for (unsigned int iTerm=0; iTerm<rank; ++iTerm) {
      error1 += pow(m_testcase.tolc_interpol()*sol2.coeffs(iTerm), 2);
    }
    error2 = 0.0;
    for (unsigned int iTerm=rank; iTerm<sol2.rank(0); ++iTerm) {
      error2 += (1.0-pow(m_testcase.tolc_interpol(), 2))*pow(sol2.coeffs(iTerm), 2);
    }
  }

  m_U[3].init(sol2.nDof_x(0), rank, m_comm);
  for (unsigned int j=0; j<rank; ++j) {
  	m_U[3](j) = sol2.terms_x(j);
  }

	this->update_precomputed_quantities();

}


void InterpolatorStokes2D::update(const CPTensor2Vars& sol1, const CPTensor2Vars& sol2, const VectorSet& sol1_terms_y, const VectorSet& sol2_terms_y) {

	unsigned int istart=0, iend=0, jstart=0, jend=0, kstart=0, kend=0;
	for (unsigned int i=0; i<3; ++i) {

		iend += sol1.nDof_x(i);
		jend += sol1.rank(i);
		kend += m_U[i].nCols();

		std::vector<double> Sk(sol1.rank(i));
		for (unsigned int i=0; i<Sk.size(); ++i) {
	    Sk[i] = sol1.coeffs(jstart+i);
	  }

		VectorSet Uk(sol1.nDof_x(i), sol1.rank(i), 0.0, m_comm);
		sol1.terms_x().extract(istart, iend, jstart, jend, Uk);

		VectorSet Y(m_U[i].nCols(), sol1_terms_y.nCols(), 0.0, m_comm);
		sol1_terms_y.extract(kstart, kend, Y);

		brand(m_U[i], Y, Uk, Sk, m_testcase.tolc_interpol(), m_comm);	

	  istart = iend;
	  jstart = jend;
	  kstart = kend;

	}

	brand(m_U[3], sol2_terms_y, sol2.terms_x(), sol2.coeffs(), m_testcase.tolc_interpol(), m_comm);

	this->update_precomputed_quantities();

}


void InterpolatorStokes2D::solve(const PointCloud& parameter, const unsigned int nStep, VectorSet& sol1_terms_y, VectorSet& sol2_terms_y) const {

	assert(m_isInitialised);

	sol1_terms_y.init(m_sol1_terms_x.nCols(), parameter.nOfPt()*(nStep+1), m_comm);
	sol2_terms_y.init(m_sol2_terms_x.nCols(), parameter.nOfPt()*(nStep+1), m_comm);

	for (unsigned int i=0; i<parameter.nOfPt(); ++i) {

		Matrix A(m_A[0]);
		A.add(m_A[1], parameter(i, 0));
		A.add(m_A[2], parameter(i, 1));
		A.add(m_A[3], parameter(i, 2));
		A.add(m_A[4], parameter(i, 3));
		A.add(m_A[5], parameter(i, 4));
		A.add(m_A[6], 1.0/parameter(i, 1));
		A.inv();

		Matrix B(m_B[0]);
		B.add(m_B[1], parameter(i, 0));
		B.add(m_B[2], parameter(i, 1));
		B.add(m_B[3], parameter(i, 2));
		B.add(m_B[4], parameter(i, 3));
		B.add(m_B[5], parameter(i, 4));

		Matrix C(m_C);
		C *= parameter(i, 2);

		Vector y1(m_sol1_terms_x.nCols(), 0.0, m_comm);
		Vector y2(m_sol2_terms_x.nCols(), 0.0, m_comm);
		Vector y1_old(m_sol1_terms_x.nCols(), 0.0, m_comm);
		Vector rhs(m_sol1_terms_x.nCols(), 0.0, m_comm);
		sol1_terms_y(i*(nStep+1)) = y1;
		sol2_terms_y(i*(nStep+1)) = y2;

	  double t = m_testcase.dt();
	  for (unsigned int k=0; k<nStep; ++k) {

			MatMult(B, y1, rhs);
			MatMultAdd(C, y2, rhs, rhs);
			if (t<=m_testcase.Tstar()) {
	  		rhs.add(m_d, m_testcase.Press()*(1.0-cos(2.0*t*M_PI/m_testcase.Tstar()))/2.0);
	  	}
	  	
			Solve(A, rhs, y1);
	  	
	  	y2 *= (-m_testcase.c());
	  	y1_old -= y1;
	  	MatMultAdd(m_E, y1_old, y2, y2);

	  	y1_old = y1;
	  	sol1_terms_y(k+1+i*(nStep+1)) = y1;
	  	sol2_terms_y(k+1+i*(nStep+1)) = y2;
	  	t += m_testcase.dt();

	  }

	}

	sol1_terms_y.finalize();
	sol2_terms_y.finalize();

}


void InterpolatorStokes2D::solve(const PointCloud& parameter, const unsigned int nStep, VectorSet& sol1_terms_y, VectorSet& sol2_terms_y, CPTensor2Vars& sol1, CPTensor2Vars& sol2) const {

	assert(m_isInitialised);

	sol1_terms_y.init(m_sol1_terms_x.nCols(), parameter.nOfPt()*(nStep+1), m_comm);
	sol2_terms_y.init(m_sol2_terms_x.nCols(), parameter.nOfPt()*(nStep+1), m_comm);

	sol1.init({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, parameter.nOfPt(), {m_U[0].nCols(), m_U[1].nCols(), m_U[2].nCols()}, m_comm);
	sol1.terms_x() = m_sol1_terms_x;
	sol1.coeffs() = m_sol1_coeffs;

	sol2.init({2*m_testcase.nSolid()}, parameter.nOfPt(), {m_U[3].nCols()}, m_comm);
	sol2.terms_x() = m_sol2_terms_x;
	sol2.coeffs() = m_sol2_coeffs;
	
	for (unsigned int i=0; i<parameter.nOfPt(); ++i) {

		Matrix A(m_A[0]);
		A.add(m_A[1], parameter(i, 0));
		A.add(m_A[2], parameter(i, 1));
		A.add(m_A[3], parameter(i, 2));
		A.add(m_A[4], parameter(i, 3));
		A.add(m_A[5], parameter(i, 4));
		A.add(m_A[6], 1.0/parameter(i, 1));
		A.inv();

		Matrix B(m_B[0]);
		B.add(m_B[1], parameter(i, 0));
		B.add(m_B[2], parameter(i, 1));
		B.add(m_B[3], parameter(i, 2));
		B.add(m_B[4], parameter(i, 3));
		B.add(m_B[5], parameter(i, 4));

		Matrix C(m_C);
		C *= parameter(i, 2);

		Vector y1(m_sol1_terms_x.nCols(), 0.0, m_comm);
		Vector y2(m_sol2_terms_x.nCols(), 0.0, m_comm);
		Vector y1_old(m_sol1_terms_x.nCols(), 0.0, m_comm);
		Vector rhs(m_sol1_terms_x.nCols(), 0.0, m_comm);
		sol1_terms_y(i*(nStep+1)) = y1;
		sol2_terms_y(i*(nStep+1)) = y2;

	  double t = m_testcase.dt();
	  for (unsigned int k=0; k<nStep; ++k) {

			MatMult(B, y1, rhs);
			MatMultAdd(C, y2, rhs, rhs);
			if (t<=m_testcase.Tstar()) {
	  		rhs.add(m_d, m_testcase.Press()*(1.0-cos(2.0*t*M_PI/m_testcase.Tstar()))/2.0);
	  	}
	  	
			Solve(A, rhs, y1);
	  	
	  	y2 *= (-m_testcase.c());
	  	y1_old -= y1;
	  	MatMultAdd(m_E, y1_old, y2, y2);

	  	y1_old = y1;
	  	sol1_terms_y(k+1+i*(nStep+1)) = y1;
	  	sol2_terms_y(k+1+i*(nStep+1)) = y2;
	  	t += m_testcase.dt();

	  }

	  for (unsigned int j=0; j<sol1.terms_y().nCols(); ++j) {
			sol1.terms_y().setMatEl(i, j, y1.getVecEl(j));
		}
		for (unsigned int j=0; j<sol2.terms_y().nCols(); ++j) {
			sol2.terms_y().setMatEl(i, j, y2.getVecEl(j));
		}

	}

	sol1_terms_y.finalize();
	sol2_terms_y.finalize();
	sol1.finalize();
	sol2.finalize();

}


// #include "InterpolatorStokes2D.hpp"


// /* 
// *  Constructors/Destructors
// */

// InterpolatorStokes2D::InterpolatorStokes2D() : m_isInitialised(-1) {}

// InterpolatorStokes2D::InterpolatorStokes2D(const std::string& filename, const MPI_Comm& comm) {
//   this->init(filename, comm);
// }

// InterpolatorStokes2D::InterpolatorStokes2D(const Testcase& testcase, const MPI_Comm& comm) {
//   this->init(testcase, comm);
// }

// InterpolatorStokes2D::~InterpolatorStokes2D() {
// 	this->clear();
// }


// void InterpolatorStokes2D::init(const std::string& filename, const MPI_Comm& comm) {
// 	m_testcase.init(filename);
// 	m_comm = comm;
// 	this->load_data();
// }


// void InterpolatorStokes2D::init(const Testcase& testcase, const MPI_Comm& comm) {
// 	m_testcase.init(testcase);
// 	m_comm = comm;
// 	this->load_data();
// }


// void InterpolatorStokes2D::clear() {

// 	for (unsigned int i=0; i<m_A.size(); ++i) {
// 		for (unsigned int j=0; j<m_A[i].size(); ++j) {
// 			m_A[i][j].clear();
// 		}
// 		m_A[i].clear();
// 	}
// 	m_A.clear();

// 	for (unsigned int i=0; i<m_B.size(); ++i) {
// 		for (unsigned int j=0; j<m_B[i].size(); ++j) {
// 			m_B[i][j].clear();
// 		}
// 		m_B[i].clear();
// 	}
// 	m_B.clear();

// 	for (unsigned int i=0; i<m_C.size(); ++i) {
// 		m_C[i].clear();
// 	}
//   m_C.clear();

//   for (unsigned int i=0; i<m_d.size(); ++i) {
// 		m_d[i].clear();
// 	}
//   m_d.clear();

//   for (unsigned int i=0; i<m_E.size(); ++i) {
// 		m_E[i].clear();
// 	}
// 	m_E.clear();

// 	for (unsigned int i=0; i<m_F.size(); ++i) {
// 		m_F[i].clear();
// 	}
// 	m_F.clear();

// 	for (unsigned int i=0; i<m_G.size(); ++i) {
// 		m_G[i].clear();
// 	}
// 	m_G.clear();

// 	for (unsigned int i=0; i<m_AA.size(); ++i) {
// 		m_AA[i].clear();
// 	}
// 	m_AA.clear();

// 	for (unsigned int i=0; i<m_BB.size(); ++i) {
// 		m_BB[i].clear();
// 	}
// 	m_BB.clear();

// 	m_CC.clear();

// 	m_dd.clear();

// 	m_H.clear();

// 	for (unsigned int i=0; i<m_rhs.size(); ++i) {
// 		m_rhs[i].clear();
// 	}
// 	m_rhs.clear();

// 	for (unsigned int i=0; i<m_y.size(); ++i) {
// 		m_y[i].clear();
// 	}
// 	m_y.clear();

// 	for (unsigned int i=0; i<m_y_aux.size(); ++i) {
// 		m_y_aux[i].clear();
// 	}
// 	m_y_aux.clear();

// 	m_sol_terms_x.clear();

//   m_sol_coeffs.clear();

// 	m_sol2_terms_x.clear();

//   m_sol2_coeffs.clear();

//   m_isInitialised = -1;

// }


// /* 
// *  Methods
// */

// void InterpolatorStokes2D::load_data() {

// 	m_AA.resize(7);
// 	m_AA[0].init(m_testcase.folderpath() + "input/Ac.dat", m_comm);
//   m_AA[1].init(m_testcase.folderpath() + "input/Arhof.dat", m_comm);
//   m_AA[2].init(m_testcase.folderpath() + "input/Amu.dat", m_comm);
//   m_AA[3].init(m_testcase.folderpath() + "input/Arhos.dat", m_comm);
//   m_AA[4].init(m_testcase.folderpath() + "input/Alambda1.dat", m_comm);
//   m_AA[5].init(m_testcase.folderpath() + "input/Alambda2.dat", m_comm);
//   m_AA[6].init(m_testcase.folderpath() + "input/Ainvmu.dat", m_comm);
//   m_BB.resize(6);
//   m_BB[0].init(m_testcase.folderpath() + "input/Bc.dat", m_comm);
//   m_BB[1].init(m_testcase.folderpath() + "input/Brhof.dat", m_comm);
//   m_BB[2].init(m_testcase.folderpath() + "input/Bmu.dat", m_comm);
//   m_BB[3].init(m_testcase.folderpath() + "input/Brhos.dat", m_comm);
//   m_BB[4].init(m_testcase.folderpath() + "input/Blambda1.dat", m_comm);
//   m_BB[5].init(m_testcase.folderpath() + "input/Blambda2.dat", m_comm);
//   m_CC.init(m_testcase.folderpath() + "input/Brhos_aux.dat", m_comm);
//   m_dd.init(m_testcase.folderpath() + "input/B1c.dat", m_comm);

//   m_A.resize(m_testcase.nStep());
//   m_B.resize(m_testcase.nStep());
//   m_C.resize(m_testcase.nStep());
//   m_d.resize(m_testcase.nStep());
//   m_E.resize(m_testcase.nStep());
//   m_F.resize(m_testcase.nStep());
//   m_G.resize(m_testcase.nStep());
//   m_rhs.resize(m_testcase.nStep());
//   m_y.resize(m_testcase.nStep()+1);
//   m_y_aux.resize(m_testcase.nStep()+1);

//   m_H.init(2*m_testcase.nSolid(), 3*m_testcase.nFluid()+2*m_testcase.nSolid(), m_comm);
//   for (unsigned int i=0; i<m_H.nRows(); ++i) {
//   	m_H.setMatEl(i, 3*m_testcase.nFluid()+i, 1.0);
//   }
// 	m_H.finalize();

// 	m_isInitialised = 0;
// }


// void InterpolatorStokes2D::update(const TriCPTensor2Vars& sol, const CPTensor2Vars& sol2, const unsigned int nStep) {

// 	assert(nStep==m_isInitialised);

// 	// Compute matrices A
// 	m_A[nStep].resize(m_AA.size());
// 	for (unsigned int i=0; i<m_AA.size(); ++i) {
// 		m_A[nStep][i].init(sol.terms_x().nCols(), sol.terms_x().nCols(), 0.0, m_comm);
// 		MatTransposeMatMult(sol.terms_x(), MatMatMult(m_AA[i], sol.terms_x()), m_A[nStep][i]);
// 	}
	
// 	// Compute matrix E
// 	m_E[nStep].init(sol2.terms_x().nCols(), sol.terms_x().nCols(), 0.0, m_comm);
// 	MatTransposeMatMult(sol2.terms_x(), MatMatMult(m_H, sol.terms_x()), m_E[nStep]);
// 	m_E[nStep] *= (1.0+m_testcase.c())/m_testcase.dt();

// 	// Compute vector d
// 	m_d[nStep].init(sol.terms_x().nCols(), 0.0, m_comm);
// 	MatMultTranspose(sol.terms_x(), m_dd, m_d[nStep]);

// 	if (nStep==0) {

// 		// Compute matrices B
// 		m_B[nStep].resize(m_BB.size());
// 		for (unsigned int i=0; i<m_BB.size(); ++i) {
// 			m_B[nStep][i].init(sol.terms_x().nCols(), sol.terms_x().nCols(), 0.0, m_comm);
// 			MatTransposeMatMult(sol.terms_x(), MatMatMult(m_BB[i], sol.terms_x()), m_B[nStep][i]);
// 		}

// 		// Compute matrix C
// 		m_C[nStep].init(sol.terms_x().nCols(), sol2.terms_x().nCols(), 0.0, m_comm);
// 		MatTransposeMatMult(sol.terms_x(), MatMatMult(m_CC, sol2.terms_x()), m_C[nStep]);

// 		// Compute matrix F
// 		m_F[nStep].init(sol2.terms_x().nCols(), sol.terms_x().nCols(), 0.0, m_comm);
// 		MatTransposeMatMult(sol2.terms_x(), MatMatMult(m_H, sol.terms_x()), m_F[nStep]);
// 		m_F[nStep] *= -(1.0+m_testcase.c())/m_testcase.dt();

// 		// Compute matrix G
// 		m_G[nStep].init(sol2.terms_x().nCols(), sol2.terms_x().nCols(), 0.0, m_comm);
// 		MatTransposeMatMult(sol2.terms_x(), sol2.terms_x(), m_G[nStep]);
// 		m_G[nStep] *= (-m_testcase.c());

// 	  m_y[nStep].init(sol.terms_x().nCols(), 0.0, m_comm);
// 		m_y_aux[nStep].init(sol2.terms_x().nCols(), 0.0, m_comm);

// 	} else {

// 		// Compute matrices B
// 		m_B[nStep].resize(m_BB.size());
// 		for (unsigned int i=0; i<m_BB.size(); ++i) {
// 			m_B[nStep][i].init(sol.terms_x().nCols(), m_sol_terms_x.nCols(), 0.0, m_comm);
// 			MatTransposeMatMult(sol.terms_x(), MatMatMult(m_BB[i], m_sol_terms_x), m_B[nStep][i]);
// 		}

// 		// Compute matrix C
// 		m_C[nStep].init(sol.terms_x().nCols(), m_sol2_terms_x.nCols(), 0.0, m_comm);
// 		MatTransposeMatMult(sol.terms_x(), MatMatMult(m_CC, m_sol2_terms_x), m_C[nStep]);

// 		// Compute matrix F
// 		m_F[nStep].init(sol2.terms_x().nCols(), m_sol_terms_x.nCols(), 0.0, m_comm);
// 		MatTransposeMatMult(sol2.terms_x(), MatMatMult(m_H, m_sol_terms_x), m_F[nStep]);
// 		m_F[nStep] *= -(1.0+m_testcase.c())/m_testcase.dt();

// 		// Compute matrix G
// 		m_G[nStep].init(sol2.terms_x().nCols(), m_sol2_terms_x.nCols(), 0.0, m_comm);
// 		MatTransposeMatMult(sol2.terms_x(), m_sol2_terms_x, m_G[nStep]);
// 		m_G[nStep] *= (-m_testcase.c());
// 	}
	
// 	// Copy terms_x of sol
// 	m_sol_terms_x = sol.terms_x();

// 	// Compute coeffs of sol
// 	m_sol_coeffs.resize(m_sol_terms_x.nCols());
// 	for (unsigned int i=0; i<m_sol_coeffs.size(); ++i) {
// 		m_sol_coeffs[i] = 1.0;
// 	}

// 	// Copy terms_x of sol2
// 	m_sol2_terms_x = sol2.terms_x();

// 	// Compute coeffs of sol2
// 	m_sol2_coeffs.resize(m_sol2_terms_x.nCols());
// 	for (unsigned int i=0; i<m_sol2_coeffs.size(); ++i) {
// 		m_sol2_coeffs[i] = 1.0;
// 	}

// 	// Initialize variables
// 	m_rhs[nStep].init(m_sol_terms_x.nCols(), 0.0, m_comm);
//   m_y[nStep+1].init(m_sol_terms_x.nCols(), 0.0, m_comm);
// 	m_y_aux[nStep+1].init(m_sol2_terms_x.nCols(), 0.0, m_comm);

// 	++m_isInitialised;

// }


// void InterpolatorStokes2D::solve(const PointCloud& parameter, const unsigned int nStep, TriCPTensor2Vars& sol, CPTensor2Vars& sol2) {

// 	assert(nStep<=m_isInitialised);

// 	VectorSet sol_terms_y(parameter.nOfPt(), m_sol_terms_x.nCols(), m_comm), sol2_terms_y(parameter.nOfPt(), m_sol2_terms_x.nCols(), m_comm);
// 	for (unsigned int i=0; i<parameter.nOfPt(); ++i) {

// 	  double t = m_testcase.dt();
// 	  for (unsigned int k=0; k<nStep; ++k) {

// 	  	Matrix B(m_B[k][0]);
// 			B.add(m_B[k][1], parameter(i, 0));
// 			B.add(m_B[k][2], parameter(i, 1));
// 			B.add(m_B[k][3], parameter(i, 2));
// 			B.add(m_B[k][4], parameter(i, 3));
// 			B.add(m_B[k][5], parameter(i, 4));
// 			MatMult(B, m_y[k], m_rhs[k]);
			
// 			Matrix C(m_C[k]);
// 			C *= parameter(i, 2);
// 			MatMultAdd(C, m_y_aux[k], m_rhs[k], m_rhs[k]);

// 			if (t<=m_testcase.Tstar()) {
// 	  		m_rhs[k].add(m_d[k], m_testcase.Press()*(1.0-cos(2.0*t*M_PI/m_testcase.Tstar()))/2.0);
// 	  	}
	  	
// 	  	Matrix A(m_A[k][0]);
// 			A.add(m_A[k][1], parameter(i, 0));
// 			A.add(m_A[k][2], parameter(i, 1));
// 			A.add(m_A[k][3], parameter(i, 2));
// 			A.add(m_A[k][4], parameter(i, 3));
// 			A.add(m_A[k][5], parameter(i, 4));
// 			A.add(m_A[k][6], 1.0/parameter(i, 1));
// 			A.inv();
// 			Solve(A, m_rhs[k], m_y[k+1]);
	  	
// 	  	MatMult(m_E[k], m_y[k+1], m_y_aux[k+1]);
// 	  	MatMultAdd(m_F[k], m_y[k], m_y_aux[k+1], m_y_aux[k+1]);
// 	  	MatMultAdd(m_G[k], m_y_aux[k], m_y_aux[k+1], m_y_aux[k+1]);
// 	  	t += m_testcase.dt();

// 	  }

// 	  for (unsigned int j=0; j<sol_terms_y.nCols(); ++j) {
// 			sol_terms_y.setMatEl(i, j, m_y[nStep].getVecEl(j));
// 		}
// 		for (unsigned int j=0; j<sol2_terms_y.nCols(); ++j) {
// 			sol2_terms_y.setMatEl(i, j, m_y_aux[nStep].getVecEl(j));
// 		}

// 	}

// 	sol_terms_y.finalize();
// 	sol2_terms_y.finalize();

// 	sol.init(2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid(), m_sol_coeffs, m_sol_terms_x, sol_terms_y, m_comm);
// 	sol2.init(m_sol2_coeffs, m_sol2_terms_x, sol2_terms_y, m_comm);

// }


