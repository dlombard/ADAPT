#include "MetropolisHastingsStokes3D.hpp"


/* 
*  Constructors/Destructors
*/

MetropolisHastingsStokes3D::MetropolisHastingsStokes3D() : m_isInitialised(false) {}

MetropolisHastingsStokes3D::MetropolisHastingsStokes3D(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm) : m_isInitialised(false) {
  assert(lbParameter.size()==ubParameter.size());
  this->init(filename, lbParameter, ubParameter, comm);
}

MetropolisHastingsStokes3D::~MetropolisHastingsStokes3D() {
	this->clear();
}

void MetropolisHastingsStokes3D::init(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm) {
	
	assert(lbParameter.size()==ubParameter.size());
	
	m_testcase.init(filename);
	m_comm = comm;

	m_parameter.init(lbParameter, ubParameter, m_testcase.nParameter());
	std::cout << m_parameter.nOfPt() << std::endl;
	this->init_observation();

  m_sol1.init({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, m_parameter.nOfPt(), {0, 0, 0}, m_comm);
  m_sol2.init({2*m_testcase.nSolid()}, m_parameter.nOfPt(), {0}, m_comm);
  m_interpol.init(m_testcase);
  m_SolverStokes3D.init(m_testcase);

  // unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  // m_generator = std::default_random_engine(seed);
  m_uniform_distribution = std::uniform_real_distribution<double>(0.0, 1.0);
  m_normal_distribution.resize(m_parameter.dim());
  for (unsigned int iComp=0; iComp<m_normal_distribution.size(); ++iComp) {
    m_normal_distribution[iComp] = std::normal_distribution<double>(0.0, m_testcase.step_mcmc()*(m_ubParameter[iComp]-m_lbParameter[iComp]));
  }
  
	m_isInitialised = true;
}


void MetropolisHastingsStokes3D::clear() {
	if (!m_isInitialised) {
		m_testcase.clear();
		m_parameter.clear();
		m_observation1.clear();
		m_observation2.clear();
		m_H1.clear();
		m_H2.clear();
		m_sol1.clear();
		m_sol2.clear();
		m_interpol.clear();
		m_SolverStokes3D.clear();
		m_normal_distribution.clear();
		m_lbParameter.clear();
		m_ubParameter.clear();
	  m_isInitialised = false;
	}
}


/* 
*  Methods
*/

void MetropolisHastingsStokes3D::init_observation() {
	
	m_H1.init(3*m_testcase.nFluid()+2*m_testcase.nSolid(), 3*m_testcase.nFluid()+2*m_testcase.nSolid(), m_comm);
	if (m_testcase.uObserved()) {
		for (unsigned int i=0; i<2*m_testcase.nFluid(); ++i) {
	    m_H1.setMatEl(i, i, 1.0);
	  }
	}
	if (m_testcase.pObserved()) {
		for (unsigned int i=2*m_testcase.nFluid(); i<3*m_testcase.nFluid(); ++i) {
	    m_H1.setMatEl(i, i, 1.0);
	  }
	}
	if (m_testcase.dObserved()) {
		for (unsigned int i=3*m_testcase.nFluid(); i<3*m_testcase.nFluid()+2*m_testcase.nSolid(); ++i) {
	    m_H1.setMatEl(i, i, 1.0);
	  }
	}
	m_H1.finalize();
  
  m_H2.init(2*m_testcase.nSolid(), 2*m_testcase.nSolid(), m_comm);
  if (m_testcase.vObserved()) {
		for (unsigned int i=0; i<2*m_testcase.nSolid(); ++i) {
	    m_H2.setMatEl(i, i, 1.0);
	  }
	}
  m_H2.finalize();

  VectorSet observation1(3*m_testcase.nFluid()+2*m_testcase.nSolid(), m_testcase.nStep(), PETSC_COMM_WORLD);
  for (unsigned int k=0; k<observation1.nCols(); ++k) {
    observation1(k).init(m_testcase.folderpath_input_observation() + "sol1_" + std::to_string(k+1) + ".dat", PETSC_COMM_WORLD);
  }
  observation1.finalize();
  m_observation1.init(m_H1.nRows(), observation1.nCols(), 0.0, m_comm);
	MatMatMult(m_H1, observation1, m_observation1);

  VectorSet observation2(2*m_testcase.nSolid(), m_testcase.nStep(), PETSC_COMM_WORLD);
  for (unsigned int k=0; k<observation1.nCols(); ++k) {
    observation2(k).init(m_testcase.folderpath_input_observation() + "sol2_" + std::to_string(k+1) + ".dat", PETSC_COMM_WORLD);
  }
  observation2.finalize();
	m_observation2.init(m_H2.nRows(), observation2.nCols(), 0.0, m_comm);
	MatMatMult(m_H2, observation2, m_observation2);

}


void MetropolisHastingsStokes3D::update_solution(const PointCloud& parameter, const unsigned int k, const bool update_InterpolatorStokes3D) {

	assert(m_isInitialised && k<m_testcase.nStep());

  if (update_InterpolatorStokes3D) {
  	VectorSet sol1_terms_y, sol2_terms_y;
		CPTensor2Vars sol1_old;
	  CPTensor2Vars sol2_old;
  	if (k==0) {
  		sol1_old.init({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, parameter.nOfPt(), {0, 0, 0}, m_comm);
    	sol2_old.init({2*m_testcase.nSolid()}, parameter.nOfPt(), {0}, m_comm);
  	} else {
  		m_interpol.solve(parameter, k, sol1_old, sol2_old, sol1_terms_y, sol2_terms_y);
  	}
  	m_SolverStokes3D.solve(parameter, (k+1)*m_testcase.dt(), sol1_old, sol2_old, m_sol1, m_sol2);
  	if (k==0) {
  		m_interpol.update(m_sol1, m_sol2);
  	} else {
  		m_interpol.update(m_sol1, m_sol2, sol1_terms_y, sol2_terms_y);
  	}
  } else {
  	m_interpol.solve(parameter, k+1, m_sol1, m_sol2);
  }
}


std::vector<double> MetropolisHastingsStokes3D::update_error(const unsigned int k) {
	assert(m_isInitialised);
	CPTensor2Vars observation1(m_H1, m_sol1);
	CPTensor2Vars observation2(m_H2, m_sol2);
	std::vector<double> error(m_parameter.nOfPt());
	for (unsigned int iPt=0; iPt<error.size(); ++iPt) {
		error[iPt] = pow(norm(m_observation1(k)-observation1.eval(iPt)), 2)+pow(norm(m_observation2(k)-observation2.eval(iPt)), 2);
  }
  return error;
}


void MetropolisHastingsStokes3D::save_solution(const PointCloud& parameter, const unsigned int k) const {
	assert(m_isInitialised);

	VectorSet sol1_terms_y, sol2_terms_y;
	CPTensor2Vars sol1, sol2;
	m_interpol.solve(parameter, k, sol1, sol2, sol1_terms_y, sol2_terms_y);

	Matrix terms1_x(sol1.terms_x());
	terms1_x.save(m_testcase.folderpath_output_sol() + "sol1_terms_x.dat");
	Matrix terms1_y(sol1_terms_y);
	terms1_y.save(m_testcase.folderpath_output_sol() + "sol1_terms_y.dat");
	Matrix terms2_x(sol2.terms_x());
	terms2_x.save(m_testcase.folderpath_output_sol() + "sol2_terms_x.dat");
	Matrix terms2_y(sol2_terms_y);
	terms2_y.save(m_testcase.folderpath_output_sol() + "sol2_terms_y.dat");
}


void MetropolisHastingsStokes3D::solve() {

	assert(m_isInitialised);

	m_parameter.save(m_testcase.folderpath_output_parameter() + "theta_0.dat");

	for (unsigned int k=0; k<m_testcase.nStep(); ++k) {
		this->update_solution(m_parameter, k, true);
		std::vector<double> parameter_error = this->update_error(k);

		for (unsigned int it=0; it<m_testcase.itmax_mcmc(); ++it) {
			PointCloud m_candidate(m_parameter.dim(), m_parameter.nOfPt());
			for (unsigned int iPt=0; iPt<m_candidate.nOfPt(); ++iPt) {
				for(unsigned int iComp=0; iComp<m_candidate.dim(); ++iComp) {
	        m_candidate(iPt, iComp) = std::max(m_lbParameter[iComp], std::min(m_ubParameter[iComp], m_parameter(iPt, iComp)+m_normal_distribution[iComp](m_generator)));
	      }
			}
			this->update_solution(m_candidate, k);
			std::vector<double> candidate_error = this->update_error(k);
			for (unsigned int iPt=0; iPt<parameter_error.size(); ++iPt) {
				double p = std::max(1.0e-16, m_uniform_distribution(m_generator));
				if (std::log(p)<=(parameter_error[iPt]-candidate_error[iPt])) {
					m_parameter(iPt) = m_candidate(iPt);
		    	parameter_error[iPt] = candidate_error[iPt];
		    }
		  }
		}
		m_parameter.save(m_testcase.folderpath_output_parameter() + "theta_" + std::to_string(k+1) + ".dat");
	}
	this->save_solution(m_parameter, m_testcase.nStep());
}
