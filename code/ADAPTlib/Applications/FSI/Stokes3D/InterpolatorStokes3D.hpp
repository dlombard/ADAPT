#ifndef InterpolatorStokes3D_hpp
#define InterpolatorStokes3D_hpp

// Including headers:
#include "../../../Applications/FSI/Vector.hpp"
#include "../../../Applications/FSI/VectorSet.hpp"
#include "../../../Applications/FSI/Matrix.hpp"
#include "../../../Applications/FSI/Operations.hpp"
#include "../../../Applications/FSI/CPTensor2Vars.hpp"
#include "../../../Applications/FSI/Testcase.hpp"
#include "../../../Applications/FSI/PointCloud.hpp"


class InterpolatorStokes3D {

private:

  bool m_isInitialised;
  std::vector<unsigned int> m_rank;
  Testcase m_testcase;
  std::vector<VectorSet> m_U;
  VectorSet m_sol1_terms_x;
  std::vector<double> m_sol1_coeffs;
  VectorSet m_sol2_terms_x;
  std::vector<double> m_sol2_coeffs;
  Matrix m_P;
  std::vector<Matrix> m_A;
  std::vector<Matrix> m_B;
  Matrix m_C;
  Vector m_d;
  Matrix m_E;
  std::vector<Matrix> m_AA;
  std::vector<Matrix> m_BB;
  Matrix m_CC;
  Vector m_dd;
  MPI_Comm m_comm;

  void load_data();
  void update_precomputed_quantities();

public:

  InterpolatorStokes3D();
  InterpolatorStokes3D(const Testcase& testcase, const MPI_Comm& comm=PETSC_COMM_WORLD);
  ~InterpolatorStokes3D();

  void init(const Testcase& testcase, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void update(const CPTensor2Vars& sol1, const CPTensor2Vars& sol2);
  void update(const CPTensor2Vars& sol1, const CPTensor2Vars& sol2, const VectorSet& sol1_terms_y, const VectorSet& sol2_terms_y);
  void solve(const PointCloud& parameter, const unsigned int nStep, CPTensor2Vars& sol1, CPTensor2Vars& sol2) const;
  void solve(const PointCloud& parameter, const unsigned int nStep, CPTensor2Vars& sol1, CPTensor2Vars& sol2, VectorSet& sol1_terms_y, VectorSet& sol2_terms_y) const;
  void clear();

};

void BrandStokes3D(VectorSet& U, const VectorSet& Y, const VectorSet& Uk, const std::vector<double>& Sk, const double tol, const MPI_Comm& comm);

#endif
