#ifndef MetropolisHastingsStokes3D_hpp
#define MetropolisHastingsStokes3D_hpp

// Including headers:
#include "../../../Applications/FSI/Vector.hpp"
#include "../../../Applications/FSI/VectorSet.hpp"
#include "../../../Applications/FSI/Matrix.hpp"
#include "../../../Applications/FSI/Operations.hpp"
#include "../../../Applications/FSI/CPTensor2Vars.hpp"
#include "../../../Applications/FSI/OperatorCPTensor2Vars.hpp"
#include "../../../Applications/FSI/Testcase.hpp"
#include "../../../Applications/FSI/PointCloud.hpp"
#include "../../../Applications/FSI/Stokes3D/InterpolatorStokes3D.hpp"
#include "../../../Applications/FSI/Stokes3D/SolverStokes3D.hpp"


class MetropolisHastingsStokes3D {

private:

  Testcase m_testcase;
  PointCloud m_parameter;
  MPI_Comm m_comm;
  VectorSet m_observation1;
  VectorSet m_observation2;
  Matrix m_H1;
  Matrix m_H2;
  CPTensor2Vars m_sol1;
  CPTensor2Vars m_sol2;
  InterpolatorStokes3D m_interpol;
  SolverStokes3D m_SolverStokes3D;
  std::default_random_engine m_generator;
  std::uniform_real_distribution<double> m_uniform_distribution;
  std::vector<std::normal_distribution<double>> m_normal_distribution;
  std::vector<double> m_lbParameter;
  std::vector<double> m_ubParameter;
  bool m_isInitialised;

  void init_observation();
  void update_solution(const PointCloud& candidate, const unsigned int k, const bool update_InterpolatorStokes3D=false);
  std::vector<double> update_error(const unsigned int k);
  void save_solution(const PointCloud& parameter, const unsigned int k) const;

public:

  MetropolisHastingsStokes3D();
  MetropolisHastingsStokes3D(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm=PETSC_COMM_WORLD);
  ~MetropolisHastingsStokes3D();

  void init(const std::string& filename, const std::vector<double>& lbParameter, const std::vector<double>& ubParameter, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void solve();
  void clear();

};

#endif
