#include "SolverStokes3D.hpp"


/* 
*  Constructors/Destructors
*/

SolverStokes3D::SolverStokes3D() : m_isInitialised(false) {}

SolverStokes3D::SolverStokes3D(const Testcase& testcase, const MPI_Comm& comm) : m_isInitialised(false) {
  this->init(testcase, comm);
}

SolverStokes3D::~SolverStokes3D() {
	this->clear();
}

void SolverStokes3D::init(const Testcase& testcase, const MPI_Comm& comm) {
	m_comm = comm;
	m_testcase.init(testcase);
  this->load_data();
	m_isInitialised = true;
}

void SolverStokes3D::clear() {
	if (m_isInitialised) {
		m_testcase.clear();
		m_operatorA.clear();
	  m_operatorB.clear();
	  m_operatorC.clear();
	  m_tensD.clear();
	  m_P.clear();
	  m_isInitialised = false;
	}
}


/* 
*  Methods
*/

void SolverStokes3D::load_data() {

	// Assemble m_operatorA
	m_operatorA.init({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, {2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, 0, 0, 7, m_comm);
	m_operatorA(0, 0).init(m_testcase.folderpath_input_data() + "Arhof.dat", m_comm);
	m_operatorA(1, 0).init(m_testcase.folderpath_input_data() + "Amu.dat", m_comm);
	m_operatorA(2, 0).init(m_testcase.folderpath_input_data() + "Arhos.dat", m_comm);
	m_operatorA(3, 0).init(m_testcase.folderpath_input_data() + "Alambda1.dat", m_comm);
	m_operatorA(4, 0).init(m_testcase.folderpath_input_data() + "Alambda2.dat", m_comm);
	m_operatorA(5, 0).init(m_testcase.folderpath_input_data() + "Ainvmu.dat", m_comm);
	m_operatorA(6, 0).init(m_testcase.folderpath_input_data() + "Ac.dat", m_comm);

	// Assemble m_operatorB
	m_operatorB.init({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, {2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, 0, 0, 6, m_comm);
	m_operatorB(0, 0).init(m_testcase.folderpath_input_data() + "Brhof.dat", m_comm);
	m_operatorB(1, 0).init(m_testcase.folderpath_input_data() + "Bmu.dat", m_comm);
	m_operatorB(2, 0).init(m_testcase.folderpath_input_data() + "Brhos.dat", m_comm);
	m_operatorB(3, 0).init(m_testcase.folderpath_input_data() + "Blambda1.dat", m_comm);
	m_operatorB(4, 0).init(m_testcase.folderpath_input_data() + "Blambda2.dat", m_comm);
	m_operatorB(5, 0).init(m_testcase.folderpath_input_data() + "Bc.dat", m_comm);

	// Assemble m_operatorC
	m_operatorC.init({2*m_testcase.nSolid()}, {3*m_testcase.nFluid()+2*m_testcase.nSolid()}, 0, 0, 1, m_comm);
	m_operatorC(0, 0).init(m_testcase.folderpath_input_data() + "Brhos_aux.dat", m_comm);

	// Assemble m_tensD
	m_tensD.init({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, 0, {1, 0, 0}, m_comm);
	m_tensD.coeffs(0) = 1.0;
	m_tensD.terms_x(0).init(m_testcase.folderpath_input_data() + "B1c.dat", m_comm);

	// Preconditioner matrix
	m_P.init(m_operatorA(6, 0));
	m_P.add(m_operatorA(0, 0), 1.0);
	m_P.add(m_operatorA(1, 0), 0.035);
	m_P.add(m_operatorA(2, 0), 1.1);
	m_P.add(m_operatorA(3, 0), 1.15e6);
	m_P.add(m_operatorA(4, 0), 1.7e6);
	m_P.add(m_operatorA(5, 0), 1.0/0.035);
	m_P.inv();

}


void SolverStokes3D::solve(const PointCloud& parameter, const double t, const CPTensor2Vars& sol1_old, const CPTensor2Vars& sol2_old, CPTensor2Vars& sol1, CPTensor2Vars& sol2) {

	assert(m_isInitialised && sol1_old.isAssembled() && sol2_old.isAssembled());

	// Matrices containing the parameter values on the diagonal.
	std::vector<Matrix> theta(parameter.dim()+2);
	for (unsigned int i=0; i<theta.size()-1; ++i) {
		theta[i].init(parameter.nOfPt(), parameter.nOfPt(), m_comm);
		if (i<parameter.dim()) {
			for (unsigned int j=0; j<parameter.nOfPt(); ++j) {
				theta[i].setMatEl(j, j, parameter(j, i));
			}
		} else {
			for (unsigned int j=0; j<parameter.nOfPt(); ++j) {
				theta[i].setMatEl(j, j, 1./parameter(j, 1));
			}
		}
		theta[i].finalize();
	}
	theta[theta.size()-1].init(eye(parameter.nOfPt(), parameter.nOfPt(), m_comm));

	CPTensor2Vars rhs({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, parameter.nOfPt(), {sol1_old.rank()*m_operatorB.nTerms(), 0, 0}, m_comm);
	rhs.finalize();

	m_operatorB.nDof_y_in() = parameter.nOfPt();
	m_operatorB.nDof_y_out() = parameter.nOfPt();
	m_operatorB(0, 1).init(theta[0]);
	m_operatorB(1, 1).init(theta[1]);
	m_operatorB(2, 1).init(theta[2]);
	m_operatorB(3, 1).init(theta[3]);
	m_operatorB(4, 1).init(theta[4]);
	m_operatorB(5, 1).init(theta[6]);
	m_operatorB.apply(sol1_old, rhs);

	m_operatorC.nDof_y_in() = parameter.nOfPt();
	m_operatorC.nDof_y_out() = parameter.nOfPt();
	m_operatorC(0, 1).init(theta[2]);
	rhs += m_operatorC.apply(sol2_old);

	if (t<=m_testcase.Tstar()) {
		m_tensD.terms_y().nRows() = parameter.nOfPt();
		m_tensD.terms_y(0).init(parameter.nOfPt(), 1.0, m_comm);
		rhs.add(m_tensD, m_testcase.Press()*(1.0-cos(2.0*t*M_PI/m_testcase.Tstar()))/2.0);
	}

	m_operatorA.nDof_y_in() = parameter.nOfPt();
	m_operatorA.nDof_y_out() = parameter.nOfPt();
	m_operatorA(0, 1).init(theta[0]);
	m_operatorA(1, 1).init(theta[1]);
	m_operatorA(2, 1).init(theta[2]);
	m_operatorA(3, 1).init(theta[3]);
	m_operatorA(4, 1).init(theta[4]);
	m_operatorA(5, 1).init(theta[5]);
	m_operatorA(6, 1).init(theta[6]);
	sol1 = sol1_old;
	double resnorm = gmresStokes3D(m_operatorA, sol1, rhs, m_testcase.tolr_gmres(), m_testcase.itmax_gmres(), m_P, m_testcase.tolc_gmres(), m_comm);

	sol2 = sol2_old;
	sol2 *= (-m_testcase.c());
	CPTensor2Vars sol1_d(sol1, 3*m_testcase.nFluid(), 3*m_testcase.nFluid()+2*m_testcase.nSolid());
	sol2.add(sol1_d, (1.0+m_testcase.c())/m_testcase.dt());
	CPTensor2Vars sol1_old_d(sol1_old, 3*m_testcase.nFluid(), 3*m_testcase.nFluid()+2*m_testcase.nSolid());
	sol2.add(sol1_old_d, -(1.0+m_testcase.c())/m_testcase.dt());
	sol2.truncate(m_testcase.tolc_gmres());

	std::cout << t << ": " << sol1.rank(0) << ", " << sol1.rank(1) << ", " << sol1.rank(2) << ", " << sol2.rank() << ", " << resnorm << std::endl;

}


/*
*  Non-member function
*/

double gmresStokes3D(const OperatorCPTensor2Vars& operatorA, CPTensor2Vars& sol, const CPTensor2Vars& rhs, const double tolr, const unsigned int itmax, const Matrix& P, const double tolc, const MPI_Comm& comm) {

  CPTensor2Vars res(rhs);
  res -= operatorA.apply(sol);
  res.truncate(tolc);
  res.solve(P);

  double resnorm = norm(res), tolb = tolr*resnorm, tmp;
  if (fabs(resnorm)==0.0) {
  	return resnorm;
  }
  std::vector<double> sn(itmax), cs(itmax), e1(itmax+1, 0.0), H((itmax+1)*itmax, 0.0), beta(itmax+1, 0.0);
  e1[0] = 1.0;
  beta[0] = resnorm;
  std::vector<CPTensor2Vars> Q(itmax+1);
  Q[0].init(res);
  Q[0] /= resnorm;
  
  unsigned int it = 0;
  while (it<itmax && resnorm>tolb) {

    // Arnoldi iteration
    Q[it+1].init({Q[it].nDof_x(0), Q[it].nDof_x(1), Q[it].nDof_x(2)}, Q[it].nDof_y(), {Q[it].rank()*operatorA.nTerms(), 0, 0}, comm);
    Q[it+1].finalize();
    operatorA.apply(Q[it], Q[it+1]);
    Q[it+1].truncate(tolc);
    Q[it+1].solve(P);
    for (unsigned int subit=0; subit<=it; ++subit) {
    	H[it*(itmax+1)+subit] = dot(Q[subit], Q[it+1]);
    	if (fabs(H[it*(itmax+1)+subit])!=0.0) {
	      Q[it+1].add(Q[subit], -H[it*(itmax+1)+subit]);
	      Q[it+1].truncate(tolc);
	    }
    }
    H[it*(itmax+1)+it+1] = norm(Q[it+1]);
    if (fabs(H[it*(itmax+1)+it+1])==0.0) {
    	std::cout << "Warning gmres stops prematurely: " << tolb << "<" << resnorm << std::endl;
    	break;
    }
    Q[it+1] /= H[it*(itmax+1)+it+1];
    
    // Givens rotation
    for (unsigned int subit=0; subit<it; ++subit) {
    	tmp = sn[subit]*H[it*(itmax+1)+subit+1]+cs[subit]*H[it*(itmax+1)+subit];
      H[it*(itmax+1)+subit+1] = cs[subit]*H[it*(itmax+1)+subit+1]-sn[subit]*H[it*(itmax+1)+subit];
      H[it*(itmax+1)+subit] = tmp;
    }
    tmp = sqrt(H[it*(itmax+1)+it]*H[it*(itmax+1)+it]+H[it*(itmax+1)+it+1]*H[it*(itmax+1)+it+1]);
    cs[it] = H[it*(itmax+1)+it]/tmp;
    sn[it] = H[it*(itmax+1)+it+1]/tmp;
    H[it*(itmax+1)+it] = sn[it]*H[it*(itmax+1)+it+1]+cs[it]*H[it*(itmax+1)+it];
    H[it*(itmax+1)+it+1] = 0.0;
    
    // Residual
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    resnorm = fabs(beta[it+1]);
    ++it;
  }
  if (it==itmax) {
  	std::cout << "Warning gmres does not converge: " << tolb << "<" << resnorm << std::endl;
  }

  // Solve triangular system
  std::vector<double> y(it);
  for (unsigned int i=it-1; i<it; --i) {
    y[i] = beta[i];
    for (unsigned int j=it-1; i<j; --j) {
      y[i] -= H[j*(itmax+1)+i]*y[j];
    }
    y[i] /= H[i*(itmax+1)+i];
  }

  // Result
  for (unsigned int subit=0; subit<it; ++subit) {
    sol.add(Q[subit], y[subit]);
  }
  sol.truncate(tolc);

  return resnorm;
}

