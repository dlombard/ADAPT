#include "InterpolatorStokes3D.hpp"


/* 
*  Constructors/Destructors
*/

InterpolatorStokes3D::InterpolatorStokes3D() : m_isInitialised(false) {}

InterpolatorStokes3D::InterpolatorStokes3D(const Testcase& testcase, const MPI_Comm& comm) {
  this->init(testcase, comm);
}

InterpolatorStokes3D::~InterpolatorStokes3D() {
	this->clear();
}

void InterpolatorStokes3D::init(const Testcase& testcase, const MPI_Comm& comm) {
	m_isInitialised = false;
	m_testcase.init(testcase);
	m_comm = comm;
	this->load_data();
}


void InterpolatorStokes3D::clear() {

	m_rank.clear();
	m_testcase.clear();
	m_sol1_coeffs.clear();
	m_sol1_terms_x.clear();
	m_sol2_coeffs.clear();
	m_sol2_terms_x.clear();
	m_P.clear();
	m_C.clear();
	m_d.clear();
	m_E.clear();
	m_CC.clear();
	m_dd.clear();

	for (unsigned int i=0; i<m_U.size(); ++i) {
		m_U[i].clear();
	}
  m_U.clear();

	for (unsigned int i=0; i<m_A.size(); ++i) {
		m_A[i].clear();
	}
	m_A.clear();

	for (unsigned int i=0; i<m_B.size(); ++i) {
		m_B[i].clear();
	}
	m_B.clear();

	for (unsigned int i=0; i<m_AA.size(); ++i) {
		m_AA[i].clear();
	}
	m_AA.clear();

	for (unsigned int i=0; i<m_BB.size(); ++i) {
		m_BB[i].clear();
	}
	m_BB.clear();

  m_isInitialised = false;

}


/* 
*  Methods
*/

void InterpolatorStokes3D::load_data() {

	m_AA.resize(7);
	m_AA[0].init(m_testcase.folderpath_input_data() + "Ac.dat", m_comm);
  m_AA[1].init(m_testcase.folderpath_input_data() + "Arhof.dat", m_comm);
  m_AA[2].init(m_testcase.folderpath_input_data() + "Amu.dat", m_comm);
  m_AA[3].init(m_testcase.folderpath_input_data() + "Arhos.dat", m_comm);
  m_AA[4].init(m_testcase.folderpath_input_data() + "Alambda1.dat", m_comm);
  m_AA[5].init(m_testcase.folderpath_input_data() + "Alambda2.dat", m_comm);
  m_AA[6].init(m_testcase.folderpath_input_data() + "Ainvmu.dat", m_comm);

  m_BB.resize(6);
  m_BB[0].init(m_testcase.folderpath_input_data() + "Bc.dat", m_comm);
  m_BB[1].init(m_testcase.folderpath_input_data() + "Brhof.dat", m_comm);
  m_BB[2].init(m_testcase.folderpath_input_data() + "Bmu.dat", m_comm);
  m_BB[3].init(m_testcase.folderpath_input_data() + "Brhos.dat", m_comm);
  m_BB[4].init(m_testcase.folderpath_input_data() + "Blambda1.dat", m_comm);
  m_BB[5].init(m_testcase.folderpath_input_data() + "Blambda2.dat", m_comm);
  m_CC.init(m_testcase.folderpath_input_data() + "Brhos_aux.dat", m_comm);
  m_dd.init(m_testcase.folderpath_input_data() + "B1c.dat", m_comm);

  m_P.init(2*m_testcase.nSolid(), 3*m_testcase.nFluid()+2*m_testcase.nSolid(), m_comm);
  for (unsigned int i=0; i<m_P.nRows(); ++i) {
  	m_P.setMatEl(i, 3*m_testcase.nFluid()+i, 1.0);
  }
	m_P.finalize();

	m_rank.resize(3, 0);
	m_U.resize(4);
	m_A.resize(m_AA.size());
	m_B.resize(m_BB.size());

}


void InterpolatorStokes3D::update(const CPTensor2Vars& sol1, const CPTensor2Vars& sol2) {

	m_U[0].init(sol1.nDof_x(0), sol1.rank(0), 0.0, m_comm);
	sol1.terms_x().extract(0, sol1.nDof_x(0), 0, sol1.rank(0), m_U[0]);

	m_U[1].init(sol1.nDof_x(1), sol1.rank(1), 0.0, m_comm);
	sol1.terms_x().extract(sol1.nDof_x(0), sol1.nDof_x(0)+sol1.nDof_x(1), sol1.rank(0), sol1.rank(0)+sol1.rank(1), m_U[1]);

	m_U[2].init(sol1.nDof_x(2), sol1.rank(2), 0.0, m_comm);
	sol1.terms_x().extract(sol1.nDof_x(0)+sol1.nDof_x(1), sol1.nDof_x(), sol1.rank(0)+sol1.rank(1), sol1.rank(), m_U[2]);

	m_U[3] = sol2.terms_x();

	this->update_precomputed_quantities();

}


void InterpolatorStokes3D::update(const CPTensor2Vars& sol1, const CPTensor2Vars& sol2, const VectorSet& sol1_terms_y, const VectorSet& sol2_terms_y) {

	std::vector<double> Sk;
	VectorSet Uk, Y;

	Sk.resize(sol1.rank(0));
	for (unsigned int i=0; i<Sk.size(); ++i) {
    Sk[i] = sol1.coeffs(i);
  }
	Uk.init(sol1.nDof_x(0), sol1.rank(0), 0.0, m_comm);
	sol1.terms_x().extract(0, sol1.nDof_x(0), 0, sol1.rank(0), Uk);
	Y.init(m_rank[0], sol1_terms_y.nCols(), 0.0, m_comm);
	sol1_terms_y.extract(0, m_rank[0], Y);
	BrandStokes3D(m_U[0], Y, Uk, Sk, m_testcase.tolc_interpol(), m_comm);

	Sk.resize(sol1.rank(1));
	for (unsigned int i=0; i<Sk.size(); ++i) {
    Sk[i] = sol1.coeffs(sol1.rank(0)+i);
  }
	Uk.init(sol1.nDof_x(1), sol1.rank(1), 0.0, m_comm);
	sol1.terms_x().extract(sol1.nDof_x(0), sol1.nDof_x(0)+sol1.nDof_x(1), sol1.rank(0), sol1.rank(0)+sol1.rank(1), Uk);
	Y.init(m_rank[1], sol1_terms_y.nCols(), 0.0, m_comm);
	sol1_terms_y.extract(m_rank[0], m_rank[0]+m_rank[1], Y);
	BrandStokes3D(m_U[1], Y, Uk, Sk, m_testcase.tolc_interpol(), m_comm);

	Sk.resize(sol1.rank(2));
	for (unsigned int i=0; i<Sk.size(); ++i) {
    Sk[i] = sol1.coeffs(sol1.rank(0)+sol1.rank(1)+i);
  }
	Uk.init(sol1.nDof_x(2), sol1.rank(2), 0.0, m_comm);
	sol1.terms_x().extract(sol1.nDof_x(0)+sol1.nDof_x(1), sol1.nDof_x(), sol1.rank(0)+sol1.rank(1), sol1.rank(), Uk);
	Y.init(m_rank[2], sol1_terms_y.nCols(), 0.0, m_comm);
	sol1_terms_y.extract(m_rank[0]+m_rank[1], m_rank[0]+m_rank[1]+m_rank[2], Y);
	BrandStokes3D(m_U[2], Y, Uk, Sk, m_testcase.tolc_interpol(), m_comm);

	BrandStokes3D(m_U[3], sol2_terms_y, sol2.terms_x(), sol2.coeffs(), m_testcase.tolc_interpol(), m_comm);

	this->update_precomputed_quantities();

}


void InterpolatorStokes3D::update_precomputed_quantities() {

	m_rank[0] = m_U[0].nCols();
	m_rank[1] = m_U[1].nCols();
	m_rank[2] = m_U[2].nCols();

	// Compute terms_x
	m_sol1_terms_x.init(m_U[0].nRows()+m_U[1].nRows()+m_U[2].nRows(), m_U[0].nCols()+m_U[1].nCols()+m_U[2].nCols(), 0.0, m_comm);
  for (unsigned int i=0; i<m_U[0].nCols(); ++i) {
    m_sol1_terms_x(i).add(m_U[0](i), 0, m_U[0].nRows());
  }
  for (unsigned int i=0; i<m_U[1].nCols(); ++i) {
    m_sol1_terms_x(m_U[0].nCols()+i).add(m_U[1](i), m_U[0].nRows(), m_U[0].nRows()+m_U[1].nRows());
  }
  for (unsigned int i=0; i<m_U[2].nCols(); ++i) {
    m_sol1_terms_x(m_U[0].nCols()+m_U[1].nCols()+i).add(m_U[2](i), m_U[0].nRows()+m_U[1].nRows(), m_U[0].nRows()+m_U[1].nRows()+m_U[2].nRows());
  }
  m_sol2_terms_x = m_U[3];

	// Compute coeffs
	m_sol1_coeffs.resize(m_sol1_terms_x.nCols(), 1.0);
	m_sol2_coeffs.resize(m_sol2_terms_x.nCols(), 1.0);

	// Compute matrices A
	for (unsigned int i=0; i<m_A.size(); ++i) {
		m_A[i].init(m_sol1_terms_x.nCols(), m_sol1_terms_x.nCols(), 0.0, m_comm);
		MatTransposeMatMult(m_sol1_terms_x, MatMatMult(m_AA[i], m_sol1_terms_x), m_A[i]);
	}

	// Compute matrices B
	for (unsigned int i=0; i<m_B.size(); ++i) {
		m_B[i].init(m_sol1_terms_x.nCols(), m_sol1_terms_x.nCols(), 0.0, m_comm);
		MatTransposeMatMult(m_sol1_terms_x, MatMatMult(m_BB[i], m_sol1_terms_x), m_B[i]);
	}

	// Compute matrix C
	m_C.init(m_sol1_terms_x.nCols(), m_sol2_terms_x.nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_sol1_terms_x, MatMatMult(m_CC, m_sol2_terms_x), m_C);

	// Compute vector d
	m_d.init(m_sol1_terms_x.nCols(), 0.0, m_comm);
	MatMultTranspose(m_sol1_terms_x, m_dd, m_d);
	
	// Compute matrix E
	m_E.init(m_sol2_terms_x.nCols(), m_sol1_terms_x.nCols(), 0.0, m_comm);
	MatTransposeMatMult(m_sol2_terms_x, MatMatMult(m_P, m_sol1_terms_x), m_E);
	m_E *= (-(1.0+m_testcase.c())/m_testcase.dt());

	m_isInitialised = true;

	std::cout << m_U[0].nCols() << " " << m_U[1].nCols() << " " << m_U[2].nCols() << " " << m_U[3].nCols() << std::endl;

}


void InterpolatorStokes3D::solve(const PointCloud& parameter, const unsigned int nStep, CPTensor2Vars& sol1, CPTensor2Vars& sol2) const {

	assert(m_isInitialised);

	sol1.init({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, parameter.nOfPt(), m_rank, m_comm);
	sol1.terms_x() = m_sol1_terms_x;
	sol1.coeffs() = m_sol1_coeffs;

	sol2.init({2*m_testcase.nSolid()}, parameter.nOfPt(), {m_sol2_terms_x.nCols()}, m_comm);
	sol2.terms_x() = m_sol2_terms_x;
	sol2.coeffs() = m_sol2_coeffs;

	for (unsigned int i=0; i<parameter.nOfPt(); ++i) {

		Vector y1(m_sol1_terms_x.nCols(), 0.0, m_comm);
		Vector y2(m_sol2_terms_x.nCols(), 0.0, m_comm);
		Vector y1_old(m_sol1_terms_x.nCols(), 0.0, m_comm);
		Vector rhs(m_sol1_terms_x.nCols(), 0.0, m_comm);

		Matrix A(m_A[0]);
		A.add(m_A[1], parameter(i, 0));
		A.add(m_A[2], parameter(i, 1));
		A.add(m_A[3], parameter(i, 2));
		A.add(m_A[4], parameter(i, 3));
		A.add(m_A[5], parameter(i, 4));
		A.add(m_A[6], 1.0/parameter(i, 1));
		A.inv();

		Matrix B(m_B[0]);
		B.add(m_B[1], parameter(i, 0));
		B.add(m_B[2], parameter(i, 1));
		B.add(m_B[3], parameter(i, 2));
		B.add(m_B[4], parameter(i, 3));
		B.add(m_B[5], parameter(i, 4));

		Matrix C(m_C);
		C *= parameter(i, 2);

	  double t = m_testcase.dt();
	  for (unsigned int k=0; k<nStep; ++k) {

			MatMult(B, y1, rhs);
			MatMultAdd(C, y2, rhs, rhs);
			if (t<=m_testcase.Tstar()) {
	  		rhs.add(m_d, m_testcase.Press()*(1.0-cos(2.0*t*M_PI/m_testcase.Tstar()))/2.0);
	  	}
	  	
			Solve(A, rhs, y1);
	  	
	  	y2 *= (-m_testcase.c());
	  	y1_old -= y1;
	  	MatMultAdd(m_E, y1_old, y2, y2);

	  	y1_old = y1;
	  	t += m_testcase.dt();

	  }

	  for (unsigned int j=0; j<sol1.terms_y().nCols(); ++j) {
			sol1.terms_y().setMatEl(i, j, y1.getVecEl(j));
		}
		for (unsigned int j=0; j<sol2.terms_y().nCols(); ++j) {
			sol2.terms_y().setMatEl(i, j, y2.getVecEl(j));
		}

	}

	sol1.finalize();
	sol2.finalize();

}


void InterpolatorStokes3D::solve(const PointCloud& parameter, const unsigned int nStep, CPTensor2Vars& sol1, CPTensor2Vars& sol2, VectorSet& sol1_terms_y, VectorSet& sol2_terms_y) const {

	assert(m_isInitialised);

	sol1.init({2*m_testcase.nFluid(), m_testcase.nFluid(), 2*m_testcase.nSolid()}, parameter.nOfPt(), m_rank, m_comm);
	sol1.terms_x() = m_sol1_terms_x;
	sol1.coeffs() = m_sol1_coeffs;

	sol2.init({2*m_testcase.nSolid()}, parameter.nOfPt(), {m_sol2_terms_x.nCols()}, m_comm);
	sol2.terms_x() = m_sol2_terms_x;
	sol2.coeffs() = m_sol2_coeffs;

	sol1_terms_y.init(m_sol1_terms_x.nCols(), parameter.nOfPt()*nStep, m_comm);
	sol2_terms_y.init(m_sol2_terms_x.nCols(), parameter.nOfPt()*nStep, m_comm);
	
	for (unsigned int i=0; i<parameter.nOfPt(); ++i) {

		Vector y1(m_sol1_terms_x.nCols(), 0.0, m_comm);
		Vector y2(m_sol2_terms_x.nCols(), 0.0, m_comm);
		Vector y1_old(m_sol1_terms_x.nCols(), 0.0, m_comm);
		Vector rhs(m_sol1_terms_x.nCols(), 0.0, m_comm);

		Matrix A(m_A[0]);
		A.add(m_A[1], parameter(i, 0));
		A.add(m_A[2], parameter(i, 1));
		A.add(m_A[3], parameter(i, 2));
		A.add(m_A[4], parameter(i, 3));
		A.add(m_A[5], parameter(i, 4));
		A.add(m_A[6], 1.0/parameter(i, 1));
		A.inv();

		Matrix B(m_B[0]);
		B.add(m_B[1], parameter(i, 0));
		B.add(m_B[2], parameter(i, 1));
		B.add(m_B[3], parameter(i, 2));
		B.add(m_B[4], parameter(i, 3));
		B.add(m_B[5], parameter(i, 4));

		Matrix C(m_C);
		C *= parameter(i, 2);

	  double t = m_testcase.dt();
	  for (unsigned int k=0; k<nStep; ++k) {

			MatMult(B, y1, rhs);
			MatMultAdd(C, y2, rhs, rhs);
			if (t<=m_testcase.Tstar()) {
	  		rhs.add(m_d, m_testcase.Press()*(1.0-cos(2.0*t*M_PI/m_testcase.Tstar()))/2.0);
	  	}
	  	
			Solve(A, rhs, y1);
	  	
	  	y2 *= (-m_testcase.c());
	  	y1_old -= y1;
	  	MatMultAdd(m_E, y1_old, y2, y2);

	  	y1_old = y1;
	  	sol1_terms_y(k+i*nStep) = y1;
	  	sol2_terms_y(k+i*nStep) = y2;
	  	t += m_testcase.dt();

	  }

	  for (unsigned int j=0; j<sol1.terms_y().nCols(); ++j) {
			sol1.terms_y().setMatEl(i, j, y1.getVecEl(j));
		}
		for (unsigned int j=0; j<sol2.terms_y().nCols(); ++j) {
			sol2.terms_y().setMatEl(i, j, y2.getVecEl(j));
		}

	}

	sol1.finalize();
	sol2.finalize();
	sol1_terms_y.finalize();
	sol2_terms_y.finalize();

}


/*
*  Non-member function
*/

void BrandStokes3D(VectorSet& U, const VectorSet& Y, const VectorSet& Uk, const std::vector<double>& Sk, const double tol, const MPI_Comm& comm) {

	VectorSet UktU(Uk.nCols(), U.nCols(), 0.0, comm);
	MatTransposeMatMult(Uk, U, UktU);
	VectorSet A(U);
	A -= MatMatMult(Uk, UktU);

	VectorSet Q;
	Matrix R;
	qr(A, Q, R);

	Matrix K(UktU.nRows()+R.nRows(), UktU.nRows()+R.nRows(), comm);
	Matrix UktUmat(UktU);
	Matrix Ymat(Y);
	Matrix YYt(Ymat.nRows(), Ymat.nRows(), 0.0, comm);
	MatMatTransposeMult(Ymat, Ymat, YYt);
	Matrix RYYt(R.nRows(), Y.nRows(), 0.0, comm);
	MatMatMult(R, YYt, RYYt);
	
	Matrix subK(UktUmat.nRows(), UktUmat.nRows(), comm);
	for (unsigned int i=0; i<subK.nRows(); ++i) {
		subK.setMatEl(i, i, Sk[i]*Sk[i]);
	}
	subK.finalize();
	subK += MatMatMult(UktUmat, MatMatTransposeMult(YYt, UktUmat));
	for (unsigned int i=0; i<subK.nRows(); ++i) {
		for (unsigned int j=0; j<subK.nCols(); ++j) {
			K.setMatEl(i, j, subK.getMatEl(i, j));
		}
	}

	subK.init(R.nRows(), UktUmat.nRows(), 0.0, comm);
	MatMatTransposeMult(RYYt, UktUmat, subK);
	for (unsigned int i=0; i<subK.nRows(); ++i) {
		for (unsigned int j=0; j<subK.nCols(); ++j) {
			K.setMatEl(i+UktUmat.nRows(), j, subK.getMatEl(i, j));
		}
	}
	for (unsigned int j=0; j<subK.nRows(); ++j) {
		for (unsigned int i=0; i<subK.nCols(); ++i) {
			K.setMatEl(i, j+UktUmat.nRows(), subK.getMatEl(j, i));
		}
	}

	subK.init(R.nRows(), R.nRows(), 0.0, comm);
	MatMatTransposeMult(RYYt, R, subK);
	for (unsigned int i=0; i<subK.nRows(); ++i) {
		for (unsigned int j=0; j<subK.nCols(); ++j) {
			K.setMatEl(i+UktUmat.nRows(), j+UktUmat.nRows(), subK.getMatEl(i, j));
		}
	}

	K.finalize();

	std::vector<double> SS;
	VectorSet UU, VV;
	svd(K, UU, SS, VV);

	unsigned int rank = 0;
  double error1 = tol, error2 = 1.0;
  while (rank<SS.size() && error1<error2) {
    ++rank;
    error1 = 0.0;
    for (unsigned int iTerm=0; iTerm<rank; ++iTerm) {
      error1 += tol*tol*SS[iTerm];
    }
    error2 = 0.0;
    for (unsigned int iTerm=rank; iTerm<SS.size(); ++iTerm) {
      error2 += (1.0-tol*tol)*SS[iTerm];
    }
  }

  VectorSet UUU(Uk);
  UUU.push_back(Q);
  UU.resize(rank);
  U.resize(rank);
  MatMatMult(UUU, UU, U);

}
