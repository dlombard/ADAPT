#ifndef SolverStokes3D_hpp
#define SolverStokes3D_hpp

// Including headers:
#include "../../../Applications/FSI/Vector.hpp"
#include "../../../Applications/FSI/VectorSet.hpp"
#include "../../../Applications/FSI/Matrix.hpp"
#include "../../../Applications/FSI/Operations.hpp"
#include "../../../Applications/FSI/CPTensor2Vars.hpp"
#include "../../../Applications/FSI/OperatorCPTensor2Vars.hpp"
#include "../../../Applications/FSI/Testcase.hpp"
#include "../../../Applications/FSI/PointCloud.hpp"
#include "../../../Applications/FSI/Stokes3D/InterpolatorStokes3D.hpp"


class SolverStokes3D {

private:

  Testcase m_testcase;
  MPI_Comm m_comm;
  OperatorCPTensor2Vars m_operatorA;
  OperatorCPTensor2Vars m_operatorB;
  OperatorCPTensor2Vars m_operatorC;
  CPTensor2Vars m_tensD;
  Matrix m_P;
  bool m_isInitialised;

  void load_data();

public:

  SolverStokes3D();
  SolverStokes3D(const Testcase& testcase, const MPI_Comm& comm=PETSC_COMM_WORLD);
  ~SolverStokes3D();

  void init(const Testcase& testcase, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void solve(const PointCloud& parameter, const double t, const CPTensor2Vars& sol1_old, const CPTensor2Vars& sol2_old, CPTensor2Vars& sol1, CPTensor2Vars& sol2);
  void clear();

};

double gmresStokes3D(const OperatorCPTensor2Vars& operatorA, CPTensor2Vars& sol1, const CPTensor2Vars& rhs, const double tolr, const unsigned int itmax, const Matrix& P, const double tolc, const MPI_Comm& comm);

#endif
