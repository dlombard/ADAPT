#include "OperatorCPTensor2Vars.hpp"


/* 
*  Constructors/Destructors
*/

OperatorCPTensor2Vars::OperatorCPTensor2Vars() : m_isInitialised(false) {}

OperatorCPTensor2Vars::OperatorCPTensor2Vars(const std::vector<unsigned int>& nDof_x_in, const std::vector<unsigned int>& nDof_x_out, const unsigned int nDof_y_in, const unsigned int nDof_y_out, const unsigned int nTerms, const MPI_Comm& comm) : m_isInitialised(false) {
  this->init(nDof_x_in, nDof_x_out, nDof_y_in, nDof_y_out, nTerms, comm);
}

OperatorCPTensor2Vars::~OperatorCPTensor2Vars() {
  this->clear();
}

void OperatorCPTensor2Vars::init(const std::vector<unsigned int>& nDof_x_in, const std::vector<unsigned int>& nDof_x_out, const unsigned int nDof_y_in, const unsigned int nDof_y_out, const unsigned int nTerms, const MPI_Comm& comm) {
  m_nDof_x_in = nDof_x_in;
  m_nDof_x_out = nDof_x_out;
  m_nDof_y_in = nDof_y_in;
  m_nDof_y_out = nDof_y_out;
  m_nTerms = nTerms;
  m_nVar = 2;
  m_data.resize(m_nTerms);
  for (unsigned int iTerm=0; iTerm<m_nTerms; ++iTerm){
    m_data[iTerm].resize(m_nVar);
  }
  m_isInitialised = true;
}

void OperatorCPTensor2Vars::clear() {
  if (m_isInitialised) {
    m_nDof_x_in.clear();
    m_nDof_x_out.clear();
    for (unsigned int iTerm=0; iTerm<m_data.size(); ++iTerm) {
      for (unsigned int iVar=0; iVar<m_data[iTerm].size(); ++iVar) {
        m_data[iTerm][iVar].clear();
      }
      m_data[iTerm].clear();
    }
    m_data.clear();
    m_isInitialised = false;
  }
}


/* 
*  Methods
*/

void OperatorCPTensor2Vars::finalize() {
  assert(m_isInitialised);
  for (unsigned int iTerm=0; iTerm<m_nTerms; ++iTerm) {
    for (unsigned int iVar=0; iVar<m_nVar; ++iVar) {
      m_data[iTerm][iVar].finalize();
    }
  }
}

void OperatorCPTensor2Vars::apply(const CPTensor2Vars& input, CPTensor2Vars& output) const {

  assert(this->isAssembled() && input.isAssembled() && output.isAssembled() && input.terms_x().nRows()==sum(m_nDof_x_in) && input.terms_y().nRows()==m_nDof_y_in && output.terms_x().nRows()==sum(m_nDof_x_out) && output.terms_y().nRows()==m_nDof_y_out && output.coeffs().size()==(input.coeffs().size()*m_nTerms));

  for(unsigned int jTerm=0, lTerm=0; jTerm<m_nTerms; ++jTerm) {
    for(unsigned int iTerm=0; iTerm<input.coeffs().size(); ++iTerm) {
      output.coeffs(lTerm) = input.coeffs(iTerm);
      ++lTerm;
    }
  }

  for (unsigned int jTerm=0, lTerm=0; jTerm<m_nTerms; ++jTerm) {
    for (unsigned int iTerm=0; iTerm<input.terms_x().nCols(); ++iTerm) {
      MatMult(m_data[jTerm][0], input.terms_x(iTerm), output.terms_x(lTerm));
      ++lTerm;
    }
  }
  
   for(unsigned int jTerm=0, lTerm=0; jTerm<m_nTerms; ++jTerm) {
    for(unsigned int iTerm=0; iTerm<input.terms_y().nCols(); ++iTerm) {
      MatMult(m_data[jTerm][1], input.terms_y(iTerm), output.terms_y(lTerm));
      ++lTerm;
    }
  }

}

CPTensor2Vars OperatorCPTensor2Vars::apply(const CPTensor2Vars& input) const {
  assert(this->isAssembled() && input.isAssembled() && input.terms_x().nRows()==sum(m_nDof_x_in) && input.terms_y().nRows()==m_nDof_y_in);
  std::vector<unsigned int> rank_out(m_nDof_x_out.size(), 0);
  rank_out[0] = input.coeffs().size()*m_nTerms;
  CPTensor2Vars output(m_nDof_x_out, m_nDof_y_out, rank_out, input.comm());
  output.finalize();
  this->apply(input, output);
  return output;
}


/*
*  Non-member function
*/

unsigned int gmres(const OperatorCPTensor2Vars& operatorA, CPTensor2Vars& X, const CPTensor2Vars& B, const double tolr, const unsigned int itmax, const Matrix& P, const double tolc, const MPI_Comm& comm) {

  CPTensor2Vars X0(X), R(B);
  R -= operatorA.apply(X);
  R.truncate(tolc);
  double resnorm = norm(R);
  R.solve(P);

  double tol = sqrt(1.0-pow(tolc, 2))*tolr*norm(B), tmp;
  std::vector<double> sn(itmax), cs(itmax), H((itmax+1)*itmax, 0.0), beta(itmax+1, 0.0);
  beta[0] = norm(R);
  std::vector<CPTensor2Vars> Q(itmax+1);
  Q[0].init(R);
  Q[0] /= beta[0];
  
  unsigned int it = 0;
  while (it<itmax && resnorm>tol) {

    // Arnoldi iteration
    std::vector<unsigned int> rank(Q[it].rank().size(), 0);
    rank[0] = Q[it].coeffs().size()*operatorA.nTerms();
    Q[it+1].init(Q[it].nDof_x(), Q[it].terms_y().nRows(), rank, comm); 
    Q[it+1].finalize();
    operatorA.apply(Q[it], Q[it+1]);
    Q[it+1].truncate(tolc);
    Q[it+1].solve(P);
    for (unsigned int subit=0; subit<=it; ++subit) {
      H[it*(itmax+1)+subit] = dot(Q[subit], Q[it+1]);
      if (fabs(H[it*(itmax+1)+subit])!=0.0) {
        Q[it+1].add(Q[subit], -H[it*(itmax+1)+subit]);
      }
    }
    Q[it+1].truncate(tolc);
    H[it*(itmax+1)+it+1] = norm(Q[it+1]);
    if (fabs(H[it*(itmax+1)+it+1])==0.0) {
      std::cout << "Warning gmres stops prematurely: " << resnorm << ">" << tol << std::endl;
      break;
    }
    Q[it+1] /= H[it*(itmax+1)+it+1];
    
    // Givens rotation
    for (unsigned int subit=0; subit<it; ++subit) {
      tmp = sn[subit]*H[it*(itmax+1)+subit+1]+cs[subit]*H[it*(itmax+1)+subit];
      H[it*(itmax+1)+subit+1] = cs[subit]*H[it*(itmax+1)+subit+1]-sn[subit]*H[it*(itmax+1)+subit];
      H[it*(itmax+1)+subit] = tmp;
    }
    tmp = sqrt(H[it*(itmax+1)+it]*H[it*(itmax+1)+it]+H[it*(itmax+1)+it+1]*H[it*(itmax+1)+it+1]);
    cs[it] = H[it*(itmax+1)+it]/tmp;
    sn[it] = H[it*(itmax+1)+it+1]/tmp;
    H[it*(itmax+1)+it] = sn[it]*H[it*(itmax+1)+it+1]+cs[it]*H[it*(itmax+1)+it];
    H[it*(itmax+1)+it+1] = 0.0;
    
    // Residual
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    ++it;
    std::vector<double> y(it);
    for (unsigned int i=it-1; i<it; --i) {
      y[i] = beta[i];
      for (unsigned int j=it-1; i<j; --j) {
        y[i] -= H[j*(itmax+1)+i]*y[j];
      }
      y[i] /= H[i*(itmax+1)+i];
    }
    X = X0;
    for (unsigned int subit=0; subit<it; ++subit) {
      X.add(Q[subit], y[subit]);
    }
    X.truncate(tolc);
    R = B;
    R -= operatorA.apply(X);
    R.truncate(tolc);
    resnorm = norm(R);    
  }

  if (it==itmax) {
    std::cout << "Warning gmres does not converge: " << resnorm << ">" << tol << std::endl;
  }

  return it;
}



// /* 
// *  Constructors/Destructors
// */

// OperatorCPTensor2Vars::OperatorCPTensor2Vars() : m_nDof_x_u_in(0), m_nDof_x_u_out(0), m_nDof_x_p_in(0), m_nDof_x_p_out(0), m_nDof_x_d_in(0), m_nDof_x_d_out(0), m_nDof_x_in(0), m_nDof_x_out(0), m_nDof_y_in(0), m_nDof_y_out(0), m_nTerms(0), m_nVar(0), m_comm(PETSC_COMM_WORLD), m_isInitialised(false) {}

// OperatorCPTensor2Vars::OperatorCPTensor2Vars(const unsigned int nDof_x_in, const unsigned int nDof_x_out, const unsigned int nDof_y_in, const unsigned int nDof_y_out, const unsigned int nTerms, const MPI_Comm& comm) : m_isInitialised(false) {
//   this->init(nDof_x_in, nDof_x_out, nDof_y_in, nDof_y_out, nTerms, comm);
// }

// OperatorCPTensor2Vars::OperatorCPTensor2Vars(const unsigned int nDof_x_u_in, const unsigned int nDof_x_u_out, const unsigned int nDof_x_p_in, const unsigned int nDof_x_p_out, const unsigned int nDof_x_d_in, const unsigned int nDof_x_d_out, const unsigned int nDof_y_in, const unsigned int nDof_y_out, const unsigned int nTerms, const MPI_Comm& comm) : m_isInitialised(false) {
//   this->init(nDof_x_u_in, nDof_x_u_out, nDof_x_p_in, nDof_x_p_out, nDof_x_d_in, nDof_x_d_out, nDof_y_in, nDof_y_out, nTerms, comm);
// }

// OperatorCPTensor2Vars::~OperatorCPTensor2Vars() {
//   this->clear();
// }

// void OperatorCPTensor2Vars::init(const unsigned int nDof_x_in, const unsigned int nDof_x_out, const unsigned int nDof_y_in, const unsigned int nDof_y_out, const unsigned int nTerms, const MPI_Comm& comm) {
//   m_nDof_x_u_in = nDof_x_in;
//   m_nDof_x_d_in = 0;
//   m_nDof_x_p_in = 0;
//   m_nDof_x_in = nDof_x_in;
//   m_nDof_x_u_out = nDof_x_out;
//   m_nDof_x_p_out = 0;
//   m_nDof_x_d_out = 0;
//   m_nDof_x_out = nDof_x_out;
//   m_nDof_y_in = nDof_y_in;
//   m_nDof_y_out = nDof_y_out;
//   m_nTerms = nTerms;
//   m_nVar = 2;
//   m_comm = comm;
//   m_data.resize(m_nTerms);
//   for (unsigned int iTerm=0; iTerm<m_nTerms; ++iTerm){
//     m_data[iTerm].resize(m_nVar);
//   }
//   m_isInitialised = true;
// }

// void OperatorCPTensor2Vars::init(const unsigned int nDof_x_u_in, const unsigned int nDof_x_u_out, const unsigned int nDof_x_p_in, const unsigned int nDof_x_p_out, const unsigned int nDof_x_d_in, const unsigned int nDof_x_d_out, const unsigned int nDof_y_in, const unsigned int nDof_y_out, const unsigned int nTerms, const MPI_Comm& comm) {
//   m_nDof_x_u_in = nDof_x_u_in;
//   m_nDof_x_d_in = nDof_x_d_in;
//   m_nDof_x_p_in = nDof_x_p_in;
//   m_nDof_x_in = (nDof_x_u_in+nDof_x_p_in+nDof_x_d_in);
//   m_nDof_x_u_out = nDof_x_u_out;
//   m_nDof_x_d_out = nDof_x_d_out;
//   m_nDof_x_p_out = nDof_x_p_out;
//   m_nDof_x_out = (nDof_x_u_out+nDof_x_p_out+nDof_x_d_out);
//   m_nDof_y_in = nDof_y_in;
//   m_nDof_y_out = nDof_y_out;
//   m_nTerms = nTerms;
//   m_nVar = 2;
//   m_comm = comm;
//   m_data.resize(m_nTerms);
//   for (unsigned int iTerm=0; iTerm<m_nTerms; ++iTerm){
//     m_data[iTerm].resize(m_nVar);
//   }
//   m_isInitialised = true;
// }

// void OperatorCPTensor2Vars::clear() {
//   if (m_isInitialised) {
//     m_nDof_x_u_in = 0;
//     m_nDof_x_u_out = 0;
//     m_nDof_x_p_in = 0;
//     m_nDof_x_p_out = 0;
//     m_nDof_x_d_in = 0;
//     m_nDof_x_d_out = 0;
//     m_nDof_x_in = 0;
//     m_nDof_x_out = 0;
//     m_nDof_y_in = 0;
//     m_nDof_y_out = 0;
//     m_nTerms = 0;
//     m_nVar = 0;
//     for (unsigned int iTerm=0; iTerm<m_data.size(); ++iTerm) {
//       for (unsigned int iVar=0; iVar<m_data[iTerm].size(); ++iVar) {
//         m_data[iTerm][iVar].clear();
//       }
//       m_data[iTerm].clear();
//     }
//     m_data.clear();
//     m_isInitialised = false;
//   }
// }


// /* 
// *  Methods
// */

// void OperatorCPTensor2Vars::finalize() {
//   assert(m_isInitialised);
//   for (unsigned int iTerm=0; iTerm<m_nTerms; ++iTerm) {
//     for (unsigned int iVar=0; iVar<m_nVar; ++iVar) {
//       m_data[iTerm][iVar].finalize();
//     }
//   }
// }

// void OperatorCPTensor2Vars::apply(const CPTensor2Vars& input, CPTensor2Vars& output) const {

//   assert(this->isAssembled() && input.isAssembled() && output.isAssembled() && input.nDof_x()==m_nDof_x_in && input.nDof_y()==m_nDof_y_in && output.nDof_x()==m_nDof_x_out && output.nDof_y()==m_nDof_y_out && output.rank()==(input.rank()*m_nTerms));

//   // if (!output.isInitialised() || output.nDof_x()!=m_nDof_x || output.nDof_y()!=m_nDof_y || output.rank()!=(input.rank()*m_nTerms)) {
//   //   output.init(m_nDof_x, m_nDof_y, input.rank()*m_nTerms, input.comm());
//   // }

//   for(unsigned int jTerm=0, lTerm=0; jTerm<m_nTerms; ++jTerm) {
//     for(unsigned int iTerm=0; iTerm<input.rank(); ++iTerm) {
//       output.coeffs(lTerm) = input.coeffs(iTerm);
//       ++lTerm;
//     }
//   }

//   for(unsigned int jTerm=0, lTerm=0; jTerm<m_nTerms; ++jTerm) {
//     for(unsigned int iTerm=0; iTerm<input.rank(); ++iTerm) {
//       MatMult(m_data[jTerm][0], input.terms_x(iTerm), output.terms_x(lTerm));
//       ++lTerm;
//     }
//   }
  
//    for(unsigned int jTerm=0, lTerm=0; jTerm<m_nTerms; ++jTerm) {
//     for(unsigned int iTerm=0; iTerm<input.rank(); ++iTerm) {
//       MatMult(m_data[jTerm][1], input.terms_y(iTerm), output.terms_y(lTerm));
//       ++lTerm;
//     }
//   }

// }


// CPTensor2Vars OperatorCPTensor2Vars::apply(const CPTensor2Vars& input) const {
//   assert(this->isAssembled() && input.isAssembled() && input.nDof_x()==m_nDof_x_in && input.nDof_y()==m_nDof_y_in);
//   CPTensor2Vars output(m_nDof_x_out, m_nDof_y_out, input.rank()*m_nTerms, input.comm());
//   output.finalize();
//   this->apply(input, output);
//   return output;
// }


// void OperatorCPTensor2Vars::apply(const TriCPTensor2Vars& input, TriCPTensor2Vars& output) const {

//   assert(this->isAssembled() && input.isAssembled() && output.isAssembled() && input.nDof_x_u()==m_nDof_x_u_in && input.nDof_x_p()==m_nDof_x_p_in && input.nDof_x_d()==m_nDof_x_d_in && input.nDof_y()==m_nDof_y_in && output.nDof_x_u()==m_nDof_x_u_out && output.nDof_x_p()==m_nDof_x_p_out && output.nDof_x_d()==m_nDof_x_d_out && output.nDof_y()==m_nDof_y_out && output.rank()==(input.rank()*m_nTerms));

//   // if (!output.isInitialised() || output.nDof_x_u()!=input.nDof_x_u() || output.nDof_x_p()!=input.nDof_x_p() || output.nDof_x_d()!=input.nDof_x_d() || output.nDof_y()!=m_nDof_y || output.rank()!=(input.rank()*m_nTerms)) {
//   //   output.init(input.nDof_x_u(), input.nDof_x_p(), input.nDof_x_d(), m_nDof_y, input.rank()*m_nTerms, input.comm());
//   // }

//   for(unsigned int jTerm=0, lTerm=0; jTerm<m_nTerms; ++jTerm) {
//     for(unsigned int iTerm=0; iTerm<input.rank(); ++iTerm) {
//       output.coeffs(lTerm) = input.coeffs(iTerm);
//       ++lTerm;
//     }
//   }

//   for(unsigned int jTerm=0, lTerm=0; jTerm<m_nTerms; ++jTerm) {
//     for(unsigned int iTerm=0; iTerm<input.rank(); ++iTerm) {
//       MatMult(m_data[jTerm][0], input.terms_x(iTerm), output.terms_x(lTerm));
//       ++lTerm;
//     }
//   }
  
//    for(unsigned int jTerm=0, lTerm=0; jTerm<m_nTerms; ++jTerm) {
//     for(unsigned int iTerm=0; iTerm<input.rank(); ++iTerm) {
//       MatMult(m_data[jTerm][1], input.terms_y(iTerm), output.terms_y(lTerm));
//       ++lTerm;
//     }
//   }

// }


// TriCPTensor2Vars OperatorCPTensor2Vars::apply(const TriCPTensor2Vars& input) const {
//   assert(this->isAssembled() && input.isAssembled() && input.nDof_x_u()==m_nDof_x_u_in && input.nDof_x_p()==m_nDof_x_p_in && input.nDof_x_d()==m_nDof_x_d_in && input.nDof_y()==m_nDof_y_in);
//   TriCPTensor2Vars output(m_nDof_x_u_out, m_nDof_x_p_out, m_nDof_x_d_out, m_nDof_y_out, input.rank()*m_nTerms, input.comm());
//   output.finalize();
//   this->apply(input, output);
//   return output;
// }

