#include "Matrix.hpp"


/* 
*  Constructors/Destructors
*/

Matrix::Matrix() : m_data(nullptr), m_nRows(0), m_nCols(0), m_comm(PETSC_COMM_WORLD), m_isInitialised(false), m_isAssembled(false), m_isInversed(false) {}

Matrix::Matrix(const unsigned int nRows, const unsigned int nCols, const MPI_Comm& comm) : m_isInitialised(false), m_isAssembled(false), m_isInversed(false) {
  this->init(nRows, nCols, comm);
}

Matrix::Matrix(const unsigned int nRows, const unsigned int nCols, const double value, const MPI_Comm& comm) : m_isInitialised(false), m_isAssembled(false), m_isInversed(false) {
  this->init(nRows, nCols, value, comm);
}

Matrix::Matrix(const unsigned int nRows, const unsigned int nCols, const std::vector<unsigned int>& ind_I, const std::vector<unsigned int>& ind_J, const std::vector<double>& value, const MPI_Comm& comm) : m_isInitialised(false), m_isAssembled(false), m_isInversed(false) {
  assert((ind_I.size()==0 || max(ind_I)<nRows) && (ind_J.size()==0 || max(ind_J)<nCols) && ind_I.size()==ind_J.size() && ind_J.size()==value.size());
  this->init(nRows, nCols, ind_I, ind_J, value, comm);
}

Matrix::Matrix(const std::vector<double>& value, const MPI_Comm& comm) : m_isInitialised(false), m_isAssembled(false), m_isInversed(false) {
  this->init(value, comm);
}

Matrix::Matrix(const VectorSet& M) : m_isInitialised(false), m_isAssembled(false), m_isInversed(false) {
  assert(M.isAssembled());
  this->init(M);
}

Matrix::Matrix(const Matrix& M, const IS& isrow, const IS& iscol) : m_isInitialised(false), m_isAssembled(false), m_isInversed(false) {
  assert(M.m_isAssembled);
  this->init(M, isrow, iscol);
}

Matrix::Matrix(const Matrix& M) : m_isInitialised(false), m_isAssembled(false), m_isInversed(false) {
  assert(M.m_isAssembled);
  this->init(M);
}

Matrix::Matrix(const std::string& filename, const MPI_Comm& comm) : m_isInitialised(false), m_isAssembled(false), m_isInversed(false) {
  this->init(filename, comm);
}

Matrix::~Matrix() {
  this->clear();
}

void Matrix::init(const unsigned int nRows, const unsigned int nCols, const MPI_Comm& comm) {
  this->clear();
  m_nRows = nRows;
  m_nCols = nCols;
  m_comm = comm;
  MatCreate(m_comm, &m_data);
  MatSetSizes(m_data, PETSC_DECIDE, PETSC_DECIDE, m_nRows, m_nCols);
  MatSetFromOptions(m_data);
  MatSetUp(m_data);
  m_isInitialised = true;
  m_isAssembled = false;
  m_isInversed = false;
}

void Matrix::init(const unsigned int nRows, const unsigned int nCols, const double value, const MPI_Comm& comm) {
  this->init(nRows, nCols, comm);
  if (fabs(value)!=0.0) {
    for (unsigned int i=0; i<m_nRows; ++i) {
      for (unsigned int j=0; j<m_nCols; ++j) {
        this->setMatEl(i, j, value);
      }
    }
  }
  this->finalize();
  m_isInversed = false;
}

void Matrix::init(const unsigned int nRows, const unsigned int nCols, const std::vector<unsigned int>& ind_I, const std::vector<unsigned int>& ind_J, const std::vector<double>& value, const MPI_Comm& comm) {
  assert((ind_I.size()==0 || max(ind_I)<nRows) && (ind_J.size()==0 || max(ind_J)<nCols) && ind_I.size()==ind_J.size() && ind_J.size()==value.size());
  this->init(nRows, nCols, comm);
  for (unsigned int i=0; i<ind_I.size(); ++i) {
    this->setMatEl(ind_I[i], ind_J[i], value[i]);
  }
  this->finalize();
  m_isInversed = false;
}

void Matrix::init(const std::vector<double>& value, const MPI_Comm& comm) {
  this->init(value.size(), value.size(), comm);
  for (unsigned int i=0; i<value.size(); ++i) {
    this->setMatEl(i, i, value[i]);
  }
  this->finalize();
}

void Matrix::init(const VectorSet& M) {
  assert(M.isAssembled());
  this->init(M.nRows(), M.nCols(), M.comm());
  for (unsigned int j=0; j<M.nCols(); ++j) {
    for (unsigned int i=0; i<M.nRows(); ++i) {
      this->setMatEl(i, j, M.getMatEl(i, j));
    }
  }
  this->finalize();
}

void Matrix::init(const Matrix& M, const IS& isrow, const IS& iscol) {
  assert(M.m_isAssembled);
  this->clear();
  m_nRows = M.m_nRows;
  m_nCols = M.m_nCols;
  m_comm = M.m_comm;
  MatGetSubMatrix(M.m_data, isrow, iscol, MAT_INITIAL_MATRIX, &m_data);
  m_isInitialised = true;
  m_isAssembled = true;
  m_isInversed = false;
}

void Matrix::init(const Matrix& M) {
  assert(M.m_isAssembled);
  this->clear();
  m_nRows = M.m_nRows;
  m_nCols = M.m_nCols;
  m_comm = M.m_comm;
  MatConvert(M.m_data, MATSAME, MAT_INITIAL_MATRIX, &m_data);
  m_isInitialised = true;
  m_isAssembled = true;
  m_isInversed = false;
}

void Matrix::init(const std::string& filename, const MPI_Comm& comm) {
  this->clear();
  m_comm = comm;
  PetscViewer viewer;
  PetscViewerBinaryOpen(m_comm, filename.c_str(), FILE_MODE_READ, &viewer);
  MatCreate(m_comm, &m_data);
  MatLoad(m_data, viewer);
  PetscViewerDestroy(&viewer);
  int nRows, nCols;
  MatGetSize(m_data, &nRows, &nCols);
  m_nRows = nRows;
  m_nCols = nCols;
  m_isInitialised = true;
  m_isAssembled = false;
  m_isInversed = false;
  this->finalize();
}

void Matrix::clear() {
  if (m_isInitialised) {
    this->finalize();
    if (m_isInversed) {
      KSPDestroy(&m_ksp);
      m_isInversed = false;
    }
    MatDestroy(&m_data);
    m_data = nullptr;
    m_nRows = 0;
    m_nCols = 0;
    m_isInitialised = false;
    m_isAssembled = false;
  }
}


/* 
*  Methods
*/

void Matrix::getOwnershipRange(unsigned int& low, unsigned int& high) const {
  assert(m_isAssembled);
  int low_, high_;
  MatGetOwnershipRange(m_data, &low_, &high_);
  low = low_;
  high = high_;
}

void Matrix::getValues(const std::vector<int>& indi, const std::vector<int>& indj, std::vector<double>& value) const {
  assert(m_isAssembled && indi.size()*indj.size()==value.size());
  MatGetValues(m_data, indi.size(), indi.data(), indj.size(), indj.data(), value.data());
}

double Matrix::getMatEl(const unsigned int i, const unsigned int j) const {
  assert(m_isAssembled && i<m_nRows && j<m_nCols);
  int rank;
  MPI_Comm_rank(m_comm, &rank);
  int startInd, endInd;
  MatGetOwnershipRange(m_data, &startInd, &endInd);
  int idRow = i, idCol = j, amIRoot = 0;
  double value = 0.0;
  if (idRow>=startInd && idRow<endInd) {
    MatGetValues(m_data, 1, &idRow, 1, &idCol, &value);
    amIRoot = 1;
  }
  int consensus = amIRoot*rank, root = 0;
  MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, m_comm);
  MPI_Bcast(&value, 1, MPI_DOUBLE, root, m_comm);
  return value;
}

void Matrix::setMatEl(const unsigned int i, const unsigned int j, const double value, const bool PAR) {
  assert(m_isInitialised && i<m_nRows && j<m_nCols);
  int idRow = i, idCol = j;
  if (!PAR) {
    int rank;
    MPI_Comm_rank(m_comm, &rank);
    if (rank==0) { // Proc 0 is doing it and communicating it
      MatSetValues(m_data, 1, &idRow, 1, &idCol, &value, INSERT_VALUES);
    }
  } else { //Parallel non-communicating way
    int startInd, endInd;
    MatGetOwnershipRange(m_data, &startInd, &endInd);
    if (idRow>=startInd && idRow<endInd) {
      MatSetValues(m_data, 1, &idRow, 1, &idCol, &value, INSERT_VALUES);
    }
  }
  m_isAssembled = false;
}

void Matrix::finalize() {
  assert(m_isInitialised);
  if (!m_isAssembled) {
    MatAssemblyBegin(m_data, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(m_data, MAT_FINAL_ASSEMBLY);
    m_isAssembled = true;
  }
}

void Matrix::inv(const PCType& type) {
  assert(m_isAssembled);
  KSPCreate(m_comm, &m_ksp);
  KSPSetOperators(m_ksp, m_data, m_data);
  KSPSetType(m_ksp, KSPPREONLY);
  KSPGetPC(m_ksp, &m_pc);
  PCSetType(m_pc, type);
  KSPSetFromOptions(m_ksp);
  KSPSetUp(m_ksp);
  m_isInversed = true;
}

void Matrix::add(const Matrix& M, const double scale) {
  assert(m_isAssembled && M.m_isAssembled && m_nRows==M.m_nRows && m_nCols==M.m_nCols && m_comm==M.m_comm);
  if (fabs(scale)!=0.0) {
    MatAXPY(m_data, scale, M.m_data, DIFFERENT_NONZERO_PATTERN);
  }
}

void Matrix::copy(const Matrix& M) {
  assert(m_isAssembled && M.m_isAssembled && m_nRows==M.m_nRows && m_nCols==M.m_nCols && m_comm==M.m_comm);
  MatCopy(M.m_data, m_data, DIFFERENT_NONZERO_PATTERN);
}

void Matrix::print() const {
  assert(m_isAssembled);
  MatView(m_data, PETSC_VIEWER_STDOUT_WORLD);
}

void Matrix::save(const std::string& filename) const {
  assert(m_isAssembled);
  PetscViewer viewer;
  PetscViewerBinaryOpen(m_comm, filename.c_str(), FILE_MODE_WRITE, &viewer);
  MatView(m_data, viewer);
  PetscViewerDestroy(&viewer);
}


/* 
*  Operators
*/

Matrix& Matrix::operator += (const double value) {
  assert(m_isAssembled);
  MatShift(m_data, value);
  return *this;
}

Matrix& Matrix::operator += (const Matrix& M) {
  assert(m_isAssembled && M.m_isAssembled && m_nRows==M.m_nRows && m_nCols==M.m_nCols && m_comm==M.m_comm);
  this->add(M, 1.0);
  return *this;
}

Matrix& Matrix::operator -= (const double value) {
  assert(m_isAssembled);
  this->operator+=(-value);
  return *this;
}

Matrix& Matrix::operator -= (const Matrix& M) {
  assert(m_isAssembled && M.m_isAssembled && m_nRows==M.m_nRows && m_nCols==M.m_nCols && m_comm==M.m_comm);
  this->add(M, -1.0);
  return *this;
}

Matrix& Matrix::operator *= (const double scale) {
  assert(m_isAssembled);
  MatScale(m_data, scale);
  return *this;
}

Matrix& Matrix::operator /= (const double scale) {
  assert(m_isAssembled && fabs(scale)>DBL_EPSILON);
  this->operator*=(1.0/scale);
  return *this;
}

Matrix& Matrix::operator = (const Matrix& M) {
  assert(M.m_isAssembled);
  if (!m_isAssembled || m_nRows!=M.m_nRows || m_nCols!=M.m_nCols || m_comm!=M.m_comm) {
    this->init(M.m_nRows, M.m_nCols, 0.0, M.m_comm);
  }
  this->copy(M);
  return *this;
}


/*
*  Friend methods
*/

double norm(const Matrix& M) {
  assert(M.m_isAssembled);
  double value;
  MatNorm(M.m_data, NORM_FROBENIUS, &value);
  return value;
}

// void MatMult(const Matrix& M, const Vector& u, Vector& v) {
//   assert(M.m_isAssembled && u.m_isAssembled && v.m_isAssembled && M.m_nRows==v.m_size && M.m_nCols==u.m_size);
//   MatMult(M.m_data, u.m_data, v.m_data);
// }

// Vector MatMult(const Matrix& M, const Vector& u) {
//   assert(M.m_isAssembled && u.m_isAssembled && M.m_nCols==u.m_size);
//   Vector v(M.m_nRows, 0.0, M.m_comm);
//   MatMult(M, u, v);
//   return v;
// }

// void MatMultAdd(const Matrix& M, const Vector& u, const Vector& v, Vector& w) {
//   assert(M.m_isAssembled && u.m_isAssembled && v.m_isAssembled && w.m_isAssembled && M.m_nRows==v.m_size && M.m_nRows==w.m_size && M.m_nCols==u.m_size);
//   MatMultAdd(M.m_data, u.m_data, v.m_data, w.m_data);
// }

// void MatMatMult(const Matrix& A, const Matrix& B, Matrix& C) {
//   assert(A.m_isAssembled && B.m_isAssembled && C.m_isAssembled && A.m_nRows==C.m_nRows && A.m_nCols==B.m_nRows && B.m_nCols==C.m_nCols);
//   MatMatMult(A.m_data, B.m_data, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &C.m_data);
// }

// Matrix MatMatMult(const Matrix& A, const Matrix& B) {
//   assert(A.m_isAssembled && B.m_isAssembled && A.m_nCols==B.m_nRows);
//   Matrix C(A.m_nRows, B.m_nCols, 0.0, A.m_comm);
//   MatMatMult(A, B, C);
//   return C;
// }

// void MatMatTransposeMult(const Matrix& A, const Matrix& B, Matrix& C) {
//   assert(A.m_isAssembled && B.m_isAssembled && C.m_isAssembled && A.m_nRows==C.m_nRows && A.m_nCols==B.m_nCols && B.m_nRows==C.m_nCols);
//   MatMatTransposeMult(A.m_data, B.m_data, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &C.m_data);
// }

// Matrix MatMatTransposeMult(const Matrix& A, const Matrix& B) {
//   assert(A.m_isAssembled && B.m_isAssembled && A.m_nCols==B.m_nCols);
//   Matrix C(A.m_nRows, B.m_nRows, 0.0, A.m_comm);
//   MatMatTransposeMult(A, B, C);
//   return C;
// }

// void MatMult(const Matrix& M, const Vector& u, Vector& v) {
//   assert(M.m_isAssembled && u.m_isAssembled && M.m_nCols==u.m_size);
//   if (!v.m_isAssembled || M.m_nRows!=v.m_size) {
//     v.init(M.m_nRows, 0.0, M.m_comm);
//   }
//   MatMult(M.m_M, u.m_x, v.m_x);
// }

// Vector MatMult(const Matrix& M, const Vector& u) {
//   assert(M.m_isAssembled && u.m_isAssembled && M.m_nCols==u.m_size);
//   Vector v(M.m_nRows, 0.0, M.m_comm);
//   MatMult(M, u, v);
//   return v;
// }

// void MatMultAdd(const Matrix& M, const Vector& u, const Vector& v, Vector& w) {
//   assert(M.m_isAssembled && u.m_isAssembled && v.m_isAssembled && M.m_nRows==v.m_size && M.m_nCols==u.m_size);
//   if (!w.m_isAssembled || M.m_nRows!=w.m_size) {
//     w.init(M.m_nRows, 0.0, M.m_comm);
//   }
//   MatMultAdd(M.m_M, u.m_x, v.m_x, w.m_x);
// }

// void MatMatMult(const Matrix& A, const Matrix& B, Matrix& C) {
//   assert(A.m_isAssembled && B.m_isAssembled && A.m_nCols==B.m_nRows);
//   if (!C.m_isAssembled || A.m_nRows!=C.m_nRows || B.m_nCols!=C.m_nCols) {
//     C.init(A.m_nRows, B.m_nCols, 0.0, A.m_comm);
//   }
//   MatMatMult(A.m_M, B.m_M, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &C.m_M);
// }

// Matrix MatMatMult(const Matrix& A, const Matrix& B) {
//   assert(A.m_isAssembled && B.m_isAssembled && A.m_nCols==B.m_nRows);
//   Matrix C(A.m_nRows, B.m_nCols, 0.0, A.m_comm);
//   MatMatMult(A, B, C);
//   return C;
// }

// void MatMatTransposeMult(const Matrix& A, const Matrix& B, Matrix& C) {
//   assert(A.m_isAssembled && B.m_isAssembled && A.m_nCols==B.m_nCols);
//   if (!C.m_isAssembled || A.m_nRows!=C.m_nRows || B.m_nRows!=C.m_nCols) {
//     C.init(A.m_nRows, B.m_nRows, 0.0, A.m_comm);
//   }
//   MatMatTransposeMult(A.m_M, B.m_M, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &C.m_M);
// }

// Matrix MatMatTransposeMult(const Matrix& A, const Matrix& B) {
//   assert(A.m_isAssembled && B.m_isAssembled && A.m_nCols==B.m_nCols);
//   Matrix C(A.m_nRows, B.m_nRows, 0.0, A.m_comm);
//   MatMatTransposeMult(A, B, C);
//   return C;
// }



/*
*  Non-member methods
*/

Matrix diag(const unsigned int nRows, const unsigned int nCols, const double value, const MPI_Comm& comm) {
  Matrix M(nRows, nCols, comm);
  for (unsigned int i=0; i<std::min(nRows, nCols); ++i) {
    M.setMatEl(i, i, value);
  }
  M.finalize();
  return M;
}

Matrix diag(const std::vector<double> value, const MPI_Comm& comm) {
  Matrix M(value.size(), value.size(), comm);
  for (unsigned int i=0; i<value.size(); ++i) {
    M.setMatEl(i, i, value[i]);
  }
  M.finalize();
  return M;
}

Matrix eye(const unsigned int nRows, const unsigned int nCols, const MPI_Comm& comm) {
  return diag(nRows, nCols, 1.0, comm);
}
