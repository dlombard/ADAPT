#ifndef Testcase_hpp
#define Testcase_hpp

#include "../../Applications/FSI/Include.hpp"

class Testcase {

private:

  unsigned int m_nFluid;
  unsigned int m_nSolid;
  unsigned int m_nStep;
  double m_dt;
  double m_Press;
  double m_Tstar; 
  double m_c;
  std::string m_folderpath_input_data;
  std::string m_folderpath_input_observation;
  std::string m_folderpath_output_parameter;
  std::string m_folderpath_output_sol;
  double m_tolr_gmres;
  unsigned int m_itmax_gmres;
  double m_tolc_gmres;
  double m_tolc_interpol;
  unsigned int m_itmax_mcmc;
  double m_step_mcmc;
  unsigned int m_nParameter;
  bool m_isInitialised;

  bool load(const std::string& filename);

public:

  Testcase();
  Testcase(const std::string& filename);
  Testcase(const Testcase& test);
  ~Testcase();

  void init(const std::string& filename);
  void init(const Testcase& test);
  void clear();

  inline const unsigned int nFluid() const;
  inline const unsigned int nSolid() const;
  inline const unsigned int nStep() const;
  inline const double dt() const;
  inline const double Press() const;
  inline const double Tstar() const;
  inline const double c() const;
  inline const std::string& folderpath_input_data() const;
  inline const std::string& folderpath_input_observation() const;
  inline const std::string& folderpath_output_parameter() const;
  inline const std::string& folderpath_output_sol() const;
  inline const double tolr_gmres() const;
  inline const unsigned int itmax_gmres() const;
  inline const double tolc_gmres() const;
  inline const double tolc_interpol() const;
  inline const unsigned int itmax_mcmc() const;
  inline const double step_mcmc() const;
  inline const unsigned int nParameter() const;
  inline const bool uObserved() const;
  inline const bool pObserved() const;
  inline const bool dObserved() const;
  inline const bool vObserved() const;

  Testcase& operator = (const Testcase& test);

};

inline const unsigned int Testcase::nFluid() const {
  assert(m_isInitialised);
  return m_nFluid;
}

inline const unsigned int Testcase::nSolid() const {
  assert(m_isInitialised);
  return m_nSolid;
}

inline const unsigned int Testcase::nStep() const {
  assert(m_isInitialised);
  return m_nStep;
}

inline const double Testcase::dt() const {
  assert(m_isInitialised);
  return m_dt;
}

inline const double Testcase::Press() const {
  assert(m_isInitialised);
  return m_Press;
}

inline const double Testcase::Tstar() const {
  assert(m_isInitialised);
  return m_Tstar;
}

inline const double Testcase::c() const {
  assert(m_isInitialised);
  return m_c;
}

inline const std::string& Testcase::folderpath_input_data() const {
  assert(m_isInitialised);
  return m_folderpath_input_data;
}

inline const std::string& Testcase::folderpath_input_observation() const {
  assert(m_isInitialised);
  return m_folderpath_input_observation;
}

inline const std::string& Testcase::folderpath_output_parameter() const {
  assert(m_isInitialised);
  return m_folderpath_output_parameter;
}

inline const std::string& Testcase::folderpath_output_sol() const {
  assert(m_isInitialised);
  return m_folderpath_output_sol;
}

inline const double Testcase::tolr_gmres() const {
  assert(m_isInitialised);
  return m_tolr_gmres;
}

inline const unsigned int Testcase::itmax_gmres() const {
  assert(m_isInitialised);
  return m_itmax_gmres;
}

inline const double Testcase::tolc_gmres() const {
  assert(m_isInitialised);
  return m_tolc_gmres;
}

inline const double Testcase::tolc_interpol() const {
  assert(m_isInitialised);
  return m_tolc_interpol;
}

inline const unsigned int Testcase::itmax_mcmc() const {
  assert(m_isInitialised);
  return m_itmax_mcmc;
}

inline const double Testcase::step_mcmc() const {
  assert(m_isInitialised);
  return m_step_mcmc;
}


inline const unsigned int Testcase::nParameter() const {
  assert(m_isInitialised);
  return m_nParameter;
}


#endif
