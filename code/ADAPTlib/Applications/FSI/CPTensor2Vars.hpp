/* 
*  Header for the CPTensor2Vars class
*/

#ifndef CPTensor2Vars_hpp
#define CPTensor2Vars_hpp

#include "../../Applications/FSI/Vector.hpp"
#include "../../Applications/FSI/VectorSet.hpp"
#include "../../Applications/FSI/Matrix.hpp"
#include "../../Applications/FSI/Operations.hpp"

class CPTensor2Vars {

  friend double dot(const CPTensor2Vars& tens1, const CPTensor2Vars& tens2);
  friend std::vector<double> vecnorm(const CPTensor2Vars& tens);
  friend double norm(const CPTensor2Vars& tens);

private:

  std::vector<unsigned int> m_nDof_x;
  std::vector<unsigned int> m_rank;
  std::vector<double> m_coeffs;
  VectorSet m_terms_x;
  VectorSet m_terms_y;
  MPI_Comm m_comm;
  bool m_isInitialised;

public:

  CPTensor2Vars();
  CPTensor2Vars(const std::vector<unsigned int>& nDof_x, const unsigned int nDof_y, const std::vector<unsigned int>& rank, const MPI_Comm& comm=PETSC_COMM_WORLD);
  CPTensor2Vars(const std::vector<unsigned int>& nDof_x, const std::vector<unsigned int>& rank, const std::vector<double>& coeffs, const VectorSet& terms_x, const VectorSet& terms_y, const MPI_Comm& comm=PETSC_COMM_WORLD);
  CPTensor2Vars(const CPTensor2Vars& tens, const unsigned int low, const unsigned int high);
  CPTensor2Vars(const Matrix& M, const CPTensor2Vars& tens);
  CPTensor2Vars(const CPTensor2Vars& tens);
  ~CPTensor2Vars();

  void init(const std::vector<unsigned int>& nDof_x, const unsigned int nDof_y, const std::vector<unsigned int>& rank, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const std::vector<unsigned int>& nDof_x, const std::vector<unsigned int>& rank, const std::vector<double>& coeffs, const VectorSet& terms_x, const VectorSet& terms_y, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const CPTensor2Vars& tens, const unsigned int low, const unsigned int high);
  void init(const Matrix& M, const CPTensor2Vars& tens);
  void init(const CPTensor2Vars& tens);
  void clear();

  inline const unsigned int nDof_x(const unsigned int i) const;
  inline const std::vector<unsigned int> nDof_x() const;
  inline const unsigned int rank(const unsigned int i) const;
  inline const std::vector<unsigned int> rank() const;
  inline double& coeffs(const unsigned int iTerm);
  inline const double& coeffs(const unsigned int iTerm) const;
  inline std::vector<double>& coeffs();
  inline const std::vector<double>& coeffs() const;
  inline Vector& terms_x(const unsigned int iTerm);
  inline const Vector& terms_x(const unsigned int iTerm) const;
  inline VectorSet& terms_x();
  inline const VectorSet& terms_x() const;
  inline Vector& terms_y(const unsigned int iTerm);
  inline const Vector& terms_y(const unsigned int iTerm) const;
  inline VectorSet& terms_y();
  inline const VectorSet& terms_y() const;
  inline const MPI_Comm& comm() const;
  inline const bool isInitialised() const;
  inline const bool isAssembled() const;

  void finalize();
  void eval(const unsigned int iTerm, Vector& v) const;
  Vector eval(const unsigned int iTerm) const;
  // void extract(const unsigned int low, const unsigned int high, CPTensor2Vars& tens) const;
  // void extract(const Matrix& M, CPTensor2Vars& tens) const;
  // void copy(const CPTensor2Vars& tens, const unsigned int low, const unsigned int high);
  // void copy(const CPTensor2Vars& tens, const Matrix& M);
  // void copy(const CPTensor2Vars& tens);
  void add(const CPTensor2Vars& tens, const double scale=1.0);
  void truncate(const double tol);
  // void apply(const Matrix& M);
  void solve(const Matrix& M);
  void print(const unsigned int iTerm) const;
  void save(const std::string& filename, const unsigned int iTerm) const;
  
  double operator () (const unsigned int ind_x, const unsigned int ind_y) const;
  CPTensor2Vars& operator += (const CPTensor2Vars& tens);
  CPTensor2Vars& operator -= (const CPTensor2Vars& tens);  
  CPTensor2Vars& operator *= (const double scale);  
  CPTensor2Vars& operator /= (const double scale); 
  CPTensor2Vars& operator = (const CPTensor2Vars& tens);

};

/*
*  Inline methods
*/

inline const unsigned int CPTensor2Vars::nDof_x(const unsigned int i) const {
  assert(i<m_nDof_x.size());
  return m_nDof_x[i];
};

inline const std::vector<unsigned int> CPTensor2Vars::nDof_x() const {
  return m_nDof_x;
};

inline const unsigned int CPTensor2Vars::rank(const unsigned int i) const {
  assert(i<m_rank.size());
  return m_rank[i];
};

inline const std::vector<unsigned int> CPTensor2Vars::rank() const {
  return m_rank;
};

inline double& CPTensor2Vars::coeffs(const unsigned int iTerm) {
  assert(iTerm<m_coeffs.size());
  return m_coeffs[iTerm];
};

inline const double& CPTensor2Vars::coeffs(const unsigned int iTerm) const {
  assert(iTerm<m_coeffs.size());
  return m_coeffs[iTerm];
};

inline std::vector<double>& CPTensor2Vars::coeffs() {
  return m_coeffs;
};

inline const std::vector<double>& CPTensor2Vars::coeffs() const {
  return m_coeffs;
};

inline Vector& CPTensor2Vars::terms_x(const unsigned int iTerm) {
  assert(iTerm<m_terms_x.nCols());
  return m_terms_x(iTerm);
};

inline const Vector& CPTensor2Vars::terms_x(const unsigned int iTerm) const {
  assert(iTerm<m_terms_x.nCols());
  return m_terms_x(iTerm);
};

inline VectorSet& CPTensor2Vars::terms_x() {
  return m_terms_x;
};

inline const VectorSet& CPTensor2Vars::terms_x() const {
  return m_terms_x;
};

inline Vector& CPTensor2Vars::terms_y(const unsigned int iTerm) {
  assert(iTerm<m_terms_y.nCols());
  return m_terms_y(iTerm);
};

inline const Vector& CPTensor2Vars::terms_y(const unsigned int iTerm) const {
  assert(iTerm<m_terms_y.nCols());
  return m_terms_y(iTerm);
};

inline VectorSet& CPTensor2Vars::terms_y() {
  return m_terms_y;
};

inline const VectorSet& CPTensor2Vars::terms_y() const {
  return m_terms_y;
};

inline const MPI_Comm& CPTensor2Vars::comm() const {
  return m_comm;
};

inline const bool CPTensor2Vars::isInitialised() const {
  return m_isInitialised;
};

inline const bool CPTensor2Vars::isAssembled() const {
  if (!m_isInitialised || m_terms_x.nRows()!=sum(m_nDof_x) || m_coeffs.size()!=sum(m_rank) || m_coeffs.size()!=m_terms_x.nCols() || m_coeffs.size()!=m_terms_y.nCols()) {
    return false;
  }
  return (m_terms_x.isAssembled() && m_terms_y.isAssembled());
};

#endif


// /* 
// *  Header for the CPTensor2Vars class
// */

// #ifndef CPTensor2Vars_hpp
// #define CPTensor2Vars_hpp

// #include "../../Applications/FSI/Vector.hpp"
// #include "../../Applications/FSI/VectorSet.hpp"
// #include "../../Applications/FSI/Matrix.hpp"
// #include "../../Applications/FSI/Operations.hpp"

// class CPTensor2Vars {

//   friend double dot(const CPTensor2Vars& tens1, const CPTensor2Vars& tens2);
//   friend double norm(const CPTensor2Vars& tens);

// private:

//   unsigned int m_nDof_x;
//   unsigned int m_nDof_y;
//   unsigned int m_rank;
//   std::vector<double> m_coeffs;
//   VectorSet m_terms_x;
//   VectorSet m_terms_y;
//   MPI_Comm m_comm;
//   bool m_isInitialised;

// public:

//   CPTensor2Vars();
//   CPTensor2Vars(const unsigned int nDof_x, const unsigned int nDof_y, const unsigned int rank, const MPI_Comm& comm=PETSC_COMM_WORLD);
//   CPTensor2Vars(const std::vector<double>& coeffs, const VectorSet& terms_x, const VectorSet& terms_y, const MPI_Comm& comm=PETSC_COMM_WORLD);
//   CPTensor2Vars(const CPTensor2Vars& tens, const unsigned int low, const unsigned int high);
//   CPTensor2Vars(const CPTensor2Vars& tens);
//   ~CPTensor2Vars();

//   void init(const unsigned int nDof_x, const unsigned int nDof_y, const unsigned int rank, const MPI_Comm& comm=PETSC_COMM_WORLD);
//   void init(const std::vector<double>& coeffs, const VectorSet& terms_x, const VectorSet& terms_y, const MPI_Comm& comm=PETSC_COMM_WORLD);
//   void init(const CPTensor2Vars& tens, const unsigned int low, const unsigned int high);
//   void init(const CPTensor2Vars& tens);
//   void clear();

//   inline const unsigned int rank() const;
//   inline const unsigned int nDof_x() const;
//   inline const unsigned int nDof_y() const;
//   inline double& coeffs(const unsigned int iTerm);
//   inline const double coeffs(const unsigned int iTerm) const;
//   inline std::vector<double>& coeffs();
//   inline const std::vector<double>& coeffs() const;
//   inline Vector& terms_x(const unsigned int iTerm);
//   inline const Vector& terms_x(const unsigned int iTerm) const;
//   inline VectorSet& terms_x();
//   inline const VectorSet& terms_x() const;
//   inline Vector& terms_y(const unsigned int iTerm);
//   inline const Vector& terms_y(const unsigned int iTerm) const;
//   inline VectorSet& terms_y();
//   inline const VectorSet& terms_y() const;
//   inline const MPI_Comm& comm() const;
//   inline const bool isInitialised() const;
//   inline const bool isAssembled() const;

//   void finalize();
//   void eval(const unsigned int iTerm, Vector& v) const;
//   Vector eval(const unsigned int iTerm) const;
//   void extract(const CPTensor2Vars& tens, const unsigned int low, const unsigned int high);
//   void extract(const Matrix& M, CPTensor2Vars& tens) const;
//   void add(const CPTensor2Vars& tens, const double scale=1.0);
//   void truncate(const double tol);
//   void solve(const Matrix& M);
//   void print(const unsigned int iTerm) const;
//   void save(const std::string& filename, const unsigned int iTerm) const;
  
//   double operator () (const unsigned int ind_x, const unsigned int ind_y) const;
//   CPTensor2Vars& operator += (const CPTensor2Vars& tens);
//   CPTensor2Vars& operator -= (const CPTensor2Vars& tens);  
//   CPTensor2Vars& operator *= (const double scale);  
//   CPTensor2Vars& operator /= (const double scale); 
//   CPTensor2Vars& operator = (const CPTensor2Vars& tens);

// };

// /*
// *  Inline methods
// */

// inline const unsigned int CPTensor2Vars::rank() const {
//   return m_rank;
// };

// inline const unsigned int CPTensor2Vars::nDof_x() const {
//   return m_nDof_x;
// };

// inline const unsigned int CPTensor2Vars::nDof_y() const {
//   return m_nDof_y;
// };

// inline double& CPTensor2Vars::coeffs(const unsigned int iTerm) {
//   return m_coeffs[iTerm];
// };

// inline const double CPTensor2Vars::coeffs(const unsigned int iTerm) const {
//   return m_coeffs[iTerm];
// };

// inline std::vector<double>& CPTensor2Vars::coeffs() {
//   return m_coeffs;
// };

// inline const std::vector<double>& CPTensor2Vars::coeffs() const {
//   return m_coeffs;
// };

// inline Vector& CPTensor2Vars::terms_x(const unsigned int iTerm) {
//   return m_terms_x(iTerm);
// };

// inline const Vector& CPTensor2Vars::terms_x(const unsigned int iTerm) const {
//   return m_terms_x(iTerm);
// };

// inline VectorSet& CPTensor2Vars::terms_x() {
//   return m_terms_x;
// };

// inline const VectorSet& CPTensor2Vars::terms_x() const {
//   return m_terms_x;
// };

// inline Vector& CPTensor2Vars::terms_y(const unsigned int iTerm) {
//   return m_terms_y(iTerm);
// };

// inline const Vector& CPTensor2Vars::terms_y(const unsigned int iTerm) const {
//   return m_terms_y(iTerm);
// };

// inline VectorSet& CPTensor2Vars::terms_y() {
//   return m_terms_y;
// };

// inline const VectorSet& CPTensor2Vars::terms_y() const {
//   return m_terms_y;
// };

// inline const MPI_Comm& CPTensor2Vars::comm() const {
//   return m_comm;
// };

// inline const bool CPTensor2Vars::isInitialised() const {
//   return m_isInitialised;
// };

// inline const bool CPTensor2Vars::isAssembled() const {
//   if (!m_isInitialised || m_terms_x.nRows()!=m_nDof_x || m_terms_y.nRows()!=m_nDof_y || m_coeffs.size()!=m_rank || m_terms_x.nCols()!=m_rank || m_terms_y.nCols()!=m_rank) {
//     return false;
//   }
//   return (m_terms_x.isAssembled() && m_terms_y.isAssembled());
// };

// #endif
