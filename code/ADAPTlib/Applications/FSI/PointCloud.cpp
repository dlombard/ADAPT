#include "PointCloud.hpp"


/* 
*  Constructors/Destructors
*/

PointCloud::PointCloud() : m_dim(0), m_nOfPt(0) {}

PointCloud::PointCloud(const unsigned int dim, const unsigned int nOfPt) {
  this->init(dim, nOfPt);
}

PointCloud::PointCloud(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt) {
  assert(lb.size()==ub.size());
  this->init(lb, ub, nOfPt);
}

PointCloud::PointCloud(const std::string& filename) {
  this->init(filename);
}

PointCloud::PointCloud(const PointCloud& pts) {
  this->init(pts);
}

PointCloud::~PointCloud() {
  this->clear();
}

void PointCloud::init(const unsigned int dim, const unsigned int nOfPt) {
  m_dim = dim;
  m_nOfPt = nOfPt;
  m_pts.resize(m_nOfPt);
  for (unsigned int i=0; i<m_nOfPt; ++i) {
    m_pts[i].resize(m_dim);
  }
}

void PointCloud::init(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt) {
  assert(lb.size()==ub.size());
  this->uniform(lb, ub, nOfPt);
}

void PointCloud::init(const std::string& filename) {
  this->load(filename);
}

void PointCloud::init(const PointCloud& pts) {
  this->operator=(pts);
}

void PointCloud::clear() {
  for (unsigned int i=0; i<m_nOfPt; ++i) {
    m_pts[i].clear();
  }
  m_pts.clear();
}


/* 
*  Methods
*/

void PointCloud::uniform(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt) {
  assert(lb.size()==ub.size());

  std::default_random_engine generator;
  std::uniform_real_distribution<double> rand_uniform(0.0, 1.0);
  this->init(lb.size(), nOfPt);
  for (unsigned int i=0; i<m_nOfPt; ++i) {
    for (unsigned int j=0; j<m_dim; ++j) {
      m_pts[i][j] = std::max(lb[j], std::min(ub[j], lb[j]+(ub[j]-lb[j])*rand_uniform(generator)));
    }
  }
}

void PointCloud::load(const std::string& filename) {

  std::ifstream file(filename.c_str(), std::ifstream::in);

  if (!file) {

    std::cerr << "Error opening file: " << filename << std::endl;

  } else {

    file >> m_nOfPt;
    file >> m_dim;
    this->init(m_dim, m_nOfPt);
    unsigned int k = 0;
    for (unsigned int i=0; i<m_nOfPt && !file.eof(); ++i) {
      for (unsigned int j=0; j<m_dim && !file.eof(); ++j) {
        file >> m_pts[i][j];
        ++k;
      }
    }
    file.close();
    if (k!=m_nOfPt*m_dim) {
      std::cerr << "Error reading file: " << filename << std::endl;
    }
  }
}

void PointCloud::print() const {
  for (unsigned int i=0; i<m_nOfPt; ++i) {
    for (unsigned int j=0; j<m_dim; ++j) {
      std::cout << m_pts[i][j] << " ";
    }
    std::cout << std::endl;
  }
}

void PointCloud::save(const std::string& filename) const {
  std::ofstream file(filename.c_str(), std::ofstream::out);

  if (!file) {

    std::cerr << "Error opening file: " << filename << std::endl;

  } else {

    file << m_nOfPt << " " << m_dim << std::endl;
    for (unsigned int i=0; i<m_nOfPt && !file.eof(); ++i) {
      for (unsigned int j=0; j<m_dim && !file.eof(); ++j) {
        file << m_pts[i][j] << " ";
      }
      file << std::endl;
    }
    file.close();
  }
}

void PointCloud::resize(const unsigned int nOfPt) {
  m_pts.resize(nOfPt);
  for (unsigned int i=m_nOfPt; i<nOfPt; ++i) {
    m_pts[i].resize(m_dim);
  }
  m_nOfPt = nOfPt;
}

void PointCloud::erase(const unsigned int i) {
  assert(i<m_nOfPt);
  m_pts.erase(std::next(m_pts.begin(), i));
  --m_nOfPt;
}

void PointCloud::insert(const unsigned int i, const std::vector<double>& pt) {
  assert(i<m_nOfPt && pt.size()==m_dim);
  m_pts.insert(std::next(m_pts.begin(), i), pt);
  ++m_nOfPt;
}

void PointCloud::pop_back() {
  assert(m_nOfPt!=0);
  m_pts.pop_back();
  --m_nOfPt;
}

void PointCloud::push_back(const std::vector<double>& pt) {
  assert(pt.size()==m_dim);
  m_pts.push_back(pt);
  ++m_nOfPt;
}

PointCloud& PointCloud::operator = (const PointCloud& pts) {
  m_dim = pts.m_dim;
  m_nOfPt = pts.m_nOfPt;
  m_pts = pts.m_pts;
  return *this;
}


// #include "PointCloud.hpp"


// /* 
// *  Constructors/Destructors
// */

// PointCloud::PointCloud() : m_dim(0), m_nOfPt(0) {}

// PointCloud::PointCloud(const unsigned int dim, const unsigned int nOfPt) {
//   this->init(dim, nOfPt);
// }

// PointCloud::PointCloud(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt) {
//   assert(lb.size()==ub.size());
//   this->init(lb, ub, nOfPt);
// }

// PointCloud::PointCloud(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt, std::vector<unsigned int>& box) {
//   assert(lb.size()==ub.size());
//   this->init(lb, ub, nOfPt, box);
// }

// PointCloud::PointCloud(const std::string& filename) {
//   this->init(filename);
// }

// PointCloud::PointCloud(const PointCloud& pts) {
//   this->init(pts);
// }

// PointCloud::~PointCloud() {
//   this->clear();
// }

// void PointCloud::init(const unsigned int dim, const unsigned int nOfPt) {
//   m_dim = dim;
//   m_nOfPt = nOfPt;
//   m_pts.resize(m_nOfPt);
//   for (unsigned int i=0; i<m_nOfPt; ++i) {
//     m_pts[i].resize(m_dim);
//   }
// }

// void PointCloud::init(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt) {
//   assert(lb.size()==ub.size());
//   this->uniform_sampling(lb, ub, nOfPt);
// }

// void PointCloud::init(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt, std::vector<unsigned int>& box) {
//   assert(lb.size()==ub.size());
//   this->uniform_sampling(lb, ub, nOfPt);
//   this->uniform_box(nOfPt, box);
// }

// void PointCloud::init(const std::string& filename) {
//   this->load(filename);
// }

// void PointCloud::init(const PointCloud& pts) {
//   this->operator=(pts);
// }

// void PointCloud::clear() {
//   for (unsigned int i=0; i<m_nOfPt; ++i) {
//     m_pts[i].clear();
//   }
//   m_pts.clear();
// }


// /* 
// *  Methods
// */

// void PointCloud::uniform_sampling(const std::vector<double>& lb, const std::vector<double>& ub, const unsigned int nOfPt) {
//   assert(lb.size()==ub.size());
//   this->init(lb.size(), pow(nOfPt, lb.size()));
//   for (unsigned int i=0; i<m_dim; ++i) {
//     std::vector<double> value = linspace(lb[i], ub[i], nOfPt);
//     for (unsigned int n=0, j=0; n<pow(nOfPt, i); ++n) {
//       for (unsigned int k=0; k<nOfPt; ++k) {
//         for (unsigned int m=0; m<pow(nOfPt, m_dim-i-1); ++m, ++j) {
//           m_pts[j][i] = value[k];
//         }
//       }
//     }
//   }
// }

// void PointCloud::uniform_box(const unsigned int nOfPt, std::vector<unsigned int>& box) {
//   std::vector<bool> isBox(m_nOfPt, true);
//   for (unsigned int i=0; i<m_dim; ++i) {
//     for (unsigned int n=0, j=0; n<pow(nOfPt, i); ++n) {
//       for (unsigned int k=0; k<nOfPt; ++k) {
//         for (unsigned int m=0; m<pow(nOfPt, m_dim-i-1); ++m, ++j) {
//           if (k!=0 && k!=nOfPt-1) {
//             isBox[j] = false;
//           }
//         }
//       }
//     }
//   }
//   box.resize(0);
//   for (unsigned int i=0; i<m_nOfPt; ++i) {
//     if (isBox[i]) {
//       box.push_back(i);
//     }
//   }
// }

// void PointCloud::load(const std::string& filename) {

//   std::ifstream file(filename.c_str(), std::ifstream::in);

//   if (!file) {

//     std::cerr << "Error opening file: " << filename << std::endl;

//   } else {

//     file >> m_nOfPt;
//     file >> m_dim;
//     this->init(m_dim, m_nOfPt);
//     unsigned int k = 0;
//     for (unsigned int i=0; i<m_nOfPt && !file.eof(); ++i) {
//       for (unsigned int j=0; j<m_dim && !file.eof(); ++j) {
//         file >> m_pts[i][j];
//         ++k;
//       }
//     }
//     file.close();
//     if (k!=m_nOfPt*m_dim) {
//       std::cerr << "Error reading file: " << filename << std::endl;
//     }
//   }
// }

// void PointCloud::print() const {
//   for (unsigned int i=0; i<m_nOfPt; ++i) {
//     for (unsigned int j=0; j<m_dim; ++j) {
//       std::cout << m_pts[i][j] << " ";
//     }
//     std::cout << std::endl;
//   }
// }

// void PointCloud::save(const std::string& filename) const {
//   std::ofstream file(filename.c_str(), std::ofstream::out);

//   if (!file) {

//     std::cerr << "Error opening file: " << filename << std::endl;

//   } else {

//     file << m_nOfPt << " " << m_dim << std::endl;
//     for (unsigned int i=0; i<m_nOfPt && !file.eof(); ++i) {
//       for (unsigned int j=0; j<m_dim && !file.eof(); ++j) {
//         file << m_pts[i][j] << " ";
//       }
//       file << std::endl;
//     }
//     file.close();
//   }
// }

// void PointCloud::resize(const unsigned int nOfPt) {
//   m_pts.resize(nOfPt);
//   for (unsigned int i=m_nOfPt; i<nOfPt; ++i) {
//     m_pts[i].resize(m_dim);
//   }
//   m_nOfPt = nOfPt;
// }

// void PointCloud::erase(const unsigned int i) {
//   assert(i<m_nOfPt);
//   m_pts.erase(std::next(m_pts.begin(), i));
//   --m_nOfPt;
// }

// void PointCloud::insert(const unsigned int i, const std::vector<double>& pt) {
//   assert(i<m_nOfPt && pt.size()==m_dim);
//   m_pts.insert(std::next(m_pts.begin(), i), pt);
//   ++m_nOfPt;
// }

// void PointCloud::pop_back() {
//   assert(m_nOfPt!=0);
//   m_pts.pop_back();
//   --m_nOfPt;
// }

// void PointCloud::push_back(const std::vector<double>& pt) {
//   assert(pt.size()==m_dim);
//   m_pts.push_back(pt);
//   ++m_nOfPt;
// }

// PointCloud& PointCloud::operator = (const PointCloud& pts) {
//   m_dim = pts.m_dim;
//   m_nOfPt = pts.m_nOfPt;
//   m_pts = pts.m_pts;
//   return *this;
// }
