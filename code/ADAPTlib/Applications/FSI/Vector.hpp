/* 
*  Header for the Vector class
*/

#ifndef Vector_hpp
#define Vector_hpp

#include "../../Applications/FSI/Include.hpp"

class VectorSet;
class Matrix;

class Vector {

  friend double dot(const Vector& v1, const Vector& v2);
  friend double norm(const Vector& v);

  friend void MatMult(const Matrix& M, const Vector& u, Vector& v);
  friend void MatMult(const VectorSet& M, const Vector& u, Vector& v);
  friend void MatMultAdd(const Matrix& M, const Vector& u, const Vector& v, Vector& w);
  friend void MatMultTranspose(const Matrix& M, const Vector& u, Vector& v);
  friend void Solve(const Matrix& M, const Vector& u, Vector& v);

private:

  Vec m_data;
  unsigned int m_size;
  MPI_Comm m_comm;
  bool m_isInitialised;
  bool m_isAssembled;

public:

  Vector();
  Vector(const unsigned int size, const MPI_Comm& comm=PETSC_COMM_WORLD);
  Vector(const unsigned int size, const double value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  Vector(const std::vector<double>& value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  Vector(const unsigned int size, const std::vector<unsigned int>& ind, const std::vector<double>& value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  Vector(const Vector& v, const unsigned int low, const unsigned int high);
  Vector(const Vector& v);
  Vector(const std::string& filename, const MPI_Comm& comm=PETSC_COMM_WORLD);
  ~Vector();

  void init(const unsigned int size, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const unsigned int size, const double value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const std::vector<double>& value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const unsigned int size, const std::vector<unsigned int>& ind, const std::vector<double>& value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const Vector& v, const unsigned int low, const unsigned int high);
  void init(const Vector& v);
  void init(const std::string& filename, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void clear();

  inline const unsigned int size() const;
  inline const MPI_Comm& comm() const;
  inline const bool isInitialised() const;
  inline const bool isAssembled() const;

  void getOwnershipRange(unsigned int& low, unsigned int& high) const;
  double getVecEl(const unsigned int i) const;
  void setValues(const std::vector<int>& ind, double* value);
  void setVecEl(const unsigned int i, const double value, const bool PAR=true);
  void finalize();
  void add(const Vector& v, const double scale=1.0);
  void add(const Vector& v, const unsigned int low, const unsigned int high, const double scale=1.0);
  void extract(const unsigned int low, const unsigned int high, Vector& v) const;
  void copy(const Vector& v);
  void print() const;
  void save(const std::string& filename) const;

  Vector& operator += (const double value);
  Vector& operator += (const Vector& v);
  Vector& operator -= (const double value);
  Vector& operator -= (const Vector& v);
  Vector& operator *= (const double scale);
  Vector& operator *= (const Vector& v);
  Vector& operator /= (const double scale);
  Vector& operator = (const double value);
  Vector& operator = (const Vector& v);

};

Vector e(const unsigned int i, const unsigned int size, const MPI_Comm& comm=PETSC_COMM_WORLD);
Vector operator + (const Vector& u, const Vector& v);
Vector operator - (const Vector& u, const Vector& v);


/*
*  Inline methods
*/

inline const unsigned int Vector::size() const {
  return m_size;
};

inline const MPI_Comm& Vector::comm() const {
  return m_comm;
};

inline const bool Vector::isInitialised() const {
  return m_isInitialised;
};

inline const bool Vector::isAssembled() const {
  return m_isAssembled;
};

#endif
