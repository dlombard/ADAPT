#include "CPTensor2Vars.hpp"


/* 
*  Constructors/Destructors
*/

CPTensor2Vars::CPTensor2Vars() : m_isInitialised(false) {}

CPTensor2Vars::CPTensor2Vars(const std::vector<unsigned int>& nDof_x, const unsigned int nDof_y, const std::vector<unsigned int>& rank, const MPI_Comm& comm) : m_isInitialised(false) {
  assert(nDof_x.size()==rank.size());
  this->init(nDof_x, nDof_y, rank, comm);
}

CPTensor2Vars::CPTensor2Vars(const std::vector<unsigned int>& nDof_x, const std::vector<unsigned int>& rank, const std::vector<double>& coeffs, const VectorSet& terms_x, const VectorSet& terms_y, const MPI_Comm& comm) : m_isInitialised(false) {
  assert(nDof_x.size()==rank.size() && terms_x.isAssembled() && terms_y.isAssembled() && terms_x.nRows()==sum(nDof_x) && coeffs.size()==sum(rank) && coeffs.size()==terms_x.nCols() && coeffs.size()==terms_y.nCols());
  this->init(nDof_x, rank, coeffs, terms_x, terms_y, comm);
}

CPTensor2Vars::CPTensor2Vars(const CPTensor2Vars& tens, const unsigned int low, const unsigned int high) : m_isInitialised(false) {
  assert(tens.isAssembled() && low<=high && high<=tens.m_terms_x.nRows());
  this->init(tens, low, high);
}

CPTensor2Vars::CPTensor2Vars(const Matrix& M, const CPTensor2Vars& tens) : m_isInitialised(false) {
  assert(tens.isAssembled() && M.isAssembled() && tens.m_terms_x.nRows()==M.nCols());
  this->init(M, tens);
}

CPTensor2Vars::CPTensor2Vars(const CPTensor2Vars& tens) : m_isInitialised(false) {
  assert(tens.isAssembled());
  this->init(tens);
}

CPTensor2Vars::~CPTensor2Vars() {
  this->clear();
}

void CPTensor2Vars::init(const std::vector<unsigned int>& nDof_x, const unsigned int nDof_y, const std::vector<unsigned int>& rank, const MPI_Comm& comm) {
  m_nDof_x = nDof_x;
  m_rank = rank;
  m_comm = comm;
  m_coeffs.resize(sum(m_rank));
  m_terms_x.init(sum(m_nDof_x), m_coeffs.size(), m_comm);
  m_terms_y.init(nDof_y, m_coeffs.size(), m_comm);
  m_isInitialised = true;
}

void CPTensor2Vars::init(const std::vector<unsigned int>& nDof_x, const std::vector<unsigned int>& rank, const std::vector<double>& coeffs, const VectorSet& terms_x, const VectorSet& terms_y, const MPI_Comm& comm) {
  assert(nDof_x.size()==rank.size() && terms_x.isAssembled() && terms_y.isAssembled() && terms_x.nRows()==sum(nDof_x) && coeffs.size()==sum(rank) && coeffs.size()==terms_x.nCols() && coeffs.size()==terms_y.nCols());
  m_nDof_x = nDof_x;
  m_rank = rank;
  m_comm = comm;
  m_coeffs = coeffs;
  m_terms_x.init(terms_x);
  m_terms_y.init(terms_y);
  m_isInitialised = true;
}

void CPTensor2Vars::init(const CPTensor2Vars& tens, const unsigned int low, const unsigned int high) {
  assert(tens.isAssembled() && low<=high && high<=tens.m_terms_x.nRows());
  m_nDof_x = {high-low};
  m_rank = {tens.m_terms_x.nCols()};
  m_comm = tens.m_comm;
  m_coeffs = tens.m_coeffs;
  m_terms_x.init(tens.m_terms_x, low, high);
  m_terms_y.init(tens.m_terms_y);
  m_isInitialised = true;
}

void CPTensor2Vars::init(const Matrix& M, const CPTensor2Vars& tens) {
  assert(tens.isAssembled() && M.isAssembled() && tens.m_terms_x.nRows()==M.nCols());
  m_nDof_x = {M.nRows()};
  m_rank = {tens.m_terms_x.nCols()};
  m_comm = tens.m_comm;
  m_coeffs = tens.m_coeffs;
  m_terms_x.init(M.nRows(), m_rank[0], 0.0, tens.m_comm);
  MatMatMult(M, tens.m_terms_x, m_terms_x);
  m_terms_y.init(tens.m_terms_y);
  m_isInitialised = true;
}

void CPTensor2Vars::init(const CPTensor2Vars& tens) {
  assert(tens.isAssembled());
  this->init(tens.m_nDof_x, tens.m_rank, tens.m_coeffs, tens.m_terms_x, tens.m_terms_y, tens.m_comm);
}

void CPTensor2Vars::clear() {
  if (m_isInitialised) {
    m_nDof_x.clear();
    m_rank.clear();
    m_coeffs.clear();
    m_terms_x.clear();
    m_terms_y.clear();
    m_isInitialised = false;
  }
}


/* 
*  Methods
*/

void CPTensor2Vars::finalize() {
  assert(m_isInitialised);
  m_terms_x.finalize();
  m_terms_y.finalize();
}

void CPTensor2Vars::eval(const unsigned int iTerm, Vector& v) const {
  assert(this->isAssembled() && v.isAssembled() && v.size()==m_terms_x.nRows() && iTerm<m_terms_y.nRows());
  v = 0.0;
  for (unsigned int jTerm=0; jTerm<m_coeffs.size(); ++jTerm) {
    v.add(m_terms_x(jTerm), m_coeffs[jTerm]*m_terms_y.getMatEl(iTerm, jTerm));
  }
}

Vector CPTensor2Vars::eval(const unsigned int iTerm) const {
  assert(this->isAssembled() && iTerm<m_terms_y.nRows());
  Vector v(m_terms_x.nRows(), 0.0, m_comm);
  this->eval(iTerm, v);
  return v;
}

// void CPTensor2Vars::extract(const unsigned int low, const unsigned int high, CPTensor2Vars& tens) const {
//   assert(this->isAssembled() && tens.isAssembled() && low<=high && high<=this->nDof_x() && tens.nDof_x()==(high-low) && this->rank()==tens.rank());
//   tens.m_coeffs = m_coeffs;
//   m_terms_x.extract(low, high, tens.m_terms_x);
//   tens.m_terms_y = m_terms_y;
// }

// void CPTensor2Vars::extract(const Matrix& M, CPTensor2Vars& tens) const {
//   assert(this->isAssembled() && tens.isAssembled() && M.nRows()==tens.nDof_x() && M.nCols()==this->nDof_x() && this->rank()==tens.rank());
//   tens.coeffs() = m_coeffs;
//   MatMatMult(M, m_terms_x, tens.terms_x());
//   tens.terms_y() = m_terms_y;
// }

void CPTensor2Vars::add(const CPTensor2Vars& tens, const double scale) {
  assert(this->isAssembled() && tens.isAssembled() && m_terms_x.nRows()==tens.m_terms_x.nRows() && m_terms_y.nRows()==tens.m_terms_y.nRows());
  if (fabs(scale)!=0.0) {
    unsigned int rank = m_coeffs.size();
    m_coeffs.resize(rank+tens.m_coeffs.size());
    for (unsigned int iTerm=0; iTerm<tens.m_coeffs.size(); ++iTerm) {
      m_coeffs[rank+iTerm] = scale*tens.m_coeffs[iTerm];    
    }
    m_terms_x.push_back(tens.m_terms_x);
    m_terms_y.push_back(tens.m_terms_y);
    if (m_rank.size()==tens.m_rank.size()) {
      for (unsigned int i=0; i<m_rank.size(); ++i) {
        m_rank[i] += tens.m_rank[i];
      }
    } else {
      m_rank[0] += tens.m_coeffs.size();
    }
  }
}


void CPTensor2Vars::truncate(const double tol) {
  assert(this->isAssembled() && 0.0<=tol && tol<=1.0);

  if (m_coeffs.size()>0) {

    // QR
    VectorSet Q_y;
    Matrix R_y;
    std::vector<VectorSet> Q_x(m_rank.size());
    std::vector<Matrix> R_x(m_rank.size());
    unsigned int start=0, end=0;
    for (unsigned int r=0; r<m_rank.size(); ++r) {
      end += m_nDof_x[r];
      VectorSet subterms_x(m_terms_x, start, end);
      qr(subterms_x, Q_x[r], R_x[r]);
      start = end;
    }
    qr(m_terms_y, Q_y, R_y);

    // SVD
    std::vector<std::vector<double>> S(m_rank.size());
    std::vector<VectorSet> U(m_rank.size()), V(m_rank.size());
    Matrix tmp(m_coeffs.size(), R_y.nRows(), 0.0);
    MatMatTransposeMult(diag(m_coeffs), R_y, tmp);
    for (unsigned int r=0; r<m_rank.size(); ++r) {
      svd(MatMatMult(R_x[r], tmp), U[r], S[r], V[r]);
    }
    
    // Find the rank satisfying the tolerance condition
    for (unsigned int r=0; r<m_rank.size(); ++r) {
      m_rank[r] = 0;
      double error1 = tol, error2 = 1.0;
      while (m_rank[r]<S[r].size() && error1<error2) {
        m_rank[r] += 1;
        error1 = 0.0;
        for (unsigned int iTerm=0; iTerm<m_rank[r]; ++iTerm) {
          error1 += pow(tol*S[r][iTerm], 2);
        }
        error2 = 0.0;
        for (unsigned int iTerm=m_rank[r]; iTerm<S[r].size(); ++iTerm) {
          error2 += (1.0-tol*tol)*pow(S[r][iTerm], 2);
        }
      }
    }

    // Truncated tensor
    m_coeffs.resize(sum(m_rank));
    unsigned int offset = 0;
    for (unsigned int r=0; r<m_rank.size(); ++r) {
      for (unsigned int iTerm=0; iTerm<m_rank[r]; ++iTerm) {
        m_coeffs[offset+iTerm] = S[r][iTerm];
      }
      offset += m_rank[r];
    }

    m_terms_x.init(m_terms_x.nRows(), m_coeffs.size(), 0.0, m_comm);
    offset = 0;
    start = 0;
    end = 0;
    for (unsigned int r=0; r<m_rank.size(); ++r) {
      end += m_nDof_x[r];
      for (unsigned int iTerm=0; iTerm<m_rank[r]; ++iTerm) {
        m_terms_x(offset+iTerm).add(MatMult(Q_x[r], U[r](iTerm)), start, end);
      }
      start = end;
      offset += m_rank[r];
    }

    m_terms_y.resize(m_coeffs.size());
    offset = 0;
    for (unsigned int r=0; r<m_rank.size(); ++r) {
      for (unsigned int iTerm=0; iTerm<m_rank[r]; ++iTerm) {
        MatMult(Q_y, V[r](iTerm), m_terms_y(offset+iTerm));
      }
      offset += m_rank[r];
    }
  }
}

// void CPTensor2Vars::apply(const Matrix& M) {
//   assert(this->isAssembled() && M.nRows()==m_terms_x.nRows() && M.nCols()==m_terms_x.nRows());
//   MatMatMult(M, m_terms_x, m_terms_x);
// }

void CPTensor2Vars::solve(const Matrix& M) {
  assert(this->isAssembled() && M.isInversed() && M.nRows()==m_terms_x.nRows() && M.nCols()==m_terms_x.nRows());
  for (unsigned int i=0; i<m_terms_x.nCols(); ++i) {
    Solve(M, m_terms_x(i), m_terms_x(i));
  }
}

void CPTensor2Vars::print(const unsigned int iTerm) const {
  assert(this->isAssembled() && iTerm<m_terms_y.nRows());
  Vector v(m_terms_x.nRows(), 0.0);
  this->eval(iTerm, v);
  v.print();
}

void CPTensor2Vars::save(const std::string& filename, const unsigned int iTerm) const {
  assert(this->isAssembled() && iTerm<m_terms_y.nRows());
  Vector v(m_terms_x.nRows(), 0.0);
  this->eval(iTerm, v);
  v.save(filename);
}


/* 
*  Operators
*/

double CPTensor2Vars::operator () (const unsigned int ind_x, const unsigned int ind_y) const {
  assert(this->isAssembled() && ind_x<m_terms_x.nRows() && ind_y<m_terms_y.nRows());
  double value = 0.;
  for(unsigned int iTerm=0; iTerm<m_coeffs.size(); ++iTerm) {
    value += m_coeffs[iTerm]*m_terms_x(iTerm).getVecEl(ind_x)*m_terms_y(iTerm).getVecEl(ind_y);
  }
  return value;
}

CPTensor2Vars& CPTensor2Vars::operator += (const CPTensor2Vars& tens) {
  assert(this->isAssembled() && tens.isAssembled() && m_terms_x.nRows()==tens.m_terms_x.nRows() && m_terms_y.nRows()==tens.m_terms_y.nRows());
  this->add(tens, 1.0);
  return *this;
}

CPTensor2Vars& CPTensor2Vars::operator -= (const CPTensor2Vars& tens) {
  assert(this->isAssembled() && tens.isAssembled() && m_terms_x.nRows()==tens.m_terms_x.nRows() && m_terms_y.nRows()==tens.m_terms_y.nRows());
  this->add(tens, -1.0);
  return *this;
} 

CPTensor2Vars& CPTensor2Vars::operator *= (const double scale) {
  assert(this->isAssembled());
  for(unsigned int iTerm=0; iTerm<m_coeffs.size(); ++iTerm) {
    m_coeffs[iTerm] *= scale;    
  }
  return *this;
}

CPTensor2Vars& CPTensor2Vars::operator /= (const double scale) {
  assert(this->isAssembled() && fabs(scale)>DBL_EPSILON);
  for(unsigned int iTerm=0; iTerm<m_coeffs.size(); ++iTerm) {
    m_coeffs[iTerm] /= scale;    
  }
  return *this;
}

CPTensor2Vars& CPTensor2Vars::operator = (const CPTensor2Vars& tens) {
  assert(tens.isAssembled());
  this->init(tens);
  return *this;
}


/*
*  Friend methods
*/

double dot(const CPTensor2Vars& tens1, const CPTensor2Vars& tens2) {
  assert(tens1.isAssembled() && tens2.isAssembled() && tens1.m_terms_x.nRows()==tens2.m_terms_x.nRows() && tens1.m_terms_y.nRows()==tens2.m_terms_y.nRows());
  double value = 0.0;
  for (unsigned int iTerm=0; iTerm<tens1.m_coeffs.size(); ++iTerm) {
    for (unsigned int jTerm=0; jTerm<tens2.m_coeffs.size(); ++jTerm) {
      value += tens1.m_coeffs[iTerm]*tens2.m_coeffs[jTerm]*dot(tens1.m_terms_x(iTerm), tens2.m_terms_x(jTerm))*dot(tens1.m_terms_y(iTerm), tens2.m_terms_y(jTerm));
    }
  }
  return value;
}

std::vector<double> vecnorm(const CPTensor2Vars& tens) {
  assert(tens.isAssembled());
  double value;
  std::vector<double> v(tens.m_terms_y.nRows(), 0.0);
  for (unsigned int iTerm=0; iTerm<tens.m_coeffs.size(); ++iTerm) {
    value = pow(tens.m_coeffs[iTerm], 2)*pow(norm(tens.m_terms_x(iTerm)), 2);
    for (unsigned int j=0; j<v.size(); ++j) {
      v[j] += value*pow(tens.m_terms_y.getMatEl(j,iTerm), 2);
    }
    for (unsigned int jTerm=iTerm+1; jTerm<tens.m_coeffs.size(); ++jTerm) {
      value = 2.0*tens.m_coeffs[iTerm]*tens.m_coeffs[jTerm]*dot(tens.m_terms_x(iTerm), tens.m_terms_x(jTerm));
      for (unsigned int j=0; j<v.size(); ++j) {
        v[j] += value*tens.m_terms_y.getMatEl(j, iTerm)*tens.m_terms_y.getMatEl(j, jTerm);
      }
    }
  }
  for (unsigned int j=0; j<v.size(); ++j) {
    v[j] = sqrt(fmax(0.0, v[j]));
  }
  return v;
}

double norm(const CPTensor2Vars& tens) {
  assert(tens.isAssembled());
  double value = 0.0;
  for (unsigned int iTerm=0; iTerm<tens.m_coeffs.size(); ++iTerm) {
    value += pow(tens.m_coeffs[iTerm], 2)*pow(norm(tens.m_terms_x(iTerm)), 2)*pow(norm(tens.m_terms_y(iTerm)), 2);
    for (unsigned int jTerm=iTerm+1; jTerm<tens.m_coeffs.size(); ++jTerm) {
      value += 2.0*tens.m_coeffs[iTerm]*tens.m_coeffs[jTerm]*dot(tens.m_terms_x(iTerm), tens.m_terms_x(jTerm))*dot(tens.m_terms_y(iTerm), tens.m_terms_y(jTerm));
    }
  }
  return sqrt(fmax(0.0, value));
}


// #include "CPTensor2Vars.hpp"


// /* 
// *  Constructors/Destructors
// */

// CPTensor2Vars::CPTensor2Vars() : m_nDof_x(0), m_nDof_y(0), m_rank(0), m_comm(PETSC_COMM_WORLD), m_isInitialised(false) {}

// CPTensor2Vars::CPTensor2Vars(const unsigned int nDof_x, const unsigned int nDof_y, const unsigned int rank, const MPI_Comm& comm) : m_isInitialised(false) {
//   this->init(nDof_x, nDof_y, rank, comm);
// }

// CPTensor2Vars::CPTensor2Vars(const std::vector<double>& coeffs, const VectorSet& terms_x, const VectorSet& terms_y, const MPI_Comm& comm) : m_isInitialised(false) {
//   assert(terms_x.isAssembled() && terms_y.isAssembled() && coeffs.size()==terms_x.nCols() && coeffs.size()==terms_y.nCols());
//   this->init(coeffs, terms_x, terms_y, comm);
// }

// CPTensor2Vars::CPTensor2Vars(const CPTensor2Vars& tens, const unsigned int low, const unsigned int high) : m_isInitialised(false) {
//   assert(tens.isAssembled() && low<=high && high<=tens.m_nDof_x);
//   this->init(tens, low, high);
// }

// CPTensor2Vars::CPTensor2Vars(const CPTensor2Vars& tens) : m_isInitialised(false) {
//   assert(tens.isAssembled());
//   this->init(tens);
// }

// CPTensor2Vars::~CPTensor2Vars() {
//   this->clear();
// }

// void CPTensor2Vars::init(const unsigned int nDof_x, const unsigned int nDof_y, const unsigned int rank, const MPI_Comm& comm) {
//   m_nDof_x = nDof_x;
//   m_nDof_y = nDof_y;
//   m_rank = rank;
//   m_comm = comm;
//   m_coeffs.resize(m_rank);
//   m_terms_x.init(m_nDof_x, m_rank, m_comm);
//   m_terms_y.init(m_nDof_y, m_rank, m_comm);
//   m_isInitialised = true;
// }

// void CPTensor2Vars::init(const std::vector<double>& coeffs, const VectorSet& terms_x, const VectorSet& terms_y, const MPI_Comm& comm) {
//   assert(terms_x.isAssembled() && terms_y.isAssembled() && coeffs.size()==terms_x.nCols() && coeffs.size()==terms_y.nCols());
//   m_nDof_x = terms_x.nRows();
//   m_nDof_y = terms_y.nRows();
//   m_rank = coeffs.size();
//   m_comm = comm;
//   m_coeffs = coeffs;
//   m_terms_x.init(terms_x);
//   m_terms_y.init(terms_y);
//   m_isInitialised = true;
// }

// void CPTensor2Vars::init(const CPTensor2Vars& tens, const unsigned int low, const unsigned int high) {
//   assert(tens.isAssembled() && low<=high && high<=tens.m_nDof_x);
//   this->init(high-low, tens.m_nDof_y, tens.m_rank, tens.m_comm);
//   this->finalize();
//   this->extract(tens, low, high);
// }

// void CPTensor2Vars::init(const CPTensor2Vars& tens) {
//   assert(tens.isAssembled());
//   this->init(tens.m_nDof_x, tens.m_nDof_y, tens.m_rank, tens.m_comm);
//   this->finalize();
//   this->operator=(tens);
// }

// void CPTensor2Vars::clear() {
//   if (m_isInitialised) {
//     m_nDof_x = 0;
//     m_nDof_y = 0;
//     m_rank = 0;
//     m_coeffs.clear();
//     m_terms_x.clear();
//     m_terms_y.clear();
//     m_isInitialised = false;
//   }
// }


// /* 
// *  Methods
// */

// void CPTensor2Vars::finalize() {
//   assert(m_isInitialised);
//   m_terms_x.finalize();
//   m_terms_y.finalize();
// }

// void CPTensor2Vars::eval(const unsigned int iTerm, Vector& v) const {
//   assert(this->isAssembled() && v.isAssembled() && v.size()==m_nDof_x && iTerm<m_nDof_y);
//   v = 0.0;
//   for (unsigned int jTerm=0; jTerm<m_rank; ++jTerm) {
//     v.add(m_terms_x(jTerm), m_coeffs[jTerm]*m_terms_y.getMatEl(iTerm, jTerm));
//   }
// }

// Vector CPTensor2Vars::eval(const unsigned int iTerm) const {
//   assert(this->isAssembled() && iTerm<m_nDof_y);
//   Vector v(m_nDof_x, 0.0, m_comm);
//   this->eval(iTerm, v);
//   return v;
// }

// void CPTensor2Vars::extract(const CPTensor2Vars& tens, const unsigned int low, const unsigned int high) {
//   assert(this->isAssembled() && tens.isAssembled() && low<=high && high<=tens.m_nDof_x && m_nDof_x==(high-low) && m_rank==tens.m_rank);
//   m_coeffs = tens.m_coeffs;
//   tens.m_terms_x.extract(low, high, m_terms_x);
//   m_terms_y = tens.m_terms_y;
// }

// void CPTensor2Vars::extract(const Matrix& M, CPTensor2Vars& tens) const {
//   assert(this->isAssembled() && tens.isAssembled() && M.nRows()==tens.nDof_x() && M.nCols()==m_nDof_x && m_rank==tens.rank());
//   tens.coeffs() = m_coeffs;
//   MatMatMult(M, m_terms_x, tens.terms_x());
//   tens.terms_y() = m_terms_y;
// }

// void CPTensor2Vars::add(const CPTensor2Vars& tens, const double scale) {
//   assert(this->isAssembled() && tens.isAssembled() && m_nDof_x==tens.m_nDof_x && m_nDof_y==tens.m_nDof_y);
//   if (fabs(scale)!=0.0) {
//     m_coeffs.resize(m_rank+tens.m_rank);
//     for(unsigned int iTerm=0; iTerm<tens.m_rank; ++iTerm) {
//       m_coeffs[m_rank+iTerm] = scale*tens.m_coeffs[iTerm];    
//     }
//     m_terms_x.push_back(tens.m_terms_x);
//     m_terms_y.push_back(tens.m_terms_y);
//     m_rank += tens.m_rank;
//   }
// }


// void CPTensor2Vars::truncate(const double tol) {
//   assert(this->isAssembled() && 0.0<=tol && tol<=1.0);

//   if (m_rank>1) {

//     // QR
//     VectorSet Q_x, Q_y;
//     Matrix R_x, R_y;
//     qr(m_terms_x, Q_x, R_x);
//     qr(m_terms_y, Q_y, R_y);

//     // SVD
//     Vector S;
//     VectorSet U, V;
//     svd(MatMatMult(R_x, MatMatTransposeMult(Matrix(m_coeffs, m_comm), R_y)), U, S, V);
    
//     // Find the rank satisfying the tolerance condition
//     m_rank = 0;
//     double error1 = tol, error2 = 1.0;
//     m_coeffs = Vec2vector(S);
//     while (m_rank<m_coeffs.size() && error1<error2) {
//       ++m_rank;
//       error1 = 0.0;
//       for (unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
//         error1 += (tol*m_coeffs[iTerm])*(tol*m_coeffs[iTerm]);
//       }
//       error2 = 0.0;
//       for (unsigned int iTerm=m_rank; iTerm<m_coeffs.size(); ++iTerm) {
//         error2 += (1.0-tol*tol)*m_coeffs[iTerm]*m_coeffs[iTerm];
//       }
//     }

//     // Truncated tensor
//     m_coeffs.resize(m_rank);

//     m_terms_x.init(m_nDof_x, m_rank, 0.0, m_comm);
//     for (unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
//       MatMult(Q_x, U(iTerm), m_terms_x(iTerm));
//     }

//     m_terms_y.init(m_nDof_y, m_rank, 0.0, m_comm);
//     for (unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
//       MatMult(Q_y, V(iTerm), m_terms_y(iTerm));
//     }
//   }
// }

// void CPTensor2Vars::solve(const Matrix& M) {
//   assert(this->isAssembled() && M.isInversed());
//   for (unsigned int i=0; i<m_rank; ++i) {
//     Solve(M, m_terms_x(i), m_terms_x(i));
//   }
// }

// void CPTensor2Vars::print(const unsigned int iTerm) const {
//   assert(this->isAssembled() && iTerm<m_nDof_y);
//   Vector v(m_nDof_x, 0.0, m_comm);
//   this->eval(iTerm, v);
//   v.print();
// }

// void CPTensor2Vars::save(const std::string& filename, const unsigned int iTerm) const {
//   assert(this->isAssembled() && iTerm<m_nDof_y);
//   Vector v(m_nDof_x, 0.0, m_comm);
//   this->eval(iTerm, v);
//   v.save(filename);
// }


// /* 
// *  Operators
// */

// double CPTensor2Vars::operator () (const unsigned int ind_x, const unsigned int ind_y) const {
//   assert(this->isAssembled() && ind_x<m_nDof_x && ind_y<m_nDof_y);
//   double value = 0.;
//   for(unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
//     value += m_coeffs[iTerm]*m_terms_x(iTerm).getVecEl(ind_x)*m_terms_y(iTerm).getVecEl(ind_y);
//   }
//   return value;
// }

// CPTensor2Vars& CPTensor2Vars::operator += (const CPTensor2Vars& tens) {
//   assert(this->isAssembled() && tens.isAssembled() && m_nDof_x==tens.m_nDof_x && m_nDof_y==tens.m_nDof_y);
//   this->add(tens, 1.0);
//   return *this;
// }

// CPTensor2Vars& CPTensor2Vars::operator -= (const CPTensor2Vars& tens) {
//   assert(this->isAssembled() && tens.isAssembled() && m_nDof_x==tens.m_nDof_x && m_nDof_y==tens.m_nDof_y);
//   this->add(tens, -1.0);
//   return *this;
// } 

// CPTensor2Vars& CPTensor2Vars::operator *= (const double scale) {
//   assert(this->isAssembled());
//   for(unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
//     m_coeffs[iTerm] *= scale;    
//   }
//   return *this;
// }

// CPTensor2Vars& CPTensor2Vars::operator /= (const double scale) {
//   assert(this->isAssembled() && fabs(scale)>DBL_EPSILON);
//   for(unsigned int iTerm=0; iTerm<m_rank; ++iTerm) {
//     m_coeffs[iTerm] /= scale;    
//   }
//   return *this;
// }

// CPTensor2Vars& CPTensor2Vars::operator = (const CPTensor2Vars& tens) {
//   assert(tens.isAssembled());
//   m_nDof_x = tens.m_nDof_x;
//   m_nDof_y = tens.m_nDof_y;
//   m_rank = tens.m_rank;
//   m_coeffs = tens.m_coeffs;
//   m_terms_x = tens.m_terms_x;
//   m_terms_y = tens.m_terms_y;
//   m_comm = tens.m_comm;
//   m_isInitialised = tens.m_isInitialised;
//   return *this;
// }


// /*
// *  Friend methods
// */

// double dot(const CPTensor2Vars& tens1, const CPTensor2Vars& tens2) {
//   assert(tens1.isAssembled() && tens2.isAssembled() && tens1.m_nDof_x==tens2.m_nDof_x && tens1.m_nDof_y==tens2.m_nDof_y);
//   double value = 0.0;
//   for(unsigned int iTerm=0; iTerm<tens1.m_rank; ++iTerm) {
//     for(unsigned int jTerm=0; jTerm<tens2.m_rank; ++jTerm) {
//       value += tens1.m_coeffs[iTerm]*tens2.m_coeffs[jTerm]*dot(tens1.m_terms_x(iTerm), tens2.m_terms_x(jTerm))*dot(tens1.m_terms_y(iTerm), tens2.m_terms_y(jTerm));
//     }
//   }
//   return value;
// }

// double norm(const CPTensor2Vars& tens) {
//   assert(tens.isAssembled());
//   double value = 0.0;
//   for(unsigned int iTerm=0; iTerm<tens.m_rank; ++iTerm) {
//     value += tens.m_coeffs[iTerm]*tens.m_coeffs[iTerm]*dot(tens.m_terms_x(iTerm), tens.m_terms_x(iTerm))*dot(tens.m_terms_y(iTerm), tens.m_terms_y(iTerm));
//     for(unsigned int jTerm=iTerm+1; jTerm<tens.m_rank; ++jTerm) {
//       value += 2.0*tens.m_coeffs[iTerm]*tens.m_coeffs[jTerm]*dot(tens.m_terms_x(iTerm), tens.m_terms_x(jTerm))*dot(tens.m_terms_y(iTerm), tens.m_terms_y(jTerm));
//     }
//   }
//   return sqrt(fmax(0.0, value));
// }

