/* 
*  Header for the Matrix class
*/

#ifndef Matrix_hpp
#define Matrix_hpp

#include "../../Applications/FSI/Include.hpp"
#include "../../Applications/FSI/Vector.hpp"
#include "../../Applications/FSI/VectorSet.hpp"

class Matrix {

  friend double norm(const Matrix& M);

  friend void MatMult(const Matrix& M, const Vector& u, Vector& v);
  friend void MatMultAdd(const Matrix& M, const Vector& u, const Vector& v, Vector& w);
  friend void MatMultTranspose(const Matrix& M, const Vector& u, Vector& v);
  friend void MatMatMult(const Matrix& A, const Matrix& B, Matrix& C);
  friend void MatTransposeMatMult(const Matrix& A, const Matrix& B, Matrix& C);
  friend void MatMatTransposeMult(const Matrix& A, const Matrix& B, Matrix& C);
  friend void Solve(const Matrix& M, const Vector& u, Vector& v);

private:

  Mat m_data;
  unsigned int m_nRows;
  unsigned int m_nCols;
  MPI_Comm m_comm;
  KSP m_ksp;
  PC m_pc;
  bool m_isInitialised;
  bool m_isAssembled;
  bool m_isInversed;

public:

  Matrix();
  Matrix(const unsigned int nRows, const unsigned int nCols, const MPI_Comm& comm=PETSC_COMM_WORLD);
  Matrix(const unsigned int nRows, const unsigned int nCols, const double value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  Matrix(const unsigned int nRows, const unsigned int nCols, const std::vector<unsigned int>& ind_I, const std::vector<unsigned int>& ind_J, const std::vector<double>& value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  Matrix(const std::vector<double>& value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  Matrix(const VectorSet& M);
  Matrix(const Matrix& M, const IS& isrow, const IS& iscol);
  Matrix(const Matrix& M);
  Matrix(const std::string& filename, const MPI_Comm& comm=PETSC_COMM_WORLD);
  ~Matrix();

  void init(const unsigned int nRows, const unsigned int nCols, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const unsigned int nRows, const unsigned int nCols, const double value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const unsigned int nRows, const unsigned int nCols, const std::vector<unsigned int>& ind_I, const std::vector<unsigned int>& ind_J, const std::vector<double>& value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const std::vector<double>& value, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void init(const VectorSet& M);
  void init(const Matrix& M, const IS& isrow, const IS& iscol);
  void init(const Matrix& M);
  void init(const std::string& filename, const MPI_Comm& comm=PETSC_COMM_WORLD);
  void clear();

  inline const unsigned int nRows() const;
  inline const unsigned int nCols() const;
  inline const MPI_Comm& comm() const;
  inline const bool isInitialised() const;
  inline const bool isAssembled() const;
  inline const bool isInversed() const;

  void getOwnershipRange(unsigned int& low, unsigned int& high) const;
  void getValues(const std::vector<int>& indi, const std::vector<int>& indj, std::vector<double>& value) const;
  double getMatEl(const unsigned int i, const unsigned int j) const;
  void setMatEl(const unsigned int i, const unsigned int j, const double value, const bool PAR=true);
  void finalize();
  void inv(const PCType& type=PCLU);
  void add(const Matrix& M, const double scale=1.0);
  void copy(const Matrix& M);
  void print() const;
  void save(const std::string& filename) const;

  Matrix& operator += (const double value);
  Matrix& operator += (const Matrix& M);
  Matrix& operator -= (const double value);
  Matrix& operator -= (const Matrix& M);
  Matrix& operator *= (const double scale);
  Matrix& operator /= (const double scale);
  Matrix& operator = (const Matrix& M);

};

Matrix diag(const unsigned int nRows, const unsigned int nCols, const double value, const MPI_Comm& comm=PETSC_COMM_WORLD);
Matrix diag(const std::vector<double> value, const MPI_Comm& comm=PETSC_COMM_WORLD);
Matrix eye(const unsigned int nRows, const unsigned int nCols, const MPI_Comm& comm=PETSC_COMM_WORLD);


/*
*  Inline methods
*/

inline const unsigned int Matrix::nRows() const {
  return m_nRows;
};

inline const unsigned int Matrix::nCols() const {
  return m_nCols;
};

inline const MPI_Comm& Matrix::comm() const {
  return m_comm;
};

inline const bool Matrix::isInitialised() const {
  return m_isInitialised;
};

inline const bool Matrix::isAssembled() const {
  return m_isAssembled;
};

inline const bool Matrix::isInversed() const {
  return m_isInversed;
};

#endif
