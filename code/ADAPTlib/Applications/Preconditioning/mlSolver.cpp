// implementation of multilinear solvers:

#include "mlSolver.h"

using namespace std;


// auxiliary: mgs between tensors: in is overwritten!
void mgs(vector<CP2var>& in, vector<CP2var>& out, mat_real& R, double tolC){
  const unsigned int kryl_rank = in.size();
  // orthonormalise the elements: (all elements have already been rounded=>use norm_orth)
  R.init(kryl_rank, kryl_rank); R.zero();
  out.resize(kryl_rank);
  for(unsigned int it=0; it<kryl_rank; it++){
    out[it] << in[it];
    out[it].round(tolC);
    double norm_q = out[it].norm_orth();
    R.setMatEl(it, it, norm_q);
    out[it] *= 1.0/norm_q;
    for(unsigned int k=it+1; k<kryl_rank; k++){
      double c = 0.0; dot(out[it], in[k], c);
      R.setMatEl(it, k, c);
      in[k].axpy(out[it], -c, true);
      in[k].round(tolC);
    }
  };
}




// apply operator Tensor:
CP2var applyOperator(operatorTensor& A, CP2var& x){
  assert(A.nTerms()>0);
  assert(A.nVar()==2);
  vector<unsigned int> resPerVar(2);
  resPerVar[0] = A.Op(0,0).nRows();
  resPerVar[1] = A.Op(0,1).nRows();

  unsigned int rank_result = x.rank() * A.nTerms();

  if(rank_result == 0){
    CP2var b; b.zero(resPerVar);
    return b;
  }
  else{
    vector<vec_petsc> R_tab(rank_result);
    vector<vec_petsc> S_tab(rank_result);
    vector<double> c_tab(rank_result);

    unsigned int cc = 0;
    for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
      mat A_r = A.Op(iTerm,0);
      mat A_s = A.Op(iTerm,1);
      for(unsigned int kTerm=0; kTerm<x.rank(); kTerm++){
        // r term:
        vec_petsc x_r = x.R(kTerm);
        matVecProd(A_r, x_r, R_tab[cc]);

        // s term:
        vec_petsc x_s = x.S(kTerm);
        matVecProd(A_s, x_s, S_tab[cc]);

        // coeffs:
        c_tab[cc] = x.coeffs(kTerm);

        cc += 1;
      }
    }

    // define the result:
    CP2var b(R_tab, S_tab, c_tab);
    return b;
  }
}

// overloaded with void type:
void applyOperator(operatorTensor& A, CP2var& x, CP2var& b){
  assert(A.nTerms()>0);
  assert(A.nVar()==2);
  vector<unsigned int> resPerVar(2);
  resPerVar[0] = A.Op(0,0).nRows();
  resPerVar[1] = A.Op(0,1).nRows();

  unsigned int rank_result = x.rank() * A.nTerms();

  if(rank_result == 0){
    b.zero(resPerVar);
  }
  else{
    //b.init(resPerVar);
    vector<vec_petsc> R_tab(rank_result);
    vector<vec_petsc> S_tab(rank_result);
    vector<double> c_tab(rank_result);

    unsigned int cc = 0;
    for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
      mat A_r = A.Op(iTerm,0);
      mat A_s = A.Op(iTerm,1);
      for(unsigned int kTerm=0; kTerm<x.rank(); kTerm++){
        // r term:
        vec_petsc x_r = x.R(kTerm);
        matVecProd(A_r, x_r, R_tab[cc]);

        // s term:
        vec_petsc x_s = x.S(kTerm);
        matVecProd(A_s, x_s, S_tab[cc]);

        // coeffs:
        c_tab[cc] = x.coeffs(kTerm);

        cc += 1;
      }
    }
    b.set_terms(R_tab,S_tab);
    b.set_coeffs(c_tab);
  }
}


// operator implementing it:
CP2var operator * (operatorTensor& A, CP2var& x){
  CP2var b = applyOperator(A, x);
  return b;
}


// Apply first type of preconditioner: multiply the terms of the first variable by the inverse of the matrix P;
void applyPreconditioner(KSP& prec, CP2var& x){
  for(unsigned int kTerm=0; kTerm<x.rank(); kTerm++){
    vec_petsc x_r = x.R(kTerm);
    vec_petsc new_r(x_r.size(),x_r.comm());
    new_r.finalize();
    KSPSolve(prec, x_r.x(), new_r.x());
    x.clear_term_R(kTerm);
    x.set_term_R(kTerm, new_r);
  }
}


// implementation of gmres with preconditioning type 1:
// sol is the initial guess, to be initialised, eventually put equal to zero.
/*
void gmres(operatorTensor& A, CP2var& b, CP2var& sol_0, CP2var& sol, mat& P, double tolR, double tolC, unsigned int maxIt){
  MPI_Comm communicator = b.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // define the tolerance:
  double norm_rhs = b.norm();
  const double tol_gmres = tolR * norm_rhs;

  // setup of the linear solver for the preconditioner:
  KSP prec;
  KSPCreate(communicator,&prec);
  KSPSetOperators(prec, P.M(), P.M());
  KSPSetType(prec, KSPGMRES);
  KSPSetFromOptions(prec);
  KSPSetUp(prec);

  // starting gmres:
  CP2var residual;
  residual << b;
  CP2var Ax = applyOperator(A,sol_0);
  //residual -= Ax;
  residual.axpy(Ax,-1.0, true);
  Ax.clear();

  residual.round(tolC);

  vector<double> res_norm(1);
  res_norm[0] = residual.norm();
  if(idProc==0){
    cout << "Residual norm = " << res_norm[0] << endl;
  }


  // apply the preconditioner:
  applyPreconditioner(prec, residual);

  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  beta[0] = residual.norm();

  residual *= (1.0/beta[0]);

  vector<CP2var> Q(maxIt+1);
  Q[0] << residual;
  mat_real H(maxIt+1,maxIt);

  unsigned int it = 0;
  bool haveToIter = (it<maxIt) && (res_norm[it]>tol_gmres);
  while(haveToIter){
    // Arnoldi iteration:
    Q[it+1] = applyOperator(A, Q[it]);
    Q[it+1].round(tolC);

    applyPreconditioner(prec, Q[it+1]);

    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = Q[it+1] * Q[sub_it];
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val, true);
      Q[it+1].round(tolC);
    }
    double value = Q[it+1].norm();
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);
    //cout << "Finished Arnoldi" << endl;

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);

    //cout << "Givens done" << endl;

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    vector<double> y(it);
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }

    // copy the initial guess:
    CP2var tmpSol;
    //sol << sol_0;
    tmpSol << sol_0;
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      //sol.axpy(Q[sub_it],y[sub_it],true);
      tmpSol.axpy(Q[sub_it],y[sub_it],true);
    }
    cout << "sol norm = " << tmpSol.norm() << endl;
    cout << "sol rank = " << tmpSol.rank() << endl;

    tmpSol.round(tolC);
    cout << "sol norm after round = " << tmpSol.norm() << endl;


    // compute the residual:
    CP2var res; res << b;
    CP2var Ax = applyOperator(A,tmpSol);

    //residual -= Ax;
    res.axpy(Ax, -1.0, true);
    Ax.clear();

    //res.round(tolC);
    res_norm[it] = res.norm();
    if(idProc==0){
      cout << "Residual norm = " << res_norm[it] << endl;
    }
    res.clear();

    // check if iterate or not
    haveToIter = (it<maxIt) && (res_norm[it]>tol_gmres);
    if(!haveToIter){
      sol << tmpSol;
    }
    tmpSol.clear();
  }

  // free the memory:
  residual.clear();
  KSPDestroy(&prec);
  for(unsigned int i=0; i<it+1; i++){
    Q[i].clear();
  }
  Q.clear();
  H.clear();
}*/


// implementation of gmres
void gmres(operatorTensor& A, CP2var& b, CP2var& sol_0, CP2var& sol, mat& P, double tolR, double tolC, unsigned int maxIt){
    MPI_Comm communicator = b.R(0).comm();
    int idProc;
    MPI_Comm_rank(communicator, &idProc);

    // define the tolerance:
    double norm_rhs = b.norm();
    const double tol_gmres = tolR * norm_rhs;

    // setup of the linear solver for the preconditioner:
    KSP prec;
    PC m_pc;
    KSPCreate(communicator,&prec);
    KSPSetOperators(prec, P.M(), P.M());
    KSPSetType(prec, KSPGMRES);
    //KSPSetType(prec, KSPPREONLY);
    //KSPGetPC(prec, &m_pc);
    //PCSetType(m_pc, PCLU);
    KSPSetTolerances(prec, 1.0e-14, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);
    KSPSetFromOptions(prec);
    KSPSetUp(prec);

    // starting gmres:
    CP2var residual;
    residual << b;
    CP2var Ax = applyOperator(A,sol_0);
    //residual -= Ax;
    residual.axpy(Ax,-1.0, true);
    Ax.clear();
    residual.round(tolC);

    vector<double> res_norm(maxIt+1);
    res_norm[0] = residual.norm();
    if(idProc==0){
      cout << "Residual norm = " << res_norm[0] << endl;
    }
    // apply the preconditioner:
    applyPreconditioner(prec, residual);
    double test_norm = residual.norm();
    if(idProc==0){
      cout << "The norm of the preconditioned res = " << test_norm << endl;
    }

    vector<double> cs(maxIt,0.0);
    vector<double> sn(maxIt,0.0);
    vector<double> beta(maxIt+1, 0.0);
    beta[0] = residual.norm();

    residual *= (1.0/beta[0]);

    vector<CP2var> Q(maxIt+1);
    Q[0] << residual;
    residual.clear();

    // initialising mat_real H:
    mat_real H(maxIt+1,maxIt);

    unsigned int it = 0;
    bool haveToIter = (it<maxIt) && (res_norm[it]>tol_gmres);
    while(haveToIter){
      // Arnoldi iteration:
      //Q[it+1] = applyOperator(A, Q[it]);
      cout << "Apply operator: " << endl;
      applyOperator(A, Q[it], Q[it+1]);
      cout << "rounding" << endl;
      Q[it+1].round(tolC);
      cout << "ok" << endl;

      applyPreconditioner(prec, Q[it+1]);
      //MPI_Barrier(communicator);

      cout << "Starting Arnoldi" << endl;
      for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
        double val = Q[it+1] * Q[sub_it];
        H.setMatEl(sub_it,it,val);
        Q[it+1].axpy(Q[sub_it], -val, true);
        Q[it+1].round(tolC);
      }
      double value = Q[it+1].norm();
      H.setMatEl(it+1,it, value);
      Q[it+1]*=(1.0/value);
      cout << "Finished Arnoldi" << endl << endl;

      // Givens Rotation:
      for(unsigned int sub_it=0; sub_it<it; sub_it++){
        double val_c = H(sub_it,it);
        double val_s = H(sub_it+1, it);
        double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
        double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
        H.setMatEl(sub_it+1, it, mat_entry);
        H.setMatEl(sub_it, it, tmp);
      }
      double tmp_d = H(it,it);
      double tmp_p = H(it+1,it);
      double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
      cs[it] = tmp_d/val;
      sn[it] = tmp_p/val;

      double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
      H.setMatEl(it,it,diagEntry);
      H.setMatEl(it+1,it, 0.0);
      cout << "Givens done" << endl << endl;

      // compute the residual:
      beta[it+1] = -sn[it]*beta[it];
      beta[it] = cs[it]*beta[it];
      // solve small triangular linear system:
      it += 1;
      vector<double> y(it,0.0);
      for(int i=it-1; i>=0; i--){
        y[i] = beta[i];
        for(int j=it-1; j>i; j--){
          y[i] -= H(i,j)*y[j];
        }
        y[i] = y[i]/(H(i,i));
      }
      cout << "y values done." << endl;
      //MPI_Barrier(communicator);

      // copy the initial guess:
      CP2var tmpSol;
      tmpSol << sol_0;
      for(unsigned int sub_it=0; sub_it<it; sub_it++){
        tmpSol.axpy(Q[sub_it],y[sub_it],true);
      }
      double tmp_norm = tmpSol.norm();

      if(idProc==0){
        cout << "sol norm = " << tmp_norm << endl;
        cout << "sol rank = " << tmpSol.rank() << endl;
      }
      tmpSol.round(tolC);
      tmp_norm = tmpSol.norm();
      if(idProc==0){
        cout << "sol rank after round = " << tmpSol.rank() << endl;
        cout << "sol norm after round = " << tmp_norm << endl;
      }
      //cout << "Proc " << idProc << " sol rank = " << tmpSol.rank() << endl;
      //cout << "Proc " << idProc << " sol norm = " << tmp_norm << endl;


      // compute the residual:
      CP2var res; res << b;
      CP2var Ax = applyOperator(A,tmpSol);

      //residual -= Ax;
      res.axpy(Ax, -1.0, true);
      Ax.clear();

      //res.round(tolC);
      //MPI_Barrier(communicator);
      res_norm[it] = res.norm();
      //MPI_Barrier(communicator);

      cout << "Proc " << idProc << " resNorm = " << res_norm[it] << endl;

      // check if iterate or not:
      haveToIter = (it<maxIt) && (res_norm[it]>tol_gmres);
      if(!haveToIter){
        cout << " copy the solution and exit"<< endl;
        sol << tmpSol;
      }
      tmpSol.clear();
    }

    // free the memory:
    KSPDestroy(&prec);
    for(unsigned int i=0; i<it+1; i++){
      Q[i].clear();
    }
    Q.clear();
    //H.clear();
}


// Arnoldi iteration for a restarted GMRES:
void Arnoldi(operatorTensor&A, CP2var& residual, KSP& prec, vector<CP2var>& Q, vector<double>& y, double tolC, unsigned int maxIt){
  // communicator:
  MPI_Comm communicator = residual.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // resize and introduce tables:
  Q.resize(maxIt+1);
  y.resize(maxIt+1);
  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  mat_real H(maxIt+1,maxIt);
  H.zero();

  // apply the preconditioner:
  Q[0] << residual;
  applyPreconditioner(prec, Q[0]);
  beta[0] = Q[0].norm();
  Q[0] *= (1.0/beta[0]);

  // Arnoldi iteration:
  if(idProc==0){
    cout << "Starting Arnoldi" << endl;
  }
  unsigned int it = 0;
  bool haveToIter = (it<maxIt);
  while(haveToIter){
    // apply operator:
    applyOperator(A, Q[it], Q[it+1]);
    Q[it+1].round(tolC);

    applyPreconditioner(prec, Q[it+1]);
    MPI_Barrier(communicator);

    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = 0.0;
      dot(Q[it+1], Q[sub_it], val);
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val, true);
      Q[it+1].round(tolC);
    }
    //double value = Q[it+1].norm();
    double value = Q[it+1].norm_orth();
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }
    MPI_Barrier(communicator);
  // end while:
  haveToIter = (it<maxIt);
  }
  if(idProc==0){
    cout << "Arnoldi finished." << endl;
  }
}


// Second type of preconditioner:

// 1 -- Apply preconditioner --
void applyPreconditioner(vector<KSP>& P, vector<vector<unsigned int> >& indices, CP2var& T){
  const unsigned int nMat = P.size();
  const unsigned int rank_result = nMat * T.rank();
  vector<vec_petsc> R_tab(rank_result);
  vector<vec_petsc> S_tab(rank_result);
  vector<double> coeff_tab(rank_result);

  unsigned int cc = 0;
  for(unsigned int iMat = 0; iMat<nMat; iMat++){
    KSP P_i = P[iMat];
    vector<unsigned int> ind_rows = indices[iMat];
    for(unsigned int iTerm=0; iTerm<T.rank(); iTerm++){

      // R_tab:
      vec_petsc r = T.R(iTerm);
      vec_petsc new_r(r.size(),r.comm());
      new_r.finalize();
      KSPSolve(P_i, r.x(), new_r.x());
      R_tab[cc] = new_r;


      // coeffs:
      double val = T.coeffs(iTerm);
      coeff_tab[cc] = val;

      // S_tab:
      vec_petsc s = T.S(iTerm);
      vec_petsc new_s(s.size(), s.comm());
      for(unsigned int iRow=0; iRow<ind_rows.size(); iRow++){
        unsigned int iDof = ind_rows[iRow];
        double val = s.getVecEl(iDof);
        new_s.setVecEl(iDof,val);
      }
      new_s.finalize();
      S_tab[cc] = new_s;

      cc += 1;
    }
  }

  // clear modes in T:
  T.clear();
  T.set_rank(rank_result);
  T.set_terms(R_tab, S_tab);
  T.set_coeffs(coeff_tab);

}


// Arnoldi iteration with second type preconditioner:
void Arnoldi(operatorTensor& A, CP2var& residual, vector<KSP>& prec_list, vector<vector<unsigned int> >& ind_list, vector<CP2var>& Q, vector<double>& y, double tolC, unsigned int maxIt){
  // communicator:
  MPI_Comm communicator = residual.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // resize and introduce tables:
  Q.resize(maxIt+1);
  y.resize(maxIt+1);
  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  mat_real H(maxIt+1,maxIt);
  H.zero();

  // apply the preconditioner:
  Q[0] << residual;
  applyPreconditioner(prec_list, ind_list, Q[0]);
  Q[0].round(tolC);
  beta[0] = Q[0].norm();
  Q[0] *= (1.0/beta[0]);

  // Arnoldi iteration:
  if(idProc==0){
    cout << "Starting Arnoldi" << endl;
  }
  unsigned int it = 0;
  bool haveToIter = (it<maxIt);
  while(haveToIter){
    // apply operator:
    applyOperator(A, Q[it], Q[it+1]);
    Q[it+1].round(tolC);

    applyPreconditioner(prec_list, ind_list, Q[it+1]);
    MPI_Barrier(communicator);

    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = 0.0;
      dot(Q[it+1], Q[sub_it], val);
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val, true);
      Q[it+1].round(tolC);
    }
    //double value = Q[it+1].norm();
    double value = Q[it+1].norm_orth();
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }
    MPI_Barrier(communicator);
  // end while:
  haveToIter = (it<maxIt);
  }
  if(idProc==0){
    cout << "Arnoldi finished." << endl;
  }
}



// overloaded, give a string to save residual, and computational time:

// auxiliary: evaluate residual norm
double eval_residual_norm(CP2var& b, operatorTensor& A, vector<CP2var>& Q, vector<double>& y, unsigned int& it, double& tolC, unsigned int& sol_rank){
  const unsigned int maxIt = it;

  // assemble the solution:
  CP2var tmpSol;
  tmpSol << Q[0]; tmpSol *= y[0];
  for(unsigned int sub_it=1; sub_it<=maxIt; sub_it++){
    tmpSol.axpy(Q[sub_it],y[sub_it], true);
  }
  tmpSol.round(tolC);
  sol_rank = tmpSol.rank();

  // compute the residual:
  CP2var res;
  res << b;
  CP2var Ax = applyOperator(A,tmpSol);
  //residual -= Ax;
  res.axpy(Ax, -1.0, true);
  Ax.clear();

  res.round(tolC);
  double res_norm = res.norm_orth();

  // free the memory:
  tmpSol.clear();
  res.clear();

  return res_norm;
}



// Arnoldi iteration with second type preconditioner:
void Arnoldi_record(string file_name, operatorTensor& A, CP2var& residual, vector<KSP>& prec_list, vector<vector<unsigned int> >& ind_list, vector<CP2var>& Q, vector<double>& y, double tolC, unsigned int maxIt){
  string file_res = file_name + "_res.txt";
  string file_time = file_name + "_time.txt";
  string file_rank = file_name + "_rank.txt";

  // communicator:
  MPI_Comm communicator = residual.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // open files:
  ofstream out_res (file_res, std::ofstream::out);
  ofstream out_time(file_time, std::ofstream::out);
  ofstream out_rank(file_rank, std::ofstream::out);

  // resize and introduce tables:
  Q.resize(maxIt+1);
  y.resize(maxIt+1);
  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  mat_real H(maxIt+1,maxIt);
  H.zero();

  // apply the preconditioner:
  Q[0] << residual;
  applyPreconditioner(prec_list, ind_list, Q[0]);
  Q[0].round(tolC);
  beta[0] = Q[0].norm();
  Q[0] *= (1.0/beta[0]);

  // Arnoldi iteration:
  if(idProc==0){
    cout << "Starting Arnoldi" << endl;
  }
  unsigned int it = 0;
  bool haveToIter = (it<maxIt);
  while(haveToIter){

    // starting chrono:
    auto start = std::chrono::high_resolution_clock::now();

    // apply operator:
    applyOperator(A, Q[it], Q[it+1]);
    Q[it+1].round(tolC);

    applyPreconditioner(prec_list, ind_list, Q[it+1]);
    MPI_Barrier(communicator);

    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = 0.0;
      dot(Q[it+1], Q[sub_it], val);
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val, true);
      Q[it+1].round(tolC);
    }
    //double value = Q[it+1].norm();
    double value = Q[it+1].norm_orth();
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }

    // stop chrono:
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

    MPI_Barrier(communicator);
    // eval residual at this iteration:
    unsigned int tmp_sol_rank;
    double residual_norm = eval_residual_norm(residual, A, Q, y, it, tolC, tmp_sol_rank);
    if(idProc == 0){
      out_time << duration.count()/1000000.0 << endl;
      out_res << residual_norm << endl;
      out_rank << tmp_sol_rank << endl;
    }
    MPI_Barrier(communicator);

  // end while:
  haveToIter = (it<maxIt);
  }
  if(idProc==0){
    cout << "Arnoldi finished." << endl;
  }
  out_res.close();
  out_time.close();
  out_rank.close();

}



// Preconditioner type 3: B_x, B_theta are the Tucker representation of 2 matrices
/*void applyPreconditioner(vector<CP2var>& proj, vector<CP2var>& rec, mat_real& M, CP2var& T){

  const unsigned int proj_size = proj.size();
  const unsigned int rec_size = rec.size();

  // scalar product between T and proj:
  vec_real ct(proj_size);
  for(unsigned int iRow=0; iRow<proj_size; iRow++){
    double value;
    dot(T, proj[iRow], value);
    ct.setVecEl(iRow, value);
  }
  // transform value:
  vec_real out;
  matVecProd(M, ct, out);

  // reconstruct:
  T.clear();
  T << rec[0];
  T *= out(0);
  for(unsigned int i=1; i<rec_size; i++){
    T.axpy(rec[i], out(i), true);
  }
}*/
// auxiliary: compute the Moore-Penrose pseudo-inverse:
void pinv(mat_real& input, mat_real& output, double tol){

  // compute the SVD of input:
  int m = input.nRows();
  int n = input.nCols();
  int k = (m < n) ? m : n;
  char jobU = 'S';
  char jobV = 'S';
  int lda = m; //(m > 1) ? m : 1;
  double* s; s = new double[k];
  double* U; U = new double[m*k];
  int ldu = m;
  double* Vt; Vt = new double[k*n];
  int ldvt = n;
  double* work; work = new double[8*k];
  int lw= 8*k; // LWORK >= MAX(1,3*MIN(M,N) + MAX(M,N),5*MIN(M,N))
  int info;

  // copy the matrix;
  double* matrix = input.M();
  dgesvd_(&jobU, &jobV, &m, &n, matrix, &lda, s , U, &ldu, Vt, &ldvt, work, &lw, &info);

  // compute S^dagger:
  vector<double> s_dagger(k);
  for(unsigned int iS=0; iS<k; iS++){
    if(s[iS]>tol){
      s_dagger[iS] = 1.0/s[iS];
    }
    else{
      s_dagger[iS] = 0.0;
    }
  };

  // the matrix output is V s_dagger U^T
  output.init(n,m);

  // U_{ji} = U[i*nRows + j], V^T[ij] = Vt[j*k + i]
  mat_real s_dag_times_Ut(k,m);
  for(unsigned int iRow=0; iRow<k; iRow++){
    for(unsigned int jCol=0; jCol<m; jCol++){
      double val_u = U[iRow*m + jCol];
      double val = val_u * s_dagger[iRow];
      s_dag_times_Ut.setMatEl(iRow,jCol,val);
    }
  }

  // output = V * s_dag_times_Ut
  for(unsigned int iRow=0; iRow<n; iRow++){
    for(unsigned int jCol=0; jCol<m; jCol++){
      double val = 0.0;
      for(unsigned int l=0; l<k; l++){
        double val_v = Vt[iRow*k + l];
        double val_sU = s_dag_times_Ut(l,jCol);
        val += val_v * val_sU;
      }
      output.setMatEl(iRow,jCol,val);
    }
  }

  // free the memory:
  delete[] s;
  delete[] U;
  delete[] Vt;
  delete[] work;
}


// q is overwritten:
void applyPreconditioner(operatorTensor& A, CP2var& q, unsigned int nIt, double tolC){

  const unsigned int n_rows = A.Op(0,0).nRows();
  const unsigned int n_cols = A.Op(0,0).nCols();
  const unsigned int nDof_space = q.nDof_var(0);
  const unsigned int nDof_theta = q.nDof_var(1);

  // copy the input: (in general in the call *q = *out)
  CP2var in; in << q;


  // assemble the space operator:
  // input:
  CP2var in_space; in_space << q;
  for(unsigned int k=0; k<in_space.rank(); k++){
    vec_petsc one_space(nDof_theta, q.S(0).comm());
    one_space.ones();
    one_space.finalize();
    in_space.clear_term_S(k);
    in_space.set_term_S(k, one_space);
  }

  // find the best linear combination of the space operators, such that:
  // A q ~ Pˆ{-1} q, Pˆ{-1} = a_1 A_1 + ... + a_n A_n
  vector<CP2var> app_space(A.nTerms());
  for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
    app_space[iTerm] << in_space;
    mat tmpM = A.Op(iTerm,0);
    tmpM.finalize();
    for(unsigned int k=0; k<q.rank(); k++){
      vec_petsc r;
      vec_petsc mode = in_space.R(k);
      matVecProd(tmpM, mode, r);
      r.finalize();
      app_space[iTerm].clear_term_R(k);
      app_space[iTerm].set_term_R(k, r);
    }
  }

  // orthonormalise them:
  vector<CP2var> tmpQ;
  mat_real tmpR;
  mgs(app_space, tmpQ, tmpR, tolC);

  // target:
  CP2var tg_space;
  applyOperator(A, in_space, tg_space);

  // find the best approximation of the target:
  vector<double> p_coeff(A.nTerms());
  for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
    dot(tmpQ[iTerm], tg_space, p_coeff[iTerm]);
  }

  // solve the triangular system:
  vector<double> sol_trg(A.nTerms());
  for(int i=A.nTerms()-1; i>=0; i--){
    double val = p_coeff[i];
    for(int k=A.nTerms()-1; k>i; k--){
      val -= tmpR(i,k) * sol_trg[k];
    }
    double diagR = tmpR(i,i);
    if(diagR>1.0e-12){
      val = val / diagR;
    }
    else{
      val = 0.0;
    }
    sol_trg[i] = val;
  }

  // assemble the matrix:
  mat P(n_rows, n_cols, PETSC_COMM_WORLD);
  P.finalize();
  for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
    mat tmpM = A.Op(iTerm,0);
    P.sum(tmpM, sol_trg[iTerm]);
  }
  P.finalize();

  // define a GMRES solver:
  KSP prec;
  KSPCreate(PETSC_COMM_WORLD,&prec);
  KSPSetOperators(prec, P.M(), P.M());
  KSPSetType(prec, KSPGMRES);
  KSPSetTolerances(prec, 1.0e-14, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);
  KSPSetFromOptions(prec);
  KSPSetUp(prec);


  // define the operators in the parameter direction:
  // input:
  CP2var in_theta; in_theta << q;
  for(unsigned int k=0; k<in_theta.rank(); k++){
    vec_petsc one_theta(nDof_space, in.R(0).comm());
    one_theta.ones();
    one_theta.finalize();
    in_theta.clear_term_R(k);
    in_theta.set_term_R(k, one_theta);
  }

  // find the best linear combination of the space operators, such that:
  // A q ~ Pˆ{-1} q, Pˆ{-1} = a_1 A_1 + ... + a_n A_n
  vector<CP2var> app_theta(A.nTerms());
  for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
    app_theta[iTerm] << in_theta;
    mat tmpM = A.Op(iTerm,1);
    tmpM.finalize();
    for(unsigned int k=0; k<q.rank(); k++){
      vec_petsc s;
      vec_petsc mode = in_theta.S(k);
      matVecProd(tmpM, mode, s);
      s.finalize();
      app_theta[iTerm].clear_term_S(k);
      app_theta[iTerm].set_term_S(k, s);
    }
  }

  // orthonormalise them:
  vector<CP2var> tmpW;
  mat_real tmpZ;
  mgs(app_theta, tmpW, tmpZ, tolC);

  // target:
  CP2var tg_theta;
  applyOperator(A, in_theta, tg_theta);

  // find the best approximation of the target:
  vector<double> z_coeff(A.nTerms());
  for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
    dot(tmpW[iTerm], tg_theta, z_coeff[iTerm]);
  }

  // solve the triangular system:
  vector<double> sol_z(A.nTerms());
  for(int i=A.nTerms()-1; i>=0; i--){
    double val = z_coeff[i];
    for(int k=A.nTerms()-1; k>i; k--){
      val -= tmpZ(i,k) * sol_z[k];
    }
    double diagR = tmpZ(i,i);
    if(diagR>1.0e-12){
      val = val / diagR;
    }
    else{
      val = 0.0;
    }
    sol_z[i] = val;
  }

  // assemble the matrix:
  mat Z(q.S(0).size(), q.S(0).size(), PETSC_COMM_WORLD);
  Z.finalize();
  for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
    mat tmpM = A.Op(iTerm,1);
    Z.sum(tmpM, sol_z[iTerm]);
  }
  Z.finalize();

  // define a GMRES solver:
  KSP pz;
  KSPCreate(PETSC_COMM_WORLD,&pz);
  KSPSetOperators(pz, Z.M(), Z.M());
  KSPSetType(pz, KSPGMRES);
  KSPSetTolerances(pz, 1.0e-14, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);
  KSPSetFromOptions(pz);
  KSPSetUp(pz);

  // Apply the preconditioner:
  CP2var Pq; Pq << in;
  for(unsigned int kTerm=0; kTerm<q.rank(); kTerm++){
    vec_petsc x_r = in.R(kTerm);
    vec_petsc new_r(x_r.size(),x_r.comm());
    new_r.finalize();
    KSPSolve(prec, x_r.x(), new_r.x());
    Pq.clear_term_R(kTerm);
    Pq.set_term_R(kTerm, new_r);

    vec_petsc x_s = in.S(kTerm);
    vec_petsc new_s(x_s.size(),x_s.comm());
    new_s.finalize();
    KSPSolve(pz, x_s.x(), new_s.x());
    Pq.clear_term_S(kTerm);
    Pq.set_term_S(kTerm, new_s);
  }

  // update: q <-- Pq:
  q.clear(); q << Pq;
  Pq.clear();

  // res = [A,P]q = APq - PAq:
  CP2var res;

  // free the memory:
  tg_space.clear();
  tg_theta.clear();
  KSPDestroy(&prec);
  KSPDestroy(&pz);
  P.clear();
  Z.clear();
  in_space.clear();
  in_theta.clear();


  for(unsigned int i=0; i<app_space.size(); i++){
    app_space[i].clear();
    tmpQ[i].clear();
    app_theta[i].clear();
    tmpW[i].clear();
  }


  for(unsigned int it=1; it<nIt; it++){

    // evaluate the residual:
    res << in;
    CP2var APq;
    applyOperator(A, q, APq);
    res.axpy(APq, -1.0, true);
    res.round(tolC);
    APq.clear();

    // assemble the space operator:
    // input:
    CP2var in_space; in_space << res;
    for(unsigned int k=0; k<in_space.rank(); k++){
      vec_petsc one_space(nDof_theta, in.S(0).comm());
      one_space.ones();
      one_space.finalize();
      in_space.clear_term_S(k);
      in_space.set_term_S(k, one_space);
    }

    // find the best linear combination of the space operators, such that:
    // A q ~ Pˆ{-1} q, Pˆ{-1} = a_1 A_1 + ... + a_n A_n
    vector<CP2var> app_space(A.nTerms());
    for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
      app_space[iTerm] << in_space;
      mat tmpM = A.Op(iTerm,0);
      tmpM.finalize();
      for(unsigned int k=0; k<in_space.rank(); k++){
        vec_petsc r;
        vec_petsc mode = in_space.R(k);
        matVecProd(tmpM, mode, r);
        r.finalize();
        app_space[iTerm].clear_term_R(k);
        app_space[iTerm].set_term_R(k, r);
      }
    }

    // orthonormalise them:
    vector<CP2var> tmpQ;
    mat_real tmpR;
    mgs(app_space, tmpQ, tmpR, tolC);

    // target:
    CP2var tg_space;
    applyOperator(A, in_space, tg_space);

    // find the best approximation of the target:
    vector<double> p_coeff(A.nTerms());
    for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
      dot(tmpQ[iTerm], tg_space, p_coeff[iTerm]);
    }

    // solve the triangular system:
    vector<double> sol_trg(A.nTerms());
    for(int i=A.nTerms()-1; i>=0; i--){
      double val = p_coeff[i];
      for(int k=A.nTerms()-1; k>i; k--){
        val -= tmpR(i,k) * sol_trg[k];
      }
      double diagR = tmpR(i,i);
      if(diagR>1.0e-12){
        val = val / diagR;
      }
      else{
        val = 0.0;
      }
      sol_trg[i] = val;
    }

    // assemble the matrix:
    mat P(n_rows, n_cols, PETSC_COMM_WORLD);
    P.finalize();
    for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
      mat tmpM = A.Op(iTerm,0);
      P.sum(tmpM, sol_trg[iTerm]);
    }
    P.finalize();

    // define a GMRES solver:
    KSP prec;
    KSPCreate(PETSC_COMM_WORLD,&prec);
    KSPSetOperators(prec, P.M(), P.M());
    KSPSetType(prec, KSPGMRES);
    KSPSetTolerances(prec, 1.0e-14, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);
    KSPSetFromOptions(prec);
    KSPSetUp(prec);


    // define the operators in the parameter direction:
    // input:
    CP2var in_theta; in_theta << res;
    for(unsigned int k=0; k<in_theta.rank(); k++){
      vec_petsc one_theta(nDof_space, in.R(0).comm());
      one_theta.ones();
      one_theta.finalize();
      in_theta.clear_term_R(k);
      in_theta.set_term_R(k, one_theta);
    }

    // find the best linear combination of the space operators, such that:
    // A q ~ Pˆ{-1} q, Pˆ{-1} = a_1 A_1 + ... + a_n A_n
    vector<CP2var> app_theta(A.nTerms());
    for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
      app_theta[iTerm] << in_theta;
      mat tmpM = A.Op(iTerm,1);
      tmpM.finalize();
      for(unsigned int k=0; k<in_theta.rank(); k++){
        vec_petsc s;
        vec_petsc mode = in_theta.S(k);
        matVecProd(tmpM, mode, s);
        s.finalize();
        app_theta[iTerm].clear_term_S(k);
        app_theta[iTerm].set_term_S(k, s);
      }
    }


    // orthonormalise them:
    vector<CP2var> tmpW;
    mat_real tmpZ;
    mgs(app_theta, tmpW, tmpZ, tolC);

    // target:
    CP2var tg_theta;
    applyOperator(A, in_theta, tg_theta);

    // find the best approximation of the target:
    vector<double> z_coeff(A.nTerms());
    for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
      dot(tmpW[iTerm], tg_theta, z_coeff[iTerm]);
    }

    // solve the triangular system:
    vector<double> sol_z(A.nTerms());
    for(int i=A.nTerms()-1; i>=0; i--){
      double val = z_coeff[i];
      for(int k=A.nTerms()-1; k>i; k--){
        val -= tmpZ(i,k) * sol_z[k];
      }
      double diagR = tmpZ(i,i);
      if(diagR>1.0e-12){
        val = val / diagR;
      }
      else{
        val = 0.0;
      }
      sol_z[i] = val;
    }

    // assemble the matrix:
    mat Z(q.S(0).size(), q.S(0).size(), PETSC_COMM_WORLD);
    Z.finalize();
    for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
      mat tmpM = A.Op(iTerm,1);
      Z.sum(tmpM, sol_z[iTerm]);
    }
    Z.finalize();

    // define a GMRES solver:
    KSP pz;
    KSPCreate(PETSC_COMM_WORLD,&pz);
    KSPSetOperators(pz, Z.M(), Z.M());
    KSPSetType(pz, KSPGMRES);
    KSPSetTolerances(pz, 1.0e-14, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);
    KSPSetFromOptions(pz);
    KSPSetUp(pz);


    // Apply the preconditioner:
    CP2var Pq; Pq << res;
    for(unsigned int kTerm=0; kTerm<Pq.rank(); kTerm++){
      vec_petsc x_r = res.R(kTerm);
      vec_petsc new_r(x_r.size(),x_r.comm());
      new_r.finalize();
      KSPSolve(prec, x_r.x(), new_r.x());
      Pq.clear_term_R(kTerm);
      Pq.set_term_R(kTerm, new_r);

      vec_petsc x_s = res.S(kTerm);
      vec_petsc new_s(x_s.size(),x_s.comm());
      new_s.finalize();
      KSPSolve(pz, x_s.x(), new_s.x());
      Pq.clear_term_S(kTerm);
      Pq.set_term_S(kTerm, new_s);
    }


    // update: q <-- Pq:
    q.axpy(Pq, 1.0, true);
    Pq.clear();

    // res = [A,P]q = APq - PAq:
    res.clear();

    // free the memory:
    tg_space.clear();
    tg_theta.clear();
    KSPDestroy(&prec);
    KSPDestroy(&pz);
    P.clear();
    Z.clear();

    in_space.clear();
    in_theta.clear();

    for(unsigned int i=0; i<app_space.size(); i++){
      app_space[i].clear();
      tmpQ[i].clear();
      app_theta[i].clear();
      tmpW[i].clear();
    }

    // end of the loop
  }

  // free the memory:
  /*for(unsigned int i=0; i<app_space.size(); i++){
    app_space[i].clear();
    tmpQ[i].clear();
    app_theta[i].clear();
    tmpW[i].clear();
  }*/
  in.clear();
  res.clear();
}


// a non-static version:
// overwrite q, try to approximate the action of the inverse on q

// auxiliary function: mgs for vector<vec_petsc>: in is overwritten
void mgs(vector<vector<vec_petsc> >& in, vector<vector<vec_petsc> >& out, mat_real& R, double tolC){
  const unsigned int kryl_rank = in.size();
  // orthonormalise the elements: (all elements have already been rounded=>use norm_orth)
  R.init(kryl_rank, kryl_rank); R.zero();
  out.resize(kryl_rank);
  for(unsigned int it=0; it<kryl_rank; it++){
    // copy in:
    out[it].resize(in[it].size());
    for(unsigned int l=0; l<in[it].size(); l++){
      out[it][l] << in[it][l];
    }
    // compute the norm of out:
    double norm_q = 0.0;
    for(unsigned int l=0; l<in[it].size(); l++){
      double val = out[it][l].norm();
      norm_q += val * val;
    }
    norm_q = sqrt(norm_q);

    R.setMatEl(it, it, norm_q);
    if(norm_q>tolC){
      //out[it] *= 1.0/norm_q;
      for(unsigned int l=0; l<in[it].size(); l++){
        out[it][l]*= 1.0/norm_q;
      }

      for(unsigned int k=it+1; k<kryl_rank; k++){
        double c = 0.0;
        //dot(out[it], in[k], c);
        for(unsigned int l=0; l<in[it].size(); l++){
          double toAdd; dot(out[it][l], in[k][l]);
          c += toAdd;
        }

        R.setMatEl(it, k, c);
        //in[k].axpy(out[it], -c, true);
        for(unsigned int l=0; l<in[it].size(); l++){
          in[k][l].axpy(out[it][l], -c);
        }
      }
      // end for
    }
    //endif
  }
  // end for
}






// Arnoldi iteration with third type preconditioner:
/*void Arnoldi(operatorTensor& A, CP2var& residual, vector<CP2var>& proj, vector<CP2var>& rec, mat_real& M, vector<CP2var>& Q, vector<double>& y, double tolC, unsigned int maxIt){
  // communicator:
  MPI_Comm communicator = residual.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // resize and introduce tables:
  Q.resize(maxIt+1);
  y.resize(maxIt+1);
  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  mat_real H(maxIt+1,maxIt);
  H.zero();

  // apply the preconditioner:
  Q[0] << residual;
  applyPreconditioner(proj, rec, M, Q[0]);
  Q[0].round(tolC);
  beta[0] = Q[0].norm();
  Q[0] *= (1.0/beta[0]);

  // Arnoldi iteration:
  if(idProc==0){
    cout << "Starting Arnoldi" << endl;
  }
  unsigned int it = 0;
  bool haveToIter = (it<maxIt);
  while(haveToIter){
    // apply operator:
    applyOperator(A, Q[it], Q[it+1]);
    Q[it+1].round(tolC);

    applyPreconditioner(proj, rec, M, Q[it+1]);
    MPI_Barrier(communicator);

    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = 0.0;
      dot(Q[it+1], Q[sub_it], val);
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val, true);
      Q[it+1].round(tolC);
    }
    //double value = Q[it+1].norm();
    double value = Q[it+1].norm_orth();
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }
    MPI_Barrier(communicator);
  // end while:
  haveToIter = (it<maxIt);
  }
  if(idProc==0){
    cout << "Arnoldi finished." << endl;
  }
}*/

void Arnoldi(operatorTensor& A, CP2var& residual, vector<CP2var>& Q, vector<double>& y, double tolC, unsigned int maxIt, unsigned int nIt){
  // communicator:
  MPI_Comm communicator = residual.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // resize and introduce tables:
  Q.resize(maxIt+1);
  y.resize(maxIt+1);
  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  mat_real H(maxIt+1,maxIt);
  H.zero();

  // apply the preconditioner:
  Q[0] << residual;
  //applyPreconditioner(A, Q[0], nIt, tolC);
  applyPreconditioner(A, Q[0], nIt, tolC);
  Q[0].round(tolC);
  beta[0] = Q[0].norm();
  Q[0] *= (1.0/beta[0]);

  // Arnoldi iteration:
  if(idProc==0){
    cout << "Starting Arnoldi" << endl;
  }
  unsigned int it = 0;
  bool haveToIter = (it<maxIt);
  while(haveToIter){
    // apply operator:
    applyOperator(A, Q[it], Q[it+1]);
    Q[it+1].round(tolC);

    //applyPreconditioner(A, Q[it+1], nIt, tolC);
    applyPreconditioner(A, Q[it+1], nIt, tolC);
    MPI_Barrier(communicator);

    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = 0.0;
      dot(Q[it+1], Q[sub_it], val);
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val, true);
      Q[it+1].round(tolC);
    }
    //double value = Q[it+1].norm();
    double value = Q[it+1].norm_orth();
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }
    MPI_Barrier(communicator);
  // end while:
  haveToIter = (it<maxIt);
  }
  if(idProc==0){
    cout << "Arnoldi finished." << endl;
  }
}


// record in a file the rank evolution, the residual and the computational time, preconditioner type 3:
void Arnoldi_record(string file_name, operatorTensor& A, CP2var& residual, vector<CP2var>& Q, vector<double>& y, double tolC, unsigned int maxIt, unsigned int nIt){
  string file_res = file_name + "_res.txt";
  string file_time = file_name + "_time.txt";
  string file_rank = file_name + "_rank.txt";

  // communicator:
  MPI_Comm communicator = residual.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // open files:
  ofstream out_res (file_res, std::ofstream::out);
  ofstream out_time(file_time, std::ofstream::out);
  ofstream out_rank(file_rank, std::ofstream::out);

  // resize and introduce tables:
  Q.resize(maxIt+1);
  y.resize(maxIt+1);
  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  mat_real H(maxIt+1,maxIt);
  H.zero();

  // apply the preconditioner:
  Q[0] << residual;
  applyPreconditioner(A, Q[0], nIt, tolC);
  Q[0].round(tolC);
  beta[0] = Q[0].norm();
  Q[0] *= (1.0/beta[0]);

  // Arnoldi iteration:
  if(idProc==0){
    cout << "Starting Arnoldi" << endl;
  }
  unsigned int it = 0;
  bool haveToIter = (it<maxIt);
  while(haveToIter){

    // starting chrono:
    auto start = std::chrono::high_resolution_clock::now();

    // apply operator:
    applyOperator(A, Q[it], Q[it+1]);
    Q[it+1].round(tolC);

    applyPreconditioner(A, Q[it+1], nIt, tolC);
    MPI_Barrier(communicator);

    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = 0.0;
      dot(Q[it+1], Q[sub_it], val);
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val, true);
      Q[it+1].round(tolC);
    }
    //double value = Q[it+1].norm();
    double value = Q[it+1].norm_orth();
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }

    // stop chrono:
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

    MPI_Barrier(communicator);
    // eval residual at this iteration:
    unsigned int tmp_sol_rank;
    double residual_norm = eval_residual_norm(residual, A, Q, y, it, tolC, tmp_sol_rank);
    if(idProc == 0){
      out_time << duration.count()/1000000.0 << endl;
      out_res << residual_norm << endl;
      out_rank << tmp_sol_rank << endl;
    }
    MPI_Barrier(communicator);

  // end while:
  haveToIter = (it<maxIt);
  }
  if(idProc==0){
    cout << "Arnoldi finished." << endl;
  }
  out_res.close();
  out_time.close();
  out_rank.close();
}


// ----------------------------------------------------------------------------
// PRECONDITIONER type 3, constant matrices, efficient coding:
// ----------------------------------------------------------------------------
void applyPreconditioner(operatorTensor& A, KSP& p_space, KSP& p_theta, CP2var& q, unsigned int nIt, double tolC){
  const unsigned int nDof_space = q.nDof_var(0);
  const unsigned int nDof_theta = q.nDof_var(1);

  // copy the input: (in general in the call *q = *out)
  CP2var in; in << q;

  // Apply the preconditioner:
  CP2var Pq; Pq << in;
  for(unsigned int kTerm=0; kTerm<q.rank(); kTerm++){
    vec_petsc x_r = in.R(kTerm);
    vec_petsc new_r(x_r.size(),x_r.comm());
    new_r.finalize();
    KSPSolve(p_space, x_r.x(), new_r.x());
    Pq.clear_term_R(kTerm);
    Pq.set_term_R(kTerm, new_r);

    vec_petsc x_s = in.S(kTerm);
    vec_petsc new_s(x_s.size(),x_s.comm());
    new_s.finalize();
    KSPSolve(p_theta, x_s.x(), new_s.x());
    Pq.clear_term_S(kTerm);
    Pq.set_term_S(kTerm, new_s);
  }

  // update: q <-- Pq:
  q.clear(); q << Pq;
  Pq.clear();

  if(nIt>1){
    q.round(tolC);
  }
  CP2var res;

  // start iterating:
  for(unsigned int it=1; it<nIt; it++){

    // evaluate the residual:
    res << in;
    CP2var APq;
    applyOperator(A, q, APq);
    res.axpy(APq, -1.0, true);
    res.round(tolC);
    APq.clear();

    // apply the preconditioner:
    CP2var Pq; Pq << res;
    for(unsigned int kTerm=0; kTerm<Pq.rank(); kTerm++){
      vec_petsc x_r = res.R(kTerm);
      vec_petsc new_r(x_r.size(),x_r.comm());
      new_r.finalize();
      KSPSolve(p_space, x_r.x(), new_r.x());
      Pq.clear_term_R(kTerm);
      Pq.set_term_R(kTerm, new_r);

      vec_petsc x_s = res.S(kTerm);
      vec_petsc new_s(x_s.size(),x_s.comm());
      new_s.finalize();
      KSPSolve(p_theta, x_s.x(), new_s.x());
      Pq.clear_term_S(kTerm);
      Pq.set_term_S(kTerm, new_s);
    }

    // update: q <-- Pq:
    q.axpy(Pq, 1.0, true);
    Pq.clear();

    if(it<nIt-1){
      q.round(tolC);
    }

    // res = [A,P]q = APq - PAq:
    res.clear();
  }

  // free the memory:
  in.clear();
  res.clear();
}


// Arnoldi iterations:
void Arnoldi(operatorTensor& A, CP2var& residual, KSP& p_space, KSP& p_theta, unsigned int nIt, vector<CP2var>& Q, vector<double>& y, double tolC, unsigned int maxIt){
  // communicator:
  MPI_Comm communicator = residual.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // resize and introduce tables:
  Q.resize(maxIt+1);
  y.resize(maxIt+1);
  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  mat_real H(maxIt+1,maxIt);
  H.zero();

  // apply the preconditioner:
  Q[0] << residual;
  //applyPreconditioner(A, Q[0], nIt, tolC);
  applyPreconditioner(A, p_space, p_theta, Q[0], nIt, tolC);
  Q[0].round(tolC);
  beta[0] = Q[0].norm();
  Q[0] *= (1.0/beta[0]);

  // Arnoldi iteration:
  if(idProc==0){
    cout << "Starting Arnoldi" << endl;
  }
  unsigned int it = 0;
  bool haveToIter = (it<maxIt);
  while(haveToIter){
    // apply operator:
    applyOperator(A, Q[it], Q[it+1]);
    Q[it+1].round(tolC);

    //applyPreconditioner(A, Q[it+1], nIt, tolC);
    applyPreconditioner(A, p_space, p_theta, Q[it+1], nIt, tolC);
    MPI_Barrier(communicator);

    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = 0.0;
      dot(Q[it+1], Q[sub_it], val);
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val, true);
      Q[it+1].round(tolC);
    }
    //double value = Q[it+1].norm();
    double value = Q[it+1].norm_orth();
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }
    MPI_Barrier(communicator);
  // end while:
  haveToIter = (it<maxIt);
  }
  if(idProc==0){
    cout << "Arnoldi finished." << endl;
  }
}


// record the residual, the computational time, the solution ranks
void Arnoldi_record(string file_name, operatorTensor& A, CP2var& residual, KSP& p_space, KSP& p_theta, unsigned int nIt, vector<CP2var>& Q, vector<double>& y, double tolC, unsigned int maxIt){
  string file_res = file_name + "_res.txt";
  string file_time = file_name + "_time.txt";
  string file_rank = file_name + "_rank.txt";

  // communicator:
  MPI_Comm communicator = residual.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // open files:
  ofstream out_res (file_res, std::ofstream::out);
  ofstream out_time(file_time, std::ofstream::out);
  ofstream out_rank(file_rank, std::ofstream::out);

  // resize and introduce tables:
  Q.resize(maxIt+1);
  y.resize(maxIt+1);
  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  mat_real H(maxIt+1,maxIt);
  H.zero();

  // apply the preconditioner:
  Q[0] << residual;
  applyPreconditioner(A, p_space, p_theta, Q[0], nIt, tolC);
  Q[0].round(tolC);
  beta[0] = Q[0].norm();
  Q[0] *= (1.0/beta[0]);

  // Arnoldi iteration:
  if(idProc==0){
    cout << "Starting Arnoldi" << endl;
  }
  unsigned int it = 0;
  bool haveToIter = (it<maxIt);
  while(haveToIter){

    // starting chrono:
    auto start = std::chrono::high_resolution_clock::now();

    // apply operator:
    applyOperator(A, Q[it], Q[it+1]);
    Q[it+1].round(tolC);

    applyPreconditioner(A, p_space, p_theta, Q[it+1], nIt, tolC);
    MPI_Barrier(communicator);

    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = 0.0;
      dot(Q[it+1], Q[sub_it], val);
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val, true);
      Q[it+1].round(tolC);
    }
    //double value = Q[it+1].norm();
    double value = Q[it+1].norm_orth();
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }

    // stop chrono:
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

    MPI_Barrier(communicator);
    // eval residual at this iteration:
    unsigned int tmp_sol_rank;
    double residual_norm = eval_residual_norm(residual, A, Q, y, it, tolC, tmp_sol_rank);
    if(idProc == 0){
      out_time << duration.count()/1000000.0 << endl;
      out_res << residual_norm << endl;
      out_rank << tmp_sol_rank << endl;
    }
    MPI_Barrier(communicator);

  // end while:
  haveToIter = (it<maxIt);
  }
  if(idProc==0){
    cout << "Arnoldi finished." << endl;
  }
  out_res.close();
  out_time.close();
  out_rank.close();
}




// ----------------------------------------------------------------------------
// PRECONDITIONER type 3, dynamical updated matrices:
// ----------------------------------------------------------------------------


// auxiliary functions:

// 1 - given a matrix, return the KSP object, initialised
KSP setSolver(mat& A, string FLAG = "KSPPREONLY"){
  KSP out;
  KSPCreate(PETSC_COMM_WORLD, &out);
  KSPSetOperators(out, A.M(), A.M());
  if(FLAG == "KSPREONLY"){
    KSPSetType(out, KSPPREONLY);
  }
  else{
    KSPSetType(out, KSPGMRES);
    KSPSetTolerances(out, 1.0e-14, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);
  }
  KSPSetFromOptions(out);
  return out;
}

// 2 - given an operator tensor A, compute its block-transposed (transposed of each mat)
operatorTensor transpose(operatorTensor& A){
  const unsigned int n_terms = A.nTerms();
  const unsigned int n_var = A.nVar();
  vector<vector<mat> > op_table(n_terms);
  for(unsigned int iTerm=0; iTerm<n_terms; iTerm++){
    op_table[iTerm].resize(n_var);
    for(unsigned int iVar=0; iVar<n_var; iVar++){
      mat A_i = A.Op(iTerm,iVar);
      unsigned int n_rows = A_i.nRows();
      unsigned int n_cols = A_i.nCols();
      mat B_i(n_rows,n_cols, PETSC_COMM_WORLD);
      Mat B_i_Mat;
      MatTranspose(A_i.M(), MAT_INITIAL_MATRIX, &B_i_Mat);
      B_i.setMatrix(B_i_Mat);
      op_table[iTerm][iVar] = B_i;
    }
  }
  operatorTensor out(op_table);
  return out;
}

// transpose a matrix, in_place:
mat transpose(mat& A){
  unsigned int n_rows = A.nRows();
  unsigned int n_cols = A.nCols();
  mat B(n_rows,n_cols, PETSC_COMM_WORLD);
  Mat B_Mat;
  MatTranspose(A.M(), MAT_INITIAL_MATRIX, &B_Mat);
  B.setMatrix(B_Mat);
  return B;
}

// substepping:
void update_operators_and_sol(operatorTensor& A, operatorTensor& At, mat& P_x, mat& P_t, mat& P_x_up, mat& P_t_up, CP2var& q, CP2var& in, double step_grad, double tolC){
  // Build the preconditioners for the input matrices P_x, P_t:
  KSP p_space = setSolver(P_x);
  KSP p_theta = setSolver(P_t);
  // compute the preconditioned residual:

  CP2var Pq; Pq << in;
  for(unsigned int kTerm=0; kTerm<q.rank(); kTerm++){
    vec_petsc x_r = in.R(kTerm);
    vec_petsc new_r(x_r.size(),x_r.comm());
    new_r.finalize();
    KSPSolve(p_space, x_r.x(), new_r.x());
    Pq.clear_term_R(kTerm);
    Pq.set_term_R(kTerm, new_r);

    vec_petsc x_s = in.S(kTerm);
    vec_petsc new_s(x_s.size(),x_s.comm());
    new_s.finalize();
    KSPSolve(p_theta, x_s.x(), new_s.x());
    Pq.clear_term_S(kTerm);
    Pq.set_term_S(kTerm, new_s);
  }
  // q = w = Pq;
  q.clear(); q << Pq;

  // w = Pq at this stage:
  CP2var Aw; applyOperator(A, q, Aw);
  CP2var d; d << in;
  d.axpy(Aw, -1.0, true);
  Aw.clear();
  d.round(tolC);
  double norm_disc = d.norm_orth();
  //cout << "Discrepancy = " << norm_disc << endl;
  // free the memory:
  Pq.clear();

  // lam_rhs = -A^T d
  CP2var lam_rhs; applyOperator(At, d, lam_rhs);
  lam_rhs.round(tolC);
  lam_rhs *= -1.0;

  // preconditioning with the transposed:
  mat P_x_T = transpose(P_x);
  mat P_t_T = transpose(P_t);
  KSP p_space_T = setSolver(P_x_T);
  KSP p_theta_T = setSolver(P_t_T);

  CP2var lambda; lambda << lam_rhs;
  for(unsigned int kTerm=0; kTerm<lam_rhs.rank(); kTerm++){
    vec_petsc x_r = lam_rhs.R(kTerm);
    vec_petsc new_r(x_r.size(),x_r.comm());
    new_r.finalize();
    KSPSolve(p_space_T, x_r.x(), new_r.x());
    lambda.clear_term_R(kTerm);
    lambda.set_term_R(kTerm, new_r);

    vec_petsc x_s = lam_rhs.S(kTerm);
    vec_petsc new_s(x_s.size(),x_s.comm());
    new_s.finalize();
    KSPSolve(p_theta_T, x_s.x(), new_s.x());
    lambda.clear_term_S(kTerm);
    lambda.set_term_S(kTerm, new_s);
  }

  // free the KSP objets:
  KSPDestroy(&p_space);
  KSPDestroy(&p_theta);
  KSPDestroy(&p_space_T);
  KSPDestroy(&p_theta_T);
  P_x_T.clear();
  P_t_T.clear();
  lam_rhs.clear();
  d.clear();

  // beta:
  // M_{mn}^{(l)} = lam^T A^{(l)}
  vector<vector<vector<double> > > M_tab(A.nTerms());
  for(unsigned int l=0; l<A.nTerms(); l++){
    mat A_l = At.Op(l,0);
    M_tab[l].resize(lambda.rank());
    for(unsigned int k=0; k<lambda.rank(); k++){
      vec_petsc r_lam = lambda.R(k);
      M_tab[l][k].resize(q.rank());
      vec_petsc temp = A_l * r_lam;
      for(unsigned int p=0; p<q.rank(); p++){
        vec_petsc q_R = q.R(p);
        double entry = dot(q_R, temp);
        M_tab[l][k][p] = entry;
      }
      temp.clear();
    }
  }

  vector<double> minus_up_beta(A.nTerms());
  for(unsigned int l=0; l<A.nTerms(); l++){
    minus_up_beta[l] = 0.0;
    for(unsigned int k=0; k<lambda.rank(); k++){
      vec_petsc lam_S = lambda.S(k);
      vec_petsc temp = P_t * lam_S;
      for(unsigned int p=0; p<q.rank(); p++){
        vec_petsc q_S = q.S(p);
        double entry = dot(q_S,temp);
        minus_up_beta[l] += M_tab[l][k][p] * entry;
      }
      temp.clear();
    }
  }

  // update now the expression of P_x:
  for(unsigned int l=0; l<A.nTerms(); l++){
    mat toAdd = A.Op(l,0);
    double tmp_coeff = step_grad * minus_up_beta[l];
    P_x.sum(toAdd, tmp_coeff);
    P_x_up.sum(toAdd, tmp_coeff);
  }
  P_x.finalize();

  // gamma:
  vector<vector<vector<double> > > N_tab(A.nTerms());
  for(unsigned int l=0; l<A.nTerms(); l++){
    mat D_l = At.Op(l,1);
    N_tab[l].resize(lambda.rank());
    for(unsigned int k=0; k<lambda.rank(); k++){
      vec_petsc lam_s = lambda.S(k);
      N_tab[l][k].resize(q.rank());
      vec_petsc temp = D_l * lam_s;
      for(unsigned int p=0; p<q.rank(); p++){
        vec_petsc q_S = q.S(p);
        double entry = dot(q_S, temp);
        N_tab[l][k][p] = entry;
      }
      temp.clear();
    }
  }

  vector<double> minus_up_gamma(A.nTerms());
  for(unsigned int l=0; l<A.nTerms(); l++){
    minus_up_gamma[l] = 0.0;
    for(unsigned int k=0; k<lambda.rank(); k++){
      vec_petsc lam_R = lambda.R(k);
      vec_petsc temp = P_x * lam_R;
      for(unsigned int p=0; p<q.rank(); p++){
        vec_petsc q_R = q.R(p);
        double entry = dot(q_R,temp);
        minus_up_gamma[l] += N_tab[l][k][p] * entry;
      }
      temp.clear();
    }
  }

  // update now the expression of P_t:
  for(unsigned int l=0; l<A.nTerms(); l++){
    mat toAdd = A.Op(l,1);
    double tmp_coeff = step_grad * minus_up_gamma[l];
    P_t.sum(toAdd, tmp_coeff);
    P_t_up.sum(toAdd, tmp_coeff);
  }
  P_t.finalize();

  // free the memory:
  M_tab.clear();
  N_tab.clear();
  lambda.clear();
}

// dynamical matrices fixed-point preconditioner:
void applyPreconditioner_dynamic(operatorTensor& A, mat& P_x, mat& P_t, CP2var& q, unsigned int nIt, double step_grad, double tolC){
  int numSubIt = 5;
  bool retrieve = false;
  int idProc;
  MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

  // keep track of the changes to go back:
  mat P_x_update(P_x.nRows(), P_x.nCols(), PETSC_COMM_WORLD);
  P_x_update.finalize();

  mat P_t_update(P_t.nRows(), P_t.nCols(), PETSC_COMM_WORLD);
  P_t_update.finalize();


  // transpose the operator:
  operatorTensor At = transpose(A);

  //Z^{(0)} = q, R^{(0)} = 0
  CP2var in; in << q;

  update_operators_and_sol(A, At, P_x, P_t, P_x_update, P_t_update, q, in, step_grad, tolC);

  // sub-iterations:
  for(unsigned int subit=0; subit<numSubIt; subit++){
    update_operators_and_sol(A, At, P_x, P_t, P_x_update, P_t_update, q, in, step_grad, tolC);
  }
  // subiteration ended:

  CP2var z; z << in;
  CP2var Aq; applyOperator(A, q, Aq);
  z.axpy(Aq, -1.0, true);
  Aq.clear();
  z.round(tolC);
  double z_norm = z.norm_orth();
  if(idProc==0){
    cout << "Norm of z_0 = " << z_norm << endl;
  }

  // iteration:
  for(unsigned int it=0; it<nIt; it++){
    CP2var update; update << z;
    update_operators_and_sol(A, At, P_x, P_t, P_x_update, P_t_update, update, z, step_grad, tolC);
    // sub-iterations:
    for(unsigned int subit=0; subit<numSubIt; subit++){
      update_operators_and_sol(A, At, P_x, P_t, P_x_update, P_t_update, update, z, step_grad, tolC);
    }

    // update q:
    q.axpy(update, 0.5, true);
    update.clear();
    z.clear();

    // compute the novel discrepancy:
    z << in;
    CP2var Aq; applyOperator(A, q, Aq);
    z.axpy(Aq, -1.0, true);
    Aq.clear();
    z.round(tolC);
    double z_norm = z.norm_orth();
    if(idProc==0){
      cout << "Norm of z = " << z_norm << endl;
    }

  }
  // end of iteration:
  z.clear();
  if(idProc==0){
    cout << endl;
  }

  if(retrieve){
    P_x.sum(P_x_update, -1.0);
    P_t.sum(P_t_update, -1.0);
  }

  P_x_update.clear();
  P_t_update.clear();

  // output has to be an update of q
  At.clear();
}



// Arnoldi iteration with dynamical preconditioner:
void Arnoldi(operatorTensor& A, CP2var& residual, mat& P_x, mat& P_t, unsigned int nIt, double step_grad, vector<CP2var>& Q, vector<double>& y, double tolC, unsigned int maxIt){
  // communicator:
  MPI_Comm communicator = residual.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // resize and introduce tables:
  Q.resize(maxIt+1);
  y.resize(maxIt+1);
  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  mat_real H(maxIt+1,maxIt);
  H.zero();

  // apply the preconditioner:
  Q[0] << residual;
  applyPreconditioner_dynamic(A, P_x, P_t, Q[0], nIt, step_grad, tolC);
  Q[0].round(tolC);
  beta[0] = Q[0].norm();
  Q[0] *= (1.0/beta[0]);

  // Arnoldi iteration:
  if(idProc==0){
    cout << "Starting Arnoldi" << endl;
  }
  unsigned int it = 0;
  bool haveToIter = (it<maxIt);
  while(haveToIter){
    // apply operator:
    applyOperator(A, Q[it], Q[it+1]);
    Q[it+1].round(tolC);

    applyPreconditioner_dynamic(A, P_x, P_t, Q[it+1], nIt, step_grad, tolC);
    MPI_Barrier(communicator);

    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = 0.0;
      dot(Q[it+1], Q[sub_it], val);
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val, true);
      Q[it+1].round(tolC);
    }
    //double value = Q[it+1].norm();
    double value = Q[it+1].norm_orth();
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }
    MPI_Barrier(communicator);
  // end while:
  haveToIter = (it<maxIt);
  }
  if(idProc==0){
    cout << "Arnoldi finished." << endl;
  }
}













// NO PRECONDITIONER:

// Arnoldi Record without preconditioning:
void Arnoldi_record(string file_name, operatorTensor& A, CP2var& residual, vector<CP2var>& Q, vector<double>& y, double tolC, unsigned int maxIt){
  string file_res = file_name + "_res.txt";
  string file_time = file_name + "_time.txt";
  string file_rank = file_name + "_rank.txt";

  // communicator:
  MPI_Comm communicator = residual.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // open files:
  ofstream out_res (file_res, std::ofstream::out);
  ofstream out_time(file_time, std::ofstream::out);
  ofstream out_rank(file_rank, std::ofstream::out);

  // resize and introduce tables:
  Q.resize(maxIt+1);
  y.resize(maxIt+1);
  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  mat_real H(maxIt+1,maxIt);
  H.zero();

  // apply the preconditioner:
  Q[0] << residual;
  Q[0].round(tolC);
  beta[0] = Q[0].norm();
  Q[0] *= (1.0/beta[0]);

  // Arnoldi iteration:
  if(idProc==0){
    cout << "Starting Arnoldi" << endl;
  }
  unsigned int it = 0;
  bool haveToIter = (it<maxIt);
  while(haveToIter){

    // starting chrono:
    auto start = std::chrono::high_resolution_clock::now();

    // apply operator:
    applyOperator(A, Q[it], Q[it+1]);
    Q[it+1].round(tolC);

    MPI_Barrier(communicator);

    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = 0.0;
      dot(Q[it+1], Q[sub_it], val);
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val, true);
      Q[it+1].round(tolC);
    }
    //double value = Q[it+1].norm();
    double value = Q[it+1].norm_orth();
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }

    // stop chrono:
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

    MPI_Barrier(communicator);
    // eval residual at this iteration:
    unsigned int tmp_sol_rank;
    double residual_norm = eval_residual_norm(residual, A, Q, y, it, tolC, tmp_sol_rank);
    if(idProc == 0){
      out_time << duration.count()/1000000.0 << endl;
      out_res << residual_norm << endl;
      out_rank << tmp_sol_rank << endl;
    }
    MPI_Barrier(communicator);

  // end while:
  haveToIter = (it<maxIt);
  }
  if(idProc==0){
    cout << "Arnoldi finished." << endl;
  }
  out_res.close();
  out_time.close();
  out_rank.close();
}




// ---------------------------------------------------------------------------------------------------------
// Parametric domains
// ---------------------------------------------------------------------------------------------------------
/*
// auxiliary: pointwise multiplication of vec_petsc:
void pointWiseMult(vec_petsc& v1, vec_petsc& v2, vec_petsc& out){
  out.init(v1.size(), v1.comm());
  out.finalize();
  VecPointwiseMult(out.x(), v1.x() , v2.x());
}

// !!! version with operator RANK == 1 and factorisation of the char_fun !!!
void applyOperator(operatorTensor& A, CP2var& in, CP2var& char_fact, CP2var& out){
  double tol_round = 1.0e-8;
  unsigned int in_rank = in.rank();
  unsigned int N_x = in.R(0).size();
  unsigned int N_t = in.S(0).size();

  out.init({N_x,N_t});

  for(unsigned int q=0; q<char_fact.rank(); q++){
    vec_petsc q_x = char_fact.R(q);
    vec_petsc q_t = char_fact.S(q);

    // copy the modes of [in], perform the pointwise multiplication, apply the operator
    mat A_r = A.Op(0,0);
    mat A_s = A.Op(0,1);
    vector<vec_petsc>R_tab(in_rank);
    vector<vec_petsc>S_tab(in_rank);
    for(unsigned int k=0; k<in_rank; k++){
      vec_petsc r_mult(N_x, in.R(k).comm());
      vec_petsc r = in.R(k);
      pointWiseMult(r, q_x, r_mult);
      matVecProd(A_r, r_mult, R_tab[k]);

      vec_petsc s_mult(N_t, in.S(k).comm());
      vec_petsc s = in.S(k);
      pointWiseMult(s, q_t, s_mult);
      matVecProd(A_s, s_mult, S_tab[k]);
    }
    // CP to add:
    CP2var toAdd(R_tab, S_tab);
    // sum to the CP tensor:
    out.axpy(toAdd, 1.0, true);
    out.round(tol_round);
    toAdd.clear();
  }
}

// !!! version with operator RANK == 1 and char_fun !!!
void applyOperator(operatorTensor& A, CP2var& in, char_fun& CharSet, vector<vec_petsc>& theta_list, double& tol_round, CP2var& out){
  unsigned int in_rank = in.rank();
  unsigned int N_x = in.R(0).size();
  unsigned int N_t = in.S(0).size();
  mat A_r = A.Op(0,0);
  mat A_s = A.Op(0,1);

  // 1 -- start by building and rounding the CP on the dofs type "one" (intersection of all the domains):
  cout << "Intersection dofs" << endl;
  vector<vec_petsc> R_tab(in_rank);
  vector<vec_petsc> S_tab(in_rank);
  vector<double> coeff_tab(in_rank);
  vector<unsigned int> one_list = CharSet.one_dofs();
  for(unsigned int k=0; k<in_rank; k++){
    S_tab[k] << in.S(k);
    coeff_tab[k] = in.coeffs(k);
    vec_petsc tmp(N_x, in.R(0).comm());
    for(unsigned int i=0; i<one_list.size(); i++){
      unsigned int i_dof = one_list[i];
      double mode_val = in.R(k).getVecEl(i_dof);
      tmp.setVecEl(i_dof, mode_val);
    }
    tmp.finalize();
    // multiply by K:
    R_tab[k].init(N_x, in.R(0).comm());
    R_tab[k].finalize();
    matVecProd(A_r, tmp, R_tab[k]);
  }
  out.init({N_x,N_t});
  out.set_terms(R_tab, S_tab);
  out.set_coeffs(coeff_tab);
  out.round(tol_round);


  // 2 -- union - intersection dofs:

  // test compression for the union - intersection:
  cout << "Union - Intersection" << endl;
  vector<unsigned int> t_list = CharSet.active_dofs();
  unsigned int cc = 0;
  vector<vec_petsc> unfolding(N_t);
  for(unsigned int i=0; i<N_t; i++){
    unfolding[i].init(t_list.size(), PETSC_COMM_WORLD);
  }

  for(unsigned int i=0; i<t_list.size(); i++){
    unsigned int j_dof = t_list[i];
    vec_petsc s_j = CharSet.sigmas(i);
    for(unsigned int k=0; k<in_rank; k++){
      double weight = in.R(k).getVecEl(j_dof);
      vec_petsc s_k = in.S(k);
      vec_petsc s(N_t, in.S(0).comm()); s.finalize();
      pointWiseMult(s_j, s_k, s);

      for(unsigned int j=0; j<N_t; j++){
        double current = unfolding[j].getVecEl(i);
        double toAdd = weight * s.getVecEl(j);
        current += toAdd;
        unfolding[j].setVecEl(i, current);
      }
      s.clear();
      cc+= 1;
    }
  }
  for(unsigned int i=0; i<N_t; i++){
    unfolding[i].finalize();
  }

  // compress and then extend:
  cout << "Compression" << endl;
  vector<vec_petsc> I_t(N_t);
  for(unsigned int i=0; i<N_t; i++){
    I_t[i].init(N_t, PETSC_COMM_WORLD);
    I_t[i].setVecEl(i, 1.0);
    I_t[i].finalize();
  }
  CP2var tmp(unfolding, I_t);
  tmp.round(tol_round);
  cout << "Union-Intersection rank = " << tmp.rank() << endl;


  cout << "Extend" << endl;

  unsigned int n_modes = tmp.rank();
  vector<vec_petsc> ext_modes(n_modes);
  vector<vec_petsc> theta_fun(n_modes);
  for(unsigned int i_t=0; i_t<n_modes; i_t++){
    theta_fun[i_t] << tmp.S(i_t);
    ext_modes[i_t].init(N_x, in.R(0).comm());
    vec_petsc mode = tmp.R(i_t);
    for(unsigned int i=0; i<t_list.size(); i++){
      unsigned int j_dof = t_list[i];
      double val = mode.getVecEl(i);
      ext_modes[i_t].setVecEl(j_dof, val);
    }
    ext_modes[i_t].finalize();
  }
  tmp.clear();


  cout << "extension done... multiply" << endl;

  vector<vec_petsc> result_active(n_modes);
  for(unsigned int i_t=0; i_t<n_modes; i_t++){
    result_active[i_t].init(N_x, in.R(0).comm());
    result_active[i_t].finalize();
    matVecProd(A_r, ext_modes[i_t], result_active[i_t]);
    ext_modes[i_t].clear();
  }

  CP2var toAdd(result_active, theta_fun);

  cout << "done." << endl;


  cout << "Adding: " << endl;
  out.axpy(toAdd, 1.0, true);

  toAdd.clear();
  out.round(tol_round);
  cout << "rank = " << out.rank() << endl;
}

// apply preconditioner:
void applyPreconditioner(KSP& prec, CP2var& in, char_fun& CharSet, vector<vec_petsc>& theta_list){
  unsigned int in_rank = in.rank();
  unsigned int N_x = in.R(0).size();
  unsigned int N_t = in.S(0).size();

  // reconstruct the solutions:
  vector<vec_petsc> reco(N_t);
  for(unsigned int j=0; j<N_t; j++){
    reco[j].init(N_x, PETSC_COMM_WORLD);
    reco[j].finalize();
    for(unsigned int k=0; k<in_rank; k++){
      double alpha = in.S(k).getVecEl(j);
      alpha *= in.coeffs(k);
      vec_petsc r = in.R(k);
      reco[j].axpy(r, alpha);
    }
  }

  // multiply by the charfun:
  for(unsigned int j=0; j<N_t; j++){
    vector<double> theta_pt(theta_list.size());
    for(unsigned int d=0; d<theta_list.size(); d++){
      theta_pt[d] = theta_list[d].getVecEl(j);
    }
    for(unsigned int i_dof=0; i_dof<N_x; i_dof++){
      double val = reco[j].getVecEl(i_dof);
      double char_val = CharSet.eval(i_dof, theta_pt);
      val  *= char_val;
      reco[j].setVecEl(i_dof, val);
    }
  }

  // apply the preconditioner:
  vector<vec_petsc> tmp_sol(N_t);
  for(unsigned int j=0; j<N_t; j++){
    tmp_sol[j].init(N_x, PETSC_COMM_WORLD);
    KSPSolve(prec, reco[j].x(), tmp_sol[j].x());
    reco[j].clear();
  }

  // multiply by the charcateristic function:
  for(unsigned int j=0; j<N_t; j++){
    vector<double> theta_pt(theta_list.size());
    for(unsigned int d=0; d<theta_list.size(); d++){
      theta_pt[d] = theta_list[d].getVecEl(j);
    }
    for(unsigned int i_dof=0; i_dof<N_x; i_dof++){
      double val = tmp_sol[j].getVecEl(i_dof);
      double char_val = CharSet.eval(i_dof, theta_pt);
      val  *= char_val;
      tmp_sol[j].setVecEl(i_dof, val);
    }
  }


  // assemble out:
  vector<vec_petsc> eye_modes(N_t);
  vector<double> c_vec(N_t);
  for(unsigned int j=0; j<N_t; j++){
    c_vec[j] = 1.0;
    eye_modes[j].init(N_t, PETSC_COMM_WORLD);
    eye_modes[j].setVecEl(N_t, 1.0);
    eye_modes[j].finalize();
  }

  in.clear();
  in.set_rank(N_t);
  in.set_terms(tmp_sol, eye_modes);
  in.set_coeffs(c_vec);
}


// Arnoldi with preconditioner type 1:
// progressive compression tolerance tol_round.
void Arnoldi(operatorTensor& A, CP2var& residual, KSP& prec, char_fun& CharSet, vector<vec_petsc>& theta_list, vector<CP2var>& Q, vector<double>& y, double tolC, unsigned int maxIt){
  // communicator:
  MPI_Comm communicator = residual.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // resize and introduce tables:
  Q.resize(maxIt+1);
  y.resize(maxIt+1);
  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  mat_real H(maxIt+1,maxIt);
  H.zero();

  // apply the preconditioner:
  Q[0] << residual;
  applyPreconditioner(prec, Q[0], CharSet, theta_list);
  beta[0] = Q[0].norm();
  Q[0] *= (1.0/beta[0]);
  if(idProc==0){
    cout << "Initial residual norm = " << beta[0] << endl;
  }

  // Arnoldi iteration:
  if(idProc==0){
    cout << "Starting Arnoldi" << endl;
  }
  unsigned int it = 0;
  bool haveToIter = (it<maxIt);
  double tol_round = 0.3;
  while(haveToIter){
    // apply operator:
    //applyOperator(A, Q[it], CharSet, theta_list, tol_round, Q[it+1]);
    applyOperator(A, Q[it], Q[it+1]);
    Q[it+1].round(tolC);

    applyPreconditioner(prec, Q[it+1], CharSet, theta_list);
    MPI_Barrier(communicator);

    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = 0.0;
      dot(Q[it+1], Q[sub_it], val);
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val, true);
      Q[it+1].round(tolC);
    }
    //double value = Q[it+1].norm();
    double value = Q[it+1].norm_orth();
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }
    MPI_Barrier(communicator);
  // end while:
  tol_round = tol_round/1.1;
  if(tol_round<tolC){tol_round=tolC;}

  haveToIter = (it<maxIt);
  }
  if(idProc==0){
    cout << "Arnoldi finished." << endl;
  }
}
*/














// ---------------------------------------------------------------------------------------------------------
// Tucker2var FUNCTIONS
// ---------------------------------------------------------------------------------------------------------

void applyOperator(operatorTensor&A, Tucker2var& x, Tucker2var& b){
  assert(A.nTerms()>0);
  assert(A.nVar()==2);
  vector<unsigned int> resPerVar(2);
  resPerVar[0] = A.Op(0,0).nRows();
  resPerVar[1] = A.Op(0,1).nRows();

  unsigned int rank_R = x.rank(0) * A.nTerms();
  unsigned int rank_S = x.rank(1) * A.nTerms();
  if( (rank_R == 0) || (rank_S==0) ){
    b.zero();
  }
  else{
    vector<vec_petsc> R_tab(rank_R);
    vector<vec_petsc> S_tab(rank_S);

    unsigned int cc_r = 0;
    unsigned int cc_s = 0;
    for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
      mat A_r = A.Op(iTerm,0);
      mat A_s = A.Op(iTerm,1);
      // filling R_tab:
      for(unsigned int i=0; i<x.rank(0); i++){
        vec_petsc x_r = x.R(i);
        matVecProd(A_r, x_r, R_tab[cc_r]);
        cc_r += 1;
      }
      // filling S_tab:
      for(unsigned int j=0; j<x.rank(1); j++){
        vec_petsc x_s = x.S(j);
        matVecProd(A_s, x_s, S_tab[cc_s]);
        cc_s += 1;
      }
    }

    b.set_terms(R_tab,S_tab); // this sets the ranks as well: b.set_rank(rank_R,rank_S);
    b.init_core(rank_R,rank_S); // the core elements are set to zero as well;
    // filling the core with copy values:
    for(unsigned int iTerm=0; iTerm<A.nTerms(); iTerm++){
      for(unsigned int i=0; i<x.rank(0); i++){
        unsigned int iRow = iTerm*x.rank(0)+i;
        for(unsigned int j=0; j<x.rank(1); j++){
          unsigned int jCol = iTerm*x.rank(1)+j;
          double val = x.A(i,j);
          b.setCoreEl(iRow,jCol,val);
        }
      }
    }
    // end of computation
  }
  // end of function.
}



// Apply first type of preconditioner: multiply the terms of the first variable by the inverse of the matrix P;
void applyPreconditioner(KSP& prec, Tucker2var& x){
  for(unsigned int kTerm=0; kTerm<x.rank(0); kTerm++){
    vec_petsc x_r = x.R(kTerm);
    vec_petsc new_r(x_r.size(),x_r.comm());
    new_r.finalize();
    KSPSolve(prec, x_r.x(), new_r.x());
    x.clear_term_R(kTerm);
    x.set_term_R(kTerm, new_r);
  }
}


// gmres with first kind of preconditioner:
void gmres(operatorTensor& A, Tucker2var& b, Tucker2var& sol_0, Tucker2var& sol, mat& P, double tolR, double tolC, unsigned int maxIt){
  MPI_Comm communicator = b.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // define the tolerance:
  double norm_rhs = b.norm();
  const double tol_gmres = tolR * norm_rhs;

  // setup of the linear solver for the preconditioner:
  KSP prec;
  KSPCreate(communicator,&prec);
  KSPSetOperators(prec, P.M(), P.M());
  KSPSetType(prec, KSPGMRES);
  KSPSetTolerances(prec, 1.0e-14, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);
  KSPSetFromOptions(prec);
  KSPSetUp(prec);

  // starting gmres:
  Tucker2var residual;
  residual << b;
  Tucker2var Ax;
  applyOperator(A, sol_0, Ax);
  //residual -= Ax;
  residual.axpy(Ax,-1.0);
  Ax.clear();
  residual.round(tolC);

  vector<double> res_norm(maxIt+1);
  res_norm[0] = residual.norm();
  if(idProc==0){
    cout << "Residual norm = " << res_norm[0] << endl;
  }
  // apply the preconditioner:
  applyPreconditioner(prec, residual);


  // define auxiliary quantities for Arnoldi iteration and update:
  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  beta[0] = residual.norm();

  residual *= (1.0/beta[0]);

  vector<Tucker2var> Q(maxIt+1);
  Q[0] << residual;
  residual.clear();

  // initialising mat_real H:
  mat_real H(maxIt+1,maxIt);

  unsigned int it = 0;
  bool haveToIter = (it<maxIt) && (res_norm[it]>tol_gmres);
  while(haveToIter){
    // Arnoldi iteration:
    cout << "Apply operator: " << endl;
    applyOperator(A, Q[it], Q[it+1]);
    cout << "rounding" << endl;
    Q[it+1].round(tolC);
    cout << "ok" << endl;

    applyPreconditioner(prec, Q[it+1]);
    //MPI_Barrier(communicator);

    cout << "Starting Arnoldi" << endl;
    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = Q[it+1] * Q[sub_it];
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val);
      Q[it+1].round(tolC);
    }
    double value = Q[it+1].norm();
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);
    cout << "Finished Arnoldi" << endl << endl;

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);
    cout << "Givens done" << endl << endl;

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    vector<double> y(it,0.0);
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }
    cout << "y values done." << endl;
    //MPI_Barrier(communicator);

    // copy the initial guess:
    Tucker2var tmpSol; // sol_0 = 0;
    tmpSol << Q[0];
    tmpSol *= y[0];
    for(unsigned int sub_it=1; sub_it<it; sub_it++){
      tmpSol.axpy(Q[sub_it],y[sub_it]);
    }
    //double tmp_norm = tmpSol.norm();

    if(idProc==0){
      cout << "sol ranks = " << tmpSol.rank(0) << " " << tmpSol.rank(1) << endl;
    }
    tmpSol.round(tolC);
    if(idProc==0){
      cout << "sol ranks after round = " << tmpSol.rank(0) << " " << tmpSol.rank(1) << endl;
    }

    // compute the residual:
    Tucker2var res; res << b;
    Tucker2var Ax;
    applyOperator(A,tmpSol, Ax);
    //residual -= Ax;
    res.axpy(Ax, -1.0);
    Ax.clear();

    res.round(tolC);
    //MPI_Barrier(communicator);
    res_norm[it] = res.norm_core();
    if(idProc==0){
      cout << "residual norm = " << res_norm[it]  << endl;
    }
    //MPI_Barrier(communicator);

    //cout << "Proc " << idProc << " resNorm = " << res_norm[it] << endl;

    // check if iterate or not:
    haveToIter = (it<maxIt) && (res_norm[it]>tol_gmres);
    if(!haveToIter){
      cout << " copy the solution and exit"<< endl;
      sol << tmpSol;
    }
    tmpSol.clear();
  }

  // free the memory:
  KSPDestroy(&prec);
  for(unsigned int i=0; i<it+1; i++){
    Q[i].clear();
  }
  Q.clear();
  //H.clear();
}



// Arnoldi iteration for a restarted GMRES:
void Arnoldi(operatorTensor&A, Tucker2var& residual, KSP& prec, vector<Tucker2var>& Q, vector<double>& y, double tolC, unsigned int maxIt){
  // communicator:
  MPI_Comm communicator = residual.R(0).comm();
  int idProc;
  MPI_Comm_rank(communicator, &idProc);

  // resize and introduce tables:
  Q.resize(maxIt+1);
  y.resize(maxIt+1);
  vector<double> cs(maxIt,0.0);
  vector<double> sn(maxIt,0.0);
  vector<double> beta(maxIt+1, 0.0);
  mat_real H(maxIt+1,maxIt);
  H.zero();

  // apply the preconditioner:
  Q[0] << residual;
  applyPreconditioner(prec, Q[0]);
  beta[0] = Q[0].norm();
  Q[0] *= (1.0/beta[0]);

  // Arnoldi iteration:
  if(idProc==0){
    cout << "Starting Arnoldi" << endl;
  }
  unsigned int it = 0;
  bool haveToIter = (it<maxIt);
  while(haveToIter){
    // apply operator:
    applyOperator(A, Q[it], Q[it+1]);
    Q[it+1].round(tolC);

    applyPreconditioner(prec, Q[it+1]);
    MPI_Barrier(communicator);

    for(unsigned int sub_it=0; sub_it<it+1; sub_it++){
      double val = 0.0;
      dot(Q[it+1], Q[sub_it], val);
      H.setMatEl(sub_it,it,val);
      Q[it+1].axpy(Q[sub_it], -val);
      Q[it+1].round(tolC);
    }

    double value = Q[it+1].norm(); // norm_core() is the fast norm computation after round.
    H.setMatEl(it+1,it, value);
    Q[it+1]*=(1.0/value);

    // Givens Rotation:
    for(unsigned int sub_it=0; sub_it<it; sub_it++){
      double val_c = H(sub_it,it);
      double val_s = H(sub_it+1, it);
      double tmp = cs[sub_it] * val_c + sn[sub_it] * val_s;
      double mat_entry = -sn[sub_it]*val_c + cs[sub_it]*val_s;
      H.setMatEl(sub_it+1, it, mat_entry);
      H.setMatEl(sub_it, it, tmp);
    }
    double tmp_d = H(it,it);
    double tmp_p = H(it+1,it);
    double val = sqrt(tmp_d*tmp_d + tmp_p*tmp_p);
    cs[it] = tmp_d/val;
    sn[it] = tmp_p/val;

    double diagEntry = cs[it]*tmp_d + sn[it]*tmp_p;
    H.setMatEl(it,it,diagEntry);
    H.setMatEl(it+1,it, 0.0);

    // compute the residual:
    beta[it+1] = -sn[it]*beta[it];
    beta[it] = cs[it]*beta[it];
    // solve small triangular linear system:
    it += 1;
    for(int i=it-1; i>=0; i--){
      y[i] = beta[i];
      for(int j=it-1; j>i; j--){
        y[i] -= H(i,j)*y[j];
      }
      y[i] = y[i]/(H(i,i));
    }
    MPI_Barrier(communicator);
  // end while:
  haveToIter = (it<maxIt);
  }
  if(idProc==0){
    cout << "Arnoldi finished." << endl;
  }
}
