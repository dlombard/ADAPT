// implementation of Tucker2var class


#include "Tucker2var.h"

using namespace std;


// overloaded constructor:
Tucker2var::Tucker2var(vector<unsigned int> NdofPerDim){
  m_nVar=2;
  m_rank.resize(2);
  m_rank[0] = 0;
  m_rank[1] = 0;
  m_nDof_var.resize(2);
  m_nDof_var[0] = NdofPerDim[0];
  m_nDof_var[1] = NdofPerDim[1];
  m_R.resize(0);
  m_S.resize(0);
  // do not initialize yet the matrix A.
}

Tucker2var::Tucker2var(vector<vec_petsc>& R_tab, vector<vec_petsc>& S_tab, mat_real& A_mat){
  m_nVar=2;
  m_rank.resize(2);
  m_rank[0] = R_tab.size();
  m_rank[1] = S_tab.size();
  m_nDof_var.resize(2);
  m_nDof_var[0] = R_tab[0].size();
  m_nDof_var[1] = R_tab[1].size();
  m_R.resize(m_rank[0]);
  for(unsigned int k=0; k<m_rank[0]; k++){
    m_R[k] = R_tab[k];
  }

  m_S.resize(m_rank[1]);
  for(unsigned int k=0; k<m_rank[1]; k++){
    m_S[k] = S_tab[k];
  }
  // by reference on the matrix:
  //m_A = A_mat;
  // copy the matrix:
  m_A << A_mat;
}

// init an empty Tucker2var:
void Tucker2var::init(vector<unsigned int> NdofPerDim){
  m_nVar=2;
  m_rank.resize(2);
  m_rank[0] = 0;
  m_rank[1] = 0;
  m_nDof_var.resize(2);
  m_nDof_var[0] = NdofPerDim[0];
  m_nDof_var[1] = NdofPerDim[1];
  m_R.resize(0);
  m_S.resize(0);
}

// set terms table by reference:
void Tucker2var::set_terms(vector<vec_petsc>& R_tab, vector<vec_petsc>& S_tab){
  m_rank[0] = R_tab.size();
  m_rank[1] = S_tab.size();
  m_nDof_var.resize(2);
  m_nDof_var[0] = R_tab[0].size();
  m_nDof_var[1] = R_tab[1].size();
  m_R.resize(m_rank[0]);
  for(unsigned int k=0; k<m_rank[0]; k++){
    m_R[k] = R_tab[k];
  }

  m_S.resize(m_rank[1]);
  for(unsigned int k=0; k<m_rank[1]; k++){
    m_S[k] = S_tab[k];
  }
}

// set just one term:
void Tucker2var::set_term_R(unsigned int iTerm, vec_petsc& r){
  m_R[iTerm] = r;
}

void Tucker2var::set_term_S(unsigned int iTerm, vec_petsc& s){
  m_S[iTerm] = s;
}


// set the core by copy:
void Tucker2var::set_core(mat_real& core){
  m_A << core;
}


// copy agiven tensor:
void Tucker2var::copyTensorFrom(Tucker2var& T){
  m_nVar = 2;
  m_nDof_var.resize(m_nVar);
  m_nDof_var[0] = T.nDof_var(0);
  m_nDof_var[1] = T.nDof_var(1);
  m_rank.resize(2);
  m_rank[0] = T.rank(0);
  m_rank[1] = T.rank(1);
  m_R.resize(m_rank[0]);
  for(unsigned int k=0; k<m_rank[0]; k++){
    vec_petsc r = T.R(k);
    m_R[k] << r;
  }
  m_S.resize(m_rank[1]);
  for(unsigned int k=0; k<m_rank[1]; k++){
    vec_petsc s = T.S(k);
    m_S[k] << s;
  }
  // copy the core:
  m_A.init(m_rank[0],m_rank[1]);
  for(unsigned int iRow=0; iRow<m_rank[0]; iRow++){
    for(unsigned int jCol=0; jCol<m_rank[1]; jCol++){
      double val = T.A(iRow,jCol);
      m_A.setMatEl(iRow,jCol,val);
    }
  }
}


// norm 2 (Frobenius squared):
double Tucker2var::norm2(){
  double prod = 0.0;
  // products between modes R: dot_r(i,l) = <R[i],R[l]> (sym!)
  mat_real dot_r(m_rank[0],m_rank[0]);
  for(unsigned int iRow=0; iRow<m_rank[0]; iRow++){
    vec_petsc u_r = m_R[iRow];
    for(unsigned int lCol=0; lCol<=iRow; lCol++){ // sym
      vec_petsc v_r = m_R[lCol];
      double val = dot(u_r,v_r);
      dot_r.setMatEl(iRow,lCol,val);
      if(iRow != lCol){
        dot_r.setMatEl(lCol,iRow,val);
      }
    }
  }


  // products between modes S: dot_s(j,m) = <S[j],S[m]> (sym!)
  mat_real dot_s(m_rank[1],m_rank[1]);
  for(unsigned int jRow=0; jRow<m_rank[1]; jRow++){
    vec_petsc u_s = m_S[jRow];
    for(unsigned int mCol=0; mCol<=jRow; mCol++){
      vec_petsc v_s = m_S[mCol];
      double val = dot(u_s,v_s);
      dot_s.setMatEl(jRow,mCol,val);
      if(jRow != mCol){
        dot_s.setMatEl(mCol,jRow,val);
      }
    }
  }

  // compute D(i,m) = sum_l dot_r(i,l) * v.A(l,m)
  mat_real D(m_rank[0],m_rank[1]);
  for(unsigned int i=0; i<m_rank[0]; i++){
    for(unsigned int m=0; m<m_rank[1]; m++){
      double entry = 0.0;
      for(unsigned int l=0; l<m_rank[0]; l++){
        double val_r = dot_r(i,l);
        double vA_lm = m_A(l,m);
        entry += val_r * vA_lm;
      }
      D.setMatEl(i,m,entry);
    }
  };

  // compute E(i,j) = sum_m D_im dot_s^T(m,j)
  mat_real E(m_rank[0],m_rank[1]);
  for(unsigned int i=0; i<m_rank[0]; i++){
    for(unsigned int j=0; j<m_rank[1]; j++){
      double entry = 0.0;
      for(unsigned int m=0; m<m_rank[1]; m++){
        double d = D(i,m);
        double val_s = dot_s(j,m);
        entry += d*val_s;
      }
      E.setMatEl(i,j,entry);
    }
  };

  // compute the result: sum_{i,j} A(i,j)*E(i,j)
  for(unsigned int i=0; i<m_rank[0]; i++){
    for(unsigned int j=0; j<m_rank[1]; j++){
      double uA_ij = m_A(i,j);
      double E_ij = E(i,j);
      prod += uA_ij * E_ij;
    }
  }
  // ensure round-off 0:
  if(prod<=0){prod = 0.0;}

  return prod;
}


// sacel a tensor by a constant:
void Tucker2var::scale(double alpha){
  m_A *= alpha;
}

// axpy: done by copy.
void Tucker2var::axpy(Tucker2var& T, double alpha){
  vector<unsigned int> sum_rank(2);
  sum_rank[0] = m_rank[0] + T.rank(0);
  sum_rank[1] = m_rank[1] + T.rank(1);

  // update the table R:
  m_R.resize(sum_rank[0]);
  for(unsigned int iTerm=0; iTerm<T.rank(0); iTerm++){
    vec_petsc r = T.R(iTerm);
    m_R[m_rank[0]+iTerm] << r;
  }
  // update the table S:
  m_S.resize(sum_rank[1]);
  for(unsigned int iTerm=0; iTerm<T.rank(1); iTerm++){
    vec_petsc s = T.S(iTerm);
    m_S[m_rank[1]+iTerm] << s;
  }

  // update the matrix: copy the current core.
  mat_real core; core << m_A;
  // clear the current core:
  m_A.clear();
  m_A.init(sum_rank[0],sum_rank[1]);
  m_A.zero(); // setting all entries to 0.
  for(unsigned int i=0; i<m_rank[0]; i++){
    for(unsigned int j=0; j<m_rank[1]; j++){
      double val = core(i,j);
      m_A.setMatEl(i,j,val);
    }
  }
  for(unsigned int i=0; i<T.rank(0); i++){
    for(unsigned int j=0; j<T.rank(1); j++){
      double val = T.A(i,j) * alpha;
      unsigned int iRow = m_rank[0] + i;
      unsigned int jCol = m_rank[1] + j;
      m_A.setMatEl(iRow,jCol,val);
    }
  }

  // update the ranks:
  m_rank[0] = sum_rank[0];
  m_rank[1] = sum_rank[1];

  // calling the destructor of core, automatically freed.
}

// compute the norm of the core: it is the norm of the tensor after rounding:
double Tucker2var::norm_core(){
  double frob = 0.0;
  for(unsigned int i=0; i<m_A.nRows(); i++){
    for(unsigned int j=0; j<m_A.nCols(); j++){
      double val = m_A(i,j);
      frob += val * val;
    }
  }
  frob = sqrt(frob);
  return frob;
}


// round a Tucker2var: round a non-zero tensor
void Tucker2var::round(double& tol){
  const double tol_sq = tol * tol;

  // perform the QR of the R modes
  vector<vec_petsc> Q_r;
  mat_real R_r;
  qr_mgs(m_R, Q_r, R_r);

  // perform the QR of the S modes:
  vector<vec_petsc> Q_s;
  mat_real R_s;
  qr_mgs(m_S, Q_s, R_s);


  // In the matrices R_r, R_s, find the positions of the rows which are 0 (not just diag):
  vector<unsigned int> zeroInd_r;
  vector<unsigned int> nzInd_r;
  for(unsigned int iRow=0; iRow<R_r.nRows(); iRow++){
    bool isItZero = true;
    for(unsigned int jCol=iRow; jCol<R_r.nCols(); jCol++){
      double val = R_r(iRow,jCol);
      if(val != 0.0){
        isItZero = false;
        break;
      }
    }
    if(isItZero){
      zeroInd_r.push_back(iRow);
    }
    else{
      nzInd_r.push_back(iRow);
    }
  }

  vector<unsigned int> zeroInd_s;
  vector<unsigned int> nzInd_s;
  for(unsigned int iRow=0; iRow<R_s.nRows(); iRow++){
    bool isItZero = true;
    for(unsigned int jCol=iRow; jCol<R_s.nCols(); jCol++){
      double val = R_s(iRow,jCol);
      if(val != 0.0){
        isItZero = false;
        break;
      }
    }
    if(isItZero){
      zeroInd_s.push_back(iRow);
    }
    else{
      nzInd_s.push_back(iRow);
    }
  }

  // compute the multiplication between m_A and the triangular matrices:

  // m_A * R_s^T: R_s is upper trg.
  mat_real tmp1(m_A.nRows(),R_s.nRows());
  tmp1.zero();
  for(unsigned int iRow=0; iRow<m_A.nRows(); iRow++){
    for(unsigned int jCol=0; jCol<R_s.nRows(); jCol++){
      double val = 0.0;
      for(unsigned int k=jCol; k<m_A.nCols(); k++){
        double a_val = m_A(iRow,k);
        double r_val = R_s(jCol,k);
        val += a_val * r_val;
      }
      if(fabs(val)<=DBL_EPSILON){
        val = 0.0;
      }
      tmp1.setMatEl(iRow,jCol,val);
    }
  }

  // compute R_r * m_A: R_r is upper trg.
  mat_real core_tr(R_r.nRows(),m_A.nCols());
  core_tr.zero();
  for(unsigned int iRow=0; iRow<R_r.nRows(); iRow++){
    for(unsigned int jCol=0; jCol<m_A.nCols(); jCol++){
      double val = 0.0;
      for(unsigned int k=iRow; k<m_A.nRows(); k++){
        double a_val = tmp1(k,jCol);
        double r_val = R_r(iRow,k);
        val += a_val * r_val;
      }
      if(fabs(val)<=DBL_EPSILON){
        val = 0.0;
      }
      core_tr.setMatEl(iRow,jCol,val);
    }
  }


  // first pruning: from A remove the rows and the cols indicated by zeroInds
  unsigned int tmpRows = m_A.nRows() - zeroInd_r.size();
  unsigned int tmpCols = m_A.nCols() - zeroInd_s.size();
  mat_real tmpA(tmpRows,tmpCols);

  for(unsigned int iRow=0; iRow<tmpRows; iRow++){
    unsigned int i = nzInd_r[iRow];
    for(unsigned int jCol=0; jCol<tmpCols; jCol++){
      unsigned int j = nzInd_s[jCol];
      double val = core_tr(i,j);
      if(fabs(val)<=DBL_EPSILON){
        val = 0.0;
      }
      tmpA.setMatEl(iRow,jCol,val);
    }
  }


  // replace the modes by Q_r and Q_s:
  for(unsigned int iTerm=0; iTerm<m_rank[0]; iTerm++){
    m_R[iTerm].clear();
  }
  m_R.clear();
  for(unsigned int iTerm=0; iTerm<m_rank[1]; iTerm++){
    m_S[iTerm].clear();
  }
  m_S.clear();

  m_R.resize(tmpRows);
  for(unsigned int iTerm=0; iTerm<tmpRows; iTerm++){
    m_R[iTerm] = Q_r[iTerm];
  }

  m_S.resize(tmpCols);
  for(unsigned int iTerm=0; iTerm<tmpCols; iTerm++){
    m_S[iTerm] = Q_s[iTerm];
  }

  // GREEDY ALGORITHM:

  // initialise:
  vector<unsigned int> list_rows(tmpRows);
  for(unsigned int iRow=0; iRow<tmpRows; iRow++){
    list_rows[iRow] = iRow;
  }

  vector<unsigned int> list_cols(tmpCols);
  for(unsigned int jCol=0; jCol<tmpCols; jCol++){
    list_cols[jCol] = jCol;
  }

  vector<unsigned int> toRemove_rows;
  vector<unsigned int> toRemove_cols;

  double fro_sq = 0.0;
  for(unsigned int iRow=0; iRow<tmpRows; iRow++){
    for(unsigned int jCol = 0; jCol<tmpCols; jCol++){
      double val = tmpA(iRow,jCol);
      fro_sq += val * val;
    }
  }

  vector<double> energy_rows(tmpRows);
  for(unsigned int iRow=0; iRow<tmpRows; iRow++){
    energy_rows[iRow] = 0.0;
    for(unsigned int jCol = 0; jCol<tmpCols; jCol++){
      double val = tmpA(iRow,jCol);
      energy_rows[iRow] += val * val;
    }
  }

  vector<double> energy_cols(tmpCols);
  for(unsigned int jCol=0; jCol<tmpCols; jCol++){
    energy_cols[jCol] = 0.0;
    for(unsigned int iRow = 0; iRow<tmpRows; iRow++){
      double val = tmpA(iRow,jCol);
      energy_cols[jCol] += val * val;
    }
  }

  double tail = 0.0;

  while((list_rows.size()>0) && (list_cols.size()>0)){

    // find the minimum energy contribution among rows and columns:
    unsigned int arg_min_list_rows = 0;
    unsigned int arg_min_rows = list_rows[0];
    double min_en_rows = energy_rows[arg_min_rows];
    unsigned int arg_min_list_cols = 0;
    unsigned int arg_min_cols = list_cols[0];
    double min_en_cols = energy_cols[arg_min_cols];


    for(unsigned int i=0; i<list_rows.size(); i++){
      unsigned int iRow = list_rows[i];
      if(energy_rows[iRow] < min_en_rows){
        min_en_rows = energy_rows[iRow];
        arg_min_list_rows = i;
        arg_min_rows = iRow;
      }
    }

    for(unsigned int j=0; j<list_cols.size(); j++){
      unsigned int jCol = list_cols[j];
      if(energy_cols[jCol] < min_en_cols){
        min_en_cols = energy_cols[jCol];
        arg_min_list_cols = j;
        arg_min_cols = jCol;
      }
    }

    bool isMinRow = (min_en_rows<=min_en_cols);
    double min_en = (isMinRow) ? min_en_rows : min_en_cols;


    tail += min_en;
    if(tail > tol_sq*fro_sq){
      break;
    }
    else{
        // remove the rows or col having the minimum energy:
        if(isMinRow){
          // pruning the modes:
          m_R[arg_min_list_rows].clear();
          m_R.erase(m_R.begin()+arg_min_list_rows);
          // updating the list and the energies:
          list_rows.erase(list_rows.begin()+arg_min_list_rows);
          energy_rows[arg_min_rows] = 0.0;
          toRemove_rows.push_back(arg_min_rows);
          // update the energy of the cols:
          for(unsigned int j=0; j<list_cols.size(); j++){
            unsigned int jCol = list_cols[j];
            double val = tmpA(arg_min_rows, jCol);
            energy_cols[jCol] -= val*val;
          }
        }
        else{
          // pruning the modes:
          m_S[arg_min_list_cols].clear();
          m_S.erase(m_S.begin()+arg_min_list_cols);
          // update the list and the energies:
          list_cols.erase(list_cols.begin()+arg_min_list_cols);
          energy_cols[arg_min_cols] = 0.0;
          toRemove_cols.push_back(arg_min_cols);
          // update the energy of the rows:
          for(unsigned int i=0; i<list_rows.size(); i++){
            unsigned int iRow= list_rows[i];
            double val = tmpA(iRow, arg_min_cols);
            energy_rows[iRow] -= val*val;
          }
        };
        // end of update
    }
    // end of greedy
  }

  // computing the new matrix A:
  m_A.clear();
  unsigned int n_rows = list_rows.size();
  unsigned int n_cols = list_cols.size();
  m_A.init(n_rows,n_cols);
  m_A.zero();
  for(unsigned int i=0; i<n_rows; i++){
    unsigned int iRow = list_rows[i];
    for(unsigned int j=0; j<n_cols; j++){
      unsigned int jCol = list_cols[j];
      double val = tmpA(iRow,jCol);
      m_A.setMatEl(i,j,val);
    }
  }


  // updating the ranks:
  m_rank[0] = n_rows;
  m_rank[1] = n_cols;

}


// scalar product between two Tcker2var tensors:
double operator * (Tucker2var& u, Tucker2var& v){
  double prod = 0.0;
  // products between modes R: dot_r(i,l) = <u_R[i],v_R[l]>
  mat_real dot_r(u.rank(0),v.rank(0));
  for(unsigned int iRow=0; iRow<u.rank(0); iRow++){
    vec_petsc u_r = u.R(iRow);
    for(unsigned int lCol=0; lCol<v.rank(0); lCol++){
      vec_petsc v_r = v.R(lCol);
      double val = dot(u_r,v_r);
      dot_r.setMatEl(iRow,lCol,val);
    }
  }
  // products between modes S: dot_s(j,m) = <u_S[j],v_S[m]>
  mat_real dot_s(u.rank(1),v.rank(1));
  for(unsigned int jRow=0; jRow<u.rank(1); jRow++){
    vec_petsc u_s = u.S(jRow);
    for(unsigned int mCol=0; mCol<v.rank(1); mCol++){
      vec_petsc v_s = v.S(mCol);
      double val = dot(u_s,v_s);
      dot_s.setMatEl(jRow,mCol,val);
    }
  }

  // compute D(i,m) = sum_l dot_r(i,l) * v.A(l,m)
  mat_real D(u.rank(0),v.rank(1));
  for(unsigned int i=0; i<u.rank(0); i++){
    for(unsigned int m=0; m<v.rank(1); m++){
      double entry = 0.0;
      for(unsigned int l=0; l<v.rank(0); l++){
        double val_r = dot_r(i,l);
        double vA_lm = v.A(l,m);
        entry += val_r * vA_lm;
      }
      D.setMatEl(i,m,entry);
    }
  };

  // compute E(i,j) = sum_m D_im dot_s^T(m,j)
  mat_real E(u.rank(0),u.rank(1));
  for(unsigned int i=0; i<u.rank(0); i++){
    for(unsigned int j=0; j<u.rank(1); j++){
      double entry = 0.0;
      for(unsigned int m=0; m<v.rank(1); m++){
        double d = D(i,m);
        double val_s = dot_s(j,m);
        entry += d*val_s;
      }
      E.setMatEl(i,j,entry);
    }
  };

  // compute the result: sum_{i,j} u.A(i,j)*E(i,j)
  for(unsigned int i=0; i<u.rank(0); i++){
    for(unsigned int j=0; j<u.rank(1); j++){
      double uA_ij = u.A(i,j);
      double E_ij = E(i,j);
      prod += uA_ij * E_ij;
    }
  }

  return prod;
}


// function:
void dot(Tucker2var& u, Tucker2var&v, double& prod){
    prod = 0.0;
    // products between modes R: dot_r(i,l) = <u_R[i],v_R[l]>
    mat_real dot_r(u.rank(0),v.rank(0));
    for(unsigned int iRow=0; iRow<u.rank(0); iRow++){
      vec_petsc u_r = u.R(iRow);
      for(unsigned int lCol=0; lCol<v.rank(0); lCol++){
        vec_petsc v_r = v.R(lCol);
        double val = dot(u_r,v_r);
        dot_r.setMatEl(iRow,lCol,val);
      }
    }
    // products between modes S: dot_s(j,m) = <u_S[j],v_S[m]>
    mat_real dot_s(u.rank(1),v.rank(1));
    for(unsigned int jRow=0; jRow<u.rank(1); jRow++){
      vec_petsc u_s = u.S(jRow);
      for(unsigned int mCol=0; mCol<v.rank(1); mCol++){
        vec_petsc v_s = v.S(mCol);
        double val = dot(u_s,v_s);
        dot_s.setMatEl(jRow,mCol,val);
      }
    }

    // compute D(i,m) = sum_l dot_r(i,l) * v.A(l,m)
    mat_real D(u.rank(0),v.rank(1));
    for(unsigned int i=0; i<u.rank(0); i++){
      for(unsigned int m=0; m<v.rank(1); m++){
        double entry = 0.0;
        for(unsigned int l=0; l<v.rank(0); l++){
          double val_r = dot_r(i,l);
          double vA_lm = v.A(l,m);
          entry += val_r * vA_lm;
        }
        D.setMatEl(i,m,entry);
      }
    };

    // compute E(i,j) = sum_m D_im dot_s^T(m,j)
    mat_real E(u.rank(0),u.rank(1));
    for(unsigned int i=0; i<u.rank(0); i++){
      for(unsigned int j=0; j<u.rank(1); j++){
        double entry = 0.0;
        for(unsigned int m=0; m<v.rank(1); m++){
          double d = D(i,m);
          double val_s = dot_s(j,m);
          entry += d*val_s;
        }
        E.setMatEl(i,j,entry);
      }
    };

    // compute the result: sum_{i,j} u.A(i,j)*E(i,j)
    for(unsigned int i=0; i<u.rank(0); i++){
      for(unsigned int j=0; j<u.rank(1); j++){
        double uA_ij = u.A(i,j);
        double E_ij = E(i,j);
        prod += uA_ij * E_ij;
      }
    }
}
