// Implementation of CP2var

#include "CP2var.h"

// I.1 Overloaded constructors:

// empty, just assign the dofs:
CP2var::CP2var(vector<unsigned int> n_dofs){
  m_nVar = 2;
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = n_dofs[iVar];
  }
  m_rank=0;
  m_R.resize(0);
  m_S.resize(0);
  m_coeffs.resize(0);
}

// assign the modes:
CP2var::CP2var(vector<vec_petsc>& R, vector<vec_petsc>& S){
  m_nVar = 2;
  m_rank=R.size();
  assert(S.size()==m_rank);

  m_nDof_var.resize(m_nVar);
  m_nDof_var[0] = R[0].size();
  m_nDof_var[1] = S[0].size();

  // do not perform copy, assign by reference.
  m_R.resize(m_rank);
  m_S.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_R[iTerm] = R[iTerm];
    m_S[iTerm] = S[iTerm];
  }

  // set all coefficients to 1:
  m_coeffs.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_coeffs[iTerm] = 1.0;
  }
}


// rank 1 term:
CP2var::CP2var(vec_petsc& r, vec_petsc& s){
  m_nVar = 2;
  m_rank=1;
  // define the resolution:
  m_nDof_var.resize(m_nVar);
  m_nDof_var[0] = r.size();
  m_nDof_var[1] = s.size();

  // do not perform copy, assign by reference.
  m_R.resize(1);
  m_R[0] = r;
  m_S.resize(1);
  m_S[0] = s;

  // set the only coefficient to 1:
  m_coeffs.resize(1);
  m_coeffs[0] = 1.0;
}

// overloaded, provide coefficients:
CP2var::CP2var(vector<vec_petsc>& R, vector<vec_petsc>& S, vector<double>& cs){
  m_nVar = 2;
  m_rank=R.size();
  assert(S.size()==m_rank);
  assert(cs.size()==m_rank);

  m_nDof_var.resize(m_nVar);
  m_nDof_var[0] = R[0].size();
  m_nDof_var[1] = S[0].size();

  // do not perform copy, assign by reference.
  m_R.resize(m_rank);
  m_S.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_R[iTerm] = R[iTerm];
    m_S[iTerm] = S[iTerm];
  }
  m_coeffs.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_coeffs[iTerm] = cs[iTerm];
  }
}

// rank 1 term:
CP2var::CP2var(vec_petsc& r, vec_petsc& s, double& c){
  m_nVar = 2;
  m_rank=1;
  // define the resolution:
  m_nDof_var.resize(m_nVar);
  m_nDof_var[0] = r.size();
  m_nDof_var[1] = s.size();

  // do not perform copy, assign by reference.
  m_R.resize(1);
  m_R[0] = r;
  m_S.resize(1);
  m_S[0] = s;

  // set the only coefficient to 1:
  m_coeffs.resize(1);
  m_coeffs[0] = c;
}


// SETTERS:
void CP2var::set_terms(vector<vec_petsc>& R_tab, vector<vec_petsc>& S_tab){
  m_rank = R_tab.size();
  m_R.resize(m_rank);
  m_S.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_R[iTerm] = R_tab[iTerm];
    m_S[iTerm] = S_tab[iTerm];
  }
  //m_R = R_tab;
  //m_S = S_tab;
  m_nVar = 2;
  m_nDof_var.resize(2);
  m_nDof_var[0] = R_tab[0].size();
  m_nDof_var[1] = S_tab[0].size();
}
// overloaded rank 1:
void CP2var::set_terms(vec_petsc& r_one, vec_petsc& s_one){
  m_nVar = 2;
  m_rank = 1;
  m_R.resize(1);
  m_R[0] = r_one;
  m_S.resize(1);
  m_S[0] = s_one;
  m_nDof_var.resize(2);
  m_nDof_var[0] = r_one.size();
  m_nDof_var[1] = s_one.size();
}

// set a single term into a specific position:
void CP2var::set_term_R(unsigned int id_term, vec_petsc& r_one){
  m_R[id_term] = r_one;
}

void CP2var::set_term_S(unsigned int id_term, vec_petsc& s_one){
  m_S[id_term] = s_one;
}

void CP2var::set_terms(unsigned int id_term, vec_petsc& r_one, vec_petsc& s_one){
  m_R[id_term] = r_one;
  m_S[id_term] = s_one;
}

// set the coefficients: !by reference!
void CP2var::set_coeffs(vector<double>& cs){
  m_coeffs = cs;
}

// overloaded: set the value of one coefficient:
void CP2var::set_coeffs(unsigned int iTerm, double coeff){
  m_coeffs[iTerm] = coeff;
}



// (I.1.5) copyTensorFrom:
void CP2var::copyTensorFrom(CP2var& A){
  m_nVar = 2;
  m_rank = A.rank();
  m_nDof_var.resize(2);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = A.nDof_var(iVar);
  }
  m_R.resize(m_rank);
  m_S.resize(m_rank);
  m_coeffs.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    vec_petsc tmp_ar = A.R(iTerm);
    m_R[iTerm].copyVecFrom(tmp_ar);
    vec_petsc tmp_as = A.S(iTerm);
    m_S[iTerm].copyVecFrom(tmp_as);
    m_coeffs[iTerm] = A.coeffs(iTerm);
  }
}


// (I.2) the zero CP: when the sizes are known(!!!)
void CP2var::zero(){
  m_nVar = 2;
  if(m_rank>1){
    for(unsigned int iTerm=1; iTerm<m_rank; iTerm++){
      m_R[iTerm].clear();
      m_S[iTerm].clear();
    }
    m_coeffs.clear();
  }
  m_rank=0;
  m_R.resize(0);
  m_S.resize(0);
  m_coeffs.resize(0);
}

void CP2var::zero(vector<unsigned int> n_dofs){
  m_nVar = 2;
  assert(n_dofs.size()==3);
  m_rank=0;
  m_nDof_var.resize(2);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = n_dofs[iVar];
  }
  m_R.resize(0);
  m_S.resize(0);
  m_coeffs.resize(0);
}

// (I.3) append a vector => increase the rank by 1:
void CP2var::append(vec_petsc& r, vec_petsc& s){
  m_rank += 1;
  m_R.push_back(r);
  m_S.push_back(s);
  m_coeffs.push_back(1.0);
}

// (I.3) append a vector => increase the rank by the size of this vector set:
void CP2var::append(vector<vec_petsc>& r_modes, vector<vec_petsc>& s_modes){
  const unsigned int nModes = r_modes.size();
  assert(s_modes.size()==nModes);
  unsigned int new_rank = m_rank+nModes;
  m_R.resize(new_rank);
  m_S.resize(new_rank);
  m_coeffs.resize(new_rank);

  for(unsigned int iMod=m_rank; iMod<new_rank; iMod++){
    m_R[iMod] = r_modes[iMod-m_rank];
    m_S[iMod] = s_modes[iMod-m_rank];
    m_coeffs[iMod] = 1.0;
  }
  m_rank = new_rank;
}

// (I.3) append a vector => increase the rank by 1:
void CP2var::append(vec_petsc& r, vec_petsc& s, double& c){
  m_rank += 1;
  m_R.push_back(r);
  m_S.push_back(s);
  m_coeffs.push_back(c);
}

// (I.3) append a set of vectors => increase the rank by the size of this set:
void CP2var::append(vector<vec_petsc>& r_modes, vector<vec_petsc>& s_modes, vector<double>& cs){
  const unsigned int nModes = r_modes.size();
  assert(s_modes.size()==nModes);
  unsigned int new_rank = m_rank+nModes;
  m_R.resize(new_rank);
  m_S.resize(new_rank);
  m_coeffs.resize(new_rank);

  for(unsigned int iMod=m_rank; iMod<new_rank; iMod++){
    m_R[iMod] = r_modes[iMod-m_rank];
    m_S[iMod] = s_modes[iMod-m_rank];
    m_coeffs[iMod] = cs[iMod-m_rank];
  }
  m_rank = new_rank;
}


// (I.4) function axpy: this = this + alpha*A
void CP2var::axpy(CP2var& A, double alpha, bool byCopy=true){
  const unsigned int old_rank = m_rank;

  m_rank += A.rank();
  m_R.resize(m_rank);
  m_S.resize(m_rank);
  m_coeffs.resize(m_rank);

  if(byCopy){
    for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
      vec_petsc tmp_ar = A.R(iTerm);
      m_R[iTerm+old_rank].copyVecFrom(tmp_ar);

      vec_petsc tmp_as = A.S(iTerm);
      m_S[iTerm+old_rank].copyVecFrom(tmp_as);

      m_coeffs[iTerm+old_rank] = alpha*A.coeffs(iTerm);
    }
  }
  else{
    for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
      vec_petsc vec_r = A.R(iTerm);
      m_R[iTerm+old_rank] = vec_r;
      vec_petsc vec_s = A.S(iTerm);
      m_S[iTerm+old_rank] = vec_s;
      m_coeffs[iTerm+old_rank] = A.coeffs(iTerm) * alpha;
    }
  }
}
// (I.4) function axpy: this = this + alpha*A,  (overloaded)
void CP2var::axpy(vec_petsc& r_one, vec_petsc& s_one, double alpha, bool byCopy=true){
  const unsigned int old_rank = m_rank;

  m_rank += 1;
  m_R.resize(m_rank);
  m_S.resize(m_rank);
  m_coeffs.resize(m_rank);

  if(byCopy){
      m_R[old_rank].copyVecFrom(r_one);
      m_S[old_rank].copyVecFrom(s_one);
      m_coeffs[old_rank] = alpha;
  }
  else{
    m_R[old_rank] = r_one;
    m_S[old_rank] = s_one;
    m_coeffs[old_rank] = alpha;
  }
}




// (I.6) norm squared of the CP:
double CP2var::norm2CP(){
  double out = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double contrib = dot(m_R[iTerm],m_R[iTerm]);
    contrib *= dot(m_S[iTerm],m_S[iTerm]);
    contrib *= m_coeffs[iTerm] * m_coeffs[iTerm];
    out += contrib;
  }

  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int jTerm=0; jTerm<iTerm; jTerm++){
      double contrib = dot(m_R[iTerm],m_R[jTerm]);
      contrib *= dot(m_S[iTerm],m_S[jTerm]);
      contrib *= m_coeffs[iTerm] * m_coeffs[jTerm];
      out += 2.0*contrib;
    }
  }
  return out;
}


// rounding: tol is a relative tolerance !
void CP2var::round(double& tol){
  const double tol_sq = tol * tol;

  // perform the QR of the R modes
  vector<vec_petsc> Q_r;
  mat_real R_r;
  qr_mgs(m_R, Q_r, R_r);
  //qr_householder(m_R, Q_r, R_r);

  // perform the QR of the S modes:
  vector<vec_petsc> Q_s;
  mat_real R_s;
  qr_mgs(m_S, Q_s, R_s);
  //qr_householder(m_S, Q_s, R_s);


  // determine the matrix K = R_r * diag(coeffs) * R_sˆT
  mat_real K;
  vector<double> cs = m_coeffs;

  multiply_trg_diag_trg(R_r, cs, R_s, K);

  // calling lapack svd routine:
  int m = K.nRows();
  int n = K.nCols();
  int k = min(m,n);//(m < n) ? m : n;
  char jobU = 'S';
  char jobV = 'S';
  int lda = m; //(m > 1) ? m : 1;
  double* s; s = new double[k];
  double* U; U = new double[m*k];
  int ldu = m;
  double* Vt; Vt = new double[k*n];
  int ldvt = n;
  double* work; work = new double[8*k];
  int lw= 8*k; // LWORK >= MAX(1,3*MIN(M,N) + MAX(M,N),5*MIN(M,N))
  int info;

  // copy the matrix;
  double* matrix = K.M();
  dgesvd_(&jobU, &jobV, &m, &n, matrix, &lda, s , U, &ldu, Vt, &ldvt, work, &lw, &info);

  // determine the truncation:
  unsigned int toRetain = (K.nRows()<K.nCols()) ? K.nRows() : K.nCols();

  double tail = s[toRetain-1]*s[toRetain-1];
  double cumul = 0.0;
  for(unsigned int iS=0; iS<toRetain-1; iS++){
    cumul += s[iS]*s[iS];
  }
  while( (toRetain>0) & ( (1.0-tol_sq) * tail < tol_sq * cumul) ){
    toRetain -= 1;
    double val = s[toRetain-1]*s[toRetain-1];
    tail += val;
    cumul -= val;
  }
  //cout << "toBeRetained = " << toRetain << endl;

  // compute the new coefficients:
  m_coeffs.resize(toRetain);
  for(unsigned int i=0; i<toRetain; i++){
    m_coeffs[i] = s[i];
  }

  // erase the modes:
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_R[iTerm].clear();
    m_S[iTerm].clear();
  }

  // compute the new modes:
  m_rank = toRetain;
  m_R.resize(m_rank);
  m_S.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_R[iTerm].init(m_nDof_var[0]);
    m_R[iTerm].finalize();
    for(unsigned int j=0; j<Q_r.size(); j++){
      unsigned int ind = iTerm * K.nRows() + j;
      double weight = U[ind]; // U[iTerm](j)
      m_R[iTerm].axpy(Q_r[j], weight);
    }
    m_R[iTerm].finalize();
  }
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_S[iTerm].init(m_nDof_var[1]);
    m_S[iTerm].finalize();
    for(unsigned int j=0; j<Q_s.size(); j++){
      unsigned int ind = j* k + iTerm;
      double weight = Vt[ind]; // Vt[j](iTerm)
      m_S[iTerm].axpy(Q_s[j], weight);
    }
    m_S[iTerm].finalize();
  }

  /*for(unsigned int i=0; i<3;i++){
    cout << s[i] << endl;
  }
  cout <<"-----"<< endl;
  */
  // free the memory:
  delete[] s;
  delete[] U;
  delete[] Vt;
  delete[] work;
  for(unsigned int i=0; i<Q_r.size(); i++){
    Q_r[i].clear();
  }
  for(unsigned int i=0; i<Q_s.size(); i++){
    Q_s[i].clear();
  }
}



// (I.) scaling the CP2var by a scalar:
void CP2var::operator *= (double alpha){
  scale(alpha);
}

// (I.) copy operator:
void CP2var::operator << (CP2var& A){
  copyTensorFrom(A);
}

// (I.) operator += : implementation without copy (!!!)
void CP2var::operator += (CP2var& A){
  axpy(A, 1.0, false);
}

// (I.) operator += : implementation without copy (!!!)
void CP2var::operator -= (CP2var& A){
  axpy(A, -1.0, false);
}

// (I.) eval operator:
double CP2var::operator()(unsigned int i, unsigned int j){
  double out = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double contrib = m_R[iTerm].getVecEl(i);
    contrib *= m_S[iTerm].getVecEl(j);
    contrib *= m_coeffs[iTerm];
    out += contrib;
  }
  return out;
}



// (I.) compute the scalar product between 2 CP2var
void dot(CP2var& A, CP2var& B, double& out){
  MPI_Comm comm = A.R(0).comm();
  out = 0.0;
  for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
    for(unsigned int jTerm=0; jTerm<B.rank(); jTerm++){
      vec_petsc tmp_ar = A.R(iTerm);
      vec_petsc tmp_br = B.R(jTerm);
      double contrib = dot(tmp_ar,tmp_br);

      vec_petsc tmp_as = A.S(iTerm);
      vec_petsc tmp_bs = B.S(jTerm);
      double tmp_1 = dot(tmp_as,tmp_bs);
      contrib *= tmp_1;
      contrib *= (A.coeffs(iTerm) * B.coeffs(jTerm));
      out += contrib;
      MPI_Barrier(comm);
    }
  }
}


double operator * (CP2var& A, CP2var& B){
  double out = 0.0;
  for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
    for(unsigned int jTerm=0; jTerm<B.rank(); jTerm++){
      vec_petsc tmp_ar = A.R(iTerm);
      vec_petsc tmp_br = B.R(jTerm);
      double contrib = dot(tmp_ar,tmp_br);

      vec_petsc tmp_as = A.S(iTerm);
      vec_petsc tmp_bs = B.S(jTerm);
      contrib *= dot(tmp_as,tmp_bs);
      contrib *= A.coeffs(iTerm) * B.coeffs(jTerm);
      out += contrib;
    }
  }
  return out;
}




// IMPLEMENTATION OF AUXILIARY FUNCTIONS:

// 1) multiply R_1, R_2ˆT, where both R_1 and R_2 are upper triangular:
void multiply_trg_mat(mat_real& R_1, mat_real& R_2, mat_real& out){
  assert(R_1.nCols()==R_2.nCols());
  out.init(R_1.nRows(),R_2.nRows());
  for(unsigned int iRow=0; iRow<R_1.nRows(); iRow++){
    for(unsigned int jRow=0; jRow<R_2.nRows(); jRow++){
      unsigned int q = (iRow > jRow) ? iRow : jRow;
      double entry = 0.0;
      for(unsigned int k=q; k<R_1.nCols(); k++){
        entry += R_1(iRow,k) * R_2(jRow,k);
      }
      out.setMatEl(iRow,jRow,entry);
    }
  }
}


// 1) multiply R_1 D R_2ˆT, where both R_1 and R_2 are upper triangular, D is diagonal
// the diagonal entries of D are stored in d
void multiply_trg_diag_trg(mat_real& R_1, vector<double>& d, mat_real& R_2, mat_real& out){
  assert(R_1.nCols()==R_2.nCols());
  assert(R_1.nCols()==d.size());
  out.init(R_1.nRows(),R_2.nRows());
  for(unsigned int iRow=0; iRow<R_1.nRows(); iRow++){
    for(unsigned int jRow=0; jRow<R_2.nRows(); jRow++){
      unsigned int q = (iRow > jRow) ? iRow : jRow;
      double entry = 0.0;
      for(unsigned int k=q; k<R_1.nCols(); k++){
        double val1 = R_1(iRow,k);
        double val2 = R_2(jRow,k);
        entry += val1 * val2 * d[k];
      }
      out.setMatEl(iRow,jRow,entry);
    }
  }
}



// I/O: save and load:
void CP2var::save(string f_name){
  // write a log file:
  string log_name = f_name + ".log";
  ofstream log_file(log_name.c_str());
  log_file << m_nDof_var[0] << endl;
  log_file << m_nDof_var[1] << endl;
  log_file << m_rank;
  log_file.close();

  // write the vec_petsc:
  string coeff_name = f_name + "_coeffs.dat";
  ofstream coeff_file(coeff_name.c_str());
  for(unsigned int k=0; k<m_rank; k++){
    stringstream ss;
    ss << k;
    string str_index = ss.str();

    string r_name = f_name + "_r_" + str_index + ".dat";
    m_R[k].save(r_name);

    string s_name = f_name + "_s_" + str_index + ".dat";
    m_S[k].save(s_name);

    coeff_file << m_coeffs[k] << endl;
  }
  coeff_file.close();
}


void CP2var::load(string f_name){
  m_nVar = 2;
  m_nDof_var.resize(2);
  // open the log file and read:
  string log_name = f_name + ".log";
  ifstream log_file(log_name.c_str());
  log_file >> m_nDof_var[0];
  log_file >> m_nDof_var[1];
  log_file >> m_rank;
  log_file.close();

  m_R.resize(m_rank);
  m_S.resize(m_rank);
  m_coeffs.resize(m_rank);
  // load the coeffs:
  string coeff_name = f_name + "_coeffs.dat";
  ifstream coeff_file(coeff_name.c_str());
  for(unsigned int k=0; k<m_rank; k++){
    coeff_file >> m_coeffs[k];
  }
  coeff_file.close();
  // load the modes:
  for(unsigned int k=0; k<m_rank; k++){
    stringstream ss;
    ss << k;
    string str_index = ss.str();

    string r_name = f_name + "_r_" + str_index + ".dat";
    m_R[k].load(r_name, PETSC_COMM_WORLD);

    string s_name = f_name + "_s_" + str_index + ".dat";
    m_S[k].load(s_name, PETSC_COMM_WORLD);
  }
}
