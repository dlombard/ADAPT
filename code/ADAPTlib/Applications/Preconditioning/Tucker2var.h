#ifndef Tucker2var_h
#define Tucker2var_h

// Including headers:
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "qr.h"
#include "pod.h"
//#include "operatorTensor.h"

using namespace std;

// class  to represent the solution:
// particularisation of Tucker format to 2 variables.
class Tucker2var{
private:
  unsigned int m_nVar;
  vector<unsigned int> m_nDof_var;
  vector<unsigned int> m_rank;
  vector<vec_petsc> m_R;
  vector<vec_petsc> m_S;
  mat_real m_A;

public:
  Tucker2var(){
    m_nVar=2;
    m_rank.resize(m_nVar);
    m_rank[0]=0;
    m_rank[1]=0;
  };
  ~Tucker2var(){};

  Tucker2var(vector<unsigned int>);
  Tucker2var(vector<vec_petsc>&, vector<vec_petsc>&, mat_real&);

  void init(vector<unsigned int>);
  void clear(){
    // the core m_A is automatically destroyed
    for(unsigned int k=0; k<m_rank[0]; k++){
      m_R[k].clear();
    }
    for(unsigned int k=0; k<m_rank[1]; k++){
      m_S[k].clear();
    }
  }

  // clear one single term:
  void clear_term_R(unsigned int iTerm){m_R[iTerm].clear();}
  void clear_term_S(unsigned int iTerm){m_S[iTerm].clear();}

  // setters for the terms:
  void set_terms(vector<vec_petsc>&, vector<vec_petsc>&);
  void set_term_R(unsigned int, vec_petsc&);
  void set_term_S(unsigned int, vec_petsc&);
  void set_core(mat_real&); // this is by copy;
  // functions to set the ranks:
  void set_rank(vector<unsigned int> ranks){
    m_rank[0] = ranks[0];
    m_rank[1] = ranks[1];
  }
  void set_rank(unsigned int n_rows, unsigned int n_cols){
    m_rank[0] = n_rows;
    m_rank[1] = n_cols;
  }
  void set_rank_r(unsigned int rank_0){m_rank[0] = rank_0;}
  void set_rank_s(unsigned int rank_1){m_rank[1] = rank_1;}
  void init_core(unsigned int n_rows, unsigned int n_cols){
    m_A.init(n_rows,n_cols); // check that the ranks are conforming.
    m_A.zero();
  }
  void setCoreEl(unsigned int i, unsigned int j, double val){
    m_A.setMatEl(i,j,val);
  }

  // METHODS:
  void copyTensorFrom(Tucker2var&);
  void operator << (Tucker2var& T){copyTensorFrom(T);}
  void scale(double);
  void operator *= (double alpha){scale(alpha);}
  void axpy(Tucker2var&, double);
  void round(double&);
  // norms:
  double norm2();
  double norm(){
    double norm = norm2();
    norm = sqrt(norm);
    return norm;
  }
  double norm_core();
  // zero:
  void zero(){
    m_nVar=2;
    m_rank.resize(m_nVar);
    m_rank[0]=0;
    m_rank[1]=0;
    m_R.resize(0);
    m_S.resize(0);
    m_A.init(0,0);
  }

  // access functions:
  inline vector<unsigned int> rank(){return m_rank;}
  inline unsigned int rank(unsigned int i){return m_rank[i];}
  inline vector<unsigned int> nDof_var(){return m_nDof_var;}
  inline unsigned int nDof_var(unsigned int iVar){return m_nDof_var[iVar];}
  inline vector<vec_petsc> R(){return m_R;}
  inline vec_petsc R(unsigned int iTerm){return m_R[iTerm];}
  inline vector<vec_petsc> S(){return m_S;}
  inline vec_petsc S(unsigned int iTerm){return m_S[iTerm];}
  inline mat_real A(){return m_A;}
  inline double A(unsigned int i, unsigned int j){return m_A(i,j);}
};

// operators taking two Tucker2var as argument:
double operator * (Tucker2var&, Tucker2var&);
void dot(Tucker2var&, Tucker2var&, double&);

#endif
