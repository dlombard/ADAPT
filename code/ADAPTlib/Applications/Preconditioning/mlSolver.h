// header file, multilinear solver

#ifndef mlSolver_h
#define mlSolver_h
#include <string>
#include <vector>
#include <map>
#include <complex>
#include <math.h>
#include <algorithm>
#include <chrono>

#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "pod.h"
#include "qr.h"
#include "CP2var.h"
#include "Tucker2var.h"
#include "operatorTensor.h"
#include "dofwise_ridge.h"

using namespace std;

// Auxiliary: mgs for CP2var tensors:
void mgs(vector<CP2var>&, vector<CP2var>&, mat_real&, double);

void gmres(operatorTensor&, CP2var&, CP2var&, CP2var&, mat&, double, double, unsigned int);
void applyOperator(operatorTensor&, CP2var&, CP2var&);
CP2var applyOperator(operatorTensor&, CP2var&);
CP2var operator * (operatorTensor&, CP2var&);
void applyPreconditioner(KSP&, CP2var&);
void Arnoldi(operatorTensor&, CP2var&, KSP&, vector<CP2var>&, vector<double>&, double, unsigned int);

// preconditioner type 2:
void applyPreconditioner(vector<KSP>&, vector<vector<unsigned int> >&, CP2var&);
void Arnoldi(operatorTensor&, CP2var&, vector<KSP>&, vector<vector<unsigned int> >&, vector<CP2var>&, vector<double>&, double, unsigned int);
void Arnoldi_record(string, operatorTensor&, CP2var&, vector<KSP>&, vector<vector<unsigned int> >&, vector<CP2var>&, vector<double>&, double, unsigned int);
void applyPreconditioner(vector<CP2var>&, vector<double>&, vector<CP2var>&, CP2var&);

// preconditioner type 3:
//void applyPreconditioner(vector<CP2var>&, vector<CP2var>&, mat_real&, CP2var&);
//void Arnoldi(operatorTensor&, CP2var&, vector<CP2var>&, vector<CP2var>&, mat_real&, vector<CP2var>&, vector<double>&, double, unsigned int);
void applyPreconditioner(operatorTensor&, CP2var&, unsigned int, double);
void Arnoldi(operatorTensor&, CP2var&, vector<CP2var>&, vector<double>&, double, unsigned int, unsigned int);
void Arnoldi_record(string, operatorTensor&, CP2var&, vector<CP2var>&, vector<double>&, double, unsigned int, unsigned int);

// preconditioner type 3: efficient coding
void applyPreconditioner(operatorTensor&, KSP&, KSP&, CP2var&, unsigned int, double);
void Arnoldi(operatorTensor&, CP2var&, KSP&, KSP&, unsigned int, vector<CP2var>&, vector<double>&, double, unsigned int);
void Arnoldi_record(string, operatorTensor&, CP2var&, KSP&, KSP&, unsigned int, vector<CP2var>&, vector<double>&, double, unsigned int);

// preconditioner type 3: dynamical preconditioner update
KSP setSolver(mat&, string);
operatorTensor transpose(operatorTensor&);
void applyPreconditioner_dynamic(operatorTensor&, mat&, mat&, CP2var&, unsigned int, double, double);
void Arnoldi(operatorTensor&, CP2var&, mat&, mat&, unsigned int, double, vector<CP2var>&, vector<double>&, double, unsigned int);


// no preconditioner, Arnoldi record:
void Arnoldi_record(string, operatorTensor&, CP2var&, vector<CP2var>&, vector<double>&, double, unsigned int);

// CASE of the PARAMETRIC DOMAINS:
//void applyOperator(operatorTensor&, CP2var&, CP2var&, CP2var&);
//void applyOperator(operatorTensor&, CP2var&, char_fun&, vector<vec_petsc>&, double&, CP2var&);
//void applyPreconditioner(KSP&, CP2var&, char_fun&, vector<vec_petsc>&);
//void Arnoldi(operatorTensor&, CP2var&, KSP&, char_fun&, vector<vec_petsc>&, vector<CP2var>&, vector<double>&, double, unsigned int);

// functions for Tucker2var:
void applyOperator(operatorTensor&, Tucker2var&, Tucker2var&);
void applyPreconditioner(KSP&, Tucker2var&);
void gmres(operatorTensor&, Tucker2var&, Tucker2var&, Tucker2var&, mat&, double, double, unsigned int);
void Arnoldi(operatorTensor&, Tucker2var&, KSP&, vector<Tucker2var>&, vector<double>&, double, unsigned int);
//void Arnoldi(operatorTensor&, Tucker2var&, vector<KSP>&, vector<unsigned int>&, vector<Tucker2var>&, vector<double>&, double, unsigned int);
#endif
