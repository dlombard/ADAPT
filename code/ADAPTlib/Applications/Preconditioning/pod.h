// Header file for POD operations.

#ifndef pod_h
#define pod_h
#include <string>
#include <vector>
#include <map>
#include <complex>
#include <math.h>
#include <algorithm>

#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"

extern "C" {
  extern int dgeev_(char*,char*,int*,double*,int*,double*, double*, double*, int*, double*, int*, double*, int*, int*);
  extern int dsyev_(char*,char*,int*,double*,int*,double*,double*,int*,int*);
  extern int dgesvd_(char*,char*,int*,int*,double*,int*,double*,double*,int*,double*,int*,double*,int*,int*);
}

// function which performs the svd of a mat_real:
void svd_real(mat_real&, vector<vec_real>&, vector<double>&, vector<vec_real>&);

// function to perfor the eigenvalue decomposition of a generic dense matrix:
void eig(mat_real&, vector<vector<double> >&, vector<vec_petsc>&, unsigned int);

// function to perform the eigenvalue decomposition of a symmetric matrix:
void eig_sym(mat_real&, vector<double>&, vector<vec_petsc>&, unsigned int);

// function which perform the POD of a set of vectors:
void pod(vector<vec_petsc>&, vector<double>&, vector<vec_petsc>&);

// function which perform the POD with truncation:
void pod(vector<vec_petsc>&, double, vector<double>&, vector<vec_petsc>&);


#endif
