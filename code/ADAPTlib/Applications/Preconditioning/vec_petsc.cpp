// implementation of the class vec_petsc:

#include "vec.h"

using namespace std;


/* I.1 - overloaded constructors:
  - inputs: vector size, (default=PETSC_COMM_WORLD)
  - output: the fields m_x, m_comm, m_size are initialised
*/
vec_petsc::vec_petsc(int itsSize, MPI_Comm theComm){
  PetscErrorCode ierr;
  ierr = VecCreate(theComm, &m_x);
  ierr = VecSetSizes(m_x, PETSC_DECIDE, itsSize);
  ierr = VecSetFromOptions(m_x);
  m_comm = theComm;
  m_size = itsSize;
}


/* I.2 - init in case of existing objects
  - inputs: vector size, MPI communicator (default=PETSC_COMM_WORLD)
  - output: the fields m_x, m_comm, m_size are initialised
*/
void vec_petsc::init(unsigned int itsSize, MPI_Comm theComm){
    PetscErrorCode ierr;
    ierr = VecCreate(theComm, &m_x);
    ierr = VecSetSizes(m_x, PETSC_DECIDE, itsSize);
    ierr = VecSetFromOptions(m_x);
    m_comm = theComm;
    m_size = itsSize;
}

// I.2.b - overloaded, copy the structure of existing vec using Duplicate
void vec_petsc::init(vec_petsc& b){
   PetscErrorCode ierr;
   ierr = VecDuplicate(b.x(), &m_x);
}


/* I.3 - set a vector element:
  - inputs: component number, value
  - output: m_x[ind] = val
  ! not finalized => finalize after, parallel !
*/
void vec_petsc::setVecEl(int ind, double val){
    int startInd, endInd;
    VecGetOwnershipRange( m_x , &startInd , &endInd );
    if((ind>=startInd) && (ind<endInd)){
      VecSetValues( m_x , 1 , &ind , &val , INSERT_VALUES );
    }
}

// sequential version: proc 0 assign the element to the vector:
void vec_petsc::setVecEl_seq(int ind, double val){
    int rank;
    MPI_Comm_rank (m_comm, &rank);
    if(rank == 0){
      VecSetValues( m_x , 1 , &ind , &val , INSERT_VALUES );
    }
}


/* I.3 - add a vector element:
  - inputs: component number, value
  - output: m_x[ind] += val
  ! not finalized => finalize after
*/
void vec_petsc::addVecEl(int ind, double val){
    int startInd, endInd;
    VecGetOwnershipRange( m_x , &startInd , &endInd );
    if((ind>=startInd) && (ind<endInd)){
      VecSetValues( m_x , 1 , &ind , &val , ADD_VALUES );
    }
}

// sequential version: proc 0 add element:
void vec_petsc::addVecEl_seq(int ind, double val){
  int rank;
  MPI_Comm_rank (m_comm, &rank);
  if(rank == 0){
    VecSetValues( m_x , 1 , &ind , &val , ADD_VALUES );
  }
}


/* I.4 set a vector
  - input: a petsc vector Vec
  - output: setting m_x to this vec, no copy
*/
inline void vec_petsc::setVector(Vec& theVec){
  m_x = theVec;
  PetscInt toBeReturned;
  VecGetSize(m_x, &toBeReturned);
  m_size = toBeReturned;
}


/* I.5 set the vector communicator
  - input: a communicator
  - output: setting m_comm to the input Comm
*/
inline void vec_petsc::setComm(MPI_Comm theComm){
  m_comm = theComm;
}


/* I.6 - get a vector element
  - inputs: component number
  - output: m_x[ind]
  ! all the procs get the value by Bcast !
*/
double vec_petsc::getVecEl(int ind) const {
  double toBeReturned = 0.0;
  int rank;
  MPI_Comm_rank (m_comm, &rank);
  int startInd, endInd;
  VecGetOwnershipRange( m_x , &startInd , &endInd );
  int amIRoot = 0;
  if( (ind >=startInd ) && (ind<endInd) ){
    VecGetValues(m_x, 1 , &ind, &toBeReturned);
    amIRoot = 1;
  }
  int consensus = amIRoot * rank;
  int root = 0;
  MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, m_comm);
  MPI_Bcast(&toBeReturned, 1, MPI_DOUBLE, root, m_comm);

  return toBeReturned;
}

// parallel version, without Bcast:
double vec_petsc::getVecEl_par(int ind) const{
  double toBeReturned = 0.0;
  int rank;
  MPI_Comm_rank (m_comm, &rank);
  int startInd, endInd;
  VecGetOwnershipRange( m_x , &startInd , &endInd );
  int amIRoot = 0;
  if( (ind >=startInd ) && (ind<endInd) ){
    VecGetValues(m_x, 1 , &ind, &toBeReturned);
    amIRoot = 1;
  }
  return toBeReturned;
}

/* I.7 - get a vector size
  - inputs: none
  - output: the size
*/
inline unsigned int vec_petsc::getSize(){
  PetscInt toBeReturned;
  VecGetSize(m_x, &toBeReturned);
  m_size = toBeReturned;
  return toBeReturned;
}


/* I.8 - get a vector 2-norm
  - inputs: none
  - output: the lˆ2 norm
*/
double vec_petsc::norm(){
  double toBeReturned = 0.0;
  VecNorm(m_x, NORM_2, &toBeReturned);
  return toBeReturned;
}

// I.8.b infty-norm of a vector:
double vec_petsc::normInfty(){
  double toBeReturned = 0.0;
  VecNorm(m_x, NORM_INFINITY, &toBeReturned);
  return toBeReturned;
}


/* I.9 - set a vector to 1
  - inputs: none
  - output: m_x[:] = 1
*/
void vec_petsc::ones(){
  VecSet(m_x, 1.0);
  finalize();
}


/* I.10 - copy a vector
  - inputs: a vector to be copied
  - output: this vector is the copy of the given one
*/
void vec_petsc::copyVecFrom(const vec_petsc& vToCopy){
  m_comm = vToCopy.comm();
  m_size = vToCopy.size();
  init(m_size, m_comm);
  VecCopy(vToCopy.x(), m_x);
  finalize();
}

// related operator:
void vec_petsc::operator << (const vec_petsc& vToCopy){copyVecFrom(vToCopy);}



// II -- OPERATIONS:

/* II.1 - scale a vector
  - inputs: a double alpha
  - output: x *= alpha
*/
void vec_petsc::scale(const double alpha) {
  VecScale(m_x, alpha);
}

// related operator:
void vec_petsc::operator *= (const double alpha){
  finalize();
  VecScale(m_x, alpha);
}


/* II.2 - linear combination axpy
  - inputs: a vector v double alpha
  - output: x += alpha * v
*/
void vec_petsc::axpy(const vec_petsc& v, double alpha){
  assert(v.m_comm==m_comm);
  VecAXPY(m_x, alpha, v.m_x);
}

// related operators: summing and subtracting.
void vec_petsc::operator += (vec_petsc toBeAdded){
  finalize();
  VecAXPY(m_x, 1.0, toBeAdded.x());
  finalize();
}

void vec_petsc::operator -= (vec_petsc toBeAdded){
  finalize();
  VecAXPY(m_x, -1.0, toBeAdded.x());
  finalize();
}


/* II.3 check if a vector is empty
  - input: none
  - output: a bool
*/
bool vec_petsc::isEmpty(){
  bool toBeReturned = true;
  getSize();
  if(m_size==0){
    return toBeReturned;
  }
  else{
    for(unsigned int iRow=0; iRow<m_size; iRow++){
      if(fabs(getVecEl(iRow)) > DBL_EPSILON){
        toBeReturned = false;
        break;
      }
    }
  }
  return toBeReturned;
}


/* II.3 check if a vector is equal to a given vector
  - input: a vector to compare with
  - output: a bool
*/
inline bool vec_petsc::operator == (vec_petsc toCheck){
  PetscBool isIt;
  VecEqual(m_x, toCheck.x(), &isIt);
  return isIt;
}


/* II.4 shift a vector by a constant
  - input: a double alpha
  - output: x[i] += alpha, 0<= i <= n-1
*/
inline void vec_petsc::operator += (double alpha){
  finalize();
  VecShift(m_x, alpha);
}


/* II.5 extract a subvector
  - input: lower and upper indices, C convention (contiguous indices)
  - output: a vector containing the subvector
*/
vec_petsc vec_petsc::extractSubvector(unsigned int iLow, unsigned int iUp) const {
  const unsigned int vecSize = iUp-iLow;
  vec_petsc output(vecSize, m_comm);
  for(unsigned int i=0; i<vecSize; i++){
    unsigned int index = i + iLow;
    double val = this->getVecEl(index);
    output.setVecEl(i, val);
  }
  output.finalize();
  return output;
}


// III - I/O operations:

/* III.1 save a vector in binary format
  - input: file name
  - output: the vec is saved in binary format
*/
void vec_petsc::save(string fileName){
  PetscViewer viewer;
  PetscViewerBinaryOpen(m_comm, fileName.c_str(), FILE_MODE_WRITE, &viewer);
  VecView(m_x, viewer);
  PetscViewerDestroy(&viewer);
}


/* III.2 load a vector in binary format
  - input: file name
  - output: the vec is loaded from binary format file
*/
void vec_petsc::load(string fileName){
  PetscViewer viewer;
  PetscViewerBinaryOpen(m_comm, fileName.c_str(), FILE_MODE_READ, &viewer);
  VecCreate(m_comm, &m_x);
  VecLoad(m_x, viewer);
  PetscViewerDestroy(&viewer);
}


/* III.3 load a vector in binary format
  - input: file name
  - output: the vec is loaded from binary format file
*/
void vec_petsc::load(const string& filename, const MPI_Comm& comm = PETSC_COMM_WORLD) {
  m_comm = comm;
  PetscViewer viewer;
  PetscViewerBinaryOpen(m_comm, filename.c_str(), FILE_MODE_READ, &viewer);
  VecCreate(m_comm, &m_x);
  VecLoad(m_x, viewer);
  PetscViewerDestroy(&viewer);
  PetscInt toBeReturned;
  VecGetSize(m_x, &toBeReturned);
  m_size = toBeReturned;
  this->finalize();
}


/* III.4 save a vector in ascii format
  - input: file name
  - output: the vec is saved in ascii format
*/
void vec_petsc::saveASCII(string fileName){
  ofstream outfile;
  outfile.open(fileName.c_str());
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    outfile << getVecEl(iDof) << endl;
  }
  outfile.close();
}


/* III.5 load a vector in ascii format (useful for freefem++)
  - input: file name
  - output: the vec is loaded from binary format file
*/
void vec_petsc::loadASCII(string fileName, MPI_Comm comm=PETSC_COMM_WORLD){
    m_comm = comm;
    vector<double> nonZerosList;
    vector<unsigned int> idList;
    double thisEntry = 0.0;

    ifstream inputFile;
    inputFile.open(fileName.c_str());
    char output[128];
    unsigned int cc = 0;
    if (inputFile.is_open()) {
      while (!inputFile.eof()) {
        inputFile >> output;
        stringstream str;
        str << output;
        str >> thisEntry;
        if(fabs(thisEntry)!=0.){
          idList.push_back(cc);
          nonZerosList.push_back(thisEntry);
        }
        cc += 1;
      }
    }
    else{
      puts("Unable to read file!");
      exit(1);
    }
    cc = cc -1;
    init(cc, m_comm);
    for(unsigned int iEl=0; iEl<nonZerosList.size(); iEl++){
      setVecEl(idList[iEl], nonZerosList[iEl]);
    }
    finalize();
    getSize();
  }

  // load from Freefem++:
  void vec_petsc::loadFreefemVec(string fileName, MPI_Comm comm=PETSC_COMM_WORLD){
    m_comm = comm;
    vector<double> nonZerosList;
    vector<unsigned int> idList;
    double thisEntry = 0.0;

    ifstream inputFile;
    inputFile.open(fileName.c_str());
    char output[128];
    unsigned int cc = 0;
    if (inputFile.is_open()) {
      while (!inputFile.eof()) {
        inputFile >> output;
        stringstream str;
        str << output;
        str >> thisEntry;
        if( (fabs(thisEntry)!=0.) && (cc>0) ){ // cc=0 freefem++ saves the vector size.
          idList.push_back(cc);
          nonZerosList.push_back(thisEntry);
        }
        cc += 1;
      }
    }
    else{
      puts("Unable to read file!");
      exit(1);
    }
    cc = cc - 2; // 1 element was the size
    init(cc, m_comm);
    for(unsigned int iEl=0; iEl<nonZerosList.size(); iEl++){
      setVecEl(idList[iEl], nonZerosList[iEl]);
    }
    finalize();
    getSize();
  }
  
