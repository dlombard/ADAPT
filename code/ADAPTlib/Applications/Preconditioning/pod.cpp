// Implementation of the POD routines:

#include "pod.h"




// svd decomposition of a real dense matrix given in mat_real format:
/*
  - input: a real dense matrix A
  - output: the left singular vectors U, the right singular vectors transposed Vt,
  the singular values S.
*/

/*
dgesvd 	( 	character  	JOBU,
    character  	JOBVT,
    integer  	M,
    integer  	N,
    double precision, dimension( lda, * )  	A,
    integer  	LDA,
    double precision, dimension( * )  	S,
    double precision, dimension( ldu, * )  	U,
    integer  	LDU,
    double precision, dimension( ldvt, * )  	VT,
    integer  	LDVT,
    double precision, dimension( * )  	WORK,
    integer  	LWORK,
    integer  	INFO
  )

  JOBU is CHAR*
        Options for computing the matrix U:
        = 'A':  all M columns of U are returned in array U:
        = 'S':  the first min(m,n) columns of U (the left singular
                vectors) are returned in the array U;
        = 'O':  the first min(m,n) columns of U (the left singular
                vectors) are overwritten on the array A;
        = 'N':  no columns of U (no left singular vectors) are
                computed.
  JOBVT is CHARACTER*1
  Specifies options for computing all or part of the matrix
  V**T:
  = 'A':  all N rows of V**T are returned in the array VT;
  = 'S':  the first min(m,n) rows of V**T (the right singular
          vectors) are returned in the array VT;
  = 'O':  the first min(m,n) rows of V**T (the right singular
          vectors) are overwritten on the array A;
  = 'N':  no rows of V**T (no right singular vectors) are
          computed.
  */
void svd_real(mat_real& A, vector<vec_real>& U_vecs, vector<double>& S, vector<vec_real>& Vt_vecs){
  int m = A.nRows();
  int n = A.nCols();
  int k = min(m,n);//(m < n) ? m : n;
  char jobU = 'S';
  char jobV = 'S';
  int lda = m; //(m > 1) ? m : 1;
  double* s; s = new double[k];
  double* U; U = new double[m*k];
  int ldu = m;
  double* Vt; Vt = new double[k*n];
  int ldvt = n;
  double* work; work = new double[8*k];
  int lw= 8*k; // LWORK >= MAX(1,3*MIN(M,N) + MAX(M,N),5*MIN(M,N))
  int info;

  // copy the matrix;
  double* matrix = A.M();

  dgesvd_(&jobU, &jobV, &m, &n, matrix, &lda, s , U, &ldu, Vt, &ldvt, work, &lw, &info);

  // getting U stored in column-major order:
  U_vecs.resize(k);
  unsigned int cc = 0;
  for(unsigned int i_vec=0; i_vec<k; i_vec++){
    U_vecs[i_vec].init(m);
    for(unsigned int iRow=0; iRow<m; iRow++){
      double val = U[cc];
      U_vecs[i_vec].setVecEl(iRow,val);
      cc += 1;
    }
  }

  // getting Vt stored in row-major order, put them in Vt:
  Vt_vecs.resize(k);
  cc = 0;
  for(unsigned int i_vec=0; i_vec<k; i_vec++){
    Vt_vecs[i_vec].init(m);
    for(unsigned int iRow=0; iRow<m; iRow++){
      double val = Vt[cc];
      Vt_vecs[i_vec].setVecEl(iRow,val);
      cc += 1;
    }
  }

  // get the singular vecots:
  S.resize(k);
  for(unsigned int i=0; i<k; i++){
    S[i] = s[i];
  }

  delete[] U;
  delete[] Vt;
  delete[] s;
  delete[] work;
}





// eigenvalue decomposition of a real dense matrix, using LAPACK:
/*
- input: a generic matrix A (column major order)
- output: the eigenvalues and the eigenvectors
*/
void eig(mat_real& A, vector<vector<double> >& lambda, vector<vec_petsc>& V, unsigned int verbose=0){
  int n = A.nRows();
  assert(n==A.nCols());

  // copy the matrix A, otherwise it would be destroyed
  double* data;
  data = new double[n*n];
  for (int i=0;i<n;i++){
    for (int j=0;j<n;j++){
      data[j*n+i] = A(i,j); // column major order
    }
  }

  // allocate data
  char Nchar='V';
  double *eigReal=new double[n];
  double *eigImag=new double[n];
  double *vl,*vr;
  vl = new double[n*n];
  vr = new double[n*n];
  int one=n;
  int lwork=6*n;
  double *work=new double[lwork];
  int info;

  // calculate eigenvalues using the DGEEV subroutine
  dgeev_(&Nchar,&Nchar,&n,data,&n,eigReal,eigImag,
        vl,&one,vr,&one,
        work,&lwork,&info);


  // check for errors
  if (info!=0){
    cout << "Error: dgeev returned error code " << info << endl;
  }

  // output eigenvalues to stdout
  if(verbose>0){
    cout << "--- Eigenvalues ---" << endl;
    for (int i=0;i<n;i++){
      cout << "( " << eigReal[i] << " , " << eigImag[i] << " )\n";
    }
    cout << endl;
  }

  lambda.resize(n);
  for (int i=0;i<n;i++){
    lambda[i].resize(2);
    lambda[i][0] = eigReal[i];
    lambda[i][1] = eigImag[i];
  }

  // output the eigenvectors:
  if(info==0){
    V.resize(n);
    for(unsigned int j=0; j<n; j++){
      V[j].init(n);
      for(unsigned int i=0; i<n; i++){
        unsigned int ind = j*n + i;
        V[j].setVecEl(i,vr[ind]);
      }
    }
  }
  else{
    cout << "info = " << info << endl;
    cout << "Convergence problem on eigenvalues computation!" << endl;
  }

  // deallocate
  delete [] data;
  delete [] eigReal;
  delete [] eigImag;
  delete [] work;
}




/*
- input: the matrix A (only the lower triangular part can be stored!)
- output: the eigenvlues and the eigenvectors
*/
void eig_sym(mat_real& A, vector<double>& lambda, vector<vec_petsc>& V, unsigned int verbose=0){
  /*
  subroutine dsyev	(	character 	JOBZ, 'N', 'V' => V computes also eigenvectors
    character 	UPLO,  'U', 'L' => upper and lower triangular respectively
    integer 	N, => matrix dimension
    double precision, dimension( lda, N ) 	A, => with 'V' A is destroyed, replaced by eigenvectors.
    integer 	LDA, => leading dimension of the array
    double precision, dimension( * ) 	W,  => output, the eigenvalues in ascending order.
    double precision, dimension( * ) 	WORK,
    integer 	LWORK,
    integer 	INFO
  )
  */

  int n = A.nRows();
  char type = 'V';
  char storage = 'L';
  double *eigenvals; eigenvals = new double[n];
  int l_work = 3*n-1;
  double *work; work = new double[l_work];
  int info;

  // copy the lower triangle matrix entries (not to overwrite A!):
  double* data;
  data = new double[n*n];
  for (int i=0;i<n;i++){
    for (int j=0;j<=i;j++){
      data[j*n+i] = A(i,j); // column major order!
    }
  }

  dsyev_(&type, &storage, &n , data ,&n, eigenvals, work, &l_work, &info);

  if(info==0){
    lambda.resize(n);
    for(unsigned int i=0; i<n; i++){
      lambda[i] = eigenvals[i];
      if(verbose>0){
        cout << eigenvals[i] << endl;
      }
    }
    V.resize(n);
    for(unsigned int j=0; j<n; j++){
      V[j].init(n);
      for(unsigned int i=0; i<n; j++){
        unsigned int ind = j*n + i;
        V[j].setVecEl(i, data[ind]);
      }
    }
  }
  else{
    cout << "info = " << info << endl;
    cout << "Convergence problem on eigenvalues computation!" << endl;
  }


  // free the memory:
  delete [] work;
  delete [] data;
  delete [] eigenvals;
}



// full POD, not truncated:
void pod(vector<vec_petsc>& A, vector<double>& lambda, vector<vec_petsc>& U){
  if(A.size()<A[0].size()){
    // number of vectors, it is also related to the size of the covariance.
    int n = A.size();
    const unsigned int nDofs = A[0].size();

    // compute the lower triangular part of the covariance matrix:
    double* data;
    data = new double[n*n];
    for (int i=0;i<n;i++){
      for (int j=0;j<=i;j++){
        data[j*n+i] = dot(A[i],A[j]); // column major order!
      }
    }
    // data will be overwritten by the eigenvectors.
    char type = 'V';
    char storage = 'L';
    double *eigenvals; eigenvals = new double[n];
    int l_work = 3*n-1;
    double *work; work = new double[l_work];
    int info;
    // computing the eigen decomposition of the covariance:
    dsyev_(&type, &storage, &n , data ,&n, eigenvals, work, &l_work, &info);

    // retuning the eigenvalues and the modes.
    if(info != 0){
      cout << "Warning! Convergence issues." << endl;
    }

    // dsyev_ computes the eigenvalues in ascending order... use n-1-j

    lambda.resize(n);
    U.resize(n);
    for(unsigned int j=0; j<n; j++){
      lambda[j] = fabs(eigenvals[n-1-j]); // unnecessary, but sometimes rounding errors on the smallest.
      U[j].init(nDofs);
      U[j].finalize();
      if(lambda[j]>1.0e-15){ // if not, the modes are empty
        for(unsigned int k=0; k<n; k++){
          unsigned int ind = (n-1-j)*n + k;
          double weight = data[ind];
          U[j].axpy(A[k], weight);
        }
        // element-wise implementation (used for vec_real)
        /*for(unsigned int iDof=0; iDof<nDofs; iDof++){
          double entry = 0.0;
          for(unsigned int k=0; k<n; k++){
            unsigned int ind = (n-1-j)*n + k;
            double weight = data[ind];
            entry += weight * A[k].getVecEl(iDof);
          }
          U[j].setVecEl(iDof, entry);
        }*/
        U[j].finalize();
        // rescale:
        U[j] *= (1.0/lambda[j]);
      // end if
      }
    }
    // free the memory:
    delete[] eigenvals;
    delete[] work;
    delete[] data;

  // end if
  }
  else{
    int n = A[0].size();
    const unsigned int nDofs = A[0].size();

    // compute the covariance:
    // compute the lower triangular part of the covariance matrix:
    double* data;
    data = new double[n*n];
    for (int i=0;i<n;i++){
      for (int j=0;j<=i;j++){
        data[j*n+i] = 0.0;
        for(unsigned int k=0; k<A.size(); k++){
          data[j*n+i] += A[k].getVecEl(i) * A[k].getVecEl(j);
        }
      }
    }

    // data will be overwritten by the eigenvectors.
    char type = 'V';
    char storage = 'L';
    double *eigenvals; eigenvals = new double[n];
    int l_work = 3*n-1;
    double *work; work = new double[l_work];
    int info;
    // computing the eigen decomposition of the covariance:
    dsyev_(&type, &storage, &n , data ,&n, eigenvals, work, &l_work, &info);

    // retuning the eigenvalues and the modes.
    if(info != 0){
      cout << "Warning! Convergence issues." << endl;
    }

    // the modes are stored in data, in reverse order.
    lambda.resize(n);
    U.resize(n);
    for(unsigned int j=0; j<n; j++){
      lambda[j] = fabs(eigenvals[n-1-j]);
      U[j].init(n);
      for(unsigned int i=0; i<n; j++){
        unsigned int ind = (n-1-j)*n + i;
        U[j].setVecEl(i, data[ind]);
      }
      U[j].finalize();
    }
    // free the memory:
    delete[] eigenvals;
    delete[] work;
    delete[] data;

    // end else:
  }
}

// POD, truncated with tol epsilon:
void pod(vector<vec_petsc>& A, double tol, vector<double>& lambda, vector<vec_petsc>& U){
  const unsigned int tol_squared = tol*tol;

  if(A.size()<A[0].size()){
    // number of vectors, it is also related to the size of the covariance.
    int n = A.size();
    const unsigned int nDofs = A[0].size();

    // compute the lower triangular part of the covariance matrix:
    double* data;
    data = new double[n*n];
    for (int i=0;i<n;i++){
      for (int j=0;j<=i;j++){
        data[j*n+i] = dot(A[i],A[j]); // column major order!
      }
    }
    // data will be overwritten by the eigenvectors.
    char type = 'V';
    char storage = 'L';
    double *eigenvals; eigenvals = new double[n];
    int l_work = 3*n-1;
    double *work; work = new double[l_work];
    int info;
    // computing the eigen decomposition of the covariance:
    dsyev_(&type, &storage, &n , data ,&n, eigenvals, work, &l_work, &info);

    // retuning the eigenvalues and the modes.
    if(info != 0){
      cout << "Warning! Convergence issues." << endl;
    }

    // dsyev_ computes the eigenvalues in ascending order... use n-1-j:

    // evaluating the tail:
    double tail = 0.0;
    unsigned int toRetain = n;
    unsigned int cc = 0;
    while (tail<tol_squared){
      tail += eigenvals[cc];
      toRetain = toRetain - 1;
      cc += 1;
    }
    toRetain += 1;

    lambda.resize(toRetain);
    U.resize(toRetain);
    for(unsigned int j=0; j<toRetain; j++){
      lambda[j] = fabs(eigenvals[toRetain-1-j]); // unnecessary, but sometimes rounding errors on the smallest.
      U[j].init(nDofs);
      U[j].finalize();
      if(lambda[j]>1.0e-15){ // if not, the modes are empty
        for(unsigned int k=0; k<n; k++){
          unsigned int ind = (toRetain-1-j)*n + k;
          double weight = data[ind];
          U[j].axpy(A[k], weight);
        }
        /* element-wise implementation used for vec_real
        for(unsigned int iDof=0; iDof<nDofs; iDof++){
          double entry = 0.0;
          for(unsigned int k=0; k<n; k++){
            unsigned int ind = (toRetain-1-j)*n + k;
            double weight = data[ind];
            entry += weight * A[k].getVecEl(iDof);
          }
          U[j].setVecEl(iDof, entry);
        }*/
        U[j].finalize();
        // rescale:
        U[j] *= (1.0/lambda[j]);
      // end if
      }
    }
    // free the memory:
    delete[] eigenvals;
    delete[] work;
    delete[] data;

  // end if
  }
  else{
    int n = A[0].size();
    const unsigned int nDofs = A[0].size();

    // compute the covariance:
    // compute the lower triangular part of the covariance matrix:
    double* data;
    data = new double[n*n];
    for (int i=0;i<n;i++){
      for (int j=0;j<=i;j++){
        data[j*n+i] = 0.0;
        for(unsigned int k=0; k<A.size(); k++){
          data[j*n+i] += A[k].getVecEl(i) * A[k].getVecEl(j);
        }
      }
    }

    // data will be overwritten by the eigenvectors.
    char type = 'V';
    char storage = 'L';
    double *eigenvals; eigenvals = new double[n];
    int l_work = 3*n-1;
    double *work; work = new double[l_work];
    int info;
    // computing the eigen decomposition of the covariance:
    dsyev_(&type, &storage, &n , data ,&n, eigenvals, work, &l_work, &info);

    // retuning the eigenvalues and the modes.
    if(info != 0){
      cout << "Warning! Convergence issues." << endl;
    }

    // evaluating the tail:
    double tail = 0.0;
    unsigned int toRetain = n;
    unsigned int cc = 0;
    while (tail<tol_squared){
      tail += eigenvals[cc];
      toRetain = toRetain - 1;
      cc += 1;
    }
    toRetain += 1;

    // the modes are stored in data, in reverse order.
    lambda.resize(toRetain);
    U.resize(toRetain);
    for(unsigned int j=0; j<n; j++){
      lambda[j] = fabs(eigenvals[toRetain-1-j]);
      U[j].init(n);
      for(unsigned int i=0; i<n; j++){
        unsigned int ind = (toRetain-1-j)*n + i;
        U[j].setVecEl(i, data[ind]);
      }
      U[j].finalize();
    }
    // free the memory:
    delete[] eigenvals;
    delete[] work;
    delete[] data;

    // end else:
  }

}
