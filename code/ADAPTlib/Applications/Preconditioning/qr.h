// Header file for QR operations.

#ifndef qr_h
#define qr_h
#include <string>
#include <vector>
#include <map>
#include <complex>
#include <math.h>
#include <algorithm>

#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"

extern "C" {
  // Lapack routines
}


// extract subvector:
vec_petsc extract_subVec(const vec_petsc&, unsigned int, unsigned int);

// function which perform the QR of a set of vectors:
void qr_householder(vector<vec_petsc>&, vector<vec_petsc>&, mat_real&);

// function which performs qr with Modified G-S:
void qr_mgs(vector<vec_petsc>&, vector<vec_petsc>&, mat_real&);



#endif
