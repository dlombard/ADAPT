// implementation of QR methods:

#include "qr.h"

using namespace std;

// auxiliary function: extract subvector, optimised:
// C convention on indices
/*vec_petsc extract_subVec(const vec_petsc& v, unsigned int iLow, unsigned int iUp){
  IS is;
  PetscInt* indices;
  const unsigned int N = iUp-iLow;
  PetscMalloc1(N, &indices);
  for(unsigned int i=iLow; i<iUp; i++){
    indices[i] = i;
  }
  ISCreateGeneral(v.comm(), N, indices, PETSC_COPY_VALUES, &is);
  PetscFree(indices);

  Vec in = v.x();
  Vec out;
  VecGetSubVector(in, is, &out);
  vec_petsc out_vec;
  out_vec.setVector(out);
  return out_vec;
}*/



// implementation of QR decomposition, Householder, with input copy
// input: a vector<vec> A_in
// output: orthonormal vectors Q, upper triangular matrix R
// adding a pivoting to deal with rank deficient A_in
void qr_householder(vector<vec_petsc>& A_in, vector<vec_petsc>& Q, mat_real& R){
  const unsigned int nRows = A_in[0].size();
  const unsigned int nCols = A_in.size();

  // copy the inputs, A will be modified.
  vector<vec_petsc> A(nCols);
  for(unsigned int jCol=0; jCol<nCols; jCol++){
    A[jCol] << A_in[jCol];
  }

  // vector of indices:
  vector<unsigned int> sing_ind;
  vector<unsigned int> lin_ind;

  // for loop in classical QR:
  for(unsigned int jCol=0; jCol<nCols; jCol++){
    vec_petsc col_vec; // copy the jCol; put zeros from 0 to jCol
    col_vec << A[jCol];
    for(unsigned int iRow=0; iRow<jCol; iRow++){
      col_vec.setVecEl(iRow, 0.0);
    }
    col_vec.finalize();

    // computing the reflector:
    double norm_vec = col_vec.norm();
    double r_jj = A[jCol].getVecEl(jCol);
    double s = (r_jj > 0) ? -1.0 : 1.0;
    double u_1 = r_jj - s*norm_vec;

    if( (fabs(norm_vec)<=DBL_EPSILON) || (fabs(u_1)<=DBL_EPSILON) ){
      sing_ind.push_back(jCol);
    }
    else{
      lin_ind.push_back(jCol);
      u_1 = 1.0/u_1;
      col_vec *= u_1;
      col_vec.setVecEl(jCol, 1.0);
      col_vec.finalize();

      double tau = -s/(u_1*norm_vec);

      // correct A except the columns from 0 to jCol included;
      // apply the Householder reflector:
      for(unsigned int kCol=0; kCol<nCols; kCol++){
        double alpha_col= -tau * dot(col_vec, A[kCol]);
        A[kCol].axpy(col_vec, alpha_col);
      }
    }

    // free the memory:
    col_vec.clear();
  }

  // initialise the matrix R:
  const unsigned int num_Q = lin_ind.size();
  //cout << "Num Cols = " << nCols << "  num lin ind = " << num_Q << endl;

  R.init(nCols, nCols);

  // fill the real matrix R:
  for(unsigned int jCol=0; jCol<nCols; jCol++){
    for(unsigned int iRow=0; iRow<=jCol; iRow++){
        double val = A[jCol].getVecEl(iRow);
        R.setMatEl(iRow, jCol, val);
    }
  }
  // correct R entries:
  for(unsigned int jS=0; jS<sing_ind.size(); jS++){
    unsigned int iRow = sing_ind[jS];
    for(unsigned int jCol=0; jCol<nCols; jCol++){
      R.setMatEl(iRow,jCol,0.0);
    }
  }


  // compute the matrix Q entries:
  Q.resize(nCols);
  for(unsigned int jS=0; jS<sing_ind.size(); jS++){
    unsigned int jCol = sing_ind[jS];
    Q[jCol].init(nRows);
    Q[jCol].finalize();
  }
  for(unsigned int jQ=0; jQ<num_Q; jQ++){
    unsigned int jCol = lin_ind[jQ];
    Q[jCol] << A_in[jCol];
    for(unsigned int iRow=0; iRow<jCol; iRow++){
      double alpha = -R(iRow,jCol);
      Q[jCol].axpy(Q[iRow], alpha);
    }
    double n_q = R(jCol,jCol);
    n_q = 1.0/n_q;
    Q[jCol] *= n_q;
    //Q[jQ].print();
  }

  // free the memory:
  for(unsigned int jCol=0; jCol<nCols; jCol++){
    A[jCol].clear();
  }
  A.clear();
}


// without overwriting the inputs:



// Implementation of MGS with pivoting:
void qr_mgs(vector<vec_petsc>& A, vector<vec_petsc>& Q, mat_real& R){
  const double tol = 1.0e-12;
  const unsigned int nCols = A.size();
  const unsigned int nRows = A[0].size();

  unsigned int k=0;
  double tmp;
  vector<bool> isZero(A.size(), false);
  vector<unsigned int> I, J;
  vector<double> val;

  Q.resize(nCols);
  for(unsigned int i=0; i<nCols; ++i) {
    Q[i] << A[i];
  }

  //Starting the MGS algorithm:
  for (unsigned int i=0; i<Q.size(); ++i) {
    tmp = Q[i].norm();
    if (tmp>tol) {
      I.push_back(k);
      J.push_back(i);
      val.push_back(tmp);
      Q[i] *= (1.0/tmp);
      for (unsigned int j=i+1; j<nCols; ++j) {
        tmp = dot(Q[i], Q[j]);
        if (fabs(tmp) != 0.0) {
          I.push_back(k);
          J.push_back(j);
          val.push_back(tmp);
          Q[j].axpy(Q[i], -tmp);
        }
      }
      ++k;
    } else {
      isZero[i] = true;
    }
  }

  for (unsigned int i=Q.size()-1; i<Q.size(); --i) {
    if (isZero[i]) {
      Q.erase(std::next(Q.begin(), i));
    }
  }

  R.init(nCols, nCols);
  for (unsigned int i=0; i<val.size(); ++i) {
    R.setMatEl(I[i], J[i], val[i]);
  }
}
