#ifndef CP2var_h
#define CP2var_h

// Including headers:
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "qr.h"
#include "pod.h"
//#include "operatorTensor.h"

using namespace std;
extern "C" {
  extern int dgesvd_(char*,char*,int*,int*,double*,int*,double*,double*,int*,double*,int*,double*,int*,int*);
}

// class  to represent the solution:
// particularisation of CP format to 3 variables.
class CP2var{
private:
  unsigned int m_nVar;
  vector<unsigned int> m_nDof_var;
  unsigned int m_rank;
  vector<vec_petsc> m_R;
  vector<vec_petsc> m_S;
  vector<double> m_coeffs;

public:
  CP2var(){m_nVar = 2; m_rank=0;};
  CP2var(vector<unsigned int>);
  CP2var(vector<vec_petsc>&, vector<vec_petsc>&);
  CP2var(vec_petsc&, vec_petsc&);
  CP2var(vector<vec_petsc>&, vector<vec_petsc>&, vector<double>&);
  CP2var(vec_petsc&, vec_petsc&, double&);

  ~CP2var(){};

  void set_rank(unsigned int k){m_rank=k;}
  // set the terms of the CP2var to a given set of modes:
  void set_terms(vector<vec_petsc>&, vector<vec_petsc>&);
  // overloaded rank 1:
  void set_terms(vec_petsc&, vec_petsc&);

  // set a single term into a specific position:
  void set_term_R(unsigned int, vec_petsc&);

  void set_term_S(unsigned int, vec_petsc&);

  void set_terms(unsigned int, vec_petsc&, vec_petsc&);

  // set the coefficients: !by reference!
  void set_coeffs(vector<double>&);

  // overloaded: set the value of one coefficient:
  void set_coeffs(unsigned int, double);

  // copy the coefficients:
  inline void copy_coeffs(vector<double>& cs){
    m_coeffs.resize(cs.size());
    for(unsigned int i=0; i<cs.size(); i++){
      m_coeffs[i] = cs[i];
    }
  }

  // multiply a single term by a double:
  void rescale_term(unsigned int iTerm, double alpha){
    m_coeffs[iTerm] *= alpha;
  }

  // multiply the whole tensor by a scalar:
  inline void scale(double alpha){
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      m_coeffs[iTerm] *= alpha;
    }
  }

  // clear a term:
  inline void clear_term_R(unsigned int id_term){
    m_R[id_term].clear();
  }

  inline void clear_term_S(unsigned int id_term){
    m_S[id_term].clear();
  }

  inline void clear_term(unsigned int id_term){
    m_R[id_term].clear();
    m_S[id_term].clear();
    m_coeffs.erase(m_coeffs.begin()+id_term);
  }

  // clear the whole tensor:
  void clear(){
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      m_R[iTerm].clear();
      m_S[iTerm].clear();
      m_coeffs.clear();
    }
    m_rank = 0;
  }

  void init(vector<unsigned int> n_dofs){
    m_nVar = 2;
    m_rank = 0;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_nDof_var[iVar] = n_dofs[iVar];
    }
  }

  // METHODS:
  void copyTensorFrom(CP2var&);
  void zero();
  void zero(vector<unsigned int>);
  void append(vec_petsc&, vec_petsc&);
  void append(vector<vec_petsc>&, vector<vec_petsc>&);
  void append(vec_petsc&, vec_petsc&, double&);
  void append(vector<vec_petsc>&, vector<vec_petsc>&, vector<double>&);
  void axpy(CP2var&, double, bool);
  void axpy(vec_petsc&, vec_petsc&, double, bool);
  double norm2CP();
  double norm(){return sqrt(norm2CP());};
  // after a rounding operation, the norm is just the coeffs norm.
  double norm_orth(){
    double out = 0.0;
    for(unsigned int k=0; k<m_rank; k++){
      out += m_coeffs[k] * m_coeffs[k];
    }
    out = sqrt(out);
    return out;
  }
  void round(double&);

  void save(string);
  void load(string);


  // OPERATORS:
  void operator *= (double);
  void operator << (CP2var&);
  void operator += (CP2var&);
  void operator -= (CP2var&);
  double operator ()(unsigned int i, unsigned int j);

  // ACCESS FUNCTIONS:
  inline vector<unsigned int> nDof_var(){return m_nDof_var;}
  inline unsigned int nDof_var(unsigned int iVar){return m_nDof_var[iVar];}
  inline unsigned int rank(){return m_rank;}
  inline vector<vec_petsc> R(){return m_R;}
  inline vec_petsc R(unsigned int iTerm){return m_R[iTerm];}
  inline vector<vec_petsc> S(){return m_S;}
  inline vec_petsc S(unsigned int iTerm){return m_S[iTerm];}
  inline vector<double> coeffs(){return m_coeffs;}
  inline double coeffs(unsigned int iTerm){return m_coeffs[iTerm];}
  inline void print(){
    cout << "Rank = " << m_rank << endl;
  }
};

// operators between two CP2var:
void dot(CP2var&, CP2var&, double&);
double operator * (CP2var&, CP2var&);

// auxiliary functions for compression:
void multiply_trg_mat(mat_real&, mat_real&, mat_real&);
void multiply_trg_diag_trg(mat_real&, vector<double>&, mat_real&, mat_real&);


#endif
