// Parallel tensor implementation
#include <stdio.h>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ostream>
#include <string>
#include <vector>
#include <map>
#include <complex>
#include <math.h>
#include <ios>
#include <algorithm>
#include <random>
#include <chrono>
#include <cfloat>
#include <iomanip>

using namespace std;
using namespace std::chrono;

#include <mpi.h>
#include "vec.h"
#include "fft.h"
#include "qr.h"
#include "pod.h"
#include "linearAlgebraOperations.h"
#include "CP2var.h"
#include "Tucker2var.h"
#include "mlSolver.h"

extern "C"{
  extern int dgesvd_(char*,char*,int*,int*,double*,int*,double*,double*,int*,double*,int*,double*,int*,int*);
}

// second type preconditioner:
void create_preconditioner(unsigned int n_prec, vector<double> bounds, vector<vector<double> >& theta_pos, mat& K, mat& M, vector<KSP>& P, vector<mat>& mat_list, vector<vector<unsigned int> >& indices){
  double Th_1_min = bounds[0];
  double Th_1_max = bounds[1];
  double Th_2_min = bounds[2];
  double Th_2_max = bounds[3];

  const double dth_1 = (Th_1_max - Th_1_min)/n_prec;
  const double dth_2 = (Th_2_max - Th_2_min)/n_prec;

  const unsigned int nMat = n_prec * n_prec;
  vector<double> pos_1(n_prec);
  vector<double> pos_2(n_prec);
  for(unsigned int i=0; i<n_prec; i++){
    double th_1_m = i*dth_1;
    double th_1_p = (i+1)*dth_1;
    pos_1[i] = 0.5 * (th_1_m + th_1_p);

    double th_2_m = i*dth_2;
    double th_2_p = (i+1)*dth_2;
    pos_2[i] = 0.5 * (th_2_m + th_2_p);
  }

  // construct the preconditioners:
  P.resize(nMat);
  mat_list.resize(nMat);
  unsigned int iMat = 0;
  for(unsigned int i=0; i<n_prec; i++){
    double p_1 = pos_1[i];
    for(unsigned int j=0; j<n_prec; j++){
      double p_2 = pos_2[j];

      mat P_i(K.nRows(), K.nCols(), K.comm());
      P_i.finalize();
      P_i.sum(M,p_2);
      P_i.sum(K,p_1);
      P_i.finalize();

      mat_list[iMat] = P_i;

      KSP m_prec;
      PC m_pc;
      KSPCreate(PETSC_COMM_WORLD, &m_prec);
      KSPSetOperators(m_prec, P_i.M(), P_i.M());
      //KSPSetType(prec, KSPGMRES);
      KSPSetType(m_prec, KSPPREONLY);
      KSPGetPC(m_prec, &m_pc);
      PCSetType(m_pc, PCCHOLESKY); // PCCHOLESKY
      //KSPSetTolerances(m_prec, 1.0e-14, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);
      KSPSetFromOptions(m_prec);
      KSPSetUp(m_prec);
      P[iMat] = m_prec;

      iMat += 1;
    }
  }


  // compute the matrix of distances:
  indices.resize(nMat);

  for(unsigned int iPt=0; iPt<theta_pos.size(); iPt++){
    double th_1 = theta_pos[iPt][0];
    double th_2 = theta_pos[iPt][1];
    double p_1 = pos_1[0];
    double p_2 = pos_2[0];
    double min_dist = sqrt( (th_1-p_1)*(th_1-p_1) + (th_2-p_2)*(th_2-p_2) );
    unsigned int arg_min = 0;

    unsigned int iMat = 0;
    for(unsigned int i=0; i<n_prec; i++){
      double p_1 = pos_1[i];
      for(unsigned int j=0; j<n_prec; j++){
        double p_2 = pos_2[j];
        double dist = sqrt( (th_1-p_1)*(th_1-p_1) + (th_2-p_2)*(th_2-p_2) );
        if(dist<min_dist){
          min_dist = dist;
          arg_min = iMat;
        }
        iMat += 1;
      }
    }
    indices[arg_min].push_back(iPt);
  }

}




int main(int argc, char **args){

  static char help[] = "Testing\n\n";
  PetscInitialize(&argc,&args,(char*)0,help);

  // Number of procs, and the ranks
  int nOfProcs;
  MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
  int idProc;
  MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


  // load discretisation matrices and vectors:
  mat M;
  //M.loadFreefemCOOMatrix("M.txt", PETSC_COMM_WORLD);
  M.load("M.dat", PETSC_COMM_WORLD);

  mat K;
  //K.loadFreefemCOOMatrix("K.txt", PETSC_COMM_WORLD);
  K.load("K.dat", PETSC_COMM_WORLD);


  vec_petsc b_x;
  //b_x.loadFreefemVec("b.txt", PETSC_COMM_WORLD);
  //b_x.save("b.dat");
  b_x.load("b.dat", PETSC_COMM_WORLD);

  const unsigned int N_x = M.nRows();


  // assembling the matrices in theta:
  const unsigned int N_theta_1 = 100;
  const unsigned int N_theta_2 = 100;
  const unsigned int N_theta = N_theta_1 * N_theta_2;

  mat Th_1(N_theta,N_theta, PETSC_COMM_WORLD);
  mat Th_2(N_theta,N_theta, PETSC_COMM_WORLD);
  vector<vector<double> > theta_pos(N_theta);

  unsigned int cc = 0;
  for(unsigned int i=0; i<N_theta_1; i++){
    double t_1 = 0.1 + (10.0-0.1)/(N_theta_1-1) * i;
    for(unsigned int j=0; j<N_theta_2; j++){
      double t_2 = 0.1 + (10.0-0.1)/(N_theta_2-1) * j;
      Th_1.setMatEl(cc,cc,t_1);
      Th_2.setMatEl(cc,cc,t_2);
      theta_pos[cc].resize(2);
      theta_pos[cc][0] = t_1;
      theta_pos[cc][1] = t_2;
      cc += 1;
    }
  }
  Th_1.finalize();
  Th_2.finalize();


  // assembling the tensorised operator:
  vector<vector<mat> > op_tab(2);
  op_tab[0].resize(2);
  op_tab[1].resize(2);
  op_tab[0][0] = K;
  op_tab[0][1] = Th_1;
  op_tab[1][0] = M;
  op_tab[1][1] = Th_2;

  operatorTensor A(op_tab);


  // Preconditioner (1) matrix:
  /*
  double theta_1_bar = 0.5*(0.1 + 10.0);
  double theta_2_bar = 0.5*(0.1 + 10.0);
  mat P(N_x,N_x,PETSC_COMM_WORLD);
  P.finalize();
  P.sum(M,theta_2_bar);
  P.sum(K,theta_1_bar);
  P.finalize();
  KSP prec;
  PC m_pc;
  KSPCreate(PETSC_COMM_WORLD,&prec);
  KSPSetOperators(prec, P.M(), P.M());
  //KSPSetType(prec, KSPGMRES);
  KSPSetType(prec, KSPPREONLY);
  KSPGetPC(prec, &m_pc);
  PCSetType(m_pc, PCCHOLESKY); // PCCHOLESKY
  //KSPSetTolerances(prec, 1.0e-14, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);
  KSPSetFromOptions(prec);
  KSPSetUp(prec);*/


  // Preconditioner (2):
  vector<mat> mat_list;
  vector<KSP> P_list;
  vector<vector<unsigned int> > ind_table;
  create_preconditioner(2, {0.1,10.0,0.1,10.0}, theta_pos, K, M, P_list, mat_list, ind_table);


  mat P0(N_x,N_x,PETSC_COMM_WORLD);
  for(unsigned int iDof=0; iDof<N_x; iDof++){
    P0.setMatEl(iDof,iDof, 1.0);
  }
  P0.finalize();


  // assembling the right-hand side:
  vec_petsc b_theta(N_theta, PETSC_COMM_WORLD);
  unsigned int iTh = 0;
  for(unsigned int i=0; i<N_theta_1; i++){
    double t_1 = 0.1 + (10.0-0.1)/(N_theta_1-1) * i;
    for(unsigned int j=0; j<N_theta_2; j++){
      double t_2 = 0.1 + (10.0-0.1)/(N_theta_2-1) * j;
      double val = 0.5*(t_1 + t_2);
      b_theta.setVecEl(iTh,val);
      iTh += 1;
    }
  }
  b_theta.finalize();

  CP2var b(b_x,b_theta);


  // initial guess:
  CP2var sol_0; sol_0.zero({N_x,N_theta});
  CP2var sol({N_x,N_theta});

  double tolR = 1.0e-6;
  double tolC = 1.0e-6;
  unsigned int maxIt = 40;
  cout << "Entering gmres: " << endl;

  auto start = high_resolution_clock::now();
  //gmres(A, b, sol_0, sol, P, tolR, tolC, maxIt);


  CP2var residual; residual << b;
  double norm_init = b.norm();
  if(idProc==0){
    cout << "residual norm = " << norm_init << endl;
  }



  vector<CP2var> Q;
  vector<double> y;
  //Arnoldi(A, residual, prec, Q, y, tolC, maxIt);
  Arnoldi(A, residual, P_list, ind_table, Q, y, tolC, maxIt);

  // assemble the solution:
  CP2var tmpSol;
  tmpSol << Q[0]; tmpSol *= y[0];
  for(unsigned int sub_it=1; sub_it<=maxIt; sub_it++){
    tmpSol.axpy(Q[sub_it],y[sub_it], true);
  }
  tmpSol.round(tolC);
  if(idProc==0){
    cout << "sol rank after round = " << tmpSol.rank() << endl;
  }
  // compute the residual:
  CP2var res;
  res << b;
  CP2var Ax = applyOperator(A,tmpSol);
  //residual -= Ax;
  res.axpy(Ax, -1.0, true);
  Ax.clear();

  res.round(tolC);
  double res_norm = res.norm();
  if(idProc==0){
    cout << "residual norm = " << res_norm << endl;
  }


  // free the memory:
  //KSPDestroy(&prec);
  for(unsigned int iP=0; iP<P_list.size(); iP++){
    mat_list[iP].clear();
    KSPDestroy(&P_list[iP]);
  }

  for(unsigned int it=0; it<maxIt+1; it++){
    Q[it].clear();
  }
  residual.clear();



  auto stop = high_resolution_clock::now();
  auto duration = duration_cast<microseconds>(stop - start);

  if(idProc==0){
    cout << "Elapsed time = " << duration.count()/1000000.0 << " s" << endl;
  }



  cout << "exit" << endl;



  //free the memory:
  b.clear();
  sol_0.clear();
  sol.clear();
  K.clear();
  //P.clear();
  P0.clear();
  M.clear();
  Th_1.clear();
  Th_2.clear();

  PetscFinalize();

  return 0;

}
