// Parallel tensor implementation
#include <stdio.h>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ostream>
#include <string>
#include <vector>
#include <map>
#include <complex>
#include <math.h>
#include <ios>
#include <algorithm>
#include <random>
#include <chrono>
#include <cfloat>
#include <iomanip>

using namespace std;
using namespace std::chrono;

#include <mpi.h>
#include "vec.h"
#include "fft.h"
#include "qr.h"
#include "linearAlgebraOperations.h"


int main(int argc, char **args){

  static char help[] = "Testing\n\n";
  PetscInitialize(&argc,&args,(char*)0,help);

  // Number of procs, and the ranks
  int nOfProcs;
  MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
  int idProc;
  MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


  const double pi = 4.0*atan(1.0);

  unsigned int N = 20000001;

  vec_petsc u(N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    double val = sin(2.0*pi*iDof/(N-1));
    u.setVecEl(iDof, val);
  }
  u.finalize();

  vec_petsc v(N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    double val = cos(2.0*pi*iDof/(N-1));
    v.setVecEl(iDof, val);
  }
  v.finalize();

  vec_petsc w(N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    double val = cos(2.0*pi*iDof/(N-1)) * iDof/(N-1);
    w.setVecEl(iDof, val);
  }
  w.finalize();


  /*
  vec_petsc u(4);
  u.setVecEl(0,1.0); u.setVecEl(1,2.0);   u.setVecEl(2,3.0); u.setVecEl(3,4.0);
  u.finalize();


  vec_petsc v(4);
  v.setVecEl(0,5.0); v.setVecEl(1,6.0);   v.setVecEl(2,7.0); v.setVecEl(3,8.0);
  v.finalize();

  vec_petsc w(4);
  w.setVecEl(0,1.0); w.setVecEl(1,3.0);   w.setVecEl(2,7.0); w.setVecEl(3,9.0);
  w.finalize();
  */

  // testing QR:
  vector<vec_petsc> A = {u,v,w};
  vector<vec_petsc> Q;
  mat_real R;
  auto start = high_resolution_clock::now();
  //qr_mgs(A,Q,R);
  qr_householder(A,Q,R);
  auto stop = high_resolution_clock::now();
  auto duration = duration_cast<microseconds>(stop - start);

  cout << "Elapsed time = " << duration.count()/1000000.0 << " s" << endl;

  //Q[0].print();
  //Q[1].print();


  if(idProc==0){
    cout << " Printing R:" << endl;
    for(unsigned int iRow=0; iRow<R.nRows(); iRow++){
      for(unsigned int jCol=0; jCol<R.nCols(); jCol++){
        cout << iRow << "," << jCol << " ==>  " <<  R(iRow,jCol) << endl;
      }
    }
  }

  /*
  Q[0].print();
  Q[1].print();
  Q[2].print();
  */

  double a_11 = dot(Q[0], Q[0]);
  double a_12 = dot(Q[0], Q[1]);
  double a_22 = dot(Q[1], Q[1]);
  if(idProc==0){
    cout << " Checking Q:" << endl;
    cout <<  a_11 << endl;
    cout << a_12 << endl;
    cout << a_22 << endl;
  }


  // free the memory:
  Q[0].clear();
  Q[1].clear();
  A[0].clear();
  A[1].clear();



  PetscFinalize();

  return 0;

}
