  //Parameters (imputs) to get the spiral solution:


    unsigned int nx = 256; //number of point in space
    int nx2 = nx * nx;
    double T = 2000.0; //Total time
    int nt = 400; //Number of time steps
    double dt = T/nt; 


    double eps = linspace(0.0015, 0.006, 10);
    double gamma = 1;
    double k = linspace(0.96, 1.2, 5);
    double a = 0.1;
    double nu = 1./62500; //Normalized

    double beta = 1./dt + eps*gamma
