aa// Parallel tensor implementation
#include "genericInclude.h"
#include "linAlg/linAlg.h"
#include "CP/CP.h"
#include "fullTensor/fullTens.h"
#include "TensorTrain/SoTT.h"
#include "compression/CPTensorCompression.hpp"
#include "Applications/Network/Network.h"
#include "Applications/Network/locHOSVD.h"
#include <string>

using namespace std;


/* Linspace function: */
vector<double> linspace(double start, double end, int num){

  vector<double> linspaced(num);


  if (num == 0) { return linspaced; }
  if (num == 1) {
      linspaced.push_back(start);
      return linspaced;
    }

  double delta = (end - start) / (num - 1.0);

  for(unsigned int i=0; i < num; ++i){
    linspaced[i] = start + delta * i;
  }
  //linspaced.push_back(end);  I want to ensure that start and end are exactly the same as the input
  return linspaced;
}



//Function to save the modes
void saveField_2d(vec& sol, unsigned int Nx, unsigned int Ny, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;

 for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();

}



//Function to save the modes
void saveField_3d(vec& sol, unsigned int Nx, unsigned int Ny, unsigned int Nz, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = Nz;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;

 for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();

}



void saveField_2d_txt(vec& sol, unsigned int Nx, unsigned int Ny, string& fName){
 string saveName = fName + ".txt";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

  for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();
}



// Simple test on vectors

int main(int argc, char **args){

  static char help[] = "Testing\n\n";

  SlepcInitialize(&argc,&args,(char*)0,help);

  if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

  // Number of procs, and the ranks
  int nOfProcs;
  MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
  int idProc;
  MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


    unsigned int begining = 0;
    unsigned int end = 200;
    unsigned int t = end-begining;
    vector<vec> u(t);
    unsigned int Dofsx = 256 * 256;

    vec un(Dofsx, PETSC_COMM_WORLD);
    unsigned int ciT = 0;


    for (unsigned int iT = begining; iT < end; iT++) {
      if (iT%1 == 0){
        string uName = string("Applications/FitzHughNagumo/Outputs/FHNu_spiral/FHNu/u_") + to_string(iT) + string(".txt");
        if (iT%20 == 0){cout << uName << endl;}
        un = loadVecASCII(uName, PETSC_COMM_WORLD);
        u[ciT].copyVecFrom(un);
        ciT +=1;
      }
    }
    un.clear();

    //Space time
    unsigned int counter =0;
    vec solToPlot(t*Dofsx,PETSC_COMM_WORLD);
    for (size_t iText = 0; iText < t; iText++) {
      for (size_t iX = 0; iX < Dofsx; iX++) {
        double value = u[iText].getVecEl(iX);
        solToPlot.setVecEl(counter, value);
        counter +=1;
      }
    }
    solToPlot.finalize();
    string testName = "test3d_500";
    saveField_3d(solToPlot, 256, 256, t, testName);
    solToPlot.clear();



    //To extract the information in t:
    vector<vec> u_t(Dofsx);
    vec un_t(t, PETSC_COMM_WORLD);

    for (unsigned int iX = 0; iX < Dofsx; iX++) {
      for (unsigned int iT = begining; iT < end; iT++) {
        un_t.setVecEl(iT, u[iT](iX));
      }
      un_t.finalize();
      u_t[iX].copyVecFrom(un_t);
    }
    un_t.clear();

    cout<< "Changed the format of our fibers base" << endl;
    cout << "the number of columns= " << u_t.size() << endl;
    cout << "the number of rows= " << u_t[0].size() << endl;


    vector<mat> Basis;
    vector<vector<unsigned int>> indOnBasis;
    vector<double> errBudget;
    vector<mat> Basis_t;
    vector<vector<unsigned int>> indOnBasis_t;
    vector<double> errBudget_t;
    Network net;
    double normalization = t * Dofsx;
    double error = 1.0e-2 * sqrt(normalization);

    //Obtaining the basis with respect to the x
    net.ComputeBases(u, error, Basis, indOnBasis, errBudget);
    if (Basis.size() != 1) {
      net.Merge2dBases(u, Basis, indOnBasis, errBudget);
    }

    //Obtaining the basis with respect to the t
    net.ComputeBases(u_t, error, Basis_t, indOnBasis_t, errBudget_t);
    if (Basis_t.size() != 1) {
      net.Merge2dBases(u_t, Basis_t, indOnBasis_t, errBudget_t);
    }

    unsigned int total_memory = 0;
    unsigned int total_memory_t = 0;


    for (size_t iGroup = 0; iGroup < indOnBasis.size(); iGroup++) {
      total_memory += Dofsx * Basis[iGroup].nCols() + Basis[iGroup].nCols() * indOnBasis[iGroup].size();
    }
    cout << "Total memory = " << total_memory << " in comparison with the full tensor memory : " << Dofsx*t << endl;
    double compression_ratio_TD = (total_memory * 1.0)/(Dofsx*t);


    for (size_t iGroup = 0; iGroup < indOnBasis_t.size(); iGroup++) {
      total_memory_t += t * Basis_t[iGroup].nCols() + Basis_t[iGroup].nCols() * indOnBasis_t[iGroup].size();
    }
    cout << "Total memory = " << total_memory_t << " in comparison with the full tensor memory : " << Dofsx*t << endl;
    double compression_ratio_TD_t = (total_memory_t * 1.0)/(Dofsx*t);

    double compression_ratio_TD_mean = 0.5 *(total_memory + total_memory_t);
    compression_ratio_TD_mean *= 1.0/(Dofsx*t);


    //Computing the lHOSVD (working but just for 2d and gives worst results than HOSVD)
    //Computing the cores:
    // vector<vector<mat>> vecCorx = vectorCoreX(u, indOnBasis_t, Basis, Basis_t);
    // vector<vector<mat>> vecCort = vectorCoreT(u_t, indOnBasis, Basis, Basis_t);
    // cout << "computed cores in t" << endl;

    double error_sum = 0.0;
    double error_sum_t = 0.0;
    double error_sum_mean = 0.0;



    //Computing basis and core for the HOSVD:
    mat Ux=computeHOSVDbasis(u, error * error);
    mat Ut=computeHOSVDbasis(u_t, error * error);

    vector<mat> basisHOSVD(2);
    basisHOSVD[0] = Ux;
    basisHOSVD[1] = Ut;

    mat core = computeHOSVDcore(u, basisHOSVD);

    unsigned int memory_pod = Ux.nCols() * (Dofsx + t);
    double compression_ratio_POD = (memory_pod * 1.0)/(Dofsx*t);

    double error_sum_HOSVD = 0.0;


    //Computing the basis and core for the new version of the local HOSVD

    double error_sum_localHOSVD = 0.0;


    unsigned int memory_locpod = 0;
    double compression_ratio_locPOD = 0.0;


    vector<vector<mat>> localCoresVec(Basis.size());
    vector<vector<vector<mat>>> locBasisHOSVDVec(Basis.size());
    vector<vector<bool>> isItNon0(Basis.size());

    for (unsigned int iBase = 0; iBase < Basis.size(); iBase++) {
      localCoresVec[iBase].resize(Basis_t.size());
      locBasisHOSVDVec[iBase].resize(Basis_t.size());
      isItNon0[iBase].resize(Basis_t.size());
      for (unsigned int jBase = 0; jBase < Basis_t.size(); jBase++) {


          vector<vec> theFibers(indOnBasis[iBase].size());
          vector<vec> theFibers_t(indOnBasis_t[jBase].size());

          for (unsigned int iFib = 0; iFib < indOnBasis[iBase].size(); iFib++) {
            //theFibers[iFib].resize(indOnBasis_t[jBase].size());
            vec insideVec(indOnBasis_t[jBase].size(), PETSC_COMM_WORLD);
            for (unsigned int jFib = 0; jFib < indOnBasis_t[jBase].size(); jFib++) {
              double val = u[indOnBasis[iBase][iFib]].getVecEl(indOnBasis_t[jBase][jFib]);
              //theFibers[iFib](jFib) = val;
              insideVec(jFib) = val;
            }
            theFibers[iFib] = insideVec;
            theFibers[iFib].finalize();
          }


          for (unsigned int jFib = 0; jFib < indOnBasis_t[jBase].size(); jFib++) {
            //theFibers_t[jFib].resize(indOnBasis[iBase].size());
            vec insideVec_t(indOnBasis[iBase].size(), PETSC_COMM_WORLD);
            for (unsigned int iFib = 0; iFib < indOnBasis[iBase].size(); iFib++) {
              double val = u_t[indOnBasis_t[jBase][jFib]].getVecEl(indOnBasis[iBase][iFib]);
              //theFibers_t[jFib](iFib) = val;
              insideVec_t(iFib) = val;
            }
            theFibers_t[jFib] = insideVec_t;
            theFibers_t[jFib].finalize();
          }
          cout << theFibers.size() << " for the t " << theFibers_t.size() << endl;
          double l2 = 0.0;
          for (unsigned int iFib = 0; iFib < theFibers.size(); iFib++) {
            for (unsigned int jFib = 0; jFib < theFibers_t.size(); jFib++) {
              double val = theFibers[iFib](jFib)*theFibers[iFib](jFib);
              l2 += val;
            }
          }

          if (l2 > error * error/(Basis.size() * Basis_t.size())) {
            cout << "I'm entering" << endl;
            isItNon0[iBase][jBase] = true;
            mat Uxi=computeHOSVDbasis(theFibers, error * error/(Basis.size() * Basis_t.size()));
            mat Utj=computeHOSVDbasis(theFibers_t, error * error/(Basis.size() * Basis_t.size()));

            locBasisHOSVDVec[iBase][jBase].resize(2);
            locBasisHOSVDVec[iBase][jBase][0]=Uxi;
            locBasisHOSVDVec[iBase][jBase][1]=Utj;

            mat lCore = computeHOSVDcore(theFibers, locBasisHOSVDVec[iBase][jBase]);

            localCoresVec[iBase][jBase]=lCore;

            memory_locpod += Uxi.nCols() * (theFibers.size() + theFibers_t.size());
          } else{isItNon0[iBase][jBase] = false;}
      }
    }




    //The reconstruction of the function for every x and t in the grid

    vector<vec> u_rec_HOSVD(t);
    vec u_j_rec_HOSVD(Dofsx, PETSC_COMM_WORLD);

    vector<vec> u_rec_localHOSVD(t);
    vec u_j_rec_localHOSVD(Dofsx, PETSC_COMM_WORLD);

    unsigned int position = 0;
    unsigned int position_j = 0;

    vector<unsigned int> theGrid(2);
    for (unsigned int jGrid = 0; jGrid < t; jGrid++) {
      for (unsigned int iGrid = 0; iGrid < Dofsx; iGrid++) {

        theGrid[0]=iGrid;
        theGrid[1]=jGrid;

        //reconstruction with the local HOSVD (works but it doesn't perform good)
        // double rec = recFx(vecCorx[i], Basis_t, Basis[i], iGrid, jGrid);
        // double rec_t = recFt(vecCort[j], Basis, Basis_t[j], iGrid, jGrid);
        // double mean_rec = 0.5 * (rec + rec_t);
        // un_rec(iGrid)= mean_rec;
        //
        //
        // double trueval = u[iGrid](jGrid);
        //
        // double the_error = trueval - rec;
        // double the_error_t = trueval - rec_t;
        // double the_error_mean = trueval - mean_rec;
        //
        // error_sum += the_error * the_error;
        // error_sum_t += the_error_t * the_error_t;
        // error_sum_mean += the_error_mean * the_error_mean;

        //Reconstruction of the function with HOSVD
        double recVal_HOSVD;
        recVal_HOSVD = reconstructionHOSVD(core, basisHOSVD, theGrid);
        u_j_rec_HOSVD(iGrid)= recVal_HOSVD;

        double the_error_HOSVD = u[jGrid](iGrid) - recVal_HOSVD;
        error_sum_HOSVD += the_error_HOSVD * the_error_HOSVD;



          //Reconstruction of the function with the new alternative of local HOSVD
          //The basis
          unsigned int i = findBasis(Basis, indOnBasis, jGrid);
          unsigned int j = findBasis(Basis_t, indOnBasis_t, iGrid);
          cout << "i = " << i << " and j = " << j <<" found" << endl;

          //The mapping
          unsigned int ind = 0;
          for (unsigned int iInd = 0; iInd < indOnBasis[i].size(); iInd++) {
            if(indOnBasis[i][iInd] == jGrid){
              ind = iInd;
              break;
            }
          }
          position = indOnBasis[i][ind];

          cout << "the position in the local grid is : " << ind << endl;

          unsigned int ind_t = 0;
          for (unsigned int jInd = 0; jInd < indOnBasis_t[j].size(); jInd++) {
            if(indOnBasis_t[j][jInd] == iGrid){
              ind_t = jInd;
              break;
            }
          }
          position_j = indOnBasis_t[j][ind_t];


          cout << "the position in the local grid for t is : " << ind_t << endl;
          vector<unsigned int> theLocalGrid(2);
          theLocalGrid[0] = ind_t;
          theLocalGrid[1] = ind;


          if(isItNon0[i][j] == true){
            double recVal_localHOSVD = reconstructionHOSVD(localCoresVec[i][j], locBasisHOSVDVec[i][j], theLocalGrid);

            u_j_rec_localHOSVD(iGrid)= recVal_localHOSVD;
            double the_error_localHOSVD = u[jGrid](iGrid) - recVal_localHOSVD;
            error_sum_localHOSVD += the_error_localHOSVD * the_error_localHOSVD;
            } else{
              u_j_rec_localHOSVD(iGrid)= 0.0;
              double the_error_localHOSVD = u[jGrid](iGrid);
            }
          }


        u_rec_HOSVD[jGrid].copyVecFrom(u_j_rec_HOSVD);
        u_j_rec_HOSVD.clear(); //Memory changes
        u_rec_localHOSVD[jGrid].copyVecFrom(u_j_rec_localHOSVD);
        u_j_rec_localHOSVD.clear();

      }
      compression_ratio_locPOD = (memory_locpod * 1.0)/(Dofsx * t);

    //
    // error_sum = sqrt(error_sum);
    // error_sum *= 1/(sqrt(Dofsx * t));
    // cout << "the global error = " << error_sum << endl;
    //
    // error_sum_t = sqrt(error_sum_t);
    // error_sum_t *= 1/(sqrt(Dofsx * t));
    // cout << "the global error in t = " << error_sum_t << endl;
    //
    // error_sum_mean = sqrt(error_sum_mean);
    // error_sum_mean *= 1/(sqrt(Dofsx * t));
    // cout << "the global error of the mean = " << error_sum_mean << endl;

    error_sum_HOSVD = sqrt(error_sum_HOSVD);
    error_sum_HOSVD *= 1/(sqrt(Dofsx * t));
    cout << "the global error of the HOSVD = " << error_sum_HOSVD << endl;

    error_sum_localHOSVD = sqrt(error_sum_localHOSVD);
    error_sum_localHOSVD *= 1/(sqrt(Dofsx * t));
    cout << "the global error of the new alternative for the local HOSVD = " << error_sum_localHOSVD << endl;




    // ofstream outfile;
    // for (unsigned int j = 0; j < t; j++) {
    //   string recName = "Frec_" + to_string(t) + "_" + to_string(j) +".txt";
    //   outfile.open(recName.c_str());
    //   for (size_t i = 0; i < Dofsx; i++) {
    //     outfile << u_rec[j](i) << flush << endl;
    //   }
    //   outfile.close();
    // }

    ofstream outfile;
    for (unsigned int j = 0; j < t; j++) {
      string recName = "FrecHOSVD_" + to_string(t) + "_" + to_string(j) +".txt";
      outfile.open(recName.c_str());
      for (size_t i = 0; i < Dofsx; i++) {
        outfile << u_rec_HOSVD[j](i) << flush << endl;
      }
      outfile.close();
    }

    for (unsigned int j = 0; j < t; j++) {
      string recName = "FrecLocHOSVD_" + to_string(t) + "_" + to_string(j) +".txt";
      outfile.open(recName.c_str());
      for (size_t i = 0; i < Dofsx; i++) {
        outfile << u_rec_localHOSVD[j](i) << flush << endl;
      }
      outfile.close();
    }


    // ofstream outfile_TD;
    // string d_TD;
    // string outFileName_TD = string("Compression_TD_") + to_string(t) + string(".txt") ;
    // outfile_TD.open(outFileName_TD.c_str());
    // outfile_TD << compression_ratio_TD << flush << endl;
    // outfile_TD.close();
    //
    // ofstream outfile_TD_t;
    // string d_TD_t;
    // string outFileName_TD_t = string("Compression_TD_t_") + to_string(t) + string(".txt") ;
    // outfile_TD_t.open(outFileName_TD_t.c_str());
    // outfile_TD_t << compression_ratio_TD_t << flush << endl;
    // outfile_TD_t.close();
    //
    // ofstream outfile_TD_mean;
    // string d_TD_mean;
    // string outFileName_TD_mean = string("Compression_TD_mean_") + to_string(t) + string(".txt") ;
    // outfile_TD_mean.open(outFileName_TD_mean.c_str());
    // outfile_TD_mean << compression_ratio_TD_mean << flush << endl;
    // outfile_TD_mean.close();


    ofstream outfile_POD;
    string d_POD;
    string outFileName_POD = string("Compression_POD_") + to_string(t) +string(".txt") ;
    outfile_POD.open(outFileName_POD.c_str());
    outfile_POD << compression_ratio_POD << flush << endl;
    outfile_POD.close();

    ofstream outfile_locPOD;
    string outFileName_locPOD = string("Compression_locPOD_") + to_string(t) +string(".txt") ;
    outfile_locPOD.open(outFileName_locPOD.c_str());
    outfile_locPOD << compression_ratio_locPOD << flush << endl;
    outfile_locPOD.close();


    // ofstream outfile_TD_error;
    // string d_TD_error;
    // string outFileName_TD_error = string("Error_TD_") + to_string(t) + string(".txt") ;
    // outfile_TD_error.open(outFileName_TD_error.c_str());
    // outfile_TD_error << error_sum << flush << endl;
    // outfile_TD_error.close();
    //
    // ofstream outfile_TD_t_error;
    // string d_TD_t_error;
    // string outFileName_TD_t_error = string("Error_TD_t_") + to_string(t) + string(".txt") ;
    // outfile_TD_t_error.open(outFileName_TD_t_error.c_str());
    // outfile_TD_t_error << error_sum_t << flush << endl;
    // outfile_TD_t_error.close();
    //
    // ofstream outfile_TD_mean_error;
    // string d_TD_mean_error;
    // string outFileName_TD_mean_error = string("Error_TD_mean_") + to_string(t) + string(".txt") ;
    // outfile_TD_mean_error.open(outFileName_TD_mean_error.c_str());
    // outfile_TD_mean_error << error_sum_mean << flush << endl;
    // outfile_TD_mean_error.close();


    ofstream outfile_POD_error;
    string d_POD_error;
    string outFileName_POD_error = string("Error_POD_") + to_string(t) +string(".txt") ;
    outfile_POD_error.open(outFileName_POD_error.c_str());
    outfile_POD_error << error_sum_HOSVD << flush << endl;
    outfile_POD_error.close();

    ofstream outfile_locPOD_error;
    string d_locPOD_error;
    string outFileName_locPOD_error = string("Error_locPOD_") + to_string(t) +string(".txt") ;
    outfile_locPOD_error.open(outFileName_locPOD_error.c_str());
    outfile_locPOD_error << error_sum_localHOSVD << flush << endl;
    outfile_locPOD_error.close();



    cout << "start saving basis and modes" << endl;

    for (size_t iBas = 0; iBas < Basis.size(); iBas++) {
      string outFileName = string("Base_")+ to_string(iBas) + string(".txt") ;
      outfile.open(outFileName.c_str());
      for (size_t iVec = 0; iVec < Basis[iBas].nCols(); iVec++) {
         for (size_t iEl = 0; iEl < Basis[iBas].nRows(); iEl++) {
           outfile << Basis[iBas](iEl,iVec) << flush << endl;
         }
      }
      outfile.close();
    }

    ofstream outfile_t;
    string d_t;
    for (size_t iBas = 0; iBas < Basis_t.size(); iBas++) {
      string outFileName_t = string("Base_t_")+ to_string(iBas) + string(".txt") ;
      outfile_t.open(outFileName_t.c_str());
      for (size_t iVec = 0; iVec < Basis_t[iBas].nCols(); iVec++) {
         for (size_t iEl = 0; iEl < Basis_t[iBas].nRows(); iEl++) {
           outfile_t << Basis_t[iBas](iEl,iVec) << flush << endl;
         }
      }
      outfile_t.close();
    }

    ofstream outfile_HOSVD;
    string d_HOSVD;
    string outFileName_HOSVD = string("Base_HOSVD")+ string(".txt") ;
    outfile_HOSVD.open(outFileName_HOSVD.c_str());
    for (size_t iVec = 0; iVec < basisHOSVD[0].nCols(); iVec++) {
       for (size_t iEl = 0; iEl < basisHOSVD[0].nRows(); iEl++) {
         outfile_HOSVD << basisHOSVD[0](iEl,iVec) << flush << endl;
       }
    }
    outfile_HOSVD.close();

    // ofstream outfile_locHOSVD;
    // string d_locHOSVD;
    // string outFileName_locHOSVD = string("Base_locHOSVD")+ string(".txt") ;
    // outfile_locHOSVD.open(outFileName_locHOSVD.c_str());
    // for (size_t iVec = 0; iVec < basisHOSVD[0].nCols(); iVec++) {
    //    for (size_t iEl = 0; iEl < basisHOSVD[0].nRows(); iEl++) {
    //      outfile_locHOSVD << basisHOSVD[0](iEl,iVec) << flush << endl;
    //    }
    // }
    // outfile_locHOSVD.close();



    // string modeName;
    // for (size_t iBas = 0; iBas < Basis.size(); iBas++) {
    //   for (size_t iVec = 0; iVec < Basis[iBas].nCols(); iVec++) {
    //     modeName = "Applications/FitzHughNagumo/Outputs/mode_" + to_string(iBas) + "_" + to_string(iVec);
    //     vec toSave = Basis[iBas].getCol(iVec);
    //     saveField_2d(toSave,256,256, modeName);
    //     saveField_2d_txt(toSave,256,256, modeName);
    //   }
    // }
    //
    // string modeName_t;
    // for (size_t iBas = 0; iBas < Basis_t.size(); iBas++) {
    //   for (size_t iVec = 0; iVec < Basis_t[iBas].nCols(); iVec++) {
    //     modeName_t = "Applications/FitzHughNagumo/Outputs/mode_t_" + to_string(iBas) + "_" + to_string(iVec);
    //     vec toSave_t = Basis_t[iBas].getCol(iVec);
    //     saveField_2d(toSave_t,256,256, modeName_t);
    //     saveField_2d_txt(toSave_t,256,256, modeName_t);
    //   }
    // }

    string modeName_HOSVD;
    for (size_t iVec = 0; iVec < basisHOSVD[0].nCols(); iVec++) {
      modeName_HOSVD = "Applications/FitzHughNagumo/Outputs/mode_HOSVD_" + to_string(iVec);
      vec toSave_HOSVD = basisHOSVD[0].getCol(iVec);
      saveField_2d(toSave_HOSVD,256,256, modeName_HOSVD);
      saveField_2d_txt(toSave_HOSVD,256,256, modeName_HOSVD);
    }

    string modeName_locHOSVD;
    for (size_t iVec = 0; iVec < Basis_t[0].nCols(); iVec++) {
      modeName_locHOSVD = "Applications/FitzHughNagumo/Outputs/mode_locHOSVD_" + to_string(iVec);
      vec toSave_locHOSVD = Basis_t[0].getCol(iVec);
      saveField_2d(toSave_locHOSVD,256,256, modeName_locHOSVD);
      saveField_2d_txt(toSave_locHOSVD,256,256, modeName_locHOSVD);
    }

    //Reconstructed function with local HOSVD
    unsigned int count =0;
    vec recVecToPlot(t*Dofsx,PETSC_COMM_WORLD);
    for (size_t iText = 0; iText < t; iText++) {
      for (size_t iX = 0; iX < Dofsx; iX++) {
        double val = u_rec_localHOSVD[iText](iX);
        recVecToPlot.setVecEl(count, val);
        count +=1;
      }
    }

    recVecToPlot.finalize();
    string testRecName = "recLocalHosvdTest3d";
    saveField_3d(recVecToPlot, 256, 256, t, testRecName);
    recVecToPlot.clear();


    //Reconstructed function with global HOSVD
    count =0;
    vec hosvdVecToPlot(t*Dofsx,PETSC_COMM_WORLD);
    for (size_t iText = 0; iText < t; iText++) {
      for (size_t iX = 0; iX < Dofsx; iX++) {
        double val = u_rec_HOSVD[iText](iX);
        hosvdVecToPlot.setVecEl(count, val);
        count +=1;
      }
    }

    hosvdVecToPlot.finalize();
    string testRecHosvdName = "recHosvdTest3d";
    saveField_3d(hosvdVecToPlot, 256, 256, t, testRecHosvdName);
    hosvdVecToPlot.clear();



  SlepcFinalize();
  return 0;
}
