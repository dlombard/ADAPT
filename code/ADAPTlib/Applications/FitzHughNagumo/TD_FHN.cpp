// Parallel tensor implementation
#include "genericInclude.h"
#include "linAlg/linAlg.h"
#include "CP/CP.h"
#include "fullTensor/fullTens.h"
#include "TensorTrain/SoTT.h"
#include "compression/CPTensorCompression.hpp"
#include "Applications/Network/Network.h"
#include "Applications/Network/locHOSVD.h"
#include <string>

using namespace std;


/* Linspace function: */
vector<double> linspace(double start, double end, int num){

  vector<double> linspaced(num);


  if (num == 0) { return linspaced; }
  if (num == 1) {
      linspaced.push_back(start);
      return linspaced;
    }

  double delta = (end - start) / (num - 1.0);

  for(unsigned int i=0; i < num; ++i){
    linspaced[i] = start + delta * i;
  }
  //linspaced.push_back(end);  I want to ensure that start and end are exactly the same as the input
  return linspaced;
}



//Function to save the modes
void saveField_2d(vec& sol, unsigned int Nx, unsigned int Ny, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;

 for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();

}



//Function to save the modes
void saveField_3d(vec& sol, unsigned int Nx, unsigned int Ny, unsigned int Nz, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = Nz;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;

 for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();

}



void saveField_2d_txt(vec& sol, unsigned int Nx, unsigned int Ny, string& fName){
 string saveName = fName + ".txt";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

  for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();
}


void saveField_3d_txt(vec& sol, unsigned int Nx, unsigned int Ny, unsigned int Nz, string& fName){
 string saveName = fName + ".txt";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = Nz;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

  for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();
}



// Compute the Tensor Dust part of the localHOSVD for the FHN equation.
//It computes the part of the basis in x

int main(int argc, char **args){

  static char help[] = "Testing\n\n";

  SlepcInitialize(&argc,&args,(char*)0,help);

  if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

  // Number of procs, and the ranks
  int nOfProcs;
  MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
  int idProc;
  MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


    unsigned int begining = 0;
    unsigned int end = 200;
    unsigned int t = end-begining;
    vector<vec> u(t);
    unsigned int Dofsx = 256 * 256;

    vec un(Dofsx, PETSC_COMM_WORLD);
    unsigned int ciT = 0;


    for (unsigned int iT = begining; iT < end; iT++) {
      if (iT%1 == 0){
        string uName = string("Applications/FitzHughNagumo/Outputs/FHNu_spiral/FHNu/u_") + to_string(iT) + string(".txt");
        if (iT%20 == 0){cout << uName << endl;}
        un = loadVecASCII(uName, PETSC_COMM_WORLD);
        u[ciT].copyVecFrom(un);
        ciT +=1;
      }
    }
    un.clear();

    //Space time
    // unsigned int counter =0;
    // vec solToPlot(t*Dofsx,PETSC_COMM_WORLD);
    // for (size_t iText = 0; iText < t; iText++) {
    //   for (size_t iX = 0; iX < Dofsx; iX++) {
    //     double value = u[iText].getVecEl(iX);
    //     solToPlot.setVecEl(counter, value);
    //     counter +=1;
    //   }
    // }
    // solToPlot.finalize();
    // string testName = "test3d_500";
    // saveField_3d(solToPlot, 256, 256, t, testName);
    // solToPlot.clear();


    vector<mat> Basis;
    vector<vector<unsigned int>> indOnBasis;
    vector<double> errBudget;
    Network net;
    double normalization = t * Dofsx;
    double error = 1.0e-2 * sqrt(normalization);

    //Obtaining the basis with respect to the x
    net.ComputeBases(u, error, Basis, indOnBasis, errBudget);
    if (Basis.size() != 1) {
      net.Merge2dBases(u, Basis, indOnBasis, errBudget);
    }


    unsigned int total_memory = 0;


    for (size_t iGroup = 0; iGroup < indOnBasis.size(); iGroup++) {
      total_memory += Dofsx * Basis[iGroup].nCols() + Basis[iGroup].nCols() * indOnBasis[iGroup].size();
    }
    cout << "Total memory = " << total_memory << " in comparison with the full tensor memory : " << Dofsx*t << endl;
    double compression_ratio_TD = (total_memory * 1.0)/(Dofsx*t);

    double error_sum = 0.0;


    cout << "start saving" << endl;

    ofstream outfile;
    for (size_t iBas = 0; iBas < Basis.size(); iBas++) {
      string outFileName = string("localHOSVD_FHN_parts/Base_")+ to_string(iBas) + string(".txt") ;
      outfile.open(outFileName.c_str());
      for (size_t iVec = 0; iVec < Basis[iBas].nCols(); iVec++) {
         for (size_t iEl = 0; iEl < Basis[iBas].nRows(); iEl++) {
           outfile << Basis[iBas](iEl,iVec) << flush << endl;
         }
      }
      outfile.close();
    }

    for (size_t iBas = 0; iBas < Basis.size(); iBas++) {
      string outFileName = string("localHOSVD_FHN_parts/colsBasis_")+ to_string(iBas) + string(".txt") ;
      outfile.open(outFileName.c_str());
      outfile << Basis[iBas].nCols() << flush << endl;
      outfile.close();
    }

    for (size_t iBas = 0; iBas < Basis.size(); iBas++) {
      string outFileName = string("localHOSVD_FHN_parts/rowsBasis_")+ to_string(iBas) + string(".txt") ;
      outfile.open(outFileName.c_str());
      outfile << Basis[iBas].nRows() << flush << endl;
      outfile.close();
    }

    for (size_t iBas = 0; iBas < indOnBasis.size(); iBas++) {
      string outFileName = string("localHOSVD_FHN_parts/indOnBasis_")+ to_string(iBas) + string(".txt") ;
      outfile.open(outFileName.c_str());
       for (size_t iFib = 0; iFib < indOnBasis[iBas].size(); iFib++) {
         outfile << indOnBasis[iBas][iFib] << flush << endl;
       }
      outfile.close();
    }

    for (size_t iBas = 0; iBas < errBudget.size(); iBas++) {
      string outFileName = string("localHOSVD_FHN_parts/errorBudget_")+ to_string(iBas) + string(".txt") ;
      outfile.open(outFileName.c_str());
      outfile << errBudget[iBas] << flush << endl;
      outfile.close();
    }



  SlepcFinalize();
  return 0;
}
