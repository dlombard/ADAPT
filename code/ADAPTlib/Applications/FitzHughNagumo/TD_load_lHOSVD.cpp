// Parallel tensor implementation
#include "genericInclude.h"
#include "linAlg/linAlg.h"
#include "CP/CP.h"
#include "fullTensor/fullTens.h"
#include "TensorTrain/SoTT.h"
#include "compression/CPTensorCompression.hpp"
#include "Applications/Network/Network.h"
#include "Applications/Network/locHOSVD.h"
#include <string>

using namespace std;


/* Linspace function: */
vector<double> linspace(double start, double end, int num){

  vector<double> linspaced(num);


  if (num == 0) { return linspaced; }
  if (num == 1) {
      linspaced.push_back(start);
      return linspaced;
    }

  double delta = (end - start) / (num - 1.0);

  for(unsigned int i=0; i < num; ++i){
    linspaced[i] = start + delta * i;
  }
  //linspaced.push_back(end);  I want to ensure that start and end are exactly the same as the input
  return linspaced;
}



//Function to save the modes
void saveField_2d(vec& sol, unsigned int Nx, unsigned int Ny, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;

 for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();

}



//Function to save the modes
void saveField_3d(vec& sol, unsigned int Nx, unsigned int Ny, unsigned int Nz, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = Nz;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;

 for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();

}



void saveField_2d_txt(vec& sol, unsigned int Nx, unsigned int Ny, string& fName){
 string saveName = fName + ".txt";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

  for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();
}



// Simple test on vectors

int main(int argc, char **args){

  static char help[] = "Testing\n\n";

  SlepcInitialize(&argc,&args,(char*)0,help);

  if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

  // Number of procs, and the ranks
  int nOfProcs;
  MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
  int idProc;
  MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


    unsigned int begining = 0;
    unsigned int end = 200;
    unsigned int t = end-begining;
    vector<vec> u(t);
    unsigned int Dofsx = 128 * 128;

    vec un(Dofsx, PETSC_COMM_WORLD);
    unsigned int ciT = 0;


    for (unsigned int iT = begining; iT < end; iT++) {
      if (iT%1 == 0){
        string uName = string("Applications/FitzHughNagumo/Outputs/FHNu_spiral/FHNu_nx128/u_") + to_string(iT) + string(".txt");
        if (iT%20 == 0){cout << uName << endl;}
        un = loadVecASCII(uName, PETSC_COMM_WORLD);
        u[ciT].copyVecFrom(un);
        ciT +=1;
      }
    }
    un.clear();

    //Space time
    unsigned int counter =0;
    vec solToPlot(t*Dofsx,PETSC_COMM_WORLD);
    for (size_t iText = 0; iText < t; iText++) {
      for (size_t iX = 0; iX < Dofsx; iX++) {
        double value = u[iText].getVecEl(iX);
        solToPlot.setVecEl(counter, value);
        counter +=1;
      }
    }
    solToPlot.finalize();
    string testName = "test3d_500";
    saveField_3d(solToPlot, 256, 256, t, testName);
    solToPlot.clear();



    //To extract the information in t:
    vector<vec> u_t(Dofsx);
    vec un_t(t, PETSC_COMM_WORLD);

    for (unsigned int iX = 0; iX < Dofsx; iX++) {
      for (unsigned int iT = begining; iT < end; iT++) {
        un_t.setVecEl(iT, u[iT](iX));
      }
      un_t.finalize();
      u_t[iX].copyVecFrom(un_t);
    }
    un_t.clear();

    cout<< "Changed the format of our fibers base" << endl;
    cout << "the number of columns= " << u_t.size() << endl;
    cout << "the number of rows= " << u_t[0].size() << endl;

    unsigned int nBasis = 28;
    unsigned int nBasis_t = 205;

    vector<mat> Basis (nBasis);
    vector<vector<unsigned int>> indOnBasis (nBasis);
    vector<double> errBudget (nBasis);
    vector<mat> Basis_t (nBasis_t);
    vector<vector<unsigned int>> indOnBasis_t (nBasis_t);
    vector<double> errBudget_t (nBasis_t);
    Network net;
    double normalization = t * Dofsx;
    double error = 1.0e-2 * sqrt(normalization);

    //Obtaining the basis with respect to the x

    vec colsB(nBasis, PETSC_COMM_WORLD);
    for (unsigned int iBas = 0; iBas < nBasis; iBas++) {
      string bName = string("localHOSVD_FHN_parts/colsBasis_") + to_string(iBas) + string(".txt");
      colsB = loadVecASCII(bName, PETSC_COMM_WORLD);
    }
    colsB.finalize();

    vec rowsB(nBasis, PETSC_COMM_WORLD);
    for (unsigned int iBas = 0; iBas < nBasis; iBas++) {
      string bName = string("localHOSVD_FHN_parts/rowsBasis_") + to_string(iBas) + string(".txt");
      rowsB = loadVecASCII(bName, PETSC_COMM_WORLD);
    }
    rowsB.finalize();


    for (size_t iBas = 0; iBas < nBasis; iBas++) {
      string bName = string("localHOSVD_FHN_parts/Base_")+ to_string(iBas) + string(".txt");
      mat bMat (colsB(iBas), rowsB(iBas));
      vec bVec = loadVecASCII(bName, PETSC_COMM_WORLD);
      for (size_t iCol = 0; iCol < colsB(iBas); iCol++) {
        for (size_t iRow = 0; iRow < rowsB(iBas); iRow++) {
          double val = bVec(iCol*rowsB(iBas) + iRow);
          bMat(iCol,iRow) = val;
        }
      }
      bMat.finalize();
      Basis[iBas]=bMat;
    }


    //Obtaining the basis with respect to the t
    vec colsBt(nBasis_t, PETSC_COMM_WORLD);
    for (unsigned int iBas = 0; iBas < nBasis_t; iBas++) {
      string bName = string("localHOSVD_FHN_parts/colsBasis_t_") + to_string(iBas) + string(".txt");
      colsBt = loadVecASCII(bName, PETSC_COMM_WORLD);
    }
    colsBt.finalize();

    vec rowsBt(nBasis_t, PETSC_COMM_WORLD);
    for (unsigned int iBas = 0; iBas < nBasis_t; iBas++) {
      string bName = string("localHOSVD_FHN_parts/rowsBasis_t_") + to_string(iBas) + string(".txt");
      rowsBt = loadVecASCII(bName, PETSC_COMM_WORLD);
    }
    rowsBt.finalize();


    for (size_t iBas = 0; iBas < nBasis_t; iBas++) {
      string bName = string("localHOSVD_FHN_parts/Base_t_")+ to_string(iBas) + string(".txt");
      mat bMatt (colsBt(iBas), rowsBt(iBas));
      vec bVec = loadVecASCII(bName, PETSC_COMM_WORLD);
      for (size_t iCol = 0; iCol < colsBt(iBas); iCol++) {
        for (size_t iRow = 0; iRow < rowsBt(iBas); iRow++) {
          double val = bVec(iCol*rowsBt(iBas) + iRow);
          bMatt(iCol,iRow) = val;
        }
      }
      bMatt.finalize();
      Basis_t[iBas]= bMatt;
    }


    double error_sum = 0.0;
    double error_sum_t = 0.0;


    //Computing basis and core for the HOSVD:
    mat Ux=computeHOSVDbasis(u, error * error);
    mat Ut=computeHOSVDbasis(u_t, error * error);

    vector<mat> basisHOSVD(2);
    basisHOSVD[0] = Ux;
    basisHOSVD[1] = Ut;

    mat core = computeHOSVDcore(u, basisHOSVD);

    unsigned int memory_pod = Ux.nCols() * (Dofsx + t);
    double compression_ratio_POD = (memory_pod * 1.0)/(Dofsx*t);

    double error_sum_HOSVD = 0.0;


    //Computing the basis and core for the new version of the local HOSVD

    double error_sum_localHOSVD = 0.0;


    unsigned int memory_locpod = 0;
    double compression_ratio_locPOD = 0.0;


    vector<vector<mat>> localCoresVec(Basis.size());
    vector<vector<vector<mat>>> locBasisHOSVDVec(Basis.size());
    vector<vector<bool>> isItNon0(Basis.size());

    for (unsigned int iBase = 0; iBase < Basis.size(); iBase++) {
      localCoresVec[iBase].resize(Basis_t.size());
      locBasisHOSVDVec[iBase].resize(Basis_t.size());
      isItNon0[iBase].resize(Basis_t.size());
      for (unsigned int jBase = 0; jBase < Basis_t.size(); jBase++) {


          vector<vec> theFibers(indOnBasis[iBase].size());
          vector<vec> theFibers_t(indOnBasis_t[jBase].size());

          for (unsigned int iFib = 0; iFib < indOnBasis[iBase].size(); iFib++) {
            //theFibers[iFib].resize(indOnBasis_t[jBase].size());
            vec insideVec(indOnBasis_t[jBase].size(), PETSC_COMM_WORLD);
            for (unsigned int jFib = 0; jFib < indOnBasis_t[jBase].size(); jFib++) {
              double val = u[indOnBasis[iBase][iFib]].getVecEl(indOnBasis_t[jBase][jFib]);
              //theFibers[iFib](jFib) = val;
              insideVec(jFib) = val;
            }
            theFibers[iFib] = insideVec;
            theFibers[iFib].finalize();
          }


          for (unsigned int jFib = 0; jFib < indOnBasis_t[jBase].size(); jFib++) {
            //theFibers_t[jFib].resize(indOnBasis[iBase].size());
            vec insideVec_t(indOnBasis[iBase].size(), PETSC_COMM_WORLD);
            for (unsigned int iFib = 0; iFib < indOnBasis[iBase].size(); iFib++) {
              double val = u_t[indOnBasis_t[jBase][jFib]].getVecEl(indOnBasis[iBase][iFib]);
              //theFibers_t[jFib](iFib) = val;
              insideVec_t(iFib) = val;
            }
            theFibers_t[jFib] = insideVec_t;
            theFibers_t[jFib].finalize();
          }
          cout << theFibers.size() << " for the t " << theFibers_t.size() << endl;
          double l2 = 0.0;
          for (unsigned int iFib = 0; iFib < theFibers.size(); iFib++) {
            for (unsigned int jFib = 0; jFib < theFibers_t.size(); jFib++) {
              double val = theFibers[iFib](jFib)*theFibers[iFib](jFib);
              l2 += val;
            }
          }

          if (l2 > error * error/(Basis.size() * Basis_t.size())) {
            cout << "I'm entering" << endl;
            isItNon0[iBase][jBase] = true;
            mat Uxi=computeHOSVDbasis(theFibers, error * error/(Basis.size() * Basis_t.size()));
            mat Utj=computeHOSVDbasis(theFibers_t, error * error/(Basis.size() * Basis_t.size()));

            locBasisHOSVDVec[iBase][jBase].resize(2);
            locBasisHOSVDVec[iBase][jBase][0]=Uxi;
            locBasisHOSVDVec[iBase][jBase][1]=Utj;

            mat lCore = computeHOSVDcore(theFibers, locBasisHOSVDVec[iBase][jBase]);

            localCoresVec[iBase][jBase]=lCore;

            memory_locpod += Uxi.nCols() * (theFibers.size() + theFibers_t.size());
          } else{isItNon0[iBase][jBase] = false;}
      }
    }




    //The reconstruction of the function for every x and t in the grid

    vector<vec> u_rec_HOSVD(t);
    vec u_j_rec_HOSVD(Dofsx, PETSC_COMM_WORLD);

    vector<vec> u_rec_localHOSVD(t);
    vec u_j_rec_localHOSVD(Dofsx, PETSC_COMM_WORLD);

    unsigned int position = 0;
    unsigned int position_j = 0;

    vector<unsigned int> theGrid(2);
    for (unsigned int jGrid = 0; jGrid < t; jGrid++) {
      for (unsigned int iGrid = 0; iGrid < Dofsx; iGrid++) {

        theGrid[0]=iGrid;
        theGrid[1]=jGrid;

        //reconstruction with the local HOSVD (works but it doesn't perform good)
        // double rec = recFx(vecCorx[i], Basis_t, Basis[i], iGrid, jGrid);
        // double rec_t = recFt(vecCort[j], Basis, Basis_t[j], iGrid, jGrid);
        // double mean_rec = 0.5 * (rec + rec_t);
        // un_rec(iGrid)= mean_rec;
        //
        //
        // double trueval = u[iGrid](jGrid);
        //
        // double the_error = trueval - rec;
        // double the_error_t = trueval - rec_t;
        // double the_error_mean = trueval - mean_rec;
        //
        // error_sum += the_error * the_error;
        // error_sum_t += the_error_t * the_error_t;
        // error_sum_mean += the_error_mean * the_error_mean;

        //Reconstruction of the function with HOSVD
        double recVal_HOSVD;
        recVal_HOSVD = reconstructionHOSVD(core, basisHOSVD, theGrid);
        u_j_rec_HOSVD(iGrid)= recVal_HOSVD;

        double the_error_HOSVD = u[jGrid](iGrid) - recVal_HOSVD;
        error_sum_HOSVD += the_error_HOSVD * the_error_HOSVD;



          //Reconstruction of the function with the new alternative of local HOSVD
          //The basis
          unsigned int i = findBasis(Basis, indOnBasis, jGrid);
          unsigned int j = findBasis(Basis_t, indOnBasis_t, iGrid);
          cout << "i = " << i << " and j = " << j <<" found" << endl;

          //The mapping
          unsigned int ind = 0;
          for (unsigned int iInd = 0; iInd < indOnBasis[i].size(); iInd++) {
            if(indOnBasis[i][iInd] == jGrid){
              ind = iInd;
              break;
            }
          }
          position = indOnBasis[i][ind];

          cout << "the position in the local grid is : " << ind << endl;

          unsigned int ind_t = 0;
          for (unsigned int jInd = 0; jInd < indOnBasis_t[j].size(); jInd++) {
            if(indOnBasis_t[j][jInd] == iGrid){
              ind_t = jInd;
              break;
            }
          }
          position_j = indOnBasis_t[j][ind_t];


          cout << "the position in the local grid for t is : " << ind_t << endl;
          vector<unsigned int> theLocalGrid(2);
          theLocalGrid[0] = ind_t;
          theLocalGrid[1] = ind;


          if(isItNon0[i][j] == true){
            double recVal_localHOSVD = reconstructionHOSVD(localCoresVec[i][j], locBasisHOSVDVec[i][j], theLocalGrid);

            u_j_rec_localHOSVD(iGrid)= recVal_localHOSVD;
            double the_error_localHOSVD = u[jGrid](iGrid) - recVal_localHOSVD;
            error_sum_localHOSVD += the_error_localHOSVD * the_error_localHOSVD;
            } else{
              u_j_rec_localHOSVD(iGrid)= 0.0;
              double the_error_localHOSVD = u[jGrid](iGrid);
            }
          }


        u_rec_HOSVD[jGrid].copyVecFrom(u_j_rec_HOSVD);
        //u_j_rec_HOSVD.clear(); //Memory changes
        u_rec_localHOSVD[jGrid].copyVecFrom(u_j_rec_localHOSVD);
        //u_j_rec_localHOSVD.clear();

      }
      compression_ratio_locPOD = (memory_locpod * 1.0)/(Dofsx * t);


    error_sum_HOSVD = sqrt(error_sum_HOSVD);
    error_sum_HOSVD *= 1/(sqrt(Dofsx * t));
    cout << "the global error of the HOSVD = " << error_sum_HOSVD << endl;

    error_sum_localHOSVD = sqrt(error_sum_localHOSVD);
    error_sum_localHOSVD *= 1/(sqrt(Dofsx * t));
    cout << "the global error of the new alternative for the local HOSVD = " << error_sum_localHOSVD << endl;



    ofstream outfile;
    for (unsigned int j = 0; j < t; j++) {
      string recName = "FrecHOSVD_" + to_string(t) + "_" + to_string(j) +".txt";
      outfile.open(recName.c_str());
      for (size_t i = 0; i < Dofsx; i++) {
        outfile << u_rec_HOSVD[j](i) << flush << endl;
      }
      outfile.close();
    }

    for (unsigned int j = 0; j < t; j++) {
      string recName = "FrecLocHOSVD_" + to_string(t) + "_" + to_string(j) +".txt";
      outfile.open(recName.c_str());
      for (size_t i = 0; i < Dofsx; i++) {
        outfile << u_rec_localHOSVD[j](i) << flush << endl;
      }
      outfile.close();
    }



    string modeName_HOSVD;
    for (size_t iVec = 0; iVec < basisHOSVD[0].nCols(); iVec++) {
      modeName_HOSVD = "AlocalHOSVD_FHN_parts/mode_HOSVD_" + to_string(iVec);
      vec toSave_HOSVD = basisHOSVD[0].getCol(iVec);
      saveField_2d(toSave_HOSVD,128,128, modeName_HOSVD);
      saveField_2d_txt(toSave_HOSVD,128,128, modeName_HOSVD);
    }

    string modeName_locHOSVD;
    for (size_t iVec = 0; iVec < Basis_t[0].nCols(); iVec++) {
      modeName_locHOSVD = "localHOSVD_FHN_parts/mode_locHOSVD_" + to_string(iVec);
      vec toSave_locHOSVD = Basis_t[0].getCol(iVec);
      saveField_2d(toSave_locHOSVD,128,128, modeName_locHOSVD);
      saveField_2d_txt(toSave_locHOSVD,128,128, modeName_locHOSVD);
    }

    //Reconstructed function with local HOSVD
    unsigned int count =0;
    vec recVecToPlot(t*Dofsx,PETSC_COMM_WORLD);
    for (size_t iText = 0; iText < t; iText++) {
      for (size_t iX = 0; iX < Dofsx; iX++) {
        double val = u_rec_localHOSVD[iText](iX);
        recVecToPlot.setVecEl(count, val);
        count +=1;
      }
    }

    recVecToPlot.finalize();
    string testRecName = "recLocalHosvdTest3d";
    saveField_3d(recVecToPlot, 256, 256, t, testRecName);
    recVecToPlot.clear();


    //Reconstructed function with global HOSVD
    count =0;
    vec hosvdVecToPlot(t*Dofsx,PETSC_COMM_WORLD);
    for (size_t iText = 0; iText < t; iText++) {
      for (size_t iX = 0; iX < Dofsx; iX++) {
        double val = u_rec_HOSVD[iText](iX);
        hosvdVecToPlot.setVecEl(count, val);
        count +=1;
      }
    }

    hosvdVecToPlot.finalize();
    string testRecHosvdName = "recHosvdTest3d";
    saveField_3d(hosvdVecToPlot, 256, 256, t, testRecHosvdName);
    hosvdVecToPlot.clear();



  SlepcFinalize();
  return 0;
}
