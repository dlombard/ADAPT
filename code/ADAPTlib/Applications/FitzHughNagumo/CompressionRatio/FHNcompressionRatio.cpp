// Parallel tensor implementation
#include "genericInclude.h"
#include "linAlg/linAlg.h"
#include "CP/CP.h"
#include "fullTensor/fullTens.h"
#include "TensorTrain/SoTT.h"
#include "compression/CPTensorCompression.hpp"
#include "Applications/Network/Network.h"
#include "Discretisations/finiteDifferences.h"
#include <string>

using namespace std;

vector<double> linspace(double start, double end, int num){

  vector<double> linspaced(num);


  if (num == 0) { return linspaced; }
  if (num == 1) {
      linspaced.push_back(start);
      return linspaced;
    }

  double delta = (end - start) / (num - 1.0);

  for(unsigned int i=0; i < num; ++i){
    linspaced[i] = start + delta * i;
  }
  //linspaced.push_back(end);  I want to ensure that start and end are exactly the same as the input
  return linspaced;
}


//Function to save the modes
void saveField_2d(vec& sol, unsigned int Nx, unsigned int Ny, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;

 for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();

}


void saveField_2d_txt(vec& sol, unsigned int Nx, unsigned int Ny, string& fName){
 string saveName = fName + ".txt";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

  for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();
}


int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


    vector<double> nt = linspace(500.0, 2500.0, 5);

    vector<vector<double>> t(nt.size());

    for (unsigned int iT = 0; iT < nt.size(); iT++) {
      t[iT]= linspace(0.0, 2000.0, nt[iT]);


      vector<vec> u(t[iT].size());

      // unsigned int begining = 100;
      // unsigned int end = 130;
      // unsigned int t = end-begining;
      //vector<vec> u(t);
      unsigned int Dofsx = 256 * 256;


      vec un(Dofsx, PETSC_COMM_WORLD);

      //unsigned int ciT = 0;
      // for (unsigned int iT = begining; iT < end; iT++) {
      //   if (iT%1 == 0){
      //     string uName = string("Applications/FitzHughNagumo/Outputs/FHNu_spiral/FHNu/u_") + to_string(iT) + string(".txt");
      //     if (iT%20 == 0){cout << uName << endl;}
      //     un = loadVecASCII(uName, PETSC_COMM_WORLD);
      //     u[ciT].copyVecFrom(un);
      //     ciT +=1;
      //   }
      // }
      int nt_int = (int) nt[iT];
      //cout << "nt iT =" << to_string(nt[iT]) << " And nt_int = " << nt_int << endl;
      unsigned int cjT = 0;
       for (unsigned int jT = 0; jT < t[iT].size(); jT++) {
        if (jT%1 == 0){
          string uName = string("Applications/FitzHughNagumo/CompressionRatio/FHNu/u_") + to_string(nt_int) + string("_") + to_string(jT) + string(".txt");
          if (jT%20 == 0){cout << uName << endl;}
          un = loadVecASCII(uName, PETSC_COMM_WORLD);
          u[cjT].copyVecFrom(un);
          cjT +=1;
        }
      }

      cout<< "Loaded data" << endl;


    //To extract the information in t:
    //vector<vec> u_new(Dofsx);

    // vec un_new(t[iT].size(), PETSC_COMM_WORLD);
    //
    // for (unsigned int iX = 0; iX < Dofsx; iX++) {
    //   for (unsigned int jT = 0; jT < t[iT].size(); jT++) {
    //     un_new.setVecEl(jT, u[jT](iX));
    //   }
    //   un_new.finalize();
    //   u_new[iX].copyVecFrom(un_new);
    // }

    // cout<< "Changed the format of our fiber's base" << endl;
    // cout << "the number of columns= " << u_new.size() << endl;
    // cout << "the number of rows= " << u_new[0].size() << endl;
    // cout << "in u_new: "<< u_new[7](6)<<" And in the old one: " << u[6](7) <<endl;



      vector<mat> Basis;
      vector<vector<unsigned int>> indOnBasis;
      vector<double> errBudget;
      vector<mat> mergedBasis;
      vector<vector<unsigned int>> indOnMerged;
      Network net;
      double normalization = t[iT].size() * Dofsx;
      double error = 1.0e-2 * sqrt(normalization);


      net.ComputeBases(u, error, Basis, indOnBasis, errBudget);
      cout<<"We compute the 2d basis" <<endl;
      net.Merge2dBases(u, Basis, indOnBasis, errBudget);

      unsigned int total_memory = 0;

      for (size_t iGroup = 0; iGroup < indOnBasis.size(); iGroup++) {
        total_memory += u[0].size() * Basis[iGroup].nCols() + Basis[iGroup].nCols() * indOnBasis[iGroup].size();
        cout << "i Group: " << iGroup << " Basis size = " << Basis[iGroup].nCols() << " Number of fibers: " << indOnBasis[iGroup].size() << endl;
      }
      cout << "Total memory = " << total_memory << " in comparison with the full tensor memory : " << u[0].size()*t[iT].size() << endl;
      double compression_ratio_TD = (total_memory * 1.0)/(u[0].size()*t[iT].size());


      svd vecSvd(u);
      double sumSig = vecSvd.Svec()(0) * vecSvd.Svec()(0);

      for (unsigned int iSig = 1; iSig < vecSvd.Svec().size(); iSig++) {
        sumSig +=  vecSvd.Svec()(iSig) * vecSvd.Svec()(iSig);
      }
      unsigned int cc = 0;
      while (sumSig >= (error * error) && cc < vecSvd.Svec().size()-1) {
        sumSig -= vecSvd.Svec()(cc) * vecSvd.Svec()(cc);
        // cout << "The cc term = " << vecSvd.Svec()(cc) << endl;
        // cout << "The cc term of the sum = " << sumSig << endl;
        cc += 1;
      }
      cout << "Sum sig = " << sumSig << endl;

      unsigned int memory_pod = (cc+1) * (u[0].size() + t[iT].size());
      // cout << "Numer of terms taken = " << cc+1 << endl;
      // cout << "POD memory = " << memory_pod << endl;
      double compression_ratio_POD = (memory_pod * 1.0)/(u[0].size()*t[iT].size());
      cout << "compression_ratio_POD " << compression_ratio_POD << endl;
      cout << "compression_ratio_TD " << compression_ratio_TD << endl;


      ofstream outfile_TD;
      string d_TD;
      string outFileName_TD = string("Compression_TD_") + to_string(t[iT].size()) + string(".txt") ;
      outfile_TD.open(outFileName_TD.c_str());
      outfile_TD << compression_ratio_TD << flush << endl;
      outfile_TD.close();

      ofstream outfile_POD;
      string d_POD;
      string outFileName_POD = string("Compression_POD_") + to_string(t[iT].size()) +string(".txt") ;
      outfile_POD.open(outFileName_POD.c_str());
      outfile_POD << compression_ratio_POD << flush << endl;
      outfile_POD.close();

    }
    // cout << "start saving" << endl;
    //
    // ofstream outfile;
    // string d;
    // for (size_t iBas = 0; iBas < Basis.size(); iBas++) {
    //   string outFileName = string("Base_")+ to_string(iBas) + string(".txt") ;
    //   outfile.open(outFileName.c_str());
    //   for (size_t iVec = 0; iVec < Basis[iBas].nCols(); iVec++) {
    //      for (size_t iEl = 0; iEl < Basis[iBas].nRows(); iEl++) {
    //        outfile << Basis[iBas](iEl,iVec) << flush << endl;
    //      }
    //   }
    //   outfile.close();
    // }
    //
    // string modeName;
    //
    // for (size_t iBas = 0; iBas < Basis.size(); iBas++) {
    //   for (size_t iVec = 0; iVec < Basis[iBas].nCols(); iVec++) {
    //     modeName = "Applications/FitzHughNagumo/Outputs/mode_" + to_string(iBas) + "_" + to_string(iVec);
    //     vec toSave = Basis[iBas].getCol(iVec);
    //     saveField_2d(toSave,256,256, modeName);
    //     saveField_2d_txt(toSave,256,256, modeName);
    //   }
    // }

    // Finalize the MPI environment.
    SlepcFinalize();
    return 0;
}
