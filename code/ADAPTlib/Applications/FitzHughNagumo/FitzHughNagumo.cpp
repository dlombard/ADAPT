// Parallel tensor implementation
#include "genericInclude.h"
// Include linear algebra headers:
#include "linAlg/linAlg.h"
// Include discretisation:
#include "Discretisations/Discretisations.h"

using namespace std;

#include <mpi.h>

//Function to save the modes
void saveField_2d(vec& sol, finiteDifferences& space, string& fName){
 string saveName = fName + ".txt";
 unsigned int nx = space.struc().nDof_var(0);
 unsigned int ny = space.struc().nDof_var(1);
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 // outfile << "# vtk DataFile Version 2.0 " <<endl;
 // outfile << "Data animation" << endl;
 // outfile << "ASCII" <<endl;
 // outfile << "DATASET STRUCTURED_POINTS" <<endl;
 // outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 // outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 // outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 // outfile << "POINT_DATA " << numOfVal << endl;
 // outfile << "SCALARS Field float" << endl;
 // outfile << "LOOKUP_TABLE default" <<endl;
 for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();

}

// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

    //Parameters (imputs)
    unsigned int nx = 256;
    int nx2 = nx * nx;
    double T = 2000.0;
    int nt = 1000;
    double dt = T/nt; //T total time, n time steps
    double eps = 0.005;
    double gamma = 1;
    double k = 1.0;
    double a = 0.1;
    double nu = 1./62500;

    double beta = 1./dt + eps*gamma;

    // Use the class finiteDifferences:
    finiteDifferences testSpace(2, {nx,nx}, PETSC_COMM_WORLD);
    testSpace.print();

    // Compute the mass matrix:
    testSpace.compute_mass();

    //Laplacian matrix assembly, without boundary cond:
    const unsigned int nDof = testSpace.nDof();
    mat Laplacian(nDof, nDof, PETSC_COMM_WORLD);
    for(unsigned int iDof=0; iDof<nDof; iDof++){
      // Assemble the bulk:
      if(!testSpace.isItOnBoundary(iDof)){
        double diagVal = 0.0;
        for(unsigned int iVar=0; iVar<testSpace.dim(); iVar++){
          diagVal += -2.0/(testSpace.dx(iVar)*testSpace.dx(iVar));
        }
        Laplacian(iDof,iDof) = diagVal;

        // number of "star points" = 2*n with increments [0,..,±1,...]
        for(unsigned int iVar=0; iVar<testSpace.dim(); iVar++){
          vector<int> inc_plus(testSpace.dim(),0);
          inc_plus[iVar] = 1;
          unsigned int lin_plus = testSpace.linIndIncrement(iDof,inc_plus);
          Laplacian(iDof,lin_plus) = 1.0/(testSpace.dx(iVar)*testSpace.dx(iVar));
          vector<int> inc_minus(testSpace.dim(),0);
          inc_minus[iVar] = -1;
          unsigned int lin_minus = testSpace.linIndIncrement(iDof,inc_minus);
          Laplacian(iDof,lin_minus) = 1.0/(testSpace.dx(iVar)*testSpace.dx(iVar));
        }
      }
      // Assemble the boundary:
      //...
    }
    Laplacian.finalize();
    //Laplacian.print();


    //vec u_ci(2 * nx2, PETSC_COMM_WORLD); //initial cond
    vec u_ci(nx2, PETSC_COMM_WORLD);
    vec w_ci(nx2, PETSC_COMM_WORLD);

    for (unsigned int i = 0; i < nx2; i++) {
      vector<unsigned int> ind = testSpace.struc().lin2sub(i);
      vector<double> coord = testSpace.compute_point(ind);
      if ((coord[0] > 0.) && (coord[0] < 0.5) && (coord[1] > 0.) && (coord[1] < 0.5)) {
        u_ci(i) = 1.0;
      } else {
      u_ci(i) = 0.0;
      }
      if ((coord[0] > 0.) && (coord[0] < 1.) && (coord[1] > 0.5) && (coord[1] < 1.)) {
        w_ci(i) = 0.1;
      } else {
      w_ci(i) = 0.0;
      }
        }
    u_ci.finalize();
    w_ci.finalize();
    //u_ci.print();

    //Saving initial conditions

    string fileName = "Applications/FitzHughNagumo/u_0";

    saveField_2d(u_ci, testSpace, fileName);
/*
    mat I = eye(nx2, PETSC_COMM_WORLD);

    //I.print();
    //Assemble the matrix B
    mat B = eye(2 * nx2, PETSC_COMM_WORLD);
    B *= 1./dt ;
    B.finalize();

  //  B.print();
    //Assemble the matrix A2
    mat I2 = eye(nx2, PETSC_COMM_WORLD);
    I2 *= eps * gamma;
    I2.finalize();

    mat A2_1 = eye(nx2, PETSC_COMM_WORLD);
    A2_1 *= 1./dt;
    A2_1.finalize();

    mat A2 = A2_1 + I2;

    double beta = 1./dt + eps*gamma;
    //A2.print();

    //Assemble the constant part of the matri A1_fix */

    //Boundary conditions

    mat C(nx2, nx2, PETSC_COMM_WORLD);

    for (unsigned int iRow = 0; iRow < nx2; iRow++) {
      if (testSpace.isItOnBoundary(iRow)) {
        C(iRow, iRow) = 1;
        vector<unsigned int> ind = testSpace.struc().lin2sub(iRow);
        bool enter = false;
        if ((ind[0] ==0) && (!enter)) {
          C(iRow, iRow + 1) = -1;
          enter = true;
        }
        if ((ind[0] == nx-1) && (!enter)) {
          C(iRow, iRow - 1) = -1;
          enter = true;
        }
        if ((ind[1] == 0) && (!enter)) {
          unsigned int iCol1 = testSpace.struc().sub2lin({ind[0], 1});
          C(iRow, iCol1) = -1;
          enter = true;
        }
        if ((ind[1] == nx - 1) && (!enter)) {
          unsigned int iCol2 = testSpace.struc().sub2lin({ind[0], nx-2});
          C(iRow, iCol2) = -1;
          enter = true;
        }
      } else {
        double value = a*k + 1./dt;
        C(iRow, iRow) = value;
      }
    }

    C.finalize();
      // C.print();

    Laplacian *= (-1.0 * nu);

    C += Laplacian;


    //C.print();
    /*
    //Assemble the matrix A1
    mat I1 = eye(nx2, PETSC_COMM_WORLD);
    I1 *= -a*k;
    I1.finalize();

    Laplacian *= -nu;

    mat A1_fix_1 = eye(nx2, PETSC_COMM_WORLD);
    A1_fix_1 *= 1./dt;
    A1_fix_1.finalize();

    mat A1_fix = A1_fix_1 + I1;
    A1_fix = A1_fix + Laplacian;

  //  A1_fix.print(); */


    //Assemble the whole matrix A:
/*
    mat A(2*nx2, 2*nx2, PETSC_COMM_WORLD);

    for (unsigned int i = 0; i <  nx2; i++) {
       for (unsigned int j = 0; j < nx2; j++) {

         A(i,j) = 1.0 * C(i,j);
         A(i, j + nx2) = -1.0 * epsI(i,j);
         A(i + nx2, j) = 1.0 * I(i,j);
         A(i + nx2, j + nx2) = 1.0 * A2(i,j);
       }
   }
    A.finalize();
  //  A.print();
*/

    vector<vec> u(nt);
    vector<vec> w(nt);

  /*  //Initial values
    u[0].init(2 * nx2);
    unsigned int cc = 0;
    for (unsigned int i = 0; i < nx; i++) {
      for (unsigned int j = 0; j < nx; j++) {
          u[0](cc) = 1.0 * u_ci(i + j*nx);
          cc +=1;
      }
    }
    u[0].finalize(); */

    u[0].init(nx2);

    for (unsigned int i = 0; i < nx2; i++) {
          u[0](i) = 1.0 * u_ci(i);

    }
    u[0].finalize();

    w[0].init(nx2);

    for (unsigned int i = 0; i < nx2; i++) {
          w[0](i) = 1.0 * w_ci(i);

    }
    w[0].finalize();
  //  u[0].print();

    //The variable part in time, A1_var, and solving

  //  mat A_var(2*nx2, 2*nx2, PETSC_COMM_WORLD);
  /*  for (int i = 0; i < 2*nx2; i++){
      for (int j = 0; j <2*nx2; j++){
        A_var(i,j) = 0;
      }
    }
    A_var.finalize(); */

/*
    for (unsigned int iT = 1; iT < nt; iT++) {
      u[iT].init(2 * nx2);
      for (unsigned int iDof = 0; iDof < nx2; iDof++) {
        double value;
        value = 1.0 * u[iT-1](iDof);
        value *= 1.0 + a;
        value += u[iT-1](iDof) * u[iT-1](iDof);
        A_var(iDof,iDof) = value;

      }
      cout << "First loop done" << endl;

      mat M;
      M.copyMatFrom(A);
      A_var *= k;
      M = M + A_var;
      //M.print();

      b = B * u[iT-1];
      b.finalize();
      sol = M / b;
      u[iT] = sol;
      u[iT].finalize();
      sol.finalize();
      u[iT].print();
    }
    A_var.finalize(); */
  //  A_var.print();

  mat pi(nx2, nx2, PETSC_COMM_WORLD);

  for (unsigned int i = 0; i < nx2; i++) {
    if (!testSpace.isItOnBoundary(i)) {
      pi(i,i) = 1;
    } else {
      pi(i,i)=0;
    }
  }
pi.finalize();
//pi.print();


mat epsI;
epsI.copyMatFrom(pi);
epsI *= eps;
epsI.finalize();

epsI *= 1./beta;

//epsI.print();

//u[0].print();
//w[0].print();


vec Iext(nx2, PETSC_COMM_WORLD);

for (unsigned int i = 0; i < nx2; i++) {
  vector<unsigned int> ind = testSpace.struc().lin2sub(i);
  vector<double> coord = testSpace.compute_point(ind);
  if ((coord[0] > 0.20) && (coord[0] < 0.80) && (coord[1] > 0.45) && (coord[1] < 0.55)) {
    Iext(i) = 0.0;
  } else {
  Iext(i) = 0.0;
  }
}
Iext.finalize();




mat A_var(nx2, nx2, PETSC_COMM_WORLD);

for (unsigned int iT = 1; iT < nt; iT++) {
  u[iT].init(nx2);
  w[iT].init(nx2);

  if (iT%10 == 0) {
    cout << "iteration: "<< iT<<endl;
  }

  for (unsigned int iDof = 0; iDof < nx2; iDof++) {
    double value;
    if (!testSpace.isItOnBoundary(iDof)) {
      value = -(1.0 + a) * u[iT-1](iDof);
      value += (u[iT-1](iDof) * u[iT-1](iDof));
      A_var(iDof,iDof) = value;
    }
  }
  A_var *= k;

  mat M;
  M.copyMatFrom(C);
  M = M + A_var;
  M = M + epsI;
  //M.print();


  vec b;
  b.copyVecFrom(u[iT-1]);
  b *= 1./dt;

  for (unsigned int i = 0; i < nx2; i++) {
    if (testSpace.isItOnBoundary(i)) {
      b(i)=0;
    }
  }
  vec f;
  f = pi * w[iT-1];
  f *= 1./beta;
  f *= 1./dt;

  b = b - f;
  //b.print();

  if (iT < 10) {
    b += pi*Iext;
  }


  vec sol = M / b;
  u[iT].copyVecFrom(sol);
  sol.clear();

  // free the memory:
  b.clear();

  //u[iT].print();

  vec f2;
  f2.copyVecFrom(w[iT-1]);
  f2 *= 1./dt;
  f2 *= 1./beta;


  w[iT].copyVecFrom(u[iT]);
  w[iT] *= (eps * 1./beta);
  w[iT] += f2;
  w[iT].finalize();
  f2.clear();
  //w[iT].print();

  stringstream ss;
  ss << iT;
  fileName = "Applications/FitzHughNagumo/Outputs/u_" + ss.str();

  saveField_2d(u[iT], testSpace, fileName);

}
A_var.finalize();

SlepcFinalize();
return 0;
}
