// Parallel tensor implementation
#include "genericInclude.h"
#include "linAlg/linAlg.h"
#include "CP/CP.h"
#include "fullTensor/fullTens.h"
#include "TensorTrain/SoTT.h"
#include "compression/CPTensorCompression.hpp"
#include "Applications/Network/Network.h"
#include "Applications/Network/locHOSVD.h"
#include <string>

using namespace std;


/* Linspace function: */
vector<double> linspace(double start, double end, int num){

  vector<double> linspaced(num);


  if (num == 0) { return linspaced; }
  if (num == 1) {
      linspaced.push_back(start);
      return linspaced;
    }

  double delta = (end - start) / (num - 1.0);

  for(unsigned int i=0; i < num; ++i){
    linspaced[i] = start + delta * i;
  }
  //linspaced.push_back(end);  I want to ensure that start and end are exactly the same as the input
  return linspaced;
}



//Function to save the modes
void saveField_2d(vec& sol, unsigned int Nx, unsigned int Ny, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;

 for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();

}



//Function to save the modes
void saveField_3d(vec& sol, unsigned int Nx, unsigned int Ny, unsigned int Nz, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = Nz;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;

 for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();

}



void saveField_2d_txt(vec& sol, unsigned int Nx, unsigned int Ny, string& fName){
 string saveName = fName + ".txt";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

  for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();
}


void saveField_3d_txt(vec& sol, unsigned int Nx, unsigned int Ny, unsigned int Nz, string& fName){
 string saveName = fName + ".txt";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = Nz;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

  for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();
}



// Simple test on vectors

int main(int argc, char **args){

  static char help[] = "Testing\n\n";

  SlepcInitialize(&argc,&args,(char*)0,help);

  if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

  // Number of procs, and the ranks
  int nOfProcs;
  MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
  int idProc;
  MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


    unsigned int begining = 0;
    unsigned int end = 200;
    unsigned int t = end-begining;
    vector<vec> u(t);
    unsigned int Dofsx = 256 * 256;

    vec un(Dofsx, PETSC_COMM_WORLD);
    unsigned int ciT = 0;


    for (unsigned int iT = begining; iT < end; iT++) {
      if (iT%1 == 0){
        string uName = string("Applications/FitzHughNagumo/Outputs/FHNu_spiral/FHNu/u_") + to_string(iT) + string(".txt");
        if (iT%20 == 0){cout << uName << endl;}
        un = loadVecASCII(uName, PETSC_COMM_WORLD);
        u[ciT].copyVecFrom(un);
        ciT +=1;
      }
    }
    un.clear();

    //To extract the information in t:
    vector<vec> u_t(Dofsx);
    vec un_t(t, PETSC_COMM_WORLD);

    for (unsigned int iX = 0; iX < Dofsx; iX++) {
      for (unsigned int iT = begining; iT < end; iT++) {
        un_t.setVecEl(iT, u[iT](iX));
      }
      un_t.finalize();
      u_t[iX].copyVecFrom(un_t);
    }
    un_t.clear();


    vector<mat> Basis_t;
    vector<vector<unsigned int>> indOnBasis_t;
    vector<double> errBudget_t;
    Network net;
    double normalization = t * Dofsx;
    double error = 1.0e-2 * sqrt(normalization);


    //Obtaining the basis with respect to the t
    net.ComputeBases(u_t, error, Basis_t, indOnBasis_t, errBudget_t);
    if (Basis_t.size() != 1) {
      net.Merge2dBases(u_t, Basis_t, indOnBasis_t, errBudget_t);
    }

    unsigned int total_memory_t = 0;


    for (size_t iGroup = 0; iGroup < indOnBasis_t.size(); iGroup++) {
      total_memory_t += t * Basis_t[iGroup].nCols() + Basis_t[iGroup].nCols() * indOnBasis_t[iGroup].size();
    }
    cout << "Total memory = " << total_memory_t << " in comparison with the full tensor memory : " << Dofsx*t << endl;
    double compression_ratio_TD_t = (total_memory_t * 1.0)/(Dofsx*t);

    double error_sum_t = 0.0;

    cout << "start saving" << endl;


    ofstream outfile_t;
    string d_t;
    for (size_t iBas = 0; iBas < Basis_t.size(); iBas++) {
      string outFileName_t = string("localHOSVD_FHN_parts/Base_t_")+ to_string(iBas) + string(".txt") ;
      outfile_t.open(outFileName_t.c_str());
      saveMat(Basis_t[iBas], outfileName_t);
      for (size_t iVec = 0; iVec < Basis_t[iBas].nCols(); iVec++) {
         for (size_t iEl = 0; iEl < Basis_t[iBas].nRows(); iEl++) {
           outfile_t << Basis_t[iBas](iEl,iVec) << flush << endl;
         }
      }
      outfile_t.close();
    }


    for (size_t iBas = 0; iBas < indOnBasis_t.size(); iBas++) {
      outFileName = string("localHOSVD_FHN_parts/indOnBasis_t_")+ to_string(iBas) + string(".txt") ;
      outfile.open(outFileName.c_str());
       for (size_t iFib = 0; iFib < indOnBasis_t[iBas].size(); iFib++) {
         outfile << indOnBasis_t[iBas][iFib] << flush << endl;
       }
      outfile.close();
    }

    for (size_t iBas = 0; iBas < errBudget_t.size(); iBas++) {
      outFileName = string("localHOSVD_FHN_parts/errorBudget_t_")+ to_string(iBas) + string(".txt") ;
      outfile.open(outFileName.c_str());
      outfile << errBudget_t[iBas] << flush << endl;
      outfile.close();
    }



  SlepcFinalize();
  return 0;
}
