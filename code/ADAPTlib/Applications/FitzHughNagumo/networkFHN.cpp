// Parallel tensor implementation
#include "genericInclude.h"
#include "linAlg/linAlg.h"
#include "CP/CP.h"
#include "fullTensor/fullTens.h"
#include "TensorTrain/SoTT.h"
#include "compression/CPTensorCompression.hpp"
#include "Applications/Network/Network.h"
#include <string>

using namespace std;

vector<double> linspace(double start, double end, int num){

  vector<double> linspaced(num);


  if (num == 0) { return linspaced; }
  if (num == 1) {
      linspaced.push_back(start);
      return linspaced;
    }

  double delta = (end - start) / (num - 1.0);

  for(unsigned int i=0; i < num; ++i){
    linspaced[i] = start + delta * i;
  }
  //linspaced.push_back(end);  I want to ensure that start and end are exactly the same as the input
  return linspaced;
}

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

    // Use the class vec
    //vector<double> nt = linspace(250.0, 2500.0, 10);

    //vector<double> x = linspace(0.0, 1.0, 100);
    //vector<double> t(nt.size());

    double t = 1000.0;

    vector<vec> u(10);
    vec un(256, PETSC_COMM_WORLD);
    unsigned int ciT = 0;


    for (unsigned int iT = 500; iT < 510; iT++) {
      if (iT%1 == 0){
        string uName = string("Applications/FitzHughNagumo/Outputs/u_") + to_string(iT) + string(".txt");
        if (iT%20 == 0){cout << uName << endl;}
        un = loadVecASCII(uName, PETSC_COMM_WORLD);
        u[ciT].copyVecFrom(un);
        ciT +=1;
      }
    }

      // vec u_j(x.size(), PETSC_COMM_WORLD);
      //
      // for (unsigned int j = 0; j < t[iT].size(); j++) {
      //   for (unsigned int i = 0; i < x.size(); i++) {
      //     u_j(i) = exp(- 500 * (x[i] - 0.25 - t[iT][j]) * (x[i] - 0.25 - t[iT][j]));
      //   }
      //   u[j].copyVecFrom(u_j);
      //   u_j.print();
      // }



      vector<mat> Basis;
      vector<vector<unsigned int>> indOnBasis;
      vector<double> errBudget;
      vector<mat> mergedBasis;
      vector<vector<unsigned int>> indOnMerged;
      Network net;
      double normalization = t * u[0].size();
      double error = 1.0e-2 * sqrt(normalization);


      net.ComputeBases(u, error, Basis, indOnBasis, errBudget);
      net.Merge2dBases(u, Basis, indOnBasis, errBudget);

      unsigned int total_memory = 0;

      for (size_t iGroup = 0; iGroup < indOnBasis.size(); iGroup++) {
        total_memory += u[0].size() * Basis[iGroup].nCols() + Basis[iGroup].nCols() * indOnBasis[iGroup].size();
        cout << "i Group: " << iGroup << " Basis size = " << Basis[iGroup].nCols() << " Number of fibers: " << indOnBasis[iGroup].size() << endl;
      }
      cout << "Total memory = " << total_memory << " in comparison with the full tensor memory : " << u[0].size()*t << endl;
      double compression_ratio_TD = (total_memory * 1.0)/(u[0].size()*t);


      svd vecSvd(u);
      double sumSig = vecSvd.Svec()(0) * vecSvd.Svec()(0);

      for (unsigned int iSig = 1; iSig < vecSvd.Svec().size(); iSig++) {
        sumSig +=  vecSvd.Svec()(iSig) * vecSvd.Svec()(iSig);
      }
      unsigned int cc = 0;
      while (sumSig >= (error * error) && cc < vecSvd.Svec().size()-1) {
        sumSig -= vecSvd.Svec()(cc) * vecSvd.Svec()(cc);
        // cout << "The cc term = " << vecSvd.Svec()(cc) << endl;
        // cout << "The cc term of the sum = " << sumSig << endl;
        cc += 1;
      }
      cout << "Sum sig = " << sumSig << endl;

      unsigned int memory_pod = (cc+1) * (u[0].size() + t);
      // cout << "Numer of terms taken = " << cc+1 << endl;
      // cout << "POD memory = " << memory_pod << endl;
      double compression_ratio_POD = (memory_pod * 1.0)/(u[0].size()*t);
      cout << "compression_ratio_POD " << compression_ratio_POD << endl;
      cout << "compression_ratio_TD " << compression_ratio_TD << endl;


      ofstream outfile_TD;
      string d_TD;
      string outFileName_TD = string("Compression_TD_") + to_string(t) + string(".txt") ;
      outfile_TD.open(outFileName_TD.c_str());
      outfile_TD << compression_ratio_TD << flush << endl;
      outfile_TD.close();

      ofstream outfile_POD;
      string d_POD;
      string outFileName_POD = string("Compression_POD_") + to_string(t) +string(".txt") ;
      outfile_POD.open(outFileName_POD.c_str());
      outfile_POD << compression_ratio_POD << flush << endl;
      outfile_POD.close();



    ofstream outfile;
    string d;
    for (size_t iBas = 0; iBas < Basis.size(); iBas++) {
      string outFileName = string("Base_")+ to_string(iBas) + string(".txt") ;
      outfile.open(outFileName.c_str());
      for (size_t iVec = 0; iVec < Basis[iBas].nCols(); iVec++) {
         for (size_t iEl = 0; iEl < Basis[iBas].getCol(0).size(); iEl++) {
           outfile << Basis[iBas](iEl,iVec) << flush << endl;
         }
      }
      outfile.close();
    }






    // Finalize the MPI environment.
    SlepcFinalize();
    return 0;
}
