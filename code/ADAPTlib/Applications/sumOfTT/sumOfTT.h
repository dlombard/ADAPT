// Implementation of the sum of TT format
#ifndef sumOfTT_h
#define sumOfTT_h

#include "../../genericInclude.h"
#include "../../linAlg/linAlg.h"
#include "../../tensor.h"
#include "../../CP/CP.h"
#include "../../fullTensor/fullTens.h"
#include "../../TensorTrain/TensorTrain.h"


class sumOfTT : public tensor{
private:
  vector<TensorTrain> m_TT_set;
  unsigned int m_nOfTT;

public:
  sumOfTT(){};
  ~sumOfTT(){};
  void clear(){
    for(unsigned int iTerm=0; iTerm<m_nOfTT; iTerm++){
      m_TT_set[iTerm].clear();
    }
  }

  // Overloaded constructors:
  sumOfTT(CPTensor&, double);
  sumOfTT(fullTensor&, double);

  // Auxiliary functions:
  TensorTrain compute_TT_term(CPTensor&);
  void selectBestUnfold(vector<vector<double> >&, unsigned int&, unsigned int&);
  unsigned int estimateRank(vector<svd>&, unsigned int);
  CPTensor pairVars(CPTensor&, unsigned int, unsigned int);


  // ACCESS Functions:
  inline unsigned int nOfTT(){return m_nOfTT;}
  inline vector<TensorTrain> TT_set(){return m_TT_set;}
  inline TensorTrain TT_set(unsigned int iTerm){assert(iTerm<m_nOfTT); return m_TT_set[iTerm];}


};

#endif
