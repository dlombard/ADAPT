// Implementation of the class sum of TT:

#include "sumOfTT.h"
using namespace std;


// I -- Overloaded Constructors:

/* Take a CP as input and transform it into a sum of TT
  - inputs: a CP tensor and a tolerance
  - output: the constructor construct the sum of TT
*/
sumOfTT::sumOfTT(CPTensor& toCompress, double tol){
  // init the tensor fields:
  m_nVar = toCompress.nVar();
  m_nDof_var = toCompress.nDof_var();
  m_comm = toCompress.comm();

  // init the residual as a copy of toCompress:
  CPTensor residual;
  residual.copyTensorFrom(toCompress);

  // starting to compress:
  vector<mat> unfold(m_nVar);
  vector<svd> svdUnfold(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    residual.computeUnfolding(iVar, unfold[iVar]);
    svdUnfold[iVar].init(unfold[iVar]);
  }

  // best unfolding: the one with the largest sigma
  unsigned int iBest = 0;
  double sigma_best = svdUnfold[0].Svec().getVecEl(0);
  for(unsigned int iVar=1; iVar<m_nVar; iVar++){
    if(svdUnfold[iVar].Svec().getVecEl(0) > sigma_best){
      sigma_best = svdUnfold[iVar].Svec().getVecEl(0);
      iBest = iVar;
    }
  }
  cout << "The best unfolding is " << iBest << endl;

  // Choose the rank:
  unsigned int rank = estimateRank(svdUnfold, iBest);
  cout << "The rank is " << rank << endl;

  //free the memory:
  for(unsigned int iVar=0;iVar<m_nVar; iVar++){
    unfold[iVar].clear();
    svdUnfold[iVar].clear();
  }
  residual.clear();
}



// II -- AUXILIARY FUNCTIONS --

/* 1 - select best unfolding:
  - input: the table of the singular values of the different unfoldings
  - outputs: the id of the best unfolding and the rank to be kept.
*/
void sumOfTT::selectBestUnfold(vector<vector<double> >& table, unsigned int& iBest, unsigned int& rank){
  const unsigned int dim = table.size();
  // start selecting the unfolding with the largest sigma;
  double largest = table[0][0];
  iBest = 0;
  for(unsigned int iVar=1; iVar<dim; iVar++){
    if(table[iVar][0] > largest){
      largest = table[iVar][0];
      iBest = iVar;
    }
  }
  rank = 1;
  largest = largest*largest;
  largest += table[iBest][1] * table[iBest][1];
  bool isStill = true;
  while(isStill){
    for(unsigned int iVar=0; iVar<dim; iVar++){
      // check if the sum of squares of sigma up to rank is larger...
      if(iVar != iBest){
        double sumOfSquares = 0.0;
        unsigned int bound = rank;
        if(rank>=table[iVar].size()){
          bound = table[iVar].size()-1;
        }
        for(unsigned int iS=0; iS<=bound; iS++){
          sumOfSquares += table[iVar][iS] * table[iVar][iS];
        }
        if(sumOfSquares > largest){
          isStill = false;
          break;
        }
      }
    }
    if(isStill){
      rank += 1;
      if(rank==table[iBest].size()){
        isStill = false;
        break;
      }
    }
  }
}


/* 1.b - select best unfolding:
  - input: the vector of svd, and the best unfolding
  - outputs: the rank to be kept
*/
unsigned int sumOfTT::estimateRank(vector<svd>& svdUnf, unsigned int iBest){
  unsigned int rank = 1;
  const unsigned int dim = svdUnf.size();

  bool isItLarger = true;
  while(isItLarger){
    unsigned int sigma_best = svdUnf[iBest].Svec().getVecEl(rank);
    for(unsigned int iVar=0; iVar<dim; iVar++){
      if(svdUnf[iVar].Svec().getVecEl(rank) > sigma_best){
        isItLarger = false;
        break;
      }
    }
    if(isItLarger){
      rank += 1;
    }
  }

  return rank;
}


/* 2 - compute TT term:
  - input: a CPTensor
  - outputs: its TT compression with adaptive ranks and variable order
*/
TensorTrain sumOfTT::compute_TT_term(CPTensor& toApprox){
  const unsigned int dim = toApprox.nVar();
  vector<unsigned int> varSet(dim);
  for(unsigned int iVar=0; iVar<dim; iVar++){
    varSet[iVar] = iVar;
  }
  // initialising TT:
  TensorTrain toBeReturned(dim, toApprox.nDof_var(), toApprox.comm());

  // starting the adaptive TT-SVD: copy the tensor (not to modify it)
  CPTensor residual;
  residual.copyTensorFrom(toApprox);

  while(varSet.size()>2){

    // compute the unfoldings and the table:
    unsigned int nUnfold = residual.nVar();
    vector<vector<double> > sig_table(nUnfold);
    vector<mat> u_vecs(nUnfold);
    for(unsigned int iVar=0; iVar<nUnfold; iVar++){
      vector<double> sig;
      vector<vec> u;
      // compute the svd of the unfolding:
      mat M = residual.computeUnfolding(iVar);
      svd unfoldSVD(M);
      vec sigma = unfoldSVD.Svec();
      mat U = unfoldSVD.Umat();
      sig_table[iVar].resize(sigma.size());
      for(unsigned int iS=0; iS<sigma.size(); iS++){
        sig_table[iVar][iS] = sigma(iS);
      }
      u_vecs[iVar].copyMatFrom(U);
      unfoldSVD.clear();
      M.clear();
    }
    // choose the best unfolding according to the criterion in selectBestUnfold:
    unsigned int iBest = 0;
    unsigned int rank = 0;
    selectBestUnfold(sig_table, iBest, rank);
    cout << "the best is " << iBest << " the rank is " << rank << endl;
    // remove the var from the variables set:
    varSet.erase(varSet.begin() + iBest);
    mat UT = transpose(u_vecs[iBest]);
    for(unsigned int iUnf=0; iUnf<u_vecs.size(); iUnf++){
      u_vecs[iBest].clear();
    }
    UT.print();

    // select the best unfolding:

    // project and free the memory

  }
  // last svd to close the adaptive TT-SVD

  // returning the TT;
  return toBeReturned;
}


/* pair variables: along a prescribed direction:
  - inputs: the CPTensor, the pairs of variables (one is the best unfold, the other is the next to be tested)
  - output: the reshaped CPTensor:
*/
CPTensor sumOfTT::pairVars(CPTensor& T, unsigned int idV_1, unsigned int idV_2){
  const unsigned int dim = T.nVar() - 1;
  vector<unsigned int> dofPerDim(dim);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
    if( (iVar != idV_1) && (iVar != idV_2) ){
      dofPerDim[cc] = T.nDof_var(iVar);
      cc += 1;
    }
    else{
      if( iVar == idV_1 ){
        dofPerDim[cc] = T.nDof_var(idV_1) * T.nDof_var(idV_2);
        cc += 1;
      }
    }
  }

  CPTensor toBeReturned(dim, dofPerDim, T.comm());
  const unsigned int rankOfT = T.rank();
  toBeReturned.set_rank(rankOfT);
  // copying coefficients:
  for(unsigned int iTerm=0; iTerm<rankOfT; iTerm++){
    toBeReturned.set_coeffs(iTerm, T.coeffs(iTerm));
  }
  // setting terms by copy:
  cc = 0;
  for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
    if( (iVar != idV_1) && (iVar != idV_2) ){
      for(unsigned int iTerm=0; iTerm<T.rank(); iTerm++){
        vec termCopy;
        vec termOfT = T.terms(iTerm,iVar);
        termCopy.copyVecFrom(termOfT);
        toBeReturned.set_terms(iTerm,cc, termCopy);
      }
      cc += 1;
    }
    else{
      if( iVar == idV_1 ){
        for(unsigned int iTerm=0; iTerm<T.rank(); iTerm++){
          vec term_1 = T.terms(iTerm, idV_1);
          vec term_2 = T.terms(iTerm, idV_2);
          vec toAdd(dofPerDim[cc], T.comm());
          unsigned int jDof = 0;
          for(unsigned int i_1=0; i_1<T.nDof_var(idV_1); i_1++){
            double val_1 = term_1(i_1);
            for(unsigned int i_2=0; i_2< T.nDof_var(idV_2); i_2++){
              double val_2 = term_2(i_2);
              double val = val_1*val_2;
              toAdd(jDof) = val;
              jDof += 1;
            }
          }
          toAdd.finalize();
          toBeReturned.set_terms(iTerm, cc, toAdd);
        }
        cc += 1;
      }
    }
  }
  toBeReturned.finalize();
  return toBeReturned;
}
