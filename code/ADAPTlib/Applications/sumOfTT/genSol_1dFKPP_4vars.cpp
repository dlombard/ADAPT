// Parallel tensor implementation
#include "genericInclude.h"
#include "linAlg/linAlg.h"
#include "CP/CP.h"
#include "fullTensor/fullTens.h"
#include "TensorTrain/TensorTrain.h"
#include "TensorTrain/SoTT.h"

using namespace std;


// DEFINE AUXILIARY FUNCTIONS





// MAIN:
int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // I -- Data and operators:

    // I.1) Space:
    const unsigned int Nx = 100;
    const vector<double> box_x = {0.0, 1.0};
    const double dx = (box_x[1]-box_x[0])/(Nx-1);

    // I.2) Time:
    const unsigned int Nt = 50;
    const vector<double> box_t = {0.0, 0.25};
    const double dt = (box_t[1]-box_t[0])/(Nt-1);

    // I.3) Parameters:
    const unsigned int Na = 10;
    const vector<double> box_a = {25.0, 100.0};
    const double da = (box_a[1]-box_a[0])/(Na-1);

    const unsigned int Nb = 10;
    const vector<double> box_b = {0.25, 0.75};
    const double db = (box_b[1]-box_b[0])/(Nb-1);


    // II) Operators:
    mat Lapl(Nx,Nx,PETSC_COMM_WORLD);
    for(unsigned int iDof=1; iDof<Nx-1; iDof++){
      Lapl(iDof,iDof) = -2.0/(dx*dx);
      Lapl(iDof,iDof+1) = 1.0/(dx*dx);
      Lapl(iDof,iDof-1) = 1.0/(dx*dx);
    }
    Lapl.finalize();
    mat Op_plus;
    Op_plus.copyMatFrom(Lapl);
    Op_plus *= (-0.5*dt);
    Op_plus += eye(Nx, PETSC_COMM_WORLD);
    mat Op_minus;
    Op_minus.copyMatFrom(Lapl);
    Op_minus *= (0.5*dt);
    Op_minus += eye(Nx, PETSC_COMM_WORLD);


    // III --  Compute the solution and fill the tensor:
    fullTensor sol({Nx,Nt,Na,Nb}, PETSC_COMM_WORLD);

    for(unsigned int i_a=0; i_a<Na; i_a++){
      const double amp = box_a[0] + i_a*da;
      for(unsigned int i_b=0; i_b<Nb; i_b++){
        const double b0 = box_b[0] + i_b*db;

        cout << "Solving for a=" << amp << " and b=" << b0 << endl;

        // initial condition:
        vec u(Nx, PETSC_COMM_WORLD);
        for(unsigned int iDof=0; iDof<Nx; iDof++){
          double x = box_x[0] + iDof*dx;
          double arg = -200*(x-b0)*(x-b0);
          u(iDof) = 0.5 * exp(arg);
        }
        u.finalize();
        // solution: putting it into the tensor:
        for(unsigned int iDof=0; iDof<Nx; iDof++){
          double value = u(iDof);
          if(fabs(value)>DBL_EPSILON){
            vector<unsigned int> ind = {iDof,0,i_a,i_b};
            sol.set_tensorElement(ind, value);
          }
        }
        for(unsigned int it=0; it<Nt; it++){
          // assembling rhs:
          vec rhs = Op_minus * u;
          vec reaction;
          reaction.copyVecFrom(u);
          reaction *= (0.5*dt*amp);
          rhs += reaction;
          vec square = pointWiseMult(u, u);
          square *= (-dt*amp);
          rhs += square;
          // assembling the system operator:
          mat sys;
          sys.copyMatFrom(Op_plus);
          sys.sum(eye(Nx,PETSC_COMM_WORLD), (-0.5*dt*amp));
          sys.finalize();
          // solving:
          vec u_p = sys/rhs;

          // putting the solution in the tensor:
          for(unsigned int iDof=0; iDof<Nx; iDof++){
            double value = u_p(iDof);
            if(fabs(value)>DBL_EPSILON){
              if(value<0.0){value = 0.0;}
              if(value>1.0){value = 1.0;}
              vector<unsigned int> ind = {iDof,it,i_a,i_b};
              sol.set_tensorElement(ind, value);
            }
          }

          // clearing u and putting u_p into u:
          u.clear();
          u.copyVecFrom(u_p);
          u_p.clear();
          sys.clear();
          reaction.clear();
          square.clear();
        }
      }
    }
    sol.finalize();

    // saving full tensor:
    string fName = "1dFKPP_4vars";
    sol.save(fName);


    Lapl.clear();
    Op_plus.clear();
    Op_minus.clear();
    sol.clear();
    SlepcFinalize();
    return 0;

}
