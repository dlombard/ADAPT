// Implementation of the Network class:

#include "Network.h"
#include "../../linAlg/linearAlgebraOperations.h"
#include "../../linAlg/svd.h"
#include "../../linAlg/eigenSolver.h"
#include "../../genericInclude.h"


unsigned int computeMemory(vector<mat>& Basis, vector<vector<unsigned int>>& indOnBasis){
  unsigned int total_memory = 0;
  for (size_t iGroup = 0; iGroup < indOnBasis.size(); iGroup++) {
    total_memory += Basis[iGroup].nRows() * Basis[iGroup].nCols() + Basis[iGroup].nCols() * indOnBasis[iGroup].size();
  }
  return total_memory;
}


/*Save the ind on basis in a vector */
void Network::save_indOnBasis(vector<vector<unsigned int>>& indOnBasis, string fileName){
  unsigned int iob_size = indOnBasis.size();
  ofstream outfile;
  outfile.open(fileName.c_str());

  outfile << iob_size << flush << endl;

  for (size_t iInd = 0; iInd < iob_size; iInd++) {
    outfile << indOnBasis[iInd].size() << flush << endl;
  }
  vector<unsigned int> vec_indices;
  for (size_t iInd = 0; iInd < iob_size; iInd++) {
    for (size_t iEl = 0; iEl < indOnBasis[iInd].size(); iEl++) {
      unsigned int iM = iEl + iInd * indOnBasis[iInd].size();
      outfile << indOnBasis[iInd][iEl] << flush << endl;
    }
  }
  outfile.close();
}


/*Load the ind on basis from a vector */
void Network::load_indOnBasis(string fileName, vector<vector<unsigned int>>& indOnBasis_return, vector<unsigned int>& sizeList, unsigned int& iob_size){
  vector<unsigned int> nonZerosList;
  unsigned int thisEntry = 0;

  ifstream inputFile;
  inputFile.open(fileName.c_str());
  char output[128];
  if (inputFile.is_open()) {
    while (!inputFile.eof()) {
      inputFile >> output;
      stringstream str;
      str << output;
      str >> thisEntry;
      if(fabs(thisEntry)!=0.){
        nonZerosList.push_back(thisEntry);
      }
    }
  } else{
    puts("Unable to read file!");
    exit(1);
  }

  iob_size = nonZerosList[0];

  sizeList.resize(iob_size);
  for(unsigned int iEl=0; iEl<iob_size; iEl++){
    sizeList[iEl] = nonZerosList[iEl+1];
  }

  indOnBasis_return.resize(iob_size);
  unsigned int cc = iob_size + 1;
  for (size_t iInd = 0; iInd < iob_size; iInd++) {
    indOnBasis_return[iInd].resize(sizeList[iInd]);
    for (size_t iEl = 0; iEl < sizeList[iInd]; iEl++) {
      //unsigned int iM = iob_size + 1 + iEl + iInd * sizeList[iInd];
      indOnBasis_return[iInd][iEl] =  nonZerosList[cc];
      cc +=1;
    }
  }
  nonZerosList.clear();
}


/* SVD 2 vecs: Compute the SVD of two vectors
   -inputs:two vecs
   -outputs: U, s (sizes assigned externally)
*/
void Network::svd2vecs(vec& vec1, vec& vec2, mat& U, vec& s){
  //Compute a b and c
  double a = scalProd(vec1, vec1);
  double b = scalProd(vec1, vec2);
  double c = scalProd(vec2, vec2);

  double lambda_1 = 0.5 * ((a + c) + sqrt((a-c)*(a-c) + 4.0*b*b));
  double lambda_2 = 0.5 * ((a + c) - sqrt((a-c)*(a-c) + 4.0*b*b));

  double s_1 = sqrt(lambda_1);
  double s_2 = sqrt(lambda_2);

  s.setVecEl(0, s_1);
  s.setVecEl(1, s_2);

  s.finalize();
  //Assemble first column
  double factor = 1.0/s_1;
  for (size_t iRow = 0; iRow < vec1.size(); iRow++) {
    double val1 = vec1.getVecEl(iRow);
    double val2 = vec2.getVecEl(iRow);
    double weight_1 = b/(sqrt(b*b + (lambda_1-a)*(lambda_1-a)));
    double weight_2 = (lambda_1-a)/(sqrt(b*b + (lambda_1-a)*(lambda_1-a)));
    double val = weight_1 * val1 + weight_2*val2;
    val *= factor;
    U.setMatEl(iRow, 0, val);
  }


  //Assemble 2nd column
  factor = 1.0/s_2;
  for (size_t iRow = 0; iRow < vec1.size(); iRow++) {
    double val1 = vec1.getVecEl(iRow);
    double val2 = vec2.getVecEl(iRow);
    double weight_1 = b/(sqrt(b*b + (lambda_2-a)*(lambda_2-a)));
    double weight_2 = (lambda_2-a)/(sqrt(b*b + (lambda_2-a)*(lambda_2-a)));
    double val = weight_1 * val1 + weight_2*val2;
    val *= factor;
    U.setMatEl(iRow, 1, val);
  }
  U.finalize();
};



/* SVD 2 vecs: Compute the low memory SVD of two vectors
   -inputs:two vecs
   -outputs: U, s (sizes assigned externally)
*/
void Network::lowMem_svd2vecs(vector<double>& vec1, vector<double>& vec2, mat& U, vec& s){
  const unsigned int nDof = vec1.size();
  //Compute a b and c
  double a = 0.0;
  double b = 0.0;
  double c = 0.0;

  for (size_t i = 0; i < nDof; i++) {
    a += vec1[i] * vec1[i];
    b += vec1[i] * vec2[i];
    c += vec2[i] * vec2[i];
  }


  double lambda_1 = 0.5 * ((a + c) + sqrt((a-c)*(a-c) + 4.0*b*b));
  double lambda_2 = 0.5 * ((a + c) - sqrt((a-c)*(a-c) + 4.0*b*b));

  double s_1 = sqrt(lambda_1);
  double s_2 = sqrt(lambda_2);

  s.setVecEl(0, s_1);
  s.setVecEl(1, s_2);

  s.finalize();
  //Assemble first column
  double factor = 1.0/s_1;
  for (size_t iRow = 0; iRow < vec1.size(); iRow++) {
    double val1 = vec1[iRow];
    double val2 = vec2[iRow];
    double weight_1 = b/(sqrt(b*b + (lambda_1-a)*(lambda_1-a)));
    double weight_2 = (lambda_1-a)/(sqrt(b*b + (lambda_1-a)*(lambda_1-a)));
    double val = weight_1 * val1 + weight_2*val2;
    val *= factor;
    U.setMatEl(iRow, 0, val);
  }


  //Assemble 2nd column
  factor = 1.0/s_2;
  for (size_t iRow = 0; iRow < vec1.size(); iRow++) {
    double val1 = vec1[iRow];
    double val2 = vec2[iRow];
    double weight_1 = b/(sqrt(b*b + (lambda_2-a)*(lambda_2-a)));
    double weight_2 = (lambda_2-a)/(sqrt(b*b + (lambda_2-a)*(lambda_2-a)));
    double val = weight_1 * val1 + weight_2*val2;
    val *= factor;
    U.setMatEl(iRow, 1, val);
  }
  U.finalize();
};

/* SVD low memory of a set of vector of vectors supposing nDof>nOfVecs*/
void Network::lowMem_svd(vector<vector<double>*>& fibers, mat& U, vector<double>& s, double& tol){
  const unsigned int nfibers = fibers.size();
  const unsigned int nDofs = fibers[0]->size();

  if (nfibers <= nDofs) {
    mat C_0(nfibers, nfibers, PETSC_COMM_WORLD);
    for (unsigned int iFiber = 0; iFiber < nfibers; iFiber++) {
      //vec iVec = fibers[iFiber];
      for (unsigned int jFiber = 0; jFiber <= iFiber; jFiber++) {
        //  vec jVec = fibers[jFiber];
          double value = 0.0;
          for (unsigned int k = 0; k < nDofs; k++) {
            value += (*fibers[iFiber])[k] * (*fibers[jFiber])[k];
          }
          //double value = scalProd(iVec, jVec);
          if (fabs(value) > DBL_EPSILON) {
            C_0.setMatEl(iFiber, jFiber, value);
            if (iFiber != jFiber) {
              C_0.setMatEl(jFiber, iFiber, value);
            }
          }
        }
      }
      C_0.finalize();
      mat I = eye(C_0.nRows(),C_0.comm());
      I *= DBL_EPSILON;
      C_0 += I;
      I.clear();
      eigenSolver eps_0;
      eps_0.init(C_0, EPS_HEP, C_0.nCols(), 1.0e-12);
      eps_0.solveHermEPS(C_0);
      vector<vec> vv_0 = eps_0.eigenVecs();
      vector<double> lambda_0 = eps_0.eigenVals();
      if (lambda_0.size() == 0 ) {
        C_0.print();
      }
      cout << "size of lambda0 = "<< lambda_0.size();

      unsigned int toKeep = lambda_0.size();
      double tail = 0.0;

      while ((tail <= tol * tol) && (toKeep > 1)) { //added that cause if not if the error is too large it doesnt take anything
        tail += lambda_0[toKeep-1];
        toKeep -=1;
      }
      toKeep +=1;
      cout << "tokeep =" << toKeep<<endl;

      s.resize(toKeep);
      for (size_t iEl = 0; iEl < toKeep; iEl++) {
        s[iEl] = sqrt(fabs(lambda_0[iEl]));
      }
      U.init(nDofs, toKeep, PETSC_COMM_WORLD);
      //If you need to fill the matrix write here the code
      U.finalize();
      //Free the memory
      C_0.clear();
      I.clear();
      eps_0.clear();
    }else{
      mat C_0(nDofs, nDofs, PETSC_COMM_WORLD);
      for (size_t iDof = 0; iDof < nDofs; iDof++) {
        for (size_t jDof = 0; jDof <= iDof; jDof++) {
          double entry = 0.0;
          for (size_t k = 0; k < nfibers; k++) {
            double value1 = (*fibers[k])[iDof];
            double value2 = (*fibers[k])[jDof];
            entry += value1 * value2;
          }
          if (fabs(entry) > DBL_EPSILON) {
            C_0.setMatEl(iDof, jDof, entry);
            if (iDof != jDof) {
              C_0.setMatEl(jDof, iDof, entry);
            }
          }
        }
      }
      C_0.finalize();
      mat I = eye(C_0.nRows(),C_0.comm());
      I *= DBL_EPSILON;
      C_0 += I;
      I.clear();
      //C_0 +=DBL_EPSILON*eye(C_0.nRows(), C_0.comm());

      // solve the eigenvalue problem for 20:
      eigenSolver eps_0;
      eps_0.init(C_0, EPS_HEP, C_0.nRows(), 1.0e-12);
      eps_0.solveHermEPS(C_0);
      vector<vec> vv_0 = eps_0.eigenVecs();
      vector<double> lambda_0 = eps_0.eigenVals();

      unsigned int n_eig_0 = lambda_0.size();

      if (n_eig_0 ==0){
        C_0.print();
      }

      unsigned int toKeep = lambda_0.size();
            double tail = 0.0;

            while ((tail <= tol * tol) && (toKeep > 1)) { //added that cause if not if the error is too large it doesnt take anything
              tail += lambda_0[toKeep-1];
              toKeep -=1;
            }
            toKeep +=1;
            cout << "tokeep =" << toKeep<<endl;

            s.resize(toKeep);
            for (size_t iEl = 0; iEl < toKeep; iEl++) {
              s[iEl] = sqrt(fabs(lambda_0[iEl]));
            }
            U.init(nDofs, toKeep, PETSC_COMM_WORLD);
            //If you need to fill the matrix write here the code
            U.finalize();
            //Free the memory
            C_0.clear();
            I.clear();
            eps_0.clear();
    }
};





/* SVD vecs: Compute the SVD of a set of vectors
   -inputs: A set of vector<vec>
   -outputs: U, s (sizes not assigned externally)
*/
void Network::svdVecs(vector<vec>& vecs, mat& U, vec& s){
 const unsigned int nOfVecs = vecs.size();
 const unsigned int nDof = vecs[0].size();
 if (nOfVecs <= nDof) {
   /*
   mat C =  compute_cov_mat(vecs);
   cout << "covariance computed" << endl;
   eigenSolver eps_0;
   eps_0.init(C, EPS_HEP, C.nCols(), 1.0e-12);
   eps_0.solveHermEPS(C);
   vector<vec> vv_0 = eps_0.eigenVecs();
   vector<double> lambda_0 = eps_0.eigenVals();*/

   //s.init(lambda_0.size(), PETSC_COMM_WORLD);
   //U.init(nDof,lambda_0.size(), PETSC_COMM_WORLD);
   vector <vec> Uvec;
   vector<double> Svec;
   vector<vec> Vvec;
   double tolvec = 1.0e-12;
    compute_svd(vecs,Uvec, Svec, Vvec,tolvec);
    U.init(nDof, Svec.size(), PETSC_COMM_WORLD);
    s.init(Svec.size(), PETSC_COMM_WORLD);
    for(unsigned int iCol=0; iCol<Svec.size(); iCol++){
      s.setVecEl(iCol,sqrt(Svec[iCol]));
      for(unsigned int iRow=0; iRow<Uvec[0].size(); iRow++){
        double value = Uvec[iCol].getVecEl(iRow); // if comm is the same use getVecElNoBroadcast
        U.setMatEl(iRow,iCol,value);
      }
    }
   for (size_t i = 0; i < Svec.size(); i++) {
     Uvec[i].clear();
     Vvec[i].clear();
   }
/*
   //Fill s
   for (size_t iEl = 0; iEl < lambda_0.size(); iEl++) {
     s.setVecEl(iEl,sqrt(lambda_0[iEl]));
   }
   s.finalize();
   cout << "eigenvalues computed" << endl;
   //Fill U
   cout << "size lambda " << lambda_0.size() << endl;
   for (size_t iCol = 0; iCol < lambda_0.size(); iCol++) {
     cout << "lambda = " << lambda_0[iCol] << endl;
     double factor = 1.0/sqrt(lambda_0[iCol]);
     cout << iCol << endl;
     for (size_t iRow = 0; iRow < nDof; iRow++) {
       double value = 0.0;
       for (size_t kWeight = 0; kWeight < vv_0[0].size(); kWeight++) {
        double weight = vv_0[iCol].getVecEl(kWeight);
        double value = vecs[kWeight].getVecEl(iRow);
        value += weight*value;
       }
       value = value * factor;
       U.setMatEl(iRow, iCol, value);
     }
   }
   */
   s.finalize();
   U.finalize();
   cout << "U filled" << endl;
   //C.clear();
   //eps_0.clear();
 } else {
   //Computing C = V V^T
   const unsigned int nMod = vecs.size();
   mat C(vecs[0].size(),vecs[0].size(), vecs[0].comm());
   for(unsigned int i=0; i<vecs[0].size(); i++){
     for(unsigned int j=0; j<=i; j++){
       double entry = 0;
       for (size_t k = 0; k < nMod; k++) {
         double val_i = vecs[k](i);
         double val_j = vecs[k](j);
         entry += val_i * val_j;
       }
       C(i,j)=entry;
       if (i != j) {
         C(j,i) = entry;
       }
     }
   }
   C.finalize();
   C += DBL_EPSILON*eye(C.nRows(),C.comm());

   eigenSolver eps_0;
   eps_0.init(C, EPS_HEP, C.nCols(), 1.0e-12);
   eps_0.solveHermEPS(C);
   vector<vec> vv_0 = eps_0.eigenVecs();
   vector<double> lambda_0 = eps_0.eigenVals();


   s.init(lambda_0.size(), PETSC_COMM_WORLD);
   U.init(nDof,lambda_0.size(), PETSC_COMM_WORLD);

   for (size_t iEl = 0; iEl < lambda_0.size(); iEl++) {
     s.setVecEl(iEl, sqrt(lambda_0[iEl]));
   }
   s.finalize();

   for (size_t iCol = 0; iCol < lambda_0.size(); iCol++) {
     for (size_t iRow = 0; iRow < vecs[0].size(); iRow++){
       double value = vv_0[iCol](iRow);
       U.setMatEl(iRow,iCol,value);
     }
   }
   U.finalize();

   C.clear();
   eps_0.clear();

 }
};


/* Compute the product of the Ubase transpose and one fiber restricted to 2 components
*/
vec Network::Ut_dot_Fib(mat& Ubase, vec& fib){
  vec output(2, Ubase.comm());
  //double first_comp = 0.0;
  //double sec_comp = 0.0;
  /*for (size_t iDof = 0; iDof < Ubase.nRows(); iDof++) {
    double matVal0 = Ubase.getMatEl(iDof, 0);
    double matVal1 = Ubase.getMatEl(iDof, 1);
    double vecVal = fib.getVecEl(iDof);
    first_comp += matVal0 * vecVal;
    sec_comp += matVal1 * vecVal;
  }*/
  Vec theCol0;
  PetscErrorCode ierr;
  ierr = VecCreate(Ubase.comm(), &theCol0);
  ierr = VecSetSizes(theCol0, PETSC_DECIDE, Ubase.nRows());
  ierr = VecSetFromOptions(theCol0);
  MatGetColumnVector(Ubase.M(), theCol0, 0);
  double first_comp = 0.0;
  VecDot(theCol0, fib.x(), &first_comp);
  Vec theCol1;
  ierr = VecCreate(Ubase.comm(), &theCol1);
  ierr = VecSetSizes(theCol1, PETSC_DECIDE, Ubase.nRows());
  ierr = VecSetFromOptions(theCol1);
  MatGetColumnVector(Ubase.M(), theCol1, 1);
  double sec_comp = 0.0;
  VecDot(theCol1, fib.x(), &sec_comp);

  output.setVecEl(0,first_comp);
  output.setVecEl(1,sec_comp);
  output.finalize();
  VecDestroy(&theCol0);
  VecDestroy(&theCol1);
  return output;
};



/* Compute the product of the Ubase transpose and one fiber restricted to 2 components
    Low memory version
*/
vec Network::lowMem_Ut_dot_Fib(mat& Ubase, vector<double>& fib){
  vec output(2, Ubase.comm());
  double first_comp = 0.0;
  double sec_comp = 0.0;
  for (size_t iDof = 0; iDof < Ubase.nRows(); iDof++) {
    double matVal0 = Ubase.getMatEl(iDof, 0);
    double matVal1 = Ubase.getMatEl(iDof, 1);
    double vecVal = fib[iDof];
    first_comp += matVal0 * vecVal;
    sec_comp += matVal1 * vecVal;
  }
  output.setVecEl(0,first_comp);
  output.setVecEl(1,sec_comp);
  output.finalize();

  return output;
};




/* Compute the nearest neighbor of a fiber given a set of fibers
  - inputs: the set of fibers, the list of remaining fibers and the id of the fiber we look the neighbor of
  - outputs: the id of the nearest neighbor. The position in the list of the id. And the distance.
*/
unsigned int Network::FindNeighbor(vector<vec>& fibers, vector<unsigned int>& list, unsigned int idFiber){
  const unsigned int fibersSize = fibers.size();
  assert(idFiber < fibersSize);
  //cout << "FibersSize: " << fibers.size() << endl;
  const unsigned int nDof = fibers[0].size();
  MPI_Comm fibersComm = fibers[0].comm();
  fibers[idFiber] *= -1.0;
  double bestDist = 1.0e12;
  unsigned int bestFib = 0;
  const unsigned int listSize = list.size();
  //cout << "listSize: " << list.size() << endl;


  for (unsigned int iFib = 0; iFib < listSize; iFib++) {
    if (list[iFib] != idFiber){
      //cout << idFiber << endl;
    //  cout << "list[iFib] = "<< list[iFib] << endl;
      vec difference = fibers[idFiber] + fibers[list[iFib]];
      double normDiff = difference.norm();
      //cout << "norm[iFib] = " << difference.norm() << endl;
      difference.clear();
      if (normDiff < bestDist){
        bestFib = iFib;
        //bestFib = list[iFib];
        bestDist = normDiff;
      }
    }
  }
  fibers[idFiber] *= -1.0;
  return bestFib;

};



/* Compute the nearest neighbor of a fiber given a set of fibers. Low in memory version.
  - inputs: the set of fibers, the list of remaining fibers and the id of the fiber we look the neighbor of
  - outputs: the id of the nearest neighbor. The position in the list of the id. And the distance.
*/
unsigned int Network::lowMem_FindNeighbor(vector<vector<double>*>& fibers, vector<unsigned int>& list, unsigned int idFiber){
  const unsigned int fibersSize = fibers.size();
  assert(idFiber < fibersSize);
  //cout << "FibersSize: " << fibers.size() << endl;
  const unsigned int nDof = fibers[0]->size();
  double bestDist = 1.0e12;
  unsigned int bestFib = 0;
  const unsigned int listSize = list.size();
  //cout << "listSize: " << list.size() << endl;


  for (unsigned int iFib = 0; iFib < listSize; iFib++) {
    if (list[iFib] != idFiber){
      //cout << idFiber << endl;
    //  cout << "list[iFib] = "<< list[iFib] << endl;
      double dist = 0.0;
      for (size_t iRow = 0; iRow < nDof; iRow++) {
        double val = (*fibers[idFiber])[iRow] - (*fibers[list[iFib]])[iRow];
        dist += val*val;
      }
      //cout << "norm[iFib] = " << difference.norm() << endl;
      if ((dist < bestDist) && (dist > 1.0e-12)){
        bestFib = iFib;
        //bestFib = list[iFib];
        bestDist = dist;
      }
    }
  }
  return bestFib;

};




/* Compute the nearest neighbor of a fiber given a set of fibers
 - input: The set of fibers, the position of the fiber given
 - output: The position of the neighbor
*/

unsigned int Network::FindNeighbor(vector<vec>& centroids, unsigned int idPosition){
  const unsigned int fibersSize = centroids.size();
  assert(idFiber < fibersSize);
  //cout << "Centroids Size: " << centroids.size() << endl;
  const unsigned int nDof = centroids[0].size();
  MPI_Comm fibersComm = centroids[0].comm();
  centroids[idPosition] *= -1.0;
  double bestDist = 1.0e12;
  unsigned int bestFib = 0;

  for (unsigned int iFib = 0; iFib < fibersSize; iFib++) {
    if (iFib != idPosition){
      //cout << idFiber << endl;
      //cout << "list[iFib] = "<< list[iFib] << endl;
      vec difference = centroids[idPosition] + centroids[iFib];
      double normDiff = difference.norm();
      //cout << "norm[iFib] = " << difference.norm() << endl;
      difference.clear();
      if (normDiff < bestDist){
        bestFib = iFib;
        //bestFib = list[iFib];
        bestDist = normDiff;
      }
    }
  }
  centroids[idPosition] *= -1.0;
  return bestFib;

};



/* Compute the nearest neighbor of a fiber given a set of fibers. Low memory version
 - input: The set of fibers, the position of the fiber given
 - output: The position of the neighbor
*/

unsigned int Network::lowMem_FindNeighbor(vector<vector<double>*>& centroids, unsigned int idPosition){
  const unsigned int fibersSize = centroids.size();
  assert(idFiber < fibersSize);
  const unsigned int nDof = centroids[0]->size();
  //MPI_Comm fibersComm = centroids[0].comm();
  //centroids[idPosition] *= -1.0;
  double bestDist = 1.0e12;
  unsigned int bestFib = 0;

  for (unsigned int iFib = 0; iFib < fibersSize; iFib++) {
    if (iFib != idPosition){
      //vec difference = centroids[idPosition] + centroids[iFib];
      double dist = 0.0;
      for (size_t iInd = 0; iInd < centroids[iFib]->size(); iInd++) {
        double difference = (*centroids[iFib])[iInd] - (*centroids[idPosition])[iInd];
         dist += difference * difference;
      }
      //double normDiff = difference.norm();
      //difference.clear();
      if (dist < bestDist){
        bestFib = iFib;
        bestDist = dist;
      }
    }
  }
  //centroids[idPosition] *= -1.0;
  return bestFib;

};




/* Given two fibers, compute the 2d basis
  - inputs: the two fibers
  - outputs: the basis
*/
vector <vec> Network::Compute2dBasis(vec& fib1, vec& fib2){
  svd vecSvd({fib1, fib2});
  mat Usvd = vecSvd.Umat();

  vector<vec> theBasis(2);
  theBasis[0]= Usvd.getCol(0);
  theBasis[1]= Usvd.getCol(1);

  vecSvd.clear();
  return theBasis;
};


/* Compute the 2D-bases given a set of fibers
  - inputs: the set of fibers, the tolerance eps
  - outputs: the vector of Matrix basis, the vector of the indices of the fibers on each basis, the error budget per basis.
*/
void Network::ComputeBases(vector<vec>& fibers, double eps, vector<mat>& Basis, vector<vector<unsigned int>>& indOnBasis, vector<double>& errBudget){
  const unsigned int fibersSize = fibers.size();
  cout << fibersSize << endl;

  vector<unsigned int> listVector(fibersSize);
  for (unsigned int iFib = 0; iFib < fibersSize; iFib++) {
     listVector[iFib]=iFib;
   }
   unsigned int ccBase = 0;

  while (listVector.size() > 1) {
    unsigned int ind_0 = listVector[0];
    //vec first = fibers[ind_0];


    if (listVector.size() > 1) { //when there's only one left

      cout << "List vector size = " << listVector.size() << endl;
      unsigned int relative_index = FindNeighbor(fibers, listVector, ind_0);
      cout << "Neighbor found" << endl;
      unsigned int second_index = listVector[relative_index];
      //vec second = fibers[second_index];

      listVector.erase(listVector.begin()); //remove first

      listVector.erase(listVector.begin()+relative_index-1); //remove second

      //cout << "Updating list" << endl;
      //vector<vec> first_base = Compute2dBasis(first, second);

      vector<unsigned int> indBase; //Save the indices of the fibers in a basis
      indBase.push_back(ind_0);
      indBase.push_back(second_index);
      /*
      vector<unsigned int>* indBase;
      indBase = new(vector<unsigned int>);
      indBase -> resize(2);
      (*indBase)[0] = ind_0;
      (*indBase)[1] = second_index;*/


      double budget = 0.0; //Save the error
      //cout<< "before SVD" << endl;

      mat Ubase(fibers[ind_0].size(), 2, PETSC_COMM_WORLD);
      vec Sbase(2, PETSC_COMM_WORLD);
      svd2vecs(fibers[ind_0], fibers[second_index], Ubase, Sbase);
      /*
      svd svd_first_base({first, second});

      mat Ubase;
      Ubase.copyMatFrom(svd_first_base.Umat());
      vec Sbase;
      Sbase.copyVecFrom(svd_first_base.Svec());

      svd_first_base.clear();
      */
      //cout<< "after SVD" << endl;

      //cout << "Basis computed" << endl;
      // Ubase.print();
       //Sbase.print();

      unsigned int ccFib = 0;
      vector<unsigned int> posToRemove;

    //  mat U_t = transpose(Ubase);

      cout << "Basis computed" << endl;

      for (size_t iInd = 0; iInd < listVector.size(); iInd++) {
        unsigned int index = listVector[iInd];
        //cout << "index: " << index <<endl;
        vec p(fibers[0].size(), fibers[0].comm());
        p.copyVecFrom(fibers[index]);
        //p.print();

        //vec alpha(2, fibers[0].comm());
        //matVecProd(U_t, fibers[index], alpha);
        vec alpha = Ut_dot_Fib(Ubase, fibers[index]);
        //alpha.print();

        vec u_alpha(fibers[0].size(), fibers[0].comm());
        matVecProd(Ubase, alpha, u_alpha);
        //u_alpha.print();
        u_alpha *= -1.0;

        p +=u_alpha;
        //p.print();
        double error = p.norm();
        u_alpha.clear();
        //cout << "the error: " << error << " norm of fibers index: " << fibers[index].norm() << endl;
        double delta2 = (eps*eps)/listVector.size();
        //cout << "The error division: " << delta2/(error * error) << endl;
        if (error * error <= delta2) {
          cout << "We enter in the tilting " << endl;
          delta2 = (eps*eps - error * error)/(listVector.size()-1);
          //cout << "the new delta2 = " << delta2 << endl;

          indBase.push_back(index);
          posToRemove.push_back(iInd);
          budget = delta2;

          //Trying with SVD
          /*
          mat Mfib(fibers[0].size(),indBase.size()+1, PETSC_COMM_WORLD);
          unsigned int cc = 0;
          for (size_t iFib = 0; iFib < indBase.size(); iFib++) {
            Mfib.setCol(cc,fibers[indBase[iFib]]);
            cc +=1;
          }
          cout <<"Mfib computed" << endl;
          Mfib.setCol(indBase.size(), fibers[index]);
          svd svd_M(Mfib);
          Mfib.clear();
          mat A = transpose(svd_M.Umat());
          mat I = A * svd_M.Umat();
          I.print();

          A.clear();
          I.clear(); */
        //  p.clear();
          //alpha.clear();

          //Assemble matrix K
          mat K(3,3,PETSC_COMM_WORLD);
          K(0,0)= Sbase(0);
          K(1,1)= Sbase(1);
          K(1,0)= 0.0;
          K(0,1)= 0.0;
          K(0,2)= alpha(0);
          K(1,2)= alpha(1);
          K(2,2)= error;

          K.finalize();
          alpha.clear();

          //mat Usvd;
          //mat Vsvd;
          //mat Ssvd;
          svd svd_K(K);
          //compute_svd(K, Usvd, Ssvd, Vsvd, 1.0e-12);

          //Update the sing values
          Sbase(0) = svd_K.Svec()(0);
          Sbase(1) = svd_K.Svec()(1);

          //Update the basis
          mat U_k(3, 2, PETSC_COMM_WORLD); //3 x2
          //U_k.setCol(0,svd_K.Umat().getCol(0));
          //U_k.setCol(1,svd_K.Umat().getCol(1));
          for (size_t iRow = 0; iRow < 3; iRow++) { //3
            for (size_t iCol = 0; iCol < 2; iCol++) { //2
              double val = svd_K.Umat().getMatEl(iRow,iCol);
              U_k.setMatEl(iRow,iCol,val);
            }
          }
          U_k.finalize();

          mat Up(fibers[index].size(), 3, PETSC_COMM_WORLD);
          //Up.setCol(0,Ubase.getCol(0));
          //Up.setCol(1,Ubase.getCol(1));
          //Up.setCol(2,p);
          for (size_t iRow = 0; iRow < fibers[index].size(); iRow++) {
            for (size_t iCol = 0; iCol < 2; iCol++) {
              double val = Ubase.getMatEl(iRow,iCol);
              Up.setMatEl(iRow, iCol, val);
            }
            double val2 = p.getVecEl(iRow);
            val2 *= 1./error;
            Up.setMatEl(iRow,2,val2);
          }
          Up.finalize();

          //Ubase = Up * U_k;
          matMatMult(Up, U_k, Ubase);


          //U_t = transpose(Ubase);
          ccFib +=1;

          svd_K.clear();
          Up.clear();
          U_k.clear();
          K.clear();
          p.clear();


        } else{
          p.clear();
          alpha.clear();
        }

        //cout << "I'm not entering " << endl;
      }
      //U_t.clear();
      /* Remove the position of the indices in the list
      */
      for (size_t i = 0; i < posToRemove.size(); i++) {
        unsigned int iToRemove = posToRemove.size()-1-i;
        listVector.erase(listVector.begin()+iToRemove);
      }

      cout << listVector.size() << endl;

      Basis.push_back(Ubase); //Saved after the possible tiltings
      indOnBasis.push_back(indBase);
      errBudget.push_back(budget); //save the errors
      Sbase.clear();
      ccBase +=1;
    } else {
      vec theLastFib;
      theLastFib.copyVecFrom(fibers[listVector[0]]);
      theLastFib *= 1.0/theLastFib.norm();

      mat theLastFibMat(fibers[0].size(), 1, PETSC_COMM_WORLD);
      theLastFibMat.setCol(0, theLastFib);
      theLastFib.clear();

      Basis.push_back(theLastFibMat);
      vector<unsigned int> lastIndex(1);
      lastIndex[0]=listVector[0];
      indOnBasis.push_back(lastIndex); //1d basis
      errBudget.push_back(0.0);
    }
  cout << "Finishing cicle" << endl;
  }

};





/* Compute the 2D-bases given a set of fibers. Low memory version.
  - inputs: the set of fibers, the tolerance eps
  - outputs: the vector of Matrix basis, the vector of the indices of the fibers on each basis, the error budget per basis.
*/

void Network::lowMem_ComputeBases(vector<vector<double>*>& fibers, double eps, vector<mat>& Basis, vector<vector<unsigned int>>& indOnBasis, vector<double>& errBudget, double& globalBudget){
  const unsigned int fibersSize = fibers.size();
  cout << fibersSize << endl;

  vector<unsigned int> listVector(fibersSize);
  for (unsigned int iFib = 0; iFib < fibersSize; iFib++) {
     listVector[iFib]=iFib;
   }
   //unsigned int ccBase = 0;

   //Initial budget
   double budget = eps * eps; //this is a global error

  while (listVector.size() >= 1) {
    unsigned int ind_0 = listVector[0];

    if (listVector.size() > 1) { //when there are more than one left

      cout << "List vector size = " << listVector.size() << endl;
      unsigned int relative_index = lowMem_FindNeighbor(fibers, listVector, ind_0);
      cout << "Neighbor found" << endl;
      unsigned int second_index = listVector[relative_index];

      listVector.erase(listVector.begin()); //remove first

      listVector.erase(listVector.begin()+relative_index-1); //remove second


      cout << "Updating list" << endl;

      vector<unsigned int> indBase; //Save the indices of the fibers in a basis
      indBase.push_back(ind_0);
      indBase.push_back(second_index);


      double locBudget = 0.0; //Save the LOCAL error this is a vector
      //double budget = (eps*eps)/listVector.size();

      mat Ubase(fibers[ind_0]->size(), 2, PETSC_COMM_WORLD);
      vec Sbase(2, PETSC_COMM_WORLD);
      lowMem_svd2vecs(*fibers[ind_0], *fibers[second_index], Ubase, Sbase);



      //unsigned int ccFib = 0;
      vector<unsigned int> posToRemove;


      cout << "Basis computed" << endl;

      for (size_t iInd = 0; iInd < listVector.size(); iInd++) {
        unsigned int index = listVector[iInd];
        //vec p(fibers[0].size(), fibers[0].comm());
        vector<double> p(fibers[0]->size());
        for (size_t i = 0; i < fibers[0]->size(); i++) {
          p[i] = (*fibers[index])[i];
        }
        //p.copyVecFrom(fibers[index]);


        vec alpha = lowMem_Ut_dot_Fib(Ubase, *fibers[index]);

        //vec u_alpha(fibers[0].size(), fibers[0].comm());
        //matVecProd(Ubase, alpha, u_alpha);
        //u_alpha *= -1.0;

        vector<double> u_alpha(fibers[0]->size());
        for (size_t i = 0; i < fibers[0]->size(); i++) {
          u_alpha[i] = 0.0;
          for (size_t j = 0; j < 2; j++) {
            double val = Ubase.getMatEl(i,j) * alpha.getVecEl(j);
            val *= -1.0;
            u_alpha[i] += val;
          }
        }

        //  p +=u_alpha;

        for (size_t i = 0; i < fibers[0]->size(); i++) {
          p[i] += u_alpha[i];
        }
        u_alpha.clear();

        //double error = p.norm();
        double error = 0.0;
        for (size_t i = 0; i < fibers[0]->size(); i++) {
          error += p[i] * p[i];
        }
        //////////error is a sqared quantity

        double delta2 = budget/listVector.size();
        //double normalizedError2 = error * fibers[0]->size() * fibers.size();
        if (error <= delta2) {
          cout << "We enter in the tilting " << endl;
          budget = budget - error;
          //budget = budget - normalizedError2;
          locBudget = locBudget + error;
          //locBudget = locBudget + normalizedError2;


          indBase.push_back(index);
          posToRemove.push_back(iInd);
          //budget = delta2;


          //Assemble matrix K
          mat K(3,3,PETSC_COMM_WORLD);
          K(0,0)= Sbase(0);
          K(1,1)= Sbase(1);
          K(1,0)= 0.0;
          K(0,1)= 0.0;
          K(0,2)= alpha(0);
          K(1,2)= alpha(1);
          K(2,2)= sqrt(error);

          K.finalize();
          alpha.clear();


          svd svd_K(K);

          //Update the sing values
          Sbase(0) = svd_K.Svec()(0);
          Sbase(1) = svd_K.Svec()(1);

          //Update the basis
          mat U_k(3, 2, PETSC_COMM_WORLD);

          for (size_t iRow = 0; iRow < 3; iRow++) {
            for (size_t iCol = 0; iCol < 2; iCol++) {
              double val = svd_K.Umat().getMatEl(iRow,iCol);
              U_k.setMatEl(iRow,iCol,val);
            }
          }
          U_k.finalize();

          mat Up(fibers[index]->size(), 3, PETSC_COMM_WORLD);

          for (size_t iRow = 0; iRow < fibers[index]->size(); iRow++) {
            for (size_t iCol = 0; iCol < 2; iCol++) {
              double val = Ubase.getMatEl(iRow,iCol);
              Up.setMatEl(iRow, iCol, val);
            }
            double val2 = p[iRow];
            val2 *= 1./(sqrt(error));
            //val2 *= 1./(sqrt(normalizedError2));
            Up.setMatEl(iRow,2,val2);
          }
          Up.finalize();


          matMatMult(Up, U_k, Ubase);



          //ccFib +=1;

          svd_K.clear();
          Up.clear();
          U_k.clear();
          K.clear();
          p.clear();


        } else{ //We don't tilt
          p.clear();
          alpha.clear();
        }
      }
      // Remove the position of the indices in the list

      for (size_t i = 0; i < posToRemove.size(); i++) {
        unsigned int iToRemove = posToRemove.size()-1-i;
        //cout << "pos to remove: " << posToRemove[i]<<endl;
        //cout << "i to remove " << posToRemove[iToRemove] << endl;
        unsigned int remove = posToRemove[iToRemove];
        listVector.erase(listVector.begin()+remove);
      }



      Basis.push_back(Ubase); //Saved after the possible tiltings
      indOnBasis.push_back(indBase);
      //errBudget.push_back(budget); //save the errors
      errBudget.push_back(locBudget); //save the errors

      Sbase.clear();
      //ccBase +=1;
      globalBudget = budget;


    } else {
      cout << "enter in the last fib" << endl;
      //vec theLastFib (fibers[listVector[0]]->size(), PETSC_COMM_WORLD);

      //theLastFib.copyVecFrom(fibers[listVector[0]]);
      /*
      for (size_t i = 0; i < fibers[listVector[0]]->size(); i++) {
        theLastFib.setVecEl(i,(*fibers[listVector[0]])[i]);
      }
      theLastFib.finalize();
      theLastFib *= 1.0/theLastFib.norm();
      cout << "the last fib computed" << endl;*/

      mat theLastFibMat(fibers[0]->size(), 1, PETSC_COMM_WORLD);
      double normLastFib = 0.0;
      for (size_t i = 0; i < fibers[listVector[0]]->size(); i++) {
        theLastFibMat.setMatEl(i,0,(*fibers[listVector[0]])[i]);
        normLastFib += (*fibers[listVector[0]])[i] * (*fibers[listVector[0]])[i];
      }
      theLastFibMat.finalize();
      theLastFibMat *= 1./normLastFib;
      //theLastFibMat.setCol(0, theLastFib);
      //theLastFib.clear();

      Basis.push_back(theLastFibMat);
      vector<unsigned int> lastIndex(1);
      lastIndex[0]=listVector[0];
      indOnBasis.push_back(lastIndex); //1d basis
      errBudget.push_back(0.0);
      globalBudget = budget;
      listVector.erase(listVector.begin());
    }
  cout << "Finishing cicle" << endl;
  }

};







/* Merge the 2d clusters computed in Compute2dBasis
- inputs: the set of fibers, the vector of the indices of the fibers on each basis, the vector of Matrix basis, the error budget per basis.
- outputs: Rewritting of the vector of the indices of the fibers on each new cluster, the vector of matrix basis of each cluster (2d or more).
*/
void Network::Merge2dBases(vector<vec>& fibers, vector<mat>& Basis, vector<vector<unsigned int>>& indOnBasis, vector<double>& errBudget){
  const unsigned int basisVecSize = Basis.size();

  //Compute centroids
  vector<vec> centroids (basisVecSize);
  for (unsigned int iBase = 0; iBase < basisVecSize; iBase++) {
    vector<unsigned int> FibsOnBasis = indOnBasis[iBase];
    vec sumOfFibs(fibers[0].size(), PETSC_COMM_WORLD);
    sumOfFibs.copyVecFrom(fibers[FibsOnBasis[0]]);
    for (unsigned int iFib = 1; iFib < FibsOnBasis.size(); iFib++) {
      sumOfFibs += fibers[FibsOnBasis[iFib]];
    }
    sumOfFibs *= 1./FibsOnBasis.size();
    centroids[iBase] = sumOfFibs;
    //centroids[iBase].print();
  }
  //Compute neighbors

  vector<unsigned int> listCentroids(centroids.size());
  for (unsigned int iCent = 0; iCent < centroids.size(); iCent++) {
    listCentroids[iCent]=iCent;
  }

  vector<unsigned int> listNeighbor(centroids.size()); //list of the absolute indices of the neighbors
  vector<double> listDist(centroids.size()); //List of distances
  vector<unsigned int> listRelNeighbor(centroids.size()); //list of the relative indices of the neighbors

  vector<unsigned int> indToCheck(listCentroids.size());
  for (unsigned int iInd = 0; iInd < listCentroids.size(); iInd++) {
    indToCheck[iInd] = iInd;
  }
  //Initialize the error budget
  vector<unsigned int> listBudget(centroids.size()); //list of the budget
  for (unsigned int iErr = 0; iErr < listBudget.size(); iErr++) {
    //listBudget[iErr] = (eps * eps)/(listBudget.size());
    listBudget[iErr] = errBudget[iErr];
    //unsigned int w = indOnBasis[iErr].size() //for the moment we keep it in the simplest situation
  }


  for (unsigned int iCent = 0; iCent < centroids.size(); iCent++) {
    unsigned int Cent_relative_index = FindNeighbor(centroids, listCentroids, iCent);
    unsigned int Cent_second_index = listCentroids[Cent_relative_index];
    listNeighbor[iCent] = Cent_second_index;
    listRelNeighbor[iCent] = Cent_relative_index;
    //We let it repeat the neighbor -> No while nor if

    //Compute Ward's distance:
    centroids[Cent_second_index]*= -1.0;
    vec difference = centroids[iCent] + centroids[Cent_second_index];
    centroids[Cent_second_index]*= -1.0;
    double dist = difference.norm();
    unsigned int Wconst = indOnBasis[iCent].size() * indOnBasis[Cent_second_index].size();
    Wconst *= 1.0/(indOnBasis[iCent].size() + indOnBasis[Cent_second_index].size());
    dist *= Wconst;
    listDist[iCent] = dist;
    difference.clear();
    //cout << "Distance between neighbor centroids "<< iCent << " and " << Cent_second_index <<" = " << dist <<endl;

  }

  double bestDist = 1.0e12;
  unsigned int indBestDist;
  //Choose the smallest distance and the corresponding pair -> WinningPair
  for (unsigned int iDist = 0; iDist < listDist.size(); iDist++) {
    if (listDist[iDist] < bestDist){
      bestDist = listDist[iDist];
      indBestDist = iDist;
    }
  }
  cout << "The smaller distance = "<< bestDist << " and it is associated to the term " << indBestDist << " and its neighbor " << listNeighbor[indBestDist] <<endl;

  vector<unsigned int> winningPair(2);
  winningPair[0] = indBestDist;
  winningPair[1] = listRelNeighbor[indBestDist];

  bool IhaveToTry = true; //When I should keep fusing or when to stop
  unsigned int cc_HaveToTry = 0;

  while (IhaveToTry == true) {

    //sing values & modes & BOOL = Check fusion(WinningPair)
    vector<vec> setOfVecs;

    for (unsigned int iFib = 0; iFib < indOnBasis[winningPair[0]].size(); iFib++) {
    unsigned int index_0 = indOnBasis[winningPair[0]][iFib];
    setOfVecs.push_back(fibers[index_0]); //memory
    }
    int nCols1 = Basis[winningPair[0]].nCols();
    double memBef1 = setOfVecs[0].size() * nCols1 + nCols1 * indOnBasis[winningPair[0]].size();


    for (unsigned int iFib = 0; iFib < indOnBasis[winningPair[1]].size(); iFib++) {
    unsigned int index_1 = indOnBasis[winningPair[1]][iFib];
    setOfVecs.push_back(fibers[index_1]);
    }
    int nCols2 = Basis[winningPair[1]].nCols();

    double memBef2 = setOfVecs[0].size() * nCols2 + nCols2 * indOnBasis[winningPair[1]].size();

    cout<< "computing svd of the merged set of fibers" << endl;
    cout << "number of terms = " << setOfVecs.size()<< endl;
    cout << "dofs = " << setOfVecs[0].size() << endl;

    /*
    mat Uall;
    vec s;
    svdVecs(setOfVecs, Uall, s);
    cout << "done"  << endl;

    double sumSig = s(0) * s(0);
    for (unsigned int iSig = 1; iSig < s.size(); iSig++) {
      sumSig +=  s(iSig) * s(iSig);
    }
    unsigned int cc = 0;
    while (sumSig >= (listBudget[winningPair[0]] * listBudget[winningPair[0]]) + (listBudget[winningPair[1]] * listBudget[winningPair[1]]) && cc < s.size()-1) {
      sumSig -= s(cc) * s(cc);
      cc += 1;
    } */


    vector <vec> Uvec;
    vector<double> Svec;
    vector<vec> Vvec;
    double tolvec = sqrt((listBudget[winningPair[0]] * listBudget[winningPair[0]]) + (listBudget[winningPair[1]] * listBudget[winningPair[1]]));
     compute_svd(setOfVecs,Uvec, Svec, Vvec,tolvec);

     double energy = 0.0;
     for (size_t iVec = 0; iVec < setOfVecs.size(); iVec++) {
       double normVec = setOfVecs[iVec].norm();
       energy += normVec * normVec;
     }
    double e_budget = energy;
    for (unsigned int iSig = 0; iSig < Svec.size(); iSig++) {
      e_budget -=  Svec[iSig] * Svec[iSig];
    }
    mat U = vectorVec2Mat(Uvec);
    for (size_t i = 0; i < Svec.size(); i++) {
      Uvec[i].clear();
      Vvec[i].clear();
    }
    unsigned int cc = Svec.size();

/*
    double e_budget;

    for (unsigned int iSig = cc+1; iSig < s.size(); iSig++) {
      e_budget +=  s(iSig) * s(iSig);
    }


    mat U = Uall.extractSubmatrix(0, Uall.nRows(), 0, cc+1);

    Uall.clear();
    s.clear();
    cout<< "finished svd of the merged set of fibers" << endl;
*/
    /*
    svd vecSvd(setOfVecs);
    double sumSig = vecSvd.Svec()(0) * vecSvd.Svec()(0);

    for (unsigned int iSig = 1; iSig < vecSvd.Svec().size(); iSig++) {
    sumSig +=  vecSvd.Svec()(iSig) * vecSvd.Svec()(iSig);
    }

    unsigned int cc = 0;
    while (sumSig >= (listBudget[winningPair[0]] * listBudget[winningPair[0]]) + (listBudget[winningPair[1]] * listBudget[winningPair[1]]) && cc < vecSvd.Svec().size()-1) {
      sumSig -= vecSvd.Svec()(cc) * vecSvd.Svec()(cc);
      cc += 1;
    }

    double e_budget;
    for (unsigned int iSig = cc+1; iSig < vecSvd.Svec().size(); iSig++) {
      e_budget +=  vecSvd.Svec()(iSig) * vecSvd.Svec()(iSig);
    }
    */


    cout << "The number of terms taken is = " << cc + 1 <<endl;
    /*
    mat U(setOfVecs[0].size(),cc+1, PETSC_COMM_WORLD);
    // for (unsigned int iu = 0; iu < cc+1; iu++) {
    //     for (unsigned int iuc = 0; iuc < setOfVecs[0].size(); iuc++) {
    //       U(iuc, iu) = vecSvd.Umat()(iuc, iu)
    //     }
    // }
    // U.finalize();
    //
    //vecSvd.Umat().print();
    vec col(setOfVecs[0].size());

    for (unsigned int iu = 0; iu < cc+1; iu++) {
      col = vecSvd.Umat().getCol(iu);
      U.setCol(iu, col);
    }
    U.finalize();
    col.clear();

    vecSvd.clear();
    */
    //cout << "SVD done " <<endl;

    //Memory checking
    double memAft = setOfVecs[0].size() * (cc+1) + (cc+1) * (indOnBasis[winningPair[0]].size() + indOnBasis[winningPair[1]].size());
    cout << "Memory after the fusing = " << memAft << endl;
    double memBef = memBef1 + memBef2;
    cout << "Memory before the fusing = " << memBef << endl;



    if (memAft < memBef) {  //WeFuse
      cout << "We fuse" << endl;
      cc_HaveToTry = 0;

      //Compare the memory with the areas
      //Compute new centroid

      vec sumOfFibs(setOfVecs[0].size(), PETSC_COMM_WORLD);

      for (unsigned int iFib = 0; iFib < setOfVecs.size(); iFib++) {
        sumOfFibs += setOfVecs[iFib];
      }
      sumOfFibs *= 1./setOfVecs.size();
      centroids[indBestDist].clear();
      centroids[indBestDist] = sumOfFibs; //Replace the value of the centroid and the basis
      Basis[indBestDist].clear();
    //  Basis[indBestDist].copyMatFrom(U);
      Basis[indBestDist]=U;

      //Update centroids and the list of centroids and basis
      centroids[listRelNeighbor[indBestDist]].clear(); //Erase the position of the neighbor in centroids
      Basis[listRelNeighbor[indBestDist]].clear(); //Erase the position of the neighbor in centroids

      //Clear and update the budget
      //listBudget[indBestDist].clear();
      listBudget[indBestDist] = e_budget;
      //listBudget[listRelNeighbor[indBestDist]].clear();
      listBudget.erase(listBudget.begin()+listRelNeighbor[indBestDist]);

      indToCheck.erase(indToCheck.begin()+listRelNeighbor[indBestDist]);

      //erase
      listCentroids.erase(listCentroids.begin()+listRelNeighbor[indBestDist]);
      centroids.erase(centroids.begin()+listRelNeighbor[indBestDist]);
      Basis.erase(Basis.begin()+listRelNeighbor[indBestDist]);

      //To reduce in 1 the size of the lists
      listNeighbor.erase(listNeighbor.begin()+listRelNeighbor[indBestDist]);
      listDist.erase(listDist.begin()+listRelNeighbor[indBestDist]);


      //Update the ind of the fibers well represented
      vector<unsigned int> fusedIndices (indOnBasis[winningPair[0]].size() + indOnBasis[winningPair[1]].size());
      for (unsigned int iInd = 0; iInd < indOnBasis[winningPair[0]].size(); iInd++) {
        fusedIndices[iInd] = indOnBasis[winningPair[0]][iInd];
      }
      for (unsigned int iInd = 0; iInd < indOnBasis[winningPair[1]].size(); iInd++) {
        fusedIndices[iInd + indOnBasis[winningPair[0]].size()] = indOnBasis[winningPair[1]][iInd];
      }

      indOnBasis[indBestDist].clear();
      indOnBasis[indBestDist] = fusedIndices;
      indOnBasis.erase(indOnBasis.begin()+listRelNeighbor[indBestDist]);

      // cout << "The size of the list of centroids now is = " << listCentroids.size() << endl;
      // for (unsigned int iCent = 0; iCent < listCentroids.size(); iCent++) {
      //   cout << listCentroids[iCent] << endl;
      // }

      if (listCentroids.size() == 1 || cc_HaveToTry >= listCentroids.size()) {
        IhaveToTry = false; //only one left or all of them tried
        cout << "the list of centroids: " << listCentroids.size() << " and the counter: " << cc_HaveToTry <<endl;

      }else{

        //Recompute the neighbors and distances

        for (unsigned int iCent = 0; iCent < listCentroids.size(); iCent++) {
          unsigned int Cent_relative_index = FindNeighbor(centroids, iCent);
          //cout << Cent_relative_index << endl;
          unsigned int Cent_second_index = listCentroids[Cent_relative_index];
          listNeighbor[iCent] = Cent_second_index;
          listRelNeighbor[iCent] = Cent_relative_index;

          //We let it repeat the neighbor -> No while nor if

          //Compute Ward's distance:
          centroids[Cent_relative_index]*= -1.0;
          vec difference = centroids[iCent] + centroids[Cent_relative_index];
          centroids[Cent_relative_index]*= -1.0;
          double dist = difference.norm();
          unsigned int Wconst = indOnBasis[iCent].size() * indOnBasis[Cent_relative_index].size();
          Wconst *= 1.0/(indOnBasis[iCent].size() + indOnBasis[Cent_relative_index].size());
          dist *= Wconst;
          listDist[iCent] = dist;
          difference.clear();
          cout << "Distance between neighbor centroids "<< iCent << " and " << Cent_relative_index <<" = " << dist <<endl;

        }

        bestDist = 1.0e12;
        for (unsigned int iDist = 0; iDist < listDist.size(); iDist++) {
          if (listDist[iDist] < bestDist){
            bestDist = listDist[iDist];
            indBestDist = iDist;
          }
        }
        cout << "The smaller distance = "<< bestDist << " and it is associated to the term " << indBestDist << " and its neighbor " << listRelNeighbor[indBestDist] <<endl;

        winningPair[0] = indBestDist;
        winningPair[1] = listRelNeighbor[indBestDist];

        //Recompute indToCheck = listCentroids

        for (unsigned int iInd = 0; iInd < listCentroids.size(); iInd++) {
          indToCheck[iInd] = iInd;
        }

      }

    } else { //We don't fuse
    U.clear(); //We erase the basis
    cc_HaveToTry +=1;
    cout << "we don't fuse" << endl;
      if (listCentroids.size() == 1 || cc_HaveToTry >= listCentroids.size()) {
        IhaveToTry = false; //only one left or all of them tried
        cout << "the list of centroids: " << listCentroids.size() << " and the counter: " << cc_HaveToTry <<endl;
      } else { //TryNextNeighbor
        cout << "Looking for the next neighbor" << endl;


        for (unsigned int iInd = 0; iInd < indToCheck.size(); iInd++) {
          cout << iInd << " and indTocheck: " << indToCheck[iInd] << endl;
        }

        unsigned int indBest = 0;
        bestDist = listDist[indToCheck[0]];
        cout << "indBest: " << indBest << endl;
        cout << "Best dist : " << bestDist << endl;
        cout << "list dist size : " << listDist.size() << endl;
        cout << "ind to check size : " << indToCheck.size() << endl;



        for (unsigned int iInd = 0; iInd < indToCheck.size(); iInd++) {
          unsigned int index = indToCheck[iInd];
          if (listDist[index] < bestDist) {
            bestDist = listDist[index];
            indBest = iInd;
          }
        }

        cout << "indBest: " << indBest << endl;
        indToCheck.erase(indToCheck.begin()+indBest);

        vector<unsigned int> winningPair(2);
        winningPair[0] = indBest;
        winningPair[1] = listRelNeighbor[indBest];

        indBestDist = indBest;

      }
    }
  }

//Clean the memory
for (size_t i = 0; i < centroids.size(); i++) {
  centroids[i].clear();
}

};





/* Merge the 2d clusters computed in Compute2dBasis. Low memory version.
- inputs: the set of fibers, the vector of the indices of the fibers on each basis, the vector of Matrix basis, the error budget per basis.
- outputs: Rewritting of the vector of the indices of the fibers on each new cluster, the vector of matrix basis of each cluster (2d or more).
*/
void Network::lowMem_Merge2dBases(string name, vector<vector<double>*>& fibers, vector<mat>& Basis, vector<vector<unsigned int>>& indOnBasis, vector<double>& listBudget, double& globalBudget, vector<double>& memory){
  const unsigned int basisVecSize = Basis.size();

  unsigned int best_memory = 0;

  //Best ind on basis vector
   vector<vector<unsigned int>> indOnBasis_best(indOnBasis.size());
   for (size_t iEl = 0; iEl < indOnBasis.size(); iEl++) {
      indOnBasis_best[iEl].resize(indOnBasis[iEl].size());
      best_memory += 2 * (indOnBasis[iEl].size() + Basis[iEl].nRows());
     for (size_t ij = 0; ij < indOnBasis[iEl].size(); ij++) {
       double val = indOnBasis[iEl][ij];
       indOnBasis_best[iEl][ij] = val;
     }
   }
   cout << "ind on basis best " << endl;

   unsigned int fusion_counter = 0;

   string outFileName = string("indOnBasis_") + name + string("_")+ to_string(fusion_counter) + (".txt") ;
   save_indOnBasis(indOnBasis, outFileName);
   fusion_counter +=1;

  //Compute centroids
  //vector<vec> centroids (basisVecSize);
  vector<vector<double>*> centroids (basisVecSize);
  for (size_t iBase = 0; iBase < basisVecSize; iBase++) {
    centroids[iBase] = new vector<double>(fibers[0]->size());
    for (size_t iEl = 0; iEl < fibers[0]->size(); iEl++) {
      (*centroids[iBase])[iEl] = 0.0;
    }
  }
  for (unsigned int iBase = 0; iBase < basisVecSize; iBase++) {
    vector<unsigned int> FibsOnBasis = indOnBasis[iBase];
    for (unsigned int iFib = 0; iFib < FibsOnBasis.size(); iFib++) {
      unsigned int ind = FibsOnBasis[iFib];
      for (size_t iEl = 0; iEl < fibers[0]->size(); iEl++) {
        double val = (*fibers[ind])[iEl];
        val *= 1./FibsOnBasis.size();
        (*centroids[iBase])[iEl] += val;
      }
    }
  }

  //Compute neighbors
  vector<unsigned int> listCentroids(centroids.size());
  for (unsigned int iCent = 0; iCent < centroids.size(); iCent++) {
    listCentroids[iCent]=iCent;
  }

  vector<unsigned int> listNeighbor(centroids.size()); //list of the absolute indices of the neighbors
  vector<double> listDist(centroids.size()); //List of distances
  vector<unsigned int> listRelNeighbor(centroids.size()); //list of the relative indices of the neighbors

  vector<unsigned int> indToCheck(listCentroids.size());
  for (unsigned int iInd = 0; iInd < listCentroids.size(); iInd++) {
    indToCheck[iInd] = iInd;
  }
  cout << "list of centroids : " << listCentroids.size()<<endl;
  //Initialize the error budget
  //vector<unsigned int> listBudget(centroids.size()); //list of the budget
  for (unsigned int iErr = 0; iErr < listBudget.size(); iErr++) {
    listBudget[iErr] += globalBudget/centroids.size(); //We split in the same way the error
    cout << "the budget to spend is = " << listBudget[iErr] << endl;
  }

  for (unsigned int iCent = 0; iCent < centroids.size(); iCent++) {
    unsigned int Cent_relative_index = lowMem_FindNeighbor(centroids, listCentroids, iCent);
    unsigned int Cent_second_index = listCentroids[Cent_relative_index];
    listNeighbor[iCent] = Cent_second_index;
    listRelNeighbor[iCent] = Cent_relative_index;
    //We let it repeat the neighbor -> No while nor if

    //Compute Ward's distance:
    double dist = 0.0;
    //centroids[Cent_second_index]*= -1.0;
    for (size_t iEl = 0; iEl < fibers[0]->size(); iEl++) {
      double val = (*centroids[iCent])[iEl] - (*centroids[Cent_second_index])[iEl];
      dist += val * val;
    }
    //vec difference = centroids[iCent] + centroids[Cent_second_index];
    //centroids[Cent_second_index]*= -1.0;
    //double dist = difference.norm();
    dist = sqrt(dist);

    unsigned int Wconst = indOnBasis[iCent].size() * indOnBasis[Cent_second_index].size();
    Wconst *= 1.0/(indOnBasis[iCent].size() + indOnBasis[Cent_second_index].size());
    dist *= Wconst;
    listDist[iCent] = dist;
    //difference.clear();
  }

  double bestDist = 1.0e12;
  unsigned int indBestDist;
  //Choose the smallest distance and the corresponding pair -> WinningPair
  for (unsigned int iDist = 0; iDist < listDist.size(); iDist++) {
    if (listDist[iDist] < bestDist){
      bestDist = listDist[iDist];
      indBestDist = iDist;
    }
  }
  cout << "The smaller distance = "<< bestDist << " and it is associated to the term " << indBestDist << " and its neighbor " << listNeighbor[indBestDist] <<endl;

  vector<unsigned int> winningPair(2);
  winningPair[0] = indBestDist;
  winningPair[1] = listRelNeighbor[indBestDist];

  bool IhaveToTry = true; //When I should keep fusing or when to stop
  unsigned int cc_HaveToTry = 0;


  while (IhaveToTry == true) {

    //vector<vec> setOfVecs;
    vector<vector<double>*> setOfVecs;

    for (unsigned int iFib = 0; iFib < indOnBasis[winningPair[0]].size(); iFib++) {
      unsigned int index_0 = indOnBasis[winningPair[0]][iFib];
      setOfVecs.push_back(fibers[index_0]);
    }
    int nCols1 = Basis[winningPair[0]].nCols();
    double memBef1 = setOfVecs[0]->size() * nCols1 + nCols1 * indOnBasis[winningPair[0]].size();


    for (unsigned int iFib = 0; iFib < indOnBasis[winningPair[1]].size(); iFib++) {
      unsigned int index_1 = indOnBasis[winningPair[1]][iFib];
      setOfVecs.push_back(fibers[index_1]);
    }
    int nCols2 = Basis[winningPair[1]].nCols();

    double memBef2 = setOfVecs[0]->size() * nCols2 + nCols2 * indOnBasis[winningPair[1]].size();

    cout<< "computing svd of the merged set of fibers" << endl;
    cout << "number of terms = " << setOfVecs.size()<< endl;
    cout << "dofs = " << setOfVecs[0]->size() << endl;

    mat U;
    vector<double> Svec;
    //vector<vec> Vvec;
    //double tolvec = sqrt((listBudget[winningPair[0]] * listBudget[winningPair[0]]) + (listBudget[winningPair[1]] * listBudget[winningPair[1]]));
    double tolvec = sqrt(listBudget[winningPair[0]] + listBudget[winningPair[1]]);
    cout << "budget" << winningPair[0]<<" = "<< listBudget[winningPair[0]] << " budget" << winningPair[1]<<" = " << listBudget[winningPair[1]]<<endl;
    cout << "tolvec = "<< tolvec << endl;
    lowMem_svd(setOfVecs, U, Svec, tolvec);
     //compute_svd(setOfVecs,Uvec, Svec, Vvec,tolvec);
     cout << "SVD lowmem computed" << endl;

     double energy = 0.0;
     for (size_t iVec = 0; iVec < setOfVecs.size(); iVec++) {
       double normVec2 = 0.0;
      for (size_t iEl = 0; iEl < setOfVecs[iVec]->size(); iEl++) {
        normVec2 += (*setOfVecs[iVec])[iEl] * (*setOfVecs[iVec])[iEl];
      }
      energy += normVec2;
    }
       //double normVec = setOfVecs[iVec].norm();
       //energy += normVec * normVec;

    double e_budget = energy;
    for (unsigned int iSig = 0; iSig < Svec.size(); iSig++) {
      e_budget -=  Svec[iSig] * Svec[iSig];
    }
    if (e_budget < 0){e_budget = 0.0;}
    cout << "budget computed" << endl;

    //mat U = vectorVec2Mat(Uvec);
  //  for (size_t i = 0; i < Svec.size(); i++) {
    //  Uvec[i].clear();
    //  Vvec[i].clear();
    //}
    unsigned int cc = Svec.size();

    cout << "The number of terms taken is = " << cc <<endl;

    //Memory checking
    double memAft = setOfVecs[0]->size() * (cc) + (cc) * (indOnBasis[winningPair[0]].size() + indOnBasis[winningPair[1]].size());
    cout << "Memory after the fusing = " << memAft << endl;
    double memBef = memBef1 + memBef2;
    cout << "Memory before the fusing = " << memBef << endl;




    //if (memAft < 1.0 * memBef) {  //WeFuse
     if(0 < 1){ //Wefuse forced
      cout << "We fuse" << endl;
      cc_HaveToTry = 0;

      double xs = tolvec * tolvec - e_budget;
      if (xs < 0.0 ) {xs = 0.0;}
      //Compare the memory with the areas
      //Compute new centroid

      //vec sumOfFibs(setOfVecs[0]->size(), PETSC_COMM_WORLD);

      //sumOfFibs *= 1./setOfVecs.size();
      centroids[indBestDist]->clear();
      centroids[indBestDist]->resize(setOfVecs[0]->size());
      for (size_t iEl = 0; iEl < setOfVecs[0]->size(); iEl++) {
        (*centroids[indBestDist])[iEl] = 0.0;
      }
      //centroids[indBestDist] = sumOfFibs; //Replace the value of the centroid and the basis
      for (size_t iFib = 0; iFib < setOfVecs.size(); iFib++) {
        for (size_t iEl = 0; iEl < setOfVecs[0]->size(); iEl++) {
          (*centroids[indBestDist])[iEl] += (*setOfVecs[iFib])[iEl];
        }
      }
      Basis[indBestDist].clear();
      Basis[indBestDist]=U;

      cout<< "Basis and set of vecs computed" << endl;

      //Update centroids and the list of centroids and basis
      centroids[listRelNeighbor[indBestDist]]->clear(); //Erase the position of the neighbor in centroids
      Basis[listRelNeighbor[indBestDist]].clear(); //Erase the position of the neighbor in centroids

      //Clear and update the budget
      listBudget[indBestDist] = e_budget;
      listBudget.erase(listBudget.begin()+listRelNeighbor[indBestDist]);
      xs = xs/(listBudget.size());
      for (size_t iBud = 0; iBud < listBudget.size(); iBud++) {
        listBudget[iBud] += xs;
      }

      indToCheck.erase(indToCheck.begin()+listRelNeighbor[indBestDist]);

      //erase
      listCentroids.erase(listCentroids.begin()+listRelNeighbor[indBestDist]);
      centroids.erase(centroids.begin()+listRelNeighbor[indBestDist]);
      Basis.erase(Basis.begin()+listRelNeighbor[indBestDist]);

      //To reduce in 1 the size of the lists
      listNeighbor.erase(listNeighbor.begin()+listRelNeighbor[indBestDist]);
      listDist.erase(listDist.begin()+listRelNeighbor[indBestDist]);


      //Update the ind of the fibers well represented
      vector<unsigned int> fusedIndices (indOnBasis[winningPair[0]].size() + indOnBasis[winningPair[1]].size());
      for (unsigned int iInd = 0; iInd < indOnBasis[winningPair[0]].size(); iInd++) {
        fusedIndices[iInd] = indOnBasis[winningPair[0]][iInd];
      }
      for (unsigned int iInd = 0; iInd < indOnBasis[winningPair[1]].size(); iInd++) {
        fusedIndices[iInd + indOnBasis[winningPair[0]].size()] = indOnBasis[winningPair[1]][iInd];
      }

      indOnBasis[indBestDist].clear();
      indOnBasis[indBestDist] = fusedIndices;
      indOnBasis.erase(indOnBasis.begin()+listRelNeighbor[indBestDist]);

      if (listCentroids.size() == 1 || cc_HaveToTry >= listCentroids.size()) {
        IhaveToTry = false; //only one left or all of them tried
        cout << "the list of centroids: " << listCentroids.size() << " and the counter: " << cc_HaveToTry <<endl;

      }else{

        //Recompute the neighbors and distances

        for (unsigned int iCent = 0; iCent < listCentroids.size(); iCent++) {
          unsigned int Cent_relative_index = lowMem_FindNeighbor(centroids, iCent);
          //cout << Cent_relative_index << endl;
          unsigned int Cent_second_index = listCentroids[Cent_relative_index];
          listNeighbor[iCent] = Cent_second_index;
          listRelNeighbor[iCent] = Cent_relative_index;

          //We let it repeat the neighbor -> No while nor if

          //Compute Ward's distance:
          double dist = 0.0;
          //centroids[Cent_second_index]*= -1.0;
          for (size_t iEl = 0; iEl < fibers[0]->size(); iEl++) {
            double val = (*centroids[iCent])[iEl] - (*centroids[Cent_relative_index])[iEl];
            dist += val * val;
          }
          dist = sqrt(dist);
          unsigned int Wconst = indOnBasis[iCent].size() * indOnBasis[Cent_relative_index].size();
          Wconst *= 1.0/(indOnBasis[iCent].size() + indOnBasis[Cent_relative_index].size());
          dist *= Wconst;
          listDist[iCent] = dist;
          cout << "Distance between neighbor centroids "<< iCent << " and " << Cent_relative_index <<" = " << dist <<endl;

        }

        bestDist = 1.0e12;
        for (unsigned int iDist = 0; iDist < listDist.size(); iDist++) {
          if (listDist[iDist] < bestDist){
            bestDist = listDist[iDist];
            indBestDist = iDist;
          }
        }
        cout << "The smaller distance = "<< bestDist << " and it is associated to the term " << indBestDist << " and its neighbor " << listRelNeighbor[indBestDist] <<endl;

        winningPair[0] = indBestDist;
        winningPair[1] = listRelNeighbor[indBestDist];

        //Recompute indToCheck = listCentroids

        for (unsigned int iInd = 0; iInd < listCentroids.size(); iInd++) {
          indToCheck[iInd] = iInd;
        }
      }
      unsigned int current_memory = computeMemory(Basis, indOnBasis);
      cout << "!!!!!!!The current memory is = " << current_memory << endl;
      memory.push_back(current_memory);

      if (current_memory < best_memory) {
        best_memory = current_memory;
        indOnBasis_best.clear();
        indOnBasis_best.resize(indOnBasis.size());
        for (size_t iEl = 0; iEl < indOnBasis.size(); iEl++) {
           indOnBasis_best[iEl].resize(indOnBasis[iEl].size());
          for (size_t ij = 0; ij < indOnBasis[iEl].size(); ij++) {
            double val = indOnBasis[iEl][ij];
            indOnBasis_best[iEl][ij] = val;
          }
        }
      }

      string outFileName = string("indOnBasis_") + name + string("_")+ to_string(fusion_counter) + (".txt") ;
      save_indOnBasis(indOnBasis, outFileName);
      fusion_counter +=1;

    } else { //We don't fuse
    U.clear(); //We erase the basis
    cc_HaveToTry +=1;
    cout << "we don't fuse" << endl;
      if (listCentroids.size() == 1 || cc_HaveToTry >= listCentroids.size()) {
        IhaveToTry = false; //only one left or all of them tried
        cout << "the list of centroids: " << listCentroids.size() << " and the counter: " << cc_HaveToTry <<endl;
      } else { //TryNextNeighbor
        cout << "Looking for the next neighbor" << endl;


        for (unsigned int iInd = 0; iInd < indToCheck.size(); iInd++) {
          cout << iInd << " and indTocheck: " << indToCheck[iInd] << endl;
        }

        unsigned int indBest = 0;
        bestDist = listDist[indToCheck[0]];
        cout << "indBest: " << indBest << endl;
        cout << "Best dist : " << bestDist << endl;
        cout << "list dist size : " << listDist.size() << endl;
        cout << "ind to check size : " << indToCheck.size() << endl;



        for (unsigned int iInd = 0; iInd < indToCheck.size(); iInd++) {
          unsigned int index = indToCheck[iInd];
          if (listDist[index] < bestDist) {
            bestDist = listDist[index];
            indBest = iInd;
          }
        }

        cout << "indBest: " << indBest << endl;
        indToCheck.erase(indToCheck.begin()+indBest);

        vector<unsigned int> winningPair(2);
        winningPair[0] = indBest;
        winningPair[1] = listRelNeighbor[indBest];

        indBestDist = indBest;

      }
    }
  }

  indOnBasis.clear();
  indOnBasis.resize(indOnBasis_best.size());
  for (size_t iEl = 0; iEl < indOnBasis_best.size(); iEl++) {
     indOnBasis[iEl].resize(indOnBasis_best[iEl].size());
    for (size_t ij = 0; ij < indOnBasis_best[iEl].size(); ij++) {
      double val = indOnBasis_best[iEl][ij];
      indOnBasis[iEl][ij] = val;
    }
  }

//Clean the memory
for (size_t i = 0; i < centroids.size(); i++) {
  centroids[i]->clear();
}

};
