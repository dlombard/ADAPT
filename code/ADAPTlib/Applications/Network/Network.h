//Header file for a network format
#ifndef network_h
#define network_h

// Including headers:
#include "../../genericInclude.h"
#include "../../linAlg/linAlg.h"
#include "../../tensor.h"
#include "../../CP/CPTensor.h"

class Network: public tensor{
private:
  vector <vector <vector <vec> > > m_basesAlphabet;
  vector <unsigned int> m_nBases;
  vector <vector <unsigned int> > m_ranks;
public:
  Network(){};
  ~Network(){};

  // -- ACCESS FUNCTIONS: --
  inline vector <vector <vector <vec> > > basesAlphabet(){return m_basesAlphabet; };
  inline vector <vector <vec > > basesAlphabet(unsigned int iVar){assert (iVar < m_nVar); return m_basesAlphabet[iVar];};
  inline vector <vec > basesAlphabet(unsigned int iVar, unsigned int iBases){assert((iVar < m_nVar)&&(iBases < m_nBases[iVar])); return m_basesAlphabet[iVar][iBases];};
  inline vec basesAlphabet(unsigned int iVar, unsigned int iBases, unsigned int iEl){assert((iVar < m_nVar)&&(iBases < m_nBases[iVar])&&(iEl < m_ranks[iVar][iBases])); return m_basesAlphabet[iVar][iBases][iEl];};
  inline vector<unsigned int > nBases(){return m_nBases;};
  inline unsigned int nBases(unsigned int iVar){assert (iVar < m_nVar); return m_nBases[iVar];};
  inline vector <vector <unsigned int> > ranks(){return m_ranks;};
  inline vector <unsigned int> ranks(unsigned int iVar){assert (iVar < m_nVar); return m_ranks[iVar];};
  inline unsigned int ranks(unsigned int iVar, unsigned int iBases){assert((iVar < m_nVar)&&(iBases < m_nBases[iVar])); return m_ranks[iVar][iBases]; };

  // -- METHODS: --

   //start constructing the probability table

   //given a certain number of groups, construct the greedy algorithm

   /* SVD 2 vecs: Compute the SVD of two vectors
      -inputs: two vecs
      -outputs: U, s (sizes assigned externally)
   */
  void svd2vecs(vec&, vec&, mat&, vec&);


  /* SVD 2 vecs: Compute the low memory SVD of two vectors
     -inputs:two vecs
     -outputs: U, s (sizes assigned externally)
  */
  void lowMem_svd2vecs(vector<double>&, vector<double>&, mat&, vec&);

  /* SVD low memory of a set of vector of vectors supposing nDof>nOfVecs*/

  void lowMem_svd(vector<vector<double>*>&, mat&, vector<double>&, double&);

  /*Load the ind on basis from a vector */
  void load_indOnBasis(string, vector<vector<unsigned int>>&, vector<unsigned int>&, unsigned int&);

  /*Save the ind on basis in a vector */
  void save_indOnBasis(vector<vector<unsigned int>>&, string);


  /* SVD vecs: Compute the SVD of a set of vectors
     -inputs: A set of vector<vec>
     -outputs: U, s (sizes assigned externally)
  */
  void svdVecs(vector<vec>& vecs, mat& U, vec& s);


  /* Compute the product of the Ubase transpose and one fiber restricted to 2 components
  */
  vec Ut_dot_Fib(mat&, vec&);


  /* Compute the product of the Ubase transpose and one fiber restricted to 2 components
      Low memory version
  */
  vec lowMem_Ut_dot_Fib(mat&, vector<double>&);



/* Compute the nearest neighbor of a fiber given a set of fibers
  - inputs: the set of fibers, the list of remaining fibers and the id of the fiber we look the neighbor of
  - outputs: the id of the nearest neighbor
*/
unsigned int FindNeighbor(vector<vec>&, vector<unsigned int>&, unsigned int);



/* Compute the nearest neighbor of a fiber given a set of fibers. Low in memory version.
  - inputs: the set of fibers, the list of remaining fibers and the id of the fiber we look the neighbor of
  - outputs: the id of the nearest neighbor. The position in the list of the id. And the distance.
*/
unsigned int lowMem_FindNeighbor(vector<vector<double>*>&, vector<unsigned int>&, unsigned int);


/* Compute the nearest neighbor of a fiber given a set of fibers (overloaded)
 - input: The set of fibers, the position of the fiber given
 - output: The position of the neighbor
*/
unsigned int FindNeighbor(vector<vec>&, unsigned int);




/* Compute the nearest neighbor of a fiber given a set of fibers. Low memory version
 - input: The set of fibers, the position of the fiber given
 - output: The position of the neighbor
*/

unsigned int lowMem_FindNeighbor(vector<vector<double>*>&, unsigned int);



/* Given two fibers, compute the 2d basis
  - inputs: the two fibers
  - outputs: the basis
*/
vector <vec> Compute2dBasis(vec&, vec&);


/* Compute the 2D-bases given a set of fibers
- inputs: the set of fibers, the tolerance eps
- outputs: the vector of Matrix basis, the vector of the indices of the fibers on each basis, the error budget per basis.
*/
void ComputeBases(vector<vec>&, double, vector<mat>&, vector<vector<unsigned int>>&, vector<double>&);



/* Compute the 2D-bases given a set of fibers. Low memory version.
  - inputs: the set of fibers, the tolerance eps
  - outputs: the vector of Matrix basis, the vector of the indices of the fibers on each basis, the error budget per basis.
*/
void lowMem_ComputeBases(vector<vector<double>*>&, double, vector<mat>&, vector<vector<unsigned int>>&, vector<double>&, double&);




/* Merge the 2d clusters computed in Compute2dBasis
- inputs: the set of fibers, the vector of the indices of the fibers on each basis, the vector of Matrix basis, the error budget per basis.
- outputs: Rewritting of the vector of the indices of the fibers on each new cluster, the vector of matrix basis of each cluster (2d or more).
*/
void Merge2dBases(vector<vec>&, vector<mat>&, vector<vector<unsigned int>>&, vector<double>&);



/* Merge the 2d clusters computed in Compute2dBasis. Low memory version.
- inputs: the set of fibers, the vector of the indices of the fibers on each basis, the vector of Matrix basis, the error budget per basis.
- outputs: Rewritting of the vector of the indices of the fibers on each new cluster, the vector of matrix basis of each cluster (2d or more).
*/
void lowMem_Merge2dBases(string, vector<vector<double>*>&, vector<mat>&, vector<vector<unsigned int>>&, vector<double>&, double&, vector<double>&);

};


#endif
