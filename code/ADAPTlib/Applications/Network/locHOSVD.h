//Header file for a network format
#ifndef locHOSVD_h
#define locHOSVD_h

// Including headers:
#include "../../genericInclude.h"
#include "../../linAlg/linAlg.h"
#include "../../tensor.h"
#include "../../CP/CPTensor.h"
#include "../../fullTensor/fullTens.h"



// -- FUNCTIONS: --

/*Find which basis well represents the given fiber
  -Inputs: The set of fibers, the set of basis, the index of the fibers well represented by each basis, fiber (in the grid).
  -Output: The matrix of the base, the index of the base.
*/
unsigned int findBasis(vector<mat>& Basis, vector<vector<unsigned int>>& indOnBasis, unsigned int fib);



/* Compute the HOSVD core in 2d
  -Inputs: The set of fibers before the HOSVD decomposition, the set of basis.
  -Output: The core matrix.
*/
mat computeHOSVDcore(vector<vec>& fibers, vector<mat>& Basis);


/* Compute the HOSVD core in 2d LowMem version.
  -Inputs: The set of fibers before the HOSVD decomposition, the set of basis.
  -Output: The core matrix.
*/
mat lowMem_computeHOSVDcore(vector<vector<double>*>& fibers, vector<mat>& Basis);


/* Compute the HOSVD core in 2d LowMem version.
  -Inputs: The set of fibers before the HOSVD decomposition, the set of basis.
  -Output: The core matrix.
*/
mat lowMem_computeHOSVDMatcore(vector<vector<double>*>& fibers, vector<mat>& Basis, vector<vector<unsigned int>>& indsOnBasis);


unsigned int estimate_basisRank(vector<vec>& fibers, double tolPerUnf);



/* Compute the HOSVD core in more dimensions
  -Inputs: The set of fibers before the HOSVD decomposition, the set of basis.
  -Output: The core matrix.
*/
fullTensor computeHOSVDtensorCore(vector<vec>& fibers, vector<mat>& Basis, vector<vector<unsigned int>>& indsOnBasis);


/* Compute the HOSVD core in more dimensions LOWMEM
  -Inputs: The set of fibers before the HOSVD decomposition, the set of basis.
  -Output: The core matrix.
*/
fullTensor computeHOSVDtensorCore(vector<vector<double>*>& fibers, vector<mat>& Basis, vector<vector<unsigned int>>& indsOnBasis);



/* Evaluation of the characteristic functions:
 -Input: the inds on basis per variable, the indices
 -Output: A double that is one or zero */

double evalCharacteristics(vector<unsigned int>& indices, vector<vector<unsigned int>>& indsOnBasis, vector<mat>& Basis);





/* Reconstruction of a function in a certain point in 2d with the HOSVD method.
  -Input: The core matrix of the HOSVD, the set of basis, the point in which we want to reconstruct.
  -Output: The value of the function in that certain point.
*/
double reconstructionHOSVD(mat& A, vector<mat>& Basis, vector<unsigned int>& indGrid);



/* Compute the truncated base of the HOSVD for 2d.
  -Inputs: The set of fibers, the error squared.
  -Output: The matrix of the truncated basis.
*/
mat computeHOSVDbasis(vector<vec>& fibers, double tolPerUnf);



/* Compute the truncated base of the HOSVD for 2d. LowMem version.
  -Inputs: The set of fibers, the error squared.
  -Output: The matrix of the truncated basis.
*/
mat lowMem_computeHOSVDbasis(vector<vector<double>*>& fibers, double tolPerUnf);


/* Compute the local core of the local HOSVD for X (2d for x and t).
  -Inputs: The set of fibers, the vector of indices well represented by a certain basis obtained in Network for x, the basis in X, the basis in T.
  -Output: The local core matrix.
*/
mat localCoreX(vector<vec>& fibers, vector<unsigned int>& jth_indOnBasis_t, mat& basisX, mat& basisT);



/* Compute the local core of the local HOSVD for T (2d for x and t).
  -Inputs: The set of fibers, the vector of indices well represented by a certain basis obtained in Network for t, the basis in X, the basis in T.
  -Output: The local core matrix.
*/
mat localCoreT(vector<vec>& fibers, vector<unsigned int>& ith_indOnBasis, mat& basisX, mat& basisT);



/* Compute the local set of cores of the local HOSVD for X (2d for x and t).
  -Inputs: The set of fibers, the vector of indices well represented by any basis obtained in Network for x, the vector of basis in X, the vector of basis in T.
  -Output: The set of local cores matrices.
*/
vector<vector<mat>> vectorCoreX(vector<vec>& fibers, vector<vector<unsigned int>>& indOnBasis_t, vector<mat>& basisX, vector<mat>& basisT);



/* Compute the local set of cores of the local HOSVD for T (2d for x and t).
  -Inputs: The set of fibers, the vector of indices well represented by any basis obtained in Network for t, the vector of basis in X, the vector of basis in T.
  -Output: The set of local cores matrices.
*/
vector<vector<mat>> vectorCoreT(vector<vec>& fibers, vector<vector<unsigned int>>& indOnBasis, vector<mat>& basisX, vector<mat>& basisT);



/* Compute the local set of coefficients of the local HOSVD for X (2d for x and t) for a certain point.
  -Inputs: The core for x in a certain point i, the vector of basis in T, the point in t, t_0.
  -Output: The vector of coefficients in x for a certain point in t.
*/
vector<double> coefX(vector<mat>& ith_coresX, vector<mat>& basisT, unsigned int t_0);



/* Compute the local set of coefficients of the local HOSVD for T (2d for x and t) for a certain point.
  -Inputs: The core for t in a certain point j, the vector of basis in X, the point in x, x_0.
  -Output: The vector of coefficients in t for a certain point in x.
*/
vector<double> coefT(vector<mat>& jth_coresT, vector<mat>& basisX, unsigned int x_0);


/* The reconstructed function by local HOSVD in a certain point from the x (2d for x and t).
  -Input: The core for x in a certain point i, the vector of basis in T, the ith basis in X, the points to evaluate x_0 and t_0.
  -Output: The reconstructed value of the function in x_0, t_0;
*/
double recFx(vector<mat>& ith_coresX, vector<mat>& basisT, mat& ith_basisX, unsigned int x_0, unsigned int t_0);



/* The reconstructed function by local HOSVD in a certain point from the t (2d for x and t).
  -Input: The core for t in a certain point j, the vector of basis in X, the ith basis in T, the points to evaluate x_0 and t_0.
  -Output: The reconstructed value of the function in x_0, t_0;
*/
double recFt(vector<mat>& jth_coresT, vector<mat>& basisX, mat& jth_basisT, unsigned int x_0, unsigned int t_0);


#endif
