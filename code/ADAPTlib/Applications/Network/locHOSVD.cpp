// Implementation of the lHOSVD inside Network:

#include "locHOSVD.h"
#include "../../linAlg/linearAlgebraOperations.h"
#include "../../linAlg/svd.h"
#include "../../linAlg/eigenSolver.h"
#include "../../genericInclude.h"
#include "../../fullTensor/fullTens.h"





/*Find which basis well represents the given fiber
  -Inputs: The set of fibers, the set of basis, the index of the fibers well represented by each basis, fiber (in the grid).
  -Output: The matrix of the base, the index of the base.
*/
unsigned int findBasis(vector<mat>& Basis, vector<vector<unsigned int>>& indOnBasis, unsigned int fib){
  unsigned int iBasisFound=0;

  for (unsigned int iBase = 0; iBase < indOnBasis.size(); iBase++) {
    for (unsigned int iFib = 0; iFib < indOnBasis[iBase].size(); iFib++) {
      if (fib == indOnBasis[iBase][iFib]) {
        //cout << "the fiber: " << fib << endl;
        //cout << "the index: " << indOnBasis[iBase][iFib] << endl;

        iBasisFound = iBase;
        return iBasisFound;

        break;
      }
    }
  }
  return iBasisFound;
}



/* VALID 2d! Computes de local HOSVD given a certain set of fibers and the basis that represent well the local domain
  -Inputs: The set of fibers(fibers with t in the rows and x in the cols), the index of the fibers well represented by each basis,
   the index of the Basis, a vector of the basis that represent well the fiber (1st the basis in x, 2nd in t).
  -Output: The reconstructed function by points.
*/
/*
mat lHOSVD(vector<vec>& fibers, vector<vector<unsigned int>>& indOnBasis, vector<vector<unsigned int>>& indOnBasis_t, vector<unsigned int>& indBasis, vector<mat>& Basis){
  //The fibers well represented by the basis in X
  unsigned int iBase = indBasis[0];
  vector<unsigned int> theXIndices(indOnBasis[iBase].size());
  for (unsigned int iInd = 0; iInd < indOnBasis[iBase].size(); iInd++) {
    theXIndices[iInd] = indOnBasis[iBase][iInd];
    //cout << "The x indices: " << theXIndices[iInd] << endl;

  }
  //cout << "the indices of x well computed" <<endl;


  //The fibers well represented by the basis in T
  unsigned int jBase = indBasis[1];
  vector<unsigned int> theTIndices(indOnBasis_t[jBase].size());
  for (unsigned int iInd = 0; iInd < indOnBasis_t[jBase].size(); iInd++) {
    theTIndices[iInd] = indOnBasis_t[jBase][iInd];
    //cout << "The t indices: " << theTIndices[iInd] << endl;
  }

  //The matrix A of coefficients:
  vector<unsigned int> mBase(Basis.size());
  for (unsigned int iBase = 0; iBase < Basis.size(); iBase++) {
    mBase[iBase] = Basis[iBase].nCols();
  }


  mat A(mBase[0],mBase[1], PETSC_COMM_WORLD);
  for (unsigned int l = 0; l < mBase[0]; l++) {
    for (unsigned int m = 0; m < mBase[1]; m++) {
      double sum = 0.0;
      for (unsigned int p = 0; p < fibers[0].size(); p++) {
        for (unsigned int q = 0; q < fibers.size(); q++) {
            //double bx = Basis[0](theXIndices[p],l);
            //double bt = Basis[1](theTIndices[q],m);
            //double theF = fibers[theTIndices[q]](theXIndices[p]);
            double bx = Basis[0](p,l);
            double bt = Basis[1](q,m);
            double theF = fibers[q](p);
            sum += theF * bt * bx;
        }
      }
      A.setMatEl(l,m,sum);
    }
  }
  A.finalize();
  return A;
 }
 */


/* Reconstruction of the function for a certain fiber
   -Input: the matrix of the encoding,  a vector of the basis that represent well the fiber (1st the basis in x, 2nd in t), the index of the fiber (in the grid).
   -Output: the value of the function in the given points
*/
/*
 double reconstructF(mat& A, vector<mat>& Basis, vector<unsigned int>& indFibers){

   double recF = 0.0;

   for (unsigned int l = 0; l < Basis[0].nCols(); l++) {
     double bx = Basis[0](indFibers[0],l);
     for (unsigned int m = 0; m < Basis[1].nCols(); m++) {
       double bt = Basis[1](indFibers[1],m);
       recF += A(l,m) * bx * bt;
     }
   }
   return recF;
}
*/

//HOSVD
/* Compute the HOSVD core in 2d
  -Inputs: The set of fibers before the HOSVD decomposition, the set of basis.
  -Output: The core matrix.
*/
mat computeHOSVDcore(vector<vec>& fibers, vector<mat>& Basis){

  //The matrix A of coefficients:
  vector<unsigned int> mBase(Basis.size());
  for (unsigned int iBase = 0; iBase < Basis.size(); iBase++) {
    mBase[iBase] = Basis[iBase].nCols();
  }

  cout << "the matrix 0 of size " << Basis[0].nRows() <<" x "<< Basis[0].nCols() << endl;
  cout<< Basis[0].getMatEl(0,0) << endl;

  cout << "the matrix 1 of size " << Basis[1].nRows() <<" x "<< Basis[1].nCols() << endl;
  cout << "the fibers size " << fibers.size() <<" x "<< fibers[0].size() << endl;


  mat A(mBase[0],mBase[1], PETSC_COMM_WORLD);
  cout << "the matrix of size " << mBase[0] << " x " << mBase[1] << endl;
  for (unsigned int l = 0; l < mBase[0]; l++) {
    for (unsigned int m = 0; m < mBase[1]; m++) {
      double sum = 0.0;
      for (unsigned int p = 0; p < fibers[0].size(); p++) {
        for (unsigned int q = 0; q < fibers.size(); q++) {
            double bx = Basis[0].getMatEl(p,l);
            double bt = Basis[1](q,m);
            double theF = fibers[q](p);

            sum += theF * bt * bx;

        }
      }
      A.setMatEl(l,m,sum);
    }
  }
  A.finalize();
  cout << "Matrix A computed" << endl;
  return A;
}

/* Compute the HOSVD core in 2d LowMem version.
  -Inputs: The set of fibers before the HOSVD decomposition, the set of basis.
  -Output: The core matrix.
*/
mat lowMem_computeHOSVDcore(vector<vector<double>*>& fibers, vector<mat>& Basis){

  //The matrix A of coefficients:
  vector<unsigned int> mBase(Basis.size());
  for (unsigned int iBase = 0; iBase < Basis.size(); iBase++) {
    mBase[iBase] = Basis[iBase].nCols();
  }

  cout << "the matrix 0 of size " << Basis[0].nRows() <<" x "<< Basis[0].nCols() << endl;
  cout<< Basis[0].getMatEl(0,0) << endl;

  cout << "the matrix 1 of size " << Basis[1].nRows() <<" x "<< Basis[1].nCols() << endl;
  cout << "the fibers size " << fibers.size() <<" x "<< fibers[0]->size() << endl;


  mat A(mBase[0],mBase[1], PETSC_COMM_WORLD);
  cout << "the matrix of size " << mBase[0] << " x " << mBase[1] << endl;
  for (unsigned int l = 0; l < mBase[0]; l++) {
    for (unsigned int m = 0; m < mBase[1]; m++) {
      double sum = 0.0;
      for (unsigned int p = 0; p < fibers[0]->size(); p++) {
        for (unsigned int q = 0; q < fibers.size(); q++) {
            double bx = Basis[0].getMatEl(p,l);
            double bt = Basis[1](q,m);
            double theF = (*fibers[q])[p];

            sum += theF * bt * bx;

        }
      }
      A.setMatEl(l,m,sum);
    }
  }
  A.finalize();
  cout << "Matrix A computed" << endl;
  return A;
}


/* Compute the HOSVD core in 2d LowMem version.
  -Inputs: The set of fibers before the HOSVD decomposition, the set of basis.
  -Output: The core matrix.
*/
mat lowMem_computeHOSVDMatcore(vector<vector<double>*>& fibers, vector<mat>& Basis, vector<vector<unsigned int>>& indsOnBasis){

  //The matrix A of coefficients:
  vector<unsigned int> mBase(Basis.size());
  for (unsigned int iBase = 0; iBase < Basis.size(); iBase++) {
    mBase[iBase] = Basis[iBase].nCols();
  }

  mat A(mBase[0], mBase[1], PETSC_COMM_WORLD);
  for (unsigned int l = 0; l < mBase[0]; l++) {
    for (unsigned int m = 0; m < mBase[1]; m++) {
      double sum = 0.0;
      for (unsigned int iX = 0; iX < Basis[0].nRows(); iX++) {
        for (unsigned int iT = 0; iT < Basis[1].nRows(); iT++) {
            double bx = Basis[0].getMatEl(iX,l);
            double bt = Basis[1].getMatEl(iT,m);
            double theF = (*fibers[iT])[iX];
            sum += theF * bt * bx;
        }
      }

      //Eval characteristics
      double yesOrNo = 0.0;
      double yesOrNo1;
      double yesOrNo2;

      for (size_t iInd = 0; iInd < indsOnBasis[0].size(); iInd++) {
        if (m == indsOnBasis[0][iInd]) {
          yesOrNo1 = 1.0;
          break;
        } else{yesOrNo1 = 0.0;}
      }
      for (size_t jInd = 0; jInd < indsOnBasis[1].size(); jInd++) {
        if (l == indsOnBasis[1][jInd]) {
          yesOrNo2 = 1.0;
          break;
        } else{yesOrNo2 = 0.0;}
      }
      yesOrNo = yesOrNo1 * yesOrNo2;
      sum *= yesOrNo;

      A.setMatEl(l,m,sum);
    }
  }
  A.finalize();
  //cout << "Matrix A computed" << endl;
  return A;
}



/* Compute the HOSVD core in 3 dimensions
  -Inputs: The set of fibers before the HOSVD decomposition from x unfolding, the set of basis.
  -Output: The core tensor.
*/
fullTensor computeHOSVDtensorCore(vector<vec>& fibers, vector<mat>& Basis, vector<vector<unsigned int>>& indsOnBasis){

  vector<unsigned int> mBase(Basis.size());
  for (unsigned int iBase = 0; iBase < Basis.size(); iBase++) {
    mBase[iBase] = Basis[iBase].nCols();
  }

  fullTensor T(mBase.size(),mBase, PETSC_COMM_WORLD);
  cout << "the tensor of size " << mBase[0] << " x " << mBase[1] << "x" << mBase[2] << endl;
  for (unsigned int i0 = 0; i0 < mBase[0]; i0++) {
    for (unsigned int i1 = 0; i1 < mBase[1]; i1++) {
      for (unsigned int i2 = 0; i2 < mBase[2]; i2++) {

        double sum = 0.0;
        for (unsigned int iX = 0; iX < Basis[0].nRows(); iX++) {
          for (unsigned int iT = 0; iT < Basis[1].nRows(); iT++) {
            for (unsigned int iTh = 0; iTh < Basis[2].nRows(); iTh++) {
              double bx = Basis[0].getMatEl(iX,i0);
              double bt = Basis[1].getMatEl(iT,i1);
              double btheta = Basis[2].getMatEl(iTh,i2);
              unsigned int iTt = iT + Basis[1].nRows() * iTh;
              double theF = fibers[iTt](iX);

              sum += theF * bt * bx * btheta;
            }
          }
        }
        vector<unsigned int> indices(3);
        indices[0] = i0;
        indices[1] = i1;
        indices[2] = i2;

        double yesOrNo = evalCharacteristics(indices, indsOnBasis, Basis);
        sum *= yesOrNo;

        T.set_tensorElement(indices,sum);
        indices.clear();
      }
    }
  }
  T.finalize();
  cout << "Core tensor T computed" << endl;
  return T;
}



/* Compute the HOSVD core in more dimensions LOWMEM
  -Inputs: The set of fibers before the HOSVD decomposition from x unfolding, the set of basis.
  -Output: The core tensor.
*/
fullTensor computeHOSVDtensorCore(vector<vector<double>*>& fibers, vector<mat>& Basis, vector<vector<unsigned int>>& indsOnBasis){

  vector<unsigned int> mBase(Basis.size());
  for (unsigned int iBase = 0; iBase < Basis.size(); iBase++) {
    mBase[iBase] = Basis[iBase].nCols();
  }

  fullTensor T(mBase.size(),mBase, PETSC_COMM_WORLD);
  cout << "the tensor of size " << mBase[0] << " x " << mBase[1] << "x" << mBase[2] << endl;
  for (unsigned int i0 = 0; i0 < mBase[0]; i0++) {
    for (unsigned int i1 = 0; i1 < mBase[1]; i1++) {
      for (unsigned int i2 = 0; i2 < mBase[2]; i2++) {

        double sum = 0.0;
        for (unsigned int iX = 0; iX < Basis[0].nRows(); iX++) {
          for (unsigned int iT = 0; iT < Basis[1].nRows(); iT++) {
            for (unsigned int iTh = 0; iTh < Basis[2].nRows(); iTh++) {
              double bx = Basis[0].getMatEl(iX,i0);
              double bt = Basis[1].getMatEl(iT,i1);
              double btheta = Basis[2].getMatEl(iTh,i2);
              unsigned int iTt = iT + Basis[1].nRows() * iTh;
              double theF = (*fibers[iTt])[iX];

              sum += theF * bt * bx * btheta;
            }
          }
        }
        vector<unsigned int> indices(3);
        indices[0] = i0;
        indices[1] = i1;
        indices[2] = i2;

        double yesOrNo = evalCharacteristics(indices, indsOnBasis, Basis);
        sum *= yesOrNo;

        T.set_tensorElement(indices,sum);
        indices.clear();
      }
    }
  }
  T.finalize();
  cout << "Core tensor T computed" << endl;
  return T;
}

/* Evaluation of the characteristic functions: ordered, X, T, THETA
 -Input: the inds on basis per variable, the indices
 -Output: A double that is one or zero */

double evalCharacteristics(vector<unsigned int>& indices, vector<vector<unsigned int>>& indsOnBasis, vector<mat>& Basis){
  double yesOrNo = 0.0;
  double yesOrNo1;
  double yesOrNo2;
  double yesOrNo3;

  unsigned int iTt = indices[1] + Basis[1].nRows() * indices[2];
  unsigned int iTx = indices[0] + Basis[0].nRows() * indices[1];
  unsigned int ixTh = indices[0] + Basis[0].nRows() * indices[2];

  for (size_t iInd = 0; iInd < indsOnBasis[0].size(); iInd++) {
      if (iTt == indsOnBasis[0][iInd]) {
        yesOrNo1 = 1.0;
        break;
      } else{yesOrNo1 = 0.0;}
  }
  for (size_t jInd = 0; jInd < indsOnBasis[1].size(); jInd++) {
      if (ixTh == indsOnBasis[1][jInd]) {
        yesOrNo2 = 1.0;
        break;
      } else{yesOrNo2 = 0.0;}
  }
  for (size_t kInd = 0; kInd < indsOnBasis[2].size(); kInd++) {
      if (iTx == indsOnBasis[2][kInd]) {
        yesOrNo3 = 1.0;
        break;
      }else{yesOrNo3 = 0.0;}
  }

  yesOrNo = yesOrNo1 * yesOrNo2 * yesOrNo3;
  return yesOrNo;
}




/* Reconstruction of a function in a certain point in 2d with the HOSVD method.
  -Input: The core matrix of the HOSVD, the set of basis, the point in which we want to reconstruct.
  -Output: The value of the function in that certain point.
*/
double reconstructionHOSVD(mat& A, vector<mat>& Basis, vector<unsigned int>& indGrid){
  //Reconstruction

  vector<unsigned int> mBase(Basis.size());
  for (unsigned int iBase = 0; iBase < Basis.size(); iBase++) {
    mBase[iBase] = Basis[iBase].nCols();
  }

  double recF = 0.0;

  for (unsigned int l = 0; l < mBase[0]; l++) {
    for (unsigned int m = 0; m < mBase[1]; m++) {
      double bx = Basis[0](indGrid[0],l);
      double bt = Basis[1](indGrid[1],m);
      recF += A(l,m) * bx * bt;
    }
  }
  return recF;
}



/* Compute the truncated base of the HOSVD for 2d.
  -Inputs: The set of fibers, the error squared.
  -Output: The matrix of the truncated basis.
*/
mat computeHOSVDbasis(vector<vec>& fibers, double tolPerUnf){
  mat C_0(fibers.size(), fibers.size(), fibers[0].comm());
  for (unsigned int iFiber = 0; iFiber < fibers.size(); iFiber++) {
    vec iVec = fibers[iFiber];
    for (unsigned int jFiber = 0; jFiber <= iFiber; jFiber++) {
      vec jVec = fibers[jFiber];
      double value = scalProd(iVec, jVec);
      if (fabs(value) > DBL_EPSILON) {
        C_0.setMatEl(iFiber, jFiber, value);
        if (iFiber != jFiber) {
          C_0.setMatEl(jFiber, iFiber, value);
        }
      }
    }
  }
  C_0.finalize();
  mat I = eye(C_0.nRows(),C_0.comm());
  I *= DBL_EPSILON;
  C_0 += I;
  //C_0 +=DBL_EPSILON*eye(C_0.nRows(), C_0.comm());

  // solve the eigenvalue problem for 20:
  eigenSolver eps_0;
  eps_0.init(C_0, EPS_HEP, C_0.nRows(), 1.0e-12);
  eps_0.solveHermEPS(C_0);
  vector<vec> vv_0 = eps_0.eigenVecs();
  vector<double> lambda_0 = eps_0.eigenVals();

  unsigned int n_eig_0 = lambda_0.size();

  if (n_eig_0 ==0){
    C_0.print();
  }
  double sum_0 = 0.0;
  // truncate:
  //cout << "Rank = " << n_eig_0 << endl;
  unsigned int cc_0 = 0;
  while ( (sum_0 < tolPerUnf) & (cc_0<n_eig_0) ){
    sum_0 += lambda_0[n_eig_0-1-cc_0];
    cc_0 += 1;
  }
  n_eig_0 = n_eig_0 + 1 - cc_0;
  //cout << "New rank 1 = " << n_eig_0 << endl;


  mat U_0(fibers[0].size(), n_eig_0, C_0.comm());

  for(unsigned int iVec=0; iVec<n_eig_0; iVec++){
    for(unsigned int iRow=0; iRow<fibers[0].size(); iRow++){
      double dofVal = 0.0;
       for(unsigned int iTerm=0; iTerm<fibers.size(); iTerm++){
         dofVal += vv_0[iVec].getVecEl(iTerm) * fibers[iTerm](iRow);
       }
       if(fabs(lambda_0[iVec]>1.0e-12)){
         dofVal = dofVal/sqrt(fabs(lambda_0[iVec]));
       }
       else{
         dofVal = 0.0;
       }
       U_0.setMatEl(iRow, iVec, dofVal);
    }
  }
  U_0.finalize();
  C_0.clear();
  eps_0.clear();
  //Clean the memory!!
  return U_0;
}

/* Estimate the rank of the U matrix of an SVD without computing it*/
unsigned int estimate_basisRank(vector<vec>& fibers, double tolPerUnf){
  const unsigned int nfibers = fibers.size();
  const unsigned int nDofs = fibers[0].size();

  if (nfibers <= nDofs) {
    mat C_0(fibers.size(), fibers.size(), fibers[0].comm());
    for (unsigned int iFiber = 0; iFiber < fibers.size(); iFiber++) {
      vec iVec = fibers[iFiber];
      for (unsigned int jFiber = 0; jFiber <= iFiber; jFiber++) {
        vec jVec = fibers[jFiber];
        double value = scalProd(iVec, jVec);
        if (fabs(value) > DBL_EPSILON) {
          C_0.setMatEl(iFiber, jFiber, value);
          if (iFiber != jFiber) {
            C_0.setMatEl(jFiber, iFiber, value);
          }
        }
      }
    }
    C_0.finalize();
    mat I = eye(C_0.nRows(),C_0.comm());
    I *= DBL_EPSILON;
    C_0 += I;
    I.clear();
    //C_0 +=DBL_EPSILON*eye(C_0.nRows(), C_0.comm());

    // solve the eigenvalue problem for 20:
    eigenSolver eps_0;
    eps_0.init(C_0, EPS_HEP, C_0.nRows(), 1.0e-12);
    eps_0.solveHermEPS(C_0);
    vector<vec> vv_0 = eps_0.eigenVecs();
    vector<double> lambda_0 = eps_0.eigenVals();

    unsigned int n_eig_0 = lambda_0.size();

    if (n_eig_0 ==0){
      //C_0.print();
    }
    double sum_0 = 0.0;
    // truncate:
    //cout << "Rank = " << n_eig_0 << endl;
    unsigned int cc_0 = 0;
    while ( (sum_0 < tolPerUnf) & (cc_0<n_eig_0) ){
      sum_0 += lambda_0[n_eig_0-1-cc_0];
      cc_0 += 1;
    }
    n_eig_0 = n_eig_0 + 1 - cc_0;
    eps_0.clear();
    C_0.clear();
    return n_eig_0;
  } else{
    mat C_0(nDofs, nDofs, fibers[0].comm());
    for (size_t iDof = 0; iDof < nDofs; iDof++) {
      for (size_t jDof = 0; jDof <= iDof; jDof++) {
        double entry = 0.0;
        for (size_t k = 0; k < nfibers; k++) {
          double value1 = fibers[k].getVecEl(iDof);
          double value2 = fibers[k].getVecEl(jDof);
          entry += value1 * value2;
        }
        if (fabs(entry) > DBL_EPSILON) {
          C_0.setMatEl(iDof, jDof, entry);
          if (iDof != jDof) {
            C_0.setMatEl(jDof, iDof, entry);
          }
        }
      }
    }
    C_0.finalize();
    mat I = eye(C_0.nRows(),C_0.comm());
    I *= DBL_EPSILON;
    C_0 += I;
    I.clear();
    //C_0 +=DBL_EPSILON*eye(C_0.nRows(), C_0.comm());

    // solve the eigenvalue problem for 20:
    eigenSolver eps_0;
    eps_0.init(C_0, EPS_HEP, C_0.nRows(), 1.0e-12);
    eps_0.solveHermEPS(C_0);
    vector<vec> vv_0 = eps_0.eigenVecs();
    vector<double> lambda_0 = eps_0.eigenVals();

    unsigned int n_eig_0 = lambda_0.size();

    if (n_eig_0 ==0){
      C_0.print();
    }
    double sum_0 = 0.0;
    // truncate:
    //cout << "Rank = " << n_eig_0 << endl;
    unsigned int cc_0 = 0;
    while ( (sum_0 < tolPerUnf) & (cc_0<n_eig_0) ){
      sum_0 += lambda_0[n_eig_0-1-cc_0];
      cc_0 += 1;
    }
    n_eig_0 = n_eig_0 + 1 - cc_0;
    eps_0.clear();
    C_0.clear();
    return n_eig_0;
  }
}



/* Compute the truncated base of the HOSVD for 2d. LowMem version.
  -Inputs: The set of fibers, the error squared.
  -Output: The matrix of the truncated basis.
*/
mat lowMem_computeHOSVDbasis(vector<vector<double>*>& fibers, double tolPerUnf){
  mat C_0(fibers.size(), fibers.size(), PETSC_COMM_WORLD);
  for (unsigned int iFiber = 0; iFiber < fibers.size(); iFiber++) {
    //vec iVec = fibers[iFiber];
    for (unsigned int jFiber = 0; jFiber <= iFiber; jFiber++) {
      //vec jVec = fibers[jFiber];
      double value = 0.0;
      for (size_t iEl = 0; iEl < fibers[0]->size(); iEl++) {
        value += (*fibers[iFiber])[iEl] * (*fibers[jFiber])[iEl];
      }
      //double value = scalProd(iVec, jVec);
      C_0.setMatEl(iFiber, jFiber, value);
      if (iFiber != jFiber) {
        C_0.setMatEl(jFiber, iFiber, value);
      }

    }
  }
  C_0.finalize();
  C_0 +=DBL_EPSILON*eye(C_0.nRows(), C_0.comm());

  // solve the eigenvalue problem for 20:
  eigenSolver eps_0;
  eps_0.init(C_0, EPS_HEP, C_0.nRows(), 1.0e-12);
  eps_0.solveHermEPS(C_0);
  vector<vec> vv_0 = eps_0.eigenVecs();
  vector<double> lambda_0 = eps_0.eigenVals();

  unsigned int n_eig_0 = lambda_0.size();
  double sum_0 = 0.0;
  // truncate:
  cout << "Rank = " << n_eig_0 << endl;
  unsigned int cc_0 = 0;
  while ( (sum_0 < tolPerUnf) & (cc_0<n_eig_0) ){
    sum_0 += lambda_0[n_eig_0-1-cc_0];
    cc_0 += 1;
  }
  n_eig_0 = n_eig_0 + 1 - cc_0;
  cout << "New rank 1 = " << n_eig_0 << endl;


  mat U_0(fibers[0]->size(), n_eig_0, C_0.comm());

  for(unsigned int iVec=0; iVec<n_eig_0; iVec++){
    for(unsigned int iRow=0; iRow<fibers[0]->size(); iRow++){
      double dofVal = 0.0;
       for(unsigned int iTerm=0; iTerm<fibers.size(); iTerm++){
         dofVal += vv_0[iVec].getVecEl(iTerm) * (*fibers[iTerm])[iRow];
       }
       if(fabs(lambda_0[iVec]>1.0e-12)){
         dofVal = dofVal/sqrt(fabs(lambda_0[iVec]));
       }
       else{
         dofVal = 0.0;
       }
       U_0.setMatEl(iRow, iVec, dofVal);
    }
  }
  U_0.finalize();
  C_0.clear();
  eps_0.clear();
  //Clean the memory!!
  return U_0;
}




//local lHOSVD

/* Compute the local core of the local HOSVD for X (2d for x and t).
  -Inputs: The set of fibers, the vector of indices well represented by a certain basis obtained in Network for x, the basis in X, the basis in T.
  -Output: The local core matrix.
*/
 mat localCoreX(vector<vec>& fibers, vector<unsigned int>& jth_indOnBasis_t, mat& basisX, mat& basisT){

  mat A(basisX.nCols(), basisT.nCols(), PETSC_COMM_WORLD);
  for (unsigned int iRow = 0; iRow < basisX.nCols(); iRow++) {
    for (unsigned int iCol = 0; iCol < basisT.nCols(); iCol++) {
      double sum = 0.0;
      for (unsigned int p = 0; p < jth_indOnBasis_t.size(); p++) {
        unsigned int dof_p = jth_indOnBasis_t[p];
        for (unsigned int q = 0; q < fibers.size(); q++) {
          double bx = basisX(dof_p,iRow);
          double bt = basisT(q, iCol);
          double F = fibers[q](dof_p);
          sum += F * bx * bt;
        }
      }
      A(iRow, iCol) = sum;
    }
  }
  A.finalize();
  return A;
}



/* Compute the local set of cores of the local HOSVD for X (2d for x and t).
  -Inputs: The set of fibers, the vector of indices well represented by any basis obtained in Network for x, the vector of basis in X, the vector of basis in T.
  -Output: The set of local cores matrices.
*/
vector<vector<mat>> vectorCoreX(vector<vec>& fibers, vector<vector<unsigned int>>& indOnBasis_t, vector<mat>& basisX, vector<mat>& basisT){
  vector<vector<mat>> coresX(basisX.size());

  for (unsigned int i = 0; i < basisX.size(); i++) {
    coresX[i].resize(basisT.size());
    for (unsigned int j = 0; j < basisT.size(); j++) {
      coresX[i][j] = localCoreX(fibers, indOnBasis_t[j], basisX[i], basisT[j]);
    }
  }
  return coresX;
}



/* Compute the local core of the local HOSVD for T (2d for x and t).
  -Inputs: The set of fibers, the vector of indices well represented by a certain basis obtained in Network for t, the basis in X, the basis in T.
  -Output: The local core matrix.
*/
mat localCoreT(vector<vec>& fibers, vector<unsigned int>& ith_indOnBasis, mat& basisX, mat& basisT){

 mat A(basisT.nCols(), basisX.nCols(), PETSC_COMM_WORLD);
 for (unsigned int iRow = 0; iRow < basisT.nCols(); iRow++) {
   for (unsigned int iCol = 0; iCol < basisX.nCols(); iCol++) {
     double sum = 0.0;
     for (unsigned int q = 0; q < ith_indOnBasis.size(); q++) {
       unsigned int dof_q = ith_indOnBasis[q];
       for (unsigned int p = 0; p < fibers.size(); p++) {
         double bx = basisX(p,iCol);
         double bt = basisT(dof_q,iRow);
         double F = fibers[p](dof_q);
         sum += F * bx * bt;
       }
     }
     A(iRow, iCol) = sum;
   }
 }
 A.finalize();
 return A;
}



/* Compute the local set of cores of the local HOSVD for T (2d for x and t).
  -Inputs: The set of fibers, the vector of indices well represented by any basis obtained in Network for t, the vector of basis in X, the vector of basis in T.
  -Output: The set of local cores matrices.
*/
vector<vector<mat>> vectorCoreT(vector<vec>& fibers, vector<vector<unsigned int>>& indOnBasis, vector<mat>& basisX, vector<mat>& basisT){
  vector<vector<mat>> coresT(basisT.size());

  for (unsigned int j = 0; j < basisT.size(); j++) {
    coresT[j].resize(basisX.size());
    for (unsigned int i = 0; i < basisX.size(); i++) {
      coresT[j][i] = localCoreT(fibers, indOnBasis[i], basisX[i], basisT[j]);
    }
  }
  return coresT;
}



/* Compute the local set of coefficients of the local HOSVD for X (2d for x and t) for a certain point.
  -Inputs: The core for x in a certain point i, the vector of basis in T, the point in t, t_0.
  -Output: The vector of coefficients in x for a certain point in t.
*/
vector<double> coefX(vector<mat>& ith_coresX, vector<mat>& basisT, unsigned int t_0){
  vector<double> alpha (ith_coresX[0].nRows());
  for (unsigned int l = 0; l < ith_coresX[0].nRows(); l++) {
    double sum = 0.0;
    for (unsigned int j = 0; j < basisT.size(); j++) {
      for (unsigned int m = 0; m < basisT[j].nCols(); m++) {
        double core_val = ith_coresX[j](l,m);
        double bt = basisT[j](t_0,m);
        sum += core_val * bt;
      }
    }
    alpha[l] = sum;
  }
  return alpha;
}



/* The reconstructed function by local HOSVD in a certain point from the x (2d for x and t).
  -Input: The core for x in a certain point i, the vector of basis in T, the ith basis in X, the points to evaluate x_0 and t_0.
  -Output: The reconstructed value of the function in x_0, t_0;
*/
double recFx(vector<mat>& ith_coresX, vector<mat>& basisT, mat& ith_basisX, unsigned int x_0, unsigned int t_0){
  double reconstructedF = 0.0;
  vector<double> alpha = coefX(ith_coresX, basisT, t_0);
  for (unsigned int l = 0; l < alpha.size(); l++) {
    double alpha_val = alpha[l];
    double bx = ith_basisX(x_0,l);
    reconstructedF += alpha_val * bx;
  }
  return reconstructedF;
}



/* Compute the local set of coefficients of the local HOSVD for T (2d for x and t) for a certain point.
  -Inputs: The core for t in a certain point j, the vector of basis in X, the point in x, x_0.
  -Output: The vector of coefficients in t for a certain point in x.
*/
vector<double> coefT(vector<mat>& jth_coresT, vector<mat>& basisX, unsigned int x_0){
  vector<double> beta (jth_coresT[0].nRows());
  for (unsigned int m = 0; m < jth_coresT[0].nRows(); m++) {
    double sum = 0.0;
    for (unsigned int i = 0; i < basisX.size(); i++) {
      for (unsigned int l = 0; l < basisX[i].nCols(); l++) {
        double core_val = jth_coresT[i](m,l);
        double bx = basisX[i](x_0,l);
        sum += core_val * bx;
      }
    }
    beta[m] = sum;
  }
  return beta;
}



/* The reconstructed function by local HOSVD in a certain point from the t (2d for x and t).
  -Input: The core for t in a certain point j, the vector of basis in X, the ith basis in T, the points to evaluate x_0 and t_0.
  -Output: The reconstructed value of the function in x_0, t_0;
*/
double recFt(vector<mat>& jth_coresT, vector<mat>& basisX, mat& jth_basisT, unsigned int x_0, unsigned int t_0){
  double reconstructedF = 0.0;
  vector<double> beta = coefT(jth_coresT, basisX, x_0);

  for (unsigned int l = 0; l < beta.size(); l++) {
    double beta_val = beta[l];
    double bt = jth_basisT(t_0,l);
    reconstructedF += beta_val * bt;
  }
  return reconstructedF;
}
