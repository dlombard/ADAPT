// Header file for fftw wrapper

#ifndef fft_h
#define fft_h

#include "fftw3.h"
#include "../../genericInclude.h"
#include "../../linAlg/linAlg.h"


using namespace std;



// 1d fft transform:



/* 1d complex fft
  - input: a complex signal of size N X 2 (real and imag part)
  - output: the complex fft of the input signal
*/
inline void fft_1d(vector<double>& in_real, vector<double>& in_imag, vector<double>& out_real, vector<double>& out_imag){
  int N = in_real.size();
  fftw_complex *in, *out; fftw_plan plan;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in[iDof][0] = in_real[iDof];
    in[iDof][1] = in_imag[iDof];
  }

  plan = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan);

  out_real.resize(N);
  out_imag.resize(N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real[iDof] = out[iDof][0];
    out_imag[iDof] = out[iDof][1];
  }

  // free the memory:
  fftw_destroy_plan(plan); fftw_free(in); fftw_free(out);
}

/* 1d complex fft
  - input: a complex signal of size N X 2 (real and imag part)
  - output: the complex fft of the input signal
*/
inline void fft_1d(vec& in_real, vec& in_imag, vec& out_real, vec& out_imag){
  int N = in_real.size();
  fftw_complex *in, *out; fftw_plan plan;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in[iDof][0] = in_real(iDof);
    in[iDof][1] = in_imag(iDof);
  }

  plan = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan);

  out_real.init(N, in_real.comm());
  out_imag.init(N, in_imag.comm());
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real.setVecEl(iDof, out[iDof][0]);
    out_imag.setVecEl(iDof, out[iDof][1]);
  }
  out_real.finalize();
  out_imag.finalize();

  // free the memory:
  fftw_destroy_plan(plan); fftw_free(in); fftw_free(out);
}


/* 1d complex ifft
  - input: a complex transform of size N X 2 (real and imag part)
  - output: the complex signal of the input transform
*/
inline void ifft_1d(vector<double>& in_real, vector<double>& in_imag, vector<double>& out_real, vector<double>& out_imag){
  int N = in_real.size();
  fftw_complex *in, *out; fftw_plan plan;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in[iDof][0] = in_real[iDof];
    in[iDof][1] = in_imag[iDof];
  }

  plan = fftw_plan_dft_1d(N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan);

  const double renorm = 1.0/N;
  out_real.resize(N);
  out_imag.resize(N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real[iDof] = renorm * out[iDof][0];
    out_imag[iDof] = renorm * out[iDof][1];
  }

  // free the memory:
  fftw_destroy_plan(plan); fftw_free(in); fftw_free(out);
}


/* 1d complex ifft
  - input: a complex transform of size N X 2 (real and imag part)
  - output: the complex signal of the input transform
*/
inline void ifft_1d(vec& in_real, vec& in_imag, vec& out_real, vec& out_imag){
  int N = in_real.size();
  fftw_complex *in, *out; fftw_plan plan;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in[iDof][0] = in_real(iDof);
    in[iDof][1] = in_imag(iDof);
  }

  plan = fftw_plan_dft_1d(N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan);


  const double renorm = 1.0/N;
  out_real.init(N, in_real.comm());
  out_imag.init(N, in_imag.comm());
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real.setVecEl(iDof, out[iDof][0]);
    out_imag.setVecEl(iDof, out[iDof][1]);
  }
  out_real.finalize();
  out_imag.finalize();
  out_real *= renorm;
  out_imag *= renorm;

  // free the memory:
  fftw_destroy_plan(plan); fftw_free(in); fftw_free(out);
}


/* computing 1d convolution:
- input: signal 1 real and imag parts, signal 2 real and imag parts
- output: the real and imaginary parts of the convolution
*/
inline void convolution_1d(vector<double>& in1_real, vector<double>& in1_imag, vector<double>& in2_real, vector<double>& in2_imag, vector<double>& out_real, vector<double>& out_imag){
  int N = in1_real.size();
  assert(in2_real.size()==N);

  // fftw of the first signal:
  fftw_complex *in1, *out1; fftw_plan plan_1;
  in1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in1[iDof][0] = in1_real[iDof];
    in1[iDof][1] = in1_imag[iDof];
  }

  plan_1 = fftw_plan_dft_1d(N, in1, out1, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_1);

  // fftw of the second signal:
  fftw_complex *in2, *out2; fftw_plan plan_2;
  in2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in2[iDof][0] = in2_real[iDof];
    in2[iDof][1] = in2_imag[iDof];
  }

  plan_2 = fftw_plan_dft_1d(N, in2, out2, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_2);

  // compute the fft of the convolution:
  fftw_complex *convFFT, *conv;
  fftw_plan plan_conv;
  convFFT = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  conv = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    convFFT[iDof][0] = out1[iDof][0] * out2[iDof][0] - out1[iDof][1] * out2[iDof][1];
    convFFT[iDof][1] = out1[iDof][0] * out2[iDof][1] + out1[iDof][1] * out2[iDof][0];
  }
  plan_conv = fftw_plan_dft_1d(N, convFFT, conv, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan_conv);

  // copy the output into the vector<double>:
  const double renorm = 1.0/(N*N);
  out_real.resize(N);
  out_imag.resize(N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real[iDof] = renorm * conv[iDof][0];
    out_imag[iDof] = renorm * conv[iDof][1];
  }

  // free the memory:
  fftw_destroy_plan(plan_1); fftw_free(in1); fftw_free(out1);
  fftw_destroy_plan(plan_2); fftw_free(in2); fftw_free(out2);
  fftw_destroy_plan(plan_conv); fftw_free(convFFT); fftw_free(conv);
}


/* computing 1d convolution:
- input: signal 1 real and imag parts, signal 2 real and imag parts
- output: the real and imaginary parts of the convolution
*/
inline void convolution_1d(vec& in1_real, vec& in1_imag, vec& in2_real, vec& in2_imag, vec& out_real, vec& out_imag){
  int N = in1_real.size();
  assert(in2_real.size()==N);

  // fftw of the first signal:
  fftw_complex *in1, *out1; fftw_plan plan_1;
  in1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in1[iDof][0] = in1_real(iDof);
    in1[iDof][1] = in1_imag(iDof);
  }

  plan_1 = fftw_plan_dft_1d(N, in1, out1, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_1);

  // fftw of the second signal:
  fftw_complex *in2, *out2; fftw_plan plan_2;
  in2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in2[iDof][0] = in2_real(iDof);
    in2[iDof][1] = in2_imag(iDof);
  }

  plan_2 = fftw_plan_dft_1d(N, in2, out2, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_2);

  // compute the fft of the convolution:
  fftw_complex *convFFT, *conv;
  fftw_plan plan_conv;
  convFFT = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  conv = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    convFFT[iDof][0] = out1[iDof][0] * out2[iDof][0] - out1[iDof][1] * out2[iDof][1];
    convFFT[iDof][1] = out1[iDof][0] * out2[iDof][1] + out1[iDof][1] * out2[iDof][0];
  }
  plan_conv = fftw_plan_dft_1d(N, convFFT, conv, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan_conv);

  // copy the output into the vector<double>:
  const double renorm = 1.0/(N*N);
  out_real.init(N, in1_real.comm());
  out_imag.init(N, in1_imag.comm());
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real.setVecEl(iDof, renorm * conv[iDof][0]);
    out_imag.setVecEl(iDof, renorm * conv[iDof][1]);
  }
  out_real.finalize();
  out_imag.finalize();


  // free the memory:
  fftw_destroy_plan(plan_1); fftw_free(in1); fftw_free(out1);
  fftw_destroy_plan(plan_2); fftw_free(in2); fftw_free(out2);
  fftw_destroy_plan(plan_conv); fftw_free(convFFT); fftw_free(conv);
}



/* computing 1d convolution:
- input: signal 1 real and imag parts, signal 2 real and imag parts
- output: the real and imaginary parts of the convolution
*/
inline void convolution_1d_schroedinger(vec& in1_real, vec& in1_imag, vec& in2_real, vec& in2_imag, vec& out_real, vec& out_imag){
  int N = in1_real.size();
  assert(in2_real.size()==N);

  // fftw of the first signal:
  fftw_complex *in1, *out1; fftw_plan plan_1;
  in1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in1[iDof][0] = in1_real(iDof);
    in1[iDof][1] = in1_imag(iDof);
  }

  plan_1 = fftw_plan_dft_1d(N, in1, out1, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_1);

  // fftw of the second signal:
  fftw_complex *in2, *out2; fftw_plan plan_2;
  in2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in2[iDof][0] = in2_real(iDof);
    in2[iDof][1] = in2_imag(iDof);
  }

  plan_2 = fftw_plan_dft_1d(N, in2, out2, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_2);

  // compute the fft of the convolution:
  fftw_complex *convFFT, *conv;
  fftw_plan plan_conv;
  convFFT = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  conv = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);


  double pi= 4*atan(1.0);

  for(unsigned int iDof=0; iDof<N; iDof++){
    double a = (out1[iDof][0] * out2[iDof][0] - out1[iDof][1] * out2[iDof][1]);
    double b = (out1[iDof][0] * out2[iDof][1] + out1[iDof][1] * out2[iDof][0]);


    //convFFT[iDof][0] = cos(pi/(4*N))*a + sin(pi/(4*N))*b;
    //convFFT[iDof][1] = cos(pi/(4*N))*b - sin(pi/(4*N))*a;

    convFFT[iDof][0] = a;
    convFFT[iDof][1] = b;
  }
  plan_conv = fftw_plan_dft_1d(N, convFFT, conv, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan_conv);

  // copy the output into the vector<double>:
  const double renorm = 1.0/(N*N);
  out_real.init(N, in1_real.comm());
  out_imag.init(N, in1_imag.comm());
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real.setVecEl(iDof, renorm * conv[iDof][0]);
    out_imag.setVecEl(iDof, renorm * conv[iDof][1]);
  }
  out_real.finalize();
  out_imag.finalize();


  // free the memory:
  fftw_destroy_plan(plan_1); fftw_free(in1); fftw_free(out1);
  fftw_destroy_plan(plan_2); fftw_free(in2); fftw_free(out2);
  fftw_destroy_plan(plan_conv); fftw_free(convFFT); fftw_free(conv);
}





// 2d fft transform:


/* 2d complex fft
  - input: a complex signal of size N X 2 (real and imag part)
  - output: the complex fft of the input signal
*/
inline void fft_2d(unsigned int Nx, unsigned int Ny, vector<double>& in_real, vector<double>& in_imag, vector<double>& out_real, vector<double>& out_imag){

  const unsigned int N = Nx * Ny;
  fftw_complex *in, *out; fftw_plan plan;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in[iDof][0] = in_real[iDof];
    in[iDof][1] = in_imag[iDof];
  }

  plan = fftw_plan_dft_2d(Nx, Ny, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan);

  out_real.resize(N);
  out_imag.resize(N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real[iDof] = out[iDof][0];
    out_imag[iDof] = out[iDof][1];
  }

  // free the memory:
  fftw_destroy_plan(plan); fftw_free(in); fftw_free(out);
}


/* 2d complex fft
  - input: a complex signal of size N X 2 (real and imag part)
  - output: the complex fft of the input signal
*/
inline void fft_2d(unsigned int Nx, unsigned int Ny, vec& in_real, vec& in_imag, vec& out_real, vec& out_imag){
  int N = Nx*Ny;
  fftw_complex *in, *out; fftw_plan plan;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in[iDof][0] = in_real(iDof);
    in[iDof][1] = in_imag(iDof);
  }

  plan = fftw_plan_dft_2d(Nx, Ny, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan);

  out_real.init(N, in_real.comm());
  out_imag.init(N, in_imag.comm());
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real.setVecEl(iDof, out[iDof][0]);
    out_imag.setVecEl(iDof, out[iDof][1]);
  }
  out_real.finalize();
  out_imag.finalize();

  // free the memory:
  fftw_destroy_plan(plan); fftw_free(in); fftw_free(out);
}


/* 2d complex ifft
  - input: a complex transform of size N X 2 (real and imag part)
  - output: the complex signal of the input transform
*/
inline void ifft_2d(unsigned int Nx, unsigned int Ny, vector<double>& in_real, vector<double>& in_imag, vector<double>& out_real, vector<double>& out_imag){
  const int N = Nx * Ny;
  fftw_complex *in, *out; fftw_plan plan;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in[iDof][0] = in_real[iDof];
    in[iDof][1] = in_imag[iDof];
  }

  plan = fftw_plan_dft_2d(Nx, Ny, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan);

  const double renorm = 1.0/N;
  out_real.resize(N);
  out_imag.resize(N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real[iDof] = renorm * out[iDof][0];
    out_imag[iDof] = renorm * out[iDof][1];
  }

  // free the memory:
  fftw_destroy_plan(plan); fftw_free(in); fftw_free(out);
}


/* 2d complex ifft
  - input: a complex transform of size N X 2 (real and imag part)
  - output: the complex signal of the input transform
*/
inline void ifft_2d(unsigned int Nx, unsigned int Ny, vec& in_real, vec& in_imag, vec& out_real, vec& out_imag){
  const int N = Nx * Ny;
  fftw_complex *in, *out; fftw_plan plan;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in[iDof][0] = in_real(iDof);
    in[iDof][1] = in_imag(iDof);
  }

  plan = fftw_plan_dft_2d(Nx, Ny, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan);


  const double renorm = 1.0/N;
  out_real.init(N, in_real.comm());
  out_imag.init(N, in_imag.comm());
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real.setVecEl(iDof, out[iDof][0]);
    out_imag.setVecEl(iDof, out[iDof][1]);
  }
  out_real.finalize();
  out_imag.finalize();
  out_real *= renorm;
  out_imag *= renorm;

  // free the memory:
  fftw_destroy_plan(plan); fftw_free(in); fftw_free(out);
}



/* computing 2d convolution:
- input: signal 1 real and imag parts, signal 2 real and imag parts
- output: the real and imaginary parts of the convolution
*/
inline void convolution_2d(unsigned int Nx, unsigned int Ny, vector<double>& in1_real, vector<double>& in1_imag, vector<double>& in2_real, vector<double>& in2_imag, vector<double>& out_real, vector<double>& out_imag){
  int N = Nx * Ny;
  assert(in2_real.size()==N);

  // fftw of the first signal:
  fftw_complex *in1, *out1; fftw_plan plan_1;
  in1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in1[iDof][0] = in1_real[iDof];
    in1[iDof][1] = in1_imag[iDof];
  }

  plan_1 = fftw_plan_dft_2d(Nx, Ny, in1, out1, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_1);

  // fftw of the second signal:
  fftw_complex *in2, *out2; fftw_plan plan_2;
  in2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in2[iDof][0] = in2_real[iDof];
    in2[iDof][1] = in2_imag[iDof];
  }

  plan_2 = fftw_plan_dft_2d(Nx, Ny, in2, out2, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_2);

  // compute the fft of the convolution:
  fftw_complex *convFFT, *conv;
  fftw_plan plan_conv;
  convFFT = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  conv = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    convFFT[iDof][0] = out1[iDof][0] * out2[iDof][0] - out1[iDof][1] * out2[iDof][1];
    convFFT[iDof][1] = out1[iDof][0] * out2[iDof][1] + out1[iDof][1] * out2[iDof][0];
  }
  plan_conv = fftw_plan_dft_2d(Nx, Ny, convFFT, conv, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan_conv);

  // copy the output into the vector<double>:
  const double renorm = 1.0/(N * N);
  out_real.resize(N);
  out_imag.resize(N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real[iDof] = renorm * conv[iDof][0];
    out_imag[iDof] = renorm * conv[iDof][1];
  }

  // free the memory:
  fftw_destroy_plan(plan_1); fftw_free(in1); fftw_free(out1);
  fftw_destroy_plan(plan_2); fftw_free(in2); fftw_free(out2);
  fftw_destroy_plan(plan_conv); fftw_free(convFFT); fftw_free(conv);
}


/* computing 2d convolution:
- input: signal 1 real and imag parts, signal 2 real and imag parts
- output: the real and imaginary parts of the convolution
*/
inline void convolution_2d(unsigned int Nx, unsigned int Ny, vec& in1_real, vec& in1_imag, vec& in2_real, vec& in2_imag, vec& out_real, vec& out_imag){
  int N = Nx * Ny;
  assert(in2_real.size()==N);

  // fftw of the first signal:
  fftw_complex *in1, *out1; fftw_plan plan_1;
  in1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in1[iDof][0] = in1_real(iDof);
    in1[iDof][1] = in1_imag(iDof);
  }

  plan_1 = fftw_plan_dft_2d(Nx, Ny, in1, out1, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_1);

  // fftw of the second signal:
  fftw_complex *in2, *out2; fftw_plan plan_2;
  in2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in2[iDof][0] = in2_real(iDof);
    in2[iDof][1] = in2_imag(iDof);
  }

  plan_2 = fftw_plan_dft_2d(Nx, Ny, in2, out2, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_2);

  // compute the fft of the convolution:
  fftw_complex *convFFT, *conv;
  fftw_plan plan_conv;
  convFFT = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  conv = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    convFFT[iDof][0] = out1[iDof][0] * out2[iDof][0] - out1[iDof][1] * out2[iDof][1];
    convFFT[iDof][1] = out1[iDof][0] * out2[iDof][1] + out1[iDof][1] * out2[iDof][0];
  }
  plan_conv = fftw_plan_dft_2d(Nx, Ny, convFFT, conv, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan_conv);

  // copy the output into the vector<double>:
  const double renorm = 1.0/(N*N);
  out_real.init(N, in1_real.comm());
  out_imag.init(N, in1_imag.comm());
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real.setVecEl(iDof, renorm * conv[iDof][0]);
    out_imag.setVecEl(iDof, renorm * conv[iDof][1]);
  }
  out_real.finalize();
  out_imag.finalize();

  // free the memory:
  fftw_destroy_plan(plan_1); fftw_free(in1); fftw_free(out1);
  fftw_destroy_plan(plan_2); fftw_free(in2); fftw_free(out2);
  fftw_destroy_plan(plan_conv); fftw_free(convFFT); fftw_free(conv);
}






// 3d fft transform:


/* 3d complex fft
  - input: a complex signal of size N X 2 (real and imag part)
  - output: the complex fft of the input signal
*/
inline void fft_3d(unsigned int Nx, unsigned int Ny, unsigned int Nz, vector<double>& in_real, vector<double>& in_imag, vector<double>& out_real, vector<double>& out_imag){

  const unsigned int N = Nx * Ny * Nz;
  fftw_complex *in, *out; fftw_plan plan;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in[iDof][0] = in_real[iDof];
    in[iDof][1] = in_imag[iDof];
  }

  plan = fftw_plan_dft_3d(Nx, Ny, Nz, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan);

  out_real.resize(N);
  out_imag.resize(N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real[iDof] = out[iDof][0];
    out_imag[iDof] = out[iDof][1];
  }

  // free the memory:
  fftw_destroy_plan(plan); fftw_free(in); fftw_free(out);
}


/* 3d complex fft
  - input: a complex signal of size N X 2 (real and imag part)
  - output: the complex fft of the input signal
*/
inline void fft_3d(unsigned int Nx, unsigned int Ny, unsigned int Nz, vec& in_real, vec& in_imag, vec& out_real, vec& out_imag){
  int N = Nx*Ny*Nz;
  fftw_complex *in, *out; fftw_plan plan;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in[iDof][0] = in_real(iDof);
    in[iDof][1] = in_imag(iDof);
  }

  plan = fftw_plan_dft_3d(Nx, Ny, Nz, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan);

  out_real.init(N, in_real.comm());
  out_imag.init(N, in_imag.comm());
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real.setVecEl(iDof, out[iDof][0]);
    out_imag.setVecEl(iDof, out[iDof][1]);
  }
  out_real.finalize();
  out_imag.finalize();

  // free the memory:
  fftw_destroy_plan(plan); fftw_free(in); fftw_free(out);
}


/* 3d complex ifft
  - input: a complex transform of size N X 2 (real and imag part)
  - output: the complex signal of the input transform
*/
inline void ifft_3d(unsigned int Nx, unsigned int Ny, unsigned int Nz, vector<double>& in_real, vector<double>& in_imag, vector<double>& out_real, vector<double>& out_imag){
  const int N = Nx * Ny * Nz;
  fftw_complex *in, *out; fftw_plan plan;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in[iDof][0] = in_real[iDof];
    in[iDof][1] = in_imag[iDof];
  }

  plan = fftw_plan_dft_3d(Nx, Ny, Nz, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan);

  const double renorm = 1.0/N;
  out_real.resize(N);
  out_imag.resize(N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real[iDof] = renorm * out[iDof][0];
    out_imag[iDof] = renorm * out[iDof][1];
  }

  // free the memory:
  fftw_destroy_plan(plan); fftw_free(in); fftw_free(out);
}


/* 3d complex ifft
  - input: a complex transform of size N X 2 (real and imag part)
  - output: the complex signal of the input transform
*/
inline void ifft_3d(unsigned int Nx, unsigned int Ny, unsigned int Nz, vec& in_real, vec& in_imag, vec& out_real, vec& out_imag){
  const int N = Nx * Ny * Nz;
  fftw_complex *in, *out; fftw_plan plan;
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in[iDof][0] = in_real(iDof);
    in[iDof][1] = in_imag(iDof);
  }

  plan = fftw_plan_dft_3d(Nx, Ny, Nz, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan);


  const double renorm = 1.0/N;
  out_real.init(N, in_real.comm());
  out_imag.init(N, in_imag.comm());
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real.setVecEl(iDof, out[iDof][0]);
    out_imag.setVecEl(iDof, out[iDof][1]);
  }
  out_real.finalize();
  out_imag.finalize();
  out_real *= renorm;
  out_imag *= renorm;

  // free the memory:
  fftw_destroy_plan(plan); fftw_free(in); fftw_free(out);
}


/* computing 3d convolution:
- input: signal 1 real and imag parts, signal 2 real and imag parts
- output: the real and imaginary parts of the convolution
*/
inline void convolution_3d(unsigned int Nx, unsigned int Ny, unsigned int Nz, vector<double>& in1_real, vector<double>& in1_imag, vector<double>& in2_real, vector<double>& in2_imag, vector<double>& out_real, vector<double>& out_imag){
  int N = Nx * Ny * Nz;
  assert(in2_real.size()==N);

  // fftw of the first signal:
  fftw_complex *in1, *out1; fftw_plan plan_1;
  in1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in1[iDof][0] = in1_real[iDof];
    in1[iDof][1] = in1_imag[iDof];
  }

  plan_1 = fftw_plan_dft_3d(Nx, Ny, Nz, in1, out1, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_1);

  // fftw of the second signal:
  fftw_complex *in2, *out2; fftw_plan plan_2;
  in2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in2[iDof][0] = in2_real[iDof];
    in2[iDof][1] = in2_imag[iDof];
  }

  plan_2 = fftw_plan_dft_3d(Nx, Ny, Nz, in2, out2, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_2);

  // compute the fft of the convolution:
  fftw_complex *convFFT, *conv;
  fftw_plan plan_conv;
  convFFT = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  conv = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    convFFT[iDof][0] = out1[iDof][0] * out2[iDof][0] - out1[iDof][1] * out2[iDof][1];
    convFFT[iDof][1] = out1[iDof][0] * out2[iDof][1] + out1[iDof][1] * out2[iDof][0];
  }
  plan_conv = fftw_plan_dft_3d(Nx, Ny, Nz, convFFT, conv, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan_conv);

  // copy the output into the vector<double>:
  const double renorm = 1.0/(N*N);
  out_real.resize(N);
  out_imag.resize(N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real[iDof] = renorm * conv[iDof][0];
    out_imag[iDof] = renorm * conv[iDof][1];
  }

  // free the memory:
  fftw_destroy_plan(plan_1); fftw_free(in1); fftw_free(out1);
  fftw_destroy_plan(plan_2); fftw_free(in2); fftw_free(out2);
  fftw_destroy_plan(plan_conv); fftw_free(convFFT); fftw_free(conv);
}


/* computing 3d convolution:
- input: signal 1 real and imag parts, signal 2 real and imag parts
- output: the real and imaginary parts of the convolution
*/
inline void convolution_3d(unsigned int Nx, unsigned int Ny, unsigned int Nz, vec& in1_real, vec& in1_imag, vec& in2_real, vec& in2_imag, vec& out_real, vec& out_imag){
  int N = Nx * Ny * Nz;
  assert(in2_real.size()==N);

  // fftw of the first signal:
  fftw_complex *in1, *out1; fftw_plan plan_1;
  in1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in1[iDof][0] = in1_real(iDof);
    in1[iDof][1] = in1_imag(iDof);
  }

  plan_1 = fftw_plan_dft_3d(Nx, Ny, Nz, in1, out1, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_1);

  // fftw of the second signal:
  fftw_complex *in2, *out2; fftw_plan plan_2;
  in2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  out2 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    in2[iDof][0] = in2_real(iDof);
    in2[iDof][1] = in2_imag(iDof);
  }

  plan_2 = fftw_plan_dft_3d(Nx, Ny, Nz, in2, out2, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan_2);

  // compute the fft of the convolution:
  fftw_complex *convFFT, *conv;
  fftw_plan plan_conv;
  convFFT = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  conv = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int iDof=0; iDof<N; iDof++){
    convFFT[iDof][0] = out1[iDof][0] * out2[iDof][0] - out1[iDof][1] * out2[iDof][1];
    convFFT[iDof][1] = out1[iDof][0] * out2[iDof][1] + out1[iDof][1] * out2[iDof][0];
  }
  plan_conv = fftw_plan_dft_3d(Nx, Ny, Nz, convFFT, conv, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan_conv);

  // copy the output into the vector<double>:
  const double renorm = 1.0/(N*N);
  out_real.init(N, in1_real.comm());
  out_imag.init(N, in1_imag.comm());
  for(unsigned int iDof=0; iDof<N; iDof++){
    out_real.setVecEl(iDof, renorm * conv[iDof][0]);
    out_imag.setVecEl(iDof, renorm * conv[iDof][1]);
  }
  out_real.finalize();
  out_imag.finalize();

  // free the memory:
  fftw_destroy_plan(plan_1); fftw_free(in1); fftw_free(out1);
  fftw_destroy_plan(plan_2); fftw_free(in2); fftw_free(out2);
  fftw_destroy_plan(plan_conv); fftw_free(convFFT); fftw_free(conv);
}




#endif
