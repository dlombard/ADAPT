// Class to solve the Schroedinger problem, two electrons.

#ifndef twoElectrons_CP_h
#define twoElectrons_CP_h

// Including headers:
#include "../../genericInclude.h"
#include "../../linAlg/linAlg.h"
#include "../../mlSolvers/mlSolvers.h"
#include "../../mlSolvers/operatorTensor.h"
#include "../../mlSolvers/operatorOperations.h"
#include "../../TensorTrain/TensorTrain.h"
#include "../../TensorTrain/TTOperations.hpp"
#include "../../mlSolvers/blockOperatorTensor.h"
#include "../../Discretisations/finiteDifferencesPer.h"
#include "fft.h"

using namespace std;


// class CP3var to represent the solution:
// particularisation of CP format to 3 variables.
class CP3var{
private:
  unsigned int m_nVar=3;
  MPI_Comm m_comm = PETSC_COMM_WORLD;
  vector<unsigned int> m_nDof_var;
  unsigned int m_rank;
  vector<vec> m_R;
  vector<vec> m_G;
  vector<vec> m_S;

public:
  CP3var(){};
  CP3var(vector<unsigned int>);
  CP3var(vector<vec>&, vector<vec>&, vector<vec>&);
  CP3var(vec&, vec&, vec&);

  ~CP3var(){
    /*for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      m_R[iTerm].clear();
      m_G[iTerm].clear();
      m_S[iTerm].clear();
    }*/
  }

  inline void set_rank(unsigned int k){m_rank=k;}
  inline void set_terms(vector<vec> R_tab, vector<vec> G_tab, vector<vec> S_tab){
    m_rank = R_tab.size();
    m_R = R_tab;
    m_G = G_tab;
    m_S = S_tab;
    m_nDof_var[0] = R_tab[0].size();
    m_nDof_var[1] = G_tab[0].size();
    m_nDof_var[2] = S_tab[0].size();
  }
  inline void set_terms(vec r_one, vec g_one, vec s_one){
    m_rank = 1;
    m_R.resize(1);
    m_R[0] = r_one;
    m_G.resize(1);
    m_G[0] = g_one;
    m_S.resize(1);
    m_S[0] = s_one;
    m_nDof_var[0] = r_one.size();
    m_nDof_var[1] = g_one.size();
    m_nDof_var[2] = s_one.size();
  }

  void clear(){
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      m_R[iTerm].clear();
      m_G[iTerm].clear();
      m_S[iTerm].clear();
    }
  }

  void init(vector<unsigned int> n_dofs){
    m_rank = 0;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_nDof_var[iVar] = n_dofs[iVar];
    }
  }

  // METHODS:
  void copyTensorFrom(CP3var&);
  void zero();
  void zero(vector<unsigned int>);
  void append(vec&, vec&, vec&);
  void append(vector<vec>&, vector<vec>&, vector<vec>&);
  void axpy(CP3var&,double, bool);
  CP3var applyOperator(operatorTensor&);
  double norm2CP();


  // OPERATORS:
  void operator *= (double);
  void operator << (CP3var&);
  void operator += (CP3var&);
  double operator ()(unsigned int i, unsigned int j, unsigned int k);

  // ACCESS FUNCTIONS:
  inline MPI_Comm comm(){return m_comm;}
  inline vector<unsigned int> nDof_var(){return m_nDof_var;}
  inline unsigned int nDof_var(unsigned int iVar){return m_nDof_var[iVar];}
  inline unsigned int rank(){return m_rank;}
  inline vector<vec> R(){return m_R;}
  inline vec R(unsigned int iTerm){return m_R[iTerm];}
  inline vector<vec> G(){return m_G;}
  inline vec G(unsigned int iTerm){return m_G[iTerm];}
  inline vector<vec> S(){return m_S;}
  inline vec S(unsigned int iTerm){return m_S[iTerm];}
  inline void print(){
    cout << "Rank = " << m_rank << endl;
  }
};



// Auxiliary functions and operators:
CP3var operator * (operatorTensor&, CP3var&);
double operator * (CP3var&, CP3var&);



class twoElectrons_CP{
private:

  //domain size
  double m_L;

  unsigned int m_dim;
  vector<unsigned int> m_Ndof;    //Number of degrees of freedom per direction: has to be odd for 0 not to be included in the list of discretization points
  unsigned int m_ndof;

  MPI_Comm m_theComm;

  finiteDifferencesPer m_testSpace;

  unsigned int m_nIt;
  double m_tFin;
  double m_dt;

  // Non-symmetrized operators
  operatorTensor m_H0_even;
  operatorTensor m_H0_odd;
  operatorTensor m_Id;

  mat m_Laplacian;
  mat m_masse;
  mat m_Potxy;
  mat m_Coulombz;
  vector<mat> m_D;
  vector<mat> m_multxyz;

  // re = real even, ro = real odd, ie = imag even, io = imag odd
  CP3var m_u_re, m_u_ro, m_u_ie, m_u_io; // current solution
  CP3var m_u_re_old, m_u_ro_old, m_u_ie_old, m_u_io_old; // previous solution


public:
  twoElectrons_CP(){};
  twoElectrons_CP(double,unsigned int);
  twoElectrons_CP(double, unsigned int, vector<unsigned int>, MPI_Comm);
  ~twoElectrons_CP(){};


  // SETTERS:
  void setInitialCondition(CP3var&, CP3var&, CP3var&, CP3var&);
  inline void setFinalTime(const double fin_T){m_tFin = fin_T;};
  inline void setNumberOfTimeSteps(const unsigned int nIt){m_nIt = nIt;};
  void setHamiltonian(operatorTensor&);
  void setMatrices(mat&, mat&, mat&, mat&, vector<mat>&, vector<mat>&);
  void setMatrices();
  void setOperatorId();
  void setOperatorH0();
  void setFiniteDifferences(finiteDifferencesPer&);

  // AUXILIARY FUNCTIONS:
  double L2scalprod(vec&, vec&, vec&);
  vec revert(vec&);
  double even_even_L2scalprod(vec&, vec&, vec&, vec&, vec&, vec&);
  double even_odd_L2scalprod(vec&, vec&, vec&, vec&, vec&, vec&);
  double odd_odd_L2scalprod(vec&, vec&, vec&, vec&, vec&, vec&);

  // METHODS:
  void save_1d_wave_fun_vtk(string, unsigned int);
  vec conv(vec&, vec&);
  double norm2xy(CP3var&);
  void rank1_up(CP3var&, vec&, vec&, vec&, double, bool verbose=false);
  void rank1_up(CP3var&, vec&, vec&, vec&, vector<vec>&, double, bool verbose=false);
  unsigned int compute_guess(CP3var&, CP3var&);
  void round_CP(CP3var&, double);

  // DYNAMICS SOLVER:
  void krylovBuild(vector<CP3var>&, blockOperatorTensor&, unsigned int, double, vector<vector<CP3var> >&);
  void krylovMultipleIter(vector<CP3var>&, blockOperatorTensor&, unsigned int, double, vector<CP3var>&);
  void one_step_schroedinger();

  // ACCESS functions:
  inline finiteDifferencesPer testSpace(){return m_testSpace;}
};

#endif
