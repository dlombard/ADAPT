// Class to solve the Schroedinger problem, two electrons.

#ifndef twoElectrons_h
#define twoElectrons_h

// Including headers:
#include "../../genericInclude.h"
#include "../../linAlg/linAlg.h"
#include "../../mlSolvers/mlSolvers.h"
#include "../../mlSolvers/operatorTensor.h"
#include "../../mlSolvers/operatorOperations.h"
#include "../../TensorTrain/TensorTrain.h"
#include "../../TensorTrain/TTOperations.hpp"
#include "../../mlSolvers/blockOperatorTensor.h"
#include "../../Discretisations/finiteDifferencesPer.h"
#include "fft.h"

using namespace std;

class twoElectrons{
private:

   //domain size
   double m_L;

  unsigned int m_dim;
  vector<unsigned int> m_Ndof;    //Number of degrees of freedom per direction: has to be odd for 0 not to be included in the list of discretization points
  unsigned int m_ndof;

  MPI_Comm m_theComm;

   finiteDifferencesPer m_testSpace;

   vector<TensorTrain> m_sol_real_even;
   vector<TensorTrain> m_sol_real_odd;

   vector<TensorTrain> m_sol_imag_even;
   vector<TensorTrain> m_sol_imag_odd;




  unsigned int m_nIt;
  double m_tFin;
  double m_dt;

  // Non-symmetrized operators
  operatorTensor m_H0_even;
  operatorTensor m_H0_odd;
  operatorTensor m_Id;

  mat m_Laplacian;
  mat m_masse;
  mat m_Potxy;
  mat m_Coulombz;
  vector<mat> m_D;
  vector<mat> m_multxyz;



public:
  twoElectrons(){};
  twoElectrons(double,unsigned int);
  twoElectrons(double,unsigned int,TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&);
  twoElectrons(double, unsigned int, vector<unsigned int>, MPI_Comm);
  ~twoElectrons(){};
/*    for(unsigned int it; it<m_sol_real_even.size(); it++){
      //m_sol_real_[it].clear();
    }
    for(unsigned int it; it<m_sol_im_.size(); it++){
      //m_sol_im_[it].clear();
    }
  }*/

  // SETTERS:
  void setInitialCondition(TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&);
  void setFinalTime(const double);
  void setNumberOfTimeSteps(const unsigned int);
  void setHamiltonian(operatorTensor&);
  void setMatrices(mat&, mat&, mat&, mat&, vector<mat>&, vector<mat>&);
  void setMatrices();
  void setOperatorId();
  void setOperatorH0();
  //void setDimAndNdof(unsigned int, vector<unsigned int>);
  void setFiniteDifferences(finiteDifferencesPer&);
  //void setOperatorH0();


  // DYNAMICS SOLVER:
  //double squareL2norm(TensorTrain&, TensorTrain&);
  double L2scalprod(vec&, vec&, vec&);
  vec revert(vec&);
  double even_even_L2scalprod(vec&, vec&, vec&, vec&, vec&, vec&);
  double even_odd_L2scalprod(vec&, vec&, vec&, vec&, vec&, vec&);
  double odd_odd_L2scalprod(vec&, vec&, vec&, vec&, vec&, vec&);
  double L2scal_even_even(TensorTrain&, TensorTrain&);
  double L2scal_even_odd(TensorTrain&, TensorTrain&);
  double L2scal_odd_odd(TensorTrain&, TensorTrain&);
  double squareL2norm(TensorTrain&, TensorTrain&);
  double L2scal(TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&);

  // Auxiliary functions:
  void round_asvd(TensorTrain&, double);
  void round_sum_asvd(TensorTrain&, TensorTrain&, double);
  void krylovBuild(vector<TensorTrain>&, blockOperatorTensor&, unsigned int, double, vector<vector<TensorTrain> >&);
  void least_squares(vector<TensorTrain>&, TensorTrain&, vec&);
  void least_squares_block(vector<TensorTrain>&, vector<TensorTrain>&, TensorTrain&, TensorTrain&, vec&);
  void extractBasis(vector<TensorTrain>&, unsigned int, vector<vec>&, vector<vec>&);

  // ACCESS FUNCTIONS:
  inline vector<TensorTrain> sol_real_even(){return m_sol_real_even;}
  inline vector<TensorTrain> sol_real_odd(){return m_sol_real_odd;}
  inline vector<TensorTrain> sol_imag_even(){return m_sol_imag_even;}
  inline vector<TensorTrain> sol_imag_odd(){return m_sol_imag_odd;}


  inline TensorTrain sol_real_even(unsigned int it){return m_sol_real_even[it];}
  inline TensorTrain sol_real_odd(unsigned int it){return m_sol_real_odd[it];}
  inline TensorTrain sol_imag_even(unsigned int it){return m_sol_imag_even[it];}
  inline TensorTrain sol_imag_odd(unsigned int it){return m_sol_imag_odd[it];}


  inline MPI_Comm comm(){return m_theComm;}
  inline double tFin(){return m_tFin;}
  inline unsigned int nIt(){return m_nIt;}
  inline double dt(){return m_dt;}
  inline operatorTensor H0_even(){return m_H0_even;}
  inline operatorTensor H0_odd(){return m_H0_odd;}
  inline operatorTensor Id(){return m_Id;}
  //inline unsigned int dim(){return m_dim;}
  //inline vector<unsigned int> Ndof(){return m_Ndof;}
  //inline unsigned int Ndof(unsigned int k){return m_Ndof[k];}
  inline finiteDifferencesPer testSpace(){return m_testSpace;}
  inline mat Laplacian(){return m_Laplacian;}


  //RESOLUTION FUNCTION
  void heat_onetimestepresolution(const double&, const double&, TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&);
  void schroe_onetimestepresolution(const double&, TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&);

  void heat_dynamicsresolution(const double&, TensorTrain&, TensorTrain&, const int&);
  void schroe_dynamicsresolution(const double&, TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&, const int&);

  void solve_min_res_heat(const double&, const double&, TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&);
  void solve_min_res_schroe(const double&, TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&, TensorTrain&);

  // Export fields:
  vec compute_density();
  void save_1d_wave_fun_vtk(string, unsigned int);


};

#endif
