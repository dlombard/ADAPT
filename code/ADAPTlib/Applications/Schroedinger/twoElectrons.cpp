// implementation of the class two electrons
#include "twoElectrons.h"

// I - CONSTRUCTORS -

/* I.1 - construct given final time and number of iterations
  - input: T and m_nIt
  - output: construct the object
*/
twoElectrons::twoElectrons(double T, unsigned int nSteps){
  m_nIt = nSteps;
  assert(m_nIt>1);
  m_tFin = T;
  assert(T>0);
  m_dt = m_tFin/(m_nIt - 1);

  m_sol_real_even.resize(m_nIt);
  m_sol_real_odd.resize(m_nIt);
  m_sol_imag_even.resize(m_nIt);
  m_sol_imag_odd.resize(m_nIt);
}


twoElectrons::twoElectrons(double L, unsigned int dim, vector<unsigned int> Ndof, MPI_Comm theComm){


  m_L = L;
  m_dim = dim;
  m_Ndof = Ndof;
  m_theComm  = theComm;


  vector<unsigned int> Dofper(dim);
  unsigned int ndof =1;
  for (unsigned int i=0; i<dim; i++)
  {
	assert( Ndof[i] % 2 == 1);
	Dofper[i] = Ndof[i];
        ndof *= Ndof[i];
  }

  m_ndof = ndof;

  vector<double> box(2*dim);
  for (unsigned int i=0; i<dim; i++)
  {
	box[2*i] = -L;
        box[2*i+1] = L;
  }


  finiteDifferencesPer testSpace(dim, Dofper,box, m_theComm);
  setFiniteDifferences(testSpace);

}




/* I.2 - construct given final time, number of iterations, initial condition
  - input: T m_nIt and u_0
  - output: construct the object
*/
twoElectrons::twoElectrons(double T, unsigned int nSteps, TensorTrain& u_0_real_even, TensorTrain& u_0_real_odd, TensorTrain& u_0_imag_even, TensorTrain& u_0_imag_odd){
  m_nIt = nSteps;
  assert(m_nIt>1);
  m_tFin = T;
  assert(T>0);
  m_dt = m_tFin/(m_nIt - 1);

  m_theComm = u_0_real_even.comm();

  m_sol_real_even.resize(m_nIt);
  m_sol_real_even[0] = u_0_real_even;
  m_sol_real_odd.resize(m_nIt);
  m_sol_real_odd[0] = u_0_real_odd;

  m_sol_imag_even.resize(m_nIt);
  m_sol_imag_even[0] = u_0_imag_even;
  m_sol_imag_odd.resize(m_nIt);
  m_sol_imag_odd[0] = u_0_imag_odd;
}



// II- SETTERS

/* II.1 set initial set the initial condition:
  - input: a CPTensor u_0
  - output: set m_sol[0] = u_0
*/
void twoElectrons::setInitialCondition(TensorTrain& u_0_real_even, TensorTrain& u_0_real_odd, TensorTrain& u_0_imag_even, TensorTrain& u_0_imag_odd){
  assert(m_sol_real_even.size()>0);
  m_sol_real_even[0] = u_0_real_even;
  assert(m_sol_real_odd.size()>0);
  m_sol_real_odd[0] = u_0_real_odd;

   assert(m_sol_imag_even.size()>0);
  m_sol_imag_even[0] = u_0_imag_even;
  assert(m_sol_imag_odd.size()>0);
  m_sol_imag_odd[0] = u_0_imag_odd;
}

/* II.2 set final time:
  - input: a double T
  - output: set m_tFin = T
*/
void twoElectrons::setFinalTime(const double T){
  m_tFin = T;
}

/* II.3 set final time:
  - input: the number of iterations
  - output: set m_nIt = nIter
*/
void twoElectrons::setNumberOfTimeSteps(const unsigned int nIter){
  m_nIt = nIter;
}

/*II.4

*/
void twoElectrons::setFiniteDifferences(finiteDifferencesPer& testSpace)
{
	m_testSpace = testSpace;
  m_ndof = testSpace.nDof();
  m_Ndof = testSpace.struc().nDof_var();
}


/* II.4 set discretized matrices:
*/
void twoElectrons::setMatrices(mat& masse, mat& Laplacian, mat& Coulombz, mat& Potxy, vector<mat>& D, vector<mat>& multxyz)
{
  	m_Laplacian = Laplacian;
	m_masse = masse;
	m_Potxy = Potxy;
  	m_Coulombz = Coulombz;
	m_D = D;
	m_multxyz = multxyz;
}


/* II.4 set discretized matrices:
*/
void twoElectrons::setMatrices()
{
	 /////////////////////////////////

    // Compute the mass matrix:
    m_testSpace.compute_mass();
    mat masse = m_testSpace.mass();
    // Test a Laplacian matrix assembly, without boundaries:

    mat Laplacian(m_ndof, m_ndof, m_theComm);
    for(unsigned int iDof=0; iDof<m_ndof; iDof++){
      // Assemble the bulk:
     // if(!testSpace.isItOnBoundary(iDof)){
        double diagVal = 0.0;
        for(unsigned int iVar=0; iVar<m_testSpace.dim(); iVar++){
          diagVal += 2.0/(m_testSpace.dx(iVar)*m_testSpace.dx(iVar));
        }
        //Laplacian(iDof,iDof) = diagVal;
        Laplacian.setMatEl(iDof,iDof,diagVal);

        // number of "star points" = 2*n with increments [0,..,±1,...]
        for(unsigned int iVar=0; iVar<m_testSpace.dim(); iVar++){
          vector<int> inc_plus(m_testSpace.dim(),0);
          inc_plus[iVar] = 1;
          unsigned int lin_plus = m_testSpace.linIndIncrementPer(iDof,inc_plus);
          //Laplacian(iDof,lin_plus) = -1.0/(m_testSpace.dx(iVar)*m_testSpace.dx(iVar));
          double val_plus = -1.0/(m_testSpace.dx(iVar)*m_testSpace.dx(iVar));
          Laplacian.setMatEl(iDof,lin_plus,val_plus);
          vector<int> inc_minus(m_testSpace.dim(),0);
          inc_minus[iVar] = -1;
          unsigned int lin_minus = m_testSpace.linIndIncrementPer(iDof,inc_minus);
          //cout << "lin_plus = " << lin_plus << "   lin_minus=" << lin_minus <<  endl;
          //Laplacian(iDof,lin_minus) = -1.0/(m_testSpace.dx(iVar)*m_testSpace.dx(iVar));
          double val_min = -1.0/(m_testSpace.dx(iVar)*m_testSpace.dx(iVar));
          Laplacian.setMatEl(iDof,lin_minus,val_min);
        }
    }
    Laplacian.finalize();
    //Laplacian.print();

//A changer
    /*double d = 1.5;
    mat Potxy(m_ndof, m_ndof, m_theComm);
    for(unsigned int iDof=0; iDof<m_ndof; iDof++){

	vector<unsigned int> ind = m_testSpace.struc().lin2sub(iDof);
        vector<double> point = m_testSpace.compute_point(ind);
        double diagValp = (point[0] + d/2)*(point[0] + d/2);
	double diagValm = (point[0] - d/2)*(point[0] - d/2);
        for(unsigned int iVar=1; iVar<m_testSpace.dim(); iVar++){

          diagValp += point[iVar]*point[iVar];
	  diagValm += point[iVar]*point[iVar];
        }
        Potxy(iDof,iDof) = (-1.0)/(0.01 + sqrt(diagValp)) + (-1.0)/(0.01 + sqrt(diagValm));
    }
    Potxy.finalize();
    Potxy.print();*/

    mat Potxy(m_ndof, m_ndof, m_theComm);
    for(unsigned int iDof=0; iDof<m_ndof; iDof++){
	      vector<unsigned int> ind = m_testSpace.struc().lin2sub(iDof);
        vector<double> point = m_testSpace.compute_point(ind);
        double diagValp = (point[0])*(point[0]);
        for(unsigned int iVar=1; iVar<m_testSpace.dim(); iVar++){
          diagValp += point[iVar]*point[iVar];
        }
        double val = (-3.0)/(0.01 + sqrt(diagValp));
        //Potxy(iDof,iDof) = (-3.0)/(0.01 + sqrt(diagValp));
        Potxy.setMatEl(iDof,iDof,val);
    }
    Potxy.finalize();
    //Potxy.print();


    mat Coulombz(m_ndof, m_ndof, m_theComm);
    for(unsigned int iDof=0; iDof<m_ndof; iDof++){
	      vector<unsigned int> ind = m_testSpace.struc().lin2sub(iDof);
        vector<double> point = m_testSpace.compute_point(ind);
        double diagVal = 0;
        for(unsigned int iVar=0; iVar<m_testSpace.dim(); iVar++){

          diagVal += point[iVar]*point[iVar];
        }
        double val = 1.0/(0.01+ sqrt(diagVal));
        //Coulombz(iDof,iDof) = 1.0/(0.01+ sqrt(diagVal));
        Coulombz.setMatEl(iDof,iDof,val);

    }
    Coulombz.finalize();
    //Coulombz.print();

    vector<mat> D(m_testSpace.dim());
    for(unsigned int idim=0; idim<m_testSpace.dim(); idim++){
	    // Test a Laplacian matrix assembly, without boundaries:
    	D[idim].init(m_ndof, m_ndof, m_theComm);
    	for(unsigned int iDof=0; iDof<m_ndof; iDof++){

          vector<int> inc_plus(m_testSpace.dim(),0);
          inc_plus[idim] = 1;
          unsigned int lin_plus = m_testSpace.linIndIncrementPer(iDof,inc_plus);
          double val_plus =  -0.5/(m_testSpace.dx(idim));
          //D[idim](iDof,lin_plus) = -0.5/(m_testSpace.dx(idim));
          D[idim].setMatEl(iDof,lin_plus,val_plus);
          vector<int> inc_minus(m_testSpace.dim(),0);
          inc_minus[idim] = -1;
          unsigned int lin_minus = m_testSpace.linIndIncrementPer(iDof,inc_minus);
          double val_min = 0.5/(m_testSpace.dx(idim));
          //D[idim](iDof,lin_minus) = 0.5/(m_testSpace.dx(idim));
          D[idim].setMatEl(iDof,lin_minus,val_min);
        }

    	D[idim].finalize();
   	  //D[idim].print();
    }


     vector<mat> multxyz(m_testSpace.dim());
      for(unsigned int idim=0; idim<m_testSpace.dim(); idim++){


    	multxyz[idim].init(m_ndof, m_ndof, m_theComm);


    	for(unsigned int iDof=0; iDof<m_ndof; iDof++){

		      vector<unsigned int> ind = m_testSpace.struc().lin2sub(iDof);
        	vector<double> point = m_testSpace.compute_point(ind);
        	double diagVal = point[idim];
        	//multxyz[idim](iDof, iDof) = diagVal;
          multxyz[idim].setMatEl(iDof,iDof,diagVal);
        }
        multxyz[idim].finalize();
        //multxyz[idim].print();
    }

	m_Laplacian = Laplacian;
	m_masse = masse;
	m_Potxy = Potxy;
  m_Coulombz = Coulombz;
	m_D = D;
	m_multxyz = multxyz;
}



/* II.5 Create operatorTensors
*/
void twoElectrons::setOperatorId()
{

  vector< vector<mat> > Op_ir_;

	vector<mat> masse_mat;
	masse_mat.resize(3);
	masse_mat[0] = m_masse;
	masse_mat[2] = m_masse;
	masse_mat[1] = m_masse;

	Op_ir_.resize(1);
	Op_ir_[0] = masse_mat;

	MPI_Comm theComm = m_masse.comm();
	m_Id.init(Op_ir_,theComm);
}

void twoElectrons::setOperatorH0()
{

	vector< vector<mat> > Op_even;
	vector< vector<mat> > Op_odd;

	vector<mat> Lap1_mat;
	Lap1_mat.resize(3);
	Lap1_mat[0] = m_Laplacian;
	Lap1_mat[2] = m_masse;
	Lap1_mat[1] = m_masse;

	vector<mat> Lap2_mat;
	Lap2_mat.resize(3);
	Lap2_mat[0] = m_masse;
	Lap2_mat[2] = m_Laplacian;
	Lap2_mat[1] = m_masse;

	mat TwotimesLap = 2.0*m_Laplacian;

	vector<mat> Lap3_mat;
	Lap3_mat.resize(3);
	Lap3_mat[0] = m_masse;
	Lap3_mat[2] = m_masse;
	Lap3_mat[1] = TwotimesLap;

	vector<mat> Coulomb_mat;
	Coulomb_mat.resize(3);
	Coulomb_mat[0] = m_masse;
	Coulomb_mat[2] = m_masse;
	Coulomb_mat[1] = m_Coulombz;

	vector<mat> Pot1_mat;
	Pot1_mat.resize(3);
	Pot1_mat[0] = m_Potxy;
	Pot1_mat[2] = m_masse;
	Pot1_mat[1] = m_masse;

	vector<mat> Pot2_mat;
	Pot2_mat.resize(3);
	Pot2_mat[0] = m_masse;
	Pot2_mat[2] = m_Potxy;
	Pot2_mat[1] = m_masse;


	unsigned int dim = m_D.size();

  Op_even.resize(6);
  Op_odd.resize(2*dim);
	Op_even[0] = Lap1_mat;
	Op_even[1] = Lap2_mat;
	Op_even[2] = Lap3_mat;
	Op_even[3] = Coulomb_mat;
	Op_even[4] = Pot1_mat;
	Op_even[5] = Pot2_mat;

	for (int i = 0; i<dim; i++)
	{
		vector<mat> Dxi_mat;
		Dxi_mat.resize(3);
		Dxi_mat[0] = 2*m_D[i];
		Dxi_mat[2] = m_masse;
		Dxi_mat[1] = m_D[i];

		Op_odd[2*i] = Dxi_mat;

	 	vector<mat> Dxi_mat2;
		Dxi_mat2.resize(3);
		Dxi_mat2[0] = m_masse;
		Dxi_mat2[2] = -2*m_D[i];
		Dxi_mat2[1] = m_D[i];

		Op_odd[2*i+1] = Dxi_mat2;
	}


	MPI_Comm theComm = m_masse.comm();

	m_H0_even.init(Op_even,theComm);
	m_H0_odd.init(Op_odd,theComm);

}

/*void twoElectrons::setDimAndNdof(unsigned int dim, vector<unsigned int> Ndof)
{
	m_dim = dim;
	m_Ndof = Ndof;
}*/


//Function which computes an integral of the form
//\int_{x,y}r(x)s(y)w(x-y)
double twoElectrons::L2scalprod(vec& r, vec& s, vec& w)
{
	assert(r.size()==s.size());
	assert(r.size() == w.size());
	vec conv_ws_real, conv_ws_imag;
        int N = r.size();

	MPI_Comm theComm = m_masse.comm();

	vec empty_s(N, theComm);
	vec empty_w(N, theComm);
	for (unsigned int iDof=0; iDof < N; iDof++)
	{
    double val = 0.0;
		empty_s.setVecEl(iDof,val);
		empty_w.setVecEl(iDof,val);
	}
	empty_s.finalize();
	empty_w.finalize();

	if (m_testSpace.dim()==1)
	{
		convolution_1d_schroedinger(s,empty_s,w, empty_w, conv_ws_real, conv_ws_imag);
    conv_ws_real*= (m_testSpace.bounds(1)-m_testSpace.bounds(0));
		//Normalement conv_ws_imag==0

	}
	else if (m_testSpace.dim()==2)
	{
		convolution_2d(m_testSpace.dofPerDim()[0]-1, m_testSpace.dofPerDim()[1]-1, s,empty_s,w, empty_w, conv_ws_real, conv_ws_imag);
		//Normalement conv_ws_imag==0
		// Attention!!! Tester!!!!
	}
	else if (m_testSpace.dim()==3)
	{
		convolution_3d(m_testSpace.dofPerDim()[0]-1, m_testSpace.dofPerDim()[1]-1, m_testSpace.dofPerDim()[2]-1, s,empty_s,w, empty_w, conv_ws_real, conv_ws_imag);
		//Normalement conv_ws_imag==0
		//Attention!!! Tester!!!
	}

	double temp = scalProd(r,conv_ws_real)*m_testSpace.dx(0);
	//cout << "scal prod done " << endl;
	return temp;

}

//Renvoie le vecteur de la fonction w(-x)
vec twoElectrons::revert(vec& w)
{
	//w.print();
	int N = w.size();
	MPI_Comm theComm = w.comm();
	vec revert_w(N, theComm);

	//N est necessairement impair

	if (m_testSpace.dim()==1)
	{
		double value = w.getVecEl(0);
		revert_w.setVecEl(0, value);
		for (unsigned int iDof=1; iDof <= (N-1)/2; iDof++) // N has to have an odd value
		{
			value = w.getVecEl(iDof);
			revert_w.setVecEl(N-iDof, value);

			value = w.getVecEl(N-iDof);
			revert_w.setVecEl(iDof, value);
		}
		revert_w.finalize();

	}
	else if (m_testSpace.dim()==2)
	{
		//TODO

	}
	else if (m_testSpace.dim()==3)
	{
		//TO DO
	}

	return revert_w;
}



//Function which computes an integral of the form
//\int_{x,y} 1/2[ r_k(x) s_k(y) + r_k(y) s_k(x)] 1/2[ w_k(x-y) + w_k(y-x)] 1/2[ r_l(x) s_l(y) + r_l(y) s_l(x)] 1/2[ w_l(x-y) + w_l(y-x)]
double twoElectrons::even_even_L2scalprod(vec& rk, vec& sk, vec& wk, vec& rl, vec& sl, vec& wl)
{
	vec rev_wk = revert(wk);
	vec rev_wl = revert(wl);

	vec wk_ev = wk + rev_wk;
	wk_ev *= 0.5;

	vec wl_ev = wl + rev_wl;
	wl_ev *=0.5;

	double sum = 0;

	//Calcul de \int_{x,y} 1/2 r_k(x) s_k(y) w_k(x-y) 1/2 r_l(x)s_l(y)  w_l(x-y)
	vec w = pointWiseMult(wk_ev,wl_ev);
	vec r1 = pointWiseMult(rk,rl);
	vec s1 = pointWiseMult(sk,sl);

	sum+=L2scalprod(r1, s1, w);

	//Calcul de \int_{x,y} 1/2 r_k(x) s_k(y) w_k(x-y) 1/2 r_l(y)s_l(x)  w_l(x-y)
	vec r2 = pointWiseMult(rk,sl);
	vec s2 = pointWiseMult(sk,rl);

	sum+=L2scalprod(r2, s2, w);


	//Calcul de \int_{x,y} 1/2 r_k(y) s_k(x) w_k(x-y) 1/2 r_l(x)s_l(y)  w_l(x-y)

	sum+=L2scalprod(s2, r2, w);


	//Calcul de \int_{x,y} 1/2 r_k(y) s_k(x) w_k(x-y) 1/2 r_l(y)s_l(x)  w_l(x-y)

	sum+=L2scalprod(s1, r1, w);

	sum = sum*(0.5*0.5);

	r1.clear();
	s1.clear();
	r2.clear();
	s2.clear();
	w.clear();

	return sum;

}




//Function which computes an integral of the form
//\int_{x,y} 1/2[ r_k(x) s_k(y) + r_k(y) s_k(x)] 1/2[ w_k(x-y) + w_k(y-x)] 1/2[ r_l(x) s_l(y) - r_l(y) s_l(x)] 1/2[ w_l(x-y) - w_l(y-x)]
double twoElectrons::even_odd_L2scalprod(vec& rk, vec& sk, vec& wk, vec& rl, vec& sl, vec& wl)
{
	vec rev_wk = revert(wk);
	vec rev_wl = revert(wl);

	vec wk_ev = wk + rev_wk;
	wk_ev *= 0.5;

	vec wl_odd = wl - rev_wl;
	wl_odd *=0.5;

	double sum = 0;

	//Calcul de \int_{x,y} 1/2 r_k(x) s_k(y)  w_k(x-y) 1/2 r_l(x)s_l(y) w_l(x-y)
	vec r1 = pointWiseMult(rk,rl);
	vec w = pointWiseMult(wk_ev,wl_odd);
	vec s1 = pointWiseMult(sk,sl);

	sum+=L2scalprod(r1, s1, w);


	//Calcul de \int_{x,y} 1/2 r_k(x) s_k(y) w_k(x-y) 1/2 r_l(y)s_l(x)  w_l(x-y)
	vec r2 = pointWiseMult(rk,sl);
	vec s2 = pointWiseMult(sk,rl);

	sum-=L2scalprod(r2, s2, w);


	//Calcul de \int_{x,y} 1/2 r_k(y) s_k(x)  w_k(x-y) 1/2 r_l(x)s_l(y) w_l(x-y)
	sum+=L2scalprod(s2, r2, w);


	//Calcul de \int_{x,y} 1/2 r_k(y) s_k(x) 1/2 w_k(x-y) 1/2 r_l(y)s_l(x) 1/2 w_l(x-y)
	sum-=L2scalprod(s1, r1, w);


	sum = sum*(0.5*0.5);

	r1.clear();
	s1.clear();
	r2.clear();
	s2.clear();
	w.clear();


	return sum;

}


//Function which computes an integral of the form
//\int_{x,y} 1/2[ r_k(x) s_k(y) - r_k(y) s_k(x)] 1/2[ w_k(x-y) - w_k(y-x)] 1/2[ r_l(x) s_l(y) - r_l(y) s_l(x)] 1/2[ w_l(x-y) - w_l(y-x)]
double twoElectrons::odd_odd_L2scalprod(vec& rk, vec& sk, vec& wk, vec& rl, vec& sl, vec& wl)
{
	vec rev_wk = revert(wk);
	vec rev_wl = revert(wl);

	vec wk_odd = wk - rev_wk;
	wk_odd *= 0.5;

	vec wl_odd = wl - rev_wl;
	wl_odd *=0.5;

	double sum = 0;

	//Calcul de \int_{x,y} 1/2 r_k(x) s_k(y) w_k(x-y) 1/2 r_l(x)s_l(y) w_l(x-y)
	vec r1 = pointWiseMult(rk,rl);
	vec w = pointWiseMult(wk_odd,wl_odd);
	vec s1 = pointWiseMult(sk,sl);

	sum+=L2scalprod(r1, s1, w);


	//Calcul de \int_{x,y} 1/2 r_k(x) s_k(y)  w_k(x-y) 1/2 r_l(y)s_l(x)  w_l(x-y)
	vec r2 = pointWiseMult(rk,sl);
	vec s2 = pointWiseMult(sk,rl);

	sum-=L2scalprod(r2, s2, w);

	//Calcul de \int_{x,y} 1/2 r_k(y) s_k(x) w_k(x-y) 1/2 r_l(x)s_l(y) w_l(x-y)
	sum-=L2scalprod(s2, r2, w);


	//Calcul de \int_{x,y} 1/2 r_k(y) s_k(x) w_k(x-y) 1/2 r_l(y)s_l(x) w_l(x-y)
	sum+=L2scalprod(s1, r1, w);


	sum = sum*(0.5*0.5);

	r1.clear();
	s1.clear();
	r2.clear();
	s2.clear();
	w.clear();


	return sum;

}


//We have a tensor train of the form
//even_part
// \sum_{k,l=1}^re 1/2[ r_k(x) s_l(y) + r_k(y) s_l(x)] 1/2[ w_{kl}(x-y) + w_{kl}(y-x)]
// odd_part
// \sum_{k,l=1}^ro 1/2[ r_k(x) s_l(y) - r_k(y) s_l(x)] 1/2[ w_{kl}(x-y) - w_{kl}(y-x)]

//On ordonne en x,z,y
double twoElectrons::L2scal_even_even(TensorTrain& even_1, TensorTrain& even_2)
{
	assert( (even_1.nVar()==3) && (even_2.nVar()==3));

	double scal = 0;
	for (int i1 = 0; i1 < even_1.ranks(0); i1++)
	{
		vec r1 = even_1.train(0,0,i1);

		for (int j1=0; j1 < even_1.ranks(1); j1++)
		{

			vec w1 = even_1.train(1,i1,j1);
			vec s1 = even_1.train(2,j1,0);

			for (int i2=0; i2 < even_2.ranks(0); i2++)
			{
				vec r2 = even_2.train(0,0,i2);

				for (int j2=0; j2 < even_2.ranks(1); j2++)
				{

					vec w2 = even_2.train(1,i2,j2);
					vec s2 = even_2.train(2,j2,0);

					scal+= even_even_L2scalprod(r1, s1, w1, r2, s2, w2);
          //cout << "i_1 = " << i1 << "  i_2 = " << i2 << "  j_1 =" << j1 << "  j_2 = " << j2 << " scal = " << scal << endl;
				}

			}

		}

	}

	return scal;
}


double twoElectrons::L2scal_even_odd(TensorTrain& even_1, TensorTrain& odd_2)
{

assert( (even_1.nVar()==3) && (odd_2.nVar()==3));

	double scal = 0;
	for (int i1 = 0; i1 < even_1.ranks(0); i1++)
	{
		vec r1 = even_1.train(0,0,i1);

		for (int j1=0; j1 < even_1.ranks(1); j1++)
		{

			vec w1 = even_1.train(1,i1,j1);
			vec s1 = even_1.train(2,j1,0);

			for (int i2=0; i2 < odd_2.ranks(0); i2++)
			{
				vec r2 = odd_2.train(0,0,i2);

				for (int j2=0; j2 < odd_2.ranks(1); j2++)
				{

					vec w2 = odd_2.train(1,i2,j2);
					vec s2 = odd_2.train(2,j2,0);

					scal+= even_odd_L2scalprod(r1, s1, w1, r2, s2, w2);
				}

			}

		}

	}

	return scal;

}

double twoElectrons::L2scal_odd_odd(TensorTrain& odd_1, TensorTrain& odd_2)
{

assert( (odd_1.nVar()==3) && (odd_2.nVar()==3));

	double scal = 0;
	for (int i1 = 0; i1 < odd_1.ranks(0); i1++)
	{
		vec r1 = odd_1.train(0,0,i1);

		for (int j1=0; j1 < odd_1.ranks(1); j1++)
		{

			vec w1 = odd_1.train(1,i1,j1);
			vec s1 = odd_1.train(2,j1,0);

			for (int i2=0; i2 < odd_2.ranks(0); i2++)
			{
				vec r2 = odd_2.train(0,0,i2);

				for (int j2=0; j2 < odd_2.ranks(1); j2++)
				{

					vec w2 = odd_2.train(1,i2,j2);
					vec s2 = odd_2.train(2,j2,0);

					scal+= odd_odd_L2scalprod(r1, s1, w1, r2, s2, w2);
				}

			}

		}

	}

	return scal;

}


double twoElectrons::squareL2norm(TensorTrain& even_part, TensorTrain& odd_part)
{
  //cout << "even even = " << L2scal_even_even(even_part, even_part) << endl;
  //cout << "odd odd = " << L2scal_odd_odd(odd_part, odd_part) << endl;
  //cout << "even odd = " << L2scal_even_odd(even_part, odd_part) << endl;
	return L2scal_even_even(even_part, even_part) + L2scal_odd_odd(odd_part, odd_part) + 2*L2scal_even_odd(even_part, odd_part);
}


double twoElectrons::L2scal(TensorTrain& even_part1, TensorTrain& odd_part1, TensorTrain& even_part2, TensorTrain& odd_part2)
{
  double ee = L2scal_even_even(even_part1, even_part2);
  cout << "ee = " << ee << endl;
  double eo = L2scal_even_odd(even_part1, odd_part2);
  cout << "eo = " << eo << endl;
  double oe = L2scal_even_odd(even_part2, odd_part1);
  cout << "oe = " << oe << endl;
  double oo = L2scal_odd_odd(odd_part1, odd_part2);
  cout << "oo = " << oo << endl;

  return ee + eo + oe + oo;
	//return L2scal_even_even(even_part1, even_part2) + L2scal_odd_odd(odd_part1, odd_part2) + L2scal_even_odd(even_part1, odd_part2) + L2scal_even_odd(even_part2, odd_part1);
}




// -- FUNCTIONS to Compute the Fundamental State: --


/* function to solve one step of the heat equation for a 2x2 block system
  - input: c, u at previous time step, the current u
  - output: modify the current u
*/
void twoElectrons::solve_min_res_heat(const double& dt, const double& c, TensorTrain& uold_even, TensorTrain& uold_odd, TensorTrain& unew_even, TensorTrain& unew_odd){
  double tol_sol = 1.0e-6;
  double tol_res = 1.0e-4;
  const unsigned int nIt_Max = 5;

  unew_even.copyTensorFrom(uold_even);
  unew_odd.copyTensorFrom(uold_odd);

  mat L_i;
  L_i.copyMatFrom(m_Laplacian);
  //L_i *= dt;
  L_i += m_masse;

  mat P_i;
  P_i.copyMatFrom(m_Potxy);
  //P_i *= (-dt);
  P_i += m_masse;

  mat C_i;
  C_i.copyMatFrom(m_Coulombz);
  //C_i *= dt;
  C_i += m_masse;

  // residual of the even part;
  TensorTrain residual_even;
  residual_even.copyTensorFrom(uold_even);
  TensorTrain I_u_e;
  I_u_e.copyTensorFrom(unew_even);
  I_u_e *= (-1.0 + c*dt);
  TensorTrain H_u_e = m_H0_even * unew_even;
  H_u_e *= (-dt);
  I_u_e += H_u_e;
  residual_even += I_u_e;
  TensorTrain H_u_o = m_H0_odd * unew_odd;
  H_u_o *= (-dt);
  residual_even += H_u_o;
  round_asvd(residual_even, tol_res);

  // free the memory:
  H_u_e.clear();
  I_u_e.clear();
  H_u_o.clear();


  // residual of the odd part:
  TensorTrain residual_odd;
  residual_odd.copyTensorFrom(uold_odd);
  TensorTrain I_u_e_2;
  I_u_e_2.copyTensorFrom(unew_odd);
  I_u_e_2 *= (-1.0 + c*dt);
  TensorTrain H_u_e_2 = m_H0_even * unew_odd;
  H_u_e_2 *= (-dt);
  I_u_e_2 += H_u_e_2;
  residual_odd += I_u_e_2;
  TensorTrain H_u_o_2 = m_H0_odd * unew_even;
  H_u_o_2 *= (-dt);
  residual_odd += H_u_o_2;
  round_asvd(residual_odd, tol_res);


  // free the memory:
  H_u_e_2.clear();
  I_u_e_2.clear();
  H_u_o_2.clear();


  // Compute the residual norm:
  double res_up = residual_even * residual_even;
  double res_down = residual_odd * residual_odd;
  double resNorm = res_up + res_down;
  resNorm = sqrt(resNorm);
  //double resNorm = sqrt( squareL2norm(residual_even, residual_odd) );
  //cout << "Res Norm Up = " << sqrt(res_up) << " and Res Norm Down = " << sqrt(res_down) <<endl;
  cout << "Norm of the residual = " << resNorm << endl;
  double resNorm_0 = resNorm;

  unsigned int it = 0;
  while( (resNorm>1.0e-2 * resNorm_0) && (it<nIt_Max) ){
    // compute A_i^{-1} b
    // 1 -- Even part --

    // Laplacian:
    TensorTrain y_0_e;
    y_0_e.copyTensorFrom(unew_even);
    for(unsigned int j=0; j<unew_even.trainSize(0,1); j++){
      vec b_j = unew_even.train(0,0,j);
      vec x_j = L_i/b_j;
      y_0_e.train(0,0,j).clear();
      y_0_e.set_train(0,0,j,x_j);
    }

    TensorTrain y_1_e;
    y_1_e.copyTensorFrom(unew_even);
    for(unsigned int i=0; i<unew_even.trainSize(2,0); i++){
      vec b_j = unew_even.train(2,i,0);
      vec x_j = L_i/b_j;
      y_1_e.train(2,i,0).clear();
      y_1_e.set_train(2,i,0,x_j);
    }

    TensorTrain y_2_e;
    y_2_e.copyTensorFrom(unew_even);
    for(unsigned int i=0; i<unew_even.trainSize(1,0); i++){
      for(unsigned int j=0; j<unew_even.trainSize(1,1); j++){
        vec b_j = unew_even.train(1,i,j);
        vec x_j = L_i/b_j;
        y_2_e.train(1,i,j).clear();
        y_2_e.set_train(1,i,j,x_j);
      }
    }

    // Potential:
    TensorTrain y_3_e;
    y_3_e.copyTensorFrom(unew_even);
    for(unsigned int j=0; j<unew_even.trainSize(0,1); j++){
      vec b_j = unew_even.train(0,0,j);
      vec x_j = P_i/b_j;
      y_3_e.train(0,0,j).clear();
      y_3_e.set_train(0,0,j,x_j);
    }

    TensorTrain y_4_e;
    y_4_e.copyTensorFrom(unew_even);
    for(unsigned int i=0; i<unew_even.trainSize(2,0); i++){
      vec b_j = unew_even.train(2,i,0);
      vec x_j = P_i/b_j;
      y_4_e.train(2,i,0).clear();
      y_4_e.set_train(2,i,0,x_j);
    }

    // Coulomb interaction:
    TensorTrain y_5_e;
    y_5_e.copyTensorFrom(unew_even);
    for(unsigned int i=0; i<unew_even.trainSize(1,0); i++){
      for(unsigned int j=0; j<unew_even.trainSize(1,1); j++){
        vec b_j = unew_even.train(1,i,j);
        vec x_j = C_i/b_j;
        y_5_e.train(1,i,j).clear();
        y_5_e.set_train(1,i,j,x_j);
      }
    }


    // 2 -- Odd part --
    // Laplacian:
    TensorTrain y_0_o;
    y_0_o.copyTensorFrom(unew_odd);
    for(unsigned int j=0; j<unew_odd.trainSize(0,1); j++){
      vec b_j = unew_odd.train(0,0,j);
      vec x_j = L_i/b_j;
      y_0_o.train(0,0,j).clear();
      y_0_o.set_train(0,0,j,x_j);
    }

    TensorTrain y_1_o;
    y_1_o.copyTensorFrom(unew_odd);
    for(unsigned int i=0; i<unew_odd.trainSize(2,0); i++){
      vec b_j = unew_odd.train(2,i,0);
      vec x_j = L_i/b_j;
      y_1_o.train(2,i,0).clear();
      y_1_o.set_train(2,i,0,x_j);
    }

    TensorTrain y_2_o;
    y_2_o.copyTensorFrom(unew_odd);
    for(unsigned int i=0; i<unew_odd.trainSize(1,0); i++){
      for(unsigned int j=0; j<unew_odd.trainSize(1,1); j++){
        vec b_j = unew_odd.train(1,i,j);
        vec x_j = L_i/b_j;
        y_2_o.train(1,i,j).clear();
        y_2_o.set_train(1,i,j,x_j);
      }
    }

    // Potential:
    TensorTrain y_3_o;
    y_3_o.copyTensorFrom(unew_odd);
    for(unsigned int j=0; j<unew_odd.trainSize(0,1); j++){
      vec b_j = unew_odd.train(0,0,j);
      vec x_j = P_i/b_j;
      y_3_o.train(0,0,j).clear();
      y_3_o.set_train(0,0,j,x_j);
    }

    TensorTrain y_4_o;
    y_4_o.copyTensorFrom(unew_odd);
    for(unsigned int i=0; i<unew_odd.trainSize(2,0); i++){
      vec b_j = unew_odd.train(2,i,0);
      vec x_j = P_i/b_j;
      y_4_o.train(2,i,0).clear();
      y_4_o.set_train(2,i,0,x_j);
    }

    // Coulomb interaction:
    TensorTrain y_5_o;
    y_5_o.copyTensorFrom(unew_odd);
    for(unsigned int i=0; i<unew_odd.trainSize(1,0); i++){
      for(unsigned int j=0; j<unew_odd.trainSize(1,1); j++){
        vec b_j = unew_odd.train(1,i,j);
        vec x_j = C_i/b_j;
        y_5_o.train(1,i,j).clear();
        y_5_o.set_train(1,i,j,x_j);
      }
    }

    // Define the system matrix:
    vector<TensorTrain> V_e = {y_0_e,y_1_e,y_2_e,y_3_e,y_4_e,y_5_e,unew_even,residual_even};
    vector<TensorTrain> V_o = {y_0_o,y_1_o,y_2_o,y_3_o,y_4_o,y_5_o,unew_odd,residual_odd};
    vector<TensorTrain> T_up(16);
    vector<TensorTrain> T_down(16);


    // even-even block:
    for(unsigned int iRow=0; iRow<8; iRow++){
      TensorTrain t_i = m_H0_even * V_e[iRow];
      t_i *= dt;
      TensorTrain s_i;
      s_i.copyTensorFrom(V_e[iRow]);
      s_i *= (1.0-dt*c);
      t_i += s_i;
      s_i.clear();
      //cout << "iRow = " << iRow << " ||t_i||^2 = " << t_i*t_i << endl;
      T_up[iRow] = t_i;
      // end of iRow:
    }

    // odd-odd block:
    for(unsigned int iRow=8; iRow<16; iRow++){
      unsigned int i = iRow-8;
      TensorTrain t_i = m_H0_even * V_o[i];
      t_i *= dt;
      TensorTrain s_i;
      s_i.copyTensorFrom(V_o[i]);
      s_i *= (1.0-dt*c);
      t_i += s_i;
      s_i.clear();
      T_down[iRow] = t_i;
    }

    // even-odd and odd-even blocks:
    // odd-odd block:
    for(unsigned int iRow=0; iRow<8; iRow++){
      TensorTrain t_i = m_H0_odd * V_e[iRow];
      t_i *= dt;
      T_down[iRow] = t_i;
      TensorTrain s_i = m_H0_odd * V_o[iRow];
      s_i *= dt;
      T_up[iRow+8] = s_i;
    }
    cout << "T constructed...entering least_squares_block" << endl;

    // optional rounding step prior of entering the least_squares_block:
    for(unsigned int i=0; i<T_up.size(); i++){
      round_asvd(T_up[i], tol_res);
      round_asvd(T_down[i], tol_res);
    }

    vec sol_coeff;
    least_squares_block(T_up, T_down, uold_even, uold_odd, sol_coeff);

    cout << "Assembling the solutions" << endl;

    TensorTrain u_up_even;
    u_up_even.copyTensorFrom(residual_even);
    u_up_even *= sol_coeff(7);
    V_e[7].clear();
    for(unsigned int i=0; i<7; i++){
      V_e[i] *= sol_coeff(i);
      u_up_even += V_e[i];
      V_e[i].clear();
    }

    unew_even.copyTensorFrom(u_up_even);
    //unew_even = u_up_even;
    u_up_even.clear();
    round_asvd(unew_even, tol_sol); // ROUNDING STEP !

    TensorTrain u_up_odd;
    u_up_odd.copyTensorFrom(residual_odd);
    u_up_odd *= sol_coeff(15);
    V_o[7].clear();
    for(unsigned int i=0; i<7; i++){
      V_o[i] *= sol_coeff(i+8);
      u_up_odd += V_o[i];
      V_o[i].clear();
    }

    unew_odd.copyTensorFrom(u_up_odd);
    //unew_odd = u_up_odd;
    u_up_odd.clear();
    round_asvd(unew_odd, tol_sol); // ROUNDING STEP !

    // free the memory:
    for(unsigned int i=0; i<6; i++){
      V_e[i].clear();
      V_o[i].clear();
    }
    for(unsigned int i=0; i<T_up.size(); i++){
      T_up[i].clear();
      T_down[i].clear();
    }

    // round and compute the new residual
    //TensorTrain residual_even;
    residual_even.copyTensorFrom(uold_even);
    TensorTrain I_u_e;
    I_u_e.copyTensorFrom(unew_even);
    I_u_e *= (-1.0 + c*dt);
    TensorTrain H_u_e = m_H0_even * unew_even;
    H_u_e *= (-dt);
    I_u_e += H_u_e;
    residual_even += I_u_e;
    TensorTrain H_u_o = m_H0_odd * unew_odd;
    H_u_o *= (-dt);
    residual_even += H_u_o;
    round_asvd(residual_even, tol_res); // ROUNDING STEP !


    // free the memory:
    H_u_e.clear();
    I_u_e.clear();
    H_u_o.clear();

    // residual of the odd part:
    //TensorTrain residual_odd;
    residual_odd.copyTensorFrom(uold_odd);
    TensorTrain I_u_e_2;
    I_u_e_2.copyTensorFrom(unew_odd);
    I_u_e_2 *= (-1.0 + c*dt);
    TensorTrain H_u_e_2 = m_H0_even * unew_odd;
    H_u_e_2 *= (-dt);
    I_u_e_2 += H_u_e_2;
    residual_odd += I_u_e_2;
    TensorTrain H_u_o_2 = m_H0_odd * unew_even;
    H_u_o_2 *= (-dt);
    residual_odd += H_u_o_2;
    round_asvd(residual_odd, tol_res); // ROUNDING STEP !


    // free the memory:
    H_u_e_2.clear();
    I_u_e_2.clear();
    H_u_o_2.clear();

    res_down = residual_odd * residual_odd;
    res_up  = residual_even * residual_even;
    resNorm = sqrt(res_up + res_down);
    //resNorm = sqrt( squareL2norm(residual_even, residual_odd) );
    //cout << "Res Norm Up = " << sqrt(res_up) << " and Res Norm Down = " << sqrt(res_down) <<endl;
    cout << "Norm of the residual = " << resNorm << endl;
    it += 1;
    //exit(1);
  }
  residual_even.clear();
  residual_odd.clear();
}




void twoElectrons::heat_onetimestepresolution(const double& dt, const double& c, TensorTrain& uold_even, TensorTrain& uold_odd, TensorTrain& unew_even, TensorTrain& unew_odd)
{
  /*cout << "Ranks of even: " << endl;
  uold_even.print_ttSize();

  cout << "Ranks of odd: " << endl;
  uold_odd.print_ttSize();*/

	TensorTrain rhs_even = m_H0_odd*uold_odd;
	rhs_even *= (-dt);
	rhs_even += uold_even;

	TensorTrain rhs_odd = m_H0_odd*uold_even;
	rhs_odd *= (-dt);
	rhs_odd += uold_odd;

  /*cout << "Ranks of rhs even: " << endl;
  rhs_even.print_ttSize();

  cout << "Ranks of rhs odd: " << endl;
  rhs_odd.print_ttSize();*/

  solve_min_res_heat(dt, c, uold_even, uold_odd, unew_even, unew_odd);
}


void twoElectrons::heat_dynamicsresolution(const double& dt, TensorTrain& u0_even, TensorTrain& u0_odd, const int& NT)
{
	m_sol_real_even.resize(NT+1);
  m_sol_real_odd.resize(NT+1);

	m_sol_real_even[0] = u0_even;
	m_sol_real_odd[0] = u0_odd;

  save_1d_wave_fun_vtk("./Applications/Schroedinger/waveFun", 0);

	TensorTrain uold_even;
	uold_even.copyTensorFrom(u0_even);

	TensorTrain uold_odd;
	uold_odd.copyTensorFrom(u0_odd);



	for (int n=0; n<NT; n++)
	{
    cout << "Time iteration: " << n << endl;
    m_sol_real_even[n].print_ttSize();
    m_sol_real_odd[n].print_ttSize();

    TensorTrain unew_odd;
    TensorTrain unew_even;

		//*******Calcul de c *******///
		//double l2u= squareL2norm(uold_even, uold_odd);
    double l2u= squareL2norm(m_sol_real_even[n], m_sol_real_odd[n]);
    cout << "CHECK = " << l2u << endl;
    m_sol_real_even[n] *= ( 1.0/sqrt(l2u) );
    m_sol_real_odd[n] *=  ( 1.0/sqrt(l2u) );
    l2u= squareL2norm(m_sol_real_even[n], m_sol_real_odd[n]);
    cout << "CHECK = " << l2u << endl;


		//TensorTrain Hu_even = m_H0_even*uold_even;
    TensorTrain Hu_even = m_H0_even*m_sol_real_even[n];
		//TensorTrain beven = m_H0_odd*uold_odd;
    TensorTrain beven = m_H0_odd*m_sol_real_odd[n];
		Hu_even +=  beven;
    round_asvd(Hu_even, 1.0e-6);

		//TensorTrain Hu_odd = m_H0_even*uold_odd;
    TensorTrain Hu_odd = m_H0_even*m_sol_real_odd[n];
		//TensorTrain bodd = m_H0_odd*uold_even;
    TensorTrain bodd = m_H0_odd*m_sol_real_even[n];
		Hu_odd +=  bodd;
    round_asvd(Hu_odd, 1.0e-6);


		//double uHu = L2scal(uold_even, uold_odd, Hu_even, Hu_odd);
    double uHu = L2scal(m_sol_real_even[n], m_sol_real_odd[n], Hu_even, Hu_odd);
		double c = uHu/l2u;
    cout << "Value l2u = " << l2u << endl;
    cout << "Value uHu = " << uHu << endl;
    cout << "Value of c = " << c << endl;


		//heat_onetimestepresolution(dt,c, uold_even,uold_odd,unew_even, unew_odd);
    heat_onetimestepresolution(dt,c, m_sol_real_even[n],m_sol_real_odd[n],unew_even, unew_odd);

		//m_sol_real_even[n+1] = unew_even;
		//m_sol_real_odd[n+1] = unew_odd;
    m_sol_real_even[n+1].copyTensorFrom(unew_even);
    m_sol_real_odd[n+1].copyTensorFrom(unew_odd);

    // clearing the memory
    unew_even.clear();
    unew_odd.clear();
    Hu_even.clear();
    Hu_odd.clear();
    beven.clear();
    bodd.clear();

    m_sol_real_even[n+1].print_ttSize();
    m_sol_real_odd[n+1].print_ttSize();

    save_1d_wave_fun_vtk("./Applications/Schroedinger/waveFun", n+1);

		//uold_even.copyTensorFrom(unew_even);
		//uold_odd.copyTensorFrom(unew_odd);
    cout << "Ending iteration." << endl;
	}

}


/* function to solve one step of the Schrödinger equation for a 2x2 block system
  - input: c, u at previous time step, the current u
  - output: modify the current u
*/
void twoElectrons::solve_min_res_schroe(const double& dt, TensorTrain& uold_even_real, TensorTrain& uold_odd_real, TensorTrain& uold_even_im, TensorTrain& uold_odd_im, TensorTrain& unew_even_real, TensorTrain& unew_odd_real, TensorTrain& unew_even_im, TensorTrain& unew_odd_im)
{
  double tol_sol = 1.0e-6;
  double tol_res = 1.0e-6;

  /*unew_even_real.copyTensorFrom(uold_even_real);
  unew_odd_real.copyTensorFrom(uold_odd_real);
  unew_even_im.copyTensorFrom(uold_even_im);
  unew_odd_im.copyTensorFrom(uold_odd_im);*/

  const unsigned int nComp = 4;
  vector<TensorTrain> g(nComp);

  // first component
  g[0].copyTensorFrom(uold_even_real);
  TensorTrain tmp1 = m_H0_even * uold_even_im;
  round_asvd(tmp1,tol_sol);
  tmp1*= (0.5*dt);
  g[0] += tmp1;
  tmp1.clear();
  TensorTrain tmp2 = m_H0_odd * uold_odd_im;
  round_asvd(tmp2,tol_sol);
  tmp2*= (0.5*dt);
  g[0] += tmp2;
  tmp2.clear();
  round_asvd(g[0],tol_sol);

  // second component
  g[1].copyTensorFrom(uold_odd_real);
  TensorTrain tmp3 = m_H0_even * uold_odd_im;
  round_asvd(tmp3,tol_sol);
  tmp3*= (0.5*dt);
  g[1] += tmp3;
  tmp3.clear();
  TensorTrain tmp4 = m_H0_odd * uold_even_im;
  round_asvd(tmp4,tol_sol);
  tmp4*= (0.5*dt);
  g[1] += tmp4;
  tmp4.clear();
  round_asvd(g[1],tol_sol);


  // third component
  g[2].copyTensorFrom(uold_even_im);
  TensorTrain tmp5 = m_H0_even * uold_even_real;
  round_asvd(tmp5,tol_sol);
  tmp5*= (-0.5*dt);
  g[2] += tmp5;
  tmp5.clear();
  TensorTrain tmp6 = m_H0_odd * uold_odd_real;
  round_asvd(tmp6,tol_sol);
  tmp6*= (-0.5*dt);
  g[2] += tmp6;
  tmp6.clear();
  round_asvd(g[2],tol_sol);

  // second component
  g[3].copyTensorFrom(uold_odd_im);
  TensorTrain tmp7 = m_H0_even * uold_odd_real;
  round_asvd(tmp7,tol_sol);
  tmp7*= (-0.5*dt);
  g[3] += tmp7;
  tmp7.clear();
  TensorTrain tmp8 = m_H0_odd * uold_even_real;
  round_asvd(tmp8,tol_sol);
  tmp8*= (-0.5*dt);
  g[3] += tmp8;
  tmp8.clear();
  round_asvd(g[3],tol_sol);

  cout << "g term computed." << endl;

  vector<vector<TensorTrain> > krylSpace;

  operatorTensor zero;
  zero.set_zero();
  blockOperatorTensor A(4);

  cout << "A block initialised." << endl;
  // first row:
  A.set_A(0,0,m_Id); A.set_coeffs(0,0,1.0);
  A.set_A(0,1,zero); A.set_coeffs(0,1,0.0);
  A.set_A(0,2,m_H0_even); A.set_coeffs(0,2,-0.5*dt);
  A.set_A(0,3,m_H0_odd); A.set_coeffs(0,3,-0.5*dt);
  // second row:
  A.set_A(1,0,zero); A.set_coeffs(1,0,0.0);
  A.set_A(1,1,m_Id); A.set_coeffs(1,1,1.0);
  A.set_A(1,2,m_H0_odd); A.set_coeffs(1,2,-0.5*dt);
  A.set_A(1,3,m_H0_even); A.set_coeffs(1,3,-0.5*dt);
  // third row:
  A.set_A(2,0,m_H0_even); A.set_coeffs(2,0,0.5*dt);
  A.set_A(2,1,m_H0_odd); A.set_coeffs(2,1,0.5*dt);
  A.set_A(2,2,m_Id); A.set_coeffs(2,2,1.0);
  A.set_A(2,3,zero); A.set_coeffs(2,3,0.0);
  // fourth row:
  A.set_A(3,0,m_H0_odd); A.set_coeffs(3,0,0.5*dt);
  A.set_A(3,1,m_H0_even); A.set_coeffs(3,1,0.5*dt);
  A.set_A(3,2,zero); A.set_coeffs(3,2,0.0);
  A.set_A(3,3,m_Id); A.set_coeffs(3,3,1.0);

  cout << "A block computed." << endl;

  krylovBuild(g, A, 2, tol_sol, krylSpace);


  exit(1);

  // On commence par calculer un guess avec Runge-Kutta2-Heun
  //Step 1:
  /*TensorTrain unew_even_real_aux1, unew_odd_real_aux1, unew_even_im_aux1, unew_odd_im_aux1;
  unew_even_real_aux1.copyTensorFrom(uold_even_real);
  unew_odd_real_aux1.copyTensorFrom(uold_odd_real);
  unew_even_im_aux1.copyTensorFrom(uold_even_im);
  unew_odd_im_aux1.copyTensorFrom(uold_odd_im);

  cout << "beginning" << endl;
  TensorTrain H_u_er1 = m_H0_even * uold_even_im;
  round_asvd(H_u_er1, tol_sol);

  cout << "asvd done" << endl;
  H_u_er1 *= (0.5*dt);
  //unew_even_real_aux1 += H_u_er1;
  round_sum_asvd(unew_even_real_aux1, H_u_er1, tol_sol);
  cout << "sum asvd done" << endl;

  TensorTrain H_u_or1 = m_H0_odd * uold_odd_im;
  round_asvd(H_u_or1, tol_sol);
  H_u_or1 *= (0.5*dt);

  //unew_even_real_aux1 += H_u_or1;
  round_sum_asvd(unew_even_real_aux1, H_u_or1, tol_sol);

  cout << "Testing the asvd:" << endl;
  unew_even_real_aux1.print_ttSize();
  round_asvd(unew_even_real_aux1, tol_sol);
  unew_even_real_aux1.print_ttSize();
  cout << "done." << endl;

  // free the memory:
  H_u_er1.clear();
  H_u_or1.clear();

  TensorTrain H_u_er2 = m_H0_even * uold_odd_im;
  round_asvd(H_u_er2, tol_sol);
  H_u_er2 *= (0.5*dt);
  //unew_odd_real_aux1 += H_u_er2;
  round_sum_asvd(unew_odd_real_aux1, H_u_er2, tol_sol);

  TensorTrain H_u_or2 = m_H0_odd * uold_even_im;
  round_asvd(H_u_or2, tol_sol);
  H_u_or2 *= (0.5*dt);
  //unew_odd_real_aux1 += H_u_or2;
  round_sum_asvd(unew_odd_real_aux1, H_u_or2, tol_sol);

  cout << "Testing the asvd:" << endl;
  unew_odd_real_aux1.print_ttSize();
  round_asvd(unew_odd_real_aux1, tol_res);
  unew_odd_real_aux1.print_ttSize();
  cout << "done." << endl;

  // free the memory:
  H_u_er2.clear();
  H_u_or2.clear();


  TensorTrain H_u_er3 = m_H0_even * uold_even_real;
  round_asvd(H_u_er3, tol_sol);
  H_u_er3 *= (-0.5*dt);
  //unew_even_im_aux1 += H_u_er3;
  round_sum_asvd(unew_even_im_aux1, H_u_er3, tol_sol);

  TensorTrain H_u_or3 = m_H0_odd * uold_odd_real;
  round_asvd(H_u_or3, tol_sol);
  H_u_or3 *= (-0.5*dt);
  //unew_even_im_aux1 += H_u_or3;
  round_sum_asvd(unew_even_im_aux1, H_u_or3, tol_sol);

  round_asvd(unew_even_im_aux1, tol_res);

  // free the memory:
  H_u_er3.clear();
  H_u_or3.clear();

  TensorTrain H_u_er4 = m_H0_even * uold_odd_real;
  round_asvd(H_u_er4, tol_sol);
  H_u_er4 *= (-0.5*dt);
  //unew_odd_im_aux1 += H_u_er4;
  round_sum_asvd(unew_odd_im_aux1, H_u_er4, tol_sol);
  round_asvd(unew_odd_im_aux1, tol_sol);

  TensorTrain H_u_or4 = m_H0_odd * uold_even_real;
  round_asvd(H_u_or4, tol_sol);
  H_u_or4 *= (-0.5*dt);
  //unew_odd_im_aux1 += H_u_or4;
  round_sum_asvd(unew_odd_im_aux1, H_u_or4, tol_sol);
  round_asvd(unew_odd_im_aux1, tol_res);


  // free the memory:
  H_u_er4.clear();
  H_u_or4.clear();

  cout << "Auxiliary 1 done." << endl;



  //Step 2:
  TensorTrain unew_even_real_aux, unew_odd_real_aux, unew_even_im_aux, unew_odd_im_aux;
  unew_even_real_aux.copyTensorFrom(uold_even_real);
  unew_odd_real_aux.copyTensorFrom(uold_odd_real);
  unew_even_im_aux.copyTensorFrom(uold_even_im);
  unew_odd_im_aux.copyTensorFrom(uold_odd_im);
  cout << "tensor copied." << endl;

  TensorTrain H_u_e_1 = m_H0_even * unew_even_im_aux1;
  round_asvd(H_u_e_1, tol_sol);
  H_u_e_1 *= (dt);
  //unew_even_real_aux += H_u_e_1;
  round_sum_asvd(unew_even_real_aux,H_u_e_1, tol_sol);
  round_asvd(unew_even_real_aux, tol_sol);

  cout << "1)" << endl;
  TensorTrain H_u_o_1 = m_H0_odd * unew_odd_im_aux1;
  round_asvd(H_u_o_1, tol_sol);
  H_u_o_1 *= (dt);
  //unew_even_real_aux += H_u_o_1;
  cout << "2)" << endl;
  unew_even_real_aux.print_ttSize();
  round_sum_asvd(unew_even_real_aux,H_u_o_1 ,tol_sol);
  round_asvd(unew_even_real_aux, tol_sol);
  cout << "asvd" << endl;
  //unew_even_real_aux.print_ttSize();

  // free the memory:
  H_u_e_1.clear();
  H_u_o_1.clear();
  cout << "clear" << endl;

  TensorTrain H_u_e_2 = m_H0_even * unew_odd_im_aux1;
  round_asvd(H_u_e_2, tol_sol);
  H_u_e_2 *= (dt);
  //unew_odd_real_aux += H_u_e_2;
  round_sum_asvd(unew_odd_real_aux,H_u_e_2 ,tol_sol);
  round_asvd(unew_odd_real_aux, tol_sol);

  cout << "3)" << endl;
  TensorTrain H_u_o_2 = m_H0_odd * unew_even_im_aux1;
  round_asvd(H_u_o_2, tol_sol);
  H_u_o_2 *= (dt);
  //unew_odd_real_aux += H_u_o_2;
  cout << "4)" << endl;
  round_sum_asvd(unew_odd_real_aux,H_u_o_2, tol_sol);
  round_asvd(unew_odd_real_aux, tol_sol);
  cout << "asvd" << endl;

  // free the memory:
  H_u_e_2.clear();
  H_u_o_2.clear();
  cout << "clear" << endl;


  TensorTrain H_u_e_3 = m_H0_even * unew_even_real_aux1;
  round_asvd(H_u_e_3, tol_sol);
  H_u_e_3 *= (-dt);
  //unew_even_im_aux += H_u_e_3;
  round_sum_asvd(unew_even_im_aux,H_u_e_3, tol_sol);
  round_asvd(unew_even_im_aux, tol_sol);
  cout << "5)" << endl;
  TensorTrain H_u_o_3 = m_H0_odd * unew_odd_real_aux1;
  round_asvd(H_u_o_3, tol_sol);
  H_u_o_3 *= (-dt);
  //unew_even_im_aux += H_u_o_3;
  round_sum_asvd(unew_even_im_aux,H_u_o_3, tol_sol);
  round_asvd(unew_even_im_aux, tol_sol);
  cout << "6)" << endl;
  //round_asvd(unew_even_im_aux, tol_sol);
  cout << "asvd" << endl;

  // free the memory:
  H_u_e_3.clear();
  H_u_o_3.clear();

  TensorTrain H_u_e_4 = m_H0_even * unew_odd_real_aux1;
  round_asvd(H_u_e_4, tol_sol);
  H_u_e_4 *= (-dt);
  //unew_odd_im_aux += H_u_e_4;
  round_sum_asvd(unew_odd_im_aux,H_u_e_4, tol_sol);
  round_asvd(unew_odd_im_aux, tol_sol);
  cout << "7)" << endl;
  TensorTrain H_u_o_4 = m_H0_odd * unew_even_real_aux1;
  round_asvd(H_u_o_4, tol_sol);
  H_u_o_4 *= (-dt);
  unew_odd_im_aux += H_u_o_4;
  cout << "8)" << endl;
  round_sum_asvd(unew_odd_im_aux, H_u_o_4, tol_sol);
  round_asvd(unew_odd_im_aux, tol_sol);
  cout << "asvd" << endl;

  // free the memory:
  H_u_e_4.clear();
  H_u_o_4.clear();

  cout << "Guess done." << endl;

  TensorTrain rhs_even_real, rhs_odd_real, rhs_even_im, rhs_odd_im;
  rhs_even_real.copyTensorFrom(unew_even_real_aux1);
  rhs_odd_real.copyTensorFrom(unew_odd_real_aux1);
  rhs_even_im.copyTensorFrom(unew_even_im_aux1);
  rhs_odd_im.copyTensorFrom(unew_odd_im_aux1);

  TensorTrain Re1;
  Re1.copyTensorFrom(unew_even_real_aux);
  Re1 *= (-1.0);
  rhs_even_real += Re1;
  TensorTrain Se1 = m_H0_even * unew_even_im_aux;
  Se1 *= (0.5*dt);
  rhs_even_real += Se1;
  TensorTrain Te1 = m_H0_odd * unew_odd_im_aux;
  Te1 *= (0.5*dt);
  rhs_even_real += Te1;
  round_asvd(rhs_even_real, tol_res);

  // free the memory:
  Re1.clear();
  Se1.clear();
  Te1.clear();


  TensorTrain Re2;
  Re2.copyTensorFrom(unew_odd_real_aux);
  Re2 *= (-1.0);
  rhs_odd_real += Re2;
  TensorTrain Se2 = m_H0_even * unew_odd_im_aux;
  Se2 *= (0.5*dt);
  rhs_odd_real += Se2;
  TensorTrain Te2 = m_H0_odd * unew_even_im_aux;
  Te2 *= (0.5*dt);
  rhs_odd_real += Te2;
  round_asvd(rhs_odd_real, tol_res);

  // free the memory:
  Re2.clear();
  Se2.clear();
  Te2.clear();

  TensorTrain Re3;
  Re3.copyTensorFrom(unew_even_im_aux);
  Re3 *= (-1.0);
  rhs_even_im += Re3;
  TensorTrain Se3 = m_H0_even * unew_even_real_aux;
  Se3 *= (-0.5*dt);
  rhs_even_im += Se3;
  TensorTrain Te3 = m_H0_odd * unew_odd_real_aux;
  Te3 *= (-0.5*dt);
  rhs_even_im += Te3;
  round_asvd(rhs_even_im, tol_res);

  // free the memory:
  Re3.clear();
  Se3.clear();
  Te3.clear();


  TensorTrain Re4;
  Re4.copyTensorFrom(unew_odd_im_aux);
  Re4 *= (-1.0);
  rhs_odd_im += Re4;
  TensorTrain Se4 = m_H0_even * unew_odd_real_aux;
  Se4 *= (-0.5*dt);
  rhs_odd_im += Se4;
  TensorTrain Te4 = m_H0_odd * unew_even_real_aux;
  Te4 *= (-0.5*dt);
  rhs_odd_im += Te4;
  round_asvd(rhs_odd_im, tol_res);

  // free the memory:
  Re4.clear();
  Se4.clear();
  Te4.clear();

  cout << "Rhs done." << endl;
  // RHS created!!
  */

  // solve the system:






  /*
 // Residual norm 0

  TensorTrain residual_even_real, residual_odd_real, residual_even_im, residual_odd_im;
  residual_even_real.copyTensorFrom(rhs_even_real);
  residual_odd_real.copyTensorFrom(rhs_odd_real);
  residual_even_im.copyTensorFrom(rhs_even_im);
  residual_odd_im.copyTensorFrom(rhs_odd_im);


   double res_even_real = residual_even_real * residual_even_real;
   double res_odd_real = residual_odd_real * residual_odd_real;
   double res_even_im = residual_even_im * residual_even_im;
   double res_odd_im = residual_odd_im * residual_odd_im;

   double resNorm = sqrt(res_even_real + res_odd_real + res_even_im + res_odd_im);
  cout << "Res Norm Even Real = " << sqrt(res_even_real) << "    Res Norm Odd Real = " << sqrt(res_odd_real)  << "   Res Norm Even Im = " << sqrt(res_even_im) << " Res Norm Odd Im = " << sqrt(res_odd_im)<<endl;
  cout << "Norm of the residual = " << resNorm << endl;

  residual_even_real.clear();
  residual_odd_real.clear();
  residual_even_im.clear();
  residual_odd_im.clear();

  rhs_even_real.clear();
  rhs_odd_real.clear();
  rhs_even_im.clear();
  rhs_odd_im.clear();

  cout << "Copy tensors" << endl;
  unew_even_real.copyTensorFrom(unew_even_real_aux);
  unew_odd_real.copyTensorFrom(unew_odd_real_aux);
  unew_even_im.copyTensorFrom(unew_even_im_aux);
  unew_odd_im.copyTensorFrom(unew_odd_im_aux);
  cout << "iteration done." << endl;

  unew_even_real_aux.clear();
  unew_odd_real_aux.clear();
  unew_even_im_aux.clear();
  unew_odd_im_aux.clear();

  unew_even_real_aux1.clear();
  unew_odd_real_aux1.clear();
  unew_even_im_aux1.clear();
  unew_odd_im_aux1.clear();*/

}

void twoElectrons::schroe_onetimestepresolution(const double& dt, TensorTrain& uold_even_real, TensorTrain& uold_odd_real, TensorTrain& uold_even_im, TensorTrain& uold_odd_im, TensorTrain& unew_even_real, TensorTrain& unew_odd_real, TensorTrain& unew_even_im, TensorTrain& unew_odd_im)
{
  solve_min_res_schroe(dt, uold_even_real, uold_odd_real, uold_even_im, uold_odd_im, unew_even_real, unew_odd_real, unew_even_im, unew_odd_im);
}




void twoElectrons::schroe_dynamicsresolution(const double& dt, TensorTrain& u0_even_real, TensorTrain& u0_odd_real, TensorTrain& u0_even_im, TensorTrain& u0_odd_im, const int& NT)
{
	m_sol_real_even.resize(NT+1);
  	m_sol_real_odd.resize(NT+1);
        m_sol_imag_even.resize(NT+1);
  	m_sol_imag_odd.resize(NT+1);

	m_sol_real_even[0] = u0_even_real;
	m_sol_real_odd[0] = u0_odd_real;
        m_sol_imag_even[0] = u0_even_im;
	m_sol_imag_odd[0] = u0_odd_im;

	TensorTrain uold_even_real;
	uold_even_real.copyTensorFrom(u0_even_real);

	TensorTrain uold_odd_real;
	uold_odd_real.copyTensorFrom(u0_odd_real);

	TensorTrain uold_even_im;
	uold_even_im.copyTensorFrom(u0_even_im);

	TensorTrain uold_odd_im;
	uold_odd_im.copyTensorFrom(u0_odd_im);

  save_1d_wave_fun_vtk("./Applications/Schroedinger/Output/waveFun", 0);


	for (int n=0; n<NT; n++)
	{
    cout << "Time iteration: " << n << endl;
    m_sol_real_even[n].print_ttSize();
    m_sol_real_odd[n].print_ttSize();
    m_sol_imag_even[n].print_ttSize();
    m_sol_imag_odd[n].print_ttSize();


    TensorTrain unew_odd_real;
    TensorTrain unew_even_real;
    TensorTrain unew_odd_im;
    TensorTrain unew_even_im;

		//*******Calcul de c *******///
		//double l2u= squareL2norm(uold_even, uold_odd);
    /*double l2u= squareL2norm(m_sol_real_even[n], m_sol_real_odd[n]);
    m_sol_real_even[n] *= 1.0/sqrt(l2u);
    m_sol_real_odd[n] *= 1.0/sqrt(l2u);
    l2u= squareL2norm(m_sol_real_even[n], m_sol_real_odd[n]);


		//TensorTrain Hu_even = m_H0_even*uold_even;
    TensorTrain Hu_even = m_H0_even*m_sol_real_even[n];
		//TensorTrain beven = m_H0_odd*uold_odd;
    TensorTrain beven = m_H0_odd*m_sol_real_odd[n];
		Hu_even +=  beven;
    round_asvd(Hu_even, 1.0e-6);

		//TensorTrain Hu_odd = m_H0_even*uold_odd;
    TensorTrain Hu_odd = m_H0_even*m_sol_real_odd[n];
		//TensorTrain bodd = m_H0_odd*uold_even;
    TensorTrain bodd = m_H0_odd*m_sol_real_even[n];
		Hu_odd +=  bodd;
    round_asvd(Hu_odd, 1.0e-6);


		//double uHu = L2scal(uold_even, uold_odd, Hu_even, Hu_odd);
    double uHu = L2scal(m_sol_real_even[n], m_sol_real_odd[n], Hu_even, Hu_odd);
		double c = uHu/l2u;
    cout << "Value l2u = " << l2u << endl;
    cout << "Value uHu = " << uHu << endl;
    cout << "Value of c = " << c << endl;*/


		//heat_onetimestepresolution(dt,c, uold_even,uold_odd,unew_even, unew_odd);
    schroe_onetimestepresolution(dt, m_sol_real_even[n],m_sol_real_odd[n], m_sol_imag_even[n],m_sol_imag_odd[n], unew_even_real, unew_odd_real, unew_even_im, unew_odd_im);

    m_sol_real_even[n+1].copyTensorFrom(unew_even_real);
    m_sol_real_odd[n+1].copyTensorFrom(unew_odd_real);
     m_sol_imag_even[n+1].copyTensorFrom(unew_even_im);
    m_sol_imag_odd[n+1].copyTensorFrom(unew_odd_im);

    // clearing the memory
    unew_even_real.clear();
    unew_odd_real.clear();
    unew_even_im.clear();
    unew_odd_im.clear();

    m_sol_real_even[n+1].print_ttSize();
    m_sol_real_odd[n+1].print_ttSize();
    m_sol_imag_even[n+1].print_ttSize();
    m_sol_imag_odd[n+1].print_ttSize();

    save_1d_wave_fun_vtk("./Applications/Schroedinger/Output/waveFun", n+1);


		//uold_even.copyTensorFrom(unew_even);
		//uold_odd.copyTensorFrom(unew_odd);
    cout << "Ending iteration." << endl;
	}

}


/* build a Krylov space
  - input: a vector of rhs, an operator block-type, the order of the Krylov
  - output: a vector of TT
*/
void twoElectrons::krylovBuild(vector<TensorTrain>& g, blockOperatorTensor& A, unsigned int order, double tolRound, vector<vector<TensorTrain> >& space){
  assert(g.size()== A.size());
  const unsigned int nComp = g.size();
  space.resize(order+1);
  vector<TensorTrain> firstTerm(nComp);
  for(unsigned int iComp=0; iComp<nComp; iComp++){
    firstTerm[iComp].copyTensorFrom(g[iComp]);
  }
  space[0] = firstTerm;
  for(unsigned int i=1; i<order+1; i++){
    vector<TensorTrain> iTerm(nComp);
    for(unsigned int iComp=0; iComp<nComp; iComp++){
      operatorTensor thisOp = A.A(iComp,0);
      TensorTrain termComp; termComp.copyTensorStructFrom(g[0]);
      if(!thisOp.isZero()){
        termComp = thisOp * space[i-1][0];
        termComp *= A.coeffs(iComp,0);
      }
      for(unsigned int j=1; j<nComp; j++){
        cout << " i = " << iComp <<  "  j = " << j << endl;
        operatorTensor thisOp = A.A(iComp,j);
        if(!thisOp.isZero()){
          TensorTrain tmp = thisOp * space[i-1][j];
          tmp *= A.coeffs(iComp,j);
          termComp += tmp;
          round_asvd(termComp, tolRound);
          tmp.clear();
        }
      }
      iTerm[iComp] = termComp;
    }
    space[i] = iTerm;
  }
}



/* least squares for 3 variables Tensor Trains
  - input: vector of Tensor Train, a rhs Tensor Train
  - output: a vec of coefficients which is the best linear combination to get rhs
*/
void twoElectrons::least_squares(vector<TensorTrain>& vecT, TensorTrain& rhs, vec& sol){
  const unsigned int nTens = vecT.size();
  sol.init(nTens, vecT[0].comm());

  // 1 - compute an orthonormal basis of x_1 modes:
  unsigned int nMod_0 = 0;
  for(unsigned int iTens=0; iTens<nTens; iTens++){
    nMod_0 += vecT[iTens].ranks(0);
  }
  vector<vec> modeTable(nMod_0);
  unsigned int cc = 0;
  for(unsigned int iTens=0; iTens<nTens; iTens++){
    for(unsigned int iFib=0; iFib<vecT[iTens].ranks(0); iFib++){
      modeTable[cc] = vecT[iTens].train(0,0,iFib);
      cc += 1;
    }
  }

  mat C(nMod_0,nMod_0, vecT[0].comm());
  for(unsigned int i=0; i<nMod_0; i++){
    vec fibre_i = modeTable[i];
    for(unsigned int j=0; j<=i; j++){
      vec fibre_j = modeTable[j];
      double entry = scalProd(fibre_i, fibre_j);
      C(i,j) = entry;
      if(i != j){
        C(j,i) = entry;
      }
    }
  }
  C.finalize();
  C += DBL_EPSILON*eye(C.nRows(),C.comm());

  // solve the eigenvalue problem:
  eigenSolver eps_0;
  eps_0.init(C, EPS_HEP, nMod_0, 1.0e-12);
  eps_0.solveHermEPS(C);
  vector<vec> vv = eps_0.eigenVecs();
  vector<double> lambda = eps_0.eigenVals();

  // finding how many modes to retain:
  unsigned int nS = lambda.size();
  unsigned int toRetain_0 = nS;
  double tail = lambda[nS-1] * lambda[nS-1];
  unsigned int ct = 1;
  while ( (tail < 1.0e-12) && (ct<nS) ){
    ct += 1;
    tail += lambda[nS-ct] * lambda[nS-ct];
    toRetain_0 -= 1;
  }

  // Compute U in order to modify the RHS:
  vector<vec> phi_0(toRetain_0);
  for(unsigned int iMod=0; iMod<toRetain_0; iMod++){
    vec mod(vecT[0].nDof_var(0), vecT[0].comm());
    mod.finalize();
    for(unsigned int iV=0; iV<vv[iMod].size(); iV++){
      mod.sum(modeTable[iV], vv[iMod].getVecEl(iV));
    }
    mod *= (1.0/sqrt(lambda[iMod]));
    phi_0[iMod] = mod;
  }

  // Rescale:
  for(unsigned int iMod=0; iMod<toRetain_0; iMod++){
    vv[iMod] *= sqrt(lambda[iMod]);
  }


  // 2 - compute an orthonormal basis of x_3 modes:
  unsigned int nMod_2 = 0;
  for(unsigned int iTens=0; iTens<nTens; iTens++){
    nMod_2 += vecT[iTens].ranks(1);
  }
  vector<vec> modeTab_2(nMod_2);
  unsigned int dd = 0;
  for(unsigned int iTens=0; iTens<nTens; iTens++){
    for(unsigned int iFib=0; iFib<vecT[iTens].ranks(1); iFib++){
      modeTab_2[cc] = vecT[iTens].train(2,iFib,0);
      dd += 1;
    }
  }

  mat D(nMod_2,nMod_2, vecT[0].comm());
  for(unsigned int i=0; i<nMod_2; i++){
    vec fibre_i = modeTab_2[i];
    for(unsigned int j=0; j<=i; j++){
      vec fibre_j = modeTab_2[j];
      double entry = scalProd(fibre_i, fibre_j);
      D(i,j) = entry;
      if(i != j){
        D(j,i) = entry;
      }
    }
  }
  D.finalize();
  D += DBL_EPSILON*eye(D.nRows(),D.comm());

  // solve the eigenvalue problem:
  eigenSolver eps_2;
  eps_2.init(D, EPS_HEP, nMod_2, 1.0e-12);
  eps_2.solveHermEPS(D);
  vector<vec> ww = eps_2.eigenVecs();
  vector<double> kappa = eps_2.eigenVals();

  // finding how many modes to retain:
  unsigned int nT = kappa.size();
  unsigned int toRetain_2 = nT;
  tail = kappa[nT-1] * kappa[nT-1];
  unsigned int dc = 1;
  while ( (tail < 1.0e-12) && (dc<nT) ){
    dc += 1;
    tail += kappa[nT-dc] * kappa[nT-dc];
    toRetain_2 -= 1;
  }

  // Compute U in order to modify the RHS:
  vector<vec> phi_2(toRetain_2);
  for(unsigned int iMod=0; iMod<toRetain_2; iMod++){
    vec mod(vecT[0].nDof_var(2), vecT[0].comm());
    mod.finalize();
    for(unsigned int iV=0; iV<ww[iMod].size(); iV++){
      mod.sum(modeTab_2[iV], ww[iMod].getVecEl(iV));
    }
    mod *= (1.0/sqrt(kappa[iMod]));
    phi_2[iMod] = mod;
  }

  // Rescale:
  for(unsigned int iMod=0; iMod<toRetain_2; iMod++){
    ww[iMod] *= sqrt(kappa[iMod]);
  }

  // 3 -- compute the vectorised least square:
  const unsigned int sizeVec = vecT[0].nDof_var(1) * toRetain_0 * toRetain_2;

  // 3.1 -- the matrix --
  vector<vec> A(nTens);
  unsigned int c_comp_0 = 0;
  unsigned int c_comp_2 = 0;
  for(unsigned int iTens=0; iTens<nTens; iTens++){
    A[iTens].init(sizeVec, vecT[iTens].comm());
    // proceed slice by slice and vectorise the matrices of the core:
    unsigned int count = 0;
    for(unsigned int iDof=0; iDof<vecT[iTens].nDof_var(1); iDof++){
      for(unsigned int i_0=0; i_0<toRetain_0; i_0++){
        for(unsigned int i_2=0; i_2<toRetain_2; i_2++){
          double value = 0.0;
          for(unsigned int j_0=0; j_0<vecT[iTens].ranks(0); j_0++){
            unsigned int ind_0 = j_0 + c_comp_0;
            for(unsigned int j_2=0; j_2<vecT[iTens].ranks(1);j_2++){
              unsigned int ind_2 = j_2 + c_comp_2;
              double tt_val = vecT[iTens].train(1,j_0,j_2).getVecEl(iDof);
              value += vv[i_0].getVecEl(ind_0) * ww[i_2].getVecEl(ind_2) * tt_val ;
            }
          }
          A[iTens](count) = value;
          count += 1;
        }
      }
    }
    A[iTens].finalize();
    c_comp_0 += vecT[iTens].ranks(0);
    c_comp_2 += vecT[iTens].ranks(1);
  }

  // 3.2 -- the rhs:
  mat P_0(toRetain_0, rhs.ranks(0), rhs.comm());
  for(unsigned int iMod=0; iMod<toRetain_0; iMod++){
    for(unsigned int iFib=0; iFib<rhs.ranks(0); iFib++){
      vec fibre = rhs.train(0,0,iFib);
      double entry = scalProd(phi_0[iMod], fibre);
      P_0(iMod,iFib) = entry;
    }
  }
  P_0.finalize();

  mat P_2(toRetain_2, rhs.ranks(1), rhs.comm());
  for(unsigned int iMod=0; iMod<toRetain_2; iMod++){
    for(unsigned int iFib=0; iFib<rhs.ranks(1); iFib++){
      vec fibre = rhs.train(2,iFib,0);
      double entry = scalProd(phi_2[iMod], fibre);
      P_2(iMod,iFib) = entry;
    }
  }
  P_2.finalize();

  vec proj_rhs(sizeVec, rhs.comm());
  unsigned int count = 0;
  for(unsigned int iDof=0; iDof<rhs.nDof_var(1); iDof++){
    for(unsigned int i_0=0; i_0<toRetain_0; i_0++){
      for(unsigned int i_2=0; i_2<toRetain_2; i_2++){
        double value = 0.0;
        for(unsigned int j_0=0; j_0<rhs.ranks(0); j_0++){
          for(unsigned int j_2=0; j_2<rhs.ranks(1); j_2++){
            double tt_val = rhs.train(1,j_0,j_2).getVecEl(iDof);
            double p_0 = P_0(i_0,j_0);
            double p_2 = P_2(i_2,j_2);
            value += tt_val * p_0 * p_2;
          }
        }
        proj_rhs(count) = value;
        count += 1;
      }
    }
  }
  proj_rhs.finalize();

  // 3.3 -- solve the least-squares problem:
  qr factorize(A);
  vector<vec> Q = factorize.Q();
  mat R = factorize.R();
  vec reduced(Q.size(), Q[0].comm());
  for(unsigned int i=0; i<Q.size(); i++){
    double value = scalProd(proj_rhs, Q[i]);
    reduced(i) = value;
  }
  reduced.finalize();

  sol = R/reduced;


  // free the memory:
  C.clear();
  eps_0.clear();
  D.clear();
  eps_2.clear();
  for(unsigned int i=0; i<nTens; i++){
    A[i].clear();
  }
  A.clear();
  P_0.clear();
  P_2.clear();
  proj_rhs.clear();
  factorize.clear();
  reduced.clear();
}


// auxiliary function: compute covariance:
mat compute_covariance(mat A){
  mat A_t = transpose(A);
  mat C = A_t*A;
  C += DBL_EPSILON*eye(C.nRows(),C.comm());
  return C;
}

mat compute_covariance(vector<vec> A){
  const unsigned int nMod = A.size();
  mat D(nMod,nMod, A[0].comm());
  for(unsigned int i=0; i<nMod; i++){
    vec fibre_i = A[i];
    for(unsigned int j=0; j<=i; j++){
      vec fibre_j = A[j];
      double entry = scalProd(fibre_i, fibre_j);
      D(i,j) = entry;
      if(i != j){
        D(j,i) = entry;
      }
    }
  }
  D.finalize();
  D += DBL_EPSILON*eye(D.nRows(),D.comm());
  return D;
}


/* Rounding by ASVD on the Tensor Train with 3 variables:
  - input: a 3 variables TT
  - output: its compression
*/
void twoElectrons::round_asvd(TensorTrain& T, double tol){
  const double tol2_per_var = tol*tol/2.0;
  const unsigned int N_0 = T.nDof_var(0);
  const unsigned int N_1 = T.nDof_var(1);
  const unsigned int N_2 = T.nDof_var(2);

  // Variable 0:

  // QR on the modes.
  vector<vec> r_k(T.ranks(0));
  for(unsigned int i1=0; i1<T.ranks(0); i1++){
    r_k[i1] = T.train(0,0,i1);
  }
  qr modes_r_qr(r_k, T.comm());
  vector<vec> Q_0 = modes_r_qr.Q();
  mat R_0 = modes_r_qr.R();
  unsigned int rank_0 = Q_0.size();

  // flatten the core linearly combined with R:
  /*mat M_0(rank_0*N_1, T.ranks(1), T.comm());
  for(unsigned int i2=0; i2<T.ranks(1); i2++){
    unsigned int iRow=0;
    for(unsigned int j=0; j<rank_0; j++){
      for(unsigned int iDof=0; iDof<N_1; iDof++){
        double val = 0.0;
        for(unsigned int k=0; k<T.ranks(0); k++){
          double valTrain = T.train(1,k,i2).getVecEl(iDof);
          val += R_0(j,k)*valTrain;
        }
        M_0.setMatEl(iRow,i2,val);
        iRow += 1;
      }
    }
  }
  M_0.finalize();*/

  vector<vec> M_0(T.ranks(1));
  for(unsigned int i2=0; i2<T.ranks(1); i2++){
    M_0[i2].init(rank_0*N_1, T.comm());
    unsigned int iRow=0;
    for(unsigned int j=0; j<rank_0; j++){
      for(unsigned int iDof=0; iDof<N_1; iDof++){
        double val = 0.0;
        for(unsigned int k=0; k<T.ranks(0); k++){
          double valTrain = T.train(1,k,i2).getVecEl(iDof);
          val += R_0(j,k)*valTrain;
        }
        M_0[i2].setVecEl(iRow,val);
        iRow += 1;
      }
      M_0[i2].finalize();
    }
  }

  // svd of M S^T: svd of M, QR of S, svd of K.
  mat C_0 = compute_covariance(M_0);

  // solve the eigenvalue problem:
  eigenSolver eps_0;
  eps_0.init(C_0, EPS_HEP, C_0.nCols(), 1.0e-12);
  eps_0.solveHermEPS(C_0);
  vector<vec> vv_0 = eps_0.eigenVecs();
  vector<double> lambda_0 = eps_0.eigenVals();


  // Compute U_M_0 in order to modify the RHS:
  /*vector<vec> U_M_0(lambda_0.size());
  for(unsigned int iMod=0; iMod<lambda_0.size(); iMod++){
    vec mod = lin_comb(M_0, vv_0[iMod]);
    mod *= (1.0/sqrt(lambda_0[iMod]));
    U_M_0[iMod] = mod;
  }*/
  // rescale vv_0;
  for(unsigned int iMod=0; iMod<lambda_0.size(); iMod++){
    vv_0[iMod] *= sqrt(fabs(lambda_0[iMod]));
  }

  // QR of S modes:
  vector<vec> s_l(T.ranks(1));
  for(unsigned int i2=0; i2<T.ranks(1); i2++){
    s_l[i2] = T.train(2,i2,0);
  }
  qr modes_s_qr(s_l, T.comm());
  vector<vec> Q_2 = modes_s_qr.Q();
  mat R_2 = modes_s_qr.R();
  unsigned int rank_2 = Q_2.size();


  // Assembly of matrix K:
  mat K(lambda_0.size(), R_2.nRows(), R_2.comm());
  for(unsigned int iRow=0; iRow<lambda_0.size(); iRow++){
    for(unsigned int iCol=0; iCol<R_2.nRows(); iCol++){
      double val = 0.0;
      for(unsigned int k=0; k<T.ranks(1); k++){
        val += vv_0[k].getVecEl(iRow) * R_2(iCol,k);
      }
      K.setMatEl(iRow, iCol, val);
    }
  }
  K.finalize();


  // svd of matrix K:
  mat C_K = compute_covariance(K);
  eigenSolver eps_k;
  eps_k.init(C_K, EPS_HEP, C_K.nCols(), 1.0e-12);
  eps_k.solveHermEPS(C_K);
  vector<vec> vv_k = eps_k.eigenVecs();
  vector<double> kappa = eps_k.eigenVals();

  // compress:
  unsigned int nT = kappa.size();
  unsigned int toRetain = nT;
  double tail = kappa[nT-1];
  unsigned int dc = 1;
  while ( (tail < tol2_per_var) && (dc<nT) ){
    dc += 1;
    tail += kappa[nT-dc];
    toRetain -= 1;
  }
  // compute left modes non-rescaled! => (U_K S_K)
  /*vector<vec> U_K(toRetain);
  for(unsigned int iMod=0; iMod<toRetain; iMod++){
    vec mod(K.nRows(), K.comm());
    for(unsigned int iDof=0; iDof<K.nRows(); iDof++){
      double val = 0.0;
      for(unsigned int iV=0; iV<vv_k[iMod].size(); iV++){
        val += K.getMatEl(iDof,iV) * vv_k[iMod].getVecEl(iV);
      }
      mod.setVecEl(iDof,val);
    }
    mod.finalize();
    U_K[iMod] = mod;
  }*/

  vector<vec> vv_s_k(toRetain);
  for(unsigned int iMod=0; iMod<toRetain; iMod++){
    vv_s_k[iMod] = vv_k[iMod];
  }

  // Rotations:
  //vector<vec> modes_w_update = lin_comb(U_M_0, U_K);
  vector<vec> modes_s_update = lin_comb(Q_2, vv_s_k);


  // Variable 2:

  // V_k R_2:
  mat W_k2(T.ranks(1), toRetain, R_2.comm());
  for(unsigned int iRow=0; iRow<T.ranks(1); iRow++){
    for(unsigned int iCol=0; iCol<toRetain; iCol++){
      double val = 0.0;
      for(unsigned int k=0; k<T.ranks(1); k++){
        val += R_2(k,iRow) * vv_k[iCol].getVecEl(k);
      }
      W_k2.setMatEl(iRow, iCol, val);
    }
  }
  W_k2.finalize();

  // Contract the original core by W_k2 and reshape:
  vector<vec> M_2(T.ranks(0));
  for(unsigned int iCol=0; iCol<T.ranks(0); iCol++){
    vec col(T.nDof_var(1)*toRetain, T.comm());
    unsigned int iRow=0;
    for(unsigned int l=0;l<toRetain; l++){
      for(unsigned int iDof=0; iDof<T.nDof_var(1); iDof++){
        double val = 0.0;
        for(unsigned int l1=0; l1<T.ranks(1); l1++){
          val += W_k2.getMatEl(l1,l) * T.train(1,iCol,l1).getVecEl(iDof);
        }
        col.setVecEl(iRow, val);
        iRow += 1;
      }
    }
    col.finalize();
    M_2[iCol] = col;
  }

  // QR of r modes has already been done!

  // compute svd of M_2:
  mat C_2 = compute_covariance(M_2);
  // solve the eigenvalue problem:
  eigenSolver eps_2;
  eps_2.init(C_2, EPS_HEP, C_2.nCols(), 1.0e-12);
  eps_2.solveHermEPS(C_2);
  vector<vec> vv_2 = eps_2.eigenVecs();
  vector<double> lambda_2 = eps_2.eigenVals();
  // rescale vv_2;
  for(unsigned int iMod=0; iMod<lambda_2.size(); iMod++){
    vv_2[iMod] *= sqrt(fabs(lambda_2[iMod]));
  }

  // Assembly of matrix L:
  mat L(lambda_2.size(), R_0.nRows(), R_0.comm());
  for(unsigned int iRow=0; iRow<lambda_2.size(); iRow++){
    for(unsigned int iCol=0; iCol<R_0.nRows(); iCol++){
      double val = 0.0;
      for(unsigned int k=0; k<T.ranks(0); k++){
        val += vv_2[k].getVecEl(iRow) * R_0(iCol,k);
      }
      L.setMatEl(iRow, iCol, val);
    }
  }
  L.finalize();

  // svd of matrix L:
  mat C_L = compute_covariance(L);
  eigenSolver eps_l;
  eps_l.init(C_L, EPS_HEP, C_L.nCols(), 1.0e-12);
  eps_l.solveHermEPS(C_L);
  vector<vec> vv_l = eps_l.eigenVecs();
  vector<double> ka_l = eps_l.eigenVals();

  // compress:
  unsigned int nL = ka_l.size();
  unsigned int toRet_L = nL;
  tail = ka_l[nL-1];
  dc = 1;
  while ( (tail < tol2_per_var) && (dc<nL) ){
    dc += 1;
    tail += ka_l[nL-dc];
    toRet_L -= 1;
  }

  //  vector<vec> vv_s_l(toRet_l);
  vector<vec> vv_r_l(toRet_L);
  for(unsigned int iMod=0; iMod<toRet_L; iMod++){
    vv_r_l[iMod] = vv_l[iMod];
  }

  // update modes r:
  vector<vec> modes_r_update = lin_comb(Q_0, vv_r_l);

  // project the central core on the updates as in round3_var:

  // 1st contraction matrix = vv_r_l^T R_0:
  mat A_0(modes_r_update.size(),T.ranks(0),T.comm());
  for(unsigned int iRow=0; iRow<modes_r_update.size(); iRow++){
    for(unsigned int iCol=0; iCol<T.ranks(0); iCol++){
      double value = 0.0;
      for(unsigned int k=0; k<T.ranks(0); k++){
        value += vv_r_l[iRow].getVecEl(k) * R_0.getMatEl(k,iCol);
      }
      A_0.setMatEl(iRow, iCol, value);
    }
  }
  A_0.finalize();

  // 2nd contraction matrix = vv_s_k^T R_2:
  mat A_2(modes_s_update.size(),T.ranks(1),T.comm());
  for(unsigned int iRow=0; iRow<modes_s_update.size(); iRow++){
    for(unsigned int iCol=0; iCol<T.ranks(1); iCol++){
      double value = 0.0;
      for(unsigned int k=0; k<T.ranks(1); k++){
        value += vv_s_k[iRow].getVecEl(k) * R_2.getMatEl(k,iCol);
      }
      A_2.setMatEl(iRow, iCol, value);
    }
  }
  A_2.finalize();

  // double contraction of the core G:
  //init core:
  vector<vector<vec>> G(modes_r_update.size());
  for(unsigned int i1=0; i1<modes_r_update.size(); i1++){
    G[i1].resize(modes_s_update.size());
    for(unsigned int i2=0; i2<modes_s_update.size(); i2++){
      G[i1][i2].init(T.nDof_var(1),T.comm());
    }
  }
  // fill core:
  for(unsigned int iDof=0; iDof<T.nDof_var(1); iDof++){
    for(unsigned int i1=0; i1<modes_r_update.size(); i1++){
      for(unsigned int i2=0; i2<modes_s_update.size(); i2++){
        double value = 0.0;
        for(unsigned int j1=0; j1<T.ranks(0); j1++){
          for(unsigned int j2=0; j2<T.ranks(1); j2++){
            value += A_0.getMatEl(i1,j1) * T.train(1,j1,j2).getVecEl(iDof) * A_2.getMatEl(i2,j2);
          }
        }
        G[i1][i2].setVecEl(iDof,value);
      }
    }
  }
  // finalize assembly:
  for(unsigned int i1=0; i1<modes_r_update.size(); i1++){
    for(unsigned int i2=0; i2<modes_s_update.size(); i2++){
      G[i1][i2].finalize();
    }
  }


  // replace the cores:
  // 1 -- clear:
  T.clear_fibers(0);
  T.clear_fibers(1);
  T.clear_fibers(2);
  // set the new sizes:
  vector<unsigned int> new_ranks(2); new_ranks[0]=modes_r_update.size(); new_ranks[1]=modes_s_update.size();
  T.set_ranks(new_ranks);
  T.resizeTrain();
  // set the modes:
  vector<vector<vec> > G_0(1);
  G_0[0] = modes_r_update;
  vector<vector<vec> > G_2(modes_s_update.size());
  for(unsigned int i2=0; i2<modes_s_update.size(); i2++){
    G_2[i2].resize(1);
    G_2[i2][0] = modes_s_update[i2];
  }
  T.set_train(0,G_0);
  T.set_train(1,G);
  T.set_train(2,G_2);

  // clear the memory:
  for(unsigned int i=0; i<Q_0.size(); i++){
    Q_0[i].clear();
  }
  Q_0.clear();
  R_0.clear();
  for(unsigned int i=0; i<M_0.size(); i++){
    M_0[i].clear();
  }
  M_0.clear();
  C_0.clear();
  eps_0.clear();
  for(unsigned int i=0; i<Q_2.size(); i++){
    Q_2[i].clear();
  }
  Q_2.clear();
  R_2.clear();
  K.clear();
  C_K.clear();
  eps_k.clear();
  W_k2.clear();
  for(unsigned int i=0; i<M_2.size(); i++){
    M_2[i].clear();
  }
  M_2.clear();
  C_2.clear();
  C_L.clear();
  eps_l.clear();
  A_0.clear();
  A_2.clear();
}


/* round asvd for the sum of two TT:
  - input: two TT, tolerance
  - output: the FIRST TT is overwritten (!!) with the compression of the sum
    T_1 <-- C(T_1 + T_2)
*/
void twoElectrons::round_sum_asvd(TensorTrain& T1, TensorTrain& T2, double tol){
  const double tol2_per_var = tol*tol/2.0;
  const unsigned int N_0 = T1.nDof_var(0);
  const unsigned int N_1 = T1.nDof_var(1);
  const unsigned int N_2 = T1.nDof_var(2);

  // Variable 0:
cout << "entering sum_asvd" << endl;
  // QR on the modes (concatenate the modes).
  vector<vec> r_k(T1.ranks(0)+T2.ranks(0));
  for(unsigned int i1=0; i1<T1.ranks(0); i1++){
    r_k[i1] = T1.train(0,0,i1);
  }
  for(unsigned int i1=0; i1<T2.ranks(0); i1++){
    unsigned int index = i1 + T1.ranks(0);
    r_k[index] = T2.train(0,0,i1);
  }
  qr modes_r_qr(r_k, T1.comm());
  vector<vec> Q_0 = modes_r_qr.Q();
  mat R_0 = modes_r_qr.R();
  unsigned int rank_0 = Q_0.size();


  // flatten the cores and concatenate:
  vector<vec> M_0(T1.ranks(1)+T2.ranks(1));
  for(unsigned int i2=0; i2<T1.ranks(1); i2++){
    M_0[i2].init(rank_0*N_1, T1.comm());
    unsigned int iRow=0;
    for(unsigned int j=0; j<rank_0; j++){
      for(unsigned int iDof=0; iDof<N_1; iDof++){
        double val = 0.0;
        for(unsigned int k=0; k<T1.ranks(0); k++){
          double valTrain = T1.train(1,k,i2).getVecEl(iDof);
          val += R_0(j,k)*valTrain;
        }
        M_0[i2].setVecEl(iRow,val);
        iRow += 1;
      }
      M_0[i2].finalize();
    }
  }
  for(unsigned int i2=0; i2<T2.ranks(1); i2++){
    unsigned int index = i2 + T1.ranks(1);
    M_0[index].init(rank_0*N_1, T2.comm());
    unsigned int iRow=0;
    for(unsigned int j=0; j<rank_0; j++){
      for(unsigned int iDof=0; iDof<N_1; iDof++){
        double val = 0.0;
        for(unsigned int k=0; k<T2.ranks(0); k++){
          double valTrain = T2.train(1,k,i2).getVecEl(iDof);
          val += R_0(j,k)*valTrain;
        }
        M_0[index].setVecEl(iRow,val);
        iRow += 1;
      }
      M_0[index].finalize();
    }
  }


  // svd of M:
  mat C_0 = compute_covariance(M_0);

  // solve the eigenvalue problem:
  eigenSolver eps_0;
  eps_0.init(C_0, EPS_HEP, C_0.nCols(), 1.0e-12);
  eps_0.solveHermEPS(C_0);
  vector<vec> vv_0 = eps_0.eigenVecs();
  vector<double> lambda_0 = eps_0.eigenVals();

  // rescale vv_0;
  for(unsigned int iMod=0; iMod<lambda_0.size(); iMod++){
    vv_0[iMod] *= sqrt(fabs(lambda_0[iMod]));
  }

  // QR of S modes:
  vector<vec> s_l(T1.ranks(1) + T2.ranks(1));
  for(unsigned int i2=0; i2<T1.ranks(1); i2++){
    s_l[i2] = T1.train(2,i2,0);
  }
  for(unsigned int i2=0; i2<T2.ranks(1); i2++){
    unsigned int index = i2 + T1.ranks(1);
    s_l[index] = T2.train(2,i2,0);
  }
  qr modes_s_qr(s_l, T1.comm());
  vector<vec> Q_2 = modes_s_qr.Q();
  mat R_2 = modes_s_qr.R();
  unsigned int rank_2 = Q_2.size();

  // Assembly of matrix K:
  mat K(lambda_0.size(), R_2.nRows(), R_2.comm());
  for(unsigned int iRow=0; iRow<lambda_0.size(); iRow++){
    for(unsigned int iCol=0; iCol<R_2.nRows(); iCol++){
      double val = 0.0;
      for(unsigned int k=0; k<vv_0.size() ;k++){
        val += vv_0[k].getVecEl(iRow) * R_2(iCol,k);
      }
      K.setMatEl(iRow, iCol, val);
    }
  }
  K.finalize();


  // svd of matrix K:
  mat C_K = compute_covariance(K);
  eigenSolver eps_k;
  eps_k.init(C_K, EPS_HEP, C_K.nCols(), 1.0e-12);
  eps_k.solveHermEPS(C_K);
  vector<vec> vv_k = eps_k.eigenVecs();
  vector<double> kappa = eps_k.eigenVals();

  // compress:
  unsigned int nT = kappa.size();
  unsigned int toRetain = nT;
  double tail = kappa[nT-1];
  unsigned int dc = 1;
  while ( (tail < tol2_per_var) && (dc<nT) ){
    dc += 1;
    tail += kappa[nT-dc];
    toRetain -= 1;
  }


  vector<vec> vv_s_k(toRetain);
  for(unsigned int iMod=0; iMod<toRetain; iMod++){
    vv_s_k[iMod] = vv_k[iMod];
  }

  // Rotations: UPDATE of MODES S
  vector<vec> modes_s_update = lin_comb(Q_2, vv_s_k);

  // Variable 2:

  // V_k R_2:
  mat W_k2(T1.ranks(1)+ T2.ranks(1), toRetain, R_2.comm());
  for(unsigned int iRow=0; iRow<T1.ranks(1)+T2.ranks(1); iRow++){
    for(unsigned int iCol=0; iCol<toRetain; iCol++){
      double val = 0.0;
      for(unsigned int k=0; k<T1.ranks(1)+T2.ranks(1); k++){
        val += R_2(k,iRow) * vv_k[iCol].getVecEl(k);
      }
      W_k2.setMatEl(iRow, iCol, val);
    }
  }
  W_k2.finalize();

  // Contract the two original cores by W_k2 and reshape:
  vector<vec> M_2(T1.ranks(0)+T2.ranks(0));
  for(unsigned int iCol=0; iCol<T1.ranks(0); iCol++){
    vec col(T1.nDof_var(1)*toRetain, T1.comm());
    unsigned int iRow=0;
    for(unsigned int l=0;l<toRetain; l++){
      for(unsigned int iDof=0; iDof<T1.nDof_var(1); iDof++){
        double val = 0.0;
        for(unsigned int l1=0; l1<T1.ranks(1); l1++){
          val += W_k2.getMatEl(l1,l) * T1.train(1,iCol,l1).getVecEl(iDof);
        }
        col.setVecEl(iRow, val);
        iRow += 1;
      }
    }
    col.finalize();
    M_2[iCol] = col;
  }
  for(unsigned int iCol=0; iCol<T2.ranks(0); iCol++){
    unsigned int index = iCol + T1.ranks(0);
    vec col(T1.nDof_var(1)*toRetain, T1.comm());
    unsigned int iRow=0;
    for(unsigned int l=0;l<toRetain; l++){
      for(unsigned int iDof=0; iDof<T1.nDof_var(1); iDof++){
        double val = 0.0;
        for(unsigned int l2=0; l2<T2.ranks(1); l2++){
          val += W_k2.getMatEl(l2+ T1.ranks(0),l) * T2.train(1,iCol,l2).getVecEl(iDof);
        }
        col.setVecEl(iRow, val);
        iRow += 1;
      }
    }
    col.finalize();
    M_2[index] = col;
  }

  // QR of r modes has already been done!

  // compute svd of M_2:
  mat C_2 = compute_covariance(M_2);
  // solve the eigenvalue problem:
  eigenSolver eps_2;
  eps_2.init(C_2, EPS_HEP, C_2.nCols(), 1.0e-12);
  eps_2.solveHermEPS(C_2);
  vector<vec> vv_2 = eps_2.eigenVecs();
  vector<double> lambda_2 = eps_2.eigenVals();

  // rescale vv_2;
  for(unsigned int iMod=0; iMod<lambda_2.size(); iMod++){
    vv_2[iMod] *= sqrt(fabs(lambda_2[iMod]));
  }
  // Assembly of matrix L:
  mat L(lambda_2.size(), R_0.nRows(), R_0.comm());
  for(unsigned int iRow=0; iRow<lambda_2.size(); iRow++){
    for(unsigned int iCol=0; iCol<R_0.nRows(); iCol++){
      double val = 0.0;
      for(unsigned int k=0; k<vv_2.size(); k++){
        val += vv_2[k].getVecEl(iRow) * R_0(iCol,k);
      }
      L.setMatEl(iRow, iCol, val);
    }
  }
  L.finalize();

  // svd of matrix L:
  mat C_L = compute_covariance(L);
  eigenSolver eps_l;
  eps_l.init(C_L, EPS_HEP, C_L.nCols(), 1.0e-12);
  eps_l.solveHermEPS(C_L);
  vector<vec> vv_l = eps_l.eigenVecs();
  vector<double> ka_l = eps_l.eigenVals();

  // compress:
  unsigned int nL = ka_l.size();
  unsigned int toRet_L = nL;
  tail = ka_l[nL-1];
  dc = 1;
  while ( (tail < tol2_per_var) && (dc<nL) ){
    dc += 1;
    tail += ka_l[nL-dc];
    toRet_L -= 1;
  }



  //  vector<vec> vv_s_l(toRet_l);
  vector<vec> vv_r_l(toRet_L);
  for(unsigned int iMod=0; iMod<toRet_L; iMod++){
    vv_r_l[iMod] = vv_l[iMod];
  }

  // update modes r:
  vector<vec> modes_r_update = lin_comb(Q_0, vv_r_l);

  // project the central core on the updates as in round3_var:

  //cout << "Project started" << endl;
  // 1st contraction matrix = vv_r_l^T R_0:
  mat A_0(modes_r_update.size(),T1.ranks(0)+T2.ranks(0),T1.comm());
  for(unsigned int iRow=0; iRow<modes_r_update.size(); iRow++){
    for(unsigned int iCol=0; iCol<T1.ranks(0)+ T2.ranks(0); iCol++){
      double value = 0.0;
      for(unsigned int k=0; k<vv_r_l[iRow].size(); k++){
        value += vv_r_l[iRow].getVecEl(k) * R_0.getMatEl(k,iCol);
      }
      A_0.setMatEl(iRow, iCol, value);
    }
  }
  A_0.finalize();
  //cout << "A_0" << endl;

  // 2nd contraction matrix = vv_s_k^T R_2:
  mat A_2(modes_s_update.size(),T1.ranks(1)+T2.ranks(1),T1.comm());
  for(unsigned int iRow=0; iRow<modes_s_update.size(); iRow++){
    for(unsigned int iCol=0; iCol<T1.ranks(1)+T2.ranks(1); iCol++){
      double value = 0.0;
      for(unsigned int k=0; k<vv_s_k[iRow].size(); k++){
        value += vv_s_k[iRow].getVecEl(k) * R_2.getMatEl(k,iCol);
      }
      A_2.setMatEl(iRow, iCol, value);
    }
  }
  A_2.finalize();
  //cout << "A_2" << endl;

  // double contraction of the core G:
  //init core:
  vector<vector<vec>> G(modes_r_update.size());
  for(unsigned int i1=0; i1<modes_r_update.size(); i1++){
    G[i1].resize(modes_s_update.size());
    for(unsigned int i2=0; i2<modes_s_update.size(); i2++){
      G[i1][i2].init(T1.nDof_var(1),T1.comm());
    }
  }

  // fill core:
  for(unsigned int iDof=0; iDof<T1.nDof_var(1); iDof++){
    for(unsigned int i1=0; i1<modes_r_update.size(); i1++){
      for(unsigned int i2=0; i2<modes_s_update.size(); i2++){
        double value = 0.0;
        for(unsigned int j1=0; j1<T1.ranks(0); j1++){
          for(unsigned int j2=0; j2<T1.ranks(1); j2++){
            value += A_0.getMatEl(i1,j1) * T1.train(1,j1,j2).getVecEl(iDof) * A_2.getMatEl(i2,j2);
          }
        }
        for(unsigned int j1=0; j1<T2.ranks(0); j1++){
          for(unsigned int j2=0; j2<T2.ranks(1); j2++){
            unsigned int index1 = j1 + T1.ranks(0);
            unsigned int index2 = j2 + T1.ranks(1);
            value += A_0.getMatEl(i1,index1) * T2.train(1,j1,j2).getVecEl(iDof) * A_2.getMatEl(i2,index2);
          }
        }
        G[i1][i2].setVecEl(iDof,value);
      }
    }
  }
  // finalize assembly:
  for(unsigned int i1=0; i1<modes_r_update.size(); i1++){
    for(unsigned int i2=0; i2<modes_s_update.size(); i2++){
      G[i1][i2].finalize();
    }
  }
  //cout << "G" << endl;

  // replace the cores in T1:
  // 1 -- clear:
  T1.clear_fibers(0);
  T1.clear_fibers(1);
  T1.clear_fibers(2);
  // set the new sizes:
  vector<unsigned int> new_ranks(2); new_ranks[0]=modes_r_update.size(); new_ranks[1]=modes_s_update.size();
  T1.set_ranks(new_ranks);
  T1.resizeTrain();
  // set the modes:
  vector<vector<vec> > G_0(1);
  G_0[0] = modes_r_update;
  vector<vector<vec> > G_2(modes_s_update.size());
  for(unsigned int i2=0; i2<modes_s_update.size(); i2++){
    G_2[i2].resize(1);
    G_2[i2][0] = modes_s_update[i2];
  }
  T1.set_train(0,G_0);
  T1.set_train(1,G);
  T1.set_train(2,G_2);

  // clear the memory:
  for(unsigned int i=0; i<Q_0.size(); i++){
    Q_0[i].clear();
  }
  Q_0.clear();
  R_0.clear();
  for(unsigned int i=0; i<M_0.size(); i++){
    M_0[i].clear();
  }
  M_0.clear();
  C_0.clear();
  eps_0.clear();
  for(unsigned int i=0; i<Q_2.size(); i++){
    Q_2[i].clear();
  }
  Q_2.clear();
  R_2.clear();
  K.clear();
  C_K.clear();
  eps_k.clear();
  W_k2.clear();
  for(unsigned int i=0; i<M_2.size(); i++){
    M_2[i].clear();
  }
  M_2.clear();
  C_2.clear();
  C_L.clear();
  eps_l.clear();
  A_0.clear();
  A_2.clear();
}



/* least squares for 3 variables Tensor Trains, 2x2 block
  - input: 2 vectors of Tensor Train, 2 rhs Tensor Trains
  - output: a vec of coefficients which is the best linear combination to get the (2-vector) rhs
*/
void twoElectrons::least_squares_block(vector<TensorTrain>& vec_up, vector<TensorTrain>& vec_down, TensorTrain& rhs_up, TensorTrain& rhs_down, vec& sol){
  const unsigned int nTens_up = vec_up.size();
  const unsigned int nTens_down = vec_down.size();
  assert(nTens_up==nTens_down);
  const unsigned int matSize = nTens_up;
  sol.init(nTens_up, vec_up[0].comm());


  mat cov_up(matSize,matSize, vec_up[0].comm());
  for(unsigned int iCol=0; iCol<matSize; iCol++){
    for(unsigned int iRow=0; iRow<=iCol; iRow++){
      double value = vec_up[iRow] * vec_up[iCol];
      cov_up.setMatEl(iCol,iRow, value);
      if(iRow != iCol){
        cov_up.setMatEl(iRow, iCol, value);
      }
    }
  }
  cov_up.finalize();
  //cov_up.print();


  mat cov_down(matSize,matSize, vec_down[0].comm());
  for(unsigned int iCol=0; iCol<matSize; iCol++){
    for(unsigned int iRow=0; iRow<=iCol; iRow++){
      double value = vec_down[iRow] * vec_down[iCol];
      cov_down.setMatEl(iCol,iRow, value);
      if(iRow != iCol){
        cov_down.setMatEl(iRow, iCol, value);
      }
    }
  }
  cov_down.finalize();

  mat cov = cov_up + cov_down;
  cov += 1.0e-12* eye(cov.nRows(), cov.comm());

  // assemble the rhs:
  vec red_up(matSize, vec_up[0].comm());
  for(unsigned int iRow=0; iRow<matSize; iRow++){
    double value = rhs_up * vec_up[iRow];
    red_up.setVecEl(iRow, value);
  }
  red_up.finalize();

  vec red_down(matSize, vec_down[0].comm());
  for(unsigned int iRow=0; iRow<matSize; iRow++){
    double value = rhs_down * vec_down[iRow];
    red_down.setVecEl(iRow, value);
  }
  red_down.finalize();

  vec reduced =  red_up + red_down;

  //cov.print();
  //reduced.print();

  // solve the least square:
  sol = cov/reduced;
  //sol.print();
}



/* auxiliary function that, given a vector of TTs compute a basis for the i-th variable
  - input: a vector of TT, the i-th variable, either 0 or 2
  - output: a basis phi and rescaled vv: A = phi (\lambda^{1/2} vv^T), rescaled_vv = \lambda^{1/2} vv
*/
void twoElectrons::extractBasis(vector<TensorTrain>& vecTT, unsigned int id_Var, vector<vec>& phi, vector<vec>& vv){
  const unsigned int nTens = vecTT.size();
  assert((id_Var==0) || (id_Var==2));
  unsigned int id_rank = 0;
  if(id_Var==2){id_rank=1;};

  // 1 - compute an orthonormal basis of x_{iVar} modes:
  unsigned int nMod_0 = 0;
  for(unsigned int iTens=0; iTens<nTens; iTens++){
    nMod_0 += vecTT[iTens].ranks(id_rank);
  }
  vector<vec> modeTable(nMod_0);
  unsigned int cc = 0;
  for(unsigned int iTens=0; iTens<nTens; iTens++){
    for(unsigned int iFib=0; iFib<vecTT[iTens].ranks(id_rank); iFib++){
      if(id_Var==0){
        modeTable[cc] = vecTT[iTens].train(0,0,iFib);
      }
      else{
        modeTable[cc] = vecTT[iTens].train(2,iFib,0);
      }
      cc += 1;
    }
  }

  mat C(nMod_0,nMod_0, vecTT[0].comm());
  for(unsigned int i=0; i<nMod_0; i++){
    vec fibre_i = modeTable[i];
    for(unsigned int j=0; j<=i; j++){
      vec fibre_j = modeTable[j];
      double entry = scalProd(fibre_i, fibre_j);
      C(i,j) = entry;
      if(i != j){
        C(j,i) = entry;
      }
    }
  }
  C.finalize();
  C += DBL_EPSILON*eye(C.nRows(),C.comm());

  // solve the eigenvalue problem:
  eigenSolver eps_0;
  eps_0.init(C, EPS_HEP, nMod_0, 1.0e-12);
  eps_0.solveHermEPS(C);
  vv = eps_0.eigenVecs();
  vector<double> lambda = eps_0.eigenVals();

  // finding how many modes to retain:
  unsigned int nS = lambda.size();
  unsigned int toRetain_0 = nS;
  double tail = lambda[nS-1] * lambda[nS-1];
  unsigned int ct = 1;
  while ( (tail < 1.0e-12) && (ct<nS) ){
    ct += 1;
    tail += lambda[nS-ct] * lambda[nS-ct];
    toRetain_0 -= 1;
  }

  // Compute U in order to modify the RHS:
  phi.resize(toRetain_0);
  for(unsigned int iMod=0; iMod<toRetain_0; iMod++){
    vec mod(vecTT[0].nDof_var(0), vecTT[0].comm());
    mod.finalize();
    for(unsigned int iV=0; iV<vv[iMod].size(); iV++){
      mod.sum(modeTable[iV], vv[iMod].getVecEl(iV));
    }
    mod *= (1.0/sqrt(lambda[iMod]));
    phi[iMod] = mod;
  }

  // Rescale VV:
  for(unsigned int iMod=0; iMod<toRetain_0; iMod++){
    vv[iMod] *= sqrt(lambda[iMod]);
  }

  // free the memory:
  C.clear();
}



// compute density: given m_sol_* compute rho = \int_{Omega_y} Psi dy
vec twoElectrons::compute_density(){
  vec density(m_Ndof[0], m_sol_real_even[0].comm());

  return density;
}
/* auxiliary function that reconstruct the wave function in 1d
  - input: string fileName, iteration in time it
  - output: save field in vtk
*/
void twoElectrons::save_1d_wave_fun_vtk(string fileName, unsigned int it){
  const unsigned int nDof = m_Ndof[0] * m_Ndof[0];
  mat sol_to_save(m_Ndof[0], m_Ndof[0], m_sol_real_even[it].comm());
  for(unsigned int i=0; i<m_Ndof[0]; i++){
    for(unsigned int j=0; j<m_Ndof[0]; j++){
      int k = i-j + (m_Ndof[0]-1)/2;
      int k_p = k+1;
      if(k<0){
        k += m_Ndof[0];
      }
      if(k>=m_Ndof[0]){
        k -= m_Ndof[0];
      }
      if(k_p<0){
        k_p += m_Ndof[0];
      }
      if(k_p>=m_Ndof[0]){
        k_p -= m_Ndof[0];
      }
      double val_even = m_sol_real_even[it](i,k,j);
      double val_even_p = m_sol_real_even[it](i,k_p,j);
      double val_odd = m_sol_real_odd[it](i,k,j);
      double val_odd_p = m_sol_real_odd[it](i,k_p,j);
      double value = 0.5*(val_even+val_even_p + val_odd+val_odd_p);
      sol_to_save.setMatEl(i,j, value);
    }
  }
  sol_to_save.finalize();
  mat sol_to_save_t = transpose(sol_to_save);
  sol_to_save += sol_to_save_t;
  sol_to_save *= 0.5;

  stringstream st;
  st << it;
  string saveName = fileName +"_"+st.str()+ ".vtk";
  unsigned int nx = m_Ndof[0];
  unsigned int ny = m_Ndof[0];
  unsigned int nz = 1;
  unsigned int numOfVal = nx*ny*nz;

  ofstream outfile (saveName.c_str());

  outfile << "# vtk DataFile Version 2.0 " <<endl;
  outfile << "Data animation" << endl;
  outfile << "ASCII" <<endl;
  outfile << "DATASET STRUCTURED_POINTS" <<endl;
  outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
  outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
  outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
  outfile << "POINT_DATA " << numOfVal << endl;
  outfile << "SCALARS Field float" << endl;
  outfile << "LOOKUP_TABLE default" <<endl;
  for(unsigned int i=0; i<m_Ndof[0]; i++){
    for(unsigned int j=0; j<m_Ndof[0]; j++){
      double value = sol_to_save(j,i);
      if(fabs(value) < 1.0e-9){value = 0.0;};
      outfile << value << endl;
    }
  }
  outfile.close();

}
