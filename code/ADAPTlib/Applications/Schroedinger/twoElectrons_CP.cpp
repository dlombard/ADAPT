// implementation of the class two electrons
#include "twoElectrons_CP.h"



// I - Implementation of the class CP3var

// I.1 Overloaded constructors:

// empty, just assign the dofs:
CP3var::CP3var(vector<unsigned int> n_dofs){
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = n_dofs[iVar];
  }
  m_rank=0;
}

// assign the modes:
CP3var::CP3var(vector<vec>& R, vector<vec>& G, vector<vec>& S){
  m_rank=R.size();
  assert(G.size()==m_rank);
  assert(S.size()==m_rank);

  m_nDof_var.resize(m_nVar);
  m_nDof_var[0] = R[0].size();
  m_nDof_var[1] = G[0].size();
  m_nDof_var[2] = S[0].size();

  // do not perform copy, assign by reference.
  m_R = R;
  m_G = G;
  m_S = S;
}


// rank 1 term:
CP3var::CP3var(vec& r, vec& g, vec& s){
  m_rank=1;
  // define the resolution:
  m_nDof_var.resize(m_nVar);
  m_nDof_var[0] = r.size();
  m_nDof_var[1] = g.size();
  m_nDof_var[2] = s.size();

  // do not perform copy, assign by reference.
  m_R.resize(1);
  m_R[0] = r;
  m_G.resize(1);
  m_G[0] = g;
  m_S.resize(1);
  m_S[0] = s;
}



// (I.1.5) copyTensorFrom:
void CP3var::copyTensorFrom(CP3var& A){
  m_rank = A.rank();
  m_nDof_var.resize(3);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = A.nDof_var(iVar);
  }
  m_R.resize(m_rank);
  m_G.resize(m_rank);
  m_S.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_R[iTerm].copyVecFrom(A.R(iTerm));
    m_G[iTerm].copyVecFrom(A.G(iTerm));
    m_S[iTerm].copyVecFrom(A.S(iTerm));
  }
}


// (I.2) the zero CP: when the sizes are known(!!!)
void CP3var::zero(){
  m_rank=1;
  m_R.resize(1);
  m_R[0].init(m_nDof_var[0]);
  m_R[0].finalize();
  m_G.resize(1);
  m_G[0].init(m_nDof_var[1]);
  m_G[0].finalize();
  m_S.resize(1);
  m_S[0].init(m_nDof_var[2]);
  m_S[0].finalize();
}

void CP3var::zero(vector<unsigned int> n_dofs){
  assert(n_dofs.size()==3);
  m_rank=1;
  m_nDof_var.resize(3);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = n_dofs[iVar];
  }
  m_R.resize(1);
  m_R[0].init(n_dofs[0], m_comm);
  m_R[0].finalize();
  m_G.resize(1);
  m_G[0].init(n_dofs[1], m_comm);
  m_G[0].finalize();
  m_S.resize(1);
  m_S[0].init(n_dofs[2], m_comm);
  m_S[0].finalize();
}

// (I.3) append a vector => increase the rank by 1:
void CP3var::append(vec& r, vec& g, vec& s){
  m_rank += 1;
  m_R.push_back(r);
  m_G.push_back(g);
  m_S.push_back(s);
}

// (I.3) append a vector => increase the rank by 1:
void CP3var::append(vector<vec>& r_modes, vector<vec>& g_modes, vector<vec>& s_modes){
  const unsigned int nModes = r_modes.size();
  assert(g_modes.size()==nModes);
  assert(s_modes.size()==nModes);
  m_rank += nModes;

  for(unsigned int iMod=0; iMod<nModes; iMod++){
    m_R.push_back(r_modes[iMod]);
    m_G.push_back(g_modes[iMod]);
    m_S.push_back(s_modes[iMod]);
  }
}


// (I.4) function axpy: this = this + alpha*A
void CP3var::axpy(CP3var& A, double alpha, bool byCopy=true){
  const unsigned int old_rank = m_rank;

  double sign = 1.0;
  if(alpha<0){sign = -1.0;}
  const double factor = pow(fabs(alpha), (1.0/3.0));

  m_rank += A.rank();
  m_R.resize(m_rank);
  m_G.resize(m_rank);
  m_S.resize(m_rank);

  if(byCopy){
    for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
      m_R[iTerm+old_rank].copyVecFrom(A.R(iTerm));
      m_R[iTerm+old_rank]*=factor*sign;
      m_G[iTerm+old_rank].copyVecFrom(A.G(iTerm));
      m_G[iTerm+old_rank]*= factor;
      m_S[iTerm+old_rank].copyVecFrom(A.S(iTerm));
      m_S[iTerm+old_rank]*= factor;
    }
  }
  else{
    A *= alpha; // changing A!
    for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
      m_R[iTerm+old_rank] = A.R(iTerm);
      m_G[iTerm+old_rank] = A.G(iTerm);
      m_S[iTerm+old_rank] = A.S(iTerm);
    }
  }
}


// (I.5) apply an operator tensor:
CP3var CP3var::applyOperator(operatorTensor& Op){
  const unsigned int op_rank = Op.numTerms();
  const unsigned int out_rank = m_rank * op_rank;
  vector<vec> out_R(out_rank);
  vector<vec> out_G(out_rank);
  vector<vec> out_S(out_rank);
  unsigned int cc = 0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int iOp=0; iOp<op_rank; iOp++){
      vec r = Op.op(iOp,0) * m_R[iTerm];
      vec g = Op.op(iOp,1) * m_G[iTerm];
      vec s = Op.op(iOp,2) * m_S[iTerm];
      out_R[cc] = r;
      out_G[cc] = g;
      out_S[cc] = s;
      cc += 1;
    }
  }
  CP3var output(out_R, out_G, out_S);
  return output;
}

// (I.6) norm squared of the CP:
double CP3var::norm2CP(){
  double out = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int jTerm=0; jTerm<m_rank; jTerm++){
      double contrib = scalProd(m_R[iTerm],m_R[jTerm]);
      contrib *= scalProd(m_G[iTerm],m_G[jTerm]);
      contrib *= scalProd(m_S[iTerm],m_S[jTerm]);
      out += contrib;
    }
  }
  return out;
}



// (I.) scaling the CP3var by a scalar:
void CP3var::operator *= (double alpha){
  double sign = 1.0;
  if(alpha<0){sign = -1.0;}
  const double factor = pow(fabs(alpha), (1.0/3.0) );

  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_R[iTerm]*=(factor*sign);
    m_G[iTerm]*=factor;
    m_S[iTerm]*=factor;
  }
}

// (I.) copy operator:
void CP3var::operator << (CP3var& A){
  copyTensorFrom(A);
}

// (I.) operator += : implementation without copy (!!!)
void CP3var::operator += (CP3var& A){
  const unsigned int old_rank = m_rank;

  m_rank += A.rank();
  m_R.resize(m_rank);
  m_G.resize(m_rank);
  m_S.resize(m_rank);

  for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
    m_R[iTerm+old_rank] = A.R(iTerm);
    m_G[iTerm+old_rank] = A.G(iTerm);
    m_S[iTerm+old_rank] = A.S(iTerm);
  }
}

// (I.) eval operator:
double CP3var::operator()(unsigned int i, unsigned int j, unsigned int k){
  double out = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double contrib = m_R[iTerm].getVecEl(i);
    contrib *= m_G[iTerm].getVecEl(j);
    contrib *= m_S[iTerm].getVecEl(k);
    out += contrib;
  }
  return out;
}



// (I.) overloading operator * to Apply operator
CP3var operator * (operatorTensor& Op, CP3var& in){
  CP3var out = in.applyOperator(Op);
  return out;
}

// (I.) compute the scalar product between 2 CP3var
double operator * (CP3var& A, CP3var& B){
  double out = 0.0;
  for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
    for(unsigned int jTerm=0; jTerm<B.rank(); jTerm++){
      double contrib = scalProd(A.R(iTerm),B.R(jTerm));
      contrib *= scalProd(A.G(iTerm),B.G(jTerm));
      contrib *= scalProd(A.S(iTerm),B.S(jTerm));
      out += contrib;
    }
  }
  return out;
}




// II - IMPLEMENTATION of the class twoElectrons:


// II.1 Constructors:
/* II.1.1 - construct given final time and number of iterations
  - input: T and m_nIt
  - output: construct the object
*/
twoElectrons_CP::twoElectrons_CP(double T, unsigned int nSteps){
  m_nIt = nSteps;
  assert(m_nIt>1);
  m_tFin = T;
  assert(T>0);
  m_dt = m_tFin/(m_nIt - 1);

}

// II.1.2: give the physical space dimensions:
twoElectrons_CP::twoElectrons_CP(double L, unsigned int dim, vector<unsigned int> Ndof, MPI_Comm theComm){
  m_L = L;
  m_dim = dim;
  m_Ndof = Ndof;
  m_theComm  = theComm;

  vector<unsigned int> Dofper(dim);
  unsigned int ndof =1;
  for (unsigned int i=0; i<dim; i++)
  {
	assert( Ndof[i] % 2 == 1);
	Dofper[i] = Ndof[i];
        ndof *= Ndof[i];
  }

  m_ndof = ndof;

  vector<double> box(2*dim);
  for (unsigned int i=0; i<dim; i++)
  {
	box[2*i] = -L;
        box[2*i+1] = L;
  }

  finiteDifferencesPer testSpace(dim, Dofper,box, m_theComm);
  setFiniteDifferences(testSpace);
}


// II.2 : set finite differences in space:
void twoElectrons_CP::setFiniteDifferences(finiteDifferencesPer& testSpace)
{
	m_testSpace = testSpace;
  m_ndof = testSpace.nDof();
  m_Ndof = testSpace.struc().nDof_var();
}


/* II.3 set discretized matrices:
*/
void twoElectrons_CP::setMatrices(mat& masse, mat& Laplacian, mat& Coulombz, mat& Potxy, vector<mat>& D, vector<mat>& multxyz)
{
  m_Laplacian = Laplacian;
	m_masse = masse;
	m_Potxy = Potxy;
  m_Coulombz = Coulombz;
	m_D = D;
	m_multxyz = multxyz;
}


/* II.4 set discretized matrices:
*/
void twoElectrons_CP::setMatrices()
{
	 /////////////////////////////////

    // Compute the mass matrix:
    m_testSpace.compute_mass();
    mat masse = m_testSpace.mass();
    // Test a Laplacian matrix assembly, without boundaries:

    mat Laplacian(m_ndof, m_ndof, m_theComm);
    for(unsigned int iDof=0; iDof<m_ndof; iDof++){
      // Assemble the bulk:
     // if(!testSpace.isItOnBoundary(iDof)){
        double diagVal = 0.0;
        for(unsigned int iVar=0; iVar<m_testSpace.dim(); iVar++){
          diagVal += 2.0/(m_testSpace.dx(iVar)*m_testSpace.dx(iVar));
        }
        //Laplacian(iDof,iDof) = diagVal;
        Laplacian.setMatEl(iDof,iDof,diagVal);

        // number of "star points" = 2*n with increments [0,..,±1,...]
        for(unsigned int iVar=0; iVar<m_testSpace.dim(); iVar++){
          vector<int> inc_plus(m_testSpace.dim(),0);
          inc_plus[iVar] = 1;
          unsigned int lin_plus = m_testSpace.linIndIncrementPer(iDof,inc_plus);
          //Laplacian(iDof,lin_plus) = -1.0/(m_testSpace.dx(iVar)*m_testSpace.dx(iVar));
          double val_plus = -1.0/(m_testSpace.dx(iVar)*m_testSpace.dx(iVar));
          Laplacian.setMatEl(iDof,lin_plus,val_plus);
          vector<int> inc_minus(m_testSpace.dim(),0);
          inc_minus[iVar] = -1;
          unsigned int lin_minus = m_testSpace.linIndIncrementPer(iDof,inc_minus);
          //cout << "lin_plus = " << lin_plus << "   lin_minus=" << lin_minus <<  endl;
          //Laplacian(iDof,lin_minus) = -1.0/(m_testSpace.dx(iVar)*m_testSpace.dx(iVar));
          double val_min = -1.0/(m_testSpace.dx(iVar)*m_testSpace.dx(iVar));
          Laplacian.setMatEl(iDof,lin_minus,val_min);
        }
    }
    Laplacian.finalize();
    //Laplacian.print();

//A changer
    /*double d = 1.5;
    mat Potxy(m_ndof, m_ndof, m_theComm);
    for(unsigned int iDof=0; iDof<m_ndof; iDof++){

	vector<unsigned int> ind = m_testSpace.struc().lin2sub(iDof);
        vector<double> point = m_testSpace.compute_point(ind);
        double diagValp = (point[0] + d/2)*(point[0] + d/2);
	double diagValm = (point[0] - d/2)*(point[0] - d/2);
        for(unsigned int iVar=1; iVar<m_testSpace.dim(); iVar++){

          diagValp += point[iVar]*point[iVar];
	  diagValm += point[iVar]*point[iVar];
        }
        Potxy(iDof,iDof) = (-1.0)/(0.01 + sqrt(diagValp)) + (-1.0)/(0.01 + sqrt(diagValm));
    }
    Potxy.finalize();
    Potxy.print();*/

    mat Potxy(m_ndof, m_ndof, m_theComm);
    for(unsigned int iDof=0; iDof<m_ndof; iDof++){
	      vector<unsigned int> ind = m_testSpace.struc().lin2sub(iDof);
        vector<double> point = m_testSpace.compute_point(ind);
        double diagValp = (point[0])*(point[0]);
        for(unsigned int iVar=1; iVar<m_testSpace.dim(); iVar++){
          diagValp += point[iVar]*point[iVar];
        }
        double val = (-3.0)/(0.01 + sqrt(diagValp));
        //Potxy(iDof,iDof) = (-3.0)/(0.01 + sqrt(diagValp));
        Potxy.setMatEl(iDof,iDof,val);
    }
    Potxy.finalize();
    //Potxy.print();


    mat Coulombz(m_ndof, m_ndof, m_theComm);
    for(unsigned int iDof=0; iDof<m_ndof; iDof++){
	      vector<unsigned int> ind = m_testSpace.struc().lin2sub(iDof);
        vector<double> point = m_testSpace.compute_point(ind);
        double diagVal = 0;
        for(unsigned int iVar=0; iVar<m_testSpace.dim(); iVar++){

          diagVal += point[iVar]*point[iVar];
        }
        double val = 1.0/(0.01+ sqrt(diagVal));
        //Coulombz(iDof,iDof) = 1.0/(0.01+ sqrt(diagVal));
        Coulombz.setMatEl(iDof,iDof,val);

    }
    Coulombz.finalize();
    //Coulombz.print();

    vector<mat> D(m_testSpace.dim());
    for(unsigned int idim=0; idim<m_testSpace.dim(); idim++){
	    // Test a Laplacian matrix assembly, without boundaries:
    	D[idim].init(m_ndof, m_ndof, m_theComm);
    	for(unsigned int iDof=0; iDof<m_ndof; iDof++){

          vector<int> inc_plus(m_testSpace.dim(),0);
          inc_plus[idim] = 1;
          unsigned int lin_plus = m_testSpace.linIndIncrementPer(iDof,inc_plus);
          double val_plus =  -0.5/(m_testSpace.dx(idim));
          //D[idim](iDof,lin_plus) = -0.5/(m_testSpace.dx(idim));
          D[idim].setMatEl(iDof,lin_plus,val_plus);
          vector<int> inc_minus(m_testSpace.dim(),0);
          inc_minus[idim] = -1;
          unsigned int lin_minus = m_testSpace.linIndIncrementPer(iDof,inc_minus);
          double val_min = 0.5/(m_testSpace.dx(idim));
          //D[idim](iDof,lin_minus) = 0.5/(m_testSpace.dx(idim));
          D[idim].setMatEl(iDof,lin_minus,val_min);
        }

    	D[idim].finalize();
   	  //D[idim].print();
    }


     vector<mat> multxyz(m_testSpace.dim());
      for(unsigned int idim=0; idim<m_testSpace.dim(); idim++){


    	multxyz[idim].init(m_ndof, m_ndof, m_theComm);


    	for(unsigned int iDof=0; iDof<m_ndof; iDof++){

		      vector<unsigned int> ind = m_testSpace.struc().lin2sub(iDof);
        	vector<double> point = m_testSpace.compute_point(ind);
        	double diagVal = point[idim];
        	//multxyz[idim](iDof, iDof) = diagVal;
          multxyz[idim].setMatEl(iDof,iDof,diagVal);
        }
        multxyz[idim].finalize();
        //multxyz[idim].print();
    }

	m_Laplacian = Laplacian;
	m_masse = masse;
	m_Potxy = Potxy;
  m_Coulombz = Coulombz;
	m_D = D;
	m_multxyz = multxyz;
}



/* II.5 Create operatorTensors
*/

// Identity:
void twoElectrons_CP::setOperatorId()
{

  vector< vector<mat> > Op_ir_;

	vector<mat> masse_mat;
	masse_mat.resize(3);
	masse_mat[0] = m_masse;
	masse_mat[2] = m_masse;
	masse_mat[1] = m_masse;

	Op_ir_.resize(1);
	Op_ir_[0] = masse_mat;

	MPI_Comm theComm = m_masse.comm();
	m_Id.init(Op_ir_,theComm);
}


// set the Hamiltonian operator:
void twoElectrons_CP::setOperatorH0()
{

	vector< vector<mat> > Op_even;
	vector< vector<mat> > Op_odd;

	vector<mat> Lap1_mat;
	Lap1_mat.resize(3);
	Lap1_mat[0] = m_Laplacian;
	Lap1_mat[2] = m_masse;
	Lap1_mat[1] = m_masse;

	vector<mat> Lap2_mat;
	Lap2_mat.resize(3);
	Lap2_mat[0] = m_masse;
	Lap2_mat[2] = m_Laplacian;
	Lap2_mat[1] = m_masse;

	mat TwotimesLap = 2.0*m_Laplacian;

	vector<mat> Lap3_mat;
	Lap3_mat.resize(3);
	Lap3_mat[0] = m_masse;
	Lap3_mat[2] = m_masse;
	Lap3_mat[1] = TwotimesLap;

	vector<mat> Coulomb_mat;
	Coulomb_mat.resize(3);
	Coulomb_mat[0] = m_masse;
	Coulomb_mat[2] = m_masse;
	Coulomb_mat[1] = m_Coulombz;

	vector<mat> Pot1_mat;
	Pot1_mat.resize(3);
	Pot1_mat[0] = m_Potxy;
	Pot1_mat[2] = m_masse;
	Pot1_mat[1] = m_masse;

	vector<mat> Pot2_mat;
	Pot2_mat.resize(3);
	Pot2_mat[0] = m_masse;
	Pot2_mat[2] = m_Potxy;
	Pot2_mat[1] = m_masse;


	unsigned int dim = m_D.size();

  Op_even.resize(6);
  Op_odd.resize(2*dim);
	Op_even[0] = Lap1_mat;
	Op_even[1] = Lap2_mat;
	Op_even[2] = Lap3_mat;
	Op_even[3] = Coulomb_mat;
	Op_even[4] = Pot1_mat;
	Op_even[5] = Pot2_mat;

	for (int i = 0; i<dim; i++)
	{
		vector<mat> Dxi_mat;
		Dxi_mat.resize(3);
		Dxi_mat[0] = 2*m_D[i];
		Dxi_mat[2] = m_masse;
		Dxi_mat[1] = m_D[i];

		Op_odd[2*i] = Dxi_mat;

	 	vector<mat> Dxi_mat2;
		Dxi_mat2.resize(3);
		Dxi_mat2[0] = m_masse;
		Dxi_mat2[2] = -2*m_D[i];
		Dxi_mat2[1] = m_D[i];

		Op_odd[2*i+1] = Dxi_mat2;
	}

	MPI_Comm theComm = m_masse.comm();

	m_H0_even.init(Op_even,theComm);
	m_H0_odd.init(Op_odd,theComm);
}


// (II.6) Set an initial condition (by reference !!)
void twoElectrons_CP::setInitialCondition(CP3var& u_re, CP3var& u_ro, CP3var& u_ie, CP3var& u_io){
  m_u_re_old = u_re;
  m_u_ro_old = u_ro;
  m_u_ie_old = u_ie;
  m_u_io_old = u_io;
}


/* (II.7) auxiliary function that reconstruct the wave function in 1d
  - input: string fileName, iteration in time it
  - output: save field in vtk
*/
void twoElectrons_CP::save_1d_wave_fun_vtk(string fileName, unsigned int it){
  const unsigned int nDof = m_Ndof[0] * m_Ndof[0];
  mat sol_to_save(m_Ndof[0], m_Ndof[0], m_u_re.comm());
  for(unsigned int i=0; i<m_Ndof[0]; i++){
    for(unsigned int j=0; j<m_Ndof[0]; j++){
      int k = i-j + (m_Ndof[0]-1)/2;
      int k_p = k+1;
      if(k<0){
        k += m_Ndof[0];
      }
      if(k>=m_Ndof[0]){
        k -= m_Ndof[0];
      }
      if(k_p<0){
        k_p += m_Ndof[0];
      }
      if(k_p>=m_Ndof[0]){
        k_p -= m_Ndof[0];
      }
      double val_even = m_u_re(i,k,j);
      double val_even_p = m_u_re(i,k_p,j);
      double val_even_2 = m_u_re(j,k,i);
      double val_even_p2 = m_u_re(j,k_p,i);

      double val_odd = m_u_ro(i,k,j);
      double val_odd_p = m_u_ro(i,k_p,j);
      double val_odd_2 = m_u_ro(j,k,i);
      double val_odd_p2 = m_u_ro(j,k_p,i);

      double value = 0.5*(val_even+val_even_p) - 0.5*(val_even_2+val_even_p2);
      value += 0.5*(val_odd+val_odd_p) + 0.5*(val_odd_2+val_odd_p2);
      sol_to_save.setMatEl(i,j, value);
    }
  }
  sol_to_save.finalize();

  stringstream st;
  st << it;
  string saveName = fileName +"_"+st.str()+ ".vtk";
  unsigned int nx = m_Ndof[0];
  unsigned int ny = m_Ndof[0];
  unsigned int nz = 1;
  unsigned int numOfVal = nx*ny*nz;

  ofstream outfile (saveName.c_str());

  outfile << "# vtk DataFile Version 2.0 " <<endl;
  outfile << "Data animation" << endl;
  outfile << "ASCII" <<endl;
  outfile << "DATASET STRUCTURED_POINTS" <<endl;
  outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
  outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
  outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
  outfile << "POINT_DATA " << numOfVal << endl;
  outfile << "SCALARS Field float" << endl;
  outfile << "LOOKUP_TABLE default" <<endl;
  for(unsigned int i=0; i<m_Ndof[0]; i++){
    for(unsigned int j=0; j<m_Ndof[0]; j++){
      double value = sol_to_save(j,i);
      if(fabs(value) < 1.0e-9){value = 0.0;};
      outfile << value << endl;
    }
  }
  outfile.close();

}



//(II.) AUXILIARY FUNCTIONS IMPLEMENTATION:

//Function which computes an integral of the form
//\int_{x,y}r(x)s(y)w(x-y)
double twoElectrons_CP::L2scalprod(vec& r, vec& s, vec& w){
	assert(r.size()==s.size());
	assert(r.size() == w.size());
	vec conv_ws_real, conv_ws_imag;
        int N = r.size();

	MPI_Comm theComm = m_masse.comm();

	vec empty_s(N, theComm);
	vec empty_w(N, theComm);
	for (unsigned int iDof=0; iDof < N; iDof++)
	{
    double val = 0.0;
		empty_s.setVecEl(iDof,val);
		empty_w.setVecEl(iDof,val);
	}
	empty_s.finalize();
	empty_w.finalize();

	if (m_testSpace.dim()==1){
		convolution_1d_schroedinger(s,empty_s,w, empty_w, conv_ws_real, conv_ws_imag);
    conv_ws_real*= (m_testSpace.bounds(1)-m_testSpace.bounds(0));
		//Normalement conv_ws_imag==0
	}
	else if (m_testSpace.dim()==2){
		convolution_2d(m_testSpace.dofPerDim()[0]-1, m_testSpace.dofPerDim()[1]-1, s,empty_s,w, empty_w, conv_ws_real, conv_ws_imag);
		//Normalement conv_ws_imag==0
		// Attention!!! Tester!!!!
	}
	else if (m_testSpace.dim()==3){
		convolution_3d(m_testSpace.dofPerDim()[0]-1, m_testSpace.dofPerDim()[1]-1, m_testSpace.dofPerDim()[2]-1, s,empty_s,w, empty_w, conv_ws_real, conv_ws_imag);
		//Normalement conv_ws_imag==0
		//Attention!!! Tester!!!
	}

	double temp = scalProd(r,conv_ws_real)*m_testSpace.dx(0);
	//cout << "scal prod done " << endl;
	return temp;
}

//Renvoie le vecteur de la fonction w(-x)
vec twoElectrons_CP::revert(vec& w){
	//w.print();
	int N = w.size();
	MPI_Comm theComm = w.comm();
	vec revert_w(N, theComm);

	//N est necessairement impair

	if (m_testSpace.dim()==1){
		double value = w.getVecEl(0);
		revert_w.setVecEl(0, value);
		for (unsigned int iDof=1; iDof <= (N-1)/2; iDof++) // N has to have an odd value
	   {
			value = w.getVecEl(iDof);
			revert_w.setVecEl(N-iDof, value);

			value = w.getVecEl(N-iDof);
			revert_w.setVecEl(iDof, value);
		}
		revert_w.finalize();

	}
	else if (m_testSpace.dim()==2)
	{
		//TODO !!!
	}
	else if (m_testSpace.dim()==3)
	{
		//TO DO
	}
	return revert_w;
}



//Function which computes an integral of the form
//\int_{x,y} 1/2[ r_k(x) s_k(y) + r_k(y) s_k(x)] 1/2[ w_k(x-y) + w_k(y-x)] 1/2[ r_l(x) s_l(y) + r_l(y) s_l(x)] 1/2[ w_l(x-y) + w_l(y-x)]
double twoElectrons_CP::even_even_L2scalprod(vec& rk, vec& sk, vec& wk, vec& rl, vec& sl, vec& wl)
{
	vec rev_wk = revert(wk);
	vec rev_wl = revert(wl);

	vec wk_ev = wk + rev_wk;
	wk_ev *= 0.5;

	vec wl_ev = wl + rev_wl;
	wl_ev *=0.5;

	double sum = 0;

	//Calcul de \int_{x,y} 1/2 r_k(x) s_k(y) w_k(x-y) 1/2 r_l(x)s_l(y)  w_l(x-y)
	vec w = pointWiseMult(wk_ev,wl_ev);
	vec r1 = pointWiseMult(rk,rl);
	vec s1 = pointWiseMult(sk,sl);

	sum+=L2scalprod(r1, s1, w);

	//Calcul de \int_{x,y} 1/2 r_k(x) s_k(y) w_k(x-y) 1/2 r_l(y)s_l(x)  w_l(x-y)
	vec r2 = pointWiseMult(rk,sl);
	vec s2 = pointWiseMult(sk,rl);

	sum+=L2scalprod(r2, s2, w);

	//Calcul de \int_{x,y} 1/2 r_k(y) s_k(x) w_k(x-y) 1/2 r_l(x)s_l(y)  w_l(x-y)
	sum+=L2scalprod(s2, r2, w);

	//Calcul de \int_{x,y} 1/2 r_k(y) s_k(x) w_k(x-y) 1/2 r_l(y)s_l(x)  w_l(x-y)
	sum+=L2scalprod(s1, r1, w);

	sum = sum*(0.5*0.5);

	r1.clear();
	s1.clear();
	r2.clear();
	s2.clear();
	w.clear();
	return sum;
}


//Function which computes an integral of the form
//\int_{x,y} 1/2[ r_k(x) s_k(y) + r_k(y) s_k(x)] 1/2[ w_k(x-y) + w_k(y-x)] 1/2[ r_l(x) s_l(y) - r_l(y) s_l(x)] 1/2[ w_l(x-y) - w_l(y-x)]
double twoElectrons_CP::even_odd_L2scalprod(vec& rk, vec& sk, vec& wk, vec& rl, vec& sl, vec& wl)
{
	vec rev_wk = revert(wk);
	vec rev_wl = revert(wl);

	vec wk_ev = wk + rev_wk;
	wk_ev *= 0.5;

	vec wl_odd = wl - rev_wl;
	wl_odd *=0.5;

	double sum = 0;

	//Calcul de \int_{x,y} 1/2 r_k(x) s_k(y)  w_k(x-y) 1/2 r_l(x)s_l(y) w_l(x-y)
	vec r1 = pointWiseMult(rk,rl);
	vec w = pointWiseMult(wk_ev,wl_odd);
	vec s1 = pointWiseMult(sk,sl);

	sum+=L2scalprod(r1, s1, w);


	//Calcul de \int_{x,y} 1/2 r_k(x) s_k(y) w_k(x-y) 1/2 r_l(y)s_l(x)  w_l(x-y)
	vec r2 = pointWiseMult(rk,sl);
	vec s2 = pointWiseMult(sk,rl);

	sum-=L2scalprod(r2, s2, w);

	//Calcul de \int_{x,y} 1/2 r_k(y) s_k(x)  w_k(x-y) 1/2 r_l(x)s_l(y) w_l(x-y)
	sum+=L2scalprod(s2, r2, w);

	//Calcul de \int_{x,y} 1/2 r_k(y) s_k(x) 1/2 w_k(x-y) 1/2 r_l(y)s_l(x) 1/2 w_l(x-y)
	sum-=L2scalprod(s1, r1, w);

	sum = sum*(0.5*0.5);

	r1.clear();
	s1.clear();
	r2.clear();
	s2.clear();
	w.clear();
	return sum;
}


//Function which computes an integral of the form
//\int_{x,y} 1/2[ r_k(x) s_k(y) - r_k(y) s_k(x)] 1/2[ w_k(x-y) - w_k(y-x)] 1/2[ r_l(x) s_l(y) - r_l(y) s_l(x)] 1/2[ w_l(x-y) - w_l(y-x)]
double twoElectrons_CP::odd_odd_L2scalprod(vec& rk, vec& sk, vec& wk, vec& rl, vec& sl, vec& wl)
{
	vec rev_wk = revert(wk);
	vec rev_wl = revert(wl);

	vec wk_odd = wk - rev_wk;
	wk_odd *= 0.5;

	vec wl_odd = wl - rev_wl;
	wl_odd *=0.5;

	double sum = 0;

	//Calcul de \int_{x,y} 1/2 r_k(x) s_k(y) w_k(x-y) 1/2 r_l(x)s_l(y) w_l(x-y)
	vec r1 = pointWiseMult(rk,rl);
	vec w = pointWiseMult(wk_odd,wl_odd);
	vec s1 = pointWiseMult(sk,sl);

	sum+=L2scalprod(r1, s1, w);

	//Calcul de \int_{x,y} 1/2 r_k(x) s_k(y)  w_k(x-y) 1/2 r_l(y)s_l(x)  w_l(x-y)
	vec r2 = pointWiseMult(rk,sl);
	vec s2 = pointWiseMult(sk,rl);

	sum-=L2scalprod(r2, s2, w);

	//Calcul de \int_{x,y} 1/2 r_k(y) s_k(x) w_k(x-y) 1/2 r_l(x)s_l(y) w_l(x-y)
	sum-=L2scalprod(s2, r2, w);


	//Calcul de \int_{x,y} 1/2 r_k(y) s_k(x) w_k(x-y) 1/2 r_l(y)s_l(x) w_l(x-y)
	sum+=L2scalprod(s1, r1, w);

	sum = sum*(0.5*0.5);

	r1.clear();
	s1.clear();
	r2.clear();
	s2.clear();
	w.clear();

	return sum;
}



// (II.) rank1 approximation of a function F(x, x-y, y):
// auxiliary: compute int_y g(x-y)s(y) dy
vec twoElectrons_CP::conv(vec& w, vec& s){
	vec conv_ws_real, conv_ws_imag;
  const unsigned int N = w.size();

	MPI_Comm theComm = m_masse.comm();

	vec empty_s(N, theComm);
	vec empty_w(N, theComm);
	for (unsigned int iDof=0; iDof < N; iDof++){
    double val = 0.0;
		empty_s.setVecEl(iDof,val);
		empty_w.setVecEl(iDof,val);
	}
	empty_s.finalize();
	empty_w.finalize();

	if (m_testSpace.dim()==1){
		convolution_1d_schroedinger(s,empty_s,w, empty_w, conv_ws_real, conv_ws_imag);
    conv_ws_real*= (m_testSpace.bounds(1)-m_testSpace.bounds(0));
		//Normalement conv_ws_imag==0
	}
	else if (m_testSpace.dim()==2){
		convolution_2d(m_testSpace.dofPerDim()[0]-1, m_testSpace.dofPerDim()[1]-1, s,empty_s,w, empty_w, conv_ws_real, conv_ws_imag);
		//Normalement conv_ws_imag==0
		// Attention!!! Tester!!!!
	}
	else if (m_testSpace.dim()==3){
		convolution_3d(m_testSpace.dofPerDim()[0]-1, m_testSpace.dofPerDim()[1]-1, m_testSpace.dofPerDim()[2]-1, s,empty_s,w, empty_w, conv_ws_real, conv_ws_imag);
		//Normalement conv_ws_imag==0
		//Attention!!! Tester!!!
	}
	//cout << "scal prod done " << endl;
	return conv_ws_real;
}


// compute the norm squared of a function sum_i r_i(x)g_i(x-y)s_i(y)
double twoElectrons_CP::norm2xy(CP3var& F){
  double err_0 = 0.0;
  for(unsigned int iTerm=0; iTerm<F.rank(); iTerm++){
    for(unsigned int jTerm=0; jTerm<F.rank(); jTerm++){
      vec tmp_r_i = F.R(iTerm);
      vec tmp_r_j = F.R(jTerm);
      vec r_factor = pointWiseMult(tmp_r_i,tmp_r_j);
      vec tmp_g_i = F.G(iTerm);
      vec tmp_g_j = F.G(jTerm);
      vec g_factor = pointWiseMult(tmp_g_i,tmp_g_j);
      vec tmp_s_i = F.S(iTerm);
      vec tmp_s_j = F.S(jTerm);
      vec s_factor = pointWiseMult(tmp_s_i,tmp_s_j);
      double value = L2scalprod(r_factor, s_factor, g_factor);
      err_0 += value;
      r_factor.clear();
      g_factor.clear();
      s_factor.clear();
    }
  }
  return err_0;
}

// tol: relative tolerance
void twoElectrons_CP::rank1_up(CP3var& F, vec& r, vec& g, vec& s, double tol, bool verbose){
  const unsigned int MaxIt = 10;
  // initialising the step
  r.init(F.nDof_var(0), F.comm());
  r.finalize();

  vec guess_g = rand(F.nDof_var(1), F.comm());
  guess_g *= 1.0e-1;
  guess_g += F.G(0);
  //vec guess_g; guess_g.copyVecFrom(F.G(0));
  vec guess_s = rand(F.nDof_var(2), F.comm());
  guess_s *= 1.0e-1;
  guess_s += F.S(0);
  //vec guess_s; guess_s.copyVecFrom(F.S(0));

  g.copyVecFrom(guess_g);
  s.copyVecFrom(guess_s);

  double pre_norm_s = 1.0/s.norm();
  s *= pre_norm_s;
  double pre_norm_g = 1.0/g.norm();
  g *= pre_norm_g;

  // free the memory:
  guess_s.clear();
  guess_g.clear();

  double err_0 = 0.0;
  for(unsigned int iTerm=0; iTerm<F.rank(); iTerm++){
    for(unsigned int jTerm=0; jTerm<F.rank(); jTerm++){
      vec tmp_r_i = F.R(iTerm);
      vec tmp_r_j = F.R(jTerm);
      vec r_factor = pointWiseMult(tmp_r_i,tmp_r_j);
      vec tmp_g_i = F.G(iTerm);
      vec tmp_g_j = F.G(jTerm);
      vec g_factor = pointWiseMult(tmp_g_i,tmp_g_j);
      vec tmp_s_i = F.S(iTerm);
      vec tmp_s_j = F.S(jTerm);
      vec s_factor = pointWiseMult(tmp_s_i,tmp_s_j);
      double value = L2scalprod(r_factor, s_factor, g_factor);
      err_0 += value;
      r_factor.clear();
      g_factor.clear();
      s_factor.clear();
    }
  }

  if(verbose){cout << "Norm of F = " << sqrt(err_0) << endl;};
  double rel_err = 1.0;
  double rel_err_old = 2.0;
  unsigned int it = 0;
  while( (fabs(rel_err_old-rel_err)>tol) && (it<MaxIt) ){
    //cout << "Iteration " << it << endl;

    // 1 - update r:
    vec tmp1_r = pointWiseMult(g,g);
    vec tmp2_r = pointWiseMult(s,s);
    vec conv_12_r = conv(tmp1_r, tmp2_r);
    tmp1_r.clear();
    tmp2_r.clear();
    vec rhs_r(F.nDof_var(0), F.comm());
    for(unsigned int iTerm=0; iTerm<F.rank(); iTerm++){
      vec tmp_g = F.G(iTerm);
      vec tmp1 = pointWiseMult(tmp_g,g);
      vec tmp_s = F.S(iTerm);
      vec tmp2 = pointWiseMult(tmp_s,s);
      vec conv_tmp = conv(tmp1, tmp2);
      tmp1.clear();
      tmp2.clear();
      vec tmp_r = F.R(iTerm);
      vec toAdd = pointWiseMult(tmp_r, conv_tmp);
      rhs_r += toAdd;
      toAdd.clear();
      conv_tmp.clear();
    }
    // update r;
    for(unsigned int iDof=0; iDof<F.nDof_var(0); iDof++){
      double value = rhs_r.getVecEl(iDof);
      double denom = conv_12_r.getVecEl(iDof);
      if(fabs(denom)>1.0e-12){
        value = value / denom;
      }
      else{
        value = 0.0;
      }
      r.setVecEl(iDof, value);
    }

    // renormalise r:
    double factor_r = 1.0/r.norm();
    r *= factor_r;
    conv_12_r.clear();


    // 2 - update s:
    vec tmp1_s = pointWiseMult(g,g);
    vec tmp1_s_rev = revert(tmp1_s);
    vec tmp2_s = pointWiseMult(r,r);
    vec conv_12_s = conv(tmp1_s_rev, tmp2_s);
    tmp1_s.clear();
    tmp2_s.clear();
    tmp1_s_rev.clear();
    vec rhs_s(F.nDof_var(2), F.comm());
    for(unsigned int iTerm=0; iTerm<F.rank(); iTerm++){
      vec tmp_g = F.G(iTerm);
      vec tmp1 = pointWiseMult(tmp_g,g);
      vec tmp1_rev = revert(tmp1);

      vec tmp_r = F.R(iTerm);
      vec tmp2 = pointWiseMult(tmp_r,r);
      vec conv_tmp = conv(tmp1_rev, tmp2);
      // free the memory:
      tmp1.clear();
      tmp2.clear();
      tmp1_rev.clear();

      vec tmp_s = F.S(iTerm);
      vec toAdd = pointWiseMult(tmp_s, conv_tmp);
      rhs_s += toAdd;
      toAdd.clear();
      conv_tmp.clear();
    }
    // update s;
    for(unsigned int iDof=0; iDof<F.nDof_var(0); iDof++){
      double value = rhs_s.getVecEl(iDof);
      double denom = conv_12_s.getVecEl(iDof);
      if(fabs(denom)>1.0e-12){
        value = value / denom;
      }
      else{
        value = 0.0;
      }
      s.setVecEl(iDof, value);
    }

    // renormalise s:
    double factor_s = 1.0/s.norm();
    s *= factor_s;
    conv_12_s.clear();


    // 2 - update g:
    vec tmp1_g = pointWiseMult(r,r);
    vec tmp2_g = pointWiseMult(s,s);
    vec tmp2_g_rev = revert(tmp2_g);
    vec conv_12_g = conv(tmp1_g, tmp2_g_rev);
    tmp1_g.clear();
    tmp2_g.clear();
    tmp2_g_rev.clear();
    vec rhs_g(F.nDof_var(1), F.comm());
    for(unsigned int iTerm=0; iTerm<F.rank(); iTerm++){
      vec tmp_r = F.R(iTerm);
      vec tmp1 = pointWiseMult(tmp_r,r);
      vec tmp_s = F.S(iTerm);
      vec tmp2 = pointWiseMult(tmp_s,s);
      vec tmp2_rev = revert(tmp2);
      vec conv_tmp = conv(tmp1, tmp2_rev);
      tmp1.clear();
      tmp2.clear();
      tmp2_rev.clear();
      vec tmp_g = F.G(iTerm);
      vec toAdd = pointWiseMult(tmp_g, conv_tmp);
      rhs_g += toAdd;
      toAdd.clear();
      conv_tmp.clear();
    }
    // update g;
    for(unsigned int iDof=0; iDof<F.nDof_var(0); iDof++){
      double value = rhs_g.getVecEl(iDof);
      double denom = conv_12_g.getVecEl(iDof);
      if(fabs(denom)>1.0e-12){
        value = value / denom;
      }
      else{
        value = 0.0;
      }
      g.setVecEl(iDof, value);
    }
    // free the memory:
    conv_12_g.clear();


    // Compute the error:
    rel_err_old = rel_err;

    double err = err_0;
    for(unsigned int iTerm=0; iTerm<F.rank(); iTerm++){
      vec tmp_r = F.R(iTerm);
      vec r_factor = pointWiseMult(tmp_r,r);
      vec tmp_g = F.G(iTerm);
      vec g_factor = pointWiseMult(tmp_g,g);
      vec tmp_s = F.S(iTerm);
      vec s_factor = pointWiseMult(tmp_s,s);
      double value = L2scalprod(r_factor, s_factor, g_factor);
      r_factor.clear();
      g_factor.clear();
      s_factor.clear();
      err = err - 2.0*value;
    }

    // last term:
    vec r_factor = pointWiseMult(r,r);
    vec s_factor = pointWiseMult(s,s);
    vec g_factor = pointWiseMult(g,g);
    double value = L2scalprod(r_factor, s_factor, g_factor);
    err = err + value;

    r_factor.clear();
    g_factor.clear();
    s_factor.clear();

    rel_err = sqrt(err/err_0);
    if(verbose){cout << "Error = " << sqrt(fabs(err)) << " Rel. err = " << rel_err << endl;};

    it += 1;
  }
}


// overloaded function, taking a guess in the form of a vector<vec>
// tol: relative tolerance
void twoElectrons_CP::rank1_up(CP3var& F, vec& r, vec& g, vec& s, vector<vec>& guess, double tol, bool verbose){
  const unsigned int MaxIt = 10;

  r.copyVecFrom(guess[0]);
  g.copyVecFrom(guess[1]);
  s.copyVecFrom(guess[2]);

  double pre_norm_s = 1.0/s.norm();
  s *= pre_norm_s;
  double pre_norm_g = 1.0/g.norm();
  g *= pre_norm_g;

  double err_0 = 0.0;
  for(unsigned int iTerm=0; iTerm<F.rank(); iTerm++){
    for(unsigned int jTerm=0; jTerm<F.rank(); jTerm++){
      vec tmp_r_i = F.R(iTerm);
      vec tmp_r_j = F.R(jTerm);
      vec r_factor = pointWiseMult(tmp_r_i,tmp_r_j);
      vec tmp_g_i = F.G(iTerm);
      vec tmp_g_j = F.G(jTerm);
      vec g_factor = pointWiseMult(tmp_g_i,tmp_g_j);
      vec tmp_s_i = F.S(iTerm);
      vec tmp_s_j = F.S(jTerm);
      vec s_factor = pointWiseMult(tmp_s_i,tmp_s_j);
      double value = L2scalprod(r_factor, s_factor, g_factor);
      err_0 += value;
      r_factor.clear();
      g_factor.clear();
      s_factor.clear();
    }
  }

  if(verbose){cout << "Norm of F = " << sqrt(err_0) << endl;};
  double rel_err = 1.0;
  double rel_err_old = 2.0;
  unsigned int it = 0;
  while( (fabs(rel_err_old-rel_err)>tol) && (it<MaxIt) ){
    //cout << "Iteration " << it << endl;

    // 1 - update r:
    vec tmp1_r = pointWiseMult(g,g);
    vec tmp2_r = pointWiseMult(s,s);
    vec conv_12_r = conv(tmp1_r, tmp2_r);
    tmp1_r.clear();
    tmp2_r.clear();
    vec rhs_r(F.nDof_var(0), F.comm());
    for(unsigned int iTerm=0; iTerm<F.rank(); iTerm++){
      vec tmp_g = F.G(iTerm);
      vec tmp1 = pointWiseMult(tmp_g,g);
      vec tmp_s = F.S(iTerm);
      vec tmp2 = pointWiseMult(tmp_s,s);
      vec conv_tmp = conv(tmp1, tmp2);
      tmp1.clear();
      tmp2.clear();
      vec tmp_r = F.R(iTerm);
      vec toAdd = pointWiseMult(tmp_r, conv_tmp);
      rhs_r += toAdd;
      toAdd.clear();
      conv_tmp.clear();
    }
    // update r;
    for(unsigned int iDof=0; iDof<F.nDof_var(0); iDof++){
      double value = rhs_r.getVecEl(iDof);
      double denom = conv_12_r.getVecEl(iDof);
      if(fabs(denom)>1.0e-12){
        value = value / denom;
      }
      else{
        value = 0.0;
      }
      r.setVecEl(iDof, value);
    }

    // renormalise r:
    double factor_r = 1.0/r.norm();
    r *= factor_r;
    conv_12_r.clear();


    // 2 - update s:
    vec tmp1_s = pointWiseMult(g,g);
    vec tmp1_s_rev = revert(tmp1_s);
    vec tmp2_s = pointWiseMult(r,r);
    vec conv_12_s = conv(tmp1_s_rev, tmp2_s);
    tmp1_s.clear();
    tmp2_s.clear();
    tmp1_s_rev.clear();
    vec rhs_s(F.nDof_var(2), F.comm());
    for(unsigned int iTerm=0; iTerm<F.rank(); iTerm++){
      vec tmp_g = F.G(iTerm);
      vec tmp1 = pointWiseMult(tmp_g,g);
      vec tmp1_rev = revert(tmp1);

      vec tmp_r = F.R(iTerm);
      vec tmp2 = pointWiseMult(tmp_r,r);
      vec conv_tmp = conv(tmp1_rev, tmp2);
      // free the memory:
      tmp1.clear();
      tmp2.clear();
      tmp1_rev.clear();

      vec tmp_s = F.S(iTerm);
      vec toAdd = pointWiseMult(tmp_s, conv_tmp);
      rhs_s += toAdd;
      toAdd.clear();
      conv_tmp.clear();
    }
    // update s;
    for(unsigned int iDof=0; iDof<F.nDof_var(0); iDof++){
      double value = rhs_s.getVecEl(iDof);
      double denom = conv_12_s.getVecEl(iDof);
      if(fabs(denom)>1.0e-12){
        value = value / denom;
      }
      else{
        value = 0.0;
      }
      s.setVecEl(iDof, value);
    }

    // renormalise s:
    double factor_s = 1.0/s.norm();
    s *= factor_s;
    conv_12_s.clear();


    // 2 - update g:
    vec tmp1_g = pointWiseMult(r,r);
    vec tmp2_g = pointWiseMult(s,s);
    vec tmp2_g_rev = revert(tmp2_g);
    vec conv_12_g = conv(tmp1_g, tmp2_g_rev);
    tmp1_g.clear();
    tmp2_g.clear();
    tmp2_g_rev.clear();
    vec rhs_g(F.nDof_var(1), F.comm());
    for(unsigned int iTerm=0; iTerm<F.rank(); iTerm++){
      vec tmp_r = F.R(iTerm);
      vec tmp1 = pointWiseMult(tmp_r,r);
      vec tmp_s = F.S(iTerm);
      vec tmp2 = pointWiseMult(tmp_s,s);
      vec tmp2_rev = revert(tmp2);
      vec conv_tmp = conv(tmp1, tmp2_rev);
      tmp1.clear();
      tmp2.clear();
      tmp2_rev.clear();
      vec tmp_g = F.G(iTerm);
      vec toAdd = pointWiseMult(tmp_g, conv_tmp);
      rhs_g += toAdd;
      toAdd.clear();
      conv_tmp.clear();
    }
    // update g;
    for(unsigned int iDof=0; iDof<F.nDof_var(0); iDof++){
      double value = rhs_g.getVecEl(iDof);
      double denom = conv_12_g.getVecEl(iDof);
      if(fabs(denom)>1.0e-12){
        value = value / denom;
      }
      else{
        value = 0.0;
      }
      g.setVecEl(iDof, value);
    }
    // free the memory:
    conv_12_g.clear();


    // Compute the error:
    rel_err_old = rel_err;

    double err = err_0;
    for(unsigned int iTerm=0; iTerm<F.rank(); iTerm++){
      vec tmp_r = F.R(iTerm);
      vec r_factor = pointWiseMult(tmp_r,r);
      vec tmp_g = F.G(iTerm);
      vec g_factor = pointWiseMult(tmp_g,g);
      vec tmp_s = F.S(iTerm);
      vec s_factor = pointWiseMult(tmp_s,s);
      double value = L2scalprod(r_factor, s_factor, g_factor);
      r_factor.clear();
      g_factor.clear();
      s_factor.clear();
      err = err - 2.0*value;
    }

    // last term:
    vec r_factor = pointWiseMult(r,r);
    vec s_factor = pointWiseMult(s,s);
    vec g_factor = pointWiseMult(g,g);
    double value = L2scalprod(r_factor, s_factor, g_factor);
    err = err + value;

    r_factor.clear();
    g_factor.clear();
    s_factor.clear();

    rel_err = sqrt(err/err_0);
    if(verbose){cout << "Error = " << sqrt(fabs(err)) << " Rel. err = " << rel_err << endl;};

    it += 1;
  }
}



// compute guess:
//- returns the index of the F terms, which maximises the projection on the residual
unsigned int twoElectrons_CP::compute_guess(CP3var& residual, CP3var& F){

  unsigned int out = 0;
  double max_scal_prod = 0.0;
  for(unsigned int iTerm=0; iTerm<F.rank(); iTerm++){
    vec tmp_r = F.R(iTerm);
    vec tmp_g = F.G(iTerm);
    vec tmp_s = F.S(iTerm);

    double scal_prod = 0.0;
    for(unsigned int jTerm=0; jTerm<residual.rank(); jTerm++){
      vec tmp_res_r = residual.R(jTerm);
      vec tmp_res_g = residual.G(jTerm);
      vec tmp_res_s = residual.S(jTerm);

      vec r_factor = pointWiseMult(tmp_r, tmp_res_r);
      vec s_factor = pointWiseMult(tmp_s, tmp_res_s);
      vec g_factor = pointWiseMult(tmp_g, tmp_res_g);

      scal_prod += L2scalprod(r_factor, s_factor, g_factor);
      r_factor.clear();
      g_factor.clear();
      s_factor.clear();
    }
    if(fabs(scal_prod)>max_scal_prod){
      out = iTerm;
      max_scal_prod = fabs(scal_prod);
    }
  }

  return out;
}

// rounding in place, ALS:
// tol is the relative tol on input
void twoElectrons_CP::round_CP(CP3var& in, double tol){
  const unsigned int MaxIt = 50;
  const double tol_fp = 1.0e-3*tol;

  // compute initial norm:
  double err_0 = norm2xy(in);

  if(sqrt(fabs(err_0)) > tol){
    CP3var residual;
    residual << in;

    double rel_err = 1.0;
    unsigned int it = 0;
    vector<vec> R_new;
    vector<vec> G_new;
    vector<vec> S_new;
    while( (rel_err>tol) && (it<MaxIt) ){
      // compute the guess: returning index of F to start with:
      unsigned int best_ind = compute_guess(residual, in);
      vector<vec> guess(3);
      guess[0] = in.R(best_ind);
      guess[1] = in.G(best_ind);
      guess[2] = in.S(best_ind);

      // computing one term
      vec r,g,s;
      rank1_up(residual, r, g, s, guess, tol_fp, true);

      // update:
      R_new.push_back(r);
      G_new.push_back(g);
      S_new.push_back(s);

      // update the residual:
      CP3var toSubtract(r,g,s);
      residual.axpy(toSubtract,-1.0);

      // compute residual norm:
      double err = norm2xy(residual);
      rel_err = sqrt(err/err_0);
      cout << "Rank = " << it+1 << "  Err = " << sqrt(err) << "  Rel. Err = " << rel_err << endl;

      it += 1;
    }

    // update the solution:
    in.clear();
    in.set_terms(R_new,G_new,S_new);
  }
  else{
    in.clear();
    vec r(in.nDof_var(0),in.comm());
    vec g(in.nDof_var(1),in.comm());
    vec s(in.nDof_var(2),in.comm());
    in.set_terms(r,g,s);
  }
}




// III - Building a Krylov space:
/* build a Krylov space
  - input: a vector of rhs, an operator block-type, the order of the Krylov
  - output: a vector of TT
*/
void twoElectrons_CP::krylovBuild(vector<CP3var>& g, blockOperatorTensor& A, unsigned int order, double tolRound, vector<vector<CP3var> >& space){
  assert(g.size()== A.size());
  const unsigned int nComp = g.size();
  space.resize(order+1);
  vector<CP3var> firstTerm(nComp);
  for(unsigned int iComp=0; iComp<nComp; iComp++){
    firstTerm[iComp] << g[iComp]; // the first term is g itself, copy it
  }
  space[0] = firstTerm;
  for(unsigned int i=1; i<order+1; i++){
    vector<CP3var> iTerm(nComp);
    for(unsigned int iComp=0; iComp<nComp; iComp++){
      operatorTensor thisOp = A.A(iComp,0);
      CP3var termComp;
      if(!thisOp.isZero()){
        termComp = thisOp * space[i-1][0];
        termComp *= A.coeffs(iComp,0);
      }
      else{
        termComp.zero(g[0].nDof_var());
      }
      for(unsigned int j=1; j<nComp; j++){
        cout << " i = " << iComp <<  "  j = " << j << endl;
        operatorTensor thisOp = A.A(iComp,j);
        if(!thisOp.isZero()){
          CP3var tmp = thisOp * space[i-1][j];
          tmp *= A.coeffs(iComp,j);
          termComp += tmp;
        }
      }
      round_CP(termComp, tolRound);
      iTerm[iComp] = termComp;
    }
    space[i] = iTerm;
  }
}

/* solving a linear system through multiple Krylov iterations:
  - input: vector<TT> rhs, block operator, order of krylov, rounding tol for compression,
  - output: vector<TT> the solution
*/
void twoElectrons_CP::krylovMultipleIter(vector<CP3var>& g, blockOperatorTensor& A, unsigned int order, double tol, vector<CP3var>& x){

  const unsigned int nComp = 4;
  const double tol_sol = 1.0e-4*tol; // the compression of the roundings is smaller than the tol

  // init the sol as empty:
  x.resize(nComp);
  double initResNorm = 0.0;
  for(unsigned int iComp=0; iComp<nComp; iComp++){
    x[iComp].init(g[iComp].nDof_var());
    initResNorm += g[iComp].norm2CP();
  }

  double rel_err = 1.0;

  while(rel_err>tol){

    vector<vector<CP3var> > krylSpace;
    krylovBuild(g, A, order, tol_sol, krylSpace);

    cout << "Krylov space size = " << krylSpace.size() << endl;

    // computing the system matrix:
    mat sys_ls(order, order, g[0].comm());
    for(unsigned int iRow=0; iRow<order; iRow++){
      for(unsigned int iCol=0; iCol<=iRow; iCol++){
        double entry = 0.0;
        for(unsigned int iComp=0; iComp<nComp; iComp++){
          entry += krylSpace[iRow+1][iComp] * krylSpace[iCol+1][iComp];
        }
        sys_ls.setMatEl(iRow,iCol,entry);
        if(iRow != iCol){
          sys_ls.setMatEl(iCol,iRow,entry);
        }
      }
    }
    sys_ls.finalize();
    //sys_ls.print();

    // computing the rhs:
    vec rhs_ls(order,g[0].comm());
    for(unsigned int iRow=0; iRow<order; iRow++){
      double entry = 0.0;
      for(unsigned int iComp=0; iComp<nComp; iComp++){
        entry += krylSpace[iRow+1][iComp] * g[iComp];
      }
      rhs_ls.setVecEl(iRow, entry);
    }
    rhs_ls.finalize();

    vec sol_ls = sys_ls / rhs_ls;
    //sol_ls.print();

    // evaluating the residual of the ls:
    double norm2_g = 0.0;
    for(unsigned int iComp=0; iComp<nComp; iComp++){
      norm2_g += g[iComp] * g[iComp];
    }

    double res = norm2_g;
    res -= 2.0*scalProd(sol_ls,rhs_ls);
    vec tmp = sys_ls * sol_ls;
    res += scalProd(sol_ls, tmp);
    rel_err = sqrt(res/initResNorm);
    cout << "Residual = " <<  rel_err << endl;

    // update the solution and the residual:
    for(unsigned int iComp=0; iComp<nComp; iComp++){
      for(unsigned int i=0; i<order; i++){
        CP3var tmp;
        tmp << krylSpace[i][iComp];
        tmp*= sol_ls.getVecEl(i);
        x[iComp] += tmp;

        CP3var tmp_g;
        tmp_g << krylSpace[i+1][iComp];
        double coeff = -1.0*sol_ls.getVecEl(i);
        tmp_g*= coeff;
        g[iComp] += tmp_g;
      }
    }
    // free the memory:
    tmp.clear();
    sol_ls.clear();
    rhs_ls.clear();
    sys_ls.clear();
    for(unsigned int i=0; i<order+1; i++){
      for(unsigned int iComp=0; iComp<nComp; iComp++){
        krylSpace[i][iComp].clear();
      }
    }
  }
}




// (III.) - solve one step, Crank-Nicolson dynamics
void twoElectrons_CP::one_step_schroedinger(){
  const double dt = m_tFin/m_nIt;
  const double tol_sol = 1.0e-2;
  const unsigned int order = 2;

  const unsigned int nComp = 4;
  vector<CP3var> g(nComp);

  operatorTensor zero;
  zero.set_zero();
  blockOperatorTensor A(4);

  cout << "A block initialised." << endl;
  // first row:
  A.set_A(0,0,m_Id); A.set_coeffs(0,0,1.0);
  A.set_A(0,1,zero); A.set_coeffs(0,1,0.0);
  A.set_A(0,2,m_H0_even); A.set_coeffs(0,2,-0.5*dt);
  A.set_A(0,3,m_H0_odd); A.set_coeffs(0,3,-0.5*dt);
  // second row:
  A.set_A(1,0,zero); A.set_coeffs(1,0,0.0);
  A.set_A(1,1,m_Id); A.set_coeffs(1,1,1.0);
  A.set_A(1,2,m_H0_odd); A.set_coeffs(1,2,-0.5*dt);
  A.set_A(1,3,m_H0_even); A.set_coeffs(1,3,-0.5*dt);
  // third row:
  A.set_A(2,0,m_H0_even); A.set_coeffs(2,0,0.5*dt);
  A.set_A(2,1,m_H0_odd); A.set_coeffs(2,1,0.5*dt);
  A.set_A(2,2,m_Id); A.set_coeffs(2,2,1.0);
  A.set_A(2,3,zero); A.set_coeffs(2,3,0.0);
  // fourth row:
  A.set_A(3,0,m_H0_odd); A.set_coeffs(3,0,0.5*dt);
  A.set_A(3,1,m_H0_even); A.set_coeffs(3,1,0.5*dt);
  A.set_A(3,2,zero); A.set_coeffs(3,2,0.0);
  A.set_A(3,3,m_Id); A.set_coeffs(3,3,1.0);

  cout << "A block computed." << endl;


  // With guess = zero:
  // first component
  g[0] << m_u_re_old;
  CP3var tmp1 = m_H0_even * m_u_ie_old;
  round_CP(tmp1,tol_sol);
  tmp1*= (0.5*dt);
  g[0] += tmp1;
  CP3var tmp2 = m_H0_odd * m_u_io_old;
  round_CP(tmp2,tol_sol);
  tmp2*= (0.5*dt);
  g[0] += tmp2;
  round_CP(g[0],tol_sol);
  cout << "g_0 " << endl;

  // second component
  g[1] << m_u_ro_old;
  CP3var tmp3 = m_H0_even * m_u_io_old;
  round_CP(tmp3,tol_sol);
  tmp3*= (0.5*dt);
  g[1] += tmp3;
  CP3var tmp4 = m_H0_odd * m_u_ie_old;
  round_CP(tmp4,tol_sol);
  tmp4*= (0.5*dt);
  g[1] += tmp4;
  round_CP(g[1],tol_sol);
  cout << "g_1 " << endl;

  // third component
  g[2] << m_u_ie_old;
  CP3var tmp5 = m_H0_even * m_u_re_old;
  round_CP(tmp5,tol_sol);
  tmp5*= (-0.5*dt);
  g[2] += tmp5;
  CP3var tmp6 = m_H0_odd * m_u_ro_old;
  round_CP(tmp6,tol_sol);
  tmp6*= (-0.5*dt);
  g[2] += tmp6;
  round_CP(g[2],tol_sol);
  cout << "g_2 " << endl;

  // fourth component
  g[3] << m_u_io_old;
  CP3var tmp7 = m_H0_even * m_u_ro_old;
  round_CP(tmp7,tol_sol);
  tmp7*= (-0.5*dt);
  g[3] += tmp7;
  CP3var tmp8 = m_H0_odd * m_u_re_old;
  round_CP(tmp8,tol_sol);
  tmp8*= (-0.5*dt);
  g[3] += tmp8;
  round_CP(g[3],tol_sol);
  cout << "g_3 " << endl;

  cout << "g term computed" << endl;

  // compute a Krylov space:
  vector<vector<CP3var> > krylSpace;
  krylovBuild(g, A, order, tol_sol, krylSpace);
  cout << "Krylov space size = " << krylSpace.size() << endl;

}
