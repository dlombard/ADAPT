// Implementation of the finite differences class:
#include "finiteDifferencesPer.h"


// I -- CONSTRUCTORS --

/* I.1 overloaded constructor
  - input: space dimension d, degrees of freedom per dimension, assuming [0,1]^d
  - output: construct the indices structure
*/
finiteDifferencesPer::finiteDifferencesPer(unsigned int space_dim, vector<unsigned int> dof_per_direction){
  assert(dof_per_direction.size()==space_dim);
  m_struct.set_nVar(space_dim);
  m_struct.set_nDof_var(dof_per_direction);
  m_struct.compute_minc();
  // total number of dofs:
  m_nDof = 1;
  for(unsigned int iVar=0; iVar<space_dim; iVar++){
    m_nDof = m_nDof * dof_per_direction[iVar];
  }
  // setting the bounds to [0,1]^d
  m_bounds.resize(2*space_dim);
  for(unsigned int iBound=0; iBound<2*space_dim; iBound++){
    if(iBound%2==0){
      m_bounds[iBound] = 0.0;
    }
    else{
      m_bounds[iBound] = 1.0;
    }
  }
  // dx:
  m_dx.resize(space_dim);
  for(unsigned int iVar=0; iVar<space_dim; iVar++){
    m_dx[iVar] = 1.0/(dof_per_direction[iVar]);
  }
}

/* I.2 overloaded constructor
  - input: space dimension d, degrees of freedom per dimension, assuming [0,1]^d
  - output: construct the indices structure
*/
finiteDifferencesPer::finiteDifferencesPer(unsigned int space_dim, vector<unsigned int> dof_per_direction, vector<double> box_bounds){
  assert(dof_per_direction.size() == space_dim);
  assert(box_bounds.size() == 2*space_dim);
  m_struct.set_nVar(space_dim);
  m_struct.set_nDof_var(dof_per_direction);
  m_struct.compute_minc();
  // total number of dofs:
  m_nDof = 1;
  for(unsigned int iVar=0; iVar<space_dim; iVar++){
    m_nDof = m_nDof * dof_per_direction[iVar];
  }
  // box bounds:
  m_bounds.resize(2*space_dim);
  for(unsigned int iBound=0; iBound<2*space_dim; iBound++){
    m_bounds[iBound] = box_bounds[iBound];
  }
  // dx:
  m_dx.resize(space_dim);
  for(unsigned int iVar=0; iVar<space_dim; iVar++){
    m_dx[iVar] = (m_bounds[2*iVar+1] - m_bounds[2*iVar])/(dof_per_direction[iVar]);
  }
}



// II - FUNCTIONS --

/* II.1 Compute the Mass Matrix = Identity
  - input: none <= the finite differences are already initialised
  - output: m_mass is filled:
*/
void finiteDifferencesPer::compute_mass(){
  m_mass.init(m_nDof,m_nDof);
  for(unsigned int iDof=0; iDof<m_nDof; iDof++){
    m_mass.setMatEl(iDof,iDof, 1.0);
  }
}


/* II.2 Given a vector of indices, compute point coordinates in the box:
  - input: the indices i_1, ..., i_d
  - output: the coordinates of the point x_1, ..., x_d
*/
vector<double> finiteDifferencesPer::compute_point(vector<unsigned int> ind){
  assert(ind.size()==m_struct.nVar());
  vector<double> toBeReturned(m_struct.nVar());
  for(unsigned int iVar=0; iVar<m_struct.nVar(); iVar++){
    toBeReturned[iVar] = m_bounds[2*iVar] + ind[iVar] * m_dx[iVar];
  }
  return toBeReturned;
}

/* II.3 Given a point, compute the lower left corner:
  - input: the point x_1, ..., x_d
  - output: the coordinates of the lower left corner i_1, ..., i_d
*/
vector<unsigned int> finiteDifferencesPer::compute_ll(vector<double> point){
  assert(point.size()==m_struct.nVar());
  vector<unsigned int> toBeReturned(m_struct.nVar());
  for(unsigned int iVar=0; iVar<m_struct.nVar(); iVar++){
    toBeReturned[iVar] = floor( (point[iVar]-m_bounds[2*iVar])/m_dx[iVar] );
  }
  return toBeReturned;
}

/* II.4 Given a function, compute its gradient, first order finite differences:
  - input: the function values in the dofs
  - output: the gradient
*/
vector<vec_real> finiteDifferencesPer::compute_gradient(vec_real& fun){
  assert(fun.size()==m_nDof);
  vector<vec_real> gradient(m_struct.nVar());
  for(unsigned int iVar=0; iVar<m_struct.nVar(); iVar++){
    vec_real df_dx(m_nDof);
    const double factor = 1.0 / m_dx[iVar];
    vector<int> increment(m_struct.nVar(),0);
    increment[iVar] = -1;
    for(unsigned int iDof=0; iDof<m_nDof; iDof++){
      double value = fun(iDof);
      //vector<unsigned int> ind = m_struct.lin2sub(iDof);

     // double fun_p = 0.0;
     // if(ind[iVar]==m_struct.nDof_var(iVar)-1){
        //vector<unsigned int> ind_p(m_struct.nVar());
        //for(unsigned int jVar=0; jVar<m_struct.nVar(); jVar++){
        //  iVar = jVar ? (ind_p[jVar] = ind[iVar]-1) : (ind_p[jVar] = ind[iVar]);
       // }
	//m_struct.sub2lin(ind_p);

        unsigned int lin = linIndIncrementPer(iDof, increment);

        double fun_p = fun(lin);
        value  = value - fun_p;
     /* }
      else{
        vector<unsigned int> ind_p(m_struct.nVar());
        for(unsigned int jVar=0; jVar<m_struct.nVar(); jVar++){
          iVar = jVar ? (ind_p[jVar] = ind[iVar]+1) : (ind_p[jVar] = ind[iVar]);
        }
        unsigned int lin = m_struct.sub2lin(ind_p);
        fun_p = fun(lin);
        value  = fun_p - value;
      }*/

      df_dx.setVecEl(iDof,value * factor);
    }
    gradient[iVar] = df_dx;
  }

  return gradient;
}

/* II.5 Is it on boundary.
 - input: the dof linear index
 - output: check if it is a boundary dof
*/
/*bool finiteDifferencesPer::isItOnBoundary(unsigned int linInd){
  vector<unsigned int> ind = m_struct.lin2sub(linInd);
  bool isIt = false;
  for(unsigned int iVar=0; iVar<m_struct.nVar(); iVar++){
    if( (ind[iVar]==0) || (ind[iVar] == m_struct.nDof_var(iVar)-1) ){
      isIt = true;
    }
  }
  return isIt;
}*/

/* II.6 Which boundary is on.
  - input: the dof linear index
  - output: tuple (index of variable, 0 or 1 )
*/
/*tuple<unsigned int, unsigned int> finiteDifferencesPer::whichBoundaryIsOn(unsigned int linInd){
  vector<unsigned int> ind = m_struct.lin2sub(linInd);
  unsigned int idOfVar = 0;
  unsigned int sign = 0;
  for(unsigned int iVar=0; iVar<m_struct.nVar(); iVar++){
    if (ind[iVar]==0) {
      idOfVar = iVar;
      sign = 0;
    }
    if ( ind[iVar] == m_struct.nDof_var(iVar)-1 ) {
      idOfVar = iVar;
      sign = 1;
    }
  }
  return make_tuple(idOfVar, sign);
}*/

/* II.7 given a liner index, and an increment in terms of indices, return the linear index
  - input: linear index linInd = sub2lin({i_1,...,i_d}), increment
  - output: the linear index whose subindices are {i_1 + inc_1,..., i_d+inc_d}
*/
unsigned int finiteDifferencesPer::linIndIncrement(unsigned int linInd, vector<int> increment){
  assert(increment.size()==m_struct.nVar());
  vector<unsigned int> ind = m_struct.lin2sub(linInd);
  for(unsigned int iVar=0; iVar<m_struct.nVar(); iVar++){
    ind[iVar] = ind[iVar] + increment[iVar];
  }
  return m_struct.sub2lin(ind);
}

/* II.8 given a liner index, and an increment in terms of indices, return the linear index
  - input: linear index linInd = sub2lin({i_1,...,i_d}), increment
  - output: the linear index whose subindices are {i_1 + inc_1,..., i_d+inc_d} modulo the size of the box for periodic boundary conditions
*/
unsigned int finiteDifferencesPer::linIndIncrementPer(unsigned int linInd, vector<int> increment){
  assert(increment.size()==m_struct.nVar());
  vector<unsigned int> ind = m_struct.lin2sub(linInd);
  for(unsigned int iVar=0; iVar<m_struct.nVar(); iVar++){
    int tmp = ind[iVar]+increment[iVar];
    if(tmp>= m_struct.nDof_var(iVar) ){
      ind[iVar] = tmp - m_struct.nDof_var(iVar);
    }
    if (tmp<0){
      unsigned int test = m_struct.nDof_var(iVar) + tmp;
      ind[iVar] = tmp + m_struct.nDof_var(iVar);
    }
    if( (tmp>=0) && (tmp<m_struct.nDof_var(iVar)) ){
      ind[iVar] = tmp;
    }
  }
  return m_struct.sub2lin(ind);
}


/* II.9 Is in box.
 - input: the vector of indices
 - output: check if it is inside the box
*/
/*bool finiteDifferencesPer::isInBox(unsigned int linInd){
  vector<unsigned int> ind = m_struct.lin2sub(linInd);
  bool isIt = true;
  for(unsigned int iVar=0; iVar<m_struct.nVar(); iVar++){
    isIt = isIt & (ind[iVar]>=0) & (ind[iVar]<m_struct.nDof_var(iVar)) ;
  }
  return isIt;
}*/
