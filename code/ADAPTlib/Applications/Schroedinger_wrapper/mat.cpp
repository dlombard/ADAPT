// implementation of the matrix class:
#include "mat.h"

// REAL SEQUENTIAL DENSE MATRICES:

//Overloaded constructors: allocate
mat_real::mat_real(unsigned int n_rows, unsigned int n_cols){
  m_nRows = n_rows;
  m_nCols = n_cols;
  const unsigned int size = n_rows * n_cols;
  m_M = new double[size];
}

mat_real::mat_real(double*& mat, unsigned int n_rows, unsigned int n_cols){
  m_nRows = n_rows;
  m_nCols = n_cols;
  m_M = mat;
}

void mat_real::init(unsigned int n_rows, unsigned int n_cols){
  m_nRows = n_rows;
  m_nCols = n_cols;
  const unsigned int size = n_rows * n_cols;
  m_M = new double[size];
}

// Copy mat from:
void mat_real::copyMatFrom(mat_real& toBeCopied){
  m_nRows = toBeCopied.nRows();
  m_nCols = toBeCopied.nCols();
  const unsigned int size = m_nRows * m_nCols;
  m_M = new double[size];
  for(unsigned int l=0; l<size; l++){
    m_M[l] = toBeCopied(l);
  }
}

// summing up a given matrix:
void mat_real::operator += (mat_real& toBeAdded){
  assert(m_nRows==toBeAdded.nRows());
  assert(m_nCols==toBeAdded.nCols());
  for(unsigned int l=0; l<m_nRows*m_nCols; l++){
    m_M[l] += toBeAdded(l);
  }
}

// subtracting up a given matrix:
void mat_real::operator -= (mat_real& toBeSubtracted){
  assert(m_nRows==toBeSubtracted.nRows());
  assert(m_nCols==toBeSubtracted.nCols());
  for(unsigned int l=0; l<m_nRows*m_nCols; l++){
    m_M[l] -= toBeSubtracted(l);
  }
}

// scaling:
void mat_real::operator *= (double alpha){
  for(unsigned int l=0; l<m_nRows*m_nCols; l++){
    m_M[l] *= alpha;
  }
}




// REAL SPARSE MATRICES:
mat_real_sp::mat_real_sp(unsigned int n_rows, unsigned int n_cols){
  m_nRows = n_rows;
  m_nCols = n_cols;
  pattern = new vector<vector<unsigned int> >;
  pattern->resize(n_rows);
  m_M = new vector<vector<double> >;
  m_M->resize(n_rows);
}
