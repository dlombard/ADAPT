// Implementation of CP3var_real

#include "CP3var.h"

// I.1 Overloaded constructors:

// empty, just assign the dofs:
CP3var_real::CP3var_real(vector<unsigned int> n_dofs){
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = n_dofs[iVar];
  }
  m_rank=0;
}

// assign the modes:
CP3var_real::CP3var_real(vector<vec_real>& R, vector<vec_real>& G, vector<vec_real>& S){
  m_rank=R.size();
  assert(G.size()==m_rank);
  assert(S.size()==m_rank);

  m_nDof_var.resize(m_nVar);
  m_nDof_var[0] = R[0].size();
  m_nDof_var[1] = G[0].size();
  m_nDof_var[2] = S[0].size();

  // do not perform copy, assign by reference.
  m_R = R;
  m_G = G;
  m_S = S;
}


// rank 1 term:
CP3var_real::CP3var_real(vec_real& r, vec_real& g, vec_real& s){
  m_rank=1;
  // define the resolution:
  m_nDof_var.resize(m_nVar);
  m_nDof_var[0] = r.size();
  m_nDof_var[1] = g.size();
  m_nDof_var[2] = s.size();

  // do not perform copy, assign by reference.
  m_R.resize(1);
  m_R[0] = r;
  m_G.resize(1);
  m_G[0] = g;
  m_S.resize(1);
  m_S[0] = s;
}



// (I.1.5) copyTensorFrom:
void CP3var_real::copyTensorFrom(CP3var_real& A){
  m_rank = A.rank();
  m_nDof_var.resize(3);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = A.nDof_var(iVar);
  }
  m_R.resize(m_rank);
  m_G.resize(m_rank);
  m_S.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    vec_real tmp_ar = A.R(iTerm);
    m_R[iTerm].copyVecFrom(tmp_ar);
    vec_real tmp_ag = A.G(iTerm);
    m_G[iTerm].copyVecFrom(tmp_ag);
    vec_real tmp_as = A.S(iTerm);
    m_S[iTerm].copyVecFrom(tmp_as);
  }
}


// (I.2) the zero CP: when the sizes are known(!!!)
void CP3var_real::zero(){
  if(m_rank>1){
    for(unsigned int iTerm=1; iTerm<m_rank; iTerm++){
      m_R[iTerm].clear();
      m_G[iTerm].clear();
      m_S[iTerm].clear();
    }
  }
  m_rank=1;
  m_R.resize(1);
  m_R[0].init(m_nDof_var[0]);
  m_G.resize(1);
  m_G[0].init(m_nDof_var[1]);
  m_S.resize(1);
  m_S[0].init(m_nDof_var[2]);
}

void CP3var_real::zero(vector<unsigned int> n_dofs){
  assert(n_dofs.size()==3);
  m_rank=1;
  m_nDof_var.resize(3);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = n_dofs[iVar];
  }
  m_R.resize(1);
  m_R[0].init(n_dofs[0]);
  m_G.resize(1);
  m_G[0].init(n_dofs[1]);
  m_S.resize(1);
  m_S[0].init(n_dofs[2]);
}

// (I.3) append a vector => increase the rank by 1:
void CP3var_real::append(vec_real& r, vec_real& g, vec_real& s){
  m_rank += 1;
  m_R.push_back(r);
  m_G.push_back(g);
  m_S.push_back(s);
}

// (I.3) append a vector => increase the rank by 1:
void CP3var_real::append(vector<vec_real>& r_modes, vector<vec_real>& g_modes, vector<vec_real>& s_modes){
  const unsigned int nModes = r_modes.size();
  assert(g_modes.size()==nModes);
  assert(s_modes.size()==nModes);
  m_rank += nModes;

  for(unsigned int iMod=0; iMod<nModes; iMod++){
    m_R.push_back(r_modes[iMod]);
    m_G.push_back(g_modes[iMod]);
    m_S.push_back(s_modes[iMod]);
  }
}


// (I.4) function axpy: this = this + alpha*A
void CP3var_real::axpy(CP3var_real& A, double alpha, bool byCopy=true){
  const unsigned int old_rank = m_rank;

  double sign = 1.0;
  if(alpha<0){sign = -1.0;}
  const double factor = pow(fabs(alpha), (1.0/3.0));

  m_rank += A.rank();
  m_R.resize(m_rank);
  m_G.resize(m_rank);
  m_S.resize(m_rank);

  if(byCopy){
    for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
      vec_real tmp_ar = A.R(iTerm);
      m_R[iTerm+old_rank].copyVecFrom(tmp_ar);
      m_R[iTerm+old_rank]*=factor*sign;
      vec_real tmp_ag = A.G(iTerm);
      m_G[iTerm+old_rank].copyVecFrom(tmp_ag);
      m_G[iTerm+old_rank]*= factor;
      vec_real tmp_as = A.S(iTerm);
      m_S[iTerm+old_rank].copyVecFrom(tmp_as);
      m_S[iTerm+old_rank]*= factor;
    }
  }
  else{
    A *= alpha; // changing A!
    for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
      m_R[iTerm+old_rank] = A.R(iTerm);
      m_G[iTerm+old_rank] = A.G(iTerm);
      m_S[iTerm+old_rank] = A.S(iTerm);
    }
  }
}
// (I.4) function axpy: this = this + alpha*A,  (overloaded)
void CP3var_real::axpy(vec_real& r_one, vec_real& g_one, vec_real& s_one, double alpha, bool byCopy=true){
  const unsigned int old_rank = m_rank;

  double sign = 1.0;
  if(alpha<0){sign = -1.0;}
  const double factor = pow(fabs(alpha), (1.0/3.0));

  m_rank += 1;
  m_R.resize(m_rank);
  m_G.resize(m_rank);
  m_S.resize(m_rank);

  if(byCopy){
      m_R[old_rank].copyVecFrom(r_one);
      m_R[old_rank]*= (factor*sign);
      m_G[old_rank].copyVecFrom(g_one);
      m_G[old_rank]*= factor;
      m_S[old_rank].copyVecFrom(s_one);
      m_S[old_rank]*= factor;
  }
  else{
    r_one *= (sign*factor); // changing A!
    g_one *= factor;
    s_one *= factor;

    m_R[old_rank] = r_one;
    m_G[old_rank] = g_one;
    m_S[old_rank] = s_one;
  }
}



// (I.5) apply an operator tensor:
CP3var_real CP3var_real::applyOperator(opTens_real_dense& Op){
  const unsigned int op_rank = Op.nTerms();
  const unsigned int out_rank = m_rank * op_rank;
  vector<vec_real> out_R(out_rank);
  vector<vec_real> out_G(out_rank);
  vector<vec_real> out_S(out_rank);
  unsigned int cc = 0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int iOp=0; iOp<op_rank; iOp++){
      mat_real A_r = Op(iOp,0);
      mat_real A_g = Op(iOp,1);
      mat_real A_s = Op(iOp,2);
      vec_real r = A_r * m_R[iTerm];
      vec_real g = A_g * m_G[iTerm];
      vec_real s = A_s * m_S[iTerm];
      out_R[cc] = r;
      out_G[cc] = g;
      out_S[cc] = s;
      cc += 1;
    }
  }
  CP3var_real output(out_R, out_G, out_S);
  return output;
}

CP3var_real CP3var_real::applyOperator(opTens_real_sparse& Op){
  const unsigned int op_rank = Op.nTerms();
  const unsigned int out_rank = m_rank * op_rank;
  vector<vec_real> out_R(out_rank);
  vector<vec_real> out_G(out_rank);
  vector<vec_real> out_S(out_rank);
  unsigned int cc = 0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int iOp=0; iOp<op_rank; iOp++){
      mat_real_sp A_r = Op(iOp,0);
      mat_real_sp A_g = Op(iOp,1);
      mat_real_sp A_s = Op(iOp,2);
      vec_real r = A_r * m_R[iTerm];
      vec_real g = A_g * m_G[iTerm];
      vec_real s = A_s * m_S[iTerm];
      out_R[cc] = r;
      out_G[cc] = g;
      out_S[cc] = s;
      cc += 1;
    }
  }
  CP3var_real output(out_R, out_G, out_S);
  return output;
}

// (I.) overloading operator * to Apply operator

CP3var_real operator * (opTens_real_dense& Op, CP3var_real& in){
  CP3var_real out = in.applyOperator(Op);
  return out;
}

CP3var_real operator * (opTens_real_sparse& Op, CP3var_real& in){
  CP3var_real out = in.applyOperator(Op);
  return out;
}


// (I.6) norm squared of the CP:
double CP3var_real::norm2CP(){
  double out = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int jTerm=0; jTerm<m_rank; jTerm++){
      double contrib = dot(m_R[iTerm],m_R[jTerm]);
      contrib *= dot(m_G[iTerm],m_G[jTerm]);
      contrib *= dot(m_S[iTerm],m_S[jTerm]);
      out += contrib;
    }
  }
  return out;
}



// (I.) scaling the CP3var_real by a scalar:
void CP3var_real::operator *= (double alpha){
  double sign = 1.0;
  if(alpha<0){sign = -1.0;}
  const double factor = pow(fabs(alpha), (1.0/3.0) );

  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_R[iTerm]*=(factor*sign);
    m_G[iTerm]*=factor;
    m_S[iTerm]*=factor;
  }
}

// (I.) copy operator:
void CP3var_real::operator << (CP3var_real& A){
  copyTensorFrom(A);
}

// (I.) operator += : implementation without copy (!!!)
void CP3var_real::operator += (CP3var_real& A){
  const unsigned int old_rank = m_rank;

  m_rank += A.rank();
  m_R.resize(m_rank);
  m_G.resize(m_rank);
  m_S.resize(m_rank);

  for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
    m_R[iTerm+old_rank] = A.R(iTerm);
    m_G[iTerm+old_rank] = A.G(iTerm);
    m_S[iTerm+old_rank] = A.S(iTerm);
  }
}


// (I.) eval operator:
double CP3var_real::operator()(unsigned int i, unsigned int j, unsigned int k){
  double out = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    double contrib = m_R[iTerm](i);
    contrib *= m_G[iTerm](j);
    contrib *= m_S[iTerm](k);
    out += contrib;
  }
  return out;
}



// (I.) compute the scalar product between 2 CP3var_real
double operator * (CP3var_real& A, CP3var_real& B){
  double out = 0.0;
  for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
    for(unsigned int jTerm=0; jTerm<B.rank(); jTerm++){
      vec_real tmp_ar = A.R(iTerm);
      vec_real tmp_br = B.R(jTerm);
      double contrib = dot(tmp_ar,tmp_br);
      vec_real tmp_ag = A.G(iTerm);
      vec_real tmp_bg = B.G(jTerm);
      contrib *= dot(tmp_ag,tmp_bg);
      vec_real tmp_as = A.S(iTerm);
      vec_real tmp_bs = B.S(jTerm);
      contrib *= dot(tmp_as,tmp_bs);
      out += contrib;
    }
  }
  return out;
}




// -- II -- Implementation of the class CP3var_comp:

// I.1 Overloaded constructors:

// empty, just assign the dofs:
CP3var_comp::CP3var_comp(vector<unsigned int> n_dofs){
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = n_dofs[iVar];
  }
  m_rank=0;
}

// assign the modes:
CP3var_comp::CP3var_comp(vector<vec_comp>& R, vector<vec_comp>& G, vector<vec_comp>& S){
  m_rank=R.size();
  assert(G.size()==m_rank);
  assert(S.size()==m_rank);

  m_nDof_var.resize(m_nVar);
  m_nDof_var[0] = R[0].size();
  m_nDof_var[1] = G[0].size();
  m_nDof_var[2] = S[0].size();

  // do not perform copy, assign by reference.
  m_R = R;
  m_G = G;
  m_S = S;
}


// rank 1 term:
CP3var_comp::CP3var_comp(vec_comp& r, vec_comp& g, vec_comp& s){
  m_rank=1;
  // define the resolution:
  m_nDof_var.resize(m_nVar);
  m_nDof_var[0] = r.size();
  m_nDof_var[1] = g.size();
  m_nDof_var[2] = s.size();

  // do not perform copy, assign by reference.
  m_R.resize(1);
  m_R[0] = r;
  m_G.resize(1);
  m_G[0] = g;
  m_S.resize(1);
  m_S[0] = s;
}



// (I.1.5) copyTensorFrom:
void CP3var_comp::copyTensorFrom(CP3var_comp& A){
  m_rank = A.rank();
  m_nDof_var.resize(3);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = A.nDof_var(iVar);
  }
  m_R.resize(m_rank);
  m_G.resize(m_rank);
  m_S.resize(m_rank);
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    vec_comp tmp_ar = A.R(iTerm);
    m_R[iTerm].copyVecFrom(tmp_ar);
    vec_comp tmp_ag = A.G(iTerm);
    m_G[iTerm].copyVecFrom(tmp_ag);
    vec_comp tmp_as = A.S(iTerm);
    m_S[iTerm].copyVecFrom(tmp_as);
  }
}


// (I.2) the zero CP: when the sizes are known(!!!)
void CP3var_comp::zero(){
  if(m_rank>1){
    for(unsigned int iTerm=1; iTerm<m_rank; iTerm++){
      m_R[iTerm].clear();
      m_G[iTerm].clear();
      m_S[iTerm].clear();
    }
  }
  m_rank=1;
  m_R.resize(1);
  m_R[0].init(m_nDof_var[0]);
  m_G.resize(1);
  m_G[0].init(m_nDof_var[1]);
  m_S.resize(1);
  m_S[0].init(m_nDof_var[2]);
}

void CP3var_comp::zero(vector<unsigned int> n_dofs){
  assert(n_dofs.size()==3);
  m_rank=1;
  m_nDof_var.resize(3);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = n_dofs[iVar];
  }
  m_R.resize(1);
  m_R[0].init(n_dofs[0]);
  m_G.resize(1);
  m_G[0].init(n_dofs[1]);
  m_S.resize(1);
  m_S[0].init(n_dofs[2]);
}

// (I.3) append a vector => increase the rank by 1:
void CP3var_comp::append(vec_comp& r, vec_comp& g, vec_comp& s){
  m_rank += 1;
  m_R.push_back(r);
  m_G.push_back(g);
  m_S.push_back(s);
}

// (I.3) append a vector => increase the rank by 1:
void CP3var_comp::append(vector<vec_comp>& r_modes, vector<vec_comp>& g_modes, vector<vec_comp>& s_modes){
  const unsigned int nModes = r_modes.size();
  assert(g_modes.size()==nModes);
  assert(s_modes.size()==nModes);
  m_rank += nModes;

  for(unsigned int iMod=0; iMod<nModes; iMod++){
    m_R.push_back(r_modes[iMod]);
    m_G.push_back(g_modes[iMod]);
    m_S.push_back(s_modes[iMod]);
  }
}


// (I.4) function axpy: this = this + alpha*A
void CP3var_comp::axpy(CP3var_comp& A, double alpha, bool byCopy=true){
  const unsigned int old_rank = m_rank;

  double sign = 1.0;
  if(alpha<0){sign = -1.0;}
  const double factor = pow(fabs(alpha), (1.0/3.0));

  m_rank += A.rank();
  m_R.resize(m_rank);
  m_G.resize(m_rank);
  m_S.resize(m_rank);

  if(byCopy){
    for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
      vec_comp tmp_ar = A.R(iTerm);
      m_R[iTerm+old_rank].copyVecFrom(tmp_ar);
      m_R[iTerm+old_rank]*=factor*sign;
      vec_comp tmp_ag = A.G(iTerm);
      m_G[iTerm+old_rank].copyVecFrom(tmp_ag);
      m_G[iTerm+old_rank]*= factor;
      vec_comp tmp_as = A.S(iTerm);
      m_S[iTerm+old_rank].copyVecFrom(tmp_as);
      m_S[iTerm+old_rank]*= factor;
    }
  }
  else{
    A *= alpha; // changing A!
    for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
      m_R[iTerm+old_rank] = A.R(iTerm);
      m_G[iTerm+old_rank] = A.G(iTerm);
      m_S[iTerm+old_rank] = A.S(iTerm);
    }
  }
}
// (I.4) function axpy: this = this + alpha*A,  (overloaded)
void CP3var_comp::axpy(vec_comp& r_one, vec_comp& g_one, vec_comp& s_one, double alpha, bool byCopy=true){
  const unsigned int old_rank = m_rank;

  double sign = 1.0;
  if(alpha<0){sign = -1.0;}
  const double factor = pow(fabs(alpha), (1.0/3.0));

  m_rank += 1;
  m_R.resize(m_rank);
  m_G.resize(m_rank);
  m_S.resize(m_rank);

  if(byCopy){
      m_R[old_rank].copyVecFrom(r_one);
      m_R[old_rank]*= (factor*sign);
      m_G[old_rank].copyVecFrom(g_one);
      m_G[old_rank]*= factor;
      m_S[old_rank].copyVecFrom(s_one);
      m_S[old_rank]*= factor;
  }
  else{
    r_one *= (sign*factor); // changing A!
    g_one *= factor;
    s_one *= factor;

    m_R[old_rank] = r_one;
    m_G[old_rank] = g_one;
    m_S[old_rank] = s_one;
  }
}



// (I.6) norm squared of the CP:
double CP3var_comp::norm2CP(){
  double out = 0.0;
  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    for(unsigned int jTerm=0; jTerm<m_rank; jTerm++){
      double contrib = dot(m_R[iTerm],m_R[jTerm]);
      contrib *= dot(m_G[iTerm],m_G[jTerm]);
      contrib *= dot(m_S[iTerm],m_S[jTerm]);
      out += contrib;
    }
  }
  return out;
}



// (I.) scaling the CP3var_real by a scalar:
void CP3var_comp::operator *= (double alpha){
  double sign = 1.0;
  if(alpha<0){sign = -1.0;}
  const double factor = pow(fabs(alpha), (1.0/3.0) );

  for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
    m_R[iTerm]*=(factor*sign);
    m_G[iTerm]*=factor;
    m_S[iTerm]*=factor;
  }
}

// (I.) copy operator:
void CP3var_comp::operator << (CP3var_comp& A){
  copyTensorFrom(A);
}

// (I.) operator += : implementation without copy (!!!)
void CP3var_comp::operator += (CP3var_comp& A){
  const unsigned int old_rank = m_rank;

  m_rank += A.rank();
  m_R.resize(m_rank);
  m_G.resize(m_rank);
  m_S.resize(m_rank);

  for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
    m_R[iTerm+old_rank] = A.R(iTerm);
    m_G[iTerm+old_rank] = A.G(iTerm);
    m_S[iTerm+old_rank] = A.S(iTerm);
  }
}




// (I.) compute the scalar product between 2 CP3var_comp
double operator * (CP3var_comp& A, CP3var_comp& B){
  double out = 0.0;
  for(unsigned int iTerm=0; iTerm<A.rank(); iTerm++){
    for(unsigned int jTerm=0; jTerm<B.rank(); jTerm++){
      vec_comp tmp_ar = A.R(iTerm);
      vec_comp tmp_br = B.R(jTerm);
      double contrib = dot(tmp_ar,tmp_br);
      vec_comp tmp_ag = A.G(iTerm);
      vec_comp tmp_bg = B.G(jTerm);
      contrib *= dot(tmp_ag,tmp_bg);
      vec_comp tmp_as = A.S(iTerm);
      vec_comp tmp_bs = B.S(jTerm);
      contrib *= dot(tmp_as,tmp_bs);
      out += contrib;
    }
  }
  return out;
}


// COMPRESSION METHODS for Complex valued 3-vars CP-tensor.
// The methods are specific to Schroedinger application!!

/* compute an auxiliary table, useful to use a customized implementation of CG
  - inputs: temporary r,s, fourier transform
  - outputs: all the Y table, in physical coordinates!
*/
void CP3var_comp::compute_aux_tab_1d(vec_comp& r, vec_comp& s, vector<vec_comp>& Y_tab){
  const int N_x = r.size();
  const int N_y = s.size();
  unsigned int Nyquist_x = (N_x%2==0) ? N_x/2 : (N_x+1)/2;
  unsigned int Nyquist_y = (N_y%2==0) ? N_y/2 : (N_y+1)/2;

  // init the table to the size rank+1
  Y_tab.resize(m_rank+1);

  // first step: compute rˆ2, sˆ2: pass through inverse fft, take the square, direct fft.

  // 1 - inverse transforms:
  fftw_plan inv_plan_x;
  fftw_complex* modes_r = r.v();
  fftw_complex* out_x;
  out_x = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_x);
  inv_plan_x = fftw_plan_dft_1d(N_x, modes_r, out_x, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(inv_plan_x);

  const double renorm_x = 1.0/N_x;
  for(unsigned int i=0; i<N_x; i++){
    out_x[i][0] *= renorm_x;
    out_x[i][1] *= renorm_x;
  }

  fftw_plan inv_plan_y;
  fftw_complex* modes_s = s.v();
  fftw_complex* out_y;
  out_y = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_y);
  inv_plan_y = fftw_plan_dft_1d(N_y, modes_s, out_y, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(inv_plan_y);

  const double renorm_y = 1.0/N_y;
  for(unsigned int i=0; i<N_y; i++){
    out_y[i][0] *= renorm_y;
    out_y[i][1] *= renorm_y;
  }

  // 2.a - compute r*r and s*s:
  fftw_complex* r_star_r;
  r_star_r = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_x);
  for(unsigned int i=0; i<N_x; i++){
    r_star_r[i][0] = out_x[i][0]*out_x[i][0] + out_x[i][1]*out_x[i][1];
    r_star_r[i][1] = 0.0;
  }

  fftw_complex* s_star_s;
  s_star_s = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_y);
  for(unsigned int i=0; i<N_y; i++){
    s_star_s[i][0] = out_y[i][0]*out_y[i][0] + out_y[i][1]*out_y[i][1];
    s_star_s[i][1] = 0.0;
  }

  // 2.b compute their fft:
  fftw_complex* r2F;
  r2F = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_x);
  fftw_plan dir_plan_x;
  dir_plan_x = fftw_plan_dft_1d(N_x, r_star_r, r2F, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(dir_plan_x);

  fftw_complex* s2F;
  s2F = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_y);
  fftw_plan dir_plan_y;
  dir_plan_y = fftw_plan_dft_1d(N_y, s_star_s, s2F, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(dir_plan_y);


  // compute [y]_k = [r]_{k} [s]_{-k}
  fftw_complex* yF;
  yF = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_y);
  yF[0][0] = r2F[0][0]*s2F[0][0];
  yF[0][1] = r2F[0][1]*r2F[0][1];
  for(unsigned int k=1; k<Nyquist_y; k++){
    yF[k][0] = r2F[k][0]*s2F[N_y-k][0];
    yF[k][1] = r2F[k][1]*s2F[N_y-k][1];
    yF[N_y-k][0] = r2F[N_y-k][0]*s2F[k][0];
    yF[N_y-k][1] = r2F[N_y-k][1]*s2F[k][1];
  }

  // compute its backward transform in the physical space: this gives the first column of Y
  fftw_complex* y;
  y = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_y);
  fftw_plan y_plan;
  y_plan = fftw_plan_dft_1d(N_y, yF, y, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(y_plan);
  // renormalisation:
  for(unsigned int i=0; i<N_x; i++){
    y[i][0] *= renorm_x;
    y[i][1] *= renorm_x;
  }

  // assign it to the first place of Y_tab
  Y_tab[0].init(y, N_y);

  // free the memory: out_x, out_y and y will be used later on. All the other objects can be erased.
  fftw_destroy_plan(inv_plan_x);
  fftw_destroy_plan(inv_plan_y);
  fftw_free(r_star_r);
  fftw_free(s_star_s);
  fftw_free(r2F);
  fftw_free(s2F);
  fftw_destroy_plan(dir_plan_x);
  fftw_destroy_plan(dir_plan_y);
  fftw_free(yF);


  // Assembling the other entries of the table:
  for(unsigned int j=0; j<m_rank; j++){
    // pass the modes in the physical space:
    fftw_plan inv_x_j;
    fftw_complex* modes_r = m_R[j].v();
    fftw_complex* out_x_j;
    out_x_j = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_x);
    inv_x_j = fftw_plan_dft_1d(N_x, modes_r, out_x_j, FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(inv_x_j);

    for(unsigned int i=0; i<N_x; i++){
      out_x_j[i][0] *= renorm_x;
      out_x_j[i][1] *= renorm_x;
    }

    fftw_plan inv_y_j;
    fftw_complex* modes_s = m_S[j].v();
    fftw_complex* out_y_j;
    out_y_j = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_y);
    inv_y_j = fftw_plan_dft_1d(N_y, modes_s, out_y_j, FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(inv_y_j);

    for(unsigned int i=0; i<N_y; i++){
      out_y_j[i][0] *= renorm_y;
      out_y_j[i][1] *= renorm_y;
    }

    // compute in the physical space r_j r* = out_x_j * conj(out_x); overwrite out_x_j
    for(unsigned int i=0; i<N_x; i++){
      double re_j = out_x_j[i][0];
      double im_j = out_x_j[i][1];
      out_x_j[i][0] = re_j*out_x[i][0] + im_j*out_x[i][1];
      out_x_j[i][1] = im_j*out_x[i][0] - re_j*out_x[i][1];
    }

    // compute in the physical space s_j s* = out_y_j * conj(out_y); overwrite out_y_j
    for(unsigned int i=0; i<N_y; i++){
      double re_j = out_y_j[i][0];
      double im_j = out_y_j[i][1];
      out_y_j[i][0] = re_j*out_y[i][0] + im_j*out_y[i][1];
      out_y_j[i][1] = im_j*out_y[i][0] - re_j*out_y[i][1];
    }

    // 2.b compute their fft:
    fftw_complex* rjF;
    rjF = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_x);
    fftw_plan dir_x_j;
    dir_x_j = fftw_plan_dft_1d(N_x, out_x_j, rjF, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(dir_x_j);

    fftw_complex* sjF;
    sjF = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_y);
    fftw_plan dir_y_j;
    dir_y_j = fftw_plan_dft_1d(N_y, out_y_j, sjF, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(dir_y_j);

    // compute the fourier transform of y:
    // compute [y]_k = [r]_{k} [s]_{-k}
    fftw_complex* yjF;
    yjF = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_y);
    yjF[0][0] = rjF[0][0]*sjF[0][0];
    yjF[0][1] = rjF[0][1]*rjF[0][1];
    for(unsigned int k=1; k<Nyquist_y; k++){
      yjF[k][0] = rjF[k][0]*sjF[N_y-k][0];
      yjF[k][1] = rjF[k][1]*sjF[N_y-k][1];
      yjF[N_y-k][0] = rjF[N_y-k][0]*sjF[k][0];
      yjF[N_y-k][1] = rjF[N_y-k][1]*sjF[k][1];
    }

    // pass yjF in the physical space and put it into the tab:
    fftw_complex* yj;
    yj = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_y);
    fftw_plan yj_plan;
    yj_plan = fftw_plan_dft_1d(N_y, yjF, yj, FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(yj_plan);
    // renormalisation:
    for(unsigned int i=0; i<N_x; i++){
      yj[i][0] *= renorm_x;
      yj[i][1] *= renorm_x;
    }

    // assign it to the first place of Y_tab
    Y_tab[j+1].init(yj, N_y);

    // free the memory: all excet out_x, out_y
    fftw_free(out_x_j);
    fftw_destroy_plan(inv_x_j);
    fftw_free(out_y_j);
    fftw_destroy_plan(inv_y_j);
    fftw_free(rjF);
    fftw_destroy_plan(dir_x_j);
    fftw_free(sjF);
    fftw_destroy_plan(dir_y_j);
    fftw_free(yjF);
    fftw_destroy_plan(yj_plan);
  }

  // free the memory:
  fftw_free(out_x);
  fftw_free(out_y);
}


/* customised conjugate gradient:
  - input: the temporary values of r and s, the initial guess for g
  - output: g
*/


// auxiliary fun:
vec_comp pointwise_prod(vec_comp& y, vec_comp& p){
  const int N_z = y.size();
  fftw_complex* out;
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_z);
  for(unsigned int i_dof=0; i_dof<N_z; i_dof++){
    out[i_dof][0] = (y.real(i_dof)*p.real(i_dof) - y.imag(i_dof)*p.imag(i_dof));
    out[i_dof][1] = (y.real(i_dof)*p.imag(i_dof) + y.imag(i_dof)*p.real(i_dof));
  }
  vec_comp toBeReturned(out, N_z);
  return toBeReturned;
}


void CP3var_comp::conj_grad_1d(vec_comp& r, vec_comp& s, vec_comp& g , double tol){
  const int N_x = r.size();
  const int N_y = s.size();
  const int N_z = N_x;
  unsigned int Nyquist_x = (N_x%2==0) ? N_x/2 : (N_x+1)/2;
  unsigned int Nyquist_y = (N_y%2==0) ? N_y/2 : (N_y+1)/2;
  unsigned int Nyquist_z = (N_z%2==0) ? N_z/2 : (N_z+1)/2;
  const double renorm_z = 1.0/N_z;

  // first, compute the Y table:
  vector<vec_comp> Y_tab;
  compute_aux_tab_1d(r, s, Y_tab);

  // transform the initial guess g into the physical space:
  fftw_complex* gF = g.v();
  fftw_complex* g_phy_v;
  g_phy_v = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_z);
  fftw_plan g_plan;
  g_plan = fftw_plan_dft_1d(N_z, gF, g_phy_v, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(g_plan);
  // renormalisation:
  for(unsigned int i=0; i<N_x; i++){
    g_phy_v[i][0] *= renorm_z;
    g_phy_v[i][1] *= renorm_z;
  }
  vec_comp g_phy(g_phy_v, N_z);

  // free the memory:
  fftw_destroy_plan(g_plan);


  // compute the m_G in the physical space:
  vector<vec_comp> m_g(m_rank);
  for(unsigned int j=0; j<m_rank; j++){
    fftw_complex* gjF = m_G[j].v();
    fftw_complex* gj;
    gj = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_z);
    fftw_plan gj_plan;
    gj_plan = fftw_plan_dft_1d(N_z, gjF, gj, FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(gj_plan);
    // renormalisation:
    for(unsigned int i=0; i<N_x; i++){
      gj[i][0] *= renorm_z;
      gj[i][1] *= renorm_z;
    }
    m_g[j].init(gj, N_z);

    // free the memory:
    fftw_destroy_plan(gj_plan);
  }

  // compute the initial residual in the physical space:
  fftw_complex* res_phy_ptr;
  res_phy_ptr = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_z);
  for(unsigned int i_dof=0; i_dof<m_rank; i_dof++){
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      res_phy_ptr[i_dof][0] += (Y_tab[iTerm+1].real(i_dof)*m_g[iTerm].real(i_dof) - Y_tab[iTerm+1].imag(i_dof)*m_g[iTerm].imag(i_dof));
      res_phy_ptr[i_dof][1] += (Y_tab[iTerm+1].real(i_dof)*m_g[iTerm].imag(i_dof) + Y_tab[iTerm+1].imag(i_dof)*m_g[iTerm].real(i_dof));
    }
  }

  for(unsigned int i_dof=0; i_dof<N_z; i_dof++){
    res_phy_ptr[i_dof][0] -= (Y_tab[0].real(i_dof)*g_phy.real(i_dof) - Y_tab[0].imag(i_dof)*g_phy.imag(i_dof));
    res_phy_ptr[i_dof][1] -= (Y_tab[0].real(i_dof)*g_phy.imag(i_dof) + Y_tab[0].imag(i_dof)*g_phy.real(i_dof));
  }
  vec_comp res_phy(res_phy_ptr, N_z);

  // define p:
  vec_comp p_phy; p_phy << res_phy;
  // q = A p
  vec_comp q_phy = pointwise_prod(Y_tab[0], p_phy);

  // starting the iterations:
  bool haveToIter = true;
  double norm_res = dot(res_phy, res_phy);
  if(sqrt(norm_res)<tol){
    haveToIter = false;
  }
  // Parseval-Bessel ensures the dot product is preserved from physical to Fourier!
  while(haveToIter){
      double num_alpha = norm_res;
      double den_alpha = dot(p_phy, q_phy);
      double alpha = num_alpha / den_alpha;
      g_phy.axpy(p_phy, alpha);
      res_phy.axpy(p_phy, -1.0*alpha);
      norm_res = dot(res_phy, res_phy);
      if(sqrt(norm_res)<tol){
        haveToIter = false;
        break;
      }

      // update p_phy:
      double beta = norm_res/num_alpha;
      p_phy *= beta;
      p_phy.axpy(res_phy, 1.0);
      // compute q_phy without re-allocating:
      fftw_complex* q_phy_v = q_phy.v();
      for(unsigned int i_dof=0; i_dof<N_z; i_dof++){
        q_phy_v[i_dof][0] = Y_tab[0].real(i_dof)*p_phy.real(i_dof) - Y_tab[0].imag(i_dof)*p_phy.imag(i_dof);
        q_phy_v[i_dof][1] = Y_tab[0].real(i_dof)*p_phy.imag(i_dof) + Y_tab[0].imag(i_dof)*p_phy.real(i_dof);
      }

  }

  // transform g_phy back in the physical space:
  fftw_complex* g_in = g_phy.v();
  fftw_complex* g_out = g.v();
  fftw_plan g_plan_dir;
  g_plan_dir = fftw_plan_dft_1d(N_z, g_in, g_out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(g_plan_dir);

  // free the memory:
  fftw_destroy_plan(g_plan_dir);
  g_phy.clear();
  res_phy.clear();
  p_phy.clear();
  q_phy.clear();
  for(unsigned int j=0; j<m_rank; j++){
    m_g.clear();
  }
  for(unsigned int j=0; j<m_rank+1; j++){
    Y_tab.clear();
  }
}


/* customised ALS rank-1:
  - input: the guess of r and s, the function g
  - output: update r and s
*/
void CP3var::comp_rs_1d(vec_comp& r, vec_comp& s, vec_comp& g , double tol){
  
}
