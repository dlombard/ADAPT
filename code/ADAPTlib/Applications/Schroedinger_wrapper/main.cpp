// Parallel tensor implementation
#include <stdio.h>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ostream>
#include <string>
#include <vector>
#include <map>
#include <complex>
#include <math.h>
#include <ios>
#include <algorithm>
#include <random>
#include <chrono>
#include <cfloat>
#include <iomanip>

using namespace std;

#include <mpi.h>
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "pod.h"

extern "C" {
  extern int dgeev_(char*,char*,int*,double*,int*,double*, double*, double*, int*, double*, int*, double*, int*, int*);
  extern int dsyev_(char*,char*,int*,double*,int*,double*,double*,int*,int*);
}
// Simple test on vectors

int main(int argc, char **args){

  MPI_Init(NULL, NULL);

  // Get the number of processes
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // Get the rank of the process
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // Get the name of the processor
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);

  // Print off a hello world message
  printf("Hello world from processor %s, rank %d out of %d processors\n",
  processor_name, world_rank, world_size);

  // testing vector:

  double *data;

  // n = number of rows
  // m = number of columns
  // storing lower matrix:

  /*
  int n = 8;
  int m = 8;
  data = new double[n*m];
  for (int i=0;i<n;i++){
    for (int j=0;j<=i;j++){
      data[j*n+i] = 1.0; // column major order
    }
  }
  */

  /*
  subroutine dsyev	(	character 	JOBZ, 'N', 'V' => V computes also eigenvectors
    character 	UPLO,  'U', 'L' => upper and lower triangular respectively
    integer 	N, => matrix dimension
    double precision, dimension( lda, N ) 	A, => with 'V' A is destroyed, replaced by eigenvectors.
    integer 	LDA, => leading dimension of the array
    double precision, dimension( * ) 	W,  => output, the eigenvalues in ascending order.
    double precision, dimension( * ) 	WORK,
    integer 	LWORK,
    integer 	INFO
  )
  */

  /*
  char type = 'V';
  char storage = 'L';
  double *eigenvals; eigenvals = new double[n];
  int l_work = 3*n-1;
  double *work; work = new double[l_work];
  int info;
  dsyev_(&type, &storage, &n , data ,&n, eigenvals, work, &l_work, &info);

  for(unsigned int i=0; i<n; i++){
    cout << eigenvals[i] << endl;
  }

  // last eigenvector: last column of data:
  if(info==0){
    cout << "Eigenvector matrix: " << endl;
    for(unsigned int i=0; i<n; i++){
      for(unsigned int j=0; j<n; j++){
        unsigned int ind = j*n + i;
        cout << data[ind] << ",   ";
      }
      cout << endl;
    }
  }
  else{
    cout << "info = " << info << endl;
    cout << "Convergence problem on eigenvalues computation!" << endl;
  }

*/


  // using the routine for classical eigenvalues:

  int n = 8;
  int m = 8;
  data = new double[n*m];
  for (int i=0;i<n;i++){
    for (int j=0;j<n;j++){
      data[j*n+i] = 1.0; // column major order
    }
  }

  // allocate data
  char Nchar='V';
  double *eigReal=new double[n];
  double *eigImag=new double[n];
  double *vl,*vr;
  vl = new double[n*n];
  vr = new double[n*n];
  int one=n;
  int lwork=6*n;
  double *work=new double[lwork];
  int info;

  // calculate eigenvalues using the DGEEV subroutine
  dgeev_(&Nchar,&Nchar,&n,data,&n,eigReal,eigImag,
        vl,&one,vr,&one,
        work,&lwork,&info);


  // check for errors
  if (info!=0){
    cout << "Error: dgeev returned error code " << info << endl;
    return -1;
  }

  // output eigenvalues to stdout
  cout << "--- Eigenvalues ---" << endl;
  for (int i=0;i<n;i++){
    cout << "( " << eigReal[i] << " , " << eigImag[i] << " )\n";
  }
  cout << endl;

  // output the eigenvectors:
  if(info==0){
    cout << "Eigenvector matrix: " << endl;
    for(unsigned int i=0; i<n; i++){
      for(unsigned int j=0; j<n; j++){
        unsigned int ind = j*n + i;
        cout << vr[ind] << ",   ";
      }
      cout << endl;
    }
  }
  else{
    cout << "info = " << info << endl;
    cout << "Convergence problem on eigenvalues computation!" << endl;
  }

  // deallocate
  delete [] data;
  delete [] eigReal;
  delete [] eigImag;
  delete [] work;


  // testing sparse mat:
  mat_real_sp A(5,5);
  A << triplet(0,0,1.0);
  A << triplet(0,1,2.0);
  A << triplet(0,0,1.5); // it sum 1.5 to 1.0 !!
  A.print();


  // Finalize the MPI environment.
  MPI_Finalize();

    return 0;

}
