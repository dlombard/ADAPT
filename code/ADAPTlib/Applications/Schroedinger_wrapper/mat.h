#ifndef mat_h
#define mat_h
#include <stdio.h>
#include <iostream>
#include <cassert>
#include "fftw3.h"
#include <string>
#include <vector>
#include <map>
#include <complex>
#include <math.h>
#include <algorithm>

using namespace std;



class mat{
protected:
  unsigned int m_nRows;
  unsigned int m_nCols;


public:
  mat(){};
  ~mat(){};

// indices maps, column major order:
inline unsigned int sub2lin(unsigned int iRow, unsigned int jCol){
  return jCol*m_nRows + iRow;
}

inline vector<unsigned int> lin2sub(unsigned int linInd){
  vector<unsigned int> out(2);
  out[0] = linInd % m_nRows;
  out[1] = linInd / m_nRows;
  return out;
}

// Access functions:
inline unsigned int nRows(){return m_nRows;}
inline unsigned int nCols(){return m_nCols;}

};


// class real matrix: real valued, dense, sequential.
class mat_real : public mat{

private:
  double* m_M;

public:
  mat_real(){};
  ~mat_real(){delete[] m_M;}

  // overloaded constructors:
  mat_real(unsigned int, unsigned int);
  mat_real(double* &, unsigned int, unsigned int);
  void init(unsigned int, unsigned int);


  // Methods:
  void copyMatFrom(mat_real&);
  void operator << (mat_real& toBeCopied){copyMatFrom(toBeCopied);}
  void operator += (mat_real&);
  void operator -= (mat_real&);
  void operator *= (double);
  void eye(unsigned int n){
    init(n,n);
    for(unsigned int iRow=0; iRow<m_nRows; iRow++){
      unsigned int l = sub2lin(iRow,iRow);
      m_M[l] = 1.0;
    }
  }

  // Setters:
  void setMatEl(unsigned int i, unsigned int j, double val){
    unsigned int l = sub2lin(i,j);
    m_M[l] = val;
  }
  inline void operator()(unsigned int i, unsigned int j, double val){
    setMatEl(i,j,val);
  }

  // Access functions:
  double* M(){return m_M;}
  inline double operator()(unsigned int i, unsigned int j){
    unsigned int l = sub2lin(i,j);
    return m_M[l];
  }
  inline double operator()(unsigned int linInd){
    return m_M[linInd];
  }

};


// class mat_real_sp: sparse, real valued, sequential

// auxiliary class triplet:
class triplet{
private:
  unsigned int m_i;
  unsigned int m_j;
  double m_val;
public:
  triplet(){};
  triplet(unsigned int i, unsigned int j, double val){
    m_i = i;
    m_j = j;
    m_val = val;
  };
  inline void operator()(unsigned int i, unsigned int j, double val){
    triplet(i, j, val);
  }
  ~triplet(){};
  inline unsigned int id_row(){return m_i;}
  inline unsigned int id_col(){return m_j;}
  inline double val(){return m_val;}
};



class mat_real_sp : public mat{
private:
  vector<vector<unsigned int> >* pattern;
  vector<vector<double> >* m_M;
public:
  mat_real_sp(){};
  mat_real_sp(unsigned int, unsigned int);
  ~mat_real_sp(){
    for(unsigned int iRow=0; iRow<m_nRows; iRow++){
      (*m_M)[iRow].clear();
      (*pattern)[iRow].clear();
    }
    pattern->clear();
    m_M->clear();
    delete m_M;
    delete pattern;
  };



  // Add entry:
  inline void operator << (triplet T){
    // go to the i-th line of the pattern and check:
    unsigned int iRow = T.id_row();
    bool isItEmpty = true;
    unsigned int pattern_entry_id = 0;
    for(unsigned int iEn=0; iEn<(*pattern)[iRow].size(); iEn++){
      if((*pattern)[iRow][iEn]==T.id_col()){
        isItEmpty = false;
        pattern_entry_id = iEn;
        break;
      }
    }
    if(isItEmpty){
      (*pattern)[iRow].push_back(T.id_col());
      (*m_M)[iRow].push_back(T.val());
    }
    else{
      (*m_M)[iRow][pattern_entry_id] += T.val();
    }
  };


  // setter: it overwrites the entry!
  inline void setMatEl(unsigned int iRow, unsigned int iCol, double val){
    bool isItInPattern = false;
    unsigned int iEl_star = 0;
    for(unsigned int iEl=0; iEl<(*pattern)[iRow].size(); iEl++){
      if( (*pattern)[iRow][iEl] == iCol ) {
        isItInPattern = true;
        iEl_star = iEl;
        break;
      }
    }
    if(isItInPattern){
      (*m_M)[iRow][iEl_star] = val;
    }else{
      (*pattern)[iRow].push_back(iCol);
      (*m_M)[iRow].push_back(val);
    }
  }


  // Access functions:
  inline vector<unsigned int> row_pattern(unsigned int iRow){return (*pattern)[iRow];}
  inline unsigned int row_pattern(unsigned int iRow, unsigned int iEl){return (*pattern)[iRow][iEl];}
  inline double M(unsigned int iRow, unsigned int iEl){return (*m_M)[iRow][iEl];}
  inline triplet T(unsigned int iRow, unsigned int iEl){return triplet(iRow, iEl, (*m_M)[iRow][iEl]);}
  inline double operator ()(unsigned int iRow, unsigned int iCol){
    bool isItNonZero = false;
    unsigned int iEl_star=0;
    for(unsigned int iEl=0; iEl<(*pattern)[iRow].size(); iEl++){
      if( (*pattern)[iRow][iEl] == iCol ) {
        isItNonZero = true;
        iEl_star = iEl;
        break;
      }
    }
    if(isItNonZero){
      return (*m_M)[iRow][iEl_star];
    }
    else{
      return 0.0;
    }
  }

  // print functions:
  inline void print(){
    cout << "Matrix size: " << m_nRows << " x " << m_nCols << endl;
    for(unsigned int iRow=0; iRow<m_nRows; iRow++){
      for(unsigned int iEl=0; iEl<(*pattern)[iRow].size(); iEl++){
        cout << "(" << iRow << ", " << (*pattern)[iRow][iEl] << ") = " << (*m_M)[iRow][iEl] << endl;
      }
    }
  }

};

#endif
