// Header file for POD operations.

#ifndef pod_h
#define pod_h
#include "fftw3.h"
#include <string>
#include <vector>
#include <map>
#include <complex>
#include <math.h>
#include <algorithm>

#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"

extern "C" {
  extern int dgeev_(char*,char*,int*,double*,int*,double*, double*, double*, int*, double*, int*, double*, int*, int*);
  extern int dsyev_(char*,char*,int*,double*,int*,double*,double*,int*,int*);
}


// function to perfor the eigenvalue decomposition of a generic dense matrix:
void eig(mat_real&, vector<vector<double> >&, vector<vec_real>&, unsigned int);

// function to perform the eigenvalue decomposition of a symmetric matrix:
void eig_sym(mat_real&, vector<double>&, vector<vec_real>&, unsigned int);

// function which perform the POD of a set of vectors:
void pod(vector<vec_real>&, vector<double>&, vector<vec_real>&);

// function which perform the POD with truncation:
void pod(vector<vec_real>&, double, vector<double>&, vector<vec_real>&);


#endif
