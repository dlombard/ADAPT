// Header class for operator tensor:

#ifndef operatorTensor_h
#define operatorTensor_h

#include "fftw3.h"
#include "mat.h"
#include "vec.h"


using namespace std;


class operatorTensor{
protected:
  unsigned int m_nTerms;
  unsigned int m_nVar=3; // static definition, specific to Schroedinger.

public:
  operatorTensor(){};
  ~operatorTensor(){};

  // ACCESS FUNCTIONS:
  inline unsigned int nTerms(){return m_nTerms;}
  inline unsigned int nVar(){return m_nVar;}

};


// matrices are real and dense:
class opTens_real_dense : public operatorTensor{
private:
  vector<vector<mat_real> > m_Op;
public:
  opTens_real_dense(){};
  ~opTens_real_dense(){m_Op.clear();};

  // SETTERS:
  inline void set_terms(vector<vector<mat_real> >& terms){
    m_Op = terms;
    m_nTerms = terms.size();
    assert(terms[0].size()==m_nVar);
  }
  // coresponding operator;
  void operator << (vector<vector<mat_real> >& terms){
    set_terms(terms);
  }
  // add one term:
  inline void append(vector<mat_real>& term){
    assert(term.size()==m_nVar);
    m_Op.push_back(term);
    m_nTerms += 1;
  }
  // operator:
  void operator << (vector<mat_real>& term){
    append(term);
  }

  // ACCESS FUNCTIONS:
  vector<mat_real> operator()(unsigned int iTerm){return m_Op[iTerm];}
  mat_real operator ()(unsigned int iTerm, unsigned int iVar){return m_Op[iTerm][iVar];}
};


// matrices are real and sparse:
class opTens_real_sparse : public operatorTensor{
private:
  vector<vector<mat_real_sp> > m_Op;
public:
  opTens_real_sparse(){};
  ~opTens_real_sparse(){m_Op.clear();};

  // SETTERS:
  inline void set_terms(vector<vector<mat_real_sp> >& terms){
    m_Op = terms;
    m_nTerms = terms.size();
    assert(terms[0].size()==m_nVar);
  }
  // coresponding operator;
  void operator << (vector<vector<mat_real_sp> >& terms){
    set_terms(terms);
  }
  // add one term:
  inline void append(vector<mat_real_sp>& term){
    assert(term.size()==m_nVar);
    m_Op.push_back(term);
    m_nTerms += 1;
  }
  // operator:
  void operator << (vector<mat_real_sp>& term){
    append(term);
  }

  // ACCESS FUNCTIONS:
  vector<mat_real_sp> operator()(unsigned int iTerm){return m_Op[iTerm];}
  mat_real_sp operator ()(unsigned int iTerm, unsigned int iVar){return m_Op[iTerm][iVar];}
};


#endif
