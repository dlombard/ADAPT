#ifndef CP3var_h
#define CP3var_h

// Including headers:
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "operatorTensor.h"

using namespace std;


// class  to represent the solution:
// particularisation of CP format to 3 variables.
class CP3var_real{
private:
  unsigned int m_nVar=3;
  vector<unsigned int> m_nDof_var;
  unsigned int m_rank;
  vector<vec_real> m_R;
  vector<vec_real> m_G;
  vector<vec_real> m_S;

public:
  CP3var_real(){};
  CP3var_real(vector<unsigned int>);
  CP3var_real(vector<vec_real>&, vector<vec_real>&, vector<vec_real>&);
  CP3var_real(vec_real&, vec_real&, vec_real&);

  ~CP3var_real(){};

  inline void set_rank(unsigned int k){m_rank=k;}
  // set the terms of the CP3var_real to a given set of modes:
  inline void set_terms(vector<vec_real> R_tab, vector<vec_real> G_tab, vector<vec_real> S_tab){
    m_rank = R_tab.size();
    m_R = R_tab;
    m_G = G_tab;
    m_S = S_tab;
    m_nDof_var[0] = R_tab[0].size();
    m_nDof_var[1] = G_tab[0].size();
    m_nDof_var[2] = S_tab[0].size();
  }
  // overloaded rank 1:
  inline void set_terms(vec_real r_one, vec_real g_one, vec_real s_one){
    m_rank = 1;
    m_R.resize(1);
    m_R[0] = r_one;
    m_G.resize(1);
    m_G[0] = g_one;
    m_S.resize(1);
    m_S[0] = s_one;
    m_nDof_var[0] = r_one.size();
    m_nDof_var[1] = g_one.size();
    m_nDof_var[2] = s_one.size();
  }

  // set a single term into a specific position:
  inline void set_terms(unsigned int id_term, vec_real r_one, vec_real g_one, vec_real s_one){
    m_R[id_term] = r_one;
    m_G[id_term] = g_one;
    m_S[id_term] = s_one;
  }

  // multiply a single term by a double:
  void rescale_term(unsigned int iTerm, double alpha){
    double sign = 1.0;
    if(alpha<0){sign = -1.0;}
    const double factor = pow(fabs(alpha), (1.0/3.0) );
    m_R[iTerm]*=(factor*sign);
    m_G[iTerm]*= factor;
    m_S[iTerm]*= factor;
  }

  // clear a term:
  inline void clear_term(unsigned int id_term){
    m_R[id_term].clear();
    m_G[id_term].clear();
    m_S[id_term].clear();
  }

  // clear the whole tensor:
  void clear(){
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      m_R[iTerm].clear();
      m_G[iTerm].clear();
      m_S[iTerm].clear();
    }
  }

  void init(vector<unsigned int> n_dofs){
    m_rank = 0;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_nDof_var[iVar] = n_dofs[iVar];
    }
  }

  // METHODS:
  void copyTensorFrom(CP3var_real&);
  void zero();
  void zero(vector<unsigned int>);
  void append(vec_real&, vec_real&, vec_real&);
  void append(vector<vec_real>&, vector<vec_real>&, vector<vec_real>&);
  void axpy(CP3var_real&, double, bool);
  void axpy(vec_real&, vec_real&, vec_real&, double, bool);
  CP3var_real applyOperator(opTens_real_dense&);
  CP3var_real applyOperator(opTens_real_sparse&);
  double norm2CP();


  // OPERATORS:
  void operator *= (double);
  void operator << (CP3var_real&);
  void operator += (CP3var_real&);
  void operator -= (CP3var_real&);
  double operator ()(unsigned int i, unsigned int j, unsigned int k);

  // ACCESS FUNCTIONS:
  inline vector<unsigned int> nDof_var(){return m_nDof_var;}
  inline unsigned int nDof_var(unsigned int iVar){return m_nDof_var[iVar];}
  inline unsigned int rank(){return m_rank;}
  inline vector<vec_real> R(){return m_R;}
  inline vec_real R(unsigned int iTerm){return m_R[iTerm];}
  inline vector<vec_real> G(){return m_G;}
  inline vec_real G(unsigned int iTerm){return m_G[iTerm];}
  inline vector<vec_real> S(){return m_S;}
  inline vec_real S(unsigned int iTerm){return m_S[iTerm];}
  inline void print(){
    cout << "Rank = " << m_rank << endl;
  }
};



// Complex:
// class  to represent the solution:
// particularisation of CP format to 3 variables.
class CP3var_comp{
private:
  unsigned int m_nVar=3;
  vector<unsigned int> m_nDof_var;
  unsigned int m_rank;
  vector<vec_comp> m_R;
  vector<vec_comp> m_G;
  vector<vec_comp> m_S;

public:
  CP3var_comp(){};
  CP3var_comp(vector<unsigned int>);
  CP3var_comp(vector<vec_comp>&, vector<vec_comp>&, vector<vec_comp>&);
  CP3var_comp(vec_comp&, vec_comp&, vec_comp&);

  ~CP3var_comp(){};

  inline void set_rank(unsigned int k){m_rank=k;}
  // set the terms of the CP3var_real to a given set of modes:
  inline void set_terms(vector<vec_comp> R_tab, vector<vec_comp> G_tab, vector<vec_comp> S_tab){
    m_rank = R_tab.size();
    m_R = R_tab;
    m_G = G_tab;
    m_S = S_tab;
    m_nDof_var[0] = R_tab[0].size();
    m_nDof_var[1] = G_tab[0].size();
    m_nDof_var[2] = S_tab[0].size();
  }
  // overloaded rank 1:
  inline void set_terms(vec_comp r_one, vec_comp g_one, vec_comp s_one){
    m_rank = 1;
    m_R.resize(1);
    m_R[0] = r_one;
    m_G.resize(1);
    m_G[0] = g_one;
    m_S.resize(1);
    m_S[0] = s_one;
    m_nDof_var[0] = r_one.size();
    m_nDof_var[1] = g_one.size();
    m_nDof_var[2] = s_one.size();
  }

  // set a single term into a specific position:
  inline void set_terms(unsigned int id_term, vec_comp r_one, vec_comp g_one, vec_comp s_one){
    m_R[id_term] = r_one;
    m_G[id_term] = g_one;
    m_S[id_term] = s_one;
  }

  // multiply a single term by a double:
  void rescale_term(unsigned int iTerm, double alpha){
    double sign = 1.0;
    if(alpha<0){sign = -1.0;}
    const double factor = pow(fabs(alpha), (1.0/3.0) );
    m_R[iTerm]*=(factor*sign);
    m_G[iTerm]*= factor;
    m_S[iTerm]*= factor;
  }

  // clear a term:
  inline void clear_term(unsigned int id_term){
    m_R[id_term].clear();
    m_G[id_term].clear();
    m_S[id_term].clear();
  }

  // clear the whole tensor:
  void clear(){
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      m_R[iTerm].clear();
      m_G[iTerm].clear();
      m_S[iTerm].clear();
    }
  }

  void init(vector<unsigned int> n_dofs){
    m_rank = 0;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_nDof_var[iVar] = n_dofs[iVar];
    }
  }

  // SETTERS:
  inline void set_mode_R(unsigned int i_term, vec_comp& v){
    m_R[i_term].clear();
    m_R[i_term] = v; // no copy is made, set by reference
  }
  inline void set_mode_G(unsigned int i_term, vec_comp& v){
    m_G[i_term].clear();
    m_G[i_term] = v; // no copy is made, set by reference
  }
  inline void set_mode_S(unsigned int i_term, vec_comp& v){
    m_S[i_term].clear();
    m_S[i_term] = v; // no copy is made, set by reference
  }

  // METHODS:
  void copyTensorFrom(CP3var_comp&);
  void zero();
  void zero(vector<unsigned int>);
  void append(vec_comp&, vec_comp&, vec_comp&);
  void append(vector<vec_comp>&, vector<vec_comp>&, vector<vec_comp>&);
  void axpy(CP3var_comp&, double, bool);
  void axpy(vec_comp&, vec_comp&, vec_comp&, double, bool);
  double norm2CP();

  // compression methods.
  void compute_aux_tab_1d(vec_comp&, vec_comp&, vector<vec_comp>&);
  void conj_grad_1d(vec_comp&, vec_comp&, vec_comp&, double);
  void comp_rs_1d(vec_comp&, vec_comp&, vec_comp&, double);


  // OPERATORS:
  void operator *= (double);
  void operator << (CP3var_comp&);
  void operator += (CP3var_comp&);
  void operator -= (CP3var_comp&);

  // ACCESS FUNCTIONS:
  inline vector<unsigned int> nDof_var(){return m_nDof_var;}
  inline unsigned int nDof_var(unsigned int iVar){return m_nDof_var[iVar];}
  inline unsigned int rank(){return m_rank;}
  inline vector<vec_comp> R(){return m_R;}
  inline vec_comp R(unsigned int iTerm){return m_R[iTerm];}
  inline vector<vec_comp> G(){return m_G;}
  inline vec_comp G(unsigned int iTerm){return m_G[iTerm];}
  inline vector<vec_comp> S(){return m_S;}
  inline vec_comp S(unsigned int iTerm){return m_S[iTerm];}
  inline void print(){
    cout << "Rank = " << m_rank << endl;
  }
};




#endif
