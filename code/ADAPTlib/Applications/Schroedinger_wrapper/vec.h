#ifndef vec_h
#define vec_h
#include <stdio.h>
#include <iostream>
#include <cassert>
#include "fftw3.h"
#include <string>
#include <vector>
#include <map>
#include <complex>
#include <math.h>
#include <algorithm>

using namespace std;

class vec{
protected:
  unsigned int m_size;
  string m_type;
public:
  vec(){};
  ~vec(){};
  virtual void init(unsigned int){};

  // ACCESS FUNCTION:
  inline unsigned int size(){return m_size;}
};


// daughter class real vectors:
class vec_real : public vec{
private:
  double *m_v;
public:
  vec_real(){};
  vec_real(unsigned int);
  vec_real(double*&, unsigned int);
  ~vec_real(){
    if(m_v != NULL){
      fftw_free(m_v);
      m_v = NULL;
    }
  }
  void init(unsigned int);
  void clear(){
    if(m_v != NULL){
      fftw_free(m_v);
      m_v = NULL;
    }
    m_size = 0;
  }

  // Methods:
  inline unsigned int get_size(){return sizeof(*m_v)/sizeof(double);}
  inline unsigned int get_size(vec_real v){return sizeof(*v.v())/sizeof(double);}
  void copyVecFrom(vec_real&);
  void operator << (vec_real& toBeCopied){copyVecFrom(toBeCopied);}
  void operator << (double*&);
  void operator += (vec_real&);
  void operator -= (vec_real&);
  void operator *= (double);
  double norm();
  double norm_squared();
  void axpy(vec_real&, double);

  // Setters:
  inline void set_size(unsigned int i){m_size = i;};
  inline void setVecEl(unsigned int i, double val){m_v[i]=val;}
  void operator ()(unsigned int i, double val){setVecEl(i,val);}

  // Access functions:
  inline double* v(){return m_v;}
  inline double v(unsigned int i){return m_v[i];}
  inline double operator()(unsigned int i){return m_v[i];}

  // print functions:
  inline void print_size(){cout<< "Real vec of size: " << m_size << endl;}
  inline void print(){
    print_size();
    for(unsigned int iDof=0; iDof<m_size; iDof++){
      cout<< m_v[iDof] << endl;
    }
    cout << endl;
  }
};






// daughter class complex vectors:
class vec_comp : public vec{
private:
  fftw_complex *m_v;

public:
  vec_comp(){};
  vec_comp(unsigned int);
  vec_comp(fftw_complex*&, unsigned int);
  ~vec_comp(){};//fftw_free(m_v);}
  void init(unsigned int);
  void init(fftw_complex*&, unsigned int);
  void clear(){
    fftw_free(m_v);
    m_size = 0;
  }

  // methods:
  inline unsigned int get_size(){return sizeof(*m_v)/(2*sizeof(double));}
  inline unsigned int get_size(vec_comp v){return sizeof(*v.v())/(2*sizeof(double));}
  void copyVecFrom(vec_comp&);
  void operator << (vec_comp& toBeCopied){copyVecFrom(toBeCopied);}
  void operator << (fftw_complex*&);
  void conj();
  void operator += (vec_comp&);
  void operator -= (vec_comp&);
  void operator *= (double);
  void operator *= (fftw_complex*);
  double norm();
  double norm_squared();
  void axpy(vec_comp&, fftw_complex*);
  void axpy(vec_comp&, double);

  // Setters:
  inline void set_size(unsigned int i){m_size = i;};
  inline void setVecEl(unsigned int i, fftw_complex* val){m_v[i][0]=val[0][0]; m_v[i][1]=val[0][1];}
  inline void setVecReal(unsigned int i, double val){m_v[i][0]=val;}
  inline void setVecImag(unsigned int i, double val){m_v[i][1]=val;}
  inline void set_ptr(fftw_complex*& new_ptr){// call a clear before.
    m_v = new_ptr;
  }

  // Access functions:
  inline fftw_complex* v(){return m_v;}
  inline fftw_complex* v(unsigned int i){
    fftw_complex* a;
    a = (fftw_complex*) fftw_malloc(sizeof(fftw_complex));
    a[0][0] = m_v[i][0];
    a[0][1] = m_v[i][1];
    return a;
  }
  inline double real(unsigned int i){return m_v[i][0];}
  inline double imag(unsigned int i){return m_v[i][1];}
  inline fftw_complex* operator()(unsigned int i){return this->v(i);}

  // printing:
  inline void print_size(){cout<< "Complex vec of size: " << m_size << endl;}
  inline void print(){
    print_size();
    for(unsigned int iDof=0; iDof<m_size; iDof++){
      cout<< m_v[iDof][0] << " + i " << m_v[iDof][1] << endl;
    }
    cout << endl;
  }
};




#endif
