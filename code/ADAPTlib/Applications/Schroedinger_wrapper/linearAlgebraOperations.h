// Implementation of Linear algebra operations:

#ifndef linearAlgebraOperations_h
#define linearAlgebraOperations_h
#include "fftw3.h"
#include <string>
#include <vector>
#include <map>
#include <complex>
#include <math.h>
#include <algorithm>
#include <ostream>
#include <fstream>

#include "vec.h"
#include "mat.h"


// REAL VECS:

// operator which perform the sum of two vectors:
vec_real  operator + (vec_real&, vec_real&);

// operator which perform the difference of two vectors:
vec_real operator - (vec_real&, vec_real&);

// function which perform the dot:
double dot(vec_real&, vec_real&);

// linear combination:
void lin_comb(vector<vec_real>&, vector<double>&, vec_real&);
vec_real operator * (vector<vec_real>&, vector<double>&);

// loading and saving in ascii:
void save(vec_real&, string);
vec_real load(string);


// COMPLEX VECS
// operator which perform the sum of two vectors:
vec_comp  operator + (vec_comp&, vec_comp&);

// operator which perform the difference of two vectors:
vec_comp operator - (vec_comp&, vec_comp&);

// function which perform the dot:
double dot(vec_comp&, vec_comp&);


// REAL MATRICES:
// sum of two sequential real dense matrices:
mat_real operator + (mat_real&, mat_real&);

// difference of two sequential real dense matrices:
mat_real operator - (mat_real&, mat_real&);

// matrix vector product:
vec_real operator * (mat_real&, vec_real&);
// product with the matrix transposed, without computing and transposing the mat
// w = A^T v
vec_real dot_T(mat_real&, vec_real&);

// matrix matrix product:
mat_real operator * (mat_real&, mat_real&);

// A^T B
mat_real dot_T(mat_real&, mat_real&);

// Solve linear system
void solve_linear_fast(mat_real&, vec_real&);
void solve_linear(mat_real&, vec_real&, vec_real&);
vec_real operator / (mat_real&, vec_real&);

// REAL sparse matrices:

// matrix vector product:
vec_real operator * (mat_real_sp&, vec_real&);

#endif
