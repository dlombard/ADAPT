// Header of the mother class tensor:
#ifndef finiteDifferencesPer_h
#define finiteDifferencesPer_h


#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "tensor.h"

using namespace std;

class finiteDifferencesPer{
private:
  tensor m_struct;
  vector<double> m_bounds;
  unsigned int m_nDof;
  vector<double> m_dx;
  mat_real m_mass;

public:
  finiteDifferencesPer(){};
  ~finiteDifferencesPer(){};
  finiteDifferencesPer(unsigned int, vector<unsigned int>);
  finiteDifferencesPer(unsigned int, vector<unsigned int>, vector<double>);

  // Functions for computation:
  void compute_mass();
  vector<double> compute_point(vector<unsigned int>);
  vector<unsigned int> compute_ll(vector<double>);
  vector<vec_real> compute_gradient(vec_real&);
 // bool isItOnBoundary(unsigned int);
 // tuple<unsigned int, unsigned int> whichBoundaryIsOn(unsigned int);
  unsigned int linIndIncrement(unsigned int, vector<int>);
  unsigned int linIndIncrementPer(unsigned int, vector<int>);
  //bool isInBox(unsigned int);


  // ACCESS FUNCTIONS:
  inline tensor struc(){return m_struct;}
  inline unsigned int dim(){return m_struct.nVar();}
  inline vector<unsigned int> dofPerDim(){return m_struct.nDof_var();}
  inline unsigned int nDof(){return m_nDof;}
  inline vector<double> dx(){return m_dx;}
  inline double dx(int iVar){return m_dx[iVar];}
  inline mat_real mass(){return m_mass;}
  inline vector<double> bounds(){return m_bounds;}
  inline double bounds(unsigned int iB){return m_bounds[iB];}

  // Print function:
  inline void print(){
    m_struct.print();
    std::cout << "Total number of dofs: " << m_nDof << endl;
    std::cout << "SpaceBox:" << endl;
    for(unsigned int iVar=0; iVar<m_struct.nVar(); iVar++){
      cout << "[ " << m_bounds[2*iVar] << "," << m_bounds[2*iVar+1] << "]" << endl;
    }
  }

};

#endif
