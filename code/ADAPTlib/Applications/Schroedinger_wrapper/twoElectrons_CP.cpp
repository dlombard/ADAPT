// implementation of the class two electrons
#include "twoElectrons_CP.h"




// II - IMPLEMENTATION of the class twoElectrons:


// II.1 Constructors:
/* II.1.1 - construct given final time and number of iterations
  - input: T and m_nIt
  - output: construct the object
*/
twoElectrons_CP::twoElectrons_CP(double T, unsigned int nSteps){
  m_nIt = nSteps;
  assert(m_nIt>1);
  m_tFin = T;
  assert(T>0);
  m_dt = m_tFin/(m_nIt - 1);

}

// II.1.2: give the physical space dimensions:
twoElectrons_CP::twoElectrons_CP(double L, unsigned int dim, vector<unsigned int> Ndof){
  m_L = L;
  m_dim = dim;
  m_Ndof = Ndof;

  vector<unsigned int> Dofper(dim);
  unsigned int ndof =1;
  for (unsigned int i=0; i<dim; i++)
  {
	assert( Ndof[i] % 2 == 1);
	Dofper[i] = Ndof[i];
        ndof *= Ndof[i];
  }

  m_ndof = ndof;

  vector<double> box(2*dim);
  for (unsigned int i=0; i<dim; i++)
  {
	box[2*i] = -L;
        box[2*i+1] = L;
  }

  finiteDifferencesPer testSpace(dim, Dofper,box);
  setFiniteDifferences(testSpace);
}


// II.2 : set finite differences in space:
void twoElectrons_CP::setFiniteDifferences(finiteDifferencesPer& testSpace)
{
	m_testSpace = testSpace;
  m_ndof = testSpace.nDof();
  m_Ndof = testSpace.struc().nDof_var();
}



// update analytically the kinetic part: 1/2(d^2_x + d^2_y) + d^2_z.
void twoElectrons_CP::update_kinetic_1d(CP3var_comp& Psi, double step, vector<double> L){
  const unsigned int rank = Psi.rank();
  const double pi = 4.0*atan(1.0);

  // cycle over modes:
  for(unsigned int iTerm=0; iTerm<rank; iTerm++){

    // -- modes in x: --
    const double L_x = L[0];
    int N_x = Psi.R(iTerm).size();
    fftw_complex *out_x; fftw_plan plan_x;
    out_x = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_x);
    vec_comp r = Psi.R(iTerm);
    fftw_complex *in_x = r.v();
    plan_x = fftw_plan_dft_1d(N_x, in_x, out_x, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan_x);

    unsigned int Nyquist_x = (N_x%2==0) ? N_x/2 : (N_x+1)/2;

    for(unsigned int k=0; k<Nyquist_x; k++){
      double kappa = (2.0*pi*k)/L_x;
      double theta_k = 0.5*step*(kappa*kappa);
      double re = out_x[k][0];
      double im = out_x[k][1];
      out_x[k][0] = re*cos(theta_k) - im*sin(theta_k);
      out_x[k][1] = re*sin(theta_k) + im*cos(theta_k);
    }
    for(unsigned int k=1; k<Nyquist_x;k++){
      double kappa = (2.0*pi*(N_x-k))/L_x;
      double theta_k = 0.5*step*(kappa*kappa);
      double re = out_x[N_x-k][0];
      double im = out_x[N_x-k][1];
      out_x[N_x-k][0] = re*cos(theta_k) - im*sin(theta_k);
      out_x[N_x-k][1] = re*sin(theta_k) + im*cos(theta_k);
    }

    // free the memory:
    fftw_destroy_plan(plan_x);

    // ifftw on out_x
    fftw_plan inv_plan_x;
    fftw_complex* modes_r = r.v();
    inv_plan_x = fftw_plan_dft_1d(N_x, out_x, modes_r , FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(inv_plan_x);

    const double renorm_x = 1.0/N_x;
    r *= renorm_x; // this is the pointer to Psi.R(iTerm);

    // free the memory:
    fftw_destroy_plan(inv_plan_x); fftw_free(out_x);


    // -- modes in z: --
    const double L_z = L[1];
    int N_z = Psi.G(iTerm).size();
    fftw_complex *out_z; fftw_plan plan_z;
    out_z = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_z);
    vec_comp g = Psi.G(iTerm);
    fftw_complex *in_z = g.v();
    plan_z = fftw_plan_dft_1d(N_z, in_z, out_z, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan_z);

    unsigned int Nyquist_z = (N_z%2==0) ? N_z/2 : (N_z+1)/2;

    for(unsigned int k=0; k<Nyquist_z; k++){
      double kappa = (2.0*pi*k)/L_z;
      double theta_k = step*(kappa*kappa);
      double re = out_z[k][0];
      double im = out_z[k][1];
      out_z[k][0] = re*cos(theta_k) - im*sin(theta_k);
      out_z[k][1] = re*sin(theta_k) + im*cos(theta_k);
    }
    for(unsigned int k=1; k<Nyquist_z;k++){
      double kappa = (2.0*pi*(N_z-k))/L_z;
      double theta_k = step*(kappa*kappa);
      double re = out_z[N_z-k][0];
      double im = out_z[N_z-k][1];
      out_z[N_z-k][0] = re*cos(theta_k) - im*sin(theta_k);
      out_z[N_z-k][1] = re*sin(theta_k) + im*cos(theta_k);
    }

    // free the memory:
    fftw_destroy_plan(plan_z);

    // ifftw on out_x
    fftw_plan inv_plan_z;
    fftw_complex* mode_z = g.v();
    inv_plan_x = fftw_plan_dft_1d(N_z, out_z, mode_z, FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(inv_plan_z);

    const double renorm_z = 1.0/N_z;
    g *= renorm_z; // this is the pointer to Psi.R(iTerm);

    // free the memory:
    fftw_destroy_plan(inv_plan_z); fftw_free(out_z);


    // -- modes in y: --
    const double L_y = L[2];
    int N_y = Psi.S(iTerm).size();
    fftw_complex *out_y; fftw_plan plan_y;
    out_y = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_y);
    vec_comp s = Psi.S(iTerm);
    fftw_complex *in_y = s.v();
    plan_y = fftw_plan_dft_1d(N_y, in_y, out_y, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan_y);

    unsigned int Nyquist_y = (N_y%2==0) ? N_y/2 : (N_y+1)/2;

    for(unsigned int k=0; k<Nyquist_y; k++){
      double kappa = (2.0*pi*k)/L_y;
      double theta_k = 0.5*step*(kappa*kappa);
      double re = out_y[k][0];
      double im = out_y[k][1];
      out_y[k][0] = re*cos(theta_k) - im*sin(theta_k);
      out_y[k][1] = re*sin(theta_k) + im*cos(theta_k);
    }
    for(unsigned int k=1; k<Nyquist_y;k++){
      double kappa = (2.0*pi*(N_y-k))/L_y;
      double theta_k = 0.5*step*(kappa*kappa);
      double re = out_y[N_y-k][0];
      double im = out_y[N_y-k][1];
      out_y[N_y-k][0] = re*cos(theta_k) - im*sin(theta_k);
      out_y[N_y-k][1] = re*sin(theta_k) + im*cos(theta_k);
    }

    // free the memory:
    fftw_destroy_plan(plan_y);

    // ifftw on out_x
    fftw_plan inv_plan_y;
    fftw_complex* mode_y = s.v();
    inv_plan_y = fftw_plan_dft_1d(N_y, out_y, mode_y, FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(inv_plan_y);

    const double renorm_y = 1.0/N_y;
    s *= renorm_y; // this is the pointer to Psi.R(iTerm);

    // free the memory:
    fftw_destroy_plan(inv_plan_y); fftw_free(out_y);
  }
}

// update analytically the kinetic part: 1/2(d^2_x + d^2_y) + d^2_z.
// Modes stored in Fourier.
void twoElectrons_CP::update_kinetic_1dF(CP3var_comp& Psi, double step, vector<double> L){
  const unsigned int rank = Psi.rank();
  const double pi = 4.0*atan(1.0);

  // cycle over modes:
  for(unsigned int iTerm=0; iTerm<rank; iTerm++){

    // -- modes in x: --
    const double L_x = L[0];
    int N_x = Psi.R(iTerm).size();
    unsigned int Nyquist_x = (N_x%2==0) ? N_x/2 : (N_x+1)/2;
    vec_comp r = Psi.R(iTerm);
    fftw_complex *out_x = r.v();

    for(unsigned int k=0; k<Nyquist_x; k++){
      double kappa = (2.0*pi*k)/L_x;
      double theta_k = 0.5*step*(kappa*kappa);
      double re = out_x[k][0];
      double im = out_x[k][1];
      out_x[k][0] = re*cos(theta_k) - im*sin(theta_k);
      out_x[k][1] = re*sin(theta_k) + im*cos(theta_k);
    }
    for(unsigned int k=1; k<Nyquist_x;k++){
      double kappa = -(2.0*pi*k)/L_x;
      double theta_k = 0.5*step*(kappa*kappa);
      double re = out_x[N_x-k][0];
      double im = out_x[N_x-k][1];
      out_x[N_x-k][0] = re*cos(theta_k) - im*sin(theta_k);
      out_x[N_x-k][1] = re*sin(theta_k) + im*cos(theta_k);
    }


    // -- modes in z: --
    const double L_z = L[1];
    int N_z = Psi.G(iTerm).size();
    unsigned int Nyquist_z = (N_z%2==0) ? N_z/2 : (N_z+1)/2;
    vec_comp g = Psi.G(iTerm);
    fftw_complex *out_z = g.v();

    for(unsigned int k=0; k<Nyquist_z; k++){
      double kappa = (2.0*pi*k)/L_z;
      double theta_k = step*(kappa*kappa);
      double re = out_z[k][0];
      double im = out_z[k][1];
      out_z[k][0] = re*cos(theta_k) - im*sin(theta_k);
      out_z[k][1] = re*sin(theta_k) + im*cos(theta_k);
    }
    for(unsigned int k=1; k<Nyquist_z;k++){
      double kappa = -(2.0*pi*k)/L_z;
      double theta_k = step*(kappa*kappa);
      double re = out_z[N_z-k][0];
      double im = out_z[N_z-k][1];
      out_z[N_z-k][0] = re*cos(theta_k) - im*sin(theta_k);
      out_z[N_z-k][1] = re*sin(theta_k) + im*cos(theta_k);
    }


    // -- modes in y: --
    const double L_y = L[2];
    int N_y = Psi.S(iTerm).size();
    unsigned int Nyquist_y = (N_y%2==0) ? N_y/2 : (N_y+1)/2;
    vec_comp s = Psi.S(iTerm);
    fftw_complex *out_y = s.v();

    for(unsigned int k=0; k<Nyquist_y; k++){
      double kappa = (2.0*pi*k)/L_y;
      double theta_k = 0.5*step*(kappa*kappa);
      double re = out_y[k][0];
      double im = out_y[k][1];
      out_y[k][0] = re*cos(theta_k) - im*sin(theta_k);
      out_y[k][1] = re*sin(theta_k) + im*cos(theta_k);
    }
    for(unsigned int k=1; k<Nyquist_y;k++){
      double kappa = -(2.0*pi*k)/L_y;
      double theta_k = 0.5*step*(kappa*kappa);
      double re = out_y[N_y-k][0];
      double im = out_y[N_y-k][1];
      out_y[N_y-k][0] = re*cos(theta_k) - im*sin(theta_k);
      out_y[N_y-k][1] = re*sin(theta_k) + im*cos(theta_k);
    }
    // end of loop over terms.
  }
}


// update according to the potential, analytically, it preserves the rank.
void twoElectrons_CP::update_potential_1d(CP3var_comp& Psi, double step){
  // nucleous position
  const double x_0 = 0.0;
  const double y_0 = 0.0;
  vector<unsigned int> res = m_testSpace.dofPerDim();
  vector<double> dx_vec = m_testSpace.dx();
  vector<double> box = m_testSpace.bounds();
  // Remark: in 1d there is just one component,
  // it is intended that N_x = N_y = N_z, dx = dy = dz

  const unsigned int rank = Psi.rank();
  for(unsigned int iTerm=0; iTerm<rank; iTerm++){
    // -- modes x --
    vec_comp r = Psi.R(iTerm);
    for(unsigned int iDof=0; iDof<res[0]; iDof++){
      double x_i = box[0] + iDof*dx_vec[0];
      double V_x = 1.0/fabs(x_i - x_0);
      double theta_i = step*V_x;
      double re = r.real(iDof);
      double im = r.imag(iDof);
      double val_re = re*cos(theta_i) - im*sin(theta_i);
      double val_im = re*sin(theta_i) + im*cos(theta_i);
      r.setVecReal(iDof, val_re);
      r.setVecImag(iDof, val_im);
    }

    // -- modes z --
    vec_comp g = Psi.G(iTerm);
    for(unsigned int iDof=0; iDof<res[1]; iDof++){
      double z_i = box[0] + iDof*dx_vec[0];
      double V_z = 1.0/fabs(z_i);
      double theta_i = step*V_z;
      double re = g.real(iDof);
      double im = g.imag(iDof);
      double val_re = re*cos(theta_i) - im*sin(theta_i);
      double val_im = re*sin(theta_i) + im*cos(theta_i);
      g.setVecReal(iDof, val_re);
      g.setVecImag(iDof, val_im);
    }

    // -- modes y --
    vec_comp s = Psi.S(iTerm);
    for(unsigned int iDof=0; iDof<res[2]; iDof++){
      double y_i = box[0] + iDof*dx_vec[0];
      double V_y = 1.0/fabs(y_i - y_0);
      double theta_i = step*V_y;
      double re = r.real(iDof);
      double im = r.imag(iDof);
      double val_re = re*cos(theta_i) - im*sin(theta_i);
      double val_im = re*sin(theta_i) + im*cos(theta_i);
      s.setVecReal(iDof, val_re);
      s.setVecImag(iDof, val_im);
    }
  }
}

// update potential when using Fourier representation:
// 1) anti-transform  2) evolve  3) transform
void twoElectrons_CP::update_potential_1dF(CP3var_comp& Psi, double step){
  // nucleous position
  const double x_0 = 0.0;
  const double y_0 = 0.0;
  vector<unsigned int> res = m_testSpace.dofPerDim();
  vector<double> dx_vec = m_testSpace.dx();
  vector<double> box = m_testSpace.bounds();
  // Remark: in 1d there is just one component,
  // it is intended that N_x = N_y = N_z, dx = dy = dz

  const unsigned int rank = Psi.rank();
  for(unsigned int iTerm=0; iTerm<rank; iTerm++){
    // -- modes x --
    int N_x = Psi.R(iTerm).size();
    vec_comp r = Psi.R(iTerm);
    fftw_complex *in_x = r.v();

    // anti-transform:
    fftw_plan inv_plan_x;
    fftw_complex* out_x;
    out_x = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_x);
    inv_plan_x = fftw_plan_dft_1d(N_x, in_x, out_x , FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(inv_plan_x);

    const double renorm_x = 1.0/N_x;
    for(unsigned int iDof=0; iDof<N_x; iDof++){
      out_x[iDof][0] *= renorm_x;
      out_x[iDof][1] *= renorm_x;
    }

    // evolve in time in the physical space:
    for(unsigned int iDof=0; iDof<res[0]; iDof++){
      double x_i = box[0] + iDof*dx_vec[0];

      //double V_x = 1.0/fabs(x_i - x_0); // COULOMB nucleous
      double V_x = 0.0; // VIRGINIUM-DAMINUM

      double theta_i = step*V_x;
      double re = out_x[iDof][0];
      double im = out_x[iDof][1];
      double val_re = re*cos(theta_i) - im*sin(theta_i);
      double val_im = re*sin(theta_i) + im*cos(theta_i);
      out_x[iDof][0] = val_re;
      out_x[iDof][1] = val_im;
    }

    // transform back in the Fourier space:
    fftw_plan plan_x;
    plan_x = fftw_plan_dft_1d(N_x, out_x, in_x , FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan_x);

    // free the memory:
    fftw_destroy_plan(inv_plan_x);
    fftw_destroy_plan(plan_x);
    fftw_free(out_x);

    // -- modes z --
    int N_z = Psi.G(iTerm).size();
    vec_comp g = Psi.G(iTerm);
    fftw_complex *in_z = g.v();

    // anti-transform:
    fftw_plan inv_plan_z;
    fftw_complex* out_z;
    out_z = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_z);
    inv_plan_z = fftw_plan_dft_1d(N_z, in_z, out_z , FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(inv_plan_z);

    const double renorm_z = 1.0/N_z;
    for(unsigned int iDof=0; iDof<N_z; iDof++){
      out_z[iDof][0] *= renorm_z;
      out_z[iDof][1] *= renorm_z;
    }

    // evolve in time in the physical space:
    for(unsigned int iDof=0; iDof<res[0]; iDof++){
      double z_i = box[0] + iDof*dx_vec[0];

      //double V_z = 1.0/fabs(z_i); // COULOMB electron-electron
      double V_z = z_i*z_i;

      double theta_i = step*V_z;
      double re = out_z[iDof][0];
      double im = out_z[iDof][1];
      double val_re = re*cos(theta_i) - im*sin(theta_i);
      double val_im = re*sin(theta_i) + im*cos(theta_i);
      out_z[iDof][0] = val_re;
      out_z[iDof][1] = val_im;
    }

    // transform back in the Fourier space:
    fftw_plan plan_z;
    plan_z = fftw_plan_dft_1d(N_z, out_z, in_z , FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan_z);

    // free the memory:
    fftw_destroy_plan(inv_plan_z);
    fftw_destroy_plan(plan_z);
    fftw_free(out_z);

    // -- modes y --
    int N_y = Psi.S(iTerm).size();
    vec_comp s = Psi.S(iTerm);
    fftw_complex *in_y = s.v();

    // anti-transform:
    fftw_plan inv_plan_y;
    fftw_complex* out_y;
    out_y = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_y);
    inv_plan_y = fftw_plan_dft_1d(N_y, in_y, out_y , FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(inv_plan_y);

    const double renorm_y = 1.0/N_y;
    for(unsigned int iDof=0; iDof<N_y; iDof++){
      out_y[iDof][0] *= renorm_y;
      out_y[iDof][1] *= renorm_y;
    }

    // evolve in time in the physical space:
    for(unsigned int iDof=0; iDof<res[0]; iDof++){
      double y_i = box[0] + iDof*dx_vec[0];

      //double V_y = 1.0/fabs(y_i - x_0); // COULOMB nucleaous
      double V_y = 0.0; // VIRGINIUM-DAMIANUM

      double theta_i = step*V_y;
      double re = out_y[iDof][0];
      double im = out_y[iDof][1];
      double val_re = re*cos(theta_i) - im*sin(theta_i);
      double val_im = re*sin(theta_i) + im*cos(theta_i);
      out_y[iDof][0] = val_re;
      out_y[iDof][1] = val_im;
    }

    // transform back in the Fourier space:
    fftw_plan plan_y;
    plan_y = fftw_plan_dft_1d(N_y, out_y, in_y , FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan_y);

    // free the memory:
    fftw_destroy_plan(inv_plan_y);
    fftw_destroy_plan(plan_y);
    fftw_free(out_y);
  }
  // end of loop over terms
}



// Auxiliary function:
void twoElectrons_CP::diagonal_rescale_fftw_re(unsigned int N, double L, fftw_complex*& in, double exponent, double alpha, vec_comp& out){
  const double pi = 4.0*atan(1.0);
  unsigned int Nyquist = (N%2==0) ? N/2 : (N+1)/2;
  const double renorm = 1.0/N;

  fftw_complex* m_toAdd;
  m_toAdd = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int k=0; k<Nyquist; k++){
    double kappa = (2.0*pi*k)/L;
    double th_k = pow(kappa,exponent)*alpha;
    m_toAdd[k][0] = in[k][0]*th_k;
    m_toAdd[k][1] = in[k][1]*th_k;
  }
  for(unsigned int k=1; k<Nyquist;k++){
    double kappa = (2.0*pi*(N-k))/L;
    double th_k = pow(kappa,exponent)*alpha;
    m_toAdd[N-k][0] = in[N-k][0]*th_k;
    m_toAdd[N-k][1] = in[N-k][1]*th_k;
  }
  fftw_plan inv_plan;
  fftw_complex* modes = out.v();
  inv_plan = fftw_plan_dft_1d(N, m_toAdd, modes , FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(inv_plan);
  // renormalise:
  out *= renorm;
  fftw_destroy_plan(inv_plan);
  fftw_free(m_toAdd);
}

// alpha is pure imaginary => Real part becomes imaginary, imaginary becomes -Real
void twoElectrons_CP::diagonal_rescale_fftw_im(unsigned int N, double L, fftw_complex*& in, double exponent, double alpha, vec_comp& out){
  const double pi = 4.0*atan(1.0);
  unsigned int Nyquist = (N%2==0) ? N/2 : (N+1)/2;
  const double renorm = 1.0/N;

  fftw_complex* m_toAdd;
  m_toAdd = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);
  for(unsigned int k=0; k<Nyquist; k++){
    double kappa = (2.0*pi*k)/L;
    double th_k = pow(kappa,exponent)*alpha;
    m_toAdd[k][0] = -in[k][1]*th_k;
    m_toAdd[k][1] = in[k][0]*th_k;
  }
  for(unsigned int k=1; k<Nyquist;k++){
    double kappa = (2.0*pi*(N-k))/L;
    double th_k = pow(kappa,exponent)*alpha;
    m_toAdd[N-k][0] = -in[N-k][1]*th_k;
    m_toAdd[N-k][1] = in[N-k][0]*th_k;
  }
  fftw_plan inv_plan;
  fftw_complex* modes = out.v();
  inv_plan = fftw_plan_dft_1d(N, m_toAdd, modes , FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(inv_plan);
  // renormalise:
  out *= renorm;
  fftw_destroy_plan(inv_plan);
  fftw_free(m_toAdd);
}



// function for the d^2_{xz} - d^2_{yz} part:
void twoElectrons_CP::update_coupling_1d(CP3var_comp& Phi_e, CP3var_comp& Phi_o, double step, vector<double> L){
  const double pi = 4.0*atan(1.0);
  const unsigned int rank_e = Phi_e.rank();
  const unsigned int rank_o = Phi_o.rank();
  const double L_x = L[0];
  const double L_z = L[1];
  const double L_y = L[2];
  int N_x = Phi_e.R(0).size();
  int N_y = Phi_e.G(0).size();
  int N_z = Phi_e.S(0).size();
  unsigned int Nyquist_x = (N_x%2==0) ? N_x/2 : (N_x+1)/2;
  unsigned int Nyquist_y = (N_y%2==0) ? N_y/2 : (N_y+1)/2;
  unsigned int Nyquist_z = (N_z%2==0) ? N_z/2 : (N_z+1)/2;
  const double renorm_x = 1.0/N_x;
  const double renorm_y = 1.0/N_y;
  const double renorm_z = 1.0/N_z;

  const unsigned int n_toAdd_e = 3*rank_e + 8*rank_o;
  const unsigned int n_toAdd_o = 3*rank_o + 8*rank_e;

  // Compute 6 tables of Fourier transform of the modes:
  vector<fftw_complex*> R_e(rank_e);
  vector<fftw_complex*> G_e(rank_e);
  vector<fftw_complex*> S_e(rank_e);

  vector<fftw_complex*> R_o(rank_o);
  vector<fftw_complex*> G_o(rank_o);
  vector<fftw_complex*> S_o(rank_o);

  for(unsigned int iTerm=0; iTerm<rank_e; iTerm++){
    // term R_e:
    R_e[iTerm] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_x);
    fftw_plan pl_r_e;
    vec_comp term_r = Phi_e.R(iTerm);
    fftw_complex* in_r = term_r.v();
    pl_r_e = fftw_plan_dft_1d(N_x, in_r, R_e[iTerm], FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(pl_r_e);
    fftw_destroy_plan(pl_r_e);

    // term G_e:
    G_e[iTerm] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_z);
    fftw_plan pl_g_e;
    vec_comp term_g = Phi_e.G(iTerm);
    fftw_complex* in_g = term_g.v();
    pl_g_e = fftw_plan_dft_1d(N_z, in_g, G_e[iTerm], FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(pl_g_e);
    fftw_destroy_plan(pl_g_e);

    // term S_e:
    S_e[iTerm] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_y);
    fftw_plan pl_s_e;
    vec_comp term_s = Phi_e.S(iTerm);
    fftw_complex* in_s = term_s.v();
    pl_s_e = fftw_plan_dft_1d(N_y, in_s, S_e[iTerm], FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(pl_s_e);
    fftw_destroy_plan(pl_s_e);
  }


  for(unsigned int iTerm=0; iTerm<rank_o; iTerm++){
    // term R_e:
    R_o[iTerm] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_x);
    fftw_plan pl_r_o;
    vec_comp term_r = Phi_o.R(iTerm);
    fftw_complex* in_r = term_r.v();
    pl_r_o = fftw_plan_dft_1d(N_x, in_r, R_o[iTerm], FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(pl_r_o);
    fftw_destroy_plan(pl_r_o);

    // term G_e:
    G_o[iTerm] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_z);
    fftw_plan pl_g_o;
    vec_comp term_g = Phi_o.G(iTerm);
    fftw_complex* in_g = term_g.v();
    pl_g_o = fftw_plan_dft_1d(N_z, in_g, G_o[iTerm], FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(pl_g_o);
    fftw_destroy_plan(pl_g_o);

    // term S_e:
    S_o[iTerm] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N_y);
    fftw_plan pl_s_o;
    vec_comp term_s = Phi_o.S(iTerm);
    fftw_complex* in_s = term_s.v();
    pl_s_o = fftw_plan_dft_1d(N_y, in_s, S_o[iTerm], FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(pl_s_o);
    fftw_destroy_plan(pl_s_o);
  }


  // -- Adding terms to the tensor e: --

  // adding 3 terms U_e
  for(unsigned int iTerm=0; iTerm<rank_e; iTerm++){
    // term -k_z^2 * k_x^2 * dt^2 =  (-k_z^2*dt)*(k_x^2*dt)
    vec_comp toAdd_r_1(N_x);
    diagonal_rescale_fftw_re(N_x, L_x, R_e[iTerm], 2.0, step, toAdd_r_1);

    vec_comp toAdd_g_1(N_z);
    diagonal_rescale_fftw_re(N_z, L_z, G_e[iTerm], 2.0, -1.0*step, toAdd_g_1);

    vec_comp term_s = Phi_e.S(iTerm);
    vec_comp toAdd_s_1; toAdd_s_1 << term_s;

    Phi_e.append(toAdd_r_1, toAdd_g_1, toAdd_s_1);


    // term -k_z^2 * k_y^2 * dt^2 =  (-k_z^2*dt)*(k_y^2*dt)
    vec_comp toAdd_g_2(N_z); toAdd_g_2 << toAdd_g_1;

    vec_comp term_r = Phi_e.R(iTerm);
    vec_comp toAdd_r_2; toAdd_r_2 << term_r;

    vec_comp toAdd_s_2(N_y);
    diagonal_rescale_fftw_re(N_y, L_y, S_e[iTerm], 2.0, step, toAdd_s_2);

    Phi_e.append(toAdd_r_2, toAdd_g_2, toAdd_s_2);


    // term 2 k_z^2 * k_y * k_x* dt^2 = -2 (-k_z^2*dt)*(k_y*dt^{1/2})*(k_x*dt^{1/2})
    vec_comp toAdd_g_3; toAdd_g_3 << toAdd_g_1;
    toAdd_g_3 *= (-2.0);

    vec_comp toAdd_r_3(N_x);
    diagonal_rescale_fftw_re(N_x, L_x, R_e[iTerm], 1.0, sqrt(step), toAdd_r_3);

    vec_comp toAdd_s_3(N_y);
    diagonal_rescale_fftw_re(N_y, L_y, S_e[iTerm], 1.0, sqrt(step), toAdd_s_3);

    Phi_e.append(toAdd_r_3, toAdd_g_3, toAdd_s_3);
  }

  // adding 8 terms U_o:
  for(unsigned int iTerm=0; iTerm<rank_o; iTerm++){

    // term -k_z*k_x*dt i = -1.0 * (k_z*sqrt(dt)*i) *(k_x*sqrt(dt))
    vec_comp toAdd_r_1(N_x);
    diagonal_rescale_fftw_re(N_x, L_x, R_o[iTerm], 1.0, sqrt(step), toAdd_r_1);

    vec_comp toAdd_g_1;
    diagonal_rescale_fftw_im(N_z, L_z, G_o[iTerm], 1.0, sqrt(step), toAdd_r_1);

    vec_comp term_s = Phi_o.S(iTerm);
    vec_comp toAdd_s_1; toAdd_s_1 << term_s;



  }


  // -- Adding the terms to the tensor o: --
}



// coupling operator, Fourier representation
void twoElectrons_CP::update_coupling_1dF(CP3var_comp& Phi_e, CP3var_comp& Phi_o, double step, vector<double> L){
  const double pi = 4.0*atan(1.0);
  const unsigned int rank_e = Phi_e.rank();
  const unsigned int rank_o = Phi_o.rank();
  const double L_x = L[0];
  const double L_z = L[1];
  const double L_y = L[2];
  int N_x = Phi_e.R(0).size();
  int N_y = Phi_e.G(0).size();
  int N_z = Phi_e.S(0).size();
  unsigned int Nyquist_x = (N_x%2==0) ? N_x/2 : (N_x+1)/2;
  unsigned int Nyquist_y = (N_y%2==0) ? N_y/2 : (N_y+1)/2;
  unsigned int Nyquist_z = (N_z%2==0) ? N_z/2 : (N_z+1)/2;
  const double renorm_x = 1.0/N_x;
  const double renorm_y = 1.0/N_y;
  const double renorm_z = 1.0/N_z;

  // I - updating Phi_e:
  // Adding the 3 terms depnding upon Phi_e:
  //-dt^2(k_x^2 x k_z^2 x 1 - 2 k_x x k_z^2 x k_y + 1 x k_z^2 x k_y^2)
  for(unsigned int iTerm=0; iTerm<rank_e; iTerm++){
    vec_comp r = Phi_e.R(iTerm);
    vec_comp g = Phi_e.G(iTerm);
    vec_comp s = Phi_e.S(iTerm);

    // -dt^2 k_x^2 x k_z^2 x 1
    vec_comp r_e_1; r_e_1 << r;
    vec_comp g_e_1; g_e_1 << g;
    vec_comp s_e_1; s_e_1 << s; // s is not modified
    fftw_complex* r_1 = r_e_1.v();
    for(unsigned int k=0; k<Nyquist_x; k++){
      double kappa = (2.0*pi*k)/L_x;
      double th_k = -kappa*kappa*step;
      r_1[k][0]*=th_k;
      r_1[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_x;k++){
      double kappa = -(2.0*pi*k)/L_x;
      double th_k = -kappa*kappa*step;
      r_1[N_x-k][0] *= th_k;
      r_1[N_x-k][1] *= th_k;
    }
    fftw_complex* g_1 = g_e_1.v();
    for(unsigned int k=0; k<Nyquist_z; k++){
      double kappa = (2.0*pi*k)/L_z;
      double th_k = kappa*kappa*step;
      g_1[k][0]*=th_k;
      g_1[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_z;k++){
      double kappa = -(2.0*pi*k)/L_z;
      double th_k = kappa*kappa*step;
      g_1[N_z-k][0] *= th_k;
      g_1[N_z-k][1] *= th_k;
    }
    // appending the term:
    Phi_e.append(r_e_1, g_e_1, s_e_1);

    // + 2.0 * dt^2 k_x * k_z^2 * k_y
    vec_comp r_e_2; r_e_2 << r;
    vec_comp g_e_2; g_e_2 << g_e_1; // k_z^2 dt already computed, just copy it!
    vec_comp s_e_2; s_e_2 << s;
    fftw_complex* r_2 = r_e_2.v();
    for(unsigned int k=0; k<Nyquist_x; k++){
      double kappa = (2.0*pi*k)/L_x;
      double th_k = sqrt(2.0*step)*kappa;
      r_2[k][0]*=th_k;
      r_2[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_x;k++){
      double kappa = -(2.0*pi*k)/L_x;
      double th_k = sqrt(2.0*step)*kappa;
      r_2[N_x-k][0] *= th_k;
      r_2[N_x-k][1] *= th_k;
    }
    fftw_complex* s_2 = s_e_2.v();
    for(unsigned int k=0; k<Nyquist_y; k++){
      double kappa = (2.0*pi*k)/L_y;
      double th_k = sqrt(2*step)*kappa;
      s_2[k][0]*=th_k;
      s_2[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_y; k++){
      double kappa = -(2.0*pi*k)/L_y;
      double th_k = sqrt(2*step)*kappa;
      s_2[N_y-k][0] *= th_k;
      s_2[N_y-k][1] *= th_k;
    }
    Phi_e.append(r_e_2, g_e_2, s_e_2);

    // -dt^2 1 * k_z^2 * k_y^2
    vec_comp r_e_3; r_e_3 << r; // just copy it
    vec_comp g_e_3; g_e_3 << g_e_1; // k_z^2 dt already computed, just copy it!
    vec_comp s_e_3; s_e_3 << s;
    fftw_complex* s_3 = s_e_3.v();
    for(unsigned int k=0; k<Nyquist_y; k++){
      double kappa = (2.0*pi*k)/L_y;
      double th_k = -step*kappa*kappa;
      s_3[k][0]*=th_k;
      s_3[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_y; k++){
      double kappa = -(2.0*pi*k)/L_y;
      double th_k = -step*kappa*kappa;
      s_3[N_y-k][0] *= th_k;
      s_3[N_y-k][1] *= th_k;
    }
    Phi_e.append(r_e_3, g_e_3, s_e_3);
  }

  // updating Phi_e by adding the contributions of Phi_o:
  // - i*dt*( k_x*k_z*1 - 1*k_z*k_y )
  // +i*dt^3*(k_x^3*k_z^3*1 -3*k_x^2*k_z^3*k_y + 3*k_x*k_z^3*k_y^2 - 1*k_z^3*k_y^3)
  for(unsigned int iTerm=0; iTerm<rank_o; iTerm++){
    vec_comp r = Phi_o.R(iTerm);
    vec_comp g = Phi_o.G(iTerm);
    vec_comp s = Phi_o.S(iTerm);

    // -i*dt k_x x k_z x 1
    vec_comp r_e_1; r_e_1 << r;
    vec_comp g_e_1; g_e_1 << g;
    vec_comp s_e_1; s_e_1 << s; // s is not modified
    fftw_complex* r_1 = r_e_1.v();
    for(unsigned int k=0; k<Nyquist_x; k++){
      double kappa = (2.0*pi*k)/L_x;
      double th_k = kappa;
      r_1[k][0] *= th_k;
      r_1[k][1] *= th_k;
    }
    for(unsigned int k=1; k<Nyquist_x;k++){
      double kappa = -(2.0*pi*k)/L_x;
      double th_k = kappa;
      r_1[N_x-k][0] *= th_k;
      r_1[N_x-k][1] *= th_k;
    }
    fftw_complex* g_1 = g_e_1.v();
    for(unsigned int k=0; k<Nyquist_z; k++){
      double kappa = (2.0*pi*k)/L_z;
      double th_k = kappa*step;
      double Re = g_1[k][0];
      double Im = g_1[k][1];
      g_1[k][0] = -th_k*Im;
      g_1[k][1] = th_k*Re;
    }
    for(unsigned int k=1; k<Nyquist_z;k++){
      double kappa = -(2.0*pi*k)/L_z;
      double th_k = kappa*step;
      double Re = g_1[N_z-k][0];
      double Im = g_1[N_z-k][1];
      g_1[N_z-k][0] = -th_k*Im;
      g_1[N_z-k][1] = th_k*Re;
    }
    // appending the term:
    Phi_e.append(r_e_1, g_e_1, s_e_1);

    // 1 x (-i*dtk_z) x (-k_y)
    vec_comp r_e_2; r_e_2 << r; // r is not modified
    vec_comp g_e_2; g_e_2 << g_e_1; // we have -i*dt*k_z
    vec_comp s_e_2; s_e_2 << s;
    fftw_complex* s_2 = s_e_2.v();
    for(unsigned int k=0; k<Nyquist_y; k++){
      double kappa = (2.0*pi*k)/L_y;
      double th_k = -kappa;
      s_2[k][0]*=th_k;
      s_2[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_y; k++){
      double kappa = -(2.0*pi*k)/L_y;
      double th_k = -kappa;
      s_2[N_y-k][0] *= th_k;
      s_2[N_y-k][1] *= th_k;
    }
    // appending the term:
    Phi_e.append(r_e_2, g_e_2, s_e_2);


    //  k_x^3 * (i*dt^3*k_z^3) * 1
    vec_comp r_e_3; r_e_3 << r;
    vec_comp g_e_3; g_e_3 << g;
    vec_comp s_e_3; s_e_3 << s; // s is not modified

    fftw_complex* r_3 = r_e_3.v();
    for(unsigned int k=0; k<Nyquist_x; k++){
      double kappa = (2.0*pi*k)/L_x;
      double th_k = kappa*kappa*kappa;
      r_3[k][0] *= th_k;
      r_3[k][1] *= th_k;
    }
    for(unsigned int k=1; k<Nyquist_x;k++){
      double kappa = -(2.0*pi*k)/L_x;
      double th_k = kappa*kappa*kappa;
      r_3[N_x-k][0] *= th_k;
      r_3[N_x-k][1] *= th_k;
    }
    fftw_complex* g_3 = g_e_3.v();
    for(unsigned int k=0; k<Nyquist_z; k++){
      double kappa = (2.0*pi*k)/L_z;
      double th_k = kappa*kappa*kappa*step*step*step;
      double Re = g_3[k][0];
      double Im = g_3[k][1];
      g_3[k][0] = th_k*Im;
      g_3[k][1] = -th_k*Re;
    }
    for(unsigned int k=1; k<Nyquist_z;k++){
      double kappa = -(2.0*pi*k)/L_z;
      double th_k = kappa*kappa*kappa*step*step*step;
      double Re = g_3[N_z-k][0];
      double Im = g_3[N_z-k][1];
      g_3[N_z-k][0] = th_k*Im;
      g_3[N_z-k][1] = -th_k*Re;
    }
    // appending the term:
    Phi_e.append(r_e_3, g_e_3, s_e_3);

    //  3*k_x^2 * (i*dt^3*k_z^3) * (-k_y)
    vec_comp r_e_4; r_e_4 << r;
    vec_comp g_e_4; g_e_4 << g_e_3; // this is not modified
    vec_comp s_e_4; s_e_4 << s_e_2; // we copy it from s_e_2
    fftw_complex* r_4 = r_e_4.v();
    for(unsigned int k=0; k<Nyquist_x; k++){
      double kappa = (2.0*pi*k)/L_x;
      double th_k = 3*kappa*kappa;
      r_4[k][0] *= th_k;
      r_4[k][1] *= th_k;
    }
    for(unsigned int k=1; k<Nyquist_x;k++){
      double kappa = -(2.0*pi*k)/L_x;
      double th_k = 3*kappa*kappa;
      r_4[N_x-k][0] *= th_k;
      r_4[N_x-k][1] *= th_k;
    }
    // appending the term:
    Phi_e.append(r_e_4, g_e_4, s_e_4);

    // k_x*(i*dt^3*k_z^3)* (3*k_y^2)
    vec_comp r_e_5; r_e_5 << r_e_1; // we copy it from r_e_1
    vec_comp g_e_5; g_e_5 << g_e_3; // this is not modified
    vec_comp s_e_5; s_e_5 << s;
    fftw_complex* s_5 = s_e_5.v();
    for(unsigned int k=0; k<Nyquist_y; k++){
      double kappa = (2.0*pi*k)/L_y;
      double th_k = 3*kappa*kappa;
      s_5[k][0]*=th_k;
      s_5[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_y; k++){
      double kappa = -(2.0*pi*k)/L_y;
      double th_k = 3*kappa*kappa;
      s_5[N_y-k][0] *= th_k;
      s_5[N_y-k][1] *= th_k;
    }
    // appending the term:
    Phi_e.append(r_e_5, g_e_5, s_e_5);


    // 1 * (i*dt^3*k_z^3) * -k_y^3
    vec_comp r_e_6; r_e_6 << r; // we copy it from
    vec_comp g_e_6; g_e_6 << g_e_3; // this is not modified
    vec_comp s_e_6; s_e_6 << s;
    fftw_complex* s_6 = s_e_6.v();
    for(unsigned int k=0; k<Nyquist_y; k++){
      double kappa = (2.0*pi*k)/L_y;
      double th_k = -kappa*kappa*kappa;
      s_6[k][0]*=th_k;
      s_6[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_y; k++){
      double kappa = -(2.0*pi*k)/L_y;
      double th_k = -kappa*kappa*kappa;
      s_6[N_y-k][0] *= th_k;
      s_6[N_y-k][1] *= th_k;
    }
    // appending the term:
    Phi_e.append(r_e_6, g_e_6, s_e_6);
  }


  // DOING THE SAME FOR THE UPDATE OF THE ODD PART:

  // II - updating Phi_o:
  // Adding the 3 terms depnding upon Phi_o:
  //-dt^2(k_x^2 x k_z^2 x 1 - 2 k_x x k_z^2 x k_y + 1 x k_z^2 x k_y^2)
  for(unsigned int iTerm=0; iTerm<rank_o; iTerm++){
    vec_comp r = Phi_o.R(iTerm);
    vec_comp g = Phi_o.G(iTerm);
    vec_comp s = Phi_o.S(iTerm);

    // -dt^2 k_x^2 x k_z^2 x 1
    vec_comp r_e_1; r_e_1 << r;
    vec_comp g_e_1; g_e_1 << g;
    vec_comp s_e_1; s_e_1 << s; // s is not modified
    fftw_complex* r_1 = r_e_1.v();
    for(unsigned int k=0; k<Nyquist_x; k++){
      double kappa = (2.0*pi*k)/L_x;
      double th_k = -kappa*kappa*step;
      r_1[k][0]*=th_k;
      r_1[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_x;k++){
      double kappa = -(2.0*pi*k)/L_x;
      double th_k = -kappa*kappa*step;
      r_1[N_x-k][0] *= th_k;
      r_1[N_x-k][1] *= th_k;
    }
    fftw_complex* g_1 = g_e_1.v();
    for(unsigned int k=0; k<Nyquist_z; k++){
      double kappa = (2.0*pi*k)/L_z;
      double th_k = kappa*kappa*step;
      g_1[k][0]*=th_k;
      g_1[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_z;k++){
      double kappa = -(2.0*pi*k)/L_z;
      double th_k = kappa*kappa*step;
      g_1[N_z-k][0] *= th_k;
      g_1[N_z-k][1] *= th_k;
    }
    // appending the term:
    Phi_o.append(r_e_1, g_e_1, s_e_1);

    // + 2.0 * dt^2 k_x * k_z^2 * k_y
    vec_comp r_e_2; r_e_2 << r;
    vec_comp g_e_2; g_e_2 << g_e_1; // k_z^2 dt already computed, just copy it!
    vec_comp s_e_2; s_e_2 << s;
    fftw_complex* r_2 = r_e_2.v();
    for(unsigned int k=0; k<Nyquist_x; k++){
      double kappa = (2.0*pi*k)/L_x;
      double th_k = sqrt(2.0*step)*kappa;
      r_2[k][0]*=th_k;
      r_2[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_x;k++){
      double kappa = -(2.0*pi*k)/L_x;
      double th_k = sqrt(2.0*step)*kappa;
      r_2[N_x-k][0] *= th_k;
      r_2[N_x-k][1] *= th_k;
    }
    fftw_complex* s_2 = s_e_2.v();
    for(unsigned int k=0; k<Nyquist_y; k++){
      double kappa = (2.0*pi*k)/L_y;
      double th_k = sqrt(2*step)*kappa;
      s_2[k][0]*=th_k;
      s_2[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_y; k++){
      double kappa = -(2.0*pi*k)/L_y;
      double th_k = sqrt(2*step)*kappa;
      s_2[N_y-k][0] *= th_k;
      s_2[N_y-k][1] *= th_k;
    }
    Phi_o.append(r_e_2, g_e_2, s_e_2);

    // -dt^2 1 * k_z^2 * k_y^2
    vec_comp r_e_3; r_e_3 << r; // just copy it
    vec_comp g_e_3; g_e_3 << g_e_1; // k_z^2 dt already computed, just copy it!
    vec_comp s_e_3; s_e_3 << s;
    fftw_complex* s_3 = s_e_3.v();
    for(unsigned int k=0; k<Nyquist_y; k++){
      double kappa = (2.0*pi*k)/L_y;
      double th_k = -step*kappa*kappa;
      s_3[k][0]*=th_k;
      s_3[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_y; k++){
      double kappa = -(2.0*pi*k)/L_y;
      double th_k = -step*kappa*kappa;
      s_3[N_y-k][0] *= th_k;
      s_3[N_y-k][1] *= th_k;
    }
    Phi_o.append(r_e_3, g_e_3, s_e_3);
  }

  // updating Phi_o by adding the contributions of Phi_e:
  // - i*dt*( k_x*k_z*1 - 1*k_z*k_y )
  // +i*dt^3*(k_x^3*k_z^3*1 -3*k_x^2*k_z^3*k_y + 3*k_x*k_z^3*k_y^2 - 1*k_z^3*k_y^3)

  for(unsigned int iTerm=0; iTerm<rank_e; iTerm++){
    vec_comp r = Phi_e.R(iTerm);
    vec_comp g = Phi_e.G(iTerm);
    vec_comp s = Phi_e.S(iTerm);

    // -i*dt k_x x k_z x 1
    vec_comp r_e_1; r_e_1 << r;
    vec_comp g_e_1; g_e_1 << g;
    vec_comp s_e_1; s_e_1 << s; // s is not modified
    fftw_complex* r_1 = r_e_1.v();
    for(unsigned int k=0; k<Nyquist_x; k++){
      double kappa = (2.0*pi*k)/L_x;
      double th_k = kappa;
      r_1[k][0] *= th_k;
      r_1[k][1] *= th_k;
    }
    for(unsigned int k=1; k<Nyquist_x;k++){
      double kappa = -(2.0*pi*k)/L_x;
      double th_k = kappa;
      r_1[N_x-k][0] *= th_k;
      r_1[N_x-k][1] *= th_k;
    }
    fftw_complex* g_1 = g_e_1.v();
    for(unsigned int k=0; k<Nyquist_z; k++){
      double kappa = (2.0*pi*k)/L_z;
      double th_k = kappa*step;
      double Re = g_1[k][0];
      double Im = g_1[k][1];
      g_1[k][0] = -th_k*Im;
      g_1[k][1] = th_k*Re;
    }
    for(unsigned int k=1; k<Nyquist_z;k++){
      double kappa = -(2.0*pi*k)/L_z;
      double th_k = kappa*step;
      double Re = g_1[N_z-k][0];
      double Im = g_1[N_z-k][1];
      g_1[N_z-k][0] = -th_k*Im;
      g_1[N_z-k][1] = th_k*Re;
    }
    // appending the term:
    Phi_o.append(r_e_1, g_e_1, s_e_1);

    // 1 x (-i*dtk_z) x (-k_y)
    vec_comp r_e_2; r_e_2 << r; // r is not modified
    vec_comp g_e_2; g_e_2 << g_e_1; // we have -i*dt*k_z
    vec_comp s_e_2; s_e_2 << s;
    fftw_complex* s_2 = s_e_2.v();
    for(unsigned int k=0; k<Nyquist_y; k++){
      double kappa = (2.0*pi*k)/L_y;
      double th_k = -kappa;
      s_2[k][0]*=th_k;
      s_2[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_y; k++){
      double kappa = -(2.0*pi*k)/L_y;
      double th_k = -kappa;
      s_2[N_y-k][0] *= th_k;
      s_2[N_y-k][1] *= th_k;
    }
    // appending the term:
    Phi_o.append(r_e_2, g_e_2, s_e_2);


    //  k_x^3 * (i*dt^3*k_z^3) * 1
    vec_comp r_e_3; r_e_3 << r;
    vec_comp g_e_3; g_e_3 << g;
    vec_comp s_e_3; s_e_3 << s; // s is not modified

    fftw_complex* r_3 = r_e_3.v();
    for(unsigned int k=0; k<Nyquist_x; k++){
      double kappa = (2.0*pi*k)/L_x;
      double th_k = kappa*kappa*kappa;
      r_3[k][0] *= th_k;
      r_3[k][1] *= th_k;
    }
    for(unsigned int k=1; k<Nyquist_x;k++){
      double kappa = -(2.0*pi*k)/L_x;
      double th_k = kappa*kappa*kappa;
      r_3[N_x-k][0] *= th_k;
      r_3[N_x-k][1] *= th_k;
    }
    fftw_complex* g_3 = g_e_3.v();
    for(unsigned int k=0; k<Nyquist_z; k++){
      double kappa = (2.0*pi*k)/L_z;
      double th_k = kappa*kappa*kappa*step*step*step;
      double Re = g_3[k][0];
      double Im = g_3[k][1];
      g_3[k][0] = th_k*Im;
      g_3[k][1] = -th_k*Re;
    }
    for(unsigned int k=1; k<Nyquist_z;k++){
      double kappa = -(2.0*pi*k)/L_z;
      double th_k = kappa*kappa*kappa*step*step*step;
      double Re = g_3[N_z-k][0];
      double Im = g_3[N_z-k][1];
      g_3[N_z-k][0] = th_k*Im;
      g_3[N_z-k][1] = -th_k*Re;
    }
    // appending the term:
    Phi_o.append(r_e_3, g_e_3, s_e_3);

    //  3*k_x^2 * (i*dt^3*k_z^3) * (-k_y)
    vec_comp r_e_4; r_e_4 << r;
    vec_comp g_e_4; g_e_4 << g_e_3; // this is not modified
    vec_comp s_e_4; s_e_4 << s_e_2; // we copy it from s_e_2
    fftw_complex* r_4 = r_e_4.v();
    for(unsigned int k=0; k<Nyquist_x; k++){
      double kappa = (2.0*pi*k)/L_x;
      double th_k = 3*kappa*kappa;
      r_4[k][0] *= th_k;
      r_4[k][1] *= th_k;
    }
    for(unsigned int k=1; k<Nyquist_x;k++){
      double kappa = -(2.0*pi*k)/L_x;
      double th_k = 3*kappa*kappa;
      r_4[N_x-k][0] *= th_k;
      r_4[N_x-k][1] *= th_k;
    }
    Phi_o.append(r_e_4, g_e_4, s_e_4);

    // k_x*(i*dt^3*k_z^3)* (3*k_y^2)
    vec_comp r_e_5; r_e_5 << r_e_1; // we copy it from r_e_1
    vec_comp g_e_5; g_e_5 << g_e_3; // this is not modified
    vec_comp s_e_5; s_e_5 << s;
    fftw_complex* s_5 = s_e_5.v();
    for(unsigned int k=0; k<Nyquist_y; k++){
      double kappa = (2.0*pi*k)/L_y;
      double th_k = 3*kappa*kappa;
      s_5[k][0]*=th_k;
      s_5[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_y; k++){
      double kappa = -(2.0*pi*k)/L_y;
      double th_k = 3*kappa*kappa;
      s_5[N_y-k][0] *= th_k;
      s_5[N_y-k][1] *= th_k;
    }
    // appending the term:
    Phi_o.append(r_e_5, g_e_5, s_e_5);


    // 1 * (i*dt^3*k_z^3) * -k_y^3
    vec_comp r_e_6; r_e_6 << r; // we copy it from
    vec_comp g_e_6; g_e_6 << g_e_3; // this is not modified
    vec_comp s_e_6; s_e_6 << s;
    fftw_complex* s_6 = s_e_6.v();
    for(unsigned int k=0; k<Nyquist_y; k++){
      double kappa = (2.0*pi*k)/L_y;
      double th_k = -kappa*kappa*kappa;
      s_6[k][0]*=th_k;
      s_6[k][1]*=th_k;
    }
    for(unsigned int k=1; k<Nyquist_y; k++){
      double kappa = -(2.0*pi*k)/L_y;
      double th_k = -kappa*kappa*kappa;
      s_6[N_y-k][0] *= th_k;
      s_6[N_y-k][1] *= th_k;
    }
    // appending the term:
    Phi_o.append(r_e_6, g_e_6, s_e_6);
  }
  // End of function.
}



// Rounding procedure, simple ALS on 3 var.
// inputs: T, a tolerance tol, a relaxation parameter omega.
// output: the rounding of T.
void twoElectrons_CP::round_als(CP3var_comp& T, double tol, double omega=1.0){
  const unsigned int in_rank = T.rank();
  const unsigned int max_it_fp = 10;

  // initialising the tables:
  vector<vec_comp> R_out;
  vector<vec_comp> G_out;
  vector<vec_comp> S_out;

  // intialising the residual and start;
  CP3var_comp Residual; Residual << T;
  double res_norm = Residual.norm2CP();
  res_norm = sqrt(res_norm);
  cout << "Res norm = " << res_norm << endl;
  bool iterate = true;
  unsigned int cc = 0;
  while(iterate){
    // intialise the term:
    vec_comp v_r = T.R(cc);
    vec_comp v_g = T.G(cc);
    vec_comp v_s = T.S(cc);
    // copy the cc term as starting point.
    vec_comp r; r<<v_r;
    vec_comp g; g<<v_g;
    vec_comp s; s<<v_s;
    vec_comp r_old; r_old << r;
    vec_comp g_old; g_old << g;
    vec_comp s_old; s_old << s;

    // initialising the norms of the vectors:
    double norm_r = r.norm();
    double norm_g = g.norm();
    double norm_s = s.norm();

    // fixed-point.
    bool it_fp=true;
    unsigned int it = 0;
    while(it_fp){
        // update r:
        r *= (1.0-omega);
        for(unsigned int i_term=0; i_term < in_rank; i_term++){
          vec_comp r_term = T.R(i_term);
          vec_comp g_term = T.G(i_term);
          vec_comp s_term = T.S(i_term);
          double alpha_i = dot(g,g_term)/(norm_g*norm_g);
          alpha_i *= dot(s,s_term)/(norm_s*norm_s);
          alpha_i *= omega;
          vec_comp toAdd; toAdd << r_term;
          toAdd *= alpha_i;
          r += toAdd;
        }
        // update norm_r:
        norm_r = r.norm();

        // update g:
        g *= (1.0-omega);
        for(unsigned int i_term=0; i_term < in_rank; i_term++){
          vec_comp r_term = T.R(i_term);
          vec_comp g_term = T.G(i_term);
          vec_comp s_term = T.S(i_term);
          double alpha_i = dot(r,r_term)/(norm_r*norm_r);
          alpha_i *= dot(s,s_term)/(norm_s*norm_s);
          alpha_i *= omega;
          vec_comp toAdd; toAdd << g_term;
          toAdd *= alpha_i;
          g += toAdd;
        }
        // update norm_g:
        norm_g = g.norm();

        // update s:
        s *= (1.0-omega);
        for(unsigned int i_term=0; i_term < in_rank; i_term++){
          vec_comp r_term = T.R(i_term);
          vec_comp g_term = T.G(i_term);
          vec_comp s_term = T.S(i_term);
          double alpha_i = dot(g,g_term)/(norm_g*norm_g);
          alpha_i *= dot(r,r_term)/(norm_r*norm_r);
          alpha_i *= omega;
          vec_comp toAdd; toAdd << s_term;
          toAdd *= alpha_i;
          s += toAdd;
        }
        // update norm_s:
        norm_s = s.norm();

        // check fixed-point iteration:
        vec_comp diff_r; diff_r << r;
        diff_r -= r_old;
        vec_comp diff_g; diff_g << g;
        diff_g -= g_old;
        vec_comp diff_s; diff_s << s;
        diff_s -= s_old;

        double norm_diff_r = diff_r.norm();
        double norm_diff_g = diff_g.norm();
        double norm_diff_s = diff_s.norm();

        double criterion = norm_diff_r*norm_diff_r;
        criterion += norm_diff_g*norm_diff_g;
        criterion += norm_diff_s*norm_diff_s;
        criterion = sqrt(criterion);

        if( (criterion<0.01*tol) || (it==max_it_fp) ){
            it_fp = false;
        }
        it += 1;
    }
    R_out.push_back(r);
    G_out.push_back(g);
    S_out.push_back(s);

    Residual.append(r,g,s);
    res_norm = Residual.norm2CP();
    res_norm = sqrt(res_norm);
    cout << "Res norm = " << res_norm << endl;

    cc += 1;
    // check if to iterate or not.
    if(res_norm<tol){
      iterate = false;
      T.clear();
      T.set_terms(R_out, G_out, S_out);
    }
    if(cc == in_rank){
      iterate = false;
      // in this case keep the original tensor, compression was not efficient.
    }
  }
}



// SAVE the WAVE function:
// to be written:


// save a given CP:
void twoElectrons_CP::save_CP_vtk(string fileName, CP3var_comp& Psi){
  const unsigned int nDof = m_Ndof[0] * m_Ndof[0];

  //cout << "N = " << nDof << endl;

  string saveName = fileName + ".vtk";
  unsigned int nx = m_Ndof[0];
  unsigned int ny = m_Ndof[0];
  unsigned int nz = 1;
  unsigned int numOfVal = nx*ny*nz;

  ofstream outfile (saveName.c_str());

  outfile << "# vtk DataFile Version 2.0 " <<endl;
  outfile << "Data animation" << endl;
  outfile << "ASCII" <<endl;
  outfile << "DATASET STRUCTURED_POINTS" <<endl;
  outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
  outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
  outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
  outfile << "POINT_DATA " << numOfVal << endl;
  outfile << "VECTORS wave float" << endl;
  //outfile << "LOOKUP_TABLE default" <<endl;
  for(unsigned int i=0; i<m_Ndof[0]; i++){
    for(unsigned int j=0; j<m_Ndof[0]; j++){
      int k = i-j + (m_Ndof[0]-1)/2;
      if(k<0){
        k += m_Ndof[0];
      }
      if(k>=m_Ndof[0]){
        k -= m_Ndof[0];
      }

      double value_real = 0.0;
      double value_imag = 0.0;

      for(unsigned int iTerm=0; iTerm<Psi.rank(); iTerm++){
        value_real += Psi.R(iTerm).real(i) * Psi.G(iTerm).real(k) * Psi.S(iTerm).real(j);
        value_real -= Psi.R(iTerm).imag(i) * Psi.G(iTerm).imag(k) * Psi.S(iTerm).real(j);
        value_real -= Psi.R(iTerm).imag(i) * Psi.G(iTerm).real(k) * Psi.S(iTerm).imag(j);
        value_real -= Psi.R(iTerm).real(i) * Psi.G(iTerm).imag(k) * Psi.S(iTerm).imag(j);

        value_imag -= Psi.R(iTerm).imag(i) * Psi.G(iTerm).imag(k) * Psi.S(iTerm).imag(j);
        value_imag += Psi.R(iTerm).real(i) * Psi.G(iTerm).real(k) * Psi.S(iTerm).imag(j);
        value_imag += Psi.R(iTerm).real(i) * Psi.G(iTerm).imag(k) * Psi.S(iTerm).real(j);
        value_imag += Psi.R(iTerm).imag(i) * Psi.G(iTerm).real(k) * Psi.S(iTerm).real(j);
      }

      if(fabs(value_real) < 1.0e-9){value_real = 0.0;};
      if(fabs(value_imag) < 1.0e-9){value_imag = 0.0;};
      outfile << value_real << " " << value_imag << " " << 0.0 << endl;
      //outfile << value_real  << endl;
    }
  }

  outfile.close();
}
