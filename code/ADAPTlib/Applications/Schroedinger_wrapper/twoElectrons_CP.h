// Class to solve the Schroedinger problem, two electrons.

#ifndef twoElectrons_CP_h
#define twoElectrons_CP_h

// Including headers:
#include "finiteDifferencesPer.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "pod.h"
#include "operatorTensor.h"
#include "CP3var.h"

using namespace std;





class twoElectrons_CP{
private:

  //domain size
  double m_L;

  unsigned int m_dim;
  vector<unsigned int> m_Ndof;    //Number of degrees of freedom per direction: has to be odd for 0 not to be included in the list of discretization points
  unsigned int m_ndof;

  finiteDifferencesPer m_testSpace;

  unsigned int m_nIt;
  double m_tFin;
  double m_dt;

  // Non-symmetrized operators

  // re = real even, ro = real odd, ie = imag even, io = imag odd
  CP3var_comp m_u_even, m_u_odd; // current solution
  CP3var_comp m_u_even_old, m_u_odd_old; // previous solution


public:
  twoElectrons_CP(){};
  twoElectrons_CP(double,unsigned int);
  twoElectrons_CP(double, unsigned int, vector<unsigned int>);
  ~twoElectrons_CP(){};


  // SETTERS:
  void setFiniteDifferences(finiteDifferencesPer&);

  // METHODS:
  void save_1d_wave_fun_vtk(string, unsigned int);
  void save_CP_vtk(string, CP3var_comp&);

  // -- DYNAMICS SOLVER: --
  // - physical variables:
  void update_kinetic_1d(CP3var_comp&, double, vector<double>);
  void update_potential_1d(CP3var_comp&, double);
  void update_coupling_1d(CP3var_comp&, CP3var_comp&, double, vector<double>);
  // - Fourier variables:
  void update_kinetic_1dF(CP3var_comp&, double, vector<double>);
  void update_potential_1dF(CP3var_comp&, double);
  void update_coupling_1dF(CP3var_comp&, CP3var_comp&, double, vector<double>);

  // -- Auxiliary functions: --
  void round_als(CP3var_comp&, double, double);
  // - physical variables:
  void diagonal_rescale_fftw_re(unsigned int, double, fftw_complex*&, double, double, vec_comp&);
  void diagonal_rescale_fftw_im(unsigned int, double, fftw_complex*&, double, double, vec_comp&);
  // - Fourier variables:
  



  // ACCESS functions:
  inline finiteDifferencesPer testSpace(){return m_testSpace;}
};

#endif
