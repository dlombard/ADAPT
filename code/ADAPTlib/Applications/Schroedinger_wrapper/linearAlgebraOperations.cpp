#include "linearAlgebraOperations.h"

// sum of two vecs:
vec_real  operator + (vec_real& u, vec_real& v){
  assert(u.size()==v.size());
  vec_real out(u.size());
  for(unsigned int iDof=0; iDof<u.size(); iDof++){
    double val = u(iDof) + v(iDof);
    out.setVecEl(iDof,val);
  }
  return out;
}


// difference of two vecs:
vec_real  operator - (vec_real& u, vec_real& v){
  assert(u.size()==v.size());
  vec_real out(u.size());
  for(unsigned int iDof=0; iDof<u.size(); iDof++){
    double val = u(iDof) - v(iDof);
    out.setVecEl(iDof,val);
  }
  return out;
}

// scalar product between two vecs:
double dot(vec_real& u, vec_real& v){
  assert(u.size()==v.size());
  double out = 0.0;
  for(unsigned int iDof=0; iDof<u.size(); iDof++){
    out += u(iDof)*v(iDof);
  }
  return out;
}

// linear combination of a vector<vec_real> with weights vector<double>
void lin_comb(vector<vec_real>& U, vector<double>& w, vec_real& out){
  assert(U.size()== w.size());
  const unsigned int nMod = U.size();
  out << U[0];
  out *= w[0];
  for(unsigned int iMod=1; iMod<nMod; iMod++){
    out.axpy(U[iMod],w[iMod]);
  }
}

// implementation into the overloaded operator *
vec_real operator * (vector<vec_real>& U, vector<double>& w){
  vec_real out;
  lin_comb(U, w, out);
  return out;
}


// I/O for real vecs:
void save(vec_real& toSave, string fname){
  ofstream file;
  file.open(fname.c_str());
  for(unsigned int iDof=0; iDof<toSave.size()-1; iDof++){
    file << toSave(iDof) << endl;
  }
  file << toSave(toSave.size()-1);
  file.close();
}

vec_real load(string fname){
  ifstream file;
  file.open(fname.c_str());
  double val;
  vector<double>* v; v = new vector<double>;
  while(!file.eof()){
    file >> val;
    (*v).push_back(val);
  }
  vec_real out;
  double* v_star = &(*v)[0];
  out << v_star;
  out.set_size(v->size());
  return out;
}



// Implementation of operations for complex vecs:
// sum of two vecs:
vec_comp  operator + (vec_comp& u, vec_comp& v){
  assert(u.size()==v.size());
  vec_comp out(u.size());
  for(unsigned int iDof=0; iDof<u.size(); iDof++){
    double val_r = u.real(iDof) + v.real(iDof);
    out.setVecReal(iDof,val_r);
    double val_i = u.imag(iDof) + v.imag(iDof);
    out.setVecImag(iDof,val_i);
  }
  return out;
}


// difference of two vecs:
vec_comp  operator - (vec_comp& u, vec_comp& v){
  assert(u.size()==v.size());
  vec_comp out(u.size());
  for(unsigned int iDof=0; iDof<u.size(); iDof++){
    double val_r = u.real(iDof) - v.real(iDof);
    out.setVecReal(iDof,val_r);
    double val_i = u.imag(iDof) - v.imag(iDof);
    out.setVecImag(iDof,val_i);
  }
  return out;
}

// scalar product between two vecs:
double dot(vec_comp& u, vec_comp& v){
  assert(u.size()==v.size());
  double out = 0.0;
  for(unsigned int iDof=0; iDof<u.size(); iDof++){
    out += u.real(iDof)*v.real(iDof) + u.imag(iDof)*v.imag(iDof);
  }
  return out;
}




// REAL MATRICES OPERATIONS:
mat_real operator + (mat_real& A, mat_real& B){
  mat_real out;
  out << A;
  out += B;
  return out;
}

mat_real operator - (mat_real& A, mat_real& B){
  mat_real out;
  out << A;
  out -= B;
  return out;
}

vec_real operator * (mat_real& A, vec_real& v){
  assert(v.size()==A.nCols());
  vec_real out(A.nRows());
  for(unsigned int iRow=0; iRow<A.nRows(); iRow++){
    double entry = 0.0;
    for(unsigned int iCol=0; iCol<A.nCols(); iCol++){
      entry += A(iRow,iCol) * v(iCol);
    }
    out.setVecEl(iRow, entry);
  }
  return out;
}

// w = A^T v
vec_real dot_T(mat_real& A, vec_real& v){
  assert(v.size()==A.nRows());
  vec_real out(A.nCols());
  for(unsigned int iCol=0; iCol<A.nCols(); iCol++){
    double entry = 0.0;
    for(unsigned int iRow=0; iRow<A.nRows(); iRow++){
      entry += A(iRow,iCol) * v(iRow);
    }
    out.setVecEl(iCol, entry);
  }
  return out;
}

// matrix matrix product:
mat_real operator * (mat_real& A, mat_real& B){
  assert(A.nCols()==B.nRows());
  mat_real out(A.nRows(),B.nCols());
  for(unsigned int iRow=0; iRow<A.nRows(); iRow++){
    for(unsigned int iCol=0; iCol<B.nCols(); iCol++){
      double entry = 0.0;
      for(unsigned int k=0; k<A.nCols(); k++){
        entry += A(iRow,k) * B(k,iCol);
      }
      out.setMatEl(iRow, iCol, entry);
    }
  }
  return out;
}

// A^T B without computing nor storing the transposed.
mat_real dot_T(mat_real& A, mat_real& B){
  assert(A.nRows()==B.nRows());
  mat_real out(A.nCols(),B.nCols());
  for(unsigned int iRow=0; iRow<A.nCols(); iRow++){
    for(unsigned int iCol=0; iCol<B.nCols(); iCol++){
      double entry = 0.0;
      for(unsigned int k=0; k<A.nCols(); k++){
        entry += A(k, iRow) * B(k,iCol);
      }
      out.setMatEl(iRow, iCol, entry);
    }
  }
  return out;
}


// solve linear systems, dense, LAPACK routine.
extern "C" {
  extern int dgesv_(int*, int*, double*, int*, int*, double*, int*, int*);
}

// solve linear system, LU partial pivoting, over-writing matrix and rhs
void solve_linear_fast(mat_real& matSys, vec_real& b){
  int n_mat = matSys.nRows();
  int n_rhs = 1;
  int lda = n_mat;
  int* piv; piv = new int[n_mat];
  int ldb = n_mat;
  int info;

  int err = dgesv_(&n_mat, &n_rhs, matSys.M(), &lda, piv, b.v(), &ldb, &info);
  delete[] piv;
}

// solve linear system, copy the matrix and the right-hand side:
void solve_linear(mat_real& matSys, vec_real& b, vec_real& sol){
  int n_mat = matSys.nRows();
  int n_rhs = 1;
  int lda = n_mat;
  int* piv; piv = new int[n_mat];
  int ldb = n_mat;
  int info;

  // copy the matrix:
  mat_real tmp_A; tmp_A << matSys;
  // copy the right-hand side:
  sol << b;
  // solve:
  int err = dgesv_(&n_mat, &n_rhs, tmp_A.M(), &lda, piv, sol.v(), &ldb, &info);

  // tmp_A is destroyed by destructor call.
  delete[] piv;
}

// solve linear system, operator: matrix is over-written, vector no:
vec_real operator / (mat_real& matSys, vec_real& b){
  vec_real sol; sol << b;
  int n_mat = matSys.nRows();
  int n_rhs = 1;
  int lda = n_mat;
  int* piv; piv = new int[n_mat];
  int ldb = n_mat;
  int info;

  // solve:
  int err = dgesv_(&n_mat, &n_rhs, matSys.M(), &lda, piv, sol.v(), &ldb, &info);

  return sol;
}


// REAL sparse matrices:

// matrix-vector product:
vec_real operator * (mat_real_sp& A, vec_real& v){
  assert(A.nCols()==v.size());
  vec_real out(A.nRows());
  for(unsigned int iRow=0; iRow<A.nRows(); iRow++){
    double entry = 0.0;
    for(unsigned int iEl=0; iEl<A.row_pattern(iRow).size(); iEl++){
      unsigned int iCol = A.row_pattern(iRow, iEl);
      entry += A.M(iRow,iEl) * v(iCol); // A.M returns m_M[iRow][iEl] == A(iRow,iCol) !!
    }
    out.setVecEl(iRow, entry);
  }
  return out;
}
