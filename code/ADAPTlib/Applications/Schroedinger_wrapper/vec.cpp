// implementation of the wrapper vec with fftw types:
#include "vec.h"


// Overloaded Constructors for real vectors:
vec_real::vec_real(unsigned int N){
  m_size = N;
  m_v = (double*) fftw_malloc(sizeof(double)*m_size);
}

vec_real::vec_real(double*& v, unsigned int){
  m_size = sizeof(*v)/sizeof(double);
  m_v = v;
}


// when the pointer is initialised elsewhere, and need to allocate.
void vec_real::init(unsigned int N){
  m_size = N;
  m_v = (double*) fftw_malloc(sizeof(double)*m_size);
}


// Methods:

// copyVecFrom:
void vec_real::copyVecFrom(vec_real& toBeCopied){
  unsigned int N = toBeCopied.size();
  m_size = N;
  m_v = (double*) fftw_malloc(sizeof(double)*m_size);
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof] = toBeCopied(iDof);
  }
}

// cast without copies: operator << overloaded
void vec_real::operator << (double*& v){
  m_size = sizeof(*v)/sizeof(double);
  m_v = v;
}

// += operator
void vec_real::operator += (vec_real& toBeAdded){
  unsigned int N = toBeAdded.size();
  assert(N == m_size);
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof] += toBeAdded(iDof);
  }
}

// -= operator
void vec_real::operator -= (vec_real& toBeAdded){
  unsigned int N = toBeAdded.size();
  assert(N == m_size);
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof] -= toBeAdded(iDof);
  }
}

// *= operator
void vec_real::operator *= (double alpha){
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof] *= alpha;
  }
}


// norm and norm squared:
double vec_real::norm(){
  double out = 0.0;
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    out += m_v[iDof] * m_v[iDof];
  }
  return sqrt(out);
}

double vec_real::norm_squared(){
  double out = 0.0;
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    out += m_v[iDof] * m_v[iDof];
  }
  return out;
}


// function axpy: this <-- this + alpha*x
void vec_real::axpy(vec_real& v, double alpha){
  assert(m_size==v.size());
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof] += alpha*v(iDof);
  }
}








// --- COMPLEX VECTORS: ---


// Overloaded Constructor for Complex vectors:
vec_comp::vec_comp(unsigned int N){
  m_size = N;
  m_v = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*m_size);
}

vec_comp::vec_comp(fftw_complex*& v, unsigned int N){
  m_size = N;
  m_v = v;
}


// when the pointer is initialised elsewhere, and need to allocate.
void vec_comp::init(unsigned int N){
  m_size = N;
  m_v = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*m_size);
}

void vec_comp::init(fftw_complex*& v, unsigned int N){
  m_size = N;
  m_v = v;
}


// Methods:

// copyVecFrom:
void vec_comp::copyVecFrom(vec_comp& toBeCopied){
  unsigned int N = toBeCopied.size();
  m_size = N;
  m_v = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*m_size);
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof][0] = toBeCopied.real(iDof);
    m_v[iDof][1] = toBeCopied.imag(iDof);
  }
}

// cast via operator << given a fftw_complex*
void vec_comp::operator << (fftw_complex*& v){
  m_v = v;
}

// compute the complex conjugate:
void vec_comp::conj(){
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof][1] = -m_v[iDof][1];
  }
}

// += operator
void vec_comp::operator += (vec_comp& toBeAdded){
  unsigned int N = toBeAdded.size();
  assert(N == m_size);
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof][0] += toBeAdded.real(iDof);
    m_v[iDof][1] += toBeAdded.imag(iDof);
  }
}

// -= operator
void vec_comp::operator -= (vec_comp& toBeAdded){
  unsigned int N = toBeAdded.size();
  assert(N == m_size);
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof][0] -= toBeAdded.real(iDof);
    m_v[iDof][1] -= toBeAdded.imag(iDof);
  }
}

// *= operator
void vec_comp::operator *= (double alpha){
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof][0] *= alpha;
    m_v[iDof][1] *= alpha;
  }
}

// *= operator, overloaded to perform the multiplication with a complex number
void vec_comp::operator *= (fftw_complex* alpha){
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof][0] = m_v[iDof][0]*alpha[0][0] - m_v[iDof][1]*alpha[0][1];
    m_v[iDof][1] = m_v[iDof][1]*alpha[0][0] + m_v[iDof][0]*alpha[0][1];
  }
}

// norm and norm squared:
double vec_comp::norm(){
  double out = 0.0;
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    out += m_v[iDof][0] * m_v[iDof][0] + m_v[iDof][1] * m_v[iDof][1];
  }
  return sqrt(out);
}

double vec_comp::norm_squared(){
  double out = 0.0;
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    out += m_v[iDof][0] * m_v[iDof][0] + m_v[iDof][1] * m_v[iDof][1];
  }
  return out;
}


// axpy function:
void vec_comp::axpy(vec_comp& v, fftw_complex* alpha){
  assert(m_size==v.size());
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof][0] += alpha[0][0]*v.real(iDof) - alpha[0][1]*v.imag(iDof);
    m_v[iDof][1] += alpha[0][1]*v.real(iDof) + alpha[0][0]*v.imag(iDof);
  }
}

// overloaded, in the case in which alpha is just a double (not to do a cast):
void vec_comp::axpy(vec_comp& v, double alpha){
  assert(m_size==v.size());
  for(unsigned int iDof=0; iDof<m_size; iDof++){
    m_v[iDof][0] += alpha*v.real(iDof);
    m_v[iDof][1] += alpha*v.imag(iDof);
  }
}
