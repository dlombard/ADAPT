// Parallel tensor implementation
#include "genericInclude.h"
// Include linear algebra headers:
#include "linAlg/linAlg.h"
// Include discretisation:
#include "Discretisations/Discretisations.h"

//Include Network
#include "Applications/Network/Network.h"
#include "Applications/Network/locHOSVD.h"
using namespace std;

#include <mpi.h>

//Function to save the modes
void saveField_2d(vec& sol, finiteDifferences& space, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = space.struc().nDof_var(0);
 unsigned int ny = space.struc().nDof_var(1);
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

  outfile << "# vtk DataFile Version 2.0 " <<endl;
  outfile << "Data animation" << endl;
  outfile << "ASCII" <<endl;
  outfile << "DATASET STRUCTURED_POINTS" <<endl;
  outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
  outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
  outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
  outfile << "POINT_DATA " << numOfVal << endl;
  outfile << "SCALARS Field float" << endl;
  outfile << "LOOKUP_TABLE default" <<endl;
 for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();

}


void saveField_2d_txt(vec& sol, unsigned int Nx, unsigned int Ny, string& fName){
 string saveName = fName + ".txt";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

  for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();
}



//Function to save the modes
void saveField_3d(vec& sol, unsigned int Nx, unsigned int Ny, unsigned int Nz, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = Nz;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;

 for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();

}




void saveField_3d_txt(vec& sol, unsigned int Nx, unsigned int Ny, unsigned int Nz, string& fName){
 string saveName = fName + ".txt";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = Nz;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

  for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();
}



/* Linspace function: */
vector<double> linspace(double start, double end, int num){

  vector<double> linspaced(num);


  if (num == 0) { return linspaced; }
  if (num == 1) {
      linspaced.push_back(start);
      return linspaced;
    }

  double delta = (end - start) / (num - 1.0);

  for(unsigned int i=0; i < num; ++i){
    linspaced[i] = start + delta * i;
  }
  //linspaced.push_back(end);  I want to ensure that start and end are exactly the same as the input
  return linspaced;
}


// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


    unsigned int begining = 0;
    unsigned int end = 200;
    unsigned int t = end-begining;
    unsigned int Dofsx = 5;
    unsigned int nThetaM = 5;
    unsigned int nThetaD = 5;
    unsigned int nThetaL = 5;
    unsigned int nThetaTau = 5;
    unsigned int nTheta = nThetaM*nThetaD*nThetaL*nThetaTau;

/*

    //UNFOLDINGS X, T*THETA
    vector<vector<double>*>u(t*nTheta);
    for (size_t i = 0; i < t*nTheta; i++) {
      u[i] = new vector<double>(Dofsx);
    }

    //Linear mapping t and theta
    unsigned int iTt;
    for (size_t iThM = 0; iThM < nThetaM; iThM++) {
      for (size_t iThD = 0; iThD < nThetaD; iThD++) {
        for (size_t iThL = 0; iThL < nThetaL; iThL++) {
          for (size_t iThTau = 0; iThTau < nThetaTau; iThTau++) {
            unsigned int iTh = iThTau + nThetaTau * iThL + nThetaTau * nThetaL * iThD + nThetaTau * nThetaL * nThetaD * iThM;
            string uName = string("Applications/EnvironmentalModel/EMu_x5_t200_5_5_5_5/u_")+to_string(iTh)+ string(".txt");
            //string uName = string("Applications/EnvironmentalModel/EMu_x5_t200_5_5_5_5/u_")+to_string(iThM+1)+ string("_") + to_string(iThD+1)+ string("_") + to_string(iThL+1)+ string("_")+to_string(iThTau+1)+ string(".txt");
            vec tmp = loadVecASCII(uName, PETSC_COMM_WORLD);
            for (size_t iT = 0; iT < t; iT++) {
              if (iT%1 == 0){
                if (iT%20 == 0){cout << uName << endl;}
                iTt = iT + (t * iTh);
                for (size_t iX = 0; iX < Dofsx; iX++) {
                  unsigned int loadInd = iT + (t * iX);
                  double val = tmp.getVecEl(loadInd);
                  (*u[iTt])[iX] = val;
                }
              }
            }
            tmp.clear();
          }
        }
      }
    } */



    //UNFOLDINGS T, X*THETA
    //lowmem
    vector<vector<double>*>u(Dofsx*nTheta);
    for (size_t i = 0; i < Dofsx*nTheta; i++) {
      u[i] = new vector<double>(t);
    }

    //Linear mapping x and theta
    unsigned int iXt;
    for (size_t iThM = 0; iThM < nThetaM; iThM++) {
      for (size_t iThD = 0; iThD < nThetaD; iThD++) {
        for (size_t iThL = 0; iThL < nThetaL; iThL++) {
          for (size_t iThTau = 0; iThTau < nThetaTau; iThTau++) {
            unsigned int iTh = iThTau + nThetaTau * iThL + nThetaTau * nThetaL * iThD + nThetaTau * nThetaL * nThetaD * iThM;
            string uName = string("Applications/EnvironmentalModel/EMu_x5_t200_5_5_5_5/u_")+to_string(iTh)+ string(".txt");
            vec uload = loadVecASCII(uName, PETSC_COMM_WORLD);
            for (size_t iT = 0; iT < t; iT++) {
              if (iT%1 == 0){
                if (iT%20 == 0){cout << uName << endl;}
                for (size_t iX = 0; iX < Dofsx; iX++) {
                  unsigned int loadInd = iT + (t * iX);
                  double val= uload.getVecEl(loadInd);
                  iXt = iX + (Dofsx * iTh);
                  (*u[iXt])[iT] = val;
                }
              }
            }
            uload.clear();
          }
        }
      }
    }


/*
    //UNFOLDINGS THETA, X*T
    vector<vector<double>*> u(Dofsx*t);
    for (size_t i = 0; i < Dofsx*t; i++) {
      u[i] = new vector<double>(nTheta);
    }

    //Linear mapping x and t
    unsigned int iXT;
    for (size_t iThM = 0; iThM < nThetaM; iThM++) {
      for (size_t iThD = 0; iThD < nThetaD; iThD++) {
        for (size_t iThL = 0; iThL < nThetaL; iThL++) {
          for (size_t iThTau = 0; iThTau < nThetaTau; iThTau++) {
            unsigned int iTh = iThTau + nThetaTau * iThL + nThetaTau * nThetaL * iThD + nThetaTau * nThetaL * nThetaD * iThM;
            string uName = string("Applications/EnvironmentalModel/EMu_x5_t200_5_5_5_5/u_")+to_string(iTh)+ string(".txt");
            vec uload = loadVecASCII(uName, PETSC_COMM_WORLD);
            for (size_t cc = 0; cc < t*Dofsx; cc++) {
              (*u[cc])[iTh] = uload.getVecEl(cc);
            }
            uload.clear();
          }
        }
      }
    }

*/



    //Clustering method

        vector<mat> Basis;
        vector<vector<unsigned int>> indOnBasis;
        vector<double> locErrBudget;
        double globalBudget;
        Network net;
        double normalization = t * Dofsx * nTheta;
        double error = 1.0e-2 * sqrt(normalization);

        //Obtaining the basis with respect to the x
        net.lowMem_ComputeBases(u, error, Basis, indOnBasis, locErrBudget, globalBudget);
        cout << "Basis computed" << endl;
        if (Basis.size() != 1) {
          net.lowMem_Merge2dBases(u, Basis, indOnBasis, locErrBudget, globalBudget);
        }
        cout << "Basis merged" << endl;

        unsigned int total_memory = 0;
        for (size_t iGroup = 0; iGroup < indOnBasis.size(); iGroup++) {
          total_memory += t * Basis[iGroup].nCols() + Basis[iGroup].nCols() * indOnBasis[iGroup].size();
        }
        cout << "Total memory = " << total_memory << " in comparison with the full tensor memory : " << Dofsx*t*nTheta << endl;
        double compression_ratio_TD = (total_memory * 1.0)/(Dofsx*t*nTheta);

        double error_sum = 0.0;


        cout << "start saving" << endl;

        ofstream outfile;
        for (size_t iBas = 0; iBas < Basis.size(); iBas++) {
          string outFileName = string("Applications/EnvironmentalModel/Base_t_")+ to_string(iBas) + string(".txt") ;
          outfile.open(outFileName.c_str());
          for (size_t iVec = 0; iVec < Basis[iBas].nCols(); iVec++) {
             for (size_t iEl = 0; iEl < Basis[iBas].nRows(); iEl++) {
               outfile << Basis[iBas](iEl,iVec) << flush << endl;
             }
          }
          outfile.close();
        }


        for (size_t iBas = 0; iBas < indOnBasis.size(); iBas++) {
          string outFileName = string("Applications/EnvironmentalModel/indOnBasis_t_")+ to_string(iBas) + string(".txt") ;
          outfile.open(outFileName.c_str());
           for (size_t iFib = 0; iFib < indOnBasis[iBas].size(); iFib++) {
             outfile << indOnBasis[iBas][iFib] << flush << endl;
           }
          outfile.close();
        }

        for (size_t iBas = 0; iBas < locErrBudget.size(); iBas++) {
          string outFileName = string("Applications/EnvironmentalModel/locErrorBudget_t_")+ to_string(iBas) + string(".txt") ;
          outfile.open(outFileName.c_str());
          outfile << locErrBudget[iBas] << flush << endl;
          outfile.close();
        }

        for (size_t iBas = 0; iBas < Basis.size(); iBas++) {
          Basis[iBas].clear();
        }

SlepcFinalize();
return 0;
}
