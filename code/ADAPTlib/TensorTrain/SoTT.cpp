// implementation of the class SoTT: Sum of Tensor Trains.

#include "SoTT.h"

using namespace std;

// I -- Overloaded Constructors:

/* I.1 - given dof per dimension and communicator, initialise the tensor.
  - inputs: the number of dofs per direction, the communicator
  - output: tensor fields are filled.
*/
SoTT::SoTT(vector<unsigned int> resPerDim, MPI_Comm theComm){
  m_nVar = resPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = resPerDim[iVar];
  }
  compute_minc();
  m_comm = theComm;
}

/* I.2 - given dimension, dof per dimension and communicator, initialise the tensor.
  - inputs: the number of dofs per direction, the communicator
  - output: tensor fields are filled.
*/
SoTT::SoTT(unsigned int dim, vector<unsigned int> resPerDim, MPI_Comm theComm){
  m_nVar = dim;
  assert(m_nVar == resPerDim.size());
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = resPerDim[iVar];
  }
  compute_minc();
  m_comm = theComm;
}


// II - METHODS TO COMPUTE SoTT:



/* II.0 choose the best unfolding:
  - input: a vector of svd
  - output: the i of the best unfolding, the rank;
*/
void SoTT::chooseUnfolding(vector<svd>& svdUnfold, unsigned int& iBest, unsigned int& rank){
  // best unfolding: the one with the largest sigma
  /*const unsigned int dim = svdUnfold.size();
  iBest = 0;
  double sigma_best = svdUnfold[0].Svec().getVecEl(0);
  for(unsigned int iVar=1; iVar<dim; iVar++){
    if(svdUnfold[iVar].Svec().getVecEl(0) > sigma_best){
      sigma_best = svdUnfold[iVar].Svec().getVecEl(0);
      iBest = iVar;
    }
  }
  cout << "The best unfolding is " << iBest << endl;

  // Choose the rank:
  rank = estimateRank(svdUnfold, iBest);
  cout << "The rank is " << rank << endl;*/

  // Implementation of the L-criterion:
  const unsigned int dim = svdUnfold.size();
  if(dim>=2){
    vector<double> Lvec(dim);
    vector<unsigned int> bestRank(dim);
    for(unsigned int iVar=0; iVar<dim; iVar++){
      // estimate the unfolding "temperature":
      double beta = 0.0;
      const unsigned int nOfSigma = svdUnfold[iVar].Svec().size();
      for(unsigned int iS=0; iS<nOfSigma; iS++){
        double value = svdUnfold[iVar].Svec().getVecEl(iS) * svdUnfold[iVar].Svec().getVecEl(iS);
        beta += value;
      }
      // we could either consider the effective rank beta/nOfSigma or the maximal possible rank
      unsigned int maxRank = svdUnfold[iVar].Umat().nRows();
      if(svdUnfold[iVar].Vmat().nRows()<=maxRank){maxRank = svdUnfold[iVar].Vmat().nRows();}
      //beta = beta/nOfSigma;
      beta = beta/maxRank;

      //cout << "Beta = " << beta << endl;
      double L = svdUnfold[iVar].Svec().getVecEl(0)*svdUnfold[iVar].Svec().getVecEl(0) - beta;
      //cout << "L = " << L << endl;
      unsigned int iS = 1;
      double L_old = 0.0;
      while(L>L_old){
        L_old = L;
        L += svdUnfold[iVar].Svec().getVecEl(iS)*svdUnfold[iVar].Svec().getVecEl(iS);
        L -= beta;
        //cout << "L = " << L << endl;
        iS+=1;
      }
      Lvec[iVar] = L;
      //cout << "L best  = " << L << endl << endl;
      bestRank[iVar] = iS-1;
    }

    // determining the best unfolding and the rank:
    iBest = 0;
    rank = bestRank[0];
    double Lbest = Lvec[0];
    for(unsigned int iVar=1; iVar<dim; iVar++){
      if(Lvec[iVar]>Lvec[0]){
        iBest = iVar;
        Lbest = Lvec[iVar];
        rank = bestRank[iVar];
      }
    }
    cout << "The best unfolding is: " << iBest << ", the rank is: " << rank << "  value of L = " << Lbest << endl;

  }
  else{
    // just estimate the rank:
    // estimate the unfolding "temperature":
    double beta = 0.0;
    const unsigned int nOfSigma = svdUnfold[0].Svec().size();
    for(unsigned int iS=0; iS<nOfSigma; iS++){
      double value = svdUnfold[0].Svec().getVecEl(iS) * svdUnfold[0].Svec().getVecEl(iS);
      beta += value;
    }
    unsigned int maxRank = svdUnfold[0].Umat().nRows();
    if(svdUnfold[0].Vmat().nRows()<=maxRank){maxRank = svdUnfold[0].Vmat().nRows();}
    //beta = beta/nOfSigma;
    beta = beta/maxRank;
    //cout << "Beta = " << beta << endl;
    double L = svdUnfold[0].Svec().getVecEl(0)*svdUnfold[0].Svec().getVecEl(0);
    unsigned int iS = 1;
    double L_old = 0.0;
    while(L>L_old){
      L_old = L;
      L += svdUnfold[0].Svec().getVecEl(iS)*svdUnfold[0].Svec().getVecEl(iS);
      L -= beta;
      //cout << "L = " << L <<endl;
      iS+=1;
    }
    iBest= 0;
    rank = iS-1;
    cout << "The best unfolding is: " << iBest << " and the rank is: " << rank << endl;
  }
}

// estimate the rank: these functions can implement different criteria.
unsigned int SoTT::estimateRank(vector<svd>& svdUnf, unsigned int iBest){
  unsigned int rank = 1;
  const unsigned int dim = svdUnf.size();

  /*bool isItLarger = true;
  while(isItLarger){
    unsigned int sigma_best = svdUnf[iBest].Svec().getVecEl(rank);
    for(unsigned int iVar=0; iVar<dim; iVar++){
      if(svdUnf[iVar].Svec().getVecEl(rank) > sigma_best){
        isItLarger = false;
        break;
      }
    }
    if(isItLarger){
      rank += 1;
    }
  }*/
  return rank;
}


/* II.0.b compute L criterion for one single unfolding
  - input: the svd of the unfolding
  - output: the value of L and i the rank;
*/
/*void SoTT::compute_L(svd& unfSvd, double& L, unsigned int& rank){
  // estimate the unfolding "temperature":
  double beta = 0.0;
  const unsigned int nOfSigma = unfSvd.Svec().size();
  for(unsigned int iS=0; iS<nOfSigma; iS++){
    double value = unfSvd.Svec().getVecEl(iS) * unfSvd.Svec().getVecEl(iS);
    beta += value;
  }
  unsigned int maxRank = unfSvd.Umod(0).size();
  if(unfSvd.Vmod(0).size()<=maxRank){maxRank = unfSvd.Vmod(0).size();}
  //beta = beta - (m_tol*m_tol)/(m_nVar-1);
  beta = beta/(maxRank);
  //beta = beta/( unfSvd.Vmod(0).size()*unfSvd.Umod(0).size() );

  // compute L, determine its maximum.
  L = unfSvd.Svec().getVecEl(0)*unfSvd.Svec().getVecEl(0) - beta;
  unsigned int iS = 1;
  double L_old = 0.0;
  while(L>L_old){
    L_old = L;
    L += unfSvd.Svec().getVecEl(iS)*unfSvd.Svec().getVecEl(iS);
    L -= beta;
    iS+=1;
  }
  rank = iS-1;
}*/

void SoTT::compute_L(svd& unfSvd, double& L, unsigned int& rank){
  //const unsigned int previous_rank = rank;
  // estimate the unfolding "temperature":
  double beta = 0.0;
  const unsigned int nOfSigma = unfSvd.Svec().size();
  /*for(unsigned int iS=0; iS<nOfSigma-1; iS++){
    double delta_energy = unfSvd.Svec().getVecEl(iS) * unfSvd.Svec().getVecEl(iS);
    delta_energy -= unfSvd.Svec().getVecEl(iS+1) * unfSvd.Svec().getVecEl(iS+1);
    beta += delta_energy;
  }
  beta = beta / (nOfSigma-1);*/
  for(unsigned int iS=0; iS<nOfSigma; iS++){
    double value = unfSvd.Svec().getVecEl(iS) * unfSvd.Svec().getVecEl(iS);
    beta += value;
  }
  //beta = beta/(maxRank);
  const double comp_factor = 1.0*( unfSvd.Vmod(0).size() + unfSvd.Umod(0).size() )/( 1.0*unfSvd.Vmod(0).size()*unfSvd.Umod(0).size() );
  unsigned int maxSize = unfSvd.Vmod(0).size();
  if(unfSvd.Umod(0).size() > unfSvd.Vmod(0).size() ){maxSize = unfSvd.Umod(0).size();}
  //const double comp_factor = (1.0*maxSize)/( 1.0*unfSvd.Vmod(0).size()*unfSvd.Umod(0).size() );
  //beta = beta/( 1.0*unfSvd.Vmod(0).size()*unfSvd.Umod(0).size() );
  beta = beta * comp_factor;

  // compute L, determine its maximum.
  //L = unfSvd.Svec().getVecEl(0)*unfSvd.Svec().getVecEl(0) - beta;
  L = 0.0;
  //unsigned int iS = 1;
  unsigned int iS = 0;
  double L_old = -1.0;
  unsigned int countSigma = 1;
  while( (L>L_old) && (countSigma<nOfSigma)){
    L_old = L;

    //beta = unfSvd.Svec().getVecEl(iS)*unfSvd.Svec().getVecEl(iS) ;
    L = 0.0;
    for(unsigned int jS=0; jS<=iS; jS++){
      L += unfSvd.Svec().getVecEl(jS)*unfSvd.Svec().getVecEl(jS);
    }
    L -= beta * (iS+1);

    iS+=1;
    countSigma+=1;
  }
  rank = iS-1;
  cout << rank << endl;

  // limit rank:
  //const double epsSquared = m_tol*m_tol/( (1 + m_nTerms) * (m_nVar-1) );
  const double epsSquared = m_tol*m_tol/( (m_nVar-1) );
  unsigned int limit_rank = nOfSigma;
  double sum = 0.0;
  iS = nOfSigma-1;
  while( sum < epsSquared ){
    sum += unfSvd.Svec().getVecEl(iS)*unfSvd.Svec().getVecEl(iS);
    iS = iS-1;
  }
  limit_rank = iS+2;
  //limit_rank = 1; // CPTT
  if(limit_rank<=0){ limit_rank = 1; }
  if(limit_rank>=nOfSigma) {limit_rank = nOfSigma; }
  cout << limit_rank << endl;
  if(limit_rank < rank){
    rank = limit_rank;
    // compute the value of L:
    /*L = 0.0;
    //beta = 0.0; // CP-TT
    for(unsigned int iS=0; iS<rank; iS++){
      double value = unfSvd.Svec().getVecEl(iS) * unfSvd.Svec().getVecEl(iS);
      L += value;
    }
    L = L - beta * rank;*/
  }
}

// overloaded for fast version:
void SoTT::compute_L(vector<double>& S, unsigned int& uSize, unsigned int& vSize, double& L, unsigned int& rank){
  // estimate the unfolding "temperature":
  double beta = 0.0;
  const unsigned int nOfSigma = S.size();

  for(unsigned int iS=0; iS<nOfSigma; iS++){
    double value = S[iS] * S[iS];
    beta += value;
  }

  const double comp_factor = 1.0*( vSize + uSize )/( 1.0*vSize*uSize );
  unsigned int maxSize = vSize;
  if(uSize > vSize ){maxSize = uSize;}
  //const double comp_factor = (1.0*maxSize)/( 1.0*unfSvd.Vmod(0).size()*unfSvd.Umod(0).size() );
  //beta = beta/( 1.0*unfSvd.Vmod(0).size()*unfSvd.Umod(0).size() );
  beta = beta * comp_factor;

  // compute L, determine its maximum.
  L = 0.0;
  //unsigned int iS = 1;
  unsigned int iS = 0;
  double L_old = -1.0;
  unsigned int countSigma = 1;
  while( (L>L_old) && (countSigma<=nOfSigma)){
    L_old = L;

    //beta = unfSvd.Svec().getVecEl(iS)*unfSvd.Svec().getVecEl(iS) ;
    L = 0.0;
    for(unsigned int jS=0; jS<=iS; jS++){
      L += S[jS]*S[jS];
    }
    L -= beta * (iS+1);

    iS+=1;
    countSigma+=1;
  }
  rank = iS; // or iS-1, depending on the approximation.

  if(rank ==0){rank =1;}

  cout << rank << endl;

  // limit rank:
  //const double epsSquared = m_tol*m_tol/( (1 + m_nTerms) * (m_nVar-1) );
  const double epsSquared = m_tol*m_tol/( (m_nVar-1) );
  unsigned int limit_rank = nOfSigma;
  double sum = 0.0;
  iS = nOfSigma-1;
  while( sum < epsSquared ){
    sum += S[iS]*S[iS];
    iS = iS-1;
  }
  limit_rank = iS+2;
  //limit_rank = 1; // CPTT
  if(limit_rank<=0){ limit_rank = 1; }
  if(limit_rank>=nOfSigma) {limit_rank = nOfSigma; }
  cout << limit_rank << endl;

  //rank = limit_rank;
  if(limit_rank < rank){
    rank = limit_rank;
    // compute the value of L: CP-TT option
    /*L = 0.0;
    beta = 0.0; // CP-TT
    for(unsigned int iS=0; iS<rank; iS++){
      double value = S[iS] * S[iS];
      L += value;
    }
    L = L - beta * rank;*/
  }
}




// -- Methods related to a CP tensor input: -- //

/* II.0 -- function to compute SoTT starting from CP --
  - input: a CP tensor
  - output: compute the SoTT approximation of it
*/
void SoTT::CP2SoTT(CPTensor& residual, double tolerance){
  ofstream outfile;
  if(!m_saveName.empty()){
    outfile.open(m_saveName.c_str());
  }

  m_nTerms = 0;
  m_tol = tolerance;
  double normRes = sqrt(residual.norm2CP());

  unsigned int cc = 0;

  if(!m_saveName.empty()){
    outfile << normRes << flush << endl;
  }

  while(normRes>tolerance){
    //compute_TT(residual);
    compute_TT_fast(residual);
    cout << endl;
    cout << "---------------------------------" << endl;
    normRes = update_residual(residual,cc);
    unsigned int mem = memory();
    cout << "Actual memory of the " << m_nTerms << " terms: " << mem << endl;
    cout << "---------------------------------" << endl << endl;

    if(!m_saveName.empty()){
      outfile << normRes << flush << endl;
    }

    cc += 1;
  }

  if(!m_saveName.empty()){
    outfile.close();
  }
}


/* II.1 -- Compute a TT term given a CP
  - input: a CP tensor
  - output: compute a TT term and put it into m_terms
*/
void SoTT::compute_TT(CPTensor& residual){
  assert(residual.nVar()==m_nVar);


  // 1 - declare the train:
  vector< vector< vector<vec> > > fibreTrain(m_nVar);
  vector<unsigned int> order_of_variables(m_nVar);

  // start by computing the Svd of the unfoldings:
  vector<svd> svdUnfold(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    mat unf;
    residual.computeUnfolding(iVar, unf);
    svdUnfold[iVar].init(unf);
    unf.clear();
    //svdUnfold[iVar].Svec().print();
  }

  // Choose the best unfolding:
  unsigned int iBest = 0;
  unsigned int rank = 1;
  chooseUnfolding(svdUnfold, iBest, rank);
  order_of_variables[0] = iBest;

  // definition of variable set:
  vector<unsigned int> varSet(m_nVar);
  varSet[0] = iBest;                 //iBest won't change throughout the process
  unsigned int countVar = 1;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != iBest){
      varSet[countVar] = iVar;
      countVar += 1;
    }
  }

  // extract the TT modes and put them in the train:
  vector<vec> modes(rank);
  for(unsigned int iMod=0; iMod<rank; iMod++){
    modes[iMod].init(m_nDof_var[iBest], m_comm);
    for(unsigned int iDof=0; iDof<m_nDof_var[iBest]; iDof++){
      double value = svdUnfold[iBest].Umat().getMatEl(iDof,iMod);
      if(fabs(value) > DBL_EPSILON){
        modes[iMod].setVecEl(iDof,value);
      }
    }
    modes[iMod].finalize();
    //modes[iMod].print();
  }
  // the first variable has size 1 x rank
  vector<vector<vec> > firstVarModes(1);
  firstVarModes[0] = modes;
  fibreTrain[0] = firstVarModes;

  //free the memory:
  for(unsigned int iVar=0;iVar<m_nVar; iVar++){
    svdUnfold[iVar].clear();
  }
  svdUnfold.clear();

  // copy the residual into a temporary object, not to modify the original tensor.
  CPTensor resCopy(m_nVar, m_nDof_var, m_comm);
  resCopy.copyTensorFrom(residual);

  cout << "Reordering: " << endl;
  cout << varSet[0] << " ---- " << varSet[1] << " ---- " << varSet[2] << endl;
  resCopy.reorderVars(varSet);
  resCopy.print();
  cout << "------------------------\n";


  //contractTensor(resCopy, iBest, modes);
  contractTensor(resCopy, 0, modes);
  cout << "After contraction: " << endl;
  resCopy.print();


  unsigned int counter = 1;
  while(varSet.size()>2){
    cout << "Compute the possible pairings and related unfoldings." << endl;
    const unsigned int currentDim = varSet.size()-1;

    vector<svd> svdUnfold(currentDim);
    vector<unsigned int> pairingVars_list(currentDim);
    unsigned int cc = 0;
    for(unsigned int jVar=1; jVar<varSet.size(); jVar++){ // varSet[0] = iBest!
      unsigned int id_of_var = varSet[jVar];
      cout << "Trying var " << varSet[0] << " with " << id_of_var << endl;
      pairingVars_list[cc] = id_of_var;
      //CPTensor tmp = pairVars(resCopy, iBest, id_of_var); iBest=varSet[0]
      CPTensor tmp = pairVars(resCopy, 0, jVar);
      //tmp.print();
      mat unf;
      //tmp.computeUnfolding(iBest, unf);
      tmp.computeUnfolding(0, unf);
      cout << "The unfolding has size: " << unf.nRows() << " x " << unf.nCols() << endl;
      svdUnfold[cc].init(unf);
      //svdUnfold[cc].Svec().print();
      // free the memory:
      unf.clear();
      tmp.clear();
      cc += 1;
    }
    // Choose the best unfolding:
    unsigned int jBest = 0;
    unsigned int rank_to_pair = 0;
    chooseUnfolding(svdUnfold, jBest, rank_to_pair);

    unsigned int varPairedWith_iBest = pairingVars_list[jBest];

    cout << "Variable to be chained is " << pairingVars_list[jBest] << endl;

    // in-place reshaping of the resCopy tensor:
    //inplace_pairVars(resCopy, iBest, varPairedWith_iBest);
    inplace_pairVars(resCopy, 0, jBest+1);
    cout << "residual copy tensor is now: \n";
    resCopy.print();

    // contraction with the modes:
    vector<vec> modes(rank_to_pair);
    const unsigned int nDofModes = resCopy.nDof_var(0); // because 0 is iBest<-varSet[0]
    for(unsigned int iMod=0; iMod<rank_to_pair; iMod++){
      modes[iMod].init(nDofModes, m_comm);
      for(unsigned int iDof=0; iDof<nDofModes; iDof++){
        double value = svdUnfold[jBest].Umat().getMatEl(iDof,iMod);
        if(fabs(value) > DBL_EPSILON){modes[iMod].setVecEl(iDof,value);}
      }
      modes[iMod].finalize();
    }

    //contractTensor(resCopy, iBest, modes); //iBest = varSet[0]
    contractTensor(resCopy, 0, modes);
    //resCopy.print();

    // reshape the modes to put them in the core:
    vector<vector<vec> > modes_table(rank);
    for(unsigned int iMin=0; iMin<rank; iMin++){
      modes_table[iMin].resize(rank_to_pair);
    }
    for(unsigned int iPlu=0; iPlu<rank_to_pair; iPlu++){
      unsigned int unfDofCount = 0;
      for(unsigned int iMin=0; iMin<rank; iMin++){
        vec fibre(m_nDof_var[varPairedWith_iBest], m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[varPairedWith_iBest]; iDof++){
          double value = modes[iPlu].getVecEl(unfDofCount);
          fibre(iDof) = value;
          unfDofCount += 1;
        }
        fibre.finalize();
        (modes_table[iMin])[iPlu] = fibre;
      }
    }

    //fibreTrain.push_back(modes_table);
    order_of_variables[counter] = varPairedWith_iBest;
    fibreTrain[counter] = modes_table;
    counter += 1;

    // free the memory:
    for(unsigned int iMod=0; iMod<rank_to_pair; iMod++){
      modes[iMod].clear();
    }
    modes.clear();

    // update ranks at the end of the cycle...iBest stays the same!
    rank = rank_to_pair;
    rank_to_pair = 1;

    // remove the varPaired from the set of variables to be considered:
    varSet.erase(varSet.begin()+varPairedWith_iBest);

    // if varSet size is 2 this means that the right modes times the singular values are the ending core:
    if(varSet.size()==2){
      const unsigned int lastVar = varSet[1];
      order_of_variables[m_nVar-1] = varSet[1];
      // at this point rank = rank_to_pair
      vector<vector<vec> > endingCore(rank);
      for(unsigned int iMin=0; iMin<rank; iMin++){
        endingCore[iMin].resize(1);
        vec mod(m_nDof_var[lastVar], m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[lastVar]; iDof++){
          double value = svdUnfold[jBest].Vmat().getMatEl(iDof,iMin) * svdUnfold[jBest].Svec().getVecEl(iMin);
          if(fabs(value) > DBL_EPSILON){mod.setVecEl(iDof,value);} // equivalent to: mod(iDof) = value;
        }
        mod.finalize();
        endingCore[iMin][0] = mod;
      }
      // putting the ending core in the table:
      fibreTrain[m_nVar-1] = endingCore;
    }

    // free the memory:
    for(unsigned int iCurr=0; iCurr<currentDim; iCurr++){
      svdUnfold[iCurr].clear();
    }
    svdUnfold.clear();
    resCopy.clear();
  }

  // updating the table of the variables order.
  m_var_order.push_back(order_of_variables);

  // assemble the tensor train and put it into m_terms;
  TensorTrain termToAdd(fibreTrain);
  m_terms.push_back(termToAdd);
  m_nTerms += 1;
}


/* II.1.b -- Compute a TT term given a CP, fast version
  - input: a CP tensor
  - output: compute a TT term and put it into m_terms
*/
void SoTT::compute_TT_fast(CPTensor& residual){
  // 1 - declare the train:
  vector< vector< vector<vec> > > fibreTrain(m_nVar);
  vector<unsigned int> order_of_variables(m_nVar);

  // start by computing the Svd of the unfoldings and choose the best:
  unsigned int iBest = 0;
  double Lbest = 0.0;
  unsigned int rank = 0;

  vector<double> S_best;
  vector<vec> U_best;

  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    vector<double> S_0;
    vector<vec> U_0;

    residual.compute_Unfolding_Svd(iVar, S_0, U_0);
    //residual.compute_Approx_Unfolding_Svd(iVar, S_0, U_0);
    unsigned int uSize = residual.nDof_var(iVar);
    unsigned int vSize = 1.0;
    for(unsigned int jVar=0; jVar<m_nVar; jVar++){
      if(jVar != iVar){
        vSize *= residual.nDof_var(jVar);
      }
    }

    double L = 0.0;
    unsigned int rank_unf = 0;
    compute_L(S_0, uSize, vSize, L, rank_unf);
    cout << "Variable " << iVar <<",  L = " << L << endl;
    if( (L>Lbest) ){
      Lbest = L;
      iBest = iVar;
      rank = rank_unf;

      for(unsigned int iMod=0; iMod<U_best.size(); iMod++){
        U_best[iMod].clear();
      }
      U_best.resize(rank);
      S_best.resize(rank);
      for(unsigned int iS=0; iS<rank; iS++){
        S_best[iS] = S_0[iS];
        U_best[iS].copyVecFrom(U_0[iS]);
      }

    }else{
      S_0.clear();
      for(unsigned int iMod=0; iMod<U_0.size(); iMod++){
        U_0[iMod].clear();
      }
      U_0.clear();
    }
  }

  // Choose the best unfolding:
  cout << "The best unfolding is " << iBest << " and the rank is " << rank << endl;


  order_of_variables[0] = iBest;

  // definition of variable set:
  vector<unsigned int> varSet(m_nVar);
  varSet[0] = iBest;                 //iBest won't change throughout the process
  unsigned int countVar = 1;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != iBest){
      varSet[countVar] = iVar;
      countVar += 1;
    }
  }

  // the first variable has size 1 x rank
  vector<vector<vec> > firstVarModes(1);
  firstVarModes[0] = U_best;
  fibreTrain[0] = firstVarModes;

  // copy the residual into a temporary object, not to modify the original tensor.
  CPTensor resCopy(m_nVar, m_nDof_var, m_comm);
  resCopy.copyTensorFrom(residual);

  cout << "Reordering: " << endl;
  resCopy.reorderVars(varSet);
  resCopy.print();
  cout << "------------------------\n";

  //contract the tensor with the modes:
  contractTensor(resCopy, 0, U_best);
  cout << "After contraction: " << endl;
  resCopy.print();

  unsigned int counter = 1;
  while(varSet.size()>2){
    cout << "Compute the possible pairings and related unfoldings." << endl;
    const unsigned int currentDim = varSet.size()-1;

    // choose the best unfolding:
    vector<unsigned int> pairingVars_list(currentDim);
    unsigned int cc = 0;
    unsigned int jBest = 0;
    double Lbest = 0.0;
    unsigned int rank_to_pair = 0;
    vector<double> S_pair;
    vector<vec> U_pair;
    for(unsigned int jVar=1; jVar<varSet.size(); jVar++){ // varSet[0] = iBest!
      unsigned int id_of_var = varSet[jVar];
      cout << "Trying var " << varSet[0] << " with " << id_of_var << endl;
      pairingVars_list[cc] = id_of_var;

      CPTensor tmp = pairVars(resCopy, 0, jVar);

      vector<double> S_0;
      vector<vec> U_0;
      tmp.compute_Unfolding_Svd(0, S_0, U_0);
      //tmp.compute_Approx_Unfolding_Svd(0, S_0, U_0);

      unsigned int uSize = tmp.nDof_var(0);
      unsigned int vSize = 1.0;
      for(unsigned int jVar=1; jVar<tmp.nVar(); jVar++){
        vSize *= tmp.nDof_var(jVar);
      }

      double L = 0.0;
      unsigned int rank_unf = 0; //
      compute_L(S_0, uSize, vSize, L, rank_unf);

      cout << "Unfolding " << cc << ",  L = " << L << endl;
      if(L>Lbest){
        Lbest = L;
        jBest = cc;
        rank_to_pair = rank_unf;
        for(unsigned int iMod=0; iMod<U_pair.size(); iMod++){
          U_pair[iMod].clear();
        }
        U_pair.resize(rank_to_pair);
        S_pair.resize(rank_to_pair);
        for(unsigned int iS=0; iS<rank_to_pair; iS++){
          S_pair[iS] = S_0[iS];
          U_pair[iS].copyVecFrom(U_0[iS]);
        }
      }
      else{
        for(unsigned int iMod=0; iMod<U_0.size(); iMod++){
          U_0[iMod].clear();
        }
        U_0.clear();
        S_0.clear();
      }
      // free the memory:
      tmp.clear();
      cc += 1;
    }

    unsigned int varPairedWith_iBest = pairingVars_list[jBest];

    cout << "Variable to be chained is " << pairingVars_list[jBest] << " and rank is " << rank_to_pair << endl;

    // in-place reshaping of the resCopy tensor:
    inplace_pairVars(resCopy, 0, jBest+1);
    cout << "residual copy tensor is now of size: \n";
    resCopy.print();

    // reshape the modes to put them in the core:
    vector<vector<vec> > modes_table(rank);
    for(unsigned int iMin=0; iMin<rank; iMin++){
      modes_table[iMin].resize(rank_to_pair);
    }
    for(unsigned int iPlu=0; iPlu<rank_to_pair; iPlu++){
      unsigned int unfDofCount = 0;
      for(unsigned int iMin=0; iMin<rank; iMin++){
        vec fibre(m_nDof_var[varPairedWith_iBest], m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[varPairedWith_iBest]; iDof++){
          double value = U_pair[iPlu].getVecEl(unfDofCount);
          if(fabs(value)>DBL_EPSILON){
            fibre(iDof) = value;
          }
          unfDofCount += 1;
        }
        fibre.finalize();
        (modes_table[iMin])[iPlu] = fibre;
      }
    }

    //fibreTrain.push_back(modes_table);
    order_of_variables[counter] = varPairedWith_iBest;
    fibreTrain[counter] = modes_table;
    counter += 1;

    // update ranks at the end of the cycle...iBest stays the same!
    rank = rank_to_pair;
    rank_to_pair = 1;

    // remove the varPaired from the set of variables to be considered:
    varSet.erase(varSet.begin()+jBest+1);

    // if varSet size is 2 this means that the right modes times the singular values are the ending core:
    if(varSet.size()==2){
      const unsigned int lastVar = varSet[1];
      order_of_variables[m_nVar-1] = varSet[1];

      contractTensor(resCopy, 0, U_pair);

      // at this point rank = rank_to_pair
      vector<vector<vec> > endingCore(rank);
      for(unsigned int iMin=0; iMin<rank; iMin++){
        endingCore[iMin].resize(1);
        vec mod(m_nDof_var[lastVar], m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[lastVar]; iDof++){
          vector<unsigned int> ind(2);
          if(resCopy.nDof_var(0) == rank){
            ind = {iMin, iDof};
          }
          else{
            ind = {iDof, iMin};
          }
          double value = resCopy.eval(ind);
          if(fabs(value) > DBL_EPSILON){mod.setVecEl(iDof,value);} // equivalent to: mod(iDof) = value;
        }
        mod.finalize();
        endingCore[iMin][0] = mod;
      }
      // putting the ending core in the table:
      fibreTrain[m_nVar-1] = endingCore;
    }
    else{
      contractTensor(resCopy, 0, U_pair);
      //resCopy.print();
    }

    // free the memory:
    for(unsigned int iMod=0; iMod<rank_to_pair; iMod++){
      U_pair[iMod].clear();
    }
    U_pair.clear();

  }
  resCopy.clear();

  // updating the table of the variables order.
  m_var_order.push_back(order_of_variables);

  // assemble the tensor train and put it into m_terms;
  TensorTrain termToAdd(fibreTrain);
  m_terms.push_back(termToAdd);
  m_nTerms += 1;
}


/* II.3 -- Pair vars: pair variables and produce a CPTensor whose unfolding will be tested.
  - input: a CP to be "reshaped" and reduced (1 variable less), the iBest, the candidate
  - output: a reshaped CP tensor with variables x_1, ..., (iBest,idV_2), ..., x_d
*/
CPTensor SoTT::pairVars(CPTensor& T, unsigned int idV_1, unsigned int idV_2){
  assert(idV_1 != idV_2);
  const unsigned int dim = T.nVar() - 1;
  vector<unsigned int> dofPerDim(dim);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
    if( (iVar != idV_1) && (iVar != idV_2) ){
      dofPerDim[cc] = T.nDof_var(iVar);
      cc += 1;
    }
    else{
      if( iVar == idV_1 ){
        dofPerDim[cc] = T.nDof_var(idV_1) * T.nDof_var(idV_2);
        cc += 1;
      }
    }
  }


  CPTensor toBeReturned(dim, dofPerDim, T.comm());
  const unsigned int rankOfT = T.rank();
  toBeReturned.set_rank(rankOfT);
  // copying coefficients:
  for(unsigned int iTerm=0; iTerm<rankOfT; iTerm++){
    toBeReturned.set_coeffs(iTerm, T.coeffs(iTerm));
  }
  // setting terms by copy:
  cc = 0;
  for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
    if( (iVar != idV_1) && (iVar != idV_2) ){
      for(unsigned int iTerm=0; iTerm<T.rank(); iTerm++){
        vec termCopy;
        vec termOfT = T.terms(iTerm,iVar);
        termCopy.copyVecFrom(termOfT);
        toBeReturned.set_terms(iTerm, cc, termCopy);
      }
      cc += 1;
    }
    else{
      if( iVar == idV_1 ){
        for(unsigned int iTerm=0; iTerm<T.rank(); iTerm++){
          vec term_1 = T.terms(iTerm, idV_1);
          vec term_2 = T.terms(iTerm, idV_2);
          vec toAdd(dofPerDim[cc], T.comm());
          unsigned int jDof = 0;
          for(unsigned int i_1=0; i_1<T.nDof_var(idV_1); i_1++){
            double val_1 = term_1(i_1);
            //double val_1 = T.terms(iTerm, idV_1).getVecEl(i_1);
            for(unsigned int i_2=0; i_2< T.nDof_var(idV_2); i_2++){
              double val_2 = term_2(i_2);
              //double val_2 = T.terms(iTerm, idV_2).getVecEl(i_2);
              double val = val_1*val_2;
              toAdd.setVecEl(jDof,val);
              jDof += 1;
            }
          }
          toAdd.finalize();
          toBeReturned.set_terms(iTerm, cc, toAdd);
        }
        cc += 1;
      }
    }
  }
  toBeReturned.finalize();

  return toBeReturned;
}

// in place version => reshape the input tensor by pairing vars!
void SoTT::inplace_pairVars(CPTensor& T, unsigned int idV_1, unsigned int idV_2){
  assert(idV_1 != idV_2);
  const unsigned int dim = T.nVar() - 1;
  vector<unsigned int> dofPerDim(dim);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
    if( (iVar != idV_1) && (iVar != idV_2) ){
      dofPerDim[cc] = T.nDof_var(iVar);
      cc += 1;
    }
    else{
      if( iVar == idV_1 ){
        dofPerDim[cc] = T.nDof_var(idV_1) * T.nDof_var(idV_2);
        cc += 1;
      }
    }
  }
  // these are the new dimension and dof per dimension of T, to be replaced in the end;
  for(unsigned int iTerm=0; iTerm<T.rank(); iTerm++){

    vector<vec> newTerm(dim);
    cc = 0;
    for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
      if( (iVar != idV_1) && (iVar != idV_2) ){
        vec termCopy;
        vec termOfT = T.terms(iTerm,iVar);
        newTerm[cc].copyVecFrom(termOfT);
        cc += 1;
      }
      else{
        if( iVar == idV_1 ){
          vec term_1 = T.terms(iTerm, idV_1);
          vec term_2 = T.terms(iTerm, idV_2);
          newTerm[cc].init(dofPerDim[cc], T.comm());
          unsigned int jDof = 0;
          for(unsigned int i_1=0; i_1<T.nDof_var(idV_1); i_1++){
            double val_1 = term_1(i_1);
            for(unsigned int i_2=0; i_2< T.nDof_var(idV_2); i_2++){
              double val_2 = term_2(i_2);
              double val = val_1*val_2;
              newTerm[cc](jDof) = val;
              jDof += 1;
            }
          }
          newTerm[cc].finalize();
          cc += 1;
        }
      }
    }
    // replace the current term with the new one.
    T.replaceTerm(iTerm, newTerm);
  }
  // correct the sizes and update indices maps:
  T.set_nVar(dim);
  T.set_nDof_var(dofPerDim);
  T.compute_minc();
  T.finalize();
}


/* II.4 -- contract the tensor with a set of modes
  - input: a CP tensor that will be modified, the direction, a set of modes.
  - output: the CP tensor is modified "in place".
*/
void SoTT::contractTensor(CPTensor& residual, unsigned int iBest, vector<vec>& modes){
  const unsigned int rank = modes.size();
  residual.change_nDof_var(iBest,rank);
  for(unsigned int iTerm=0; iTerm<residual.rank(); iTerm++){
    vec termToExchange(rank, residual.comm());
    vec t_0 = residual.terms(iTerm, iBest);
    for(unsigned int iDof=0; iDof<rank; iDof++){
      vec t_1 = modes[iDof];
      double value = scalProd(t_1, t_0);
      termToExchange(iDof) = value;
    }
    termToExchange.finalize();
    residual.replaceTerm(iTerm, iBest, termToExchange);
  }
}



/* II.5 -- update the residual given in a CP format by subtracting a TT term
  - input: the residual (in CP format) and the TT to subtract
  - output: the residual is updated.
*/
double SoTT::update_residual(CPTensor& residual, unsigned int iTerm){

  // initial residual norm:
  double normRes = sqrt(residual.norm2CP());
  cout << "Initial residual norm = " << normRes << endl;

  // variable order to be used:
  vector<unsigned int> order = m_var_order[iTerm];

  // compute progressively the CPterms to be subtracted.
  unsigned int n_pure_terms = 1;
  for(unsigned int iVar=0; iVar<m_nVar-1; iVar++){
    n_pure_terms *= m_terms[iTerm].ranks(iVar);
  }

  vector<unsigned int> lin_inc(m_nVar-1);
  lin_inc[0] = 1;
  for(unsigned int iVar=1; iVar<m_nVar-1; iVar++){
    lin_inc[iVar] = lin_inc[iVar-1] * m_terms[iTerm].ranks(iVar-1);
  }

  cout << "Total number of pure tensor terms = " << n_pure_terms << endl;

  // cycling over all possible pure tensor terms (remapping the order of variables!)
  for(unsigned int jT=0; jT<n_pure_terms; jT++){
    vector<vec> toAdd(m_nVar);
    // id of the fibres:
    vector<unsigned int> index_sum(m_nVar-1);
    unsigned int remainder = jT;
    for(unsigned int iVar=0; iVar<m_nVar-1; iVar++){
      index_sum[m_nVar-2-iVar] = remainder/lin_inc[m_nVar-2-iVar]; // integer division.
      remainder = remainder - lin_inc[m_nVar-2-iVar] * index_sum[m_nVar-2-iVar];
    }
    //index_sum[0] = remainder;
    //cout << "jT = " << jT << ": " << index_sum[0] << " -- " << index_sum[1] << " -- " << index_sum[2] <<  endl;

    // for the first and the last unfolding id_fibre identifies the fibers, for the intermediate cores
    // another step is needed:

    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      unsigned int iVar_CP = order[iVar];

      if( (iVar==0) || (iVar==m_nVar-1) ){
        if(iVar==0){
          unsigned int iTT = index_sum[iVar];
          toAdd[iVar_CP] = m_terms[iTerm].train(iVar,0,iTT);
        }
        else{
          unsigned int iTT = index_sum[m_nVar-2];
          toAdd[iVar_CP] = m_terms[iTerm].train(iVar,iTT,0);
        }
      }
      else{
        unsigned int I = index_sum[iVar-1];
        unsigned int J = index_sum[iVar];
        toAdd[iVar_CP] = m_terms[iTerm].train(iVar,I,J);
      }
    }
    // adding the term to the residual:
    /*toAdd[0].print();
    toAdd[1].print();
    toAdd[2].print();
    cout << "-----" << endl << endl;*/

    //CPTensor add(toAdd, m_comm);
    //residual.sum(add, -1.0, false);
    residual.addPureTensorTerm(toAdd, -1.0);
  }
  double normRes_after = sqrt(residual.norm2CP());
  cout << "The norm of the residual is: " << normRes_after <<endl;
  return normRes_after;
}





// -- Methods related to a full tensor input: -- //


/* main function: full2Sott
  - input: a full tensor, a tolerance
  - output: assemble its SoTT approximation
*/
void SoTT::full2SoTT(fullTensor& residual, double tolerance, bool full_unfold=true){
  m_nTerms = 0;
  m_tol = tolerance;
  double normRes = residual.tensorEntries().norm();

  unsigned int cc = 0;
  while(normRes>tolerance){
    //compute_TT(residual);
    compute_TT_fast(residual, full_unfold);
    cout << endl;
    cout << "---------------------------------" << endl;
    normRes = update_residual(residual,cc);
    unsigned int mem = memory();
    cout << "Actual memory of the " << m_nTerms << " terms: " << mem << endl;
    cout << "---------------------------------" << endl << endl;
    cc += 1;
  }
}



/* II.6 -- Compute a TT term given a fullTensor
  - input: a full tensor
  - output: compute a TT term and put it into m_terms
*/
void SoTT::compute_TT(fullTensor& residual){
  assert(residual.nVar()==m_nVar);


  // 1 - declare the train:
  vector< vector< vector<vec> > > fibreTrain(m_nVar);
  vector<unsigned int> order_of_variables(m_nVar);

  // start by computing the Svd of the unfoldings:
  vector<svd> svdUnfold(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    mat unf;
    residual.computeUnfolding(iVar, unf);
    svdUnfold[iVar].init(unf);
    unf.clear();
  }

  // Choose the best unfolding:
  unsigned int iBest = 0;
  unsigned int rank = 1;
  chooseUnfolding(svdUnfold, iBest, rank);
  order_of_variables[0] = iBest;

  // definition of variable set:
  vector<unsigned int> varSet(m_nVar);
  varSet[0] = iBest;                 //iBest won't change throughout the process
  unsigned int countVar = 1;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != iBest){
      varSet[countVar] = iVar;
      countVar += 1;
    }
  }

  // extract the TT modes and put them in the train:
  vector<vec> modes(rank);
  for(unsigned int iMod=0; iMod<rank; iMod++){
    modes[iMod].init(m_nDof_var[iBest], m_comm);
    for(unsigned int iDof=0; iDof<m_nDof_var[iBest]; iDof++){
      double value = svdUnfold[iBest].Umat().getMatEl(iDof,iMod);
      if(fabs(value) > DBL_EPSILON){
        modes[iMod].setVecEl(iDof,value);
      }
    }
    modes[iMod].finalize();
  }
  // the first variable has size 1 x rank
  vector<vector<vec> > firstVarModes(1);
  firstVarModes[0] = modes;
  fibreTrain[0] = firstVarModes;

  //free the memory:
  for(unsigned int iVar=0;iVar<m_nVar; iVar++){
    svdUnfold[iVar].clear();
  }
  svdUnfold.clear();

  // copy the residual into a temporary object, not to modify the original tensor.
  fullTensor resCopy(m_nVar, m_nDof_var, m_comm);
  resCopy.copyTensorFrom(residual);

  cout << "Reordering: " << endl;
  cout << varSet[0] << " ---- " << varSet[1] << " ---- " << varSet[2] << endl;
  resCopy.reorderVars(varSet);
  resCopy.print();
  cout << "------------------------\n";

  //contract the tensor with the modes:
  contractTensor(resCopy, 0, modes);
  cout << "After contraction: " << endl;
  resCopy.print();

  unsigned int counter = 1;
  while(varSet.size()>2){
    cout << "Compute the possible pairings and related unfoldings." << endl;
    const unsigned int currentDim = varSet.size()-1;

    vector<svd> svdUnfold(currentDim);
    vector<unsigned int> pairingVars_list(currentDim);
    unsigned int cc = 0;
    for(unsigned int jVar=1; jVar<varSet.size(); jVar++){ // varSet[0] = iBest!
      unsigned int id_of_var = varSet[jVar];
      cout << "Trying var " << varSet[0] << " with " << id_of_var << endl;
      pairingVars_list[cc] = id_of_var;
      //resCopy.print();

      fullTensor tmp = pairVars(resCopy, 0, jVar);

      //tmp.print();
      mat unf;
      //tmp.computeUnfolding(iBest, unf);
      tmp.computeUnfolding(0, unf);
      cout << "The unfolding has size: " << unf.nRows() << " x " << unf.nCols() << endl;
      svdUnfold[cc].init(unf);
      //svdUnfold[cc].Svec().print();
      // free the memory:
      unf.clear();
      tmp.clear();
      cc += 1;
    }
    // Choose the best unfolding:
    unsigned int jBest = 0;
    unsigned int rank_to_pair = 0;
    chooseUnfolding(svdUnfold, jBest, rank_to_pair);

    unsigned int varPairedWith_iBest = pairingVars_list[jBest];

    cout << "Variable to be chained is " << pairingVars_list[jBest] << endl;

    // in-place reshaping of the resCopy tensor:
    //inplace_pairVars(resCopy, iBest, varPairedWith_iBest);
    inplace_pairVars(resCopy, 0, jBest+1);
    cout << "residual copy tensor is now: \n";
    resCopy.print();

    // extraction of the modes:
    vector<vec> modes(rank_to_pair);
    const unsigned int nDofModes = resCopy.nDof_var(0); // because 0 is iBest<-varSet[0]
    for(unsigned int iMod=0; iMod<rank_to_pair; iMod++){
      modes[iMod].init(nDofModes, m_comm);
      for(unsigned int iDof=0; iDof<nDofModes; iDof++){
        double value = svdUnfold[jBest].Umat().getMatEl(iDof,iMod);
        if(fabs(value) > DBL_EPSILON){modes[iMod].setVecEl(iDof,value);}
      }
      modes[iMod].finalize();
    }

    // reshape the modes to put them in the core:
    vector<vector<vec> > modes_table(rank);
    for(unsigned int iMin=0; iMin<rank; iMin++){
      modes_table[iMin].resize(rank_to_pair);
    }
    for(unsigned int iPlu=0; iPlu<rank_to_pair; iPlu++){
      unsigned int unfDofCount = 0;
      for(unsigned int iMin=0; iMin<rank; iMin++){
        vec fibre(m_nDof_var[varPairedWith_iBest], m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[varPairedWith_iBest]; iDof++){
          double value = modes[iPlu].getVecEl(unfDofCount);
          fibre(iDof) = value;
          unfDofCount += 1;
        }
        fibre.finalize();
        (modes_table[iMin])[iPlu] = fibre;
      }
    }

    //fibreTrain.push_back(modes_table);
    order_of_variables[counter] = varPairedWith_iBest;
    fibreTrain[counter] = modes_table;
    counter += 1;

    // update ranks at the end of the cycle...iBest stays the same!
    rank = rank_to_pair;
    rank_to_pair = 1;

    // remove the varPaired from the set of variables to be considered:
    //varSet.erase(varSet.begin()+varPairedWith_iBest);
    varSet.erase(varSet.begin()+jBest+1);

    // if varSet size is 2 this means that the right modes times the singular values are the ending core:
    if(varSet.size()==2){
      const unsigned int lastVar = varSet[1];
      order_of_variables[m_nVar-1] = varSet[1];
      // at this point rank = rank_to_pair
      vector<vector<vec> > endingCore(rank);
      for(unsigned int iMin=0; iMin<rank; iMin++){
        endingCore[iMin].resize(1);
        vec mod(m_nDof_var[lastVar], m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[lastVar]; iDof++){
          double value = svdUnfold[jBest].Vmat().getMatEl(iDof,iMin) * svdUnfold[jBest].Svec().getVecEl(iMin);
          if(fabs(value) > DBL_EPSILON){mod.setVecEl(iDof,value);} // equivalent to: mod(iDof) = value;
        }
        mod.finalize();
        endingCore[iMin][0] = mod;
      }
      // putting the ending core in the table:
      fibreTrain[m_nVar-1] = endingCore;
    }
    else{
      contractTensor(resCopy, 0, modes);
      //resCopy.print();
    }

    // free the memory:
    for(unsigned int iMod=0; iMod<rank_to_pair; iMod++){
      modes[iMod].clear();
    }
    modes.clear();
    for(unsigned int iCurr=0; iCurr<currentDim; iCurr++){
      svdUnfold[iCurr].clear();
    }
    svdUnfold.clear();
  }
  resCopy.clear();

  // updating the table of the variables order.
  m_var_order.push_back(order_of_variables);

  // assemble the tensor train and put it into m_terms;
  TensorTrain termToAdd(fibreTrain);
  m_terms.push_back(termToAdd);
  m_nTerms += 1;
}


/* II.6.b -- Compute a TT term given a fullTensor, fast version (less memory demanding)
  - input: a full tensor
  - output: compute a TT term and put it into m_terms
*/
void SoTT::compute_TT_fast(fullTensor& residual){
  assert(residual.nVar()==m_nVar);

  // 1 - declare the train:
  vector< vector< vector<vec> > > fibreTrain(m_nVar);
  vector<unsigned int> order_of_variables(m_nVar);

  // start by computing the Svd of the unfoldings and choose the best:
  unsigned int iBest = 0;
  double Lbest = 0.0;
  unsigned int rank = 0;
  vector<svd> svdUnfold(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    mat unf;
    residual.computeUnfolding(iVar, unf);
    //svdUnfold[iVar].compute_svdCov(unf);
    svdUnfold[iVar].init(unf, true, true);

    double L = 0.0;
    unsigned int rank_unf = 0;
    compute_L(svdUnfold[iVar], L, rank_unf);
    cout << "Variable " << iVar <<",  L = " << L << endl;
    if( (L>Lbest) ){
      Lbest = L;
      iBest = iVar;
      rank = rank_unf;
    }else{
      svdUnfold[iVar].clear();
    }
    unf.clear();
  }

  // Choose the best unfolding:
  cout << "The best unfolding is " << iBest << " and the rank is " << rank << endl;

  order_of_variables[0] = iBest;

  // definition of variable set:
  vector<unsigned int> varSet(m_nVar);
  varSet[0] = iBest;                 //iBest won't change throughout the process
  unsigned int countVar = 1;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != iBest){
      varSet[countVar] = iVar;
      countVar += 1;
    }
  }

  // extract the TT modes and put them in the train:
  vector<vec> modes(rank);
  for(unsigned int iMod=0; iMod<rank; iMod++){
    modes[iMod].copyVecFrom(svdUnfold[iBest].Umod(iMod));
  }
  // the first variable has size 1 x rank
  vector<vector<vec> > firstVarModes(1);
  firstVarModes[0] = modes;
  fibreTrain[0] = firstVarModes;

  //free the memory:
  svdUnfold[iBest].clear();
  svdUnfold.clear();


  // copy the residual into a temporary object, not to modify the original tensor.
  fullTensor resCopy(m_nVar, m_nDof_var, m_comm);
  resCopy.copyTensorFrom(residual);

  cout << "Reordering: " << endl;
  resCopy.reorderVars(varSet);
  resCopy.print();
  cout << "------------------------\n";

  //contract the tensor with the modes:
  contractTensor(resCopy, 0, modes);
  cout << "After contraction: " << endl;
  resCopy.print();

  unsigned int counter = 1;
  while(varSet.size()>2){
    cout << "Compute the possible pairings and related unfoldings." << endl;
    const unsigned int currentDim = varSet.size()-1;

    // choose the best unfolding:
    vector<svd> svdUnfold(currentDim);
    vector<unsigned int> pairingVars_list(currentDim);
    unsigned int cc = 0;
    unsigned int jBest = 0;
    double Lbest = 0.0;
    unsigned int rank_to_pair = 0;
    for(unsigned int jVar=1; jVar<varSet.size(); jVar++){ // varSet[0] = iBest!
      unsigned int id_of_var = varSet[jVar];
      cout << "Trying var " << varSet[0] << " with " << id_of_var << endl;
      pairingVars_list[cc] = id_of_var;

      fullTensor tmp = pairVars(resCopy, 0, jVar);

      mat unf;
      tmp.computeUnfolding(0, unf);

      cout << "The unfolding has size: " << unf.nRows() << " x " << unf.nCols() << endl;
      //svdUnfold[cc].compute_svdCov(unf);
      svdUnfold[cc].init(unf, true, true);

      double L = 0.0;
      unsigned int rank_unf = 0; //
      compute_L(svdUnfold[cc], L, rank_unf);

      cout << "Unfolding " << cc << ",  L = " << L << endl;
      if(L>Lbest){
        Lbest = L;
        jBest = cc;
        rank_to_pair = rank_unf;
      }
      else{
        svdUnfold[cc].clear();
      }
      // free the memory:
      unf.clear();
      tmp.clear();
      cc += 1;
    }

    unsigned int varPairedWith_iBest = pairingVars_list[jBest];

    cout << "Variable to be chained is " << pairingVars_list[jBest] << " and rank is " << rank_to_pair << endl;

    // in-place reshaping of the resCopy tensor:
    inplace_pairVars(resCopy, 0, jBest+1);
    cout << "residual copy tensor is now of size: \n";
    resCopy.print();

    // extraction of the modes:
    vector<vec> modes(rank_to_pair);
    for(unsigned int iMod=0; iMod<rank_to_pair; iMod++){
      modes[iMod].copyVecFrom(svdUnfold[jBest].Umod(iMod));
    }

    // reshape the modes to put them in the core:
    vector<vector<vec> > modes_table(rank);
    for(unsigned int iMin=0; iMin<rank; iMin++){
      modes_table[iMin].resize(rank_to_pair);
    }
    for(unsigned int iPlu=0; iPlu<rank_to_pair; iPlu++){
      unsigned int unfDofCount = 0;
      for(unsigned int iMin=0; iMin<rank; iMin++){
        vec fibre(m_nDof_var[varPairedWith_iBest], m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[varPairedWith_iBest]; iDof++){
          double value = modes[iPlu].getVecEl(unfDofCount);
          if(fabs(value)>DBL_EPSILON){
            fibre(iDof) = value;
          }
          unfDofCount += 1;
        }
        fibre.finalize();
        (modes_table[iMin])[iPlu] = fibre;
      }
    }

    //fibreTrain.push_back(modes_table);
    order_of_variables[counter] = varPairedWith_iBest;
    fibreTrain[counter] = modes_table;
    counter += 1;

    // update ranks at the end of the cycle...iBest stays the same!
    rank = rank_to_pair;
    rank_to_pair = 1;

    // remove the varPaired from the set of variables to be considered:
    varSet.erase(varSet.begin()+jBest+1);

    // if varSet size is 2 this means that the right modes times the singular values are the ending core:
    if(varSet.size()==2){
      const unsigned int lastVar = varSet[1];
      order_of_variables[m_nVar-1] = varSet[1];
      // at this point rank = rank_to_pair
      vector<vector<vec> > endingCore(rank);
      for(unsigned int iMin=0; iMin<rank; iMin++){
        endingCore[iMin].resize(1);
        vec mod(m_nDof_var[lastVar], m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[lastVar]; iDof++){
          double value = svdUnfold[jBest].Vmod(iMin).getVecEl(iDof) * svdUnfold[jBest].Svec().getVecEl(iMin);
          if(fabs(value) > DBL_EPSILON){mod.setVecEl(iDof,value);} // equivalent to: mod(iDof) = value;
        }
        mod.finalize();
        endingCore[iMin][0] = mod;
      }
      // putting the ending core in the table:
      fibreTrain[m_nVar-1] = endingCore;
    }
    else{
      contractTensor(resCopy, 0, modes);
      //resCopy.print();
    }

    // free the memory:
    for(unsigned int iMod=0; iMod<rank_to_pair; iMod++){
      modes[iMod].clear();
    }
    modes.clear();

    svdUnfold[jBest].clear();
    svdUnfold.clear();
  }
  resCopy.clear();

  // updating the table of the variables order.
  m_var_order.push_back(order_of_variables);

  // assemble the tensor train and put it into m_terms;
  TensorTrain termToAdd(fibreTrain);
  m_terms.push_back(termToAdd);
  m_nTerms += 1;
}


/* II.6.c -- Compute a TT term given a fullTensor, fast version (less memory demanding)
  - input: a full tensor, bool (compute and store the unfoldings)
  - output: compute a TT term and put it into m_terms
*/
void SoTT::compute_TT_fast(fullTensor& residual, bool full_unfold){
  assert(residual.nVar()==m_nVar);

  // 1 - declare the train:
  vector< vector< vector<vec> > > fibreTrain(m_nVar);
  vector<unsigned int> order_of_variables(m_nVar);

  if(full_unfold){
    // start by computing the Svd of the unfoldings and choose the best:
    unsigned int iBest = 0;
    double Lbest = 0.0;
    unsigned int rank = 0;
    vector<svd> svdUnfold(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      mat unf;
      residual.computeUnfolding(iVar, unf);
      //svdUnfold[iVar].compute_svdCov(unf);
      svdUnfold[iVar].init(unf, true, true);

      double L = 0.0;
      unsigned int rank_unf = 0;
      compute_L(svdUnfold[iVar], L, rank_unf);
      cout << "Variable " << iVar <<",  L = " << L << endl;
      if( (L>Lbest) ){
        Lbest = L;
        iBest = iVar;
        rank = rank_unf;
      }else{
        svdUnfold[iVar].clear();
      }
      unf.clear();
    }

    // Choose the best unfolding:
    cout << "The best unfolding is " << iBest << " and the rank is " << rank << endl;

    order_of_variables[0] = iBest;

    // definition of variable set:
    vector<unsigned int> varSet(m_nVar);
    varSet[0] = iBest;                 //iBest won't change throughout the process
    unsigned int countVar = 1;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != iBest){
        varSet[countVar] = iVar;
        countVar += 1;
      }
    }

    // extract the TT modes and put them in the train:
    vector<vec> modes(rank);
    for(unsigned int iMod=0; iMod<rank; iMod++){
      modes[iMod].copyVecFrom(svdUnfold[iBest].Umod(iMod));
    }
    // the first variable has size 1 x rank
    vector<vector<vec> > firstVarModes(1);
    firstVarModes[0] = modes;
    fibreTrain[0] = firstVarModes;

    //free the memory:
    svdUnfold[iBest].clear();
    svdUnfold.clear();


    // copy the residual into a temporary object, not to modify the original tensor.
    fullTensor resCopy(m_nVar, m_nDof_var, m_comm);
    resCopy.copyTensorFrom(residual);

    cout << "Reordering: " << endl;
    resCopy.reorderVars(varSet);
    resCopy.print();
    cout << "------------------------\n";

    //contract the tensor with the modes:
    contractTensor(resCopy, 0, modes);
    cout << "After contraction: " << endl;
    resCopy.print();

    unsigned int counter = 1;
    while(varSet.size()>2){
      cout << "Compute the possible pairings and related unfoldings." << endl;
      const unsigned int currentDim = varSet.size()-1;

      // choose the best unfolding:
      vector<svd> svdUnfold(currentDim);
      vector<unsigned int> pairingVars_list(currentDim);
      unsigned int cc = 0;
      unsigned int jBest = 0;
      double Lbest = 0.0;
      unsigned int rank_to_pair = 0;
      for(unsigned int jVar=1; jVar<varSet.size(); jVar++){ // varSet[0] = iBest!
        unsigned int id_of_var = varSet[jVar];
        cout << "Trying var " << varSet[0] << " with " << id_of_var << endl;
        pairingVars_list[cc] = id_of_var;

        fullTensor tmp = pairVars(resCopy, 0, jVar);

        mat unf;
        tmp.computeUnfolding(0, unf);

        cout << "The unfolding has size: " << unf.nRows() << " x " << unf.nCols() << endl;
        //svdUnfold[cc].compute_svdCov(unf);
        svdUnfold[cc].init(unf, true, true);

        double L = 0.0;
        unsigned int rank_unf = 0; //
        compute_L(svdUnfold[cc], L, rank_unf);

        cout << "Unfolding " << cc << ",  L = " << L << endl;
        if(L>Lbest){
          Lbest = L;
          jBest = cc;
          rank_to_pair = rank_unf;
        }
        else{
          svdUnfold[cc].clear();
        }
        // free the memory:
        unf.clear();
        tmp.clear();
        cc += 1;
      }

      unsigned int varPairedWith_iBest = pairingVars_list[jBest];

      cout << "Variable to be chained is " << pairingVars_list[jBest] << " and rank is " << rank_to_pair << endl;

      // in-place reshaping of the resCopy tensor:
      inplace_pairVars(resCopy, 0, jBest+1);
      cout << "residual copy tensor is now of size: \n";
      resCopy.print();

      // extraction of the modes:
      vector<vec> modes(rank_to_pair);
      for(unsigned int iMod=0; iMod<rank_to_pair; iMod++){
        modes[iMod].copyVecFrom(svdUnfold[jBest].Umod(iMod));
      }

      // reshape the modes to put them in the core:
      vector<vector<vec> > modes_table(rank);
      for(unsigned int iMin=0; iMin<rank; iMin++){
        modes_table[iMin].resize(rank_to_pair);
      }
      for(unsigned int iPlu=0; iPlu<rank_to_pair; iPlu++){
        unsigned int unfDofCount = 0;
        for(unsigned int iMin=0; iMin<rank; iMin++){
          vec fibre(m_nDof_var[varPairedWith_iBest], m_comm);
          for(unsigned int iDof=0; iDof<m_nDof_var[varPairedWith_iBest]; iDof++){
            double value = modes[iPlu].getVecEl(unfDofCount);
            if(fabs(value)>DBL_EPSILON){
              fibre(iDof) = value;
            }
            unfDofCount += 1;
          }
          fibre.finalize();
          (modes_table[iMin])[iPlu] = fibre;
        }
      }

      //fibreTrain.push_back(modes_table);
      order_of_variables[counter] = varPairedWith_iBest;
      fibreTrain[counter] = modes_table;
      counter += 1;

      // update ranks at the end of the cycle...iBest stays the same!
      rank = rank_to_pair;
      rank_to_pair = 1;

      // remove the varPaired from the set of variables to be considered:
      varSet.erase(varSet.begin()+jBest+1);

      // if varSet size is 2 this means that the right modes times the singular values are the ending core:
      if(varSet.size()==2){
        const unsigned int lastVar = varSet[1];
        order_of_variables[m_nVar-1] = varSet[1];
        // at this point rank = rank_to_pair
        vector<vector<vec> > endingCore(rank);
        for(unsigned int iMin=0; iMin<rank; iMin++){
          endingCore[iMin].resize(1);
          vec mod(m_nDof_var[lastVar], m_comm);
          for(unsigned int iDof=0; iDof<m_nDof_var[lastVar]; iDof++){
            double value = svdUnfold[jBest].Vmod(iMin).getVecEl(iDof) * svdUnfold[jBest].Svec().getVecEl(iMin);
            if(fabs(value) > DBL_EPSILON){mod.setVecEl(iDof,value);} // equivalent to: mod(iDof) = value;
          }
          mod.finalize();
          endingCore[iMin][0] = mod;
        }
        // putting the ending core in the table:
        fibreTrain[m_nVar-1] = endingCore;
      }
      else{
        contractTensor(resCopy, 0, modes);
        //resCopy.print();
      }

      // free the memory:
      for(unsigned int iMod=0; iMod<rank_to_pair; iMod++){
        modes[iMod].clear();
      }
      modes.clear();

      svdUnfold[jBest].clear();
      svdUnfold.clear();
    }
    resCopy.clear();

    // updating the table of the variables order.
    m_var_order.push_back(order_of_variables);

    // assemble the tensor train and put it into m_terms;
    TensorTrain termToAdd(fibreTrain);
    m_terms.push_back(termToAdd);
    m_nTerms += 1;
  }
  else{// compute the unfolding svd without computing and storing the unfolding

    // start by computing the Svd of the unfoldings and choose the best:
    unsigned int iBest = 0;
    double Lbest = 0.0;
    unsigned int rank = 0;

    vector<double> S_best;
    vector<vec> U_best;

    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      vector<double> S_0;
      vector<vec> U_0;

      residual.computeUnfoldingSVD(iVar, S_0, U_0, DBL_EPSILON);
      unsigned int uSize = residual.nDof_var(iVar);
      unsigned int vSize = 1.0;
      for(unsigned int jVar=0; jVar<m_nVar; jVar++){
        if(jVar != iVar){
          vSize *= residual.nDof_var(jVar);
        }
      }

      double L = 0.0;
      unsigned int rank_unf = 0;
      compute_L(S_0, uSize, vSize, L, rank_unf);
      cout << "Variable " << iVar <<",  L = " << L << endl;
      if( (L>Lbest) ){
        Lbest = L;
        iBest = iVar;
        rank = rank_unf;

        for(unsigned int iMod=0; iMod<U_best.size(); iMod++){
          U_best[iMod].clear();
        }
        U_best.resize(rank);
        S_best.resize(rank);
        for(unsigned int iS=0; iS<rank; iS++){
          S_best[iS] = S_0[iS];
          U_best[iS].copyVecFrom(U_0[iS]);
        }

      }else{
        S_0.clear();
        for(unsigned int iMod=0; iMod<U_0.size(); iMod++){
          U_0[iMod].clear();
        }
        U_0.clear();
      }
    }

    // Choose the best unfolding:
    cout << "The best unfolding is " << iBest << " and the rank is " << rank << endl;

    order_of_variables[0] = iBest;

    // definition of variable set:
    vector<unsigned int> varSet(m_nVar);
    varSet[0] = iBest;                 //iBest won't change throughout the process
    unsigned int countVar = 1;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != iBest){
        varSet[countVar] = iVar;
        countVar += 1;
      }
    }

    // the first variable has size 1 x rank
    vector<vector<vec> > firstVarModes(1);
    firstVarModes[0] = U_best;
    fibreTrain[0] = firstVarModes;

    // copy the residual into a temporary object, not to modify the original tensor.
    fullTensor resCopy(m_nVar, m_nDof_var, m_comm);
    resCopy.copyTensorFrom(residual);

    cout << "Reordering: " << endl;
    resCopy.reorderVars(varSet);
    resCopy.print();
    cout << "------------------------\n";

    //contract the tensor with the modes:
    contractTensorNoUnfold(resCopy, 0, U_best);
    cout << "After contraction: " << endl;
    resCopy.print();

    unsigned int counter = 1;
    while(varSet.size()>2){
      cout << "Compute the possible pairings and related unfoldings." << endl;
      const unsigned int currentDim = varSet.size()-1;

      // choose the best unfolding:
      vector<unsigned int> pairingVars_list(currentDim);
      unsigned int cc = 0;
      unsigned int jBest = 0;
      double Lbest = 0.0;
      unsigned int rank_to_pair = 0;
      vector<double> S_pair;
      vector<vec> U_pair;
      for(unsigned int jVar=1; jVar<varSet.size(); jVar++){ // varSet[0] = iBest!
        unsigned int id_of_var = varSet[jVar];
        cout << "Trying var " << varSet[0] << " with " << id_of_var << endl;
        pairingVars_list[cc] = id_of_var;

        fullTensor tmp = pairVars(resCopy, 0, jVar);

        vector<double> S_0;
        vector<vec> U_0;
        tmp.computeUnfoldingSVD(0, S_0, U_0, DBL_EPSILON);
        unsigned int uSize = tmp.nDof_var(0);
        unsigned int vSize = 1.0;
        for(unsigned int jVar=1; jVar<tmp.nVar(); jVar++){
          vSize *= tmp.nDof_var(jVar);
        }

        double L = 0.0;
        unsigned int rank_unf = 0; //
        compute_L(S_0, uSize, vSize, L, rank_unf);

        cout << "Unfolding " << cc << ",  L = " << L << endl;
        if(L>Lbest){
          Lbest = L;
          jBest = cc;
          rank_to_pair = rank_unf;
          for(unsigned int iMod=0; iMod<U_pair.size(); iMod++){
            U_pair[iMod].clear();
          }
          U_pair.resize(rank_to_pair);
          S_pair.resize(rank_to_pair);
          for(unsigned int iS=0; iS<rank_to_pair; iS++){
            S_pair[iS] = S_0[iS];
            U_pair[iS].copyVecFrom(U_0[iS]);
          }
        }
        else{
          for(unsigned int iMod=0; iMod<U_0.size(); iMod++){
            U_0[iMod].clear();
          }
          U_0.clear();
          S_0.clear();
        }
        // free the memory:
        tmp.clear();
        cc += 1;
      }

      unsigned int varPairedWith_iBest = pairingVars_list[jBest];

      cout << "Variable to be chained is " << pairingVars_list[jBest] << " and rank is " << rank_to_pair << endl;

      // in-place reshaping of the resCopy tensor:
      inplace_pairVars(resCopy, 0, jBest+1);
      cout << "residual copy tensor is now of size: \n";
      resCopy.print();

      // reshape the modes to put them in the core:
      vector<vector<vec> > modes_table(rank);
      for(unsigned int iMin=0; iMin<rank; iMin++){
        modes_table[iMin].resize(rank_to_pair);
      }
      for(unsigned int iPlu=0; iPlu<rank_to_pair; iPlu++){
        unsigned int unfDofCount = 0;
        for(unsigned int iMin=0; iMin<rank; iMin++){
          vec fibre(m_nDof_var[varPairedWith_iBest], m_comm);
          for(unsigned int iDof=0; iDof<m_nDof_var[varPairedWith_iBest]; iDof++){
            double value = U_pair[iPlu].getVecEl(unfDofCount);
            if(fabs(value)>DBL_EPSILON){
              fibre(iDof) = value;
            }
            unfDofCount += 1;
          }
          fibre.finalize();
          (modes_table[iMin])[iPlu] = fibre;
        }
      }

      //fibreTrain.push_back(modes_table);
      order_of_variables[counter] = varPairedWith_iBest;
      fibreTrain[counter] = modes_table;
      counter += 1;

      // update ranks at the end of the cycle...iBest stays the same!
      rank = rank_to_pair;
      rank_to_pair = 1;

      // remove the varPaired from the set of variables to be considered:
      varSet.erase(varSet.begin()+jBest+1);

      // if varSet size is 2 this means that the right modes times the singular values are the ending core:
      if(varSet.size()==2){
        const unsigned int lastVar = varSet[1];
        order_of_variables[m_nVar-1] = varSet[1];

        contractTensorNoUnfold(resCopy, 0, U_pair);

        // at this point rank = rank_to_pair
        vector<vector<vec> > endingCore(rank);
        for(unsigned int iMin=0; iMin<rank; iMin++){
          endingCore[iMin].resize(1);
          vec mod(m_nDof_var[lastVar], m_comm);
          for(unsigned int iDof=0; iDof<m_nDof_var[lastVar]; iDof++){
            vector<unsigned int> ind(2);
            if(resCopy.nDof_var(0) == rank){
              ind = {iMin, iDof};
            }
            else{
              ind = {iDof, iMin};
            }
            double value = resCopy.eval(ind);
            if(fabs(value) > DBL_EPSILON){mod.setVecEl(iDof,value);} // equivalent to: mod(iDof) = value;
          }
          mod.finalize();
          endingCore[iMin][0] = mod;
        }
        // putting the ending core in the table:
        fibreTrain[m_nVar-1] = endingCore;
      }
      else{
        contractTensorNoUnfold(resCopy, 0, U_pair);
        //resCopy.print();
      }

      // free the memory:
      for(unsigned int iMod=0; iMod<rank_to_pair; iMod++){
        U_pair[iMod].clear();
      }
      U_pair.clear();

    }
    resCopy.clear();

    // updating the table of the variables order.
    m_var_order.push_back(order_of_variables);

    // assemble the tensor train and put it into m_terms;
    TensorTrain termToAdd(fibreTrain);
    m_terms.push_back(termToAdd);
    m_nTerms += 1;

  } // end else

}



/* II.7 contract a full tensor with a set of modes, in place
  - input: the fullTensor, the index along which to contract, the set of modes
  - output: the fullTensor is modified
*/
void SoTT::contractTensor(fullTensor& T, unsigned int id, vector<vec>& modes){
  const unsigned int nMod = modes.size();
  const unsigned int nDof = modes[0].size();
  // we use a function in fullTensor:
  mat modMatrixT(nMod, nDof, m_comm);
  for(unsigned int iRow=0; iRow<nMod; iRow++){
    for(unsigned int iCol=0; iCol<nDof; iCol++){
      double value = modes[iRow].getVecEl(iCol);
      if(fabs(value)>DBL_EPSILON){modMatrixT.setMatEl(iRow,iCol,value);}
    }
  }
  modMatrixT.finalize();
  T.inPlaceMatTensProd(id, modMatrixT);
  modMatrixT.clear();
}

/* II.7.(ii) contract tensor without computing explicitly the unfolding:
  - input: a full tensor, a set of modes, the direction along with we contract
  - output: the full tensor is modified.
*/
void SoTT::contractTensorNoUnfold(fullTensor& T, unsigned int id, vector<vec>& modes){
  const unsigned int nMod = modes.size();
  const unsigned int nDof = modes[0].size();

  T.inplaceModeIContraction(id, modes);
}


/* II.8 pair variables
  - input: a full Tensor, the first variable, the second variable
  - output: a full Tensor which is the reshaping of the original one
*/
fullTensor SoTT::pairVars(fullTensor& T, unsigned int idV_1, unsigned int idV_2){
  assert(idV_1 != idV_2);
  const unsigned int dim = T.nVar() - 1;
  vector<unsigned int> dofPerDim(dim);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
    if( (iVar != idV_1) && (iVar != idV_2) ){
      dofPerDim[cc] = T.nDof_var(iVar);
      cc += 1;
    }
    else{
      if( iVar == idV_1 ){
        dofPerDim[cc] = T.nDof_var(idV_1) * T.nDof_var(idV_2);
        cc += 1;
      }
    }
  }


  fullTensor toBeReturned(dim, dofPerDim, T.comm());
  for(unsigned int iEn=0; iEn<T.nEntries(); iEn++){
    double value = T.tensorEntries().getVecEl(iEn);
    if(fabs(value)>DBL_EPSILON){
      vector<unsigned int> subT = T.lin2sub(iEn);
      vector<unsigned int> subR(dim);
      unsigned int cc = 0;
      for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
        if( (iVar != idV_1) && (iVar != idV_2) ){
          subR[cc] = subT[iVar];
          cc += 1;
        }
        else{
          if( iVar == idV_1 ){
            subR[cc] = subT[idV_2] + T.nDof_var(idV_2)*subT[idV_1];
            cc += 1;
          }
        }
      }
      unsigned int lin = toBeReturned.sub2lin(subR);
      toBeReturned.set_tensorElement(lin, value);
    }
  }
  toBeReturned.finalize();

  return toBeReturned;
}

/* II.8.b pair variables, in-place version
  - input: a full Tensor, the first variable, the second variable
  - output: a full Tensor which is the reshaping of the original one
*/
void SoTT::inplace_pairVars(fullTensor& T, unsigned int idV_1, unsigned int idV_2){
  assert(idV_1 != idV_2);
  const unsigned int dim = T.nVar() - 1;
  vector<unsigned int> dofPerDim(dim);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
    if( (iVar != idV_1) && (iVar != idV_2) ){
      dofPerDim[cc] = T.nDof_var(iVar);
      cc += 1;
    }
    else{
      if( iVar == idV_1 ){
        dofPerDim[cc] = T.nDof_var(idV_1) * T.nDof_var(idV_2);
        cc += 1;
      }
    }
  }

  fullTensor empty(dim, dofPerDim, T.comm());
  vec entriesRemappedCopy(T.nEntries(), T.comm());
  for(unsigned int iEn=0; iEn<T.nEntries(); iEn++){
    double value = T.tensorEntries().getVecEl(iEn);
    if(fabs(value)>DBL_EPSILON){
      vector<unsigned int> subT = T.lin2sub(iEn);
      vector<unsigned int> subR(dim);
      unsigned int cc = 0;
      for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
        if( (iVar != idV_1) && (iVar != idV_2) ){
          subR[cc] = subT[iVar];
          cc += 1;
        }
        else{
          if( iVar == idV_1 ){
            subR[cc] = subT[idV_2] + T.nDof_var(idV_2)*subT[idV_1];
            cc += 1;
          }
        }
      }
      unsigned int lin = empty.sub2lin(subR);
      entriesRemappedCopy.setVecEl(lin, value);
    }
  }
  entriesRemappedCopy.finalize();
  T.clear();
  T.set_nVar(dim);
  T.set_nDof_var(dofPerDim);
  T.compute_minc();
  T.set_tensorEntries(entriesRemappedCopy);
}


/* II.9 update residual:
  - input: the residual in the full tensor format, the i-th term to be subtracted
  - output: the updated residual
*/
double SoTT::update_residual(fullTensor& residual, unsigned int iTerm){
  double resNorm = residual.tensorEntries().norm();
  cout << "Initial residual norm = " << resNorm << endl;
  for(unsigned int iEn=0; iEn<residual.nEntries(); iEn++){
    vector<unsigned int> ind = residual.lin2sub(iEn);
    double value = residual.tensorEntries().getVecEl(iEn);
    // mapping the indices in the order of the TT term:
    vector<unsigned int> ind_TT(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      unsigned int id = m_var_order[iTerm][iVar];
      ind_TT[iVar] = ind[id];
    }
    double toSubtract = m_terms[iTerm].eval(ind_TT);
    value = value - toSubtract;
    residual.set_tensorElement(iEn, value);
  }
  resNorm = residual.tensorEntries().norm();
  cout << "Residual norm = " << resNorm << endl;
  return resNorm;
}
