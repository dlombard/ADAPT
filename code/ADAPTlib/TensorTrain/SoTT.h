#ifndef SoTT_h
#define SoTT_h

#include "../genericInclude.h"
#include "../tensor.h"
#include "../linAlg/linAlg.h"
#include "../fullTensor/fullTensor.h"
#include "../CP/CP.h"
#include "TensorTrain.h"


// declaration of the class SoTT
class SoTT : public tensor{
private:
  vector<TensorTrain> m_terms;
  unsigned int m_nTerms;
  vector<vector<unsigned int> > m_var_order;
  double m_tol;
  string m_saveName;

public:
  // constructor and destructor:
  SoTT(){};
  ~SoTT(){};
  void clear(){
    for(unsigned int iTerm=0; iTerm<m_nTerms; iTerm++){
      m_terms[iTerm].clear();
    }
  }
  // Overloaded constructors:
  SoTT(vector<unsigned int>, MPI_Comm);
  SoTT(unsigned int, vector<unsigned int>, MPI_Comm);


  // Methods of the class:
  unsigned int estimateRank(vector<svd>&, unsigned int);
  void chooseUnfolding(vector<svd>&, unsigned int&, unsigned int&);
  void compute_L(svd&, double&, unsigned int&);
  void compute_L(vector<double>&, unsigned int&, unsigned int&, double&, unsigned int&);

  // Methods related to a CP input tensor:
  void CP2SoTT(CPTensor&, double);
  CPTensor pairVars(CPTensor&, unsigned int, unsigned int);
  void inplace_pairVars(CPTensor&, unsigned int, unsigned int);
  void contractTensor(CPTensor&, unsigned int, vector<vec>&);
  void compute_TT(CPTensor&);
  void compute_TT_fast(CPTensor&);
  double update_residual(CPTensor&, unsigned int);
  // Methods related to a full tensor input:
  void full2SoTT(fullTensor&, double, bool);
  void compute_TT(fullTensor&);
  void compute_TT_fast(fullTensor&);
  void compute_TT_fast(fullTensor&, bool);
  void contractTensor(fullTensor&, unsigned int, vector<vec>&);
  void contractTensorNoUnfold(fullTensor&, unsigned int, vector<vec>&);
  fullTensor pairVars(fullTensor&, unsigned int, unsigned int);
  void inplace_pairVars(fullTensor&, unsigned int, unsigned int);
  double update_residual(fullTensor&, unsigned int);

  // ACCESS FUNCTIONS:
  inline vector<TensorTrain> terms(){return m_terms;}
  inline TensorTrain terms(unsigned int iTerm){assert(iTerm<m_nTerms); return m_terms[iTerm];}
  inline unsigned int nTerms(){return m_nTerms;}
  inline vector<vector<unsigned int> > var_order(){return m_var_order;}
  inline vector<unsigned int> var_order(unsigned int iTerm){assert(iTerm<m_nTerms);return m_var_order[iTerm];}
  inline unsigned int var_order(unsigned int iTerm, unsigned int iVar){
    assert(iTerm<m_nTerms);
    assert(iVar<m_nVar);
    return m_var_order[iTerm][iVar];
  }
  inline double tol(){return m_tol;}
  inline string saveName(){return m_saveName;}

  // SETTERS:
    inline void set_saveName(string fileName){m_saveName = fileName;}

  // Auxiliary functions:
  inline unsigned int memory(){
    unsigned int mem = 0;
    for(unsigned int iTerm=0; iTerm<m_nTerms; iTerm++){
      unsigned int toAdd = m_terms[iTerm].memory();
      mem += toAdd;
    }
    return mem;
  }

};

#endif
