// Implementation of the Tensor Train Class

#include "TensorTrain.h"


// I -- CONSTRUCTORS --

/* overloaded constructors
  - input: degrees of freedom per direction and communicator
  - output: initialising TT
*/
TensorTrain::TensorTrain(vector<unsigned int> resPerDim, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = resPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = resPerDim[iVar];
  }
  compute_minc();
  m_train.resize(m_nVar);
  m_ranks.resize(m_nVar-1);
  m_trainSize.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_trainSize[iVar].resize(2);
  }
  m_trainSize[0][0] = 1;
  m_trainSize[m_nVar-1][1] = 1;
  m_train[0].resize(1);
  m_isInitialised = true;
}


/* overloaded constructors
  - input: number of variables degrees of freedom per direction and communicator
  - output: initialising TT
*/
TensorTrain::TensorTrain(unsigned int numVar, vector<unsigned int> resPerDim, MPI_Comm theComm){
  assert(numVar == resPerDim.size());
  m_comm = theComm;
  m_nVar = numVar;
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = resPerDim[iVar];
  }
  compute_minc();
  m_train.resize(m_nVar);
  m_ranks.resize(m_nVar-1);
  m_trainSize.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_trainSize[iVar].resize(2);
  }
  m_trainSize[0][0] = 1;
  m_trainSize[m_nVar-1][1] = 1;
  m_train[0].resize(1);
  m_isInitialised = true;
}


/* overloaded constructors
  - input: degrees of freedom per direction, TT-ranks and communicator
  - output: initialising TT
*/
TensorTrain::TensorTrain(vector<unsigned int> resPerDim, vector<unsigned int> TTranks, MPI_Comm theComm){
  assert(TTranks.size() == resPerDim.size()-1);
  m_comm = theComm;
  m_nVar = resPerDim.size();
  m_nDof_var.resize(m_nVar);
  m_train.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = resPerDim[iVar];
  }
  m_ranks.resize(m_nVar-1);
  for(unsigned int iVar=0; iVar<m_nVar-1; iVar++){
    m_ranks[iVar] = TTranks[iVar];
  }

  compute_minc();

  // setting up train size:
  m_trainSize.resize(m_nVar);
  unsigned int iVar=0;
  m_trainSize[iVar].resize(2);
  m_trainSize[iVar][0] = 1;
  m_trainSize[iVar][1] = TTranks[0];
  for(unsigned int iVar=1; iVar<m_nVar-1; iVar++){
    m_trainSize[iVar][0] = TTranks[iVar-1];
    m_trainSize[iVar][1] = TTranks[iVar];
  }
  iVar = m_nVar-1;
  m_trainSize[iVar][0] = TTranks[iVar-1];
  m_trainSize[iVar][1] = 1;

  // resizing the train:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    unsigned int I = m_trainSize[iVar][0];
    m_train[iVar].resize(I);
    for(unsigned int iM = 0; iM<I; iM++){
      unsigned int J = m_trainSize[iVar][1];
      m_train[iVar][iM].resize(J);
    }
  }
  m_isInitialised = true;
}


/* overloaded constructors
  - input: the data of the TT
  - output: initialising TT
*/
TensorTrain::TensorTrain(vector<vector<vector<vec> > >& theTT){
  m_comm = theTT[0][0][0].comm();
  m_nVar = theTT.size();
  m_nDof_var.resize(m_nVar);
  m_trainSize.resize(m_nVar);
  m_ranks.resize(m_nVar-1);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = theTT[iVar][0][0].size();
    m_trainSize[iVar].resize(2);
    m_trainSize[iVar][0] = theTT[iVar].size();
    m_trainSize[iVar][1] = theTT[iVar][0].size();
    if(iVar!=m_nVar-1){
      m_ranks[iVar] = m_trainSize[iVar][1];
    }
  }
  compute_minc();
  m_train = theTT;
  m_isInitialised = true;
}


// init (overloaded constructor for existing empty initialised)
void TensorTrain::init(vector<unsigned int> resPerDim, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = resPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = resPerDim[iVar];
  }
  compute_minc();
  m_train.resize(m_nVar);
  m_ranks.resize(m_nVar-1);
  m_trainSize.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_trainSize[iVar].resize(2);
  }
  m_trainSize[0][0] = 1;
  m_trainSize[m_nVar-1][1] = 1;
  m_train[0].resize(1);
  m_isInitialised = true;
}



// -- METHODS implementing generic operations on Tensor Train --


/* Copy the tensor structure:
  - input: the Tensor Train to be copied
  - output: the structure of this tensor is copied.
*/
void TensorTrain::copyTensorStructFrom(TensorTrain& source){
  //if(m_isInitialised){clear();}
  m_comm = source.comm();
  m_nVar = source.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = source.nDof_var(iVar);
  }
  compute_minc();
  m_train.resize(m_nVar);
  m_ranks.resize(m_nVar-1);
  m_trainSize.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_trainSize[iVar].resize(2);
  }
  m_trainSize[0][0] = 1;
  m_trainSize[m_nVar-1][1] = 1;
  //m_train[0].resize(1);
  m_isInitialised = true;
}


/* Copy the tensor from a given Tensor Train:
  - input: the tensor to be copied
  - output: the tensor (structure + copy of the entries)
*/
void TensorTrain::copyTensorFrom(TensorTrain& source){
  copyTensorStructFrom(source);

  for(unsigned int iR=0; iR<m_nVar-1; iR++){
    m_ranks[iR] = source.ranks(iR);
  }
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int I=0; I<2; I++){
      m_trainSize[iVar][I] = source.trainSize(iVar,I);
    }
  }

  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_train[iVar].resize(m_trainSize[iVar][0]);
    for(unsigned int I=0; I<m_trainSize[iVar][0]; I++){
      m_train[iVar][I].resize(m_trainSize[iVar][1]);
      for(unsigned int J=0; J<m_trainSize[iVar][1]; J++){
        vec trainEntry = source.train(iVar,I,J);
        vec fiber;
        fiber.copyVecFrom(trainEntry);
        m_train[iVar][I][J] = fiber;
      }
    }
  }
}


/* check if this tensor has the same structure of a given tensor
  - input: the tensor to compare with
  - output: true if if has the same structure, false otherwise
*/

bool TensorTrain::hasSameStructure(TensorTrain& toBeComparedTo){
    bool toBeReturned = true;
    if(m_nVar != toBeComparedTo.nVar()){
      return false;
    }
    else{
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        if(m_nDof_var[idVar] != toBeComparedTo.nDof_var(idVar)){
          return false;
        }
      }
    }
    return toBeReturned;
}


/* check if this TT is equal to a given one:
  - input: a TT to be compared
  - output: a bool
*/
bool TensorTrain::isEqualTo(TensorTrain& source){
  bool isIt = hasSameStructure(source);
  if(isIt){
    // check ranks:
    for(unsigned int iVar=0; iVar<m_nVar-1; iVar++){
      if(m_ranks[iVar] != source.ranks(iVar)){
        isIt=false;
        break;
      }
    }
    if(isIt){
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        for(unsigned int I=0; I<m_trainSize[iVar][0]; I++){
          for(unsigned int J=0; J<m_trainSize[iVar][1]; J++){
            if(!(m_train[iVar][I][J]== source.train(iVar,I,J))){
              isIt = false;
            }
          }
        }
      }
    }
  }
  return isIt;
}

// overloaded equality:
bool TensorTrain::operator == (TensorTrain& source){
  return isEqualTo(source);
}


/* sum between this tt and a rescaled one: T <-- T + alpha S
  - input: a TT, a scalar alpha, a bool (copy it or not)
  - output: this TT is changed
*/
void TensorTrain::sum(TensorTrain& source, double alpha, bool COPY){
  if(hasSameStructure(source)){

    // check the ranks:
    bool thisIsZero = false;
    for(unsigned int iVar=0; iVar<m_nVar-1; iVar++){
      if(m_ranks[iVar]==0){
        thisIsZero = true;
        break;
      }
    }

    bool addendIsZero = false;
    for(unsigned int iVar=0; iVar<m_nVar-1; iVar++){
      if(source.ranks(iVar)==0){
        addendIsZero = true;
        break;
      }
    }

    if(thisIsZero){
      copyTensorFrom(source);
      multiplyByScalar(alpha);
    }
    else if (!addendIsZero){

    // copy the current train-size:
    vector<vector<unsigned int> > current_trainSize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      current_trainSize[iVar].resize(2);
      current_trainSize[iVar][0] = m_trainSize[iVar][0];
      current_trainSize[iVar][1] = m_trainSize[iVar][1];
    }
    // the rank is the sum of the ranks:
    for(unsigned int iVar=0; iVar<m_nVar-1; iVar++){
      m_ranks[iVar] += source.ranks(iVar);
    }
    // update the train size:
    m_trainSize[0][1] += source.trainSize(0,1);
    for(unsigned int iVar=1; iVar<m_nVar-1; iVar++){
      m_trainSize[iVar][0] += source.trainSize(iVar,0);
      m_trainSize[iVar][1] += source.trainSize(iVar,1);
    }
    m_trainSize[m_nVar-1][0] += source.trainSize(m_nVar-1,0);
    // resize the train and fill the entries:
    // first variable:
    m_train[0][0].resize(m_trainSize[0][1]);
    for(unsigned int J=current_trainSize[0][1]; J<m_trainSize[0][1]; J++){
      if(!COPY){
        m_train[0][0][J] = source.train(0,0, J-current_trainSize[0][1]);
      }
      else{
        vec fiber;
        fiber.copyVecFrom(source.train(0,0, J-current_trainSize[0][1]));
        m_train[0][0][J] = fiber;
      }
    }

    // from second to last-1:
    for(unsigned int iVar=1; iVar<m_nVar-1; iVar++){
      m_train[iVar].resize(m_trainSize[iVar][0]);
      for(unsigned int I=0; I<m_trainSize[iVar][0]; I++){
        m_train[iVar][I].resize(m_trainSize[iVar][1]);
        for(unsigned int J=0; J<m_trainSize[iVar][1]; J++){
          if( (I<current_trainSize[iVar][0]) && (J>=current_trainSize[iVar][1])){
            vec empty(m_nDof_var[iVar], m_comm);
            m_train[iVar][I][J] = empty;
          }
          if( (I>=current_trainSize[iVar][0]) && (J<current_trainSize[iVar][1])){
            vec empty(m_nDof_var[iVar], m_comm);
            m_train[iVar][I][J] = empty;
          }
          if( (I>=current_trainSize[iVar][0]) && (J>=current_trainSize[iVar][1])){
            if(!COPY){
              m_train[iVar][I][J] = source.train(iVar, I-current_trainSize[iVar][0], J-current_trainSize[iVar][1]);
            }
            else{
              vec fiber;
              fiber.copyVecFrom(source.train(iVar, I-current_trainSize[iVar][0], J-current_trainSize[iVar][1]));
              m_train[iVar][I][J] = fiber;
            }
          }
        }
      }
    }

    // last variable:
    m_train[m_nVar-1].resize(m_trainSize[m_nVar-1][0]);
    for(unsigned int I=current_trainSize[m_nVar-1][0]; I<m_trainSize[m_nVar-1][0]; I++){
      m_train[m_nVar-1][I].resize(1);
      if(!COPY){
        m_train[m_nVar-1][I][0] = source.train(m_nVar-1, I-current_trainSize[m_nVar-1][0], 0);
        m_train[m_nVar-1][I][0] *= alpha;
      }
      else{
        vec fiber;
        fiber.copyVecFrom(source.train(m_nVar-1, I-current_trainSize[m_nVar-1][0], 0) ) ;
        m_train[m_nVar-1][I][0] = fiber;
        m_train[m_nVar-1][I][0] *= alpha;
      }
    }
    // end else if
  }

  }
  else{
    PetscPrintf(m_comm, "Cannot be summed!\n");
    exit(1);
  }
}


// overloaded operator for sum:
void TensorTrain::operator += (TensorTrain& source){
  sum(source, 1.0, true);
}


/* multiply the given TT by a scalar:
- input: the scalar
- output: this TT is multiplied by the given scalar
*/
void  TensorTrain::multiplyByScalar(double alpha){
  double abs_alpha = fabs(alpha);
  double sign = 1.0;
  if(alpha<0){sign = -1.0;}
  const double factor = pow(abs_alpha, 1.0/(m_nVar));
  for(unsigned int iVar=0; iVar<m_nVar-1; iVar++){
    for(unsigned int i=0; i<m_trainSize[iVar][0]; i++){
      for(unsigned int j=0; j<m_trainSize[iVar][1]; j++){
        m_train[iVar][i][j] *= factor;
      }
    }
  }
  // last variable: put the sign
  for(unsigned int I=0; I<m_trainSize[m_nVar-1][0]; I++){
    m_train[m_nVar-1][I][0] *= (sign * factor);
  }
}

// overloaded to multiply by a scalar:
void TensorTrain::operator *= (double alpha){
  double abs_alpha = fabs(alpha);
  double sign = 1.0;
  if(alpha<0){sign = -1.0;}
  const double factor = pow(abs_alpha, 1.0/(m_nVar));

  for(unsigned int iVar=0; iVar<m_nVar-1; iVar++){
    for(unsigned int i=0; i<m_trainSize[iVar][0]; i++){
      for(unsigned int j=0; j<m_trainSize[iVar][1]; j++){
        m_train[iVar][i][j] *= factor;
        //m_train[iVar][i][j].print();
      }
    }
  }
  // last variable: put the sign
  for(unsigned int I=0; I<m_trainSize[m_nVar-1][0]; I++){
    m_train[m_nVar-1][I][0] *= (sign * factor);
    //m_train[m_nVar-1][I][0] *= (alpha);
  }
  /*for(unsigned int J=0; J<m_trainSize[0][1]; J++){
    m_train[0][0][J] *= (alpha);
  }*/
}


/* shift the given TT by a scalar:
- input: the scalar
- output: this TT is shifted by the given scalar
*/
void TensorTrain::shiftByScalar(double alpha){
  // copy the current train-size:
  vector<vector<unsigned int> > current_trainSize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    current_trainSize[iVar].resize(2);
    current_trainSize[iVar][0] = m_trainSize[iVar][0];
    current_trainSize[iVar][1] = m_trainSize[iVar][1];
  }
  // the rank is the sum of the ranks:
  for(unsigned int iVar=0; iVar<m_nVar-1; iVar++){
    m_ranks[iVar] += 1;
  }
  // update the train size:
  m_trainSize[0][1] += 1;
  for(unsigned int iVar=1; iVar<m_nVar-1; iVar++){
    m_trainSize[iVar][0] += 1;
    m_trainSize[iVar][1] += 1;
  }
  m_trainSize[m_nVar-1][0] += 1;
  // resize the train and fill the entries:
  // first variable:
  m_train[0][0].resize(m_trainSize[0][1]);
  for(unsigned int J=current_trainSize[0][1]; J<m_trainSize[0][1]; J++){
    vec fiber(m_nDof_var[0], m_comm);
    fiber.ones();
    m_train[0][0][J] = fiber;
  }

  // from second to last-1:
  for(unsigned int iVar=1; iVar<m_nVar-1; iVar++){
    m_train[iVar].resize(m_trainSize[iVar][0]);
    for(unsigned int I=0; I<m_trainSize[iVar][0]; I++){
      m_train[iVar][I].resize(m_trainSize[iVar][1]);
      for(unsigned int J=0; J<m_trainSize[iVar][1]; J++){
        if( (I<current_trainSize[iVar][0]) && (J>=current_trainSize[iVar][1])){
          vec empty(m_nDof_var[iVar], m_comm);
          m_train[iVar][I][J] = empty;
        }
        if( (I>=current_trainSize[iVar][0]) && (J<current_trainSize[iVar][1])){
          vec empty(m_nDof_var[iVar], m_comm);
          m_train[iVar][I][J] = empty;
        }
        if( (I>=current_trainSize[iVar][0]) && (J>=current_trainSize[iVar][1])){
          vec fiber(m_nDof_var[iVar], m_comm);
          fiber.ones();
          m_train[iVar][I][J] = fiber;
        }
      }
    }
  }

  // last variable:
  m_train[m_nVar-1].resize(m_trainSize[m_nVar-1][0]);
  for(unsigned int I=current_trainSize[m_nVar-1][0]; I<m_trainSize[m_nVar-1][0]; I++){
    m_train[m_nVar-1][I].resize(1);
    vec fiber(m_nDof_var[m_nVar-1], m_comm);
    fiber.ones();
    m_train[m_nVar-1][I][0] = fiber;
    m_train[m_nVar-1][I][0] *= alpha;
  }
}


// overloaded operator:
void TensorTrain::operator += (double alpha){
  shiftByScalar(alpha);
}


/* Extract subtensor, contiguous indices:
  - input: indices bounds, C convention
  - output: given tensor is filled
*/
void TensorTrain::extractSubtensor(vector<unsigned int> indBounds, TensorTrain& subTensor){
  assert(indBounds.size() == 2*m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    resPerDim[iVar] = indBounds[2*iVar+1] - indBounds[2*iVar];
  }
  subTensor.init(resPerDim, m_comm);
  subTensor.set_ranks(m_ranks);
  subTensor.resizeTrain();
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int I=0; I<m_trainSize[iVar][0]; I++){
      for(unsigned int J=0; J<m_trainSize[iVar][1]; J++){
        vec fiber(resPerDim[iVar], m_comm);
        for(unsigned int iDof=0; iDof<resPerDim[iVar]; iDof++){
          unsigned int globInd = iDof + indBounds[2*iVar];
          double val = m_train[iVar][I][J].getVecEl(globInd);
          fiber.setVecEl(iDof, val);
        }
        fiber.finalize();
        subTensor.set_train(iVar, I, J, fiber);
      }
    }
  }
}


/* Extract subtensor, contiguous indices:
  - input: indices bounds, C convention
  - output: a TT tensor
*/
TensorTrain TensorTrain::extractSubtensor(vector<unsigned int> indBounds){
  assert(indBounds.size() == 2*m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    resPerDim[iVar] = indBounds[2*iVar+1] - indBounds[2*iVar];
  }
  TensorTrain subTensor(resPerDim, m_comm);
  subTensor.set_ranks(m_ranks);
  subTensor.resizeTrain();
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int I=0; I<m_trainSize[iVar][0]; I++){
      for(unsigned int J=0; J<m_trainSize[iVar][1]; J++){
        vec fiber(resPerDim[iVar], m_comm);
        for(unsigned int iDof=0; iDof<resPerDim[iVar]; iDof++){
          unsigned int globInd = iDof + indBounds[2*iVar];
          double val = m_train[iVar][I][J].getVecEl(globInd);
          fiber.setVecEl(iDof, val);
        }
        fiber.finalize();
        subTensor.set_train(iVar, I, J, fiber);
      }
    }
  }
  return subTensor;
}

/* Extract subtensor, non-contiguous indices:
  - input: list of indices (dofvar1; dofvar2; ...), subtensor pointer
  - output: the subtensor is filled
*/
void TensorTrain::extractSubtensor(vector<vector<unsigned int> > indList, TensorTrain& subTensor){
  assert(indList.size()==m_nVar);

  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    resPerDim[iVar] = indList[iVar].size();
  }
  subTensor.init(resPerDim, m_comm);
  subTensor.set_ranks(m_ranks);
  subTensor.resizeTrain();
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int I=0; I<m_trainSize[iVar][0]; I++){
      for(unsigned int J=0; J<m_trainSize[iVar][1]; J++){
        vec fiber(resPerDim[iVar], m_comm);
        for(unsigned int iDof=0; iDof<resPerDim[iVar]; iDof++){
          unsigned int globInd = indList[iVar][iDof];
          double value = m_train[iVar][I][J].getVecEl(globInd);
          fiber.setVecEl(iDof, value);
        }
        fiber.finalize();
        subTensor.set_train(iVar, I, J, fiber);
      }
    }
  }
}


/* Extract subtensor, non-contiguous indices:
  - input: list of indices (dofvar1; dofvar2; ...)
  - output: a TT subtensor
*/
TensorTrain TensorTrain::extractSubtensor(vector<vector<unsigned int> > indList){
  assert(indList.size()==m_nVar);

  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    resPerDim[iVar] = indList[iVar].size();
  }

  TensorTrain subTensor(resPerDim, m_comm);
  subTensor.set_ranks(m_ranks);
  subTensor.resizeTrain();
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int I=0; I<m_trainSize[iVar][0]; I++){
      for(unsigned int J=0; J<m_trainSize[iVar][1]; J++){
        vec fiber(resPerDim[iVar], m_comm);
        for(unsigned int iDof=0; iDof<resPerDim[iVar]; iDof++){
          unsigned int globInd = indList[iVar][iDof];
          double value = m_train[iVar][I][J].getVecEl(globInd);
          fiber.setVecEl(iDof, value);
        }
        fiber.finalize();
        subTensor.set_train(iVar, I, J, fiber);
      }
    }
  }
  return subTensor;
}


/* Assign subtensor, contiguous indices:
  - input: indices bound, C notation, the subtensor to be assigned
  - output: the subtensor is assigned to this TT
*/
void TensorTrain::assignSubtensor(vector<unsigned int> indBounds, TensorTrain& subTensor){
  assert(indBounds.size() == 2*m_nVar);
  // compute the subtensor to be added
  TensorTrain currentSubTens = extractSubtensor(indBounds);
  currentSubTens.multiplyByScalar(-1.0);
  currentSubTens.sum(subTensor, 1.0, true);

  // compute the zero extension:
  TensorTrain toBeAdded(m_nDof_var, m_comm);
  toBeAdded.set_ranks(currentSubTens.ranks());
  toBeAdded.resizeTrain();
  // fill the TT:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int I=0; I<currentSubTens.trainSize(iVar,0); I++){
      for(unsigned int J=0; J<currentSubTens.trainSize(iVar,1); J++){
        vec fiber(m_nDof_var[iVar], m_comm);
        for(unsigned int iSub=0; iSub<currentSubTens.nDof_var(iVar); iSub++){
          unsigned int globInd = iSub + indBounds[2*iVar];
          double val = currentSubTens.train(iVar,I,J).getVecEl(iSub);
          fiber.setVecEl(globInd, val);
        }
        fiber.finalize();
        toBeAdded.set_train(iVar,I,J, fiber);
      }
    }
  }
  // sum the zero extension to the current:
  sum(toBeAdded, 1.0, true);

  //free the memory:
  toBeAdded.clear();
  currentSubTens.clear();
}





// II - METHODS SPECIFIC to TT:

/* left multiplication of one of the cores by a matrix:
- input: the variable id, the matrix ( variable cannot be 0!!!)
- output: the core is changed, in place C <- A C
*/
vector<vector<vec> > TensorTrain::leftMultiplyCoreByMatrix(unsigned int iVar, mat& A){
  assert(A.nCols() == m_trainSize[iVar][0]);
  assert(iVar>0);

  // init the core:
  vector<vector<vec> > core(A.nRows());
  for(unsigned int I=0; I<A.nRows(); I++){
    core.resize(m_trainSize[iVar][1]);
    for(unsigned int J=0; J<m_trainSize[iVar][1]; J++){
      core[I][J].init(m_nDof_var[iVar], m_comm);
    }
  }

  // filling the core:
  for(unsigned int iDof=0; iDof<m_nDof_var[iVar]; iDof++){
    mat slice = getCoreSlice(iVar, iDof);
    mat result = A * slice;
    for(unsigned int I=0; I<A.nRows(); I++){
      for(unsigned int J=0; J<m_trainSize[iVar][1]; J++){
        double val = result.getMatEl(I,J);
        core[I][J].setVecEl(iDof, val);
      }
    }
  }
  // finalizing the assembly:
  for(unsigned int I=0; I<A.nRows(); I++){
    for(unsigned int J=0; J<m_trainSize[iVar][1]; J++){
      core[I][J].finalize();
    }
  }
  return core;
}


/* right multiplication of one of the cores by a matrix:
- input: the variable id, the matrix ( variable cannot be 0!!!)
- output: the core is changed, in place C <- C A
*/
vector<vector<vec> > TensorTrain::rightMultiplyCoreByMatrix(unsigned int iVar, mat& A){
  assert(A.nRows() == m_trainSize[iVar][1]);
  assert(iVar<m_nVar-1);

  // init the core:
  vector<vector<vec> > core(m_trainSize[iVar][0]);
  for(unsigned int I=0; I<m_trainSize[iVar][0]; I++){
    core.resize(A.nCols());
    for(unsigned int J=0; J<A.nCols(); J++){
      core[I][J].init(m_nDof_var[iVar], m_comm);
    }
  }

  // filling the core:
  for(unsigned int iDof=0; iDof<m_nDof_var[iVar]; iDof++){
    mat slice = getCoreSlice(iVar, iDof);
    mat result = slice * A;
    for(unsigned int I=0; I<m_trainSize[iVar][0]; I++){
      for(unsigned int J=0; J<A.nCols(); J++){
        double val = result.getMatEl(I,J);
        core[I][J].setVecEl(iDof, val);
      }
    }
  }
  // finalizing the assembly:
  for(unsigned int I=0; I<m_trainSize[iVar][0]; I++){
    for(unsigned int J=0; J<A.nCols(); J++){
      core[I][J].finalize();
    }
  }
  return core;
}




// ---  TT-Svd --- //

// II.(i)  TT-Svd from a CP-Tensor

/* II.(i).1 given a CP and a Tolerance, obtain the TT-svd
  - input: a CPTensor, a tolerance
  - output: this TT is the TT-svd approximation of the input tensor
*/
void TensorTrain::TTsvd(CPTensor& T, double tol){
  if(!m_isInitialised){
    m_comm = T.comm();
    m_nVar = T.nVar();
    m_nDof_var.resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_nDof_var[iVar] = T.nDof_var(iVar);
    }
    compute_minc();
    m_train.resize(m_nVar);
    m_ranks.resize(m_nVar-1);
    m_trainSize.resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_trainSize[iVar].resize(2);
    }
    m_trainSize[0][0] = 1;
    m_trainSize[m_nVar-1][1] = 1;
    m_train[0].resize(1);
    m_isInitialised = true;
  }

  // tolerance squared/d-1
  const double epsSquared = tol*tol/(m_nVar-1);

  // compute the first svd unfolding:
  mat unf;
  T.computeUnfolding(0, unf);
  svd first(unf);
  const unsigned int numSingVal = first.Svec().size();
  unsigned int toRetain = first.Svec().size();
  double sum_S_2 = first.Svec().getVecEl(numSingVal-1) * first.Svec().getVecEl(numSingVal-1);
  unsigned int iS = 1;
  while(sum_S_2<=epsSquared){
    toRetain -= 1;
    sum_S_2 += first.Svec().getVecEl(numSingVal-1-iS) * first.Svec().getVecEl(numSingVal-1-iS);
    iS += 1;
  }

  // modes: first core has size 1 x rank:
  vector<vec> modes(toRetain);
  for(unsigned int iMod=0; iMod<toRetain; iMod++){
    modes[iMod].init(m_nDof_var[0], m_comm);
    for(unsigned int iDof=0; iDof<m_nDof_var[0]; iDof++){
      double value = first.Umat().getMatEl(iDof,iMod);
      if(fabs(value) > DBL_EPSILON){
        modes[iMod].setVecEl(iDof,value);
      }
    }
    modes[iMod].finalize();
  }
  // the first variable has size 1 x rank
  m_ranks[0] = toRetain;
  m_trainSize[0][1] = toRetain;
  m_train[0][0].resize(toRetain);
  m_train[0][0] = modes;
  // the next train will have the first size equal to toRetain:
  m_trainSize[1][0] = toRetain;
  m_train[1].resize(toRetain);

  // free the memory:
  first.clear();
  unf.clear();

  // copy the residual into a temporary object, not to modify the original tensor.
  CPTensor resCopy(m_nVar, m_nDof_var, m_comm);
  resCopy.copyTensorFrom(T);

  // contract the tensor:
  contractTensor(resCopy, 0, modes);
  cout << "After contraction: " << endl;
  resCopy.print();

  for(unsigned int iDim=1; iDim<m_nVar-1; iDim++){
    // pair the variable:
    inplace_pairVars(resCopy, 0, 1);

    // compute the unfolding and its svd:
    mat unf;
    resCopy.computeUnfolding(0, unf);
    svd unfSvd(unf);
    const unsigned int numSingVal = unfSvd.Svec().size();
    unsigned int toRetain = unfSvd.Svec().size();
    double sum_S_2 = unfSvd.Svec().getVecEl(numSingVal-1) * unfSvd.Svec().getVecEl(numSingVal-1);
    unsigned int iS = 1;
    while(sum_S_2<=epsSquared){
      toRetain -= 1;
      sum_S_2 += unfSvd.Svec().getVecEl(numSingVal-1-iS) * unfSvd.Svec().getVecEl(numSingVal-1-iS);
      iS += 1;
    }

    // assign the sizes:
    m_ranks[iDim] = toRetain;
    m_trainSize[iDim][1] = toRetain;
    m_trainSize[iDim+1][0] = toRetain;
    // assemble the core:
    for(unsigned int iMin=0; iMin<m_trainSize[iDim][0]; iMin++){
      m_train[iDim][iMin].resize(toRetain);
    }

    for(unsigned int iPlu=0; iPlu<toRetain; iPlu++){
      unsigned int dofCount = 0;
      for(unsigned int iMin=0; iMin<m_trainSize[iDim][0]; iMin++){
        vec mode(m_nDof_var[iDim], m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[iDim]; iDof++){
          double value = unfSvd.Umat().getMatEl(dofCount,iPlu);
          if(fabs(value)>DBL_EPSILON){
            mode.setVecEl(iDof,value);
          }
          dofCount += 1;
        }
        mode.finalize();
        m_train[iDim][iMin][iPlu] = mode;
      }
    }

    // contract the tensor:
    vector<vec> modes(toRetain);
    const unsigned int nDofModes = resCopy.nDof_var(0); // because 0 is iBest<-varSet[0]
    for(unsigned int iMod=0; iMod<toRetain; iMod++){
      modes[iMod].init(nDofModes, m_comm);
      for(unsigned int iDof=0; iDof<nDofModes; iDof++){
        double value = unfSvd.Umat().getMatEl(iDof,iMod);
        if(fabs(value) > DBL_EPSILON){modes[iMod].setVecEl(iDof,value);}
      }
      modes[iMod].finalize();
    }


    // if we are in the last unfolding, we finalize
    if(iDim==m_nVar-2){
      vector<vector<vec> > endingCore(toRetain);
      for(unsigned int iMin=0; iMin<toRetain; iMin++){
        endingCore[iMin].resize(1);
        vec mod(m_nDof_var[iDim+1], m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[iDim+1]; iDof++){
          double value = unfSvd.Vmat().getMatEl(iDof,iMin) * unfSvd.Svec().getVecEl(iMin);
          if(fabs(value) > DBL_EPSILON){mod.setVecEl(iDof,value);} // equivalent to: mod(iDof) = value;
        }
        mod.finalize();
        endingCore[iMin][0] = mod;
      }
      m_train[iDim+1] = endingCore;
    }
    else{
      //contractTensor(resCopy, iBest, modes); //iBest = varSet[0]
      contractTensor(resCopy, 0, modes);
    }

    // free the memory:
    for(unsigned int iMod=0; iMod<toRetain; iMod++){
      modes.clear();
    }
    modes.clear();
    unf.clear();
    unfSvd.clear();
  }
  resCopy.clear();
}


/* II.(i).2 tensor contraction for a CP
  - input: a CP tensor that will be modified, the direction, a set of modes.
  - output: the CP tensor is modified "in place".
*/
void TensorTrain::contractTensor(CPTensor& residual, unsigned int iBest, vector<vec>& modes){
  const unsigned int rank = modes.size();
  residual.change_nDof_var(iBest,rank);
  for(unsigned int iTerm=0; iTerm<residual.rank(); iTerm++){
    vec termToExchange(rank, residual.comm());
    vec t_0 = residual.terms(iTerm, iBest);
    for(unsigned int iDof=0; iDof<rank; iDof++){
      vec t_1 = modes[iDof];
      double value = scalProd(t_1, t_0);
      termToExchange(iDof) = value;
    }
    termToExchange.finalize();
    residual.replaceTerm(iTerm, iBest, termToExchange);
  }
}


/* II.(i).3 in-place variable pairing for a CP
  - input: a CP tensor, the two indices to pair
  - output: the CP tensor is modified "in place".
*/
void TensorTrain::inplace_pairVars(CPTensor& T, unsigned int idV_1, unsigned int idV_2){
  assert(idV_1 != idV_2);
  const unsigned int dim = T.nVar() - 1;
  vector<unsigned int> dofPerDim(dim);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
    if( (iVar != idV_1) && (iVar != idV_2) ){
      dofPerDim[cc] = T.nDof_var(iVar);
      cc += 1;
    }
    else{
      if( iVar == idV_1 ){
        dofPerDim[cc] = T.nDof_var(idV_1) * T.nDof_var(idV_2);
        cc += 1;
      }
    }
  }
  // these are the new dimension and dof per dimension of T, to be replaced in the end;
  for(unsigned int iTerm=0; iTerm<T.rank(); iTerm++){

    vector<vec> newTerm(dim);
    cc = 0;
    for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
      if( (iVar != idV_1) && (iVar != idV_2) ){
        vec termCopy;
        vec termOfT = T.terms(iTerm,iVar);
        newTerm[cc].copyVecFrom(termOfT);
        cc += 1;
      }
      else{
        if( iVar == idV_1 ){
          vec term_1 = T.terms(iTerm, idV_1);
          vec term_2 = T.terms(iTerm, idV_2);
          newTerm[cc].init(dofPerDim[cc], T.comm());
          unsigned int jDof = 0;
          for(unsigned int i_1=0; i_1<T.nDof_var(idV_1); i_1++){
            double val_1 = term_1(i_1);
            for(unsigned int i_2=0; i_2< T.nDof_var(idV_2); i_2++){
              double val_2 = term_2(i_2);
              double val = val_1*val_2;
              newTerm[cc](jDof) = val;
              jDof += 1;
            }
          }
          newTerm[cc].finalize();
          cc += 1;
        }
      }
    }
    // replace the current term with the new one.
    T.replaceTerm(iTerm, newTerm);
  }
  // correct the sizes and update indices maps:
  T.set_nVar(dim);
  T.set_nDof_var(dofPerDim);
  T.compute_minc();
  T.finalize();
}





// II.(ii)  TT-Svd from a full Tensor

/* II.(ii).1 given a full tensor and a Tolerance, obtain the TT-svd
  - input: a full tensor, a tolerance
  - output: this TT is the TT-svd approximation of the input tensor
*/
void TensorTrain::TTsvd(fullTensor& T, double tol, bool full_unfold = true){
  if(!m_isInitialised){
    m_comm = T.comm();
    m_nVar = T.nVar();
    m_nDof_var.resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_nDof_var[iVar] = T.nDof_var(iVar);
    }
    compute_minc();
    m_train.resize(m_nVar);
    m_ranks.resize(m_nVar-1);
    m_trainSize.resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_trainSize[iVar].resize(2);
    }
    m_trainSize[0][0] = 1;
    m_trainSize[m_nVar-1][1] = 1;
    m_train[0].resize(1);
    m_isInitialised = true;
  }

  // tolerance squared/d-1
  const double epsSquared = tol*tol/(m_nVar-1);

  // if full_unfold == true => compute and store unfoldings, use QR pre-conditioned SVD.
  if (full_unfold==true){
    // compute the first svd unfolding:
    mat unf;
    T.computeUnfolding(0, unf);
    svd first(unf);
    const unsigned int numSingVal = first.Svec().size();
    unsigned int toRetain = first.Svec().size();
    double sum_S_2 = first.Svec().getVecEl(numSingVal-1) * first.Svec().getVecEl(numSingVal-1);
    unsigned int iS = 1;
    while(sum_S_2<=epsSquared){
      toRetain -= 1;
      sum_S_2 += first.Svec().getVecEl(numSingVal-1-iS) * first.Svec().getVecEl(numSingVal-1-iS);
      iS += 1;
    }

    // modes: first core has size 1 x rank:
    vector<vec> modes(toRetain);
    for(unsigned int iMod=0; iMod<toRetain; iMod++){
      modes[iMod].init(m_nDof_var[0], m_comm);
      for(unsigned int iDof=0; iDof<m_nDof_var[0]; iDof++){
        double value = first.Umat().getMatEl(iDof,iMod);
        if(fabs(value) > DBL_EPSILON){
          modes[iMod].setVecEl(iDof,value);
        }
      }
      modes[iMod].finalize();
    }
    // the first variable has size 1 x rank
    m_ranks[0] = toRetain;
    m_trainSize[0][1] = toRetain;
    m_train[0][0].resize(toRetain);
    m_train[0][0] = modes;
    // the next train will have the first size equal to toRetain:
    m_trainSize[1][0] = toRetain;
    m_train[1].resize(toRetain);

    // free the memory:
    first.clear();
    unf.clear();

    // copy the residual into a temporary object, not to modify the original tensor.
    fullTensor resCopy(m_nVar, m_nDof_var, m_comm);
    resCopy.copyTensorFrom(T);

    // contract the tensor:
    contractTensor(resCopy, 0, modes);
    cout << "After contraction: " << endl;
    resCopy.print();

    for(unsigned int iDim=1; iDim<m_nVar-1; iDim++){
      // pair the variable:
      inplace_pairVars(resCopy, 0, 1);
      cout << "Pairing with var " << iDim << endl;
      cout << "Getting:" << endl;
      resCopy.print();

      // compute the unfolding and its svd:
      mat unf;
      resCopy.computeUnfolding(0, unf);
      svd unfSvd(unf);
      const unsigned int numSingVal = unfSvd.Svec().size();
      unsigned int toRetain = unfSvd.Svec().size();
      double sum_S_2 = unfSvd.Svec().getVecEl(numSingVal-1) * unfSvd.Svec().getVecEl(numSingVal-1);
      unsigned int iS = 1;
      while(sum_S_2<=epsSquared){
        toRetain -= 1;
        sum_S_2 += unfSvd.Svec().getVecEl(numSingVal-1-iS) * unfSvd.Svec().getVecEl(numSingVal-1-iS);
        iS += 1;
      }
      cout << "Modes to be retained: " << toRetain << endl;

      // assign the sizes:
      m_ranks[iDim] = toRetain;
      m_trainSize[iDim][1] = toRetain;
      m_trainSize[iDim+1][0] = toRetain;
      // assemble the core:
      m_train[iDim].resize(m_trainSize[iDim][0]);
      for(unsigned int iMin=0; iMin<m_trainSize[iDim][0]; iMin++){
        m_train[iDim][iMin].resize(toRetain);
      }

      for(unsigned int iPlu=0; iPlu<toRetain; iPlu++){
        unsigned int dofCount = 0;
        for(unsigned int iMin=0; iMin<m_trainSize[iDim][0]; iMin++){
          vec mode(m_nDof_var[iDim], m_comm);
          for(unsigned int iDof=0; iDof<m_nDof_var[iDim]; iDof++){
            double value = unfSvd.Umat().getMatEl(dofCount,iPlu);
            if(fabs(value)>DBL_EPSILON){
              mode.setVecEl(iDof,value);
            }
            dofCount += 1;
          }
          mode.finalize();
          m_train[iDim][iMin][iPlu] = mode;
        }
      }

      // contract the tensor:
      vector<vec> modes(toRetain);
      const unsigned int nDofModes = resCopy.nDof_var(0); // because 0 is iBest<-varSet[0]
      for(unsigned int iMod=0; iMod<toRetain; iMod++){
        modes[iMod].init(nDofModes, m_comm);
        for(unsigned int iDof=0; iDof<nDofModes; iDof++){
          double value = unfSvd.Umat().getMatEl(iDof,iMod);
          if(fabs(value) > DBL_EPSILON){modes[iMod].setVecEl(iDof,value);}
        }
        modes[iMod].finalize();
      }

      // if we are in the last unfolding, we finalize
      if(iDim==(m_nVar-2)){
        cout << "Finalizing the train." << endl;
        vector<vector<vec> > endingCore(toRetain);
        for(unsigned int iMin=0; iMin<toRetain; iMin++){
          endingCore[iMin].resize(1);
          vec mod(m_nDof_var[iDim+1], m_comm);
          for(unsigned int iDof=0; iDof<m_nDof_var[iDim+1]; iDof++){
            double value = unfSvd.Vmat().getMatEl(iDof,iMin) * unfSvd.Svec().getVecEl(iMin);
            if(fabs(value) > DBL_EPSILON){mod.setVecEl(iDof,value);} // equivalent to: mod(iDof) = value;
          }
          mod.finalize();
          endingCore[iMin][0] = mod;
        }
        m_train[iDim+1] = endingCore;
      }
      else{
        //contractTensor(resCopy, iBest, modes); //iBest = varSet[0]
        contractTensor(resCopy, 0, modes);
        cout << "Contracting the tensor: \n";
        resCopy.print();
      }

      // free the memory:
      for(unsigned int iMod=0; iMod<toRetain; iMod++){
        modes.clear();
      }
      modes.clear();
      unf.clear();
      unfSvd.clear();
    }
    resCopy.clear();
    // end if
  }
  else{ // use fast unfolding function, compute left unfolding modes without storing the unfolding

    vector<double> S_0;
    vector<vec> U_0;

    T.computeUnfoldingSVD(0, S_0, U_0, 1.0e-4*epsSquared);
    // determine how many modes to retain
    const unsigned int numSingVal = S_0.size();
    unsigned int toRetain = numSingVal;
    double sum_S_2 = S_0[numSingVal-1] * S_0[numSingVal-1];
    unsigned int iS = 1;
    while(sum_S_2<=epsSquared){
      toRetain -= 1;
      sum_S_2 += S_0[numSingVal-1-iS] * S_0[numSingVal-1-iS];
      iS += 1;
    }
    // erase the modes not to be used.
    for(unsigned int iS=toRetain+1; iS<numSingVal; iS++){
      U_0[iS].clear();
    }
    U_0.resize(toRetain);


    // the first variable has size 1 x rank
    m_ranks[0] = toRetain;
    m_trainSize[0][1] = toRetain;
    m_train[0][0].resize(toRetain);
    m_train[0][0] = U_0;
    // the next train will have the first size equal to toRetain:
    m_trainSize[1][0] = toRetain;
    m_train[1].resize(toRetain);

    // copy the residual into a temporary object, not to modify the original tensor.
    fullTensor resCopy(m_nVar, m_nDof_var, m_comm);
    resCopy.copyTensorFrom(T);

    // contract the tensor:
    contractTensorNoUnfold(resCopy, 0, U_0);
    cout << "After contraction: " << endl;
    resCopy.print();

    for(unsigned int iDim=1; iDim<m_nVar-1; iDim++){
      // pair the variable:
      inplace_pairVars(resCopy, 0, 1);
      cout << "Pairing with var " << iDim << endl;
      cout << "Getting:" << endl;
      resCopy.print();

      // compute the unfolding SVD:
      vector<double> S_i;
      vector<vec> U_i;
      resCopy.computeUnfoldingSVD(0, S_i, U_i, 1.0e-4*epsSquared);
      const unsigned int numSingVal = S_i.size();
      unsigned int toRetain = numSingVal;
      double sum_S_2 = S_i[numSingVal-1] * S_i[numSingVal-1];
      unsigned int iS = 1;
      while(sum_S_2<=epsSquared){
        toRetain -= 1;
        sum_S_2 += S_i[numSingVal-1-iS] * S_i[numSingVal-1-iS];
        iS += 1;
      }
      cout << "Modes to be retained: " << toRetain << endl;
      // free the memory:
      for(unsigned int iS=toRetain+1; iS<numSingVal; iS++){
        U_i[iS].clear();
      }
      U_i.resize(toRetain);

      // assign the sizes:
      m_ranks[iDim] = toRetain;
      m_trainSize[iDim][1] = toRetain;
      m_trainSize[iDim+1][0] = toRetain;
      // assemble the core:
      m_train[iDim].resize(m_trainSize[iDim][0]);
      for(unsigned int iMin=0; iMin<m_trainSize[iDim][0]; iMin++){
        m_train[iDim][iMin].resize(toRetain);
      }

      for(unsigned int iPlu=0; iPlu<toRetain; iPlu++){
        unsigned int dofCount = 0;
        for(unsigned int iMin=0; iMin<m_trainSize[iDim][0]; iMin++){
          vec mode(m_nDof_var[iDim], m_comm);
          for(unsigned int iDof=0; iDof<m_nDof_var[iDim]; iDof++){
            double value = U_i[iPlu].getVecEl(dofCount);
            if(fabs(value)>DBL_EPSILON){
              mode.setVecEl(iDof,value);
            }
            dofCount += 1;
          }
          mode.finalize();
          m_train[iDim][iMin][iPlu] = mode;
        }
      }

      // if we are in the last unfolding, we finalize
      if(iDim==(m_nVar-2)){
        cout << "Finalizing the train." << endl;
        contractTensorNoUnfold(resCopy, 0, U_i);
        cout << "Residual tensor is of size: " << endl;
        resCopy.print();

        vector<vector<vec> > endingCore(toRetain);
        for(unsigned int iMin=0; iMin<toRetain; iMin++){
          endingCore[iMin].resize(1);
          vec mod(m_nDof_var[iDim+1], m_comm);
          for(unsigned int iDof=0; iDof<m_nDof_var[iDim+1]; iDof++){
            vector<unsigned int> ind(2);
            if(resCopy.nDof_var(0) == toRetain){
              ind = {iMin, iDof};
            }
            else{
              ind = {iDof, iMin};
            }
            double value = resCopy.eval(ind);
            if(fabs(value) > DBL_EPSILON){mod.setVecEl(iDof,value);} // equivalent to: mod(iDof) = value;
          }
          mod.finalize();
          endingCore[iMin][0] = mod;
        }
        m_train[iDim+1] = endingCore;
      }
      else{
        //contractTensor(resCopy, iBest, modes); //iBest = varSet[0]
        contractTensorNoUnfold(resCopy, 0, U_i);
        cout << "Contracting the tensor: \n";
        resCopy.print();
      }

      // free the memory:
      for(unsigned int iMod=0; iMod<toRetain; iMod++){
        U_i[iMod].clear();
      }
      U_i.clear();
      S_i.clear();

    }
    resCopy.clear();
    // end else
  }
}


/* II.(ii).2 contract tensor:
  - input: a full tensor, a set of modes, the direction along with we contract
  - output: the full tensor is modified.
*/
void TensorTrain::contractTensor(fullTensor& T, unsigned int id, vector<vec>& modes){
  const unsigned int nMod = modes.size();
  const unsigned int nDof = modes[0].size();
  // we use a function in fullTensor:
  mat modMatrixT(nMod, nDof, m_comm);
  for(unsigned int iRow=0; iRow<nMod; iRow++){
    for(unsigned int iCol=0; iCol<nDof; iCol++){
      double value = modes[iRow].getVecEl(iCol);
      if(fabs(value)>DBL_EPSILON){modMatrixT.setMatEl(iRow,iCol,value);}
    }
  }
  modMatrixT.finalize();
  T.inPlaceMatTensProd(id, modMatrixT);
  modMatrixT.clear();
}

/* II.(ii).2 contract tensor without computing explicitly the unfolding:
  - input: a full tensor, a set of modes, the direction along with we contract
  - output: the full tensor is modified.
*/
void TensorTrain::contractTensorNoUnfold(fullTensor& T, unsigned int id, vector<vec>& modes){
  const unsigned int nMod = modes.size();
  const unsigned int nDof = modes[0].size();

  T.inplaceModeIContraction(id, modes);
}


/* II.(ii).3 pair variables, in-place version
  - input: a full Tensor, the first variable, the second variable
  - output: a full Tensor which is the reshaping of the original one
*/
void TensorTrain::inplace_pairVars(fullTensor& T, unsigned int idV_1, unsigned int idV_2){
  assert(idV_1 != idV_2);
  const unsigned int dim = T.nVar() - 1;
  vector<unsigned int> dofPerDim(dim);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
    if( (iVar != idV_1) && (iVar != idV_2) ){
      dofPerDim[cc] = T.nDof_var(iVar);
      cc += 1;
    }
    else{
      if( iVar == idV_1 ){
        dofPerDim[cc] = T.nDof_var(idV_1) * T.nDof_var(idV_2);
        cc += 1;
      }
    }
  }

  fullTensor empty(dim, dofPerDim, T.comm());
  vec entriesRemappedCopy(T.nEntries(), T.comm());
  for(unsigned int iEn=0; iEn<T.nEntries(); iEn++){
    double value = T.tensorEntries().getVecEl(iEn);
    if(fabs(value)>DBL_EPSILON){
      vector<unsigned int> subT = T.lin2sub(iEn);
      vector<unsigned int> subR(dim);
      unsigned int cc = 0;
      for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
        if( (iVar != idV_1) && (iVar != idV_2) ){
          subR[cc] = subT[iVar];
          cc += 1;
        }
        else{
          if( iVar == idV_1 ){
            subR[cc] = subT[idV_2] + T.nDof_var(idV_2)*subT[idV_1];
            cc += 1;
          }
        }
      }
      unsigned int lin = empty.sub2lin(subR);
      entriesRemappedCopy.setVecEl(lin, value);
    }
  }
  entriesRemappedCopy.finalize();
  T.clear();
  T.set_nVar(dim);
  T.set_nDof_var(dofPerDim);
  T.compute_minc();
  T.set_tensorEntries(entriesRemappedCopy);
}


/* update the residual
  - input: a full Tensor
  - output: the full tensor to which we subtract the tensor train, return the norm of it
*/
double TensorTrain::update_residual(fullTensor& residual){
  double resNorm = residual.tensorEntries().norm();
  cout << "Initial residual norm = " << resNorm << endl;
  for(unsigned int iEn=0; iEn<residual.nEntries(); iEn++){
    vector<unsigned int> ind = residual.lin2sub(iEn);
    double value = residual.tensorEntries().getVecEl(iEn);
    double toSubtract = eval(ind);
    value = value - toSubtract;
    residual.set_tensorElement(iEn, value);
  }
  resNorm = residual.tensorEntries().norm();
  cout << "Residual norm = " << resNorm << endl;
  return resNorm;
}




// II.(iii)  TT-Svd from a sparse Tensor

/* II.(iii).1 given a sparse tensor and a Tolerance, obtain the TT-svd
  - input: a sparse tensor, a tolerance
  - output: this TT is the TT-svd approximation of the input tensor
*/
void TensorTrain::TTsvd(sparseTensor& T, double tol){
  if(!m_isInitialised){
    m_comm = T.comm();
    m_nVar = T.nVar();
    m_nDof_var.resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_nDof_var[iVar] = T.nDof_var(iVar);
    }
    compute_minc();
    m_train.resize(m_nVar);
    m_ranks.resize(m_nVar-1);
    m_trainSize.resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_trainSize[iVar].resize(2);
    }
    m_trainSize[0][0] = 1;
    m_trainSize[m_nVar-1][1] = 1;
    m_train[0].resize(1);
    m_isInitialised = true;
  }

  // tolerance squared/d-1
  const double epsSquared = tol*tol/(m_nVar-1);

  // compute the first svd unfolding:
  mat unf;
  T.computeUnfolding(0, unf);


  svd first(unf);
  const unsigned int numSingVal = first.Svec().size();
  unsigned int toRetain = first.Svec().size();
  double sum_S_2 = first.Svec().getVecEl(numSingVal-1) * first.Svec().getVecEl(numSingVal-1);
  unsigned int iS = 1;
  while(sum_S_2<=epsSquared){
    toRetain -= 1;
    sum_S_2 += first.Svec().getVecEl(numSingVal-1-iS) * first.Svec().getVecEl(numSingVal-1-iS);
    iS += 1;
  }


  // modes: first core has size 1 x rank:
  vector<vec> modes(toRetain);
  for(unsigned int iMod=0; iMod<toRetain; iMod++){
    modes[iMod].init(m_nDof_var[0], m_comm);
    for(unsigned int iDof=0; iDof<m_nDof_var[0]; iDof++){
      double value = first.Umat().getMatEl(iDof,iMod);
      if(fabs(value) > DBL_EPSILON){
        modes[iMod].setVecEl(iDof,value);
      }
    }
    modes[iMod].finalize();
  }
  // the first variable has size 1 x rank
  m_ranks[0] = toRetain;
  m_trainSize[0][1] = toRetain;
  m_train[0][0].resize(toRetain);
  m_train[0][0] = modes;
  // the next train will have the first size equal to toRetain:
  m_trainSize[1][0] = toRetain;
  m_train[1].resize(toRetain);

  // free the memory:
  first.clear();
  unf.clear();

  // copy the residual into a temporary object, not to modify the original tensor.
  sparseTensor resCopy(m_nVar, m_nDof_var, m_comm);
  resCopy.copyTensorFrom(T);

  // contract the tensor:
  contractTensor(resCopy, 0, modes);
  cout << "After contraction: " << endl;
  resCopy.print();

  for(unsigned int iDim=1; iDim<m_nVar-1; iDim++){
    // pair the variable:
    inplace_pairVars(resCopy, 0, 1);
    cout << "Pairing with var " << iDim << endl;
    cout << "Getting:" << endl;
    resCopy.print();

    // compute the unfolding and its svd:
    mat unf;
    resCopy.computeUnfolding(0, unf);
    svd unfSvd(unf);
    const unsigned int numSingVal = unfSvd.Svec().size();
    unsigned int toRetain = unfSvd.Svec().size();
    double sum_S_2 = unfSvd.Svec().getVecEl(numSingVal-1) * unfSvd.Svec().getVecEl(numSingVal-1);
    unsigned int iS = 1;
    while(sum_S_2<=epsSquared){
      toRetain -= 1;
      sum_S_2 += unfSvd.Svec().getVecEl(numSingVal-1-iS) * unfSvd.Svec().getVecEl(numSingVal-1-iS);
      iS += 1;
    }
    cout << "Modes to be retained: " << toRetain << endl;

    // assign the sizes:
    m_ranks[iDim] = toRetain;
    m_trainSize[iDim][1] = toRetain;
    m_trainSize[iDim+1][0] = toRetain;
    // assemble the core:
    m_train[iDim].resize(m_trainSize[iDim][0]);
    for(unsigned int iMin=0; iMin<m_trainSize[iDim][0]; iMin++){
      m_train[iDim][iMin].resize(toRetain);
    }

    for(unsigned int iPlu=0; iPlu<toRetain; iPlu++){
      unsigned int dofCount = 0;
      for(unsigned int iMin=0; iMin<m_trainSize[iDim][0]; iMin++){
        vec mode(m_nDof_var[iDim], m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[iDim]; iDof++){
          double value = unfSvd.Umat().getMatEl(dofCount,iPlu);
          if(fabs(value)>DBL_EPSILON){
            mode.setVecEl(iDof,value);
          }
          dofCount += 1;
        }
        mode.finalize();
        m_train[iDim][iMin][iPlu] = mode;
      }
    }

    // contract the tensor:
    vector<vec> modes(toRetain);
    const unsigned int nDofModes = resCopy.nDof_var(0); // because 0 is iBest<-varSet[0]
    for(unsigned int iMod=0; iMod<toRetain; iMod++){
      modes[iMod].init(nDofModes, m_comm);
      for(unsigned int iDof=0; iDof<nDofModes; iDof++){
        double value = unfSvd.Umat().getMatEl(iDof,iMod);
        if(fabs(value) > DBL_EPSILON){modes[iMod].setVecEl(iDof,value);}
      }
      modes[iMod].finalize();
    }

    // if we are in the last unfolding, we finalize
    if(iDim==(m_nVar-2)){
      cout << "Finalizing the train." << endl;
      vector<vector<vec> > endingCore(toRetain);
      for(unsigned int iMin=0; iMin<toRetain; iMin++){
        endingCore[iMin].resize(1);
        vec mod(m_nDof_var[iDim+1], m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[iDim+1]; iDof++){
          double value = unfSvd.Vmat().getMatEl(iDof,iMin) * unfSvd.Svec().getVecEl(iMin);
          if(fabs(value) > DBL_EPSILON){mod.setVecEl(iDof,value);} // equivalent to: mod(iDof) = value;
        }
        mod.finalize();
        endingCore[iMin][0] = mod;
      }
      m_train[iDim+1] = endingCore;
    }
    else{
      //contractTensor(resCopy, iBest, modes); //iBest = varSet[0]
      contractTensor(resCopy, 0, modes);
      cout << "Contracting the tensor: \n";
      resCopy.print();
    }

    // free the memory:
    for(unsigned int iMod=0; iMod<toRetain; iMod++){
      modes.clear();
    }
    modes.clear();
    unf.clear();
    unfSvd.clear();
  }
  resCopy.clear();
}


/* II.(iii).2 contract tensor:
  - input: a sparse tensor, a set of modes, the direction along with we contract
  - output: the full tensor is modified.
*/
void TensorTrain::contractTensor(sparseTensor& T, unsigned int id, vector<vec>& modes){
  const unsigned int nMod = modes.size();
  const unsigned int nDof = modes[0].size();
  // we use a function in fullTensor:
  mat modMatrixT(nMod, nDof, m_comm);
  for(unsigned int iRow=0; iRow<nMod; iRow++){
    for(unsigned int iCol=0; iCol<nDof; iCol++){
      double value = modes[iRow].getVecEl(iCol);
      if(fabs(value)>DBL_EPSILON){modMatrixT.setMatEl(iRow,iCol,value);}
    }
  }
  modMatrixT.finalize();
  T.inPlaceMatTensProd(id, modMatrixT);
  modMatrixT.clear();
}


/* II.(iii).3 pair variables, in-place version
  - input: a sparse Tensor, the first variable, the second variable
  - output: a sparse Tensor which is the reshaping of the original one
*/
void TensorTrain::inplace_pairVars(sparseTensor& T, unsigned int idV_1, unsigned int idV_2){
  assert(idV_1 != idV_2);
  const unsigned int dim = T.nVar() - 1;
  vector<unsigned int> dofPerDim(dim);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
    if( (iVar != idV_1) && (iVar != idV_2) ){
      dofPerDim[cc] = T.nDof_var(iVar);
      cc += 1;
    }
    else{
      if( iVar == idV_1 ){
        dofPerDim[cc] = T.nDof_var(idV_1) * T.nDof_var(idV_2);
        cc += 1;
      }
    }
  }

  fullTensor empty(dim, dofPerDim, T.comm());
  vector<double> entriesRemappedCopy(T.nonZeros());
  for(unsigned int iEn=0; iEn<T.nonZeros(); iEn++){
    double value = T.tensorEntries(iEn);
    if(fabs(value)>DBL_EPSILON){
      vector<unsigned int> subT = T.indices(iEn);
      vector<unsigned int> subR(dim);
      unsigned int cc = 0;
      for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
        if( (iVar != idV_1) && (iVar != idV_2) ){
          subR[cc] = subT[iVar];
          cc += 1;
        }
        else{
          if( iVar == idV_1 ){
            subR[cc] = subT[idV_2] + T.nDof_var(idV_2)*subT[idV_1];
            cc += 1;
          }
        }
      }
      unsigned int lin = empty.sub2lin(subR);
      entriesRemappedCopy[lin]= value;
    }
  }

  T.clear();
  T.set_nVar(dim);
  T.set_nDof_var(dofPerDim);
  T.compute_minc();
  T.set_tensorEntries(entriesRemappedCopy);
}


/* update the residual
  - input: a sparse Tensor
  - output: the sparse tensor to which we subtract the tensor train, return the norm of it
*/
double TensorTrain::update_residual(sparseTensor& residual){
  double resNorm = residual.tensorEntries().norm();
  cout << "Initial residual norm = " << resNorm << endl;
  for(unsigned int iEn=0; iEn<residual.nonZeros(); iEn++){
    vector<unsigned int> ind = residual.indices(iEn);
    double value = residual.tensorEntries(iEn);
    double toSubtract = eval(ind);
    value = value - toSubtract;
    residual.set_tensorElement(ind, value);
  }
  resNorm = residual.tensorEntries().norm();
  cout << "Residual norm = " << resNorm << endl;
  return resNorm;
}




/* TT rounding
  - input: tolerance
  - output: this tensor is rounded up to the tolerance
*/
void TensorTrain::round(double tol){

  const double tolPerUnf = tol * tol / (m_nVar - 1);

  // starting by compressing the modes of the first direction:
  unsigned int nMod = m_trainSize[0][1];
  mat C(nMod,nMod);
  for(unsigned int iMod=0; iMod<nMod; iMod++){
    for(unsigned int jMod=0; jMod<=iMod; jMod++){
      double val = scalProd(m_train[0][0][iMod], m_train[0][0][jMod]);
      C.setMatEl(iMod,jMod, val);
      if(iMod != jMod){
        C.setMatEl(jMod,iMod,val);
      }
    }
  }
  C.finalize();

  // solve the eigenvalue problem:
  eigenSolver eps;
  eps.init(C, EPS_HEP, nMod, 1.0e-12);
  eps.solveHermEPS(C);
  vector<vec> vv = eps.eigenVecs();
  vector<double> lambda = eps.eigenVals();

  // finding how many modes to retain:
  unsigned int nS = lambda.size();
  unsigned int toRetain = nS;
  double tail = fabs(lambda[nS-1]);// * lambda[nS-1];
  unsigned int cc = 1;
  while (tail < tolPerUnf){
    cc += 1;
    tail += fabs(lambda[nS-cc]);// * lambda[nS-cc];
    toRetain -= 1;
  }


  // compute the new terms of the train:
  vector<vector<vector<vec> > > roundedTrain(m_nVar);
  roundedTrain[0].resize(1);
  roundedTrain[0][0].resize(toRetain);
  // filling the first direction modes:
  for(unsigned int iMod=0; iMod<toRetain; iMod++){
    roundedTrain[0][0][iMod].init(m_nDof_var[0],m_comm);
    for(unsigned int iDof=0; iDof<m_nDof_var[0]; iDof++){
      double value = 0.0;
      for(unsigned int k=0; k<m_trainSize[0][1]; k++){
        value += m_train[0][0][k].getVecEl(iDof) * vv[iMod].getVecEl(k);
      }
      roundedTrain[0][0][iMod].setVecEl(iDof, value); // or simply roundedTrain[0][0][iMod](iDof) = value;
    }
    roundedTrain[0][0][iMod].finalize();
    double scaling = 1.0/sqrt(lambda[iMod]);
    roundedTrain[0][0][iMod] *= scaling;
  }

  // apply the matrix lambda^1/2 V^T to the subsequent train and free the memory:
  for(unsigned int iMod=0; iMod<toRetain; iMod++){
    //double scaling = 1.0/sqrt(lambda[iMod]);
    double scaling = sqrt(lambda[iMod]);
    vv[iMod] *= scaling;
  }

  // contract the second core with the matrix:
  /*vector<vector<vec> > temporary(toRetain);
  for(unsigned int iMod=0; iMod<toRetain; iMod++){
    temporary[iMod].resize(m_trainSize[1][1]); // variable 1, r2
    for(unsigned int J=0; J<m_trainSize[1][1]; J++){
      temporary[iMod][J].init(m_nDof_var[1],m_comm);
      for(unsigned int iDof=0; iDof<m_nDof_var[1]; iDof++){
        double value = 0.0
        for(unsigned int I=0; I<m_trainSize[1][0]; I++){
          value += vv[iMod].getVecEl(I) * m_train[1][I][J].getVecEl(iDof);
        }
        temporary[iMod][J].setVecEl(iDof, value);
      }

    }
  }*/

  // contract the core and reshape it in 1 pass, without copies:
  vector<vec> reshaped_tmp(m_trainSize[1][1]);
  for(unsigned int J=0; J<m_trainSize[1][1]; J++){
    reshaped_tmp[J].init(toRetain * m_nDof_var[1], m_comm);
    unsigned int cc = 0;
    for(unsigned int iMod=0; iMod<toRetain; iMod++){
      for(unsigned int iDof=0; iDof<m_nDof_var[1]; iDof++){
        double value = 0.0;
        for(unsigned int I=0; I<m_trainSize[1][0]; I++){
          value += vv[iMod].getVecEl(I) * m_train[1][I][J].getVecEl(iDof);
        }
        reshaped_tmp[J].setVecEl(cc,value);
        cc += 1;
      }
    }
    reshaped_tmp[J].finalize();
  }

  // free the memory:
  eps.clear();
  C.clear();

  // start looping over all the other variables:
  for(unsigned int iVar=1; iVar<m_nVar-1; iVar++){

    unsigned int I_round = toRetain;
    roundedTrain[iVar].resize(I_round);

    // 1 -- contract the reshaped_tmp:
    unsigned int nMod = m_trainSize[iVar][1]; // which is the number of columns of reshaped_tmp
    cout << "Size of covariance: " << nMod << endl;
    mat C(nMod,nMod);
    for(unsigned int iMod=0; iMod<nMod; iMod++){
      for(unsigned int jMod=0; jMod<=iMod; jMod++){
        double val = scalProd(reshaped_tmp[iMod], reshaped_tmp[jMod]);
        C.setMatEl(iMod,jMod, val);
        if(iMod != jMod){
          C.setMatEl(jMod,iMod,val);
        }
      }
    }
    C.finalize();

    // solve the eigenvalue problem:
    eigenSolver eps;
    eps.init(C, EPS_HEP, nMod, 1.0e-12);
    eps.solveHermEPS(C);
    vector<vec> vv = eps.eigenVecs();
    vector<double> lambda = eps.eigenVals();


    // find how many modes to be retained:
    unsigned int nS = lambda.size();
    toRetain = nS;
    double tail = fabs(lambda[nS-1]); //* lambda[nS-1];
    unsigned int cc = 1;
    while (tail < tolPerUnf){
      cc += 1;
      tail += fabs(lambda[nS-cc]); // * lambda[nS-cc];
      toRetain -= 1;
    }
    cout << "To be retained = " << toRetain << endl;

    // compute the modes and put them in the core:
    for(unsigned int i=0; i<I_round; i++){
      roundedTrain[iVar][i].resize(toRetain);
    }

    for(unsigned int iMod=0; iMod<toRetain; iMod++){
      // scaling factor:
      double scaling = 1.0/sqrt(lambda[iMod]);
      unsigned int cc = 0;
      for(unsigned int i=0; i<I_round; i++){
        roundedTrain[iVar][i][iMod].init(m_nDof_var[iVar],m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[iVar]; iDof++){
          double value = 0.0;
          for(unsigned int k=0; k<m_trainSize[iVar][1]; k++){
            value += reshaped_tmp[k].getVecEl(cc) * vv[iMod].getVecEl(k);
          }
          roundedTrain[iVar][i][iMod].setVecEl(iDof, value);
          cc += 1;
        }
        roundedTrain[iVar][i][iMod].finalize();
        roundedTrain[iVar][i][iMod] *= scaling;
      }
    }

    // compute the matrix to contract the following core:
    for(unsigned int iMod=0; iMod<toRetain; iMod++){
      double scaling = sqrt(lambda[iMod]);
      vv[iMod] *= scaling;
    }

    // clear reshaped_tmp:
    for(unsigned int j=0; j<m_trainSize[iVar][1]; j++){
      reshaped_tmp[j].clear();
    }

    // contract the following core and flatten it, in one pass:
    reshaped_tmp.resize(m_trainSize[iVar+1][1]);
    for(unsigned int J=0; J<m_trainSize[iVar+1][1]; J++){
      reshaped_tmp[J].init(toRetain * m_nDof_var[iVar+1], m_comm);
      unsigned int cc = 0;
      for(unsigned int iMod=0; iMod<toRetain; iMod++){
        for(unsigned int iDof=0; iDof<m_nDof_var[iVar+1]; iDof++){
          double value = 0.0;
          for(unsigned int I=0; I<m_trainSize[iVar+1][0]; I++){
            value += vv[iMod].getVecEl(I) * m_train[iVar+1][I][J].getVecEl(iDof);
          }
          reshaped_tmp[J].setVecEl(cc,value);
          cc += 1;
        }
      }
      reshaped_tmp[J].finalize();
    }

    // free the memory:
    eps.clear();
    C.clear();
  }

  // last variable and finalize: reshape and put into the train
  roundedTrain[m_nVar-1].resize(toRetain);
  unsigned int counter = 0;
  for(unsigned int i=0; i<toRetain; i++){
    roundedTrain[m_nVar-1][i].resize(1);
    roundedTrain[m_nVar-1][i][0].init(m_nDof_var[m_nVar-1], m_comm);
    for(unsigned int iDof=0; iDof<m_nDof_var[m_nVar-1]; iDof++){
      double value = reshaped_tmp[0].getVecEl(counter);
      roundedTrain[m_nVar-1][i][0].setVecEl(iDof,value);
      counter += 1;
    }
  }

  // reinit tensor train:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int I=0; I<m_trainSize[iVar][0]; I++){
      for(unsigned int J=0; J<m_trainSize[iVar][1]; J++){
        m_train[iVar][I][J].clear();
      }
    }
  }
  m_train.clear();
  m_train = roundedTrain;

  // set the ranks and the new train sizes:
  m_trainSize[0][1] = roundedTrain[0][0].size();
  m_ranks[0] = roundedTrain[0][0].size();
  for(unsigned int iVar=1; iVar<m_nVar-1; iVar++){
    m_trainSize[iVar][0] = m_trainSize[iVar-1][1];
    m_ranks[iVar] = roundedTrain[iVar][1].size();
    m_trainSize[iVar][1] = roundedTrain[iVar][0].size();
  }
  m_trainSize[m_nVar-1][0] = roundedTrain[m_nVar-1].size();
  m_trainSize[m_nVar-1][1] = 1;

  // free the memory:
  for(unsigned int j=0; j<reshaped_tmp.size(); j++){
    reshaped_tmp[j].clear();
  }
}
