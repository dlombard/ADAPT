// Header class for Tensor Train format

#ifndef TensorTrain_h
#define TensorTrain_h

#include "../tensor.h"
#include "../linAlg/linAlg.h"
#include "../fullTensor/fullTensor.h"
#include "../sparseTensor/sparseTensor.h"
#include "../CP/CP.h"

class TensorTrain : public tensor{
private:
  vector<vector<vector<vec> > > m_train;
  vector<unsigned int> m_ranks;
  vector<vector<unsigned int > > m_trainSize;
  bool m_isInitialised;

public:
  TensorTrain(){m_isInitialised = false;};
  ~TensorTrain(){};
  inline void clear(){
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      //m_trainSize[iVar].clear();
      for(unsigned int iMin=0; iMin<m_train[iVar].size(); iMin++){
        for(unsigned int iPlu=0; iPlu<m_train[iVar][iMin].size(); iPlu++){
          m_train[iVar][iMin][iPlu].clear();
        }
      }
    }
    //m_isInitialised = false;
    //m_trainSize.clear();
    //m_ranks.clear();
  }

  TensorTrain(vector<unsigned int>, MPI_Comm);
  TensorTrain(unsigned int, vector<unsigned int>, MPI_Comm);
  TensorTrain(vector<unsigned int>, vector<unsigned int>, MPI_Comm);
  TensorTrain(vector<vector<vector<vec> > >&);
  void init(vector<unsigned int>, MPI_Comm);


  // - ACCESS FUNCTIONS -
  inline vector<unsigned int> ranks(){return m_ranks;};
  inline unsigned int ranks(unsigned int iR){return m_ranks[iR];}
  inline vector<vector<vector<vec> > > train(){return m_train;}
  inline vector<vector<vec> > train(unsigned int iVar){return m_train[iVar];}
  inline vec train(unsigned int iVar, unsigned int I, unsigned int J){
    if(iVar==0){assert(I == 0);}
    if(iVar==m_nVar-1){assert(J==0);}
    return m_train[iVar][I][J];
  }
  inline double train(unsigned int iVar, unsigned int I, unsigned int J, unsigned int iDof){
    if(iVar==0){assert(I == 0);}
    if(iVar==m_nVar-1){assert(J==0);}
    return m_train[iVar][I][J].getVecEl(iDof);
  }

  inline vector<vector<unsigned int> > trainSize(){return m_trainSize;}
  inline vector<unsigned int> trainSize(unsigned int iVar){return m_trainSize[iVar];}
  inline unsigned int trainSize(unsigned int iVar, unsigned int i_or_j){
    assert( (i_or_j == 0) || (i_or_j == 1) );
    return m_trainSize[iVar][i_or_j];
  }

  inline bool isInitialised(){return m_isInitialised;}

  // printing train sizes:
  inline void print_ttSize(){
    print();
    PetscPrintf(m_comm,"\n");
    PetscPrintf(m_comm, "TT ranks: %d ", m_ranks[0]);
    for(unsigned int iVar=1; iVar<m_nVar-1; iVar++){
      PetscPrintf(m_comm," %d ", m_ranks[iVar]);
    }
    PetscPrintf(m_comm,"\n");
    PetscPrintf(m_comm,"TT core sizes:\n");
    for(unsigned int iVar=0; iVar<3; iVar++){
      PetscPrintf(m_comm," %d x %d \n", m_trainSize[iVar][0], m_trainSize[iVar][1]);
    }
    PetscPrintf(m_comm,"\n");
  }

  // evaluate the memory (in number of doubles):
  inline unsigned int memory(){
    unsigned int mem = 0;
    mem = m_ranks[0] * m_nDof_var[0];
    for(unsigned int iVar=1; iVar<m_nVar-1; iVar++){
      unsigned int toAdd = m_ranks[iVar-1] * m_ranks[iVar] * m_nDof_var[iVar];
      mem = mem + toAdd;
    }
    mem = mem + (m_ranks[m_nVar-2] * m_nDof_var[m_nVar-1]);
    return mem;
  }


  // -- SETTERS --
  inline void set_ranks(vector<unsigned int> TTranks){
    assert(TTranks.size() == m_nVar - 1);
    for(unsigned int iVar=0; iVar<m_nVar-1; iVar++){
      m_ranks[iVar] = TTranks[iVar];
    }

    m_trainSize.resize(m_nVar);
    unsigned int iVar=0;
    m_trainSize[iVar].resize(2);
    m_trainSize[iVar][0] = 1;
    m_trainSize[iVar][1] = TTranks[0];
    for(unsigned int iVar=1; iVar<m_nVar-1; iVar++){
      m_trainSize[iVar][0] = TTranks[iVar-1];
      m_trainSize[iVar][1] = TTranks[iVar];
    }
    iVar = m_nVar-1;
    m_trainSize[iVar][0] = TTranks[iVar-1];
    m_trainSize[iVar][1] = 1;
  }


  // resize a tensor train according to trainSize
  inline void resizeTrain(){
    clear();
    // resizing the train:
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      unsigned int I = m_trainSize[iVar][0];
      m_train[iVar].resize(I);
      for(unsigned int iM = 0; iM<I; iM++){
        unsigned int J = m_trainSize[iVar][1];
        m_train[iVar][iM].resize(J);
      }
    }
  }

  // resize a core for one variable:
  inline void resizeTrain(unsigned int iVar){
    unsigned int I = m_trainSize[iVar][0];
    m_train[iVar].resize(I);
    for(unsigned int iM = 0; iM<I; iM++){
      unsigned int J = m_trainSize[iVar][1];
      m_train[iVar][iM].resize(J);
    }
  }

  // setting a tensor element without changing the rest:
  inline void set_tensorElement(vector<unsigned int> ind, double val){
    double actual = eval(ind);
    double toBeAdded = val - actual;

    // first:
    vec first(m_nDof_var[0], m_comm);
    first.setVecEl(ind[0], 1.0);
    first.finalize();
    m_ranks[0] += 1;
    m_trainSize[0][1] += 1;
    m_train[0][0].push_back(first);

    // directions from second to last-1:
    for(unsigned int iVar=1; iVar<m_nVar-1; iVar++){
      m_ranks[iVar] += 1;
      m_trainSize[iVar][0] += 1;
      m_trainSize[iVar][1] += 1;
      m_train[iVar].resize(m_trainSize[iVar][0]);
      for(unsigned int iR=0; iR<m_trainSize[iVar][0]; iR++){
        m_train[iVar][iR].resize(m_trainSize[iVar][1]);
      }
      unsigned int iR = m_trainSize[iVar][0]-1;
      for(unsigned int jR=0; jR < m_trainSize[iVar][1]-1; jR++){
        vec toPut(m_nDof_var[iVar], m_comm);
        toPut.finalize();
        m_train[iVar][iR][jR] = toPut;
      }
      unsigned int jR = m_trainSize[iVar][1]-1;
      for(unsigned int iR=0; iR< m_trainSize[iVar][0]-1; iR++){
        vec toPut(m_nDof_var[iVar], m_comm);
        toPut.finalize();
        m_train[iVar][iR][jR] = toPut;
      }
      vec toPut(m_nDof_var[iVar], m_comm);
      toPut.setVecEl(ind[iVar], 1.0);
      toPut.finalize();
      m_train[iVar][m_trainSize[iVar][0]-1][m_trainSize[iVar][1]-1] = toPut;
    }

    // last direction:
    m_trainSize[m_nVar-1][0] += 1;
    m_train[m_nVar-1].resize(m_trainSize[m_nVar-1][0]);
    m_train[m_nVar-1][m_trainSize[m_nVar-1][0]-1].resize(1);
    vec last(m_nDof_var[m_nVar-1], m_comm);
    last.setVecEl(ind[m_nVar-1], toBeAdded);
    last.finalize();
    m_train[m_nVar-1][m_trainSize[m_nVar-1][0]-1][0] = last;
  }


  // setting a train or train elements:
  inline void set_train(vector<vector<vector<vec> > >& aTrain){
    m_train = aTrain;
  }

  inline void set_train(unsigned int iVar, vector<vector<vec> >& aCore){
    m_train[iVar] = aCore;
  }

  inline void set_train(unsigned int iVar, unsigned int I, unsigned int J, vec& fiber){
    m_train[iVar][I][J] = fiber;
  }

  // clear fiber or fibers to free the memory:
  inline void clear_fibers(unsigned int iVar){
    for(unsigned int i=0; i<m_train[iVar].size(); i++){
      for(unsigned int j=0; j<m_train[iVar][i].size(); j++){
        m_train[iVar][i][j].clear();
      }
    }
  }
  // clear an individual fiber:
  inline void clear_fibers(unsigned int iVar, unsigned int I, unsigned int J){
      m_train[iVar][I][J].clear();
  }


  // - Function to evaluate a TT -
  // get the core:
  inline fullTensor getCore(unsigned int iVar){
    fullTensor core({m_trainSize[iVar][0], m_nDof_var[iVar], m_trainSize[iVar][1]}, m_comm);
    unsigned int cc = 0;
    for(unsigned int iR=0; iR<m_trainSize[iVar][0]; iR++){
      for(unsigned int jR=0; jR<m_trainSize[iVar][1]; jR++){
        for(unsigned int iDof=0; iDof<m_nDof_var[iVar]; iDof++){
          double val = m_train[iVar][iR][jR].getVecEl(iDof);
          if(val != 0.0){
            core.set_tensorElement(cc, val);
          }
          cc += 1;
        }
      }
    }
    core.finalize();
    return core;
  }


  // get the core slices:
  inline mat getCoreSlice(unsigned int iVar, unsigned int iDof){
    mat toBeReturned(m_trainSize[iVar][0], m_trainSize[iVar][1], m_comm);
    for(unsigned int iRow=0; iRow < m_trainSize[iVar][0]; iRow++){
      for(unsigned int iCol=0; iCol< m_trainSize[iVar][1]; iCol++){
        double val = m_train[iVar][iRow][iCol].getVecEl(iDof);
        toBeReturned.setMatEl(iRow, iCol, val);
      }
      toBeReturned.finalize();
    }
    return toBeReturned;
  }

  // Evaluate a TT element:

  /* eval function (base version)
    - input: the multi-index
    - output: the value
  */
  void eval(const vector<unsigned int>& ind, double& val){

    if(m_trainSize[0][1] == 0){
      val = 0.0;
    }
    else{
      vector<double> firstVec(m_trainSize[0][1]);
      unsigned int iDof = ind[0];
      for(unsigned int iR=0; iR<m_trainSize[0][1]; iR++){
        firstVec[iR] = m_train[0][0][iR].getVecEl(iDof);
      }

      for(unsigned int iVar=1; iVar<m_nVar; iVar++){
        vector<double> res(m_trainSize[iVar][1]);
        iDof = ind[iVar];
        for(unsigned int jRes=0; jRes<m_trainSize[iVar][1]; jRes++){
          res[jRes] = 0.0;
          for(unsigned int iRes=0; iRes<m_trainSize[iVar][0]; iRes++){
            double val = m_train[iVar][iRes][jRes].getVecEl(iDof);
            res[jRes] += val * firstVec[iRes];
          }
        }
        // copy into firstVec
        firstVec.clear();
        firstVec.resize(m_trainSize[iVar][1]);
        for(unsigned int iR=0; iR<m_trainSize[iVar][1]; iR++){
          firstVec[iR] = res[iR];
        }
        res.clear();
      }
      // return
      val = firstVec[0];
    }
  }


  /* eval function (overloaded to return double)
    - input: the multi-index
    - output: the value
  */
  double eval(const vector<unsigned int>& ind){
    if(m_trainSize[0][1] == 0){
      return 0.0;
    }
    else{
      vector<double> firstVec(m_trainSize[0][1]);
      unsigned int iDof = ind[0];
      for(unsigned int iR=0; iR<m_trainSize[0][1]; iR++){
        firstVec[iR] = m_train[0][0][iR].getVecEl(iDof);
      }

      for(unsigned int iVar=1; iVar<m_nVar; iVar++){
        vector<double> res(m_trainSize[iVar][1]);
        iDof = ind[iVar];
        for(unsigned int jRes=0; jRes<m_trainSize[iVar][1]; jRes++){
          res[jRes] = 0.0;
          for(unsigned int iRes=0; iRes<m_trainSize[iVar][0]; iRes++){
            double val = m_train[iVar][iRes][jRes].getVecEl(iDof);
            res[jRes] += val * firstVec[iRes];
          }
        }
        // copy into firstVec
        firstVec.clear();
        firstVec.resize(m_trainSize[iVar][1]);
        for(unsigned int iR=0; iR<m_trainSize[iVar][1]; iR++){
          firstVec[iR] = res[iR];
        }
        res.clear();
      }
      // return
      return firstVec[0];
    }
  }

  /* eval function (overloaded to be variadic)
    - input: the multi-index
    - output: the value
  */
  double eval(unsigned int iComp,...){
    vector<unsigned int> ind(m_nVar);
    va_list ap;
    va_start(ap, iComp);
    ind[0] = iComp;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      ind[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);

    if(m_trainSize[0][1] == 0){
      return 0.0;
    }
    else{
      vector<double> firstVec(m_trainSize[0][1]);
      unsigned int iDof = ind[0];
      for(unsigned int iR=0; iR<m_trainSize[0][1]; iR++){
        firstVec[iR] = m_train[0][0][iR].getVecEl(iDof);
      }

      for(unsigned int iVar=1; iVar<m_nVar; iVar++){
        vector<double> res(m_trainSize[iVar][1]);
        iDof = ind[iVar];
        for(unsigned int jRes=0; jRes<m_trainSize[iVar][1]; jRes++){
          res[jRes] = 0.0;
          for(unsigned int iRes=0; iRes<m_trainSize[iVar][0]; iRes++){
            double val = m_train[iVar][iRes][jRes].getVecEl(iDof);
            res[jRes] += val * firstVec[iRes];
          }
        }
        // copy into firstVec
        firstVec.clear();
        firstVec.resize(m_trainSize[iVar][1]);
        for(unsigned int iR=0; iR<m_trainSize[iVar][1]; iR++){
          firstVec[iR] = res[iR];
        }
        res.clear();
      }
      // return
      return firstVec[0];
    }
  };


  // Nested proxy class for accessing and manipulating tensor elements
  // overloading operator () in assignement through a proxy class:
  class Proxy : public tensor
  {
    vector<unsigned int> idx;
    vector<vector<vector<vec> > >* x_train;
    vector<unsigned int>* x_ranks;
    vector<vector<unsigned int> >* x_trainSize;

  public:

      // Set Tensor Elements:
      inline void set_tensorElement(vector<unsigned int> ind, double val){
        double actual = eval(ind);
        double toBeAdded = val - actual;

        // first:
        vec first(m_nDof_var[0], m_comm);
        first.setVecEl(ind[0], 1.0);
        first.finalize();
        (*x_ranks)[0] += 1;
        (*x_trainSize)[0][1] += 1;
        (*x_train)[0][0].push_back(first);

        // directions from second to last-1:
        for(unsigned int iVar=1; iVar<m_nVar-1; iVar++){
          (*x_ranks)[iVar] += 1;
          (*x_trainSize)[iVar][0] += 1;
          (*x_trainSize)[iVar][1] += 1;
          (*x_train)[iVar].resize((*x_trainSize)[iVar][0]);
          for(unsigned int iR=0; iR< (*x_trainSize)[iVar][0]; iR++){
            (*x_train)[iVar][iR].resize((*x_trainSize)[iVar][1]);
          }
          unsigned int iR = (*x_trainSize)[iVar][0]-1;
          for(unsigned int jR=0; jR < (*x_trainSize)[iVar][1]-1; jR++){
            vec toPut(m_nDof_var[iVar], m_comm);
            toPut.finalize();
            (*x_train)[iVar][iR][jR] = toPut;
          }
          unsigned int jR = (*x_trainSize)[iVar][1]-1;
          for(unsigned int iR=0; iR< (*x_trainSize)[iVar][0]-1; iR++){
            vec toPut(m_nDof_var[iVar], m_comm);
            toPut.finalize();
            (*x_train)[iVar][iR][jR] = toPut;
          }
          vec toPut(m_nDof_var[iVar], m_comm);
          toPut.setVecEl(ind[iVar], 1.0);
          toPut.finalize();
          (*x_train)[iVar][(*x_trainSize)[iVar][0]-1][(*x_trainSize)[iVar][1]-1] = toPut;
        }

        // last direction:
        (*x_trainSize)[m_nVar-1][0] += 1;
        (*x_train)[m_nVar-1].resize((*x_trainSize)[m_nVar-1][0]);
        (*x_train)[m_nVar-1][(*x_trainSize)[m_nVar-1][0]-1].resize(1);
        vec last(m_nDof_var[m_nVar-1], m_comm);
        last.setVecEl(ind[m_nVar-1], toBeAdded);
        last.finalize();
        (*x_train)[m_nVar-1][(*x_trainSize)[m_nVar-1][0]-1][0] = last;
      }

      // overloaded to return a double:
      double eval(const vector<unsigned int>& ind){
        if((*x_trainSize)[0][1] == 0){
          return 0.0;
        }
        else{
          vector<double> firstVec((*x_trainSize)[0][1]);
          unsigned int iDof = ind[0];
          for(unsigned int iR=0; iR< (*x_trainSize)[0][1]; iR++){
            firstVec[iR] = (*x_train)[0][0][iR].getVecEl(iDof);
          }

          for(unsigned int iVar=1; iVar<m_nVar; iVar++){
            vector<double> res((*x_trainSize)[iVar][1]);
            iDof = ind[iVar];
            for(unsigned int jRes=0; jRes< (*x_trainSize)[iVar][1]; jRes++){
              res[jRes] = 0.0;
              for(unsigned int iRes=0; iRes< (*x_trainSize)[iVar][0]; iRes++){
                double val = (*x_train)[iVar][iRes][jRes].getVecEl(iDof);
                res[jRes] += val * firstVec[iRes];
              }
            }
            // copy into firstVec
            firstVec.clear();
            firstVec.resize((*x_trainSize)[iVar][1]);
            for(unsigned int iR=0; iR< (*x_trainSize)[iVar][1]; iR++){
              firstVec[iR] = res[iR];
            }
            res.clear();
          }
          // return
          return firstVec[0];
        }
      }

      // Constructor of the proxy class:
      Proxy(vector<unsigned int> idx, vector<unsigned int> dofPerDim, vector<vector< vector<vec> > >* x_train, vector<unsigned int>* x_ranks, vector<vector<unsigned int> >* x_trainSize , MPI_Comm theComm) : idx(idx), x_train(x_train), x_ranks(x_ranks), x_trainSize(x_trainSize){
        m_comm = theComm;
        m_nVar = idx.size();
        m_nDof_var.resize(m_nVar);
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          m_nDof_var[iVar] = dofPerDim[iVar];
        }
        compute_minc();
      }

      // equal operator overload:
      inline double operator= (double value) {
        set_tensorElement(idx, value);
        return value;
      }

      // Overloading the double operator for assignement:
      operator double(){
        double toBeReturned = eval(idx);
        return toBeReturned;
      }
  };


  Proxy operator() (unsigned int I,...) {

    vector<unsigned int> ind(m_nVar);
    va_list ap;
    va_start(ap, I);
    ind[0] = I;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      ind[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);
    return Proxy(ind, m_nDof_var, &m_train, &m_ranks, &m_trainSize, m_comm);
  }



  // Methods and operations for Tensor Train:
  // COPY Tensor Structure:
	void copyTensorStructFrom(TensorTrain&);

	// COPY the full tensor:
	void copyTensorFrom(TensorTrain&);

  // OPERATIONS on Tensor Train (generic):
	bool hasSameStructure(TensorTrain&);
	bool isEqualTo(TensorTrain&);
  bool operator == (TensorTrain&);
	void sum(TensorTrain&, double, bool);
  void operator += (TensorTrain&);
	void multiplyByScalar(double);
  void operator *= (double);
	void shiftByScalar(double);
  void operator += (double);
	void extractSubtensor(vector<unsigned int>, TensorTrain&);
  TensorTrain extractSubtensor(vector<unsigned int>);
  void extractSubtensor(vector<vector<unsigned int> >, TensorTrain&);
  TensorTrain extractSubtensor(vector<vector<unsigned int> >);
	void assignSubtensor(vector<unsigned int>, TensorTrain&);
	void reshape(vector<unsigned int>, TensorTrain&, bool);
  TensorTrain reshape(vector<unsigned int>, bool);
  TensorTrain modeIContraction(unsigned int, vec);
  void modeIContraction(unsigned int, vec, TensorTrain&);

  // specific to TT:
  vector<vector<vec> > leftMultiplyCoreByMatrix(unsigned int, mat&);
  vector<vector<vec> > rightMultiplyCoreByMatrix(unsigned int, mat&);
  // -- TT-SVD from CP:
  void TTsvd(CPTensor&, double);
  void inplace_pairVars(CPTensor&, unsigned int, unsigned int);
  void contractTensor(CPTensor&, unsigned int, vector<vec>&);
  // -- TT-SVD from a full tensor:
  void TTsvd(fullTensor&, double, bool);
  void inplace_pairVars(fullTensor&, unsigned int, unsigned int);
  void contractTensor(fullTensor&, unsigned int, vector<vec>&);
  void contractTensorNoUnfold(fullTensor&, unsigned int, vector<vec>&);
  double update_residual(fullTensor&);
  // -- TT-SVD from a sparse tensor:
  void TTsvd(sparseTensor&, double);
  void inplace_pairVars(sparseTensor&, unsigned int, unsigned int);
  void contractTensor(sparseTensor&, unsigned int, vector<vec>&);
  double update_residual(sparseTensor&);

  // -- TT rounding --
  void round(double);


// end of class:
};


#endif
