// implementation of operations between TTs

#include "TTOperations.hpp"



/* 1 -- sum of two TTs:
  - input: T1, T2
  - output: T1 + T2
*/
TensorTrain operator + (TensorTrain& T1, TensorTrain& T2){
  // check the inputs consistency:
  assert(T1.nVar() == T2.nVar());
  for(unsigned int iVar=0; iVar<T1.nVar(); iVar++){
    assert(T1.nDof_var(iVar) == T2.nDof_var(iVar));
  }
  // sum the TTs:
  TensorTrain toBeReturned;
  toBeReturned.copyTensorFrom(T1);
  toBeReturned.sum(T2, 1.0, true);
  return toBeReturned;
}



/* 2 -- scalar product between two TTs:
  - input: T1, T2
  - output: T1*T2
*/
double operator * (TensorTrain& T1, TensorTrain& T2){
  // check the inputs consistency:
  assert(T1.nVar() == T2.nVar());
  for(unsigned int iVar=0; iVar<T1.nVar(); iVar++){
    assert(T1.nDof_var(iVar) == T2.nDof_var(iVar));
  }
  const unsigned int nVar = T1.nVar();

  // indices structure for T1:
  unsigned int n_pure_terms_T1 = 1;
  for(unsigned int iVar=0; iVar<nVar-1; iVar++){
    n_pure_terms_T1 *= T1.ranks(iVar);
  }

  vector<unsigned int> lin_inc_T1(nVar-1);
  lin_inc_T1[0] = 1;
  for(unsigned int iVar=1; iVar<nVar-1; iVar++){
    lin_inc_T1[iVar] = lin_inc_T1[iVar-1] * T1.ranks(iVar-1);
  }

  // indices structure for T2:
  unsigned int n_pure_terms_T2 = 1;
  for(unsigned int iVar=0; iVar<nVar-1; iVar++){
    n_pure_terms_T2 *= T2.ranks(iVar);
  }

  vector<unsigned int> lin_inc_T2(nVar-1);
  lin_inc_T2[0] = 1;
  for(unsigned int iVar=1; iVar<nVar-1; iVar++){
    lin_inc_T2[iVar] = lin_inc_T2[iVar-1] * T2.ranks(iVar-1);
  }

  // compute the scalar product:
  double toBeReturned = 0.0;
  for(unsigned int i_1=0; i_1<n_pure_terms_T1; i_1++){

    vector<vec> pure_term_1(nVar);
    // index structure T1:
    vector<unsigned int> index_1(nVar-1);
    unsigned int remainder = i_1;
    for(unsigned int iVar=0; iVar<nVar-1; iVar++){
      index_1[nVar-2-iVar] = remainder/lin_inc_T1[nVar-2-iVar]; // integer division.
      remainder = remainder - lin_inc_T1[nVar-2-iVar] * index_1[nVar-2-iVar];
    }
    // assign the pure term:
    for(unsigned int iVar=0; iVar<nVar; iVar++){
      if( (iVar==0) || (iVar==nVar-1) ){
        if(iVar==0){
          unsigned int iTT = index_1[0];
          pure_term_1[0] = T1.train(iVar,0,iTT);
        }
        else{
          unsigned int iTT = index_1[nVar-2];
          pure_term_1[nVar-1] = T1.train(iVar,iTT,0);
        }
      }
      else{
        unsigned int I = index_1[iVar-1];
        unsigned int J = index_1[iVar];
        pure_term_1[iVar] = T1.train(iVar,I,J);
      }
    }

    for(unsigned int i_2=0; i_2<n_pure_terms_T2; i_2++){

      vector<vec> pure_term_2(nVar);
      // index structure T1:
      vector<unsigned int> index_2(nVar-1);
      unsigned int remainder = i_2;
      for(unsigned int iVar=0; iVar<nVar-1; iVar++){
        index_2[nVar-2-iVar] = remainder/lin_inc_T2[nVar-2-iVar]; // integer division.
        remainder = remainder - lin_inc_T2[nVar-2-iVar] * index_2[nVar-2-iVar];
      }
      // assign the pure term:
      for(unsigned int iVar=0; iVar<nVar; iVar++){
        if( (iVar==0) || (iVar==nVar-1) ){
          if(iVar==0){
            unsigned int iTT = index_2[0];
            pure_term_2[0] = T2.train(iVar,0,iTT);
          }
          else{
            unsigned int iTT = index_2[nVar-2];
            pure_term_2[nVar-1] = T2.train(iVar,iTT,0);
          }
        }
        else{
          unsigned int I = index_2[iVar-1];
          unsigned int J = index_2[iVar];
          pure_term_2[iVar] = T2.train(iVar,I,J);
        }
      }
      // compute the scalar product between pure terms and sum it:
      double contrib = 1.0;
      for(unsigned int iVar=0; iVar<nVar; iVar++){
        double value = scalProd(pure_term_1[iVar],pure_term_2[iVar]);
        contrib *= value;
      }
      toBeReturned += contrib;
      // end of double loop.
    }
  }
  return toBeReturned;
}
