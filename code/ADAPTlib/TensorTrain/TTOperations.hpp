// Operations between Tensor Trains:
#ifndef TTOperations_hpp
#define TTOperations_hpp

#include "TensorTrain.h"
#include "../linAlg/linAlg.h"
using namespace std;

/* 1 -- sum of two TTs:
  - input: T1, T2
  - output: T1 + T2
*/
TensorTrain operator + (TensorTrain&, TensorTrain&);

/* 2 -- scalar product of two TTs:
  - input: T1, T2
  - output: T1*T2
*/
double operator * (TensorTrain&, TensorTrain&);

#endif
