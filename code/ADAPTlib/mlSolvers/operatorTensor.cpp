// implementation of operatorTensor class methods

#include "operatorTensor.h"

// Overloaded Constructors:

// -- empty resized operator --
operatorTensor::operatorTensor(unsigned int nTerms, unsigned int dim, MPI_Comm theComm){
    m_numTerms = nTerms;
    m_nVar = dim;
    m_comm = theComm;

    m_op.resize(nTerms);
    for(unsigned int iTerm=0; iTerm<m_numTerms; iTerm++){
      m_op[iTerm].resize(m_nVar);
    }
}

/* -- one term tensorised operator --
  - input: the terms of the rank one operator
  - output: contruction
*/
operatorTensor::operatorTensor(vector<mat>& oneTermOp, MPI_Comm theComm){
  m_numTerms = 1;
  m_nVar = oneTermOp.size();
  m_comm = theComm;

  m_op.push_back(oneTermOp);
}

/* -- multiple term tensorised operator --
  - input: the terms of the operator
  - output: contruction
*/
operatorTensor::operatorTensor(vector<vector<mat> >& Op, MPI_Comm theComm){
  m_numTerms = Op.size();
  m_nVar = Op[0].size();
  m_comm = theComm;

  m_op = Op;
}


// init after empty Constructors:

// -- empty resized operator --
void operatorTensor::init(unsigned int nTerms, unsigned int dim, MPI_Comm theComm){
    m_numTerms = nTerms;
    m_nVar = dim;
    m_comm = theComm;

    m_op.resize(nTerms);
    for(unsigned int iTerm=0; iTerm<m_numTerms; iTerm++){
      m_op[iTerm].resize(m_nVar);
    }
}

/* -- one term tensorised operator --
  - input: the terms of the rank one operator
  - output: contruction
*/
void operatorTensor::init(vector<mat>& oneTermOp, MPI_Comm theComm){
  m_numTerms = 1;
  m_nVar = oneTermOp.size();
  m_comm = theComm;

  m_op.push_back(oneTermOp);
}

/* -- multiple term tensorised operator --
  - input: the terms of the operator
  - output: contruction
*/
void operatorTensor::init(vector<vector<mat> >& Op, MPI_Comm theComm){
  m_numTerms = Op.size();
  m_nVar = Op[0].size();
  m_comm = theComm;

  m_op = Op;
}




/* add a given operator tensor
  - input and operator tensor
  - output: this += A
  REMARK: A is passed by reference, its matrices are NOT copied.
*/
void operatorTensor::add(operatorTensor& A){
  assert(A.comm() == m_comm);
  assert(A.nVar() == m_nVar);
  const unsigned int current_rank = m_numTerms;
  m_numTerms += A.numTerms();
  m_op.resize(m_numTerms);
  for(unsigned int iTerm=0; iTerm<A.numTerms(); iTerm++){
    m_op[iTerm + current_rank].resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      mat M = A.op(iTerm,iVar);
      m_op[iTerm + current_rank][iVar] = M;
    }
  }
}




/* -- apply the operator --
- input => non-empty CP;
- output => an empty CP is filled with the result
*/
void operatorTensor::applyOperator(CPTensor& input, CPTensor& result){

    assert(m_nVar == input.nVar());

    unsigned int nTermsResult = input.rank() * m_numTerms;

    vector<unsigned int> dofPerDim(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      assert(m_op[0][iVar].nCols() == input.nDof_var(iVar));
      dofPerDim[iVar] = m_op[0][iVar].nRows();
    }
    result.init(dofPerDim, input.comm());

    if(!m_isZero){
      // init result fields:
      vector<double> coeffs(nTermsResult);
      vector<vector<vec> > terms(nTermsResult);
      unsigned int cc = 0;
      for(unsigned int iTerm=0; iTerm<input.rank(); iTerm++){

        for(unsigned int lTerm=0; lTerm<m_numTerms; lTerm++){

          vector<vec> thisTerm(m_nVar);
          for(unsigned int iVar=0; iVar<m_nVar; iVar++){
            vec term_li = input.terms(iTerm,iVar);
            vec prod = m_op[lTerm][iVar] * term_li;
            thisTerm[iVar] = prod;
          }
          coeffs[cc] = input.coeffs(iTerm);
          terms[cc] = thisTerm;
          cc = cc + 1;
        }
      }
      result.set_terms(terms);
      result.set_coeffs(coeffs);
      result.get_rank();
    }
}



// -- apply the operator, overloaded function
// input => non-empty CP;
// output => the resulting CP;
CPTensor operatorTensor::applyOperator(CPTensor& input){
  assert(m_nVar == input.nVar());

  unsigned int nTermsResult = input.rank() * m_numTerms;

  vector<unsigned int> dofPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    assert(m_op[0][iVar].nCols() == input.nDof_var(iVar));
    dofPerDim[iVar] = m_op[0][iVar].nRows();
  }
  CPTensor result(dofPerDim, input.comm());
  if(!m_isZero){

    // init result fields:
    vector<double> coeffs(nTermsResult);
    vector<vector<vec> > terms(nTermsResult);

    unsigned int cc = 0;
    for(unsigned int iTerm=0; iTerm<input.rank(); iTerm++){

      for(unsigned int lTerm=0; lTerm<m_numTerms; lTerm++){

        vector<vec> thisTerm(m_nVar);
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          vec term_li = input.terms(iTerm,iVar);
          vec prod = m_op[lTerm][iVar] * term_li;
          thisTerm[iVar] = prod;
        }

        terms[cc] = thisTerm;
        coeffs[cc] = input.coeffs(iTerm);
        cc = cc + 1;
      }
    }
    result.set_terms(terms);
    result.set_coeffs(coeffs);
    result.get_rank();
  }
  return result;
}


/* apply a given operator to a Tucker tensor:
  - input: Tucker
  - output: Tucker given as an empty Tucker
*/
void operatorTensor::applyOperator(Tucker& input, Tucker& output){
  if(input.isEmpty()){
    PetscPrintf(input.comm(), "Warning! Input operator is empty\n");
  }
  // computing the resolution:
  vector<unsigned int> dofPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    assert(m_op[0][iVar].nCols() == input.nDof_var(iVar));
    dofPerDim[iVar] = m_op[0][iVar].nRows();
  }
  output.init(dofPerDim, input.comm());

  if(!m_isZero){

    // computing output ranks:
    vector<unsigned int> outRanks(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      outRanks[iVar] = m_numTerms * input.ranks(iVar);
    }
    output.set_ranks(outRanks);

    // copy the core:
    fullTensor out_core(outRanks, m_comm);
    fullTensor in_core = input.core();
    for(unsigned int lTerm=0; lTerm<m_numTerms; lTerm++){
      vector<unsigned int> bounds(2*m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        bounds[2*iVar] = lTerm * input.ranks(iVar);
        bounds[2*iVar+1] = (lTerm + 1) * input.ranks(iVar);
      }
      out_core.assignSubtensor(bounds, in_core);
    }
    out_core.finalize();

    output.set_core(out_core);

    // computing the modes:
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      for(unsigned int lTerm=0; lTerm<m_numTerms; lTerm++){
        for(unsigned int iTerm=0; iTerm<input.ranks(iVar); iTerm++){
          unsigned int iMod = lTerm * input.ranks(iVar) + iTerm;
          vec in_vec = input.modes(iVar, iTerm);
          vec out_vec = m_op[lTerm][iVar] * in_vec;
          output.set_modes(iVar, iMod, out_vec);
        }
      }
    }
  }
}


/* apply a given operator to a Tucker tensor (overloaded):
  - input: Tucker
  - output: Tucker
*/
Tucker operatorTensor::applyOperator(Tucker& input){
  if(input.isEmpty()){
    PetscPrintf(input.comm(), "Warning! Input operator is empty\n");
  }

  // computing the resolution:
  vector<unsigned int> dofPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    assert(m_op[0][iVar].nCols() == input.nDof_var(iVar));
    dofPerDim[iVar] = m_op[0][iVar].nRows();
  }
  Tucker output(dofPerDim, input.comm());

  if(!m_isZero){
    // computing output ranks:
    vector<unsigned int> outRanks(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      outRanks[iVar] = m_numTerms * input.ranks(iVar);
    }
    output.set_ranks(outRanks);

    // copy the core:
    fullTensor out_core(outRanks, m_comm);
    fullTensor in_core = input.core();
    for(unsigned int lTerm=0; lTerm<m_numTerms; lTerm++){
      vector<unsigned int> bounds(2*m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        bounds[2*iVar] = lTerm * input.ranks(iVar);
        bounds[2*iVar+1] = (lTerm + 1) * input.ranks(iVar);
      }
      out_core.assignSubtensor(bounds, in_core);
    }
    out_core.finalize();

    output.set_core(out_core);

    // computing the modes:
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      for(unsigned int lTerm=0; lTerm<m_numTerms; lTerm++){
        for(unsigned int iTerm=0; iTerm<input.ranks(iVar); iTerm++){
          unsigned int iMod = lTerm * input.ranks(iVar) + iTerm;
          vec in_vec = input.modes(iVar, iTerm);
          vec out_vec = m_op[lTerm][iVar] * in_vec;
          output.set_modes(iVar, iMod, out_vec);
        }
      }
    }
  }
  return output;
}


/* apply a given operator to a Tensor Train:
  - input: TT
  - output: TT given as an empty TT
*/
void operatorTensor::applyOperator(TensorTrain& input, TensorTrain& output){
  assert(input.nVar()==m_nVar);
  vector<unsigned int> dofPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    dofPerDim[iVar] = m_op[0][iVar].nRows();
  }
  MPI_Comm m_comm = input.comm();
  output.init(dofPerDim, m_comm);

  if(!m_isZero){
    for(unsigned int lTerm=0; lTerm<m_numTerms; lTerm++){
      TensorTrain lTT(dofPerDim, m_comm);
      lTT.set_ranks(input.ranks());
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        vector<vector<vec> > in_fibers = input.train(iVar);
        const unsigned int I = input.trainSize(iVar,0);
        const unsigned int J = input.trainSize(iVar,1);
        vector<vector<vec> > out_fibers(I);
        for(unsigned int i=0; i<I; i++){
          out_fibers.resize(J);
          for(unsigned int j=0; j<J; j++){
            out_fibers[i][j] = m_op[lTerm][iVar] * in_fibers[i][j];
          }
        }
        lTT.set_train(iVar, out_fibers);
      }
      // summing to output: output.sum(lTT, 1.0, false); //or with copy:
      output += lTT;
      lTT.clear();
    }
  }
}


/* apply a given operator to a Tensor Train (overloaded):
  - input: TT
  - output: TT
*/
/*TensorTrain operatorTensor::applyOperator(TensorTrain& input){
  assert(input.nVar()==m_nVar);
	cout << "entering function" << endl;
  vector<unsigned int> dofPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    dofPerDim[iVar] = m_op[0][iVar].nRows();
  }
  MPI_Comm m_comm = input.comm();
  TensorTrain output(dofPerDim, m_comm);

	cout << "output initialized" << endl;

  for(unsigned int lTerm=0; lTerm<m_numTerms; lTerm++){
    TensorTrain lTT(dofPerDim, m_comm);
    lTT.set_ranks(input.ranks());
    lTT.resizeTrain();
	cout << "lTerm = " << lTerm << endl;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      vector<vector<vec> > in_fibers = input.train(iVar);
      const unsigned int I = input.trainSize(iVar,0);
      const unsigned int J = input.trainSize(iVar,1);
	cout << "I = " << I << "   J=" << J << endl;
      vector<vector<vec> > out_fibers(I);
      for(unsigned int i=0; i<I; i++){
        out_fibers[i].resize(J);
        for(unsigned int j=0; j<J; j++){
	   cout << "i = " << i << "   j=" << j << endl;
	   cout << "mat" << endl;
	   m_op[lTerm][iVar].print();

	   cout << "vec" << endl;
	   in_fibers[i][j].print();

          out_fibers[i][j] = m_op[lTerm][iVar] * in_fibers[i][j];
		cout << "mult done" << endl;
        }
      }
      lTT.set_train(iVar, out_fibers);
    }
    // summing to output: output.sum(lTT, 1.0, false); //or with copy:
    output += lTT;
    cout << "DONE" << endl;
    lTT.clear();
  }
  return output;
}*/

TensorTrain operatorTensor::applyOperator(TensorTrain& input){
  assert(input.nVar()==m_nVar);
  vector<unsigned int> dofPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    dofPerDim[iVar] = m_op[0][iVar].nRows();
  }
  MPI_Comm m_comm = input.comm();
  TensorTrain output(dofPerDim, m_comm);

  if(!m_isZero){
    for(unsigned int lTerm=0; lTerm<m_numTerms; lTerm++){
      vector<vector<vector<vec> > > thisTerm(m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        const unsigned int I = input.trainSize(iVar,0);
        const unsigned int J = input.trainSize(iVar,1);
        thisTerm[iVar].resize(I);
        for(unsigned int i=0; i<I; i++){
          thisTerm[iVar][i].resize(J);
          for(unsigned int j=0; j<J; j++){
            vec fibre = input.train(iVar,i,j);
            thisTerm[iVar][i][j] = m_op[lTerm][iVar] * fibre;
          }
        }
      }
      TensorTrain termToAdd(thisTerm);
      output += termToAdd;
    }
  }
  return output;
}


// -- apply a given operator to a CPTensor2Var
// input => non-empty CPTensor2Var;
// output => the resulting CPTensor2Var;
CPTensor2Var operatorTensor::applyOperator(const CPTensor2Var& input) const {
  assert(m_nVar==2);

  std::vector<double> coeffs(input.rank()*m_numTerms);
  for(unsigned int jTerm=0, lTerm=0; jTerm<m_numTerms; ++jTerm) {
    for(unsigned int iTerm=0; iTerm<input.rank(); ++iTerm) {
      coeffs[lTerm] = input.coeffs(iTerm);
      ++lTerm;
    }
  }

  std::vector<vec> terms_x(input.rank()*m_numTerms);
  for(unsigned int jTerm=0, lTerm=0; jTerm<m_numTerms; ++jTerm) {
    for(unsigned int iTerm=0; iTerm<input.rank(); ++iTerm) {
      terms_x[lTerm] = m_op[jTerm][0]*input.terms_x(iTerm);
      ++lTerm;
    }
  }

  std::vector<vec> terms_y(input.rank()*m_numTerms);
   for(unsigned int jTerm=0, lTerm=0; jTerm<m_numTerms; ++jTerm) {
    for(unsigned int iTerm=0; iTerm<input.rank(); ++iTerm) {
      terms_y[lTerm] = m_op[jTerm][1]*input.terms_y(iTerm);
      ++lTerm;
    }
  }

  return CPTensor2Var(m_op[0][0].nRows(), m_op[0][1].nRows(), coeffs, terms_x, terms_y, input.comm());
}


// -- apply a given operator to a CPTensor2Var
// input => non-empty CPTensor2Var;
// output => the resulting CPTensor2Var;
void operatorTensor::applyOperator(const CPTensor2Var& input, CPTensor2Var& output) const {
  output = this->applyOperator(input);
}


//Adjoint of the OperatorTensor itself
operatorTensor operatorTensor::adjoint(){
  operatorTensor adjA(m_numTerms, m_nVar, m_comm);
  for (unsigned int iTerm = 0; iTerm < m_numTerms; iTerm++) {
    for (unsigned int iVar = 0; iVar < m_nVar; iVar++) {
       mat adj = transpose(m_op[iTerm][iVar]);
       adjA.set_term(iTerm, iVar, adj);
    }
  }
  return adjA;
};
