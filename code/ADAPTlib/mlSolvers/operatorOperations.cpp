// implementation of the functions defined in operatorOperations.h

#include "operatorOperations.h"


// sum of two operator tensors:
operatorTensor operator + (operatorTensor &O1, operatorTensor &O2){
     assert(O1.comm() == O2.comm());
     assert(O1.nVar() == O2.nVar());
     const unsigned int nT = O1.numTerms() + O2.numTerms();
     operatorTensor toBeReturned(nT, O1.nVar(), O1.comm());
     for(unsigned int iTerm=0; iTerm<O1.numTerms(); iTerm++){
       for(unsigned int iVar=0; iVar<O1.nVar(); iVar++){
         mat A = O1.op(iTerm, iVar);
         toBeReturned.set_term(iTerm, iVar, A);
       }
     }
     for(unsigned int iTerm=0; iTerm<O2.numTerms(); iTerm++){
       for(unsigned int iVar=0; iVar<O2.nVar(); iVar++){
         mat A = O2.op(iTerm, iVar);
         toBeReturned.set_term(iTerm + O1.numTerms(), iVar, A);
       }
     }
     return toBeReturned;
}


/* given a list of operator tensor, a list of indices [i,j], sizes, produce A[i,j] block operators
  - inputs: list of operator tensor, list of positions, sizes of the matrix
  - output: the block operator
*/
vector<vector<operatorTensor> > blockOperator(vector<operatorTensor> listOp, vector<vector<unsigned int> > listPos, unsigned int nRows, unsigned int nCols){
  assert(listOp.size()==listPos.size());
  vector<vector<operatorTensor> > toBeReturned(nRows);
  for(unsigned int iRow=0; iRow<nRows; iRow++){
    toBeReturned[nRows].resize(nCols);
  }

  for(unsigned int iOp=0; iOp<listOp.size(); iOp++){
    assert(listPos[iOp][0]<nRows);
    assert(listPos[iOp][1]<nCols);
    unsigned int iRow = listPos[iOp][0];
    unsigned int iCol = listPos[iOp][1];
    toBeReturned[iRow][iCol] = listOp[iOp];
  }
  return toBeReturned;
}



// Apply an operator tensor to a tensor in a given format:
CPTensor operator * (operatorTensor& O, CPTensor& A){
  CPTensor toBeReturned = O.applyOperator(A);
  return toBeReturned;
}

Tucker operator * (operatorTensor& O, Tucker& A){
  Tucker toBeReturned = O.applyOperator(A);
  return toBeReturned;
}

TensorTrain operator * (operatorTensor& O, TensorTrain& A){
  TensorTrain toBeReturned = O.applyOperator(A);
  return toBeReturned;
}
