// Header file for operator tensor

#ifndef blockOperatorTensor_h
#define blockOperatorTensor_h

#include "../linAlg/linAlg.h"
#include "../CP/CPTensor.h"
#include "../CP/CPTensor2Var.hpp"
#include "../fullTensor/fullTensor.h"
#include "../sparseTensor/sparseTensor.h"
#include "../TensorTrain/TensorTrain.h"
#include "../Tucker/Tucker.h"
#include "operatorTensor.h"
#include "operatorOperations.h"

using namespace std;

class blockOperatorTensor{

private:
  unsigned int m_nBlock;
  vector<vector<operatorTensor> > m_A;
  vector<vector<double> > m_coeffs;


  public:
    blockOperatorTensor(){};
    blockOperatorTensor(unsigned int);
    blockOperatorTensor(vector<vector<operatorTensor> >&);
    blockOperatorTensor(vector<vector<operatorTensor> >&, vector<vector<double> >&);
    ~blockOperatorTensor(){};

    // SETTER:
    void set_nBlock(unsigned int n){m_nBlock=n;}
    void set_A(unsigned int i, unsigned int j, operatorTensor op){m_A[i][j] = op;};
    void set_coeffs(unsigned int i, unsigned int j, double alpha){m_coeffs[i][j] = alpha;}

    // METHODS:
    void resize(unsigned int n){
      m_A.resize(n);
      for(unsigned int iComp=0; iComp<n; iComp++){
        m_A[iComp].resize(n);
      }
      m_coeffs.resize(n);
      for(unsigned int iComp=0; iComp<n; iComp++){
        m_coeffs[iComp].resize(n);
      }
    }

    // ACCESS FUNCTIONS:
    inline unsigned int nBlock(){return m_nBlock;}
    inline vector<vector<operatorTensor> > A(){return m_A;}
    inline operatorTensor A(unsigned int i, unsigned int j){return m_A[i][j];}
    inline vector<vector<double> > coeffs(){return m_coeffs;}
    inline double coeffs(unsigned int i, unsigned int j){return m_coeffs[i][j];}

};

#endif
