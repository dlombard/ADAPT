#ifndef operatorOperations_h
#define operatorOperations_h

#include "operatorTensor.h"

/* 1 -- sum of two operator tensors
  - input: O1, O2
  - output: O1 + O2
*/
operatorTensor operator + (operatorTensor& O1, operatorTensor& O2);


vector<vector<operatorTensor> > blockOperator(vector<operatorTensor>, vector<vector<unsigned int> >, unsigned int, unsigned int);


/* 2 -- apply an operator to a tensor
  - input: O, tensor A
  - output: tensor O*A
*/
CPTensor operator * (operatorTensor& O, CPTensor& A);

Tucker operator * (operatorTensor& O, Tucker& A);

TensorTrain operator * (operatorTensor& O, TensorTrain& A);



#endif
