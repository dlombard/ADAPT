// Implementation of multilinear solver class

#include "multilinearSolver.h"


/* solving operator tensor rank-one systems
  - input: the rank-one operator tensor, the rhs in CP format
  - output: the result
*/
CPTensor multilinearSolver::oneRankSolve(operatorTensor& A, CPTensor& rhs){
  assert(A.numTerms() == 1);
  const unsigned int m_nVar = A.op()[0].size();
  vector<unsigned int> dofPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    dofPerDim[iVar] = A.op()[0][iVar].nCols();
  }
  // initialising the output:
  CPTensor output(dofPerDim, rhs.comm());
  unsigned int outRank = rhs.rank();
  output.set_rank(outRank);

  // solving the system:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    linearSolver iSolver;
    iSolver.init(A.op(0,iVar));
    for(unsigned int iTerm=0; iTerm<outRank; iTerm++){
      vec inputRhs = rhs.terms(iTerm, iVar);
      vec outFiber = iSolver.solve(inputRhs, false);
      output.set_terms(iTerm, iVar, outFiber);
    }
    iSolver.clear();
  }

  // setting the coefficients:
  for(unsigned int iTerm=0; iTerm<outRank; iTerm++){
    double val = rhs.coeffs(iTerm);
    output.set_coeffs(iTerm, val);
  }

  m_result = &output;
  return output;
}


/* solving operator tensor rank-one systems
  - input: the rank-one operator tensor, the rhs in Tucker format
  - output: the result in Tucker format
*/
Tucker multilinearSolver::oneRankSolve(operatorTensor& A, Tucker& rhs){
  assert(A.numTerms() == 1);
  const unsigned int m_nVar = A.op()[0].size();
  vector<unsigned int> dofPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    dofPerDim[iVar] = A.op()[0][iVar].nCols();
  }
  // initialising the output:
  Tucker output(dofPerDim, rhs.comm());
  fullTensor rhsCore = rhs.core();
  fullTensor outCore;
  outCore.copyTensorFrom(rhsCore);
  output.set_core(outCore);
  vector<unsigned int> outRanks = outCore.nDof_var();

  // solving the system:
  vector<vector<vec> > outModes(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    linearSolver iSolver;
    iSolver.init(A.op(0,iVar));
    outModes[iVar].resize(outRanks[iVar]);
    for(unsigned int iMod=0; iMod<outRanks[iVar]; iMod++){
      vec inRhs = rhs.modes(iVar,iMod);
      vec outFiber = iSolver.solve(inRhs, false);
      outModes[iVar][iMod] = outFiber;
    }
    iSolver.clear();
  }

  output.set_modes(outModes);
  output.get_ranks();

  m_result = &output;
  return output;
}


/* solving operator tensor rank-one systems
  - input: the rank-one operator tensor, the rhs in TensorTrain format
  - output: the result in TensorTrain format
*/
TensorTrain multilinearSolver::oneRankSolve(operatorTensor& A, TensorTrain& rhs){
  assert(A.numTerms() == 1);
  const unsigned int m_nVar = A.op()[0].size();
  vector<unsigned int> dofPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    dofPerDim[iVar] = A.op()[0][iVar].nCols();
  }
  // initialising the output:
  TensorTrain output(dofPerDim, rhs.comm());
  vector<unsigned int> outRanks = rhs.ranks();
  output.set_ranks(outRanks);
  output.resizeTrain();

  // solving the system:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    linearSolver iSolver;
    iSolver.init(A.op(0,iVar));
    for(unsigned int I=0; I<output.trainSize(iVar,0); I++){
      for(unsigned int J=0; J<output.trainSize(iVar,1); J++){
        vec ij_Rhs = rhs.train(iVar, I, J);
        vec outFiber = iSolver.solve(ij_Rhs, false);
        output.set_train(iVar, I, J, outFiber);
      }
    }
    iSolver.clear();
  }

  m_result = &output;
  return output;
}
