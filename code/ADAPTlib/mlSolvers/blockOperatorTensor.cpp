// Implementation of blockOperatorTensor class

#include "blockOperatorTensor.h"


// I -- CONSTRUCTORS
blockOperatorTensor::blockOperatorTensor(unsigned int n){
  m_nBlock = n;
  m_A.resize(n);
  for(unsigned int iComp=0; iComp<n; iComp++){
    m_A[iComp].resize(n);
  }
  m_coeffs.resize(n);
  for(unsigned int iComp=0; iComp<n; iComp++){
    m_coeffs[iComp].resize(n);
  }
}
