// Header of multilinear solver class: solving multi-linear systems

#ifndef multilinearSolver_h
#define multilinearSolver_h

#include "../linAlg/linAlg.h"
#include "../tensor.h"
#include "operatorTensor.h"
#include "../fullTensor/fullTensor.h"
#include "../CP/CPTensor.h"
#include "../Tucker/Tucker.h"
#include "../TensorTrain/TensorTrain.h"
#include "../HPFTucker/HPFTucker.h"
#include "../HPFCP/HPFCP.h"


class multilinearSolver{
private:
  double m_tol;
  tensor* m_result;
  unsigned int m_verbosity;

public:
  multilinearSolver(){};
  ~multilinearSolver(){};

  // ACCESS FUNCTIONS:
  double tol(){return m_tol;}
  unsigned int verbosity(){return m_verbosity;}
  tensor result(){return *m_result;}

  // SETTERS:
  void set_tol(double tolerance){m_tol=tolerance;}
  void set_verbosity(unsigned int verb){m_verbosity=verb;}

  // METHODS:
  CPTensor oneRankSolve(operatorTensor&, CPTensor&);
  Tucker oneRankSolve(operatorTensor&, Tucker&);
  TensorTrain oneRankSolve(operatorTensor&, TensorTrain&);


};



#endif
