// Header file for operator tensor

#ifndef operatorTensor_h
#define operatorTensor_h

#include "../linAlg/linAlg.h"
#include "../CP/CPTensor.h"
#include "../CP/CPTensor2Var.hpp"
#include "../fullTensor/fullTensor.h"
#include "../sparseTensor/sparseTensor.h"
#include "../TensorTrain/TensorTrain.h"
#include "../Tucker/Tucker.h"

using namespace std;

class operatorTensor{

private:
  MPI_Comm m_comm;
  unsigned int m_numTerms;
  unsigned int m_nVar;
  vector<vector<mat> > m_op;
  bool m_isZero;

public:

  operatorTensor(){};
  ~operatorTensor(){};
  inline void clear(){
    for(unsigned int iTerm=0; iTerm<m_numTerms; iTerm++){
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        m_op[iTerm][iVar].clear();
      }
    }
  }

  // overloaded CONSTRUCTOR:
  operatorTensor(unsigned int, unsigned int, MPI_Comm);
  operatorTensor(vector<mat>&, MPI_Comm);
  operatorTensor(vector<vector<mat> >&, MPI_Comm);

  // init after empty constructor:
  void init(unsigned int, unsigned int, MPI_Comm);
  void init(vector<mat>&, MPI_Comm);
  void init(vector<vector<mat> >&, MPI_Comm);


  // SETTERS
  inline void set_numTerms(unsigned int nTerms){m_numTerms = nTerms;};
  inline void set_nVar(unsigned int nVar){m_nVar = nVar;};
  inline void set_comm(MPI_Comm theComm){m_comm = theComm;};
  inline void set_term(unsigned int iTerm, unsigned int iVar, mat& A){m_op[iTerm][iVar] = A;};
  inline void set_zero(){m_isZero=true;}


  // ACCES FUNCTIONS:
  MPI_Comm comm(){return m_comm;}
  unsigned int numTerms(){return m_numTerms;};
  unsigned int nVar(){return m_nVar;};
  vector<vector<mat> > op(){return m_op;};
  vector<mat> op(unsigned int iTerm){return m_op[iTerm];};
  mat op(unsigned int iTerm, unsigned int jVar){return m_op[iTerm][jVar];};
  inline mat operator () (unsigned int iTerm, unsigned int jVar){
    return m_op[iTerm][jVar];
  }
  inline bool isZero(){return m_isZero;};

  // Operations:
  // -- operation between operator tensors:
  void add(operatorTensor&);
  inline void operator += (operatorTensor& O){
    add(O);
  }
  inline void operator *= (double alpha){
    for(unsigned int iTerm=0; iTerm<m_numTerms; iTerm++){
      m_op[iTerm][0] *= alpha;
    }
  }

  // -- apply the operator tensor to a tensor:
  void applyOperator(CPTensor&, CPTensor&);
  CPTensor applyOperator(CPTensor&);
  void applyOperator(Tucker&, Tucker&);
  Tucker applyOperator(Tucker&);
  void applyOperator(TensorTrain&, TensorTrain&);
  TensorTrain applyOperator(TensorTrain&);
  void applyOperator(const CPTensor2Var&, CPTensor2Var&) const;
  CPTensor2Var applyOperator(const CPTensor2Var&) const;

  operatorTensor adjoint();
};
#endif
