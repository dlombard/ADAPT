// Implementation of Tucker class:
#include "Tucker.h"
#include "../linAlg/linearAlgebraOperations.h"
#include "../linAlg/svd.h"

/* overloaded constructor:
  - input: dim, dofPerDim, Communicator
  - output: initialisation of the tensor
*/
Tucker::Tucker(unsigned int dim, vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = dim;
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  compute_minc();
  m_ranks.resize(m_nVar);
  m_modes.resize(m_nVar);
  m_isInitialised = true;
  m_isSigmaTableFilled = false;
}


/* overloaded constructor:
  - input: dofPerDim, Communicator
  - output: initialisation of the tensor
*/
Tucker::Tucker(vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = dofPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  compute_minc();
  m_ranks.resize(m_nVar);
  m_modes.resize(m_nVar);
  m_isInitialised = true;
  m_isSigmaTableFilled = false;
}


/* overloaded constructor:
  - input: core and modes
  - output: initialisation of the tensor
*/
Tucker::Tucker(fullTensor& core, vector<vector<vec> >& modes){
  assert(core.nVar() == modes.size());
  m_comm = core.comm();
  m_nVar = modes.size();
  m_core = core;
  m_modes = modes;
  m_ranks.resize(m_nVar);
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar = 0; iVar<m_nVar; iVar++){
    m_ranks[iVar] = modes[iVar].size();
    m_nDof_var[iVar] = modes[iVar][0].size();
  }
  compute_minc();
  m_isInitialised = true;
  m_isSigmaTableFilled = false;
}


/* overloaded constructor (init):
  - input: core and modes
  - output: initialisation of the tensor
*/
void Tucker::init(vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = dofPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  compute_minc();
  m_ranks.resize(m_nVar);
  m_modes.resize(m_nVar);
  m_isInitialised = true;
  m_isSigmaTableFilled = false;
}




// -- METHODS: --


/*  copy tensor structure from a given tensor:
  - input: the tensor whose structure is copied
  - output: the structure of this tensor is shaped
*/
void Tucker::copyTensorStructFrom(Tucker& source){
  if(m_isInitialised){clear();}
  m_comm = source.comm();
  m_nVar = source.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = source.nDof_var(iVar);
  }
  compute_minc();
  m_ranks.resize(m_nVar);
  m_modes.resize(m_nVar);
  m_isInitialised = true;
  m_isSigmaTableFilled = false;
}


/*  copy the whole tensor from a given tensor:
  - input: the tensor which is copied
  - output: this tensor is the copy
*/
void Tucker::copyTensorFrom(Tucker& source){
  copyTensorStructFrom(source);
  m_ranks.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_ranks[iVar] = source.ranks(iVar);
  }
  fullTensor core = source.core();
  m_core.copyTensorFrom(core);
  m_modes.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_modes[iVar].resize(m_ranks[iVar]);
    for(unsigned int iMod=0; iMod<m_ranks[iVar]; iMod++){
      vec toBeCopied = source.modes(iVar,iMod);
      m_modes[iVar][iMod].copyVecFrom(toBeCopied);
    }
  }
  if(source.isSigmaTableFilled()){
    m_sigmaTable.resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_sigmaTable[iVar].resize(source.sigmaTable(iVar).size());
      for(unsigned int iMod=0; iMod<source.sigmaTable(iVar).size(); iMod++){
        m_sigmaTable[iVar][iMod] = source.sigmaTable(iVar,iMod);
      }
    }
  }
}


/* check if the tensor has the same structure of a given one
 - input: the tensor whose structure is compared
 - output: a bool
*/
bool Tucker::hasSameStructure(Tucker& toBeComparedTo){
  bool toBeReturned = true;
  if(m_nVar != toBeComparedTo.nVar()){
    return false;
  }
  else{
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(m_nDof_var[idVar] != toBeComparedTo.nDof_var(idVar)){
        return false;
      }
    }
  }
  return toBeReturned;
}


/* check if the tensor is equal to a given one
 - input: the tensor to be compared
 - output: a bool
*/
bool Tucker::isEqualTo(Tucker& toBeCompared){
  bool isIt = hasSameStructure(toBeCompared);

  if(isIt){
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(m_ranks[iVar] != toBeCompared.ranks(iVar)){
        isIt = false;
        break;
      }
    }
    if(isIt){
      // check core:
      if(!(toBeCompared.core() == m_core)){
        isIt = false;
      }
      else{
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          for(unsigned int iMod=0; iMod<m_ranks[iVar]; iMod++){
            bool isThisMode = (m_modes[iVar][iMod] == toBeCompared.modes(iVar,iMod));
            if(!isThisMode){
              isIt = false;
              break;
            }
          }
        }
      }

    }
  }
  return isIt;
}


/* check if the tensor is equal to a given one (overloaded operator)
 - input: the tensor to be compared
 - output: a bool
*/
bool Tucker::operator == (Tucker& toBeCompared){
  return isEqualTo(toBeCompared);
}


/* sum of two Tucker tensors
  - inputs: Tucker, coefficient, bool to define if it is copied
  - output: T <- T + alpha S
*/
void Tucker::sum(Tucker& toSum, double alpha, bool COPY){
  if(!hasSameStructure(toSum)){
    PetscPrintf(m_comm, "Cannot be summed!\n");
    exit(1);
  }

  if(isEmpty()){
    toSum *= alpha;
    copyTensorFrom(toSum);
  }
  else{

    // copy the current ranks
    vector<unsigned int> currentRanks(m_nVar);
    for(unsigned int iVar=0; iVar< m_nVar; iVar++){
      currentRanks[iVar] = m_ranks[iVar];
    }
    // new ranks:
    for(unsigned int iVar=0; iVar< m_nVar; iVar++){
      m_ranks[iVar] = m_ranks[iVar] + toSum.ranks(iVar);
      m_modes[iVar].resize(m_ranks[iVar]);
      for(unsigned int iMod=0; iMod<toSum.ranks(iVar); iMod++){
        unsigned int iGlob = iMod + currentRanks[iVar];
        if(!COPY){
          m_modes[iVar][iGlob] = toSum.modes(iVar,iMod);
        }
        else{
          vec iThMode = toSum.modes(iVar,iMod);
          m_modes[iVar][iGlob].copyVecFrom(iThMode);
        }
      }
    }

    // the core:
    fullTensor currentCore;
    currentCore.copyTensorFrom(m_core);
    m_core.clear();
    m_core.init(m_ranks, m_comm);
    vector<unsigned int> firstBounds(2*m_nVar);
    for(unsigned int iB=0; iB<2*m_nVar; iB++){
      if(iB%2==0){
        firstBounds[iB] = 0;
      }
      else{
        unsigned int iVar = (iB-1)/2;
        firstBounds[iB] = currentRanks[iVar];
      }
    }
    m_core.assignSubtensor(firstBounds, currentCore);
    currentCore.clear();

    vector<unsigned int> secondBounds(2*m_nVar);
    for(unsigned int iB=0; iB<2*m_nVar; iB++){
      if(iB%2==0){
        unsigned int iVar = (iB)/2;
        secondBounds[iB] = currentRanks[iVar];
      }
      else{
        unsigned int iVar = (iB-1)/2;
        secondBounds[iB] = m_ranks[iVar];
      }
    }
    fullTensor toSumCore = toSum.core();
    if(!COPY){
      toSumCore *= alpha;
      m_core.assignSubtensor(secondBounds, toSumCore);
    }
    else{
      fullTensor coreToAdd;
      coreToAdd.copyTensorFrom(toSumCore);
      coreToAdd *= alpha;
      m_core.assignSubtensor(secondBounds, coreToAdd);
      coreToAdd.clear();
    }
    m_core.finalize();

  }
}


/* sum of two Tucker tensors (overloaded operator)
  - inputs: Tucker, coefficient, bool to define if it is copied
  - output: T <- T + S
*/
void Tucker::operator += (Tucker& toAdd){
  sum(toAdd, 1.0, true);
}


/* multiplying the current tensor by a scalar
  - input: the scalar
  - output: the core is multiplied by a scalar
*/
void Tucker::multiplyByScalar(double alpha){
  m_core *= alpha;
}


/* overloaded operator to multiply */
void Tucker:: operator *= (double alpha){
  m_core *= alpha;
}


/* shift by scalar:
 - input: the scalar value to shift
 - output: this tensor is shifted
*/
void Tucker::shiftByScalar(double alpha){
  fullTensor coreCopy;
  coreCopy.copyTensorFrom(m_core);
  m_core.clear();
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_ranks[iVar] = m_ranks[iVar]  + 1;
  }
  m_core.init(m_ranks, m_comm);
  vector<unsigned int> indBound(2*m_nVar);
  for(unsigned int iB=0; iB<2*m_nVar; iB++){
    if(iB%2 == 0){
      indBound[iB] = 0;
    }
    else{
      unsigned int iVar = (iB - 1)/2;
      indBound[iB] = m_ranks[iVar]-1;
    }
  }
  m_core.assignSubtensor(indBound, coreCopy);
  coreCopy.clear();
  vector<unsigned int> indToFill(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    indToFill[iVar] = m_ranks[iVar]-1;
  }
  m_core.set_tensorElement(indToFill, alpha);
  m_core.finalize();

  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    vec iThMode(m_nDof_var[iVar], m_comm);
    iThMode.ones();
    m_modes[iVar].push_back(iThMode);
  }
}


/* overloaded operator to shift the tensor:
  - input: alpha
*/
void Tucker::operator += (double alpha){
  fullTensor coreCopy;
  coreCopy.copyTensorFrom(m_core);
  m_core.clear();
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_ranks[iVar] = m_ranks[iVar]  + 1;
  }
  m_core.init(m_ranks, m_comm);
  vector<unsigned int> indBound(2*m_nVar);
  for(unsigned int iB=0; iB<2*m_nVar; iB++){
    if(iB%2 == 0){
      indBound[iB] = 0;
    }
    else{
      unsigned int iVar = (iB - 1)/2;
      indBound[iB] = m_ranks[iVar]-1;
    }
  }
  m_core.assignSubtensor(indBound, coreCopy);
  coreCopy.clear();
  vector<unsigned int> indToFill(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    indToFill[iVar] = m_ranks[iVar]-1;
  }
  m_core.set_tensorElement(indToFill, alpha);
  m_core.finalize();

  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    vec iThMode(m_nDof_var[iVar], m_comm);
    iThMode.ones();
    m_modes[iVar].push_back(iThMode);
  }
}


/* Extract subtensor, contiguous indices
  - input: sub-indices C convention [low_1, up_1),[low_2, up_2)...
  - output: the given Tucker tensor is filled
*/
void Tucker::extractSubtensor(vector<unsigned int> indBounds, Tucker& subTensor){
  assert(indBounds.size()==2*m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    resPerDim[iVar] = indBounds[2*iVar+1] - indBounds[2*iVar];
  }
  subTensor.init(resPerDim, m_comm);
  subTensor.set_ranks(m_ranks);
  // set the core:
  fullTensor subCore;
  subCore.copyTensorFrom(m_core); // copy the current core.
  subTensor.set_core(subCore);
  // modes:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int iMod=0; iMod<m_ranks[iVar]; iMod++){
      vec mode(resPerDim[iVar], m_comm);
      for(unsigned int iDof=0; iDof<resPerDim[iVar]; iDof++){
        unsigned int globI = iDof + indBounds[2*iVar];
        double val = m_modes[iVar][iMod].getVecEl(globI);
        mode.setVecEl(iDof, val);
      }
      mode.finalize();
      subTensor.set_modes(iVar, iMod, mode);
    }
  }

}


/* Extract subtensor, contiguous indices (overloaded to return a Tucker)
  - input: sub-indices C convention [low_1, up_1),[low_2, up_2)...
  - output: the given Tucker tensor is filled
*/
Tucker Tucker::extractSubtensor(vector<unsigned int> indBounds){
  assert(indBounds.size()==2*m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    resPerDim[iVar] = indBounds[2*iVar+1] - indBounds[2*iVar];
  }

  Tucker subTensor(resPerDim, m_comm);
  subTensor.set_ranks(m_ranks);

  // set the core:
  fullTensor subCore;
  subCore.copyTensorFrom(m_core); // copy the current core.
  subTensor.set_core(subCore);
  // modes:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int iMod=0; iMod<m_ranks[iVar]; iMod++){
      vec mode(resPerDim[iVar], m_comm);
      for(unsigned int iDof=0; iDof<resPerDim[iVar]; iDof++){
        unsigned int globI = iDof + indBounds[2*iVar];
        double val = m_modes[iVar][iMod].getVecEl(globI);
        mode.setVecEl(iDof, val);
      }
      mode.finalize();
      subTensor.set_modes(iVar, iMod, mode);
    }
  }
  return subTensor;
}


/* Extract subtensor, non-contiguous list of indices [list_x1 ; list_x2; ...;list_xd]
  - input: sub-indices list
  - output: the given Tucker tensor is filled
*/
void Tucker::extractSubtensor(vector<vector<unsigned int> > indList, Tucker& subTensor){
  assert(indList.size()==m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    resPerDim[iVar] = indList[iVar].size();
  }

  subTensor.init(resPerDim, m_comm);
  subTensor.set_ranks(m_ranks);
  // set the core:
  fullTensor subCore;
  subCore.copyTensorFrom(m_core); // copy the current core.
  subTensor.set_core(subCore);
  // modes:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int iMod=0; iMod<m_ranks[iVar]; iMod++){
      vec mode(resPerDim[iVar], m_comm);
      for(unsigned int iDof=0; iDof<resPerDim[iVar]; iDof++){
        unsigned int globI = indList[iVar][iDof];
        double val = m_modes[iVar][iMod].getVecEl(globI);
        mode.setVecEl(iDof, val);
      }
      mode.finalize();
      subTensor.set_modes(iVar, iMod, mode);
    }
  }
}


/* Extract subtensor, non-contiguous list of indices (overloaded)
  - input: sub-indices list
  - output: the given Tucker tensor is filled
*/
Tucker Tucker::extractSubtensor(vector<vector<unsigned int> > indList){
  assert(indList.size()==m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    resPerDim[iVar] = indList[iVar].size();
  }

  Tucker subTensor(resPerDim, m_comm);
  subTensor.set_ranks(m_ranks);
  // set the core:
  fullTensor subCore;
  subCore.copyTensorFrom(m_core); // copy the current core.
  subTensor.set_core(subCore);
  // modes:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int iMod=0; iMod<m_ranks[iVar]; iMod++){
      vec mode(resPerDim[iVar], m_comm);
      for(unsigned int iDof=0; iDof<resPerDim[iVar]; iDof++){
        unsigned int globI = indList[iVar][iDof];
        double val = m_modes[iVar][iMod].getVecEl(globI);
        mode.setVecEl(iDof, val);
      }
      mode.finalize();
      subTensor.set_modes(iVar, iMod, mode);
    }
  }
  return subTensor;
}


/* Assign subtensor, contiguous list of indices
  - input: sub-indices list
  - output: this tensor is filled with the given Subtensor
*/
void Tucker::assignSubtensor(vector<unsigned int> indBounds, Tucker& subTensor){
  if(!isEmpty()){
    Tucker currentSubTens = extractSubtensor(indBounds);
    subTensor.sum(currentSubTens, -1.0, true);
    currentSubTens.clear(); // it has been copied;
  }

  Tucker toAdd(m_nDof_var, m_comm);
  toAdd.set_ranks(subTensor.ranks());
  fullTensor subCore = subTensor.core();
  fullTensor toAddCore;
  toAddCore.copyTensorFrom(subCore);
  toAdd.set_core(toAddCore);

  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int iMod=0; iMod<subTensor.ranks(iVar); iMod++){
      vec mode(m_nDof_var[iVar], m_comm);
      const unsigned int nDof = indBounds[2*iVar+1] - indBounds[2*iVar];
      for(unsigned int iDof=0; iDof<nDof; iDof++){
        unsigned int globI = indBounds[2*iVar] + iDof;
        double val = subTensor.modes(iVar, iMod).getVecEl(iDof);
        mode.setVecEl(globI, val);
      }
      mode.finalize();
      toAdd.set_modes(iVar, iMod, mode);
    }
  }

  sum(toAdd, 1.0, true);
  toAdd.clear();
}


/* Append subtensor, contiguous list of indices
  - input: sub-indices list
  - output: this tensor is augmented with the given Subtensor (!! potentially modifying existing values in the indices)
*/
void Tucker::appendSubtensor(vector<unsigned int> indBounds, Tucker& subTensor){
  Tucker toAdd(m_nDof_var, m_comm);
  toAdd.set_ranks(subTensor.ranks());
  fullTensor subCore = subTensor.core();
  fullTensor toAddCore;
  toAddCore.copyTensorFrom(subCore);
  toAdd.set_core(toAddCore);
  // embedding the modes:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int iMod=0; iMod<subTensor.ranks(iVar); iMod++){
      vec mode(m_nDof_var[iVar], m_comm);
      const unsigned int nDof = indBounds[2*iVar+1] - indBounds[2*iVar];
      for(unsigned int iDof=0; iDof<nDof; iDof++){
        unsigned int globI = indBounds[2*iVar] + iDof;
        double val = subTensor.modes(iVar, iMod).getVecEl(iDof);
        mode.setVecEl(globI, val);
      }
      mode.finalize();
      toAdd.set_modes(iVar, iMod, mode);
    }
  }

  sum(toAdd, 1.0, true);
  toAdd.clear();
}




/*  Mode-I contraction:
  - input: the variable along which to contract
  - output: a Tucker tensor which is the contraction
*/
Tucker Tucker::modeIContraction(unsigned int Ivar, vec b){
  assert(b.size()==m_nDof_var[Ivar]);

  // contract the modes:
  vec modeContraction(m_ranks[Ivar], m_comm);
  for(unsigned int iMod=0; iMod<m_ranks[Ivar]; iMod++){
    double val = scalProd(b, m_modes[Ivar][iMod]);
    modeContraction.setVecEl(iMod, val);
  }
  modeContraction.finalize();

  // create the core:
  fullTensor contCore = m_core.modeIContraction(Ivar, modeContraction);

  vector<unsigned int> dofPerDim(m_nVar-1);
  vector<unsigned int> contRanks(m_nVar-1);
  unsigned int cc = 0;
  for(unsigned int jVar=0; jVar<m_nVar; jVar++){
    if(jVar != Ivar){
      dofPerDim[cc] = m_nDof_var[jVar];
      contRanks[cc] = m_ranks[jVar];
      cc += 1;
    }
  }
  Tucker toBeReturned(dofPerDim, m_comm);
  toBeReturned.set_ranks(contRanks);
  toBeReturned.set_core(contCore);
  cc = 0;
  for(unsigned int jVar=0; jVar<m_nVar; jVar++){
    if(jVar != Ivar){
      for(unsigned int iMod=0; iMod<m_ranks[jVar]; iMod++){
        toBeReturned.set_modes(cc, iMod, m_modes[jVar][iMod]);
      }
      cc += 1;
    }
  }

  return toBeReturned;
}


/*  Mode-I contraction:
  - input: the variable along which to contract
  - output: a Tucker tensor which is the contraction
*/
void Tucker::modeIContraction(unsigned int Ivar, vec b, Tucker& result){
  assert(b.size()==m_nDof_var[Ivar]);

  // contract the modes:
  vec modeContraction(m_ranks[Ivar], m_comm);
  for(unsigned int iMod=0; iMod<m_ranks[Ivar]; iMod++){
    double val = scalProd(b, m_modes[Ivar][iMod]);
    modeContraction.setVecEl(iMod, val);
  }
  modeContraction.finalize();

  // create the core:
  fullTensor contCore = m_core.modeIContraction(Ivar, modeContraction);

  vector<unsigned int> dofPerDim(m_nVar-1);
  vector<unsigned int> contRanks(m_nVar-1);
  unsigned int cc = 0;
  for(unsigned int jVar=0; jVar<m_nVar; jVar++){
    if(jVar != Ivar){
      dofPerDim[cc] = m_nDof_var[jVar];
      contRanks[cc] = m_ranks[jVar];
      cc += 1;
    }
  }
  result.init(dofPerDim, m_comm);
  result.set_ranks(contRanks);
  result.set_core(contCore);
  cc = 0;
  for(unsigned int jVar=0; jVar<m_nVar; jVar++){
    if(jVar != Ivar){
      for(unsigned int iMod=0; iMod<m_ranks[jVar]; iMod++){
        result.set_modes(cc, iMod, m_modes[jVar][iMod]);
      }
      cc += 1;
    }
  }
}


/* compute the SVD of the Unfolding without explicitly assembling the unfolding
  - input: the iVar which becomes the row index of the unfolding
  - output: the svd left modes and singular values of the unfolding
*/
void Tucker::compute_UnfoldingSVD(unsigned int varMode, vec& sigmas, vector<vec>& singVecs){

  // compute the covariances:
  vector<mat> covMat(m_nVar-1);
  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != varMode){
      covMat[cc].init(m_ranks[iVar], m_ranks[iVar], m_comm);
      for(unsigned int iMod=0; iMod<m_ranks[iVar]; iMod++){
        for(unsigned int jMod=0; jMod<=iMod; jMod++){
          double val = scalProd(m_modes[iVar][iMod], m_modes[iVar][jMod]);
          covMat[cc].setMatEl(iMod, jMod, val);
          if(iMod != jMod){
            covMat[cc].setMatEl(jMod, iMod, val);
          }
        }
      }
      covMat[cc].finalize();
      cc += 1;
    }
  }

  //cout << "Computing K...";

  // compute the matrix K:
  mat K(m_ranks[varMode], m_ranks[varMode], m_comm);
  for(unsigned int l=0; l<m_core.nEntries(); l++){
    vector<unsigned int> ind_l = m_core.lin2sub(l);
    double coreVal_l = m_core.tensorEntries(l);
    const unsigned int iRow = ind_l[varMode];
    if(coreVal_l != 0.0){
      for(unsigned int m=0; m<m_core.nEntries(); m++){
        vector<unsigned int> ind_m = m_core.lin2sub(m);
        double coreVal_m = m_core.tensorEntries(m);
        const unsigned int iCol = ind_m[varMode];

        if(coreVal_m != 0.0){
          double value = coreVal_l * coreVal_m;
          unsigned int counter = 0;
          for(unsigned int iVar=0; iVar<m_nVar; iVar++){
            if(iVar!=varMode){
              unsigned int jRow = ind_l[iVar];
              unsigned int jCol = ind_m[iVar];
              double valCov = covMat[counter].getMatEl(jRow, jCol);
              value *= valCov;
              counter += 1;
            }
          }
          K.addMatEl(iRow, iCol, value);
        }
      }
    }

  }
  K.finalize();
  //cout << "done!.\n";


  // compute the svd of the modes:
  svd svd_varMode(m_modes[varMode]);
  mat U = svd_varMode.Umat();
  mat S = svd_varMode.Smat();
  mat V = svd_varMode.Vmat();
  mat Vt = transpose(V);
  mat W = S * Vt;
  mat Wt = transpose(W);

  // compute the change of basis induced by W:
  mat tmp = W * K;
  mat toDiagonalise = tmp * Wt;

  // diagonalise the matrix:
  eigenSolver eigCov(toDiagonalise);
  unsigned int nOfEigVals = eigCov.eigenVals().size();

  // return the singular values:
  sigmas.init(nOfEigVals, m_comm);
  for(unsigned int iS=0; iS<nOfEigVals; iS++){
    double val = sqrt(fabs(eigCov.eigenVals(iS)));
    sigmas.setVecEl(iS, val);
  }
  sigmas.finalize();


  // transform U by using the eigenvectors:
  singVecs.resize(nOfEigVals);
  for(unsigned int iS=0; iS<nOfEigVals; iS++){
    vec weights = eigCov.eigenVecs(iS);
    vec theMode(U.nRows(), m_comm);
    for(unsigned int iRow = 0; iRow<U.nRows(); iRow++){
      double value = 0.0;
      for(unsigned int jCol=0; jCol<U.nCols(); jCol++){
        double matVal = U.getMatEl(iRow, jCol);
        double vecVal = weights.getVecEl(jCol);
        value += matVal * vecVal;
      }
      theMode.setVecEl(iRow, value);
    }
    theMode.finalize();
    singVecs[iS] = theMode;
  }

  // free the memory:
  for(unsigned int iV=0; iV<m_nVar-1; iV++){
    covMat[iV].clear();
  }
  K.clear();
  Vt.clear();
  W.clear();
  Wt.clear();
  svd_varMode.clear();
  eigCov.clear();
}



/*  rounding via HOSVD:
  - input: tolerance
  - output: this Tucker tensor is the compressed one.
*/
void Tucker::rounding(double tol){
  // svd of the modes:
  vector<svd> svd_modes(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    svd_modes[iVar].svdVecSet(m_modes[iVar]);
    mat U = svd_modes[iVar].Umat();
    mat V = svd_modes[iVar].Vmat();
    mat S = svd_modes[iVar].Smat();
    mat Vt = transpose(V);
    mat W = S * Vt;

    m_core.inPlaceMatTensProd(iVar, W);

    for(unsigned int iMod = S.nRows(); iMod<m_ranks[iVar]; iMod++){
      m_modes[iVar][iMod].clear();
    }
    m_ranks[iVar] = S.nRows();
    m_modes[iVar].resize(m_ranks[iVar]);
    for(unsigned int iMod=0; iMod<m_ranks[iVar]; iMod++){
      m_modes[iVar][iMod].clear();
      vec col(U.nRows(), m_comm);
      for(unsigned int iRow=0; iRow<U.nRows(); iRow++){
        double val = U.getMatEl(iRow,iMod);
        col.setVecEl(iRow,val);
      }
      col.finalize();
      m_modes[iVar][iMod] = col;
    }
    svd_modes[iVar].clear();
    Vt.clear();
  }



  // greedy selection of the ranks:
  vector<unsigned int> resRanks(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    resRanks[iVar] = 1;
  }
  double coreNorm = m_core.tensorEntries().norm();
  double normLeft = sqrt(coreNorm*coreNorm - m_core.tensorEntries(0)*m_core.tensorEntries(0));
  while(normLeft>tol){
    vector<double> normCandidate(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      // try to increase the rank for variable iVar;
      vector<unsigned int> bounds(2*m_nVar);
      for(unsigned int jVar=0; jVar<m_nVar; jVar++){
        if(jVar != iVar){
          bounds[2*jVar+1] = resRanks[jVar];
        }
        else{
          if(resRanks[iVar] + 1 <= m_core.nDof_var(iVar)){
            bounds[2*jVar+1] = resRanks[iVar] + 1; // try to increase the rank by 1;
          }
          else{
            bounds[2*jVar+1] = resRanks[iVar];
          }
        }
      }
      fullTensor subCore = m_core.extractSubtensor(bounds);
      double normSub = subCore.tensorEntries().norm();
      normCandidate[iVar] = sqrt(normLeft*normLeft - normSub*normSub);
      subCore.clear();
    }
    unsigned int iBest=0;
    double minNorm = normCandidate[0];
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      if(normCandidate[iVar] < minNorm){
        iBest = iVar;
        minNorm = normCandidate[iVar];
      }
    }
    resRanks[iBest] += 1;
    normLeft = normCandidate[iBest];
  }

  // retrieving the core and the modes:
  vector<unsigned int> bounds(2*m_nVar);
  for(unsigned int jVar=0; jVar<m_nVar; jVar++){
      bounds[2*jVar+1] = resRanks[jVar];
  }
  fullTensor coreToRetrieve = m_core.extractSubtensor(bounds);
  m_core.clear();
  set_core(coreToRetrieve);

  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int iMod=resRanks[iVar]; iMod<m_ranks[iVar]; iMod++){
      m_modes[iVar][iMod].clear();
    }
    m_modes[iVar].resize(resRanks[iVar]);
    m_ranks[iVar] = resRanks[iVar];
  }

}


/* recompression procedure based on the HOSVD of the unfoldings
  - input: the tolerance
  - output: recompression
*/
void Tucker::recompress(double tolerance){
  vector<vec> sigmas(m_nVar);
  vector<vector<vec> > modesSet(m_nVar);
  vector<unsigned int> nSig(m_nVar);
  double energy = 0.0;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    cout << "iVar = " << iVar << endl;
    compute_UnfoldingSVD(iVar, sigmas[iVar], modesSet[iVar]);
    nSig[iVar] = sigmas[iVar].size();
    cout << "number of sigmas = " << nSig[iVar] << endl;
    for(unsigned int iMod=0; iMod<nSig[iVar]; iMod++){
      double value = sigmas[iVar].getVecEl(iMod);
      energy = energy + value * value;
    }
  }
  cout << "Energy = " << energy << endl;
  // greedy choice of the modes:
  m_isSigmaTableFilled = true;
  m_sigmaTable.resize(m_nVar);
  vector<unsigned int> toRetain(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    toRetain[iVar] = 0;
  }
  double error = sqrt(energy);
  while(error > tolerance){
    // find the best term to add:
    unsigned int i_var_star;
    double sigma_star = 0.0;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      unsigned int iToGet = toRetain[iVar];
      if(iToGet <  sigmas[iVar].size()){
        double value = sigmas[iVar].getVecEl(iToGet);
        if(value > sigma_star){
          sigma_star = value;
          i_var_star = iVar;
        }
      }
    }
    toRetain[i_var_star] += 1;
    m_sigmaTable[i_var_star].push_back(sigma_star);
    double toSubtract = sigma_star * sigma_star;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if( (iVar != i_var_star) && (toRetain[iVar] == 0) ){
        toRetain[iVar] = 1;
        double singVal = sigmas[iVar].getVecEl(0);
        m_sigmaTable[iVar].push_back(singVal);
        toSubtract = toSubtract + singVal * singVal;
      }
    }
    // roundoffs:
    if(error*error - toSubtract<0.0){
      if(fabs(error*error - toSubtract) > 1.0e-14){
        PetscPrintf(m_comm, "Warning, problem in recompression!\n");
      }
      error = 0.0;
    }
    else{
      error = sqrt(error*error - toSubtract);
    }
    cout << error << endl;
  }

  // set the modes:
  vector<mat> projMatrices(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    projMatrices[iVar].init(toRetain[iVar], m_ranks[iVar], m_comm);
    for(unsigned int iMod=0; iMod<toRetain[iVar]; iMod++){
      for(unsigned int jMod=0; jMod<m_ranks[iVar]; jMod++){
        double val = scalProd(m_modes[iVar][jMod], modesSet[iVar][iMod]);
        projMatrices[iVar].setMatEl(iMod, jMod, val);
      }
    }
    projMatrices[iVar].finalize();
  }

  // testing the core agains the projection matrices:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_core.inPlaceMatTensProd(iVar, projMatrices[iVar]);
    for(unsigned int iMod=0; iMod<m_ranks[iVar]; iMod++){
      m_modes[iVar].clear();
    }
    m_ranks[iVar] = toRetain[iVar];
  }
  m_modes = modesSet;

  // free the memory:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    projMatrices[iVar].clear();
  }
}
