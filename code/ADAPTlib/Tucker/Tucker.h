// Header file for Tucker format:

#ifndef Tucker_h
#define Tucker_h

#include "../tensor.h"
#include "../linAlg/linAlg.h"
#include "../fullTensor/fullTensor.h"


class Tucker : public tensor{
private:
  vector<unsigned int> m_ranks;
  vector<vector<vec> > m_modes;
  vector<vector<double> > m_sigmaTable;
  fullTensor m_core;
  bool m_isInitialised;
  bool m_isSigmaTableFilled;

public:
  // constructor and destructor:
  Tucker(){m_isInitialised = false;};
  ~Tucker(){};
  void clear(){
    if(m_core.isInitialised()){
      m_core.clear();
    }
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      for(unsigned int iMod=0; iMod<m_ranks[iVar]; iMod++){
        m_modes[iVar][iMod].clear();
      }
      m_modes[iVar].clear();
    }
    if(m_isSigmaTableFilled){
      m_sigmaTable.clear();
    }
  }
  // overloaded constructors and init:
  Tucker(unsigned int, vector<unsigned int>, MPI_Comm);
  Tucker(vector<unsigned int>, MPI_Comm);
  Tucker(fullTensor&, vector<vector<vec> >&);
  void init(vector<unsigned int>, MPI_Comm);

  // -- ACCESS FUNCTIONS --
  inline bool isInitialised(){return m_isInitialised;}
  inline vector<unsigned int> ranks(){return m_ranks;}
  inline unsigned int ranks(unsigned int iVar){return m_ranks[iVar];}
  inline vector<vector<vec> > modes(){return m_modes;}
  inline vector<vec> modes(unsigned int iVar){return m_modes[iVar];}
  inline vec modes(unsigned int iVar, unsigned int iMod){return m_modes[iVar][iMod];}
  inline fullTensor core(){return m_core;}
  inline bool isSigmaTableFilled(){return m_isSigmaTableFilled;}
  inline vector<vector<double> > sigmaTable(){return m_sigmaTable;}
  inline vector<double> sigmaTable(unsigned int iVar){return m_sigmaTable[iVar];}
  inline double sigmaTable(unsigned int iVar, unsigned int iMod){return m_sigmaTable[iVar][iMod];}


  // -- SETTERS --
  inline void set_ranks(vector<unsigned int> tuckerRanks){
    m_ranks.resize(tuckerRanks.size());
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_ranks[iVar] = tuckerRanks[iVar];
    }
    if(m_core.isInitialised()){
      m_core.clear();
      m_core.init(m_ranks, m_comm);
    }
    else{
      m_core.init(m_ranks, m_comm);
    }
    m_modes.resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_modes[iVar].resize(m_ranks[iVar]);
    }
  }

  // set modes:
  inline void set_modes(vector<vector<vec> >& modes){
    m_modes = modes;
  }

  inline void set_modes(unsigned int iVar, vector<vec>& modes_iVar){
    m_modes[iVar] = modes_iVar;
  }

  inline void set_modes(unsigned int iVar, unsigned int iMod, vec& mode){
    assert(iMod<m_ranks[iVar]);
    m_modes[iVar][iMod] = mode;
  }

  inline void set_core(fullTensor& core){
    m_core = core;
  }

  // set tensor element
  inline void set_tensorElement(vector<unsigned int> ind, double val){
    // check if there is an element:
    bool isThereSomething = true;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(m_ranks[iVar] == 0){
        isThereSomething = false;
      }
    }
    if(!isThereSomething){
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        m_ranks[iVar] = 1;
      }
      m_core.init(m_ranks, m_comm);
      m_core.set_tensorElement(0, val);
      m_core.finalize();
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        m_modes[iVar].resize(m_ranks[iVar]);
        vec iThMode(m_nDof_var[iVar], m_comm);
        unsigned int iDof = ind[iVar];
        iThMode.setVecEl(iDof, 1.0);
        iThMode.finalize();
        m_modes[iVar][0] = iThMode;
      }
    }
    else{
      double actualValue = eval(ind);
      fullTensor coreCopy;
      coreCopy.copyTensorFrom(m_core);
      m_core.clear();
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        m_ranks[iVar] = m_ranks[iVar]  + 1;
      }
      m_core.init(m_ranks, m_comm);
      vector<unsigned int> indBound(2*m_nVar);
      for(unsigned int iB=0; iB<2*m_nVar; iB++){
        if(iB%2 == 0){
          indBound[iB] = 0;
        }
        else{
          unsigned int iVar = (iB - 1)/2;
          indBound[iB] = m_ranks[iVar]-1;
        }
      }
      m_core.assignSubtensor(indBound, coreCopy);
      coreCopy.clear();
      vector<unsigned int> indToFill(m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        indToFill[iVar] = m_ranks[iVar]-1;
      }
      double valToFill = val - actualValue;
      m_core.set_tensorElement(indToFill, valToFill);
      m_core.finalize();
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        vec iThMode(m_nDof_var[iVar], m_comm);
        unsigned int iDof = ind[iVar];
        iThMode.setVecEl(iDof, 1.0);
        iThMode.finalize();
        m_modes[iVar].push_back(iThMode);
      }
    }
  }


  // -- functions and proxy class to access Tucker
  inline void get_ranks(){
    m_ranks = m_core.nDof_var();
  }

  // check if a tensor is empty:
  inline bool isEmpty(){
    bool isIt = true;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(m_ranks[iVar] > 0){
        isIt = false;
        break;
      }
    }
    return isIt;
  }


  /* eval function (base version)
    - input: the multi-index
    - output: the value
  */
  void eval(const vector<unsigned int>& ind, double& val){
    const unsigned int N = m_core.tensorEntries().size();
    for(unsigned int l=0; l<N; l++){
      vector<unsigned int> modeInd = m_core.lin2sub(l);
      double toBeAdded = m_core.tensorEntries().getVecEl(l);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        unsigned int iMod = modeInd[iVar];
        unsigned int iDof = ind[iVar];
        double modeVal = m_modes[iVar][iMod].getVecEl(iDof);
        toBeAdded = toBeAdded * modeVal;
      }
      val = val + toBeAdded;
    }
  };


  /* eval function (overloaded to return double)
    - input: the multi-index
    - output: the value
  */
  double eval(const vector<unsigned int>& ind){
    double val = 0.0;
    const unsigned int N = m_core.tensorEntries().size();
    for(unsigned int l=0; l<N; l++){
      vector<unsigned int> modeInd = m_core.lin2sub(l);
      double toBeAdded = m_core.tensorEntries().getVecEl(l);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        unsigned int iMod = modeInd[iVar];
        unsigned int iDof = ind[iVar];
        double modeVal = m_modes[iVar][iMod].getVecEl(iDof);
        toBeAdded = toBeAdded * modeVal;
      }
      val = val + toBeAdded;
    }
    return val;
  }


  /* eval function (overloaded to be variadic)
    - input: the multi-index
    - output: the value
  */
  double eval(unsigned int iComp,...){
    vector<unsigned int> indices(m_nVar);
    va_list ap;
    va_start(ap, iComp);
    indices[0] = iComp;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      indices[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);
    double val = 0.0;

    const unsigned int N = m_core.tensorEntries().size();
    for(unsigned int l=0; l<N; l++){
      vector<unsigned int> modeInd = m_core.lin2sub(l);
      double toBeAdded = m_core.tensorEntries().getVecEl(l);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        unsigned int iMod = modeInd[iVar];
        unsigned int iDof = indices[iVar];
        double modeVal = m_modes[iVar][iMod].getVecEl(iDof);
        toBeAdded = toBeAdded * modeVal;
      }
      val = val + toBeAdded;
    }
    return val;
  }

  // Nested proxy class for accessing and manipulating tensor elements
  // overloading operator () in assignement through a proxy class:
  class Proxy : public tensor
  {
    vector<vector<vec> >* x_modes;
    fullTensor* c;
    vector<unsigned int> idx;
    vector<unsigned int>* x_ranks;

  public:

      // Set Tensor Elements:
      inline void set_tensorElement(vector<unsigned int> ind, double val){
        // check if there is an element:
        bool isThereSomething = true;
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          if((*x_ranks)[iVar] == 0){
            isThereSomething = false;
          }
        }
        if(!isThereSomething){
          for(unsigned int iVar=0; iVar<m_nVar; iVar++){
            (*x_ranks)[iVar] = 1;
          }
          c->init(*x_ranks, m_comm);
          c->set_tensorElement(0, val);
          c->finalize();
          for(unsigned int iVar=0; iVar<m_nVar; iVar++){
            (*x_modes)[iVar].resize((*x_ranks)[iVar]);
            vec iThMode(m_nDof_var[iVar], m_comm);
            unsigned int iDof = ind[iVar];
            iThMode.setVecEl(iDof, 1.0);
            iThMode.finalize();
            (*x_modes)[iVar][0] = iThMode;
          }
        }
        else{
          double actualValue = eval(ind);
          fullTensor coreCopy;
          coreCopy.copyTensorFrom(*c);
          c->clear();
          for(unsigned int iVar=0; iVar<m_nVar; iVar++){
            (*x_ranks)[iVar] = (*x_ranks)[iVar]  + 1;
          }
          c->init(*x_ranks, m_comm);
          vector<unsigned int> indBound(2*m_nVar);
          for(unsigned int iB=0; iB<2*m_nVar; iB++){
            if(iB%2 == 0){
              indBound[iB] = 0;
            }
            else{
              unsigned int iVar = (iB - 1)/2;
              indBound[iB] = (*x_ranks)[iVar]-1;
            }
          }
          c->assignSubtensor(indBound, coreCopy);
          coreCopy.clear();
          vector<unsigned int> indToFill(m_nVar);
          for(unsigned int iVar=0; iVar<m_nVar; iVar++){
            indToFill[iVar] = (*x_ranks)[iVar]-1;
          }
          double valToFill = val - actualValue;
          c->set_tensorElement(indToFill, valToFill);
          c->finalize();
          for(unsigned int iVar=0; iVar<m_nVar; iVar++){
            vec iThMode(m_nDof_var[iVar], m_comm);
            unsigned int iDof = ind[iVar];
            iThMode.setVecEl(iDof, 1.0);
            iThMode.finalize();
            (*x_modes)[iVar].push_back(iThMode);
          }
        }
      }

      // overloaded to return a double:
      double eval(const vector<unsigned int>& ind){
        double val = 0.0;
        const unsigned int N = c->tensorEntries().size();
        for(unsigned int l=0; l<N; l++){
          vector<unsigned int> modeInd = c->lin2sub(l);
          double toBeAdded = c->tensorEntries().getVecEl(l);
          for(unsigned int iVar=0; iVar<m_nVar; iVar++){
            unsigned int iMod = modeInd[iVar];
            unsigned int iDof = ind[iVar];
            double modeVal = (*x_modes)[iVar][iMod].getVecEl(iDof);
            toBeAdded = toBeAdded * modeVal;
          }
          val = val + toBeAdded;
        }
        return val;
      }

      // Constructor of the proxy class:
      Proxy(vector<unsigned int> idx, vector<unsigned int> dofPerDim, vector<vector<vec> >* x_modes, fullTensor* c, vector<unsigned int>* x_ranks, MPI_Comm theComm) : idx(idx), x_modes(x_modes), c(c), x_ranks(x_ranks){
        m_comm = theComm;
        m_nVar = idx.size();
        m_nDof_var.resize(m_nVar);
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          m_nDof_var[iVar] = dofPerDim[iVar];
        }
        compute_minc();
      }

      // equal operator overload:
      inline double operator= (double value) {
        set_tensorElement(idx, value);
        return value;
      }

      // Overloading the double operator for assignement:
      operator double(){
        double toBeReturned = eval(idx);
        return toBeReturned;
      }
  };

  // Calling the proxy object through variadic:
  Proxy operator() (unsigned int I,...) {

    vector<unsigned int> ind(m_nVar);
    va_list ap;
    va_start(ap, I);
    ind[0] = I;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      ind[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);
    return Proxy(ind, m_nDof_var, &m_modes, &m_core, &m_ranks, m_comm);
  }


  // Methods and operations for Tucker:
  // COPY Tensor Structure:
	void copyTensorStructFrom(Tucker&);

	// COPY the whole tensor:
	void copyTensorFrom(Tucker&);

  // OPERATIONS on CP tensor (generic):
	bool hasSameStructure(Tucker&);
	bool isEqualTo(Tucker&);
  bool operator == (Tucker&);
	void sum(Tucker&, double, bool);
  void operator += (Tucker&);
	void multiplyByScalar(double);
  void operator *= (double);
	void shiftByScalar(double);
  void operator += (double);
	void extractSubtensor(vector<unsigned int>, Tucker&);
  Tucker extractSubtensor(vector<unsigned int>);
  void extractSubtensor(vector<vector<unsigned int> >, Tucker&);
  Tucker extractSubtensor(vector<vector<unsigned int> >);
	void assignSubtensor(vector<unsigned int>, Tucker&);
  void appendSubtensor(vector<unsigned int>, Tucker&);
	void reshape(vector<unsigned int>, Tucker&, bool);
  Tucker reshape(vector<unsigned int>, bool);
  Tucker modeIContraction(unsigned int, vec);
  void modeIContraction(unsigned int, vec, Tucker&);

  // Operation specific to Tucker:
  void compute_UnfoldingSVD(unsigned int, vec&, vector<vec>&);
  void rounding(double);
  void recompress(double);

};


#endif
