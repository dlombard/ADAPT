// Implementation of fullTensor operations


#include "fullTensorOperations.hpp"
#include "../linAlg/linearAlgebraOperations.h"


/* Left Kronecker product of tensors
  - input: tensors A and B
  - output: a tensor which is the Kronecker product of the given tensors
*/
fullTensor leftKronecker(fullTensor& A, fullTensor& B){
  assert(A.nVar()==B.nVar());
  assert(A.comm()==B.comm());
  vector<unsigned int>resPerDim(A.nVar());
  for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
    resPerDim[idVar] = A.nDof_var(idVar) * B.nDof_var(idVar);
  }
  fullTensor toBeReturned(resPerDim, A.comm());
  for(unsigned int lB=0; lB<B.nEntries(); lB++){
    vector<unsigned int> indicesB = B.lin2sub(lB);
    double valB = B.tensorEntries(lB);
    for(unsigned int lA=0; lA<A.nEntries(); lA++){
      vector<unsigned int> indicesA = A.lin2sub(lA);
      double valA = A.tensorEntries(lA);
      vector<unsigned int> ind(A.nVar());
      for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
        ind[idVar] = indicesB[idVar]*A.nDof_var(idVar) + indicesA[idVar];
      }
      unsigned int lin = toBeReturned.sub2lin(ind);
      double val = valA*valB;
      if(val != 0.0){
        toBeReturned.set_tensorElement(lin, val);
      }
    }
  }
  toBeReturned.finalize();
  return toBeReturned;
}


/* Right Kronecker product of tensors
  - input: tensors A and B
  - output: this tensor (empty) is the right Kronecker product of the tensors
*/
fullTensor rightKronecker(fullTensor& A, fullTensor& B){
  assert(A.nVar()==B.nVar());
  assert(A.comm()==B.comm());
  vector<unsigned int> resPerDim(A.nVar());
  for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
    resPerDim[idVar] = A.nDof_var(idVar) * B.nDof_var(idVar);
  }
  fullTensor toBeReturned(resPerDim, A.comm());
  for(unsigned int lB=0; lB<B.nEntries(); lB++){
    vector<unsigned int> indicesB = B.lin2sub(lB);
    double valB = B.tensorEntries(lB);
    for(unsigned int lA=0; lA<A.nEntries(); lA++){
      vector<unsigned int> indicesA = A.lin2sub(lA);
      double valA = A.tensorEntries(lA);
      vector<unsigned int> ind(A.nVar());
      for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
        ind[idVar] = indicesA[idVar]*B.nDof_var(idVar) + indicesB[idVar];
      }
      unsigned int lin = toBeReturned.sub2lin(ind);
      double val = valA*valB;
      if(val != 0.0){
        toBeReturned.set_tensorElement(lin, val);
      }
    }
  }
  toBeReturned.finalize();
  return toBeReturned;
}


/* mode I Khatri-Rao product of two given full Tensors.
  - input: the full tensors A and B, the mode I
  - output: full tensor which is the mode I Khatri-Rao product of A and B
*/
fullTensor modeIKhatriRao(unsigned int modeI, fullTensor& A, fullTensor& B){
  assert(A.nVar()==B.nVar());
  assert(A.comm()==B.comm());
  assert(A.nDof_var(modeI) == B.nDof_var(modeI));
  assert(modeI < A.nVar());

  // Init the resolution
  vector<unsigned int> dofPerDim(A.nVar());

  vector<unsigned int>indicesBoundsA(2*A.nVar());
  vector<unsigned int>indicesBoundsB(2*A.nVar());
  vector<unsigned int>indicesBoundsThis(2*A.nVar());
  for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
    if(idVar != modeI){
      dofPerDim[idVar] = A.nDof_var(idVar) * B.nDof_var(idVar);
      indicesBoundsA[2*idVar] = 0;
      indicesBoundsA[2*idVar+1] = A.nDof_var(idVar);
      indicesBoundsB[2*idVar] = 0;
      indicesBoundsB[2*idVar+1] = B.nDof_var(idVar);
      indicesBoundsThis[2*idVar] = 0;
      indicesBoundsThis[2*idVar+1] = dofPerDim[idVar];
    }
    else{
      dofPerDim[idVar] = A.nDof_var(modeI);
    }
  }

  fullTensor toBeReturned(dofPerDim, A.comm());

  for(unsigned int idDof=0; idDof<dofPerDim[modeI]; idDof++){
    fullTensor subA;
    indicesBoundsA[2*modeI] = idDof;
    indicesBoundsA[2*modeI+1] = idDof+1;
    A.extractSubtensor(indicesBoundsA,subA);
    fullTensor subB;
    indicesBoundsB[2*modeI] = idDof;
    indicesBoundsB[2*modeI+1] = idDof+1;
    B.extractSubtensor(indicesBoundsB,subB);

    fullTensor subKronProd = rightKronecker(subA,subB);
    indicesBoundsThis[2*modeI] = idDof;
    indicesBoundsThis[2*modeI+1] = idDof+1;
    toBeReturned.assignSubtensor(indicesBoundsThis,subKronProd);
  }
  toBeReturned.finalize();
  return toBeReturned;
}


/* mode I concatenation of two full order tensors
  - input: tensor A and tensor B in full tensor format
  - output: this tensor is the concatenation
*/
fullTensor modeIConcatenate(unsigned int modeI, fullTensor& A, fullTensor& B){
  assert(A.nVar()==B.nVar());
  assert(A.comm()==B.comm());
  assert(modeI < A.nVar());
  for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
    if(idVar!=modeI){
      assert(A.nDof_var(idVar)==B.nDof_var(idVar));
    }
  }

  vector<unsigned int> dofPerDim(A.nVar());

  vector<unsigned int>indicesBoundsA(2*A.nVar());
  vector<unsigned int>indicesBoundsB(2*A.nVar());
  for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
    if(idVar != modeI){
      dofPerDim[idVar] = A.nDof_var(idVar);
      indicesBoundsA[2*idVar] = 0;
      indicesBoundsA[2*idVar+1] = A.nDof_var(idVar);
      indicesBoundsB[2*idVar] = 0;
      indicesBoundsB[2*idVar+1] = B.nDof_var(idVar);
    }
    else{
      dofPerDim[idVar] = A.nDof_var(idVar) + B.nDof_var(idVar);
      indicesBoundsA[2*idVar] = 0;
      indicesBoundsA[2*idVar+1] = A.nDof_var(idVar);
      indicesBoundsB[2*idVar] = A.nDof_var(idVar);
      indicesBoundsB[2*idVar+1] = dofPerDim[idVar];
    }
  }
  fullTensor toBeReturned(dofPerDim, A.comm());
  toBeReturned.assignSubtensor(indicesBoundsA,A);
  toBeReturned.assignSubtensor(indicesBoundsB,B);
  toBeReturned.finalize();
  return toBeReturned;
}


/* outer Product of two given tensors A and B
  - input: the tensors A and B (full tensors)
  - output: the tensor which is the outer product of the two
*/
fullTensor outerProduct(fullTensor& A, fullTensor& B){
  assert(A.comm() == B.comm());

  const unsigned int dim = A.nVar() + B.nVar();
  vector<unsigned int> dofPerDim(dim);
  for(unsigned int idVar=0; idVar<dim; idVar++){
    if(idVar<A.nVar()){
      dofPerDim[idVar] = A.nDof_var(idVar);
    }
    else{
      unsigned int idB = idVar - A.nVar();
      dofPerDim[idVar] = B.nDof_var(idB);
    }
  }
  fullTensor toBeReturned(dofPerDim, A.comm());

  for(unsigned int lA=0; lA<A.nEntries(); lA++){
    vector<unsigned int> indA = A.lin2sub(lA);
    double valA = A.tensorEntries(lA);
    vector<unsigned int> ind(dim);
    for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
      ind[idVar] = indA[idVar];
    }
    for(unsigned int lB=0; lB<B.nEntries(); lB++){
      vector<unsigned int> indB = B.lin2sub(lB);
      double valB = B.tensorEntries(lB);
      double val = valA*valB;
      for(unsigned int idVar=0; idVar<B.nVar(); idVar++){
          unsigned int idThis = idVar + A.nVar();
          ind[idThis] = indB[idVar];
      }
      unsigned int thisLin = toBeReturned.sub2lin(ind);
      toBeReturned.set_tensorElement(thisLin,val);
    }
  }
  toBeReturned.finalize();
  return toBeReturned;
}


/* mode I contraction of a tensor and a given vector
  - input: the vector b, an empty result tensor
  - output: this tensor contracted by b to provide result
*/
fullTensor modeIContraction(unsigned int modeI, fullTensor& A, vec& b){
  assert(b.size() == A.nDof_var(modeI));
  assert(A.comm() == b.comm());

  vector<unsigned int> dofPerVar(A.nVar()-1);
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
    if(idVar != modeI){
      dofPerVar[cc] = A.nDof_var(idVar);
      cc = cc + 1;
    }
  }
  fullTensor result(dofPerVar, A.comm());

  for(unsigned int l=0; l<result.nEntries(); l++){
    double value = 0.0;
    vector<unsigned int> indResult =  result.lin2sub(l);
    vector<unsigned int> thisInd(A.nVar());
    unsigned int count = 0;
    for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
      if(idVar != modeI){
        thisInd[idVar] = indResult[count];
        count = count + 1;
      }
    }
    for(unsigned int idDof=0; idDof<A.nDof_var(modeI); idDof++){
      thisInd[modeI] = idDof;
      unsigned int thisLin = A.sub2lin(thisInd);
      double tEnt = A.tensorEntries().getVecEl(thisLin);
      double bEnt = b.getVecEl(idDof);
      value = value + tEnt * bEnt;
    }
    if(value != 0.0){ // to promote sparsity
      result.set_tensorElement(indResult, value);
    }
  }
  result.finalize();
  return result;
};


/* operator +, to sum two Tensors
  - input: tensor A and B
  - output A + B
*/
fullTensor operator + (fullTensor& A, fullTensor& B){
  assert(A.hasSameStructure(B));
  fullTensor result;
  result.copyTensorFrom(A);
  result += B;
  return result;
}


/* scalar Product of Full Tensors:
  - input: two Full tensors
  - output: A:B
*/
double scalProd(fullTensor& A, fullTensor& B){
  assert(A.hasSameStructure(B));
  vec vA = A.tensorEntries();
  vec vB = B.tensorEntries();
  double result = scalProd(vA , vB);
  return result;
}


/* Multiply a full tensor by a matrix:
  - input: Full tensor, a matrix, the mode along which to compute.
  - output: T*M
*/
fullTensor matTensProd(unsigned int Jvar, fullTensor& T, mat& A){
  assert(T.nDof_var(Jvar)== A.nCols());

  vector<unsigned int> resPerDim(T.nVar());
  for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
    if(iVar != Jvar){
      resPerDim[iVar] = T.nDof_var(iVar);
    }
    else{
      resPerDim[iVar] = A.nRows();
    }
  }

  fullTensor result(resPerDim, T.comm());

  // compute the Jvar unfolding of the result:
  mat jUnfold = T.computeUnfolding(Jvar);
  mat resUnfold = A * jUnfold;

  // compute the tensor entries through mapping:
  unsigned int nEnt = resUnfold.nRows()*resUnfold.nCols();
  vec resEntries(nEnt, T.comm());
  for(unsigned int l=0; l<nEnt; l++){
    vector<unsigned int> ind = result.lin2sub(l);
    unsigned int iRow = ind[Jvar];
    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
      if(iVar != Jvar){
        jCol = jCol + unfoldInc*ind[iVar];
        unfoldInc = unfoldInc * resPerDim[iVar];
      }
    }
    double value = resUnfold.getMatEl(iRow, jCol);
    if(value != 0.0){
      resEntries.setVecEl(l, value);
    }
  }
  resEntries.finalize();
  result.set_tensorEntries(resEntries);
  result.finalize();

  // free the memory;
  resUnfold.clear();
  jUnfold.clear();

  return result;
}
