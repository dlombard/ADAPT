// Implementation of the fullTensor class:

#include "fullTensor.h"
#include "../linAlg/linearAlgebraOperations.h"


using namespace std;



// I - CONSTRUCTORS --

/* overloaded constructor
  - inputs: dimension, degrees of freedom per dimension, communicator.
  - output: construction of a full tensor
*/
fullTensor::fullTensor(unsigned int dim, vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_nVar = dim;
  m_nDof_var.resize(m_nVar);
  m_nEntries = 1;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
    m_nEntries = m_nEntries * m_nDof_var[iVar];
  }
  m_comm = theComm;
  m_tensorEntries.init(m_nEntries, m_comm);
  compute_minc();
  m_isInitialised = true;
};


/* overloaded constructor
  - inputs: degrees of freedom per dimension, communicator.
  - output: construction of a full tensor
*/
fullTensor::fullTensor(vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_nVar = dofPerDim.size();
  m_nDof_var.resize(m_nVar);
  m_nEntries = 1;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
    m_nEntries = m_nEntries * m_nDof_var[iVar];
  }
  m_comm = theComm;
  m_tensorEntries.init(m_nEntries, m_comm);
  compute_minc();
  m_isInitialised = true;
};


/* init - just a variation over constructor (after empty constructor is used)
  - input: dof per dimension, communicator
  - output: the tensor is initialised
*/
void fullTensor::init(vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_nVar = dofPerDim.size();
  m_nDof_var.resize(m_nVar);
  m_nEntries = 1;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
    m_nEntries = m_nEntries * m_nDof_var[iVar];
  }
  m_comm = theComm;
  m_tensorEntries.init(m_nEntries, m_comm);
  compute_minc();
  m_isInitialised = true;
}


/*  copy tensor structure from a given tensor:
  - input: the tensor whose structure is copied
  - output: the structure of this tensor is shaped
*/
void fullTensor::copyTensorStructFrom(fullTensor& source){

  if(m_isInitialised){clear();}

  m_comm = source.comm();
  m_nVar = source.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = source.nDof_var(iVar);
  }

  m_nEntries = 1;
  m_inc.resize(m_nVar);
  for(unsigned int idVar = 0; idVar < m_nVar; idVar++){
    m_inc[idVar] = 1;
    for (unsigned int n=0; n<idVar; n++){
        m_inc[idVar] = m_inc[idVar] * m_nDof_var[n];
    };
    m_nEntries = m_nEntries * m_nDof_var[idVar];
  }
  m_tensorEntries.init(m_nEntries, m_comm);
  m_isInitialised = true;
}


/* Copy the tensor from a given fullTensor:
  - input: the tensor to be copied
  - output: the tensor (structure + copy of the entries)
*/
void fullTensor::copyTensorFrom(fullTensor& source){
  copyTensorStructFrom(source);
  m_tensorEntries.copyVecFrom(source.tensorEntries());
  finalize();
}


/* check if this tensor has the same structure of a given full tensor
  - input: the tensor to compare with
  - output: true if if has the same structure, false otherwise
*/

bool fullTensor::hasSameStructure(fullTensor& toBeComparedTo){
    bool toBeReturned = true;
    if(m_nVar != toBeComparedTo.nVar()){
      return false;
    }
    else{
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        if(m_nDof_var[idVar] != toBeComparedTo.nDof_var(idVar)){
          return false;
        }
      }
    }
    return toBeReturned;
}


/* check if this tensor is equal a given full tensor
  - input: the tensor to compare with
  - output: true if it is equal, false otherwise
*/
bool fullTensor::isEqualTo(fullTensor& toBeComparedTo){
    bool toBeReturned = hasSameStructure(toBeComparedTo);
    if(toBeReturned){
      for(unsigned int l=0; l<m_nEntries; l++){
        double thisVal = m_tensorEntries.getVecEl(l);
        if(toBeComparedTo.tensorEntries(l) != thisVal){
          toBeReturned = false;
          break;
        }
      }
    }
    return toBeReturned;
}


/* check if this tensor is equal a given full tensor (overloaded operator)
  - input: the tensor to compare with
  - output: true if it is equal, false otherwise
*/
bool fullTensor::operator == (fullTensor& toBeCompared){
  bool isItEqual = isEqualTo(toBeCompared);
  return isItEqual;
}


/* sum a given tensor times alpha to the current tensor
  - input: the tensor to be summed
  - output: the present tensor T <- T + alpha S
*/

void fullTensor::sum(fullTensor& toBeAdded, double alpha){
  if(hasSameStructure(toBeAdded)){
    m_tensorEntries.sum(toBeAdded.tensorEntries(), alpha);
  }
  else{
    PetscPrintf(m_comm, "Tensor cannot be summed!\n");
  }
}


/* overloaded operator +=
  - input: the tensor to be summed
  - output:  T <- T + toBeAdded;
*/
void fullTensor::operator += (fullTensor& toBeAdded){
  if(hasSameStructure(toBeAdded)){
    m_tensorEntries.sum(toBeAdded.tensorEntries(), 1.0);
  }
  else{
    PetscPrintf(m_comm, "Tensor cannot be summed!\n");
  }
}


/* multiply this tensor by a scalar
  - input: the scalar
  - output: the present tensor T <- alpha T
*/
void fullTensor::multiplyByScalar(double alpha){
  m_tensorEntries *= alpha;
}


/* overloaded operator *=
  - input: the scalar
  - output: the present tensor T <- alpha T
*/
void fullTensor::operator *= (double alpha){
  m_tensorEntries *= alpha;
}


/* shift this tensor entries by a scalar
  - input: the scalar
  - output: the present tensor T <- alpha + T
*/
void fullTensor::shiftByScalar(double alpha){
  m_tensorEntries += alpha;
}


/* overloaded operator to shift
  - input: the scalar
  - output: the present tensor T <- alpha + T
*/
void fullTensor::operator += (double alpha){
  m_tensorEntries += alpha;
}


/* Extract a subtensor and put it into a given full tensor
  - input: the indices bounds (C notation [lower_1,upper_1, lower2, upper_2,...]) and an empty full tensor
  - output: the subtensor
*/
void fullTensor::extractSubtensor(vector<unsigned int> indicesBounds, fullTensor& target){
  assert(indicesBounds.size()==2*m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  unsigned int N = 1;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    resPerDim[idVar] = indicesBounds[2*idVar+1] - indicesBounds[2*idVar];
    N = N * resPerDim[idVar];
  }
  target.init(resPerDim, m_comm);
  for(unsigned int l=0; l<N; l++){
      vector<unsigned int> trgLocalInd = target.lin2sub(l);
      vector<unsigned int> thisIndices(m_nVar);
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        thisIndices[idVar] = trgLocalInd[idVar] + indicesBounds[2*idVar];
      }
      unsigned int linInd = sub2lin(thisIndices);
      double value = m_tensorEntries.getVecEl(linInd);
      if(value != 0.0){
        target.set_tensorElement(trgLocalInd,value);
      }
  }
  target.finalize();
}


/* Extract a subtensor: overloaded to provide a full tensor
  - input: the indices bounds (C notation [lower_1,upper_1, lower2, upper_2,...]) and an empty full tensor
  - output: the subtensor
*/
fullTensor fullTensor::extractSubtensor(vector<unsigned int> indicesBounds){
  assert(indicesBounds.size()==2*m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  unsigned int N = 1;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    resPerDim[idVar] = indicesBounds[2*idVar+1] - indicesBounds[2*idVar];
    N = N * resPerDim[idVar];
  }
  fullTensor toBeReturned(m_nVar, resPerDim, m_comm);
  for(unsigned int l=0; l<N; l++){
      vector<unsigned int> trgLocalInd = toBeReturned.lin2sub(l);
      vector<unsigned int> thisIndices(m_nVar);
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        thisIndices[idVar] = trgLocalInd[idVar] + indicesBounds[2*idVar];
      }
      unsigned int linInd = sub2lin(thisIndices);
      double value = m_tensorEntries.getVecEl(linInd);
      if(value != 0.0){
        toBeReturned.set_tensorElement(trgLocalInd,value);
      }
  }
  toBeReturned.finalize();
  return toBeReturned;
}


/* Extract a subtensor: overloaded to provide a full tensor
  - input: a list of indices
  - output: the subtensor (automatic extraction of bounds!)
*/
void fullTensor::extractSubtensor(vector<vector<unsigned int> > listOfEntries, fullTensor& result){
  vector<unsigned int> indicesBounds(2*m_nVar);
  const unsigned int nOfEl = listOfEntries.size();
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    unsigned int iMax = 0;
    unsigned int iMin = UINT_MAX;
    for(unsigned int iEl=0; iEl<nOfEl; iEl++){
      if(listOfEntries[iEl][iVar]>iMax){
        iMax = listOfEntries[iEl][iVar];
      }
      if(listOfEntries[iEl][iVar]<iMin){
        iMin = listOfEntries[iEl][iVar];
      }
    }
    indicesBounds[2*iVar] = iMin;
    indicesBounds[2*iVar+1] = iMax + 1; // C convention!
  }

  vector<unsigned int> resPerDim(m_nVar);
  unsigned int N = 1;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    resPerDim[idVar] = indicesBounds[2*idVar+1] - indicesBounds[2*idVar];
    N = N * resPerDim[idVar];
  }
  result.init(resPerDim, m_comm);
  for(unsigned int l=0; l<N; l++){
      vector<unsigned int> trgLocalInd = result.lin2sub(l);
      vector<unsigned int> thisIndices(m_nVar);
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        thisIndices[idVar] = trgLocalInd[idVar] + indicesBounds[2*idVar];
      }
      unsigned int linInd = sub2lin(thisIndices);
      double value = m_tensorEntries.getVecEl(linInd);
      if(value != 0.0){
        result.set_tensorElement(trgLocalInd,value);
      }
  }
  result.finalize();
}



/* Extract a subtensor: overloaded to provide a full tensor
  - input: a list of indices
  - output: the subtensor (automatic extraction of bounds!)
*/
fullTensor fullTensor::extractSubtensor(vector<vector<unsigned int> > listOfEntries){
  vector<unsigned int> indicesBounds(2*m_nVar);
  const unsigned int nOfEl = listOfEntries.size();
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    unsigned int iMax = 0;
    unsigned int iMin = UINT_MAX;
    for(unsigned int iEl=0; iEl<nOfEl; iEl++){
      if(listOfEntries[iEl][iVar]>iMax){
        iMax = listOfEntries[iEl][iVar];
      }
      if(listOfEntries[iEl][iVar]<iMin){
        iMin = listOfEntries[iEl][iVar];
      }
    }
    indicesBounds[2*iVar] = iMin;
    indicesBounds[2*iVar+1] = iMax + 1; // C convention!
  }

  vector<unsigned int> resPerDim(m_nVar);
  unsigned int N = 1;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    resPerDim[idVar] = indicesBounds[2*idVar+1] - indicesBounds[2*idVar];
    N = N * resPerDim[idVar];
  }
  fullTensor toBeReturned(m_nVar, resPerDim, m_comm);
  for(unsigned int l=0; l<N; l++){
      vector<unsigned int> trgLocalInd = toBeReturned.lin2sub(l);
      vector<unsigned int> thisIndices(m_nVar);
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        thisIndices[idVar] = trgLocalInd[idVar] + indicesBounds[2*idVar];
      }
      unsigned int linInd = sub2lin(thisIndices);
      double value = m_tensorEntries.getVecEl(linInd);
      if(value != 0.0){
        toBeReturned.set_tensorElement(trgLocalInd,value);
      }
  }
  toBeReturned.finalize();
  return toBeReturned;
}


/* Put a given subtensor into this full tensor, at specified positions
  - input: the indices bounds (C notation [lower_1,upper_1, lower2, upper_2,...]) and the full subtensor
  - output: the subtensor is put into the current tensor
*/
void fullTensor::assignSubtensor(vector<unsigned int> indicesBounds, fullTensor& source){
  assert(indicesBounds.size()==2*m_nVar);
  assert(source.comm()==m_comm);
  assert(source.nVar()==m_nVar);
  unsigned int N  = 1;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    assert(indicesBounds[2*idVar+1]-indicesBounds[2*idVar]<m_nDof_var[idVar]);
    N = N* (indicesBounds[2*idVar+1]-indicesBounds[2*idVar]);
  }
  for(unsigned int l=0; l<N; l++){
    vector<unsigned int> sourceIndices = source.lin2sub(l);
    vector<unsigned int> thisIndices(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      thisIndices[idVar] = sourceIndices[idVar] + indicesBounds[2*idVar];
    }
    unsigned int linInd = sub2lin(thisIndices);
    double value = source.tensorEntries(l);
    m_tensorEntries.setVecEl(linInd, value);
  }
  finalize();
}


/* reshape the current tensor into a new one (pointer given)
  - input: the set of new resolution per variables, COPY flag (to actually copy the entries)
  - output: result is the tensor reshaped.
  REMARK: could be use to have unfoldings as fullTensors ( without copy ) => fast
*/
void fullTensor::reshape(vector<unsigned int> nDofPerVar, fullTensor& result, bool COPY = true){
  assert(nDofPerVar.size()==m_nVar);
  unsigned int nDof = nDofPerVar[0];
  for(unsigned int idVar=1; idVar<m_nVar; idVar++){
    nDof = nDof * nDofPerVar[idVar];
  }
  assert(nDof == m_nEntries);
  result.init(nDofPerVar, m_comm);
  if(!COPY){
    result.set_tensorEntries(m_tensorEntries); // without copy
  }
  else{
    vec theEntries(m_nEntries, m_comm);
    theEntries.copyVecFrom(m_tensorEntries);
    result.set_tensorEntries(theEntries);
  }
  result.finalize();
}


/* reshape the current tensor into a new one
  - input: the set of new resolution per variables, COPY flag (to actually copy the entries)
  - output: result is the tensor reshaped.
  REMARK: could be use to have unfoldings as fullTensors ( without copy ) => fast
*/
fullTensor fullTensor::reshape(vector<unsigned int> nDofPerVar, bool COPY = true){
  assert(nDofPerVar.size()==m_nVar);
  unsigned int nDof = nDofPerVar[0];
  for(unsigned int idVar=1; idVar<m_nVar; idVar++){
    nDof = nDof * nDofPerVar[idVar];
  }
  assert(nDof == m_nEntries);
  fullTensor result(m_nVar, nDofPerVar, m_comm);
  if(!COPY){
    result.set_tensorEntries(m_tensorEntries); // without copy
  }
  else{
    vec theEntries(m_nEntries, m_comm);
    theEntries.copyVecFrom(m_tensorEntries);
    result.set_tensorEntries(theEntries);
  }
  result.finalize();
  return result;
}


/* reorder the variables:
  - input: a vector<unsigned int> containing the new variable order
  - output: remap the current tensor.
*/
void fullTensor::reorderVars(vector<unsigned int> order){
  assert(order.size()==m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    unsigned int id_var = order[iVar];
    resPerDim[iVar] = m_nDof_var[id_var];
  }

  fullTensor empty(resPerDim, m_comm);
  vec remappedEntries(m_nEntries,m_comm);
  for(unsigned int iEn=0; iEn<m_nEntries; iEn++){
    vector<unsigned int> originalIndices = lin2sub(iEn);
    vector<unsigned int> orderedIndices(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      unsigned int id = order[iVar];
      orderedIndices[iVar] = originalIndices[id];
    }
    unsigned int ord_linear = empty.sub2lin(orderedIndices);
    double value = m_tensorEntries.getVecEl(iEn);
    if(fabs(value)>DBL_EPSILON){remappedEntries.setVecEl(ord_linear,value);}
  }
  remappedEntries.finalize();
  m_tensorEntries.clear();
  m_tensorEntries = remappedEntries;
  // changing the variable order and the mappings:
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = resPerDim[iVar];
  }
  compute_minc();
}


/* mode I contraction of a tensor and a given vector
  - input: the vector b, an empty result tensor
  - output: this tensor contracted by b to provide result
*/
fullTensor fullTensor::modeIContraction(unsigned int modeI, vec b){
  assert(b.size() == m_nDof_var[modeI]);

  vector<unsigned int> dofPerVar(m_nVar-1);
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar != modeI){
      dofPerVar[cc] = m_nDof_var[idVar];
      cc = cc + 1;
    }
  }
  fullTensor result(dofPerVar, m_comm);

  for(unsigned int l=0; l<result.nEntries(); l++){
    double value = 0.0;
    vector<unsigned int> indResult =  result.lin2sub(l);
    vector<unsigned int> thisInd(m_nVar);
    unsigned int count = 0;
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(idVar != modeI){
        thisInd[idVar] = indResult[count];
        count = count + 1;
      }
    }
    for(unsigned int idDof=0; idDof<m_nDof_var[modeI]; idDof++){
      thisInd[modeI] = idDof;
      unsigned int thisLin = sub2lin(thisInd);
      double tEnt = m_tensorEntries.getVecEl(thisLin);
      double bEnt = b.getVecEl(idDof);
      value = value + tEnt * bEnt;
    }
    if(value != 0.0){
      result.set_tensorElement(indResult, value);
    }
  }
  result.finalize();
  return result;
};


/* in place mode I contraction of a tensor and a given vector
  - input: the vector b,
  - output: this tensor contracted by b
*/
void fullTensor::inplaceModeIContraction(unsigned int modeI, vec b){
  assert(b.size() == m_nDof_var[modeI]);

  vector<unsigned int> dofPerVar(m_nVar-1);
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar != modeI){
      dofPerVar[cc] = m_nDof_var[idVar];
      cc = cc + 1;
    }
  }
  fullTensor result(dofPerVar, m_comm);

  for(unsigned int l=0; l<result.nEntries(); l++){
    double value = 0.0;
    vector<unsigned int> indResult =  result.lin2sub(l);
    vector<unsigned int> thisInd(m_nVar);
    unsigned int count = 0;
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(idVar != modeI){
        thisInd[idVar] = indResult[count];
        count = count + 1;
      }
    }
    for(unsigned int idDof=0; idDof<m_nDof_var[modeI]; idDof++){
      thisInd[modeI] = idDof;
      unsigned int thisLin = sub2lin(thisInd);
      double tEnt = m_tensorEntries.getVecEl(thisLin);
      double bEnt = b.getVecEl(idDof);
      value = value + tEnt * bEnt;
    }
    if(value != 0.0){
      result.set_tensorElement(indResult, value);
    }
  }
  result.finalize();

  clear();
  copyTensorFrom(result);
  result.clear();
};


/* in place mode I contraction of a tensor and a set of vectors (overloaded)
  - input: the set of vectors b,
  - output: this tensor contracted by b
*/
void fullTensor::inplaceModeIContraction(unsigned int modeI, vector<vec> b){
  assert(b[0].size() == m_nDof_var[modeI]);
  const unsigned int nVec = b.size();

  vector<unsigned int> dofPerVar(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar != modeI){
      dofPerVar[idVar] = m_nDof_var[idVar];
    }
    else{
      dofPerVar[idVar] = nVec;
    }
  }
  fullTensor result(dofPerVar, m_comm);

  for(unsigned int l=0; l<result.nEntries(); l++){
    vector<unsigned int> ind_result = result.lin2sub(l);
    unsigned int id_b = ind_result[modeI];

    // fix the indices except in the direction modeI:
    vector<unsigned int> ind_original(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != modeI){
        ind_original[iVar] = ind_result[iVar];
      }
    }

    // contraction:
    double scalar = 0.0;
    for(unsigned int iDof=0; iDof<m_nDof_var[modeI]; iDof++){
      ind_original[modeI] = iDof;
      unsigned int l_original = sub2lin(ind_original);
      scalar += b[id_b].getVecEl(iDof) * m_tensorEntries.getVecEl(l_original);
    }
    if(scalar != 0.0){
      result.set_tensorElement(l, scalar);
    }
  }

  // put result in the current tensor:
  clear();
  copyTensorFrom(result);
  result.clear();
}


/* Compute unfolding and put it into a given matrix
  - input: the index along which the unfolding has to be computed
  - output: the unfolding into the Matrix
*/
void fullTensor::computeUnfolding(const unsigned int index, mat& M){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  M.init(nRows, nCols, m_comm);

  for(unsigned int l=0; l<m_nEntries; l++){
    double value = m_tensorEntries.getVecEl(l);

    if(value != 0.0){ // to provide some sparsity
      vector<unsigned int> ind = lin2sub(l);
      unsigned int iRow = ind[index];
      unsigned int jCol = 0;
      unsigned int unfoldInc = 1;
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        if(idVar != index){
          jCol = jCol + unfoldInc*ind[idVar];
          unfoldInc = unfoldInc * m_nDof_var[idVar];
        }
      }
      M.setMatEl(iRow,jCol, value);
    }
  }
  M.finalize();
}


/* Compute unfolding (overloaded to provide matrix)
  - input: the index along which the unfolding has to be computed
  - output: the unfolding into the Matrix
*/
mat fullTensor::computeUnfolding(const unsigned int index){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  mat M(nRows, nCols, m_comm);

  for(unsigned int l=0; l<m_nEntries; l++){
    double value = m_tensorEntries.getVecEl(l);
    if(value != 0.0){ // to provide some sparsity
      vector<unsigned int> ind = lin2sub(l);
      unsigned int iRow = ind[index];
      unsigned int jCol = 0;
      unsigned int unfoldInc = 1;
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        if(idVar != index){
          jCol = jCol + unfoldInc*ind[idVar];
          unfoldInc = unfoldInc * m_nDof_var[idVar];
        }
      }

      M.setMatEl(iRow,jCol, value);
    }
  }
  M.finalize();
  return M;
}


/* Compute unfolding SVD without computing the unfolding
  - input: the index along which the unfolding has to be computed, the tolerance.
  - output: the left modes and the singular vectors
*/
void fullTensor::computeUnfoldingSVD(const unsigned int index, vector<double>& S, vector<vec>& U, double tol){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  mat C(nRows, nRows, m_comm);

  /*for(unsigned int iRow=0; iRow<nRows; iRow++){
    for(unsigned int jRow=0; jRow<=iRow; jRow++){
      double scalar = 0.0;
      for(unsigned int kCol=0; kCol<nCols; kCol++){
        double val_I = evalUnfolding(index, iRow, kCol);
        double val_J = evalUnfolding(index, jRow, kCol);
        scalar += val_I * val_J;
      }
      C.setMatEl(iRow, jRow, scalar);
      if(iRow != jRow){C.setMatEl(jRow, iRow, scalar);}
    }
  }
  C.finalize();*/
  // loop order which reduces the communications and unfolding evaluations
  for(unsigned int iRow=0; iRow<nRows; iRow++){
    //cout << iRow << endl;
    vector<double> scalar(iRow+1);
    for(unsigned int kCol=0; kCol<nCols; kCol++){
      double val_I = evalUnfolding(index, iRow, kCol);
      if(fabs(val_I) > DBL_EPSILON){
        for(unsigned int jRow=0; jRow<=iRow; jRow++){
          double val_J = evalUnfolding(index, jRow, kCol);
          scalar[jRow] += val_I * val_J;
        }
      }
    }
    for(unsigned int jRow=0; jRow<=iRow; jRow++){
      C.setMatEl(iRow, jRow, scalar[jRow]);
      if(iRow != jRow){C.setMatEl(jRow, iRow, scalar[jRow]);}
    }
    scalar.clear();
  }
  C.finalize();

  eigenSolver eps;
  eps.init(C, EPS_HEP, nRows, tol);
  eps.solveHermEPS(C);
  vector<vec> vv = eps.eigenVecs();
  vector<double> lambda = eps.eigenVals();

  // setting the singular values:
  const unsigned int nS = lambda.size();
  S.resize(nS);
  for(unsigned int iS=0; iS<nS; iS++){
    S[iS] = sqrt(fabs(lambda[iS]));
  }

  // copy the mode, sparsify if needed:
  U.resize(nS);
  for(unsigned int iS=0; iS<nS; iS++){
    U[iS].copyVecFrom(vv[iS]);
  }

  // free the memory:
  eps.clear();
  C.clear();
}


/* Compute unfolding and put it into a set of vectors (get the fibers)
  - input: the index along which the unfolding has to be computed
  - output: the set of fibers
*/
void fullTensor::computeFibers(const unsigned int index, vector<vec>& fibers){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nFibers = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nFibers = nFibers*m_nDof_var[idVar];
		}
	}

  fibers.resize(nFibers);
  for(unsigned int jFiber=0; jFiber<nFibers; jFiber++){
    fibers[jFiber].init(nRows, m_comm);
  }

  for(unsigned int l=0; l<m_nEntries; l++){
    double value = m_tensorEntries.getVecEl(l);
    if(value != 0.0){
      vector<unsigned int> ind = lin2sub(l);
      unsigned int iRow = ind[index];
      unsigned int jFiber = 0;
      unsigned int unfoldInc = 1;
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        if(idVar != index){
          jFiber = jFiber + unfoldInc*ind[idVar];
          unfoldInc = unfoldInc * m_nDof_var[idVar];
        }
      }
      if(fabs(value)>DBL_EPSILON){
        fibers[jFiber].setVecEl(iRow, value);
      }
    }
  }

  for(unsigned int jFiber=0; jFiber<nFibers; jFiber++){
    fibers[jFiber].finalize();
  }
}


/* Compute unfolding overloaed to provide fibers
  - input: the index along which the unfolding has to be computed
  - output: the set of fibers
*/
vector<vec> fullTensor::computeFibers(const unsigned int index){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nFibers = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nFibers = nFibers*m_nDof_var[idVar];
		}
	}

  vector<vec> fibers(nFibers);
  for(unsigned int jFiber=0; jFiber<nFibers; jFiber++){
    fibers[jFiber].init(nRows, m_comm);
  }

  for(unsigned int l=0; l<m_nEntries; l++){
    double value = m_tensorEntries.getVecEl(l);
    if(value != 0.0){
      vector<unsigned int> ind = lin2sub(l);
      unsigned int iRow = ind[index];
      unsigned int jFiber = 0;
      unsigned int unfoldInc = 1;
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        if(idVar != index){
          jFiber = jFiber + unfoldInc*ind[idVar];
          unfoldInc = unfoldInc * m_nDof_var[idVar];
        }
      }
      if(fabs(value)>DBL_EPSILON){
        fibers[jFiber].setVecEl(iRow, value);
      }
    }
  }

  for(unsigned int jFiber=0; jFiber<nFibers; jFiber++){
    fibers[jFiber].finalize();
  }
  return fibers;
}


/* Evaluate unfolding without assembling it!
  - input: the index along which the unfolding has to be computed, (I,J) of the unfolding
  - output: the value of the unfolding_[index] in I,J
*/
void fullTensor::evalUnfolding(const unsigned int index, const unsigned int I, const unsigned int J, double& value){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}

  vector<unsigned int> ind(m_nVar);
  ind[index] = I;
  //define the unfolding increments
  vector<unsigned int> unfoldInc(m_nVar-1);
  unsigned int unfInc = 1;
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar!=index){
      unfoldInc[cc] = unfInc;
      unfInc = unfInc * m_nDof_var[idVar];
      cc = cc + 1;
    }
  }
  // compute the tensor indices
  unsigned int reminder = J;
  cc = m_nVar-2;
  for (int h=m_nVar-1; h>=0; h--){
      if(h != index){
        ind[h] = reminder/unfoldInc[cc];
        reminder = reminder - ind[h] * unfoldInc[cc];
        cc = cc - 1;
      }
  };
  // get the element
  unsigned int linInd = sub2lin(ind);
  value = m_tensorEntries.getVecEl(linInd);
}


/* Evaluate unfolding without assembling it!
  - input: the index along which the unfolding has to be computed, (I,J) of the unfolding
  - output: the value of the unfolding_[index] in I,J
*/
double fullTensor::evalUnfolding(const unsigned int index, const unsigned int I, const unsigned int J){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}

  vector<unsigned int> ind(m_nVar);
  ind[index] = I;
  //define the unfolding increments
  vector<unsigned int> unfoldInc(m_nVar-1);
  unsigned int unfInc = 1;
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar!=index){
      unfoldInc[cc] = unfInc;
      unfInc = unfInc * m_nDof_var[idVar];
      cc = cc + 1;
    }
  }
  // compute the tensor indices
  unsigned int reminder = J;
  cc = m_nVar-2;
  for (int h=m_nVar-1; h>=0; h--){
      if(h != index){
        ind[h] = reminder/unfoldInc[cc];
        reminder = reminder - ind[h] * unfoldInc[cc];
        cc = cc - 1;
      }
  };
  // get the element
  unsigned int linInd = sub2lin(ind);
  double value = m_tensorEntries.getVecEl(linInd);
  return value;
}


/* in place matrix tensor product
  - input: the variable along which to compute the product
  - output: this tensor is transformed into the result
*/
void fullTensor::inPlaceMatTensProd(unsigned int Jvar, mat& A){
  assert(m_nDof_var[Jvar]== A.nCols());

  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != Jvar){
      resPerDim[iVar] = m_nDof_var[iVar];
    }
    else{
      resPerDim[iVar] = A.nRows();
    }
  }

  // compute the Jvar unfolding of the result:
  mat jUnfold = computeUnfolding(Jvar);
  mat resUnfold = A * jUnfold;

  // clear the current tensor and re-init:
  MPI_Comm currentComm = m_comm;
  clear();
  init(resPerDim, currentComm);

  // compute the tensor entries through mapping:
  unsigned int nEnt = resUnfold.nRows()*resUnfold.nCols();
  m_tensorEntries.init(nEnt, m_comm);
  for(unsigned int l=0; l<nEnt; l++){
    vector<unsigned int> ind = lin2sub(l);
    unsigned int iRow = ind[Jvar];
    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != Jvar){
        jCol = jCol + unfoldInc*ind[iVar];
        unfoldInc = unfoldInc * resPerDim[iVar];
      }
    }
    double value = resUnfold.getMatEl(iRow, jCol);
    if(value != 0.0){
      m_tensorEntries.setVecEl(l, value);
    }
  }
  m_tensorEntries.finalize();

  // free the memory;
  resUnfold.clear();
  jUnfold.clear();
}



/* matrix tensor product
  - input: the variable along which to compute the product
  - output: this tensor is transformed into the result
*/
fullTensor fullTensor::matTensProd(unsigned int Jvar, mat& A){
  assert(m_nDof_var[Jvar]== A.nCols());

  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != Jvar){
      resPerDim[iVar] = m_nDof_var[iVar];
    }
    else{
      resPerDim[iVar] = A.nRows();
    }
  }

  fullTensor result(resPerDim, m_comm);

  // compute the Jvar unfolding of the result:
  mat jUnfold = computeUnfolding(Jvar);
  mat resUnfold = A * jUnfold;

  // compute the tensor entries through mapping:
  unsigned int nEnt = resUnfold.nRows()*resUnfold.nCols();
  vec resEntries(nEnt, m_comm);
  for(unsigned int l=0; l<nEnt; l++){
    vector<unsigned int> ind = result.lin2sub(l);
    unsigned int iRow = ind[Jvar];
    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != Jvar){
        jCol = jCol + unfoldInc*ind[iVar];
        unfoldInc = unfoldInc * resPerDim[iVar];
      }
    }
    double value = resUnfold.getMatEl(iRow, jCol);
    if(value != 0.0){
      resEntries.setVecEl(l, value);
    }
  }
  resEntries.finalize();
  result.set_tensorEntries(resEntries);
  result.finalize();

  // free the memory;
  resUnfold.clear();
  jUnfold.clear();

  return result;
}


/* saving a full tensor:
  - input: a string
  - output: a file containing the entries in binary, a log file containing the dofs per variable
*/
void fullTensor::save(string fName){
  string entriesName = fName + ".tns";
  m_tensorEntries.save(entriesName);
  string logName = fName + ".log";
  ofstream logFile(logName.c_str());
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    logFile << m_nDof_var[iVar] << endl;
  }
  logFile.close();
}

/* load a full tensor:
  - input: a string
  - output: this fullTensor is filled with the created tensor
*/
void fullTensor::load(string fName, MPI_Comm theComm){
  string logName = fName + ".log";
  ifstream logFile(logName.c_str());
  vector<unsigned int> tmp;
  unsigned int value;
  while(logFile >> value){
    tmp.push_back(value);
  }
  init(tmp, theComm);

  string entriesName = fName + ".tns";
  m_tensorEntries.load(entriesName);
}


/* Importing full tensor from FROSTT repository.
  - Input: file name, reference to an empty full tensor, number of entries per line
  - Output: initialise and fill the full tensor.
*/
void fullTensor::importFrostt(string fName, unsigned int nInLine, MPI_Comm theComm){
  m_comm = theComm;
  unsigned int dim = nInLine - 1;

  ifstream inputFile;
  inputFile.open(fName.c_str());
  char output[128];
  unsigned int IthMode;
  double tensorValue;

  unsigned int cc = 0;

  vector<double>* values;
  values = new vector<double>;

  vector<vector<int> >* tab;
  tab = new vector<vector<int> >;

  vector<int> maxModes(nInLine-1, 0); // assuming modes are positive in the tensor!!
  vector<int> minModes(nInLine-1, 16777216);  // 2^24
  vector<int> thisLine;
  if (inputFile.is_open()) {
    cout << "Reading...";
    while (!inputFile.eof()) {
      inputFile >> output;
      stringstream str;
      str << output;
      //vector<int> thisLine(nInLine-1);
      if(cc%nInLine<nInLine-1){
        unsigned int iVar = cc%nInLine;
        if(iVar==0){thisLine.clear(); thisLine.resize(nInLine-1);}
        // convert it to unsigned int
        str >> IthMode;
        thisLine[iVar] = int(IthMode);
        if(IthMode>maxModes[iVar]){
          maxModes[iVar] = IthMode;
        }
        if(IthMode<minModes[iVar]){
          minModes[iVar] = IthMode;
        }
        if(iVar == nInLine-2){
          tab->push_back(thisLine);
        }
      }
      else{
        // it is the tensor value, convert it to double
        str >> tensorValue;
        values->push_back(tensorValue);
      }
      cc +=1;
     }
     cout << "done.\n";
  }
  else{
    puts("Unable to open file!");
    exit(1);
  }
  inputFile.close();


  unsigned int nOfNonZeros = values->size();

  /* analize the tab to get the resolution.
  assuming implicitly that the step in each mode is 1 (otherwise a map is required!)
  */
  vector<unsigned int> resPerDim(nInLine-1);
  for(unsigned int iVar = 0; iVar< nInLine-1; iVar++){
    assert(maxModes[iVar] > minModes[iVar]);
    resPerDim[iVar] = maxModes[iVar] - minModes[iVar] + 1;
  }

  m_nVar = nInLine-1;
  m_nDof_var.resize(m_nVar);
  m_nEntries = 1;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = resPerDim[iVar];
    m_nEntries = m_nEntries * m_nDof_var[iVar];
  }
  m_tensorEntries.init(m_nEntries, m_comm);
  compute_minc();
  m_isInitialised = true;


  // tensor is ready to be filled with the data collected.
  for(unsigned int iEntry=0; iEntry<nOfNonZeros; iEntry++){
    double val = (*values)[iEntry];
    if(fabs(val)>DBL_EPSILON){
      vector<unsigned int> indices(nInLine-1);
      for(unsigned int iVar=0; iVar<nInLine-1; iVar++){
        indices[iVar] = (*tab)[iEntry][iVar] - minModes[iVar];
      }
      unsigned int lin = sub2lin(indices);
      m_tensorEntries.setVecEl(lin, val);
    }
    //set_tensorElement(indices, (*values)[iEntry]);
  }
  m_tensorEntries.finalize();

  delete values;
  delete tab;
}
