// Header of the full tensor daughter class

#ifndef fullTensor_h
#define fullTensor_h

#include "../tensor.h"
#include "../linAlg/vec.h"
#include "../linAlg/mat.h"
#include "../linAlg/eigenSolver.h"

class fullTensor: public tensor{
private:
  unsigned int m_nEntries;
  vec m_tensorEntries;
  bool m_isInitialised;

public:

  // -- CONSTRUCTORS and DESTRUCTOR --
  fullTensor(){m_isInitialised=false;};
  ~fullTensor(){};
  void clear(){m_tensorEntries.clear();}

  void init(vector<unsigned int>, MPI_Comm);


  // overloaded to initialise an empty tensor
  fullTensor(unsigned int, vector<unsigned int>, MPI_Comm);
  fullTensor(vector<unsigned int>, MPI_Comm);


  // finalize the parallel assembly:
  inline void finalize(){
    m_tensorEntries.finalize();
  }


  // -- ACCESS FUNCTIONS: --
  inline vec tensorEntries(){return m_tensorEntries;}
  inline double tensorEntries(unsigned int l){return m_tensorEntries.getVecEl(l);}
  inline bool isInitialised(){return m_isInitialised;}
  inline unsigned int nEntries(){return m_nEntries;}

  // -- SETTERS: --
  inline void set_tensorEntries(vec& entries){
    m_tensorEntries = entries;
    m_nEntries = entries.size();
  }

  inline void set_tensorElement(vector<unsigned int> ind, double val){
    unsigned int lin = sub2lin(ind);
    m_tensorEntries.setVecEl(lin, val);
  }

  inline void set_tensorElement(unsigned int lin, double val){
    m_tensorEntries.setVecEl(lin, val);
  }


  // -- EVAL function for full tensors --

  // basic void version:
  void eval(const vector<unsigned int>& ind, double& val){
		unsigned int lin = sub2lin(ind);
		val = m_tensorEntries.getVecEl(lin);
	};

  // overloaded to return a double:
  double eval(const vector<unsigned int>& ind){
    unsigned int lin = sub2lin(ind);
		double val = m_tensorEntries.getVecEl(lin);
    return val;
  }

  // overloaded with variadic list of indices:
  double eval(unsigned int iComp,...){
    vector<unsigned int> indices(m_nVar);
    va_list ap;
    va_start(ap, iComp);
    indices[0] = iComp;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      indices[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);
    unsigned int lin = sub2lin(indices);
    double val = m_tensorEntries.getVecEl(lin);
    return val;
  }

  // Nested proxy class for accessing and manipulating tensor elements
  // overloading operator () in assignement through a proxy class:
  class Proxy : public tensor
  {
    vector<unsigned int> idx;
    vec x;
  public:

      // Set Tensor Elements:
      inline void set_tensorElement(vector<unsigned int> ind, double val){
        unsigned int lin = sub2lin(ind);
        x.setVecEl(lin, val);
      }

      // overloaded to return a double:
      double eval(const vector<unsigned int>& ind){
        unsigned int lin = sub2lin(ind);
    		double val = x.getVecEl(lin);
        return val;
      }

      // Constructor of the proxy class:
      Proxy(vector<unsigned int> idx, vec x, vector<unsigned int> dofPerDim) : idx(idx), x(x){
        m_nVar = dofPerDim.size();
        m_nDof_var.resize(m_nVar);
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          m_nDof_var[iVar] = dofPerDim[iVar];
        }
        compute_minc();
      }

      // equal operator overload:
      inline double operator= (double value) {
        set_tensorElement(idx, value);
        return value;
      }

      // Overloading the double operator for assignement:
      operator double(){
        double toBeReturned = eval(idx);
        return toBeReturned;
      }
  };

  // Calling the proxy object through variadic:
  Proxy operator() (unsigned int I,...) {

    vector<unsigned int> indices(m_nVar);
    va_list ap;
    va_start(ap, I);
    indices[0] = I;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      indices[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);
    return Proxy(indices, m_tensorEntries, m_nDof_var);
  }


  // -- METHODS for full tensor: --

  // COPY Tensor Structure:
	void copyTensorStructFrom(fullTensor&);

	// COPY the full tensor:
	void copyTensorFrom(fullTensor&);

  // OPERATIONS on full tensor:
	bool hasSameStructure(fullTensor&);
	bool isEqualTo(fullTensor&);
  bool operator == (fullTensor&);
	void sum(fullTensor&, double);
  void operator += (fullTensor&);
	void multiplyByScalar(double);
  void operator *= (double);
	void shiftByScalar(double);
  void operator += (double);
	void extractSubtensor(vector<unsigned int>, fullTensor&);
  fullTensor extractSubtensor(vector<unsigned int>);
  void extractSubtensor(vector<vector<unsigned int> >, fullTensor&);
  fullTensor extractSubtensor(vector<vector<unsigned int> >);
	void assignSubtensor(vector<unsigned int>, fullTensor&);
	void reshape(vector<unsigned int>, fullTensor&, bool);
  fullTensor reshape(vector<unsigned int>, bool);
  void reorderVars(vector<unsigned int>);
  fullTensor modeIContraction(unsigned int, vec);
  void inplaceModeIContraction(unsigned int, vec);
  void inplaceModeIContraction(unsigned int, vector<vec>);

  // specific to fullTensor:
  void inPlaceMatTensProd(unsigned int Jvar, mat& A);
  fullTensor matTensProd(unsigned int Jvar, mat& A);

	// - Compute unfolding -
	void computeUnfolding(const unsigned int, mat&);
  mat computeUnfolding(const unsigned int);
	void computeFibers(const unsigned int, vector<vec>&);
  vector<vec> computeFibers(const unsigned int);
  void computeUnfoldingSVD(const unsigned int, vector<double>&, vector<vec>&, double);
	void evalUnfolding(const unsigned int,const unsigned int,const unsigned int,double&);
  double evalUnfolding(const unsigned int,const unsigned int,const unsigned int);
  void save(string);
  void load(string, MPI_Comm);
  void importFrostt(string, unsigned int, MPI_Comm);

};




// end of File
#endif
