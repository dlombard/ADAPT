// Header for fullTensor operations

#ifndef fullTensorOperations_hpp
#define fullTensorOperations_hpp

#include "fullTensor.h"



/* Left Kronecker product of tensors
  - input: tensors A and B
  - output: a tensor which is the Kronecker product of the given tensors
*/
fullTensor leftKronecker(fullTensor& A, fullTensor& B);


/* Right Kronecker product of tensors
  - input: tensors A and B
  - output: this tensor (empty) is the right Kronecker product of the tensors
*/
fullTensor rightKronecker(fullTensor& A, fullTensor& B);


/* mode I Khatri-Rao product of two given full Tensors.
  - input: the full tensors A and B, the mode I
  - output: full tensor which is the mode I Khatri-Rao product of A and B
*/
fullTensor modeIKhatriRao(unsigned int modeI, fullTensor& A, fullTensor& B);



/* mode I concatenation of two full order tensors
  - input: tensor A and tensor B in full tensor format
  - output: this tensor is the concatenation
*/
fullTensor modeIConcatenate(unsigned int modeI, fullTensor& A, fullTensor& B);


/* outer Product of two given tensors A and B
  - input: the tensors A and B (full tensors)
  - output: the tensor which is the outer product of the two
*/
fullTensor outerProduct(fullTensor& A, fullTensor& B);


/* mode I contraction of a tensor and a given vector
  - input: the vector b, an empty result tensor
  - output: this tensor contracted by b to provide result
*/
fullTensor modeIContraction(unsigned int modeI, fullTensor& A, vec& b);


/* operator +, to sum two Tensors
  - input: tensor A and B
  - output A + B
*/
fullTensor operator + (fullTensor& A, fullTensor& B);


/* scalar Product of Full Tensors:
  - input: two Full tensors
  - output: A:B
*/
double scalProd(fullTensor& A, fullTensor& B);


/* Multiply a full tensor by a matrix:
  - input: Full tensor, a matrix, the mode along which to compute.
  - output: T*M
*/
fullTensor matTensProd(unsigned int, fullTensor&, mat&);


#endif
