// Created by Damiano & Virginie on 03-04-2015
// implementation of daughter class finite difference discretisation for velocity:


#include "nearest.h"

using namespace std;


// Implementation of the class nearest, implementing k-nearest neighbors search

/* Initialise:
  - input: the data points, the parameter of the KD-tree
  - output: the object is initialised
*/
void nearest::initialize(vector<vector<double>* >& data, int k, int numTree, int numCheck){
  m_dataset = new flann::Matrix<float>;
  *m_dataset = fromVecDoubleToFlann(data);

  m_nOfNeigh = k;
  m_nOfTrees = numTree;
  m_nOfCheck = numCheck;
  m_index = new flann::Index<flann::L2<float> > (*m_dataset, flann::KDTreeIndexParams(m_nOfTrees));
  m_index->buildIndex();
};


/* Computing the k-nearest neighbors
  - input: set of points
  - output: the nearest neighbors
*/
void nearest::computeKNN(vector<vector<double>* >& pts){

   // 1 - convert pts into m_dataset and query:
   m_query = new flann::Matrix<float>;
   *m_query = fromVecDoubleToFlann(pts);

   // 2 - perform the computation:
   flann::Matrix<int> outIndices(new int[m_query->rows*m_nOfNeigh], m_query->rows, m_nOfNeigh);
   flann::Matrix<float> outDists(new float[m_query->rows*m_nOfNeigh], m_query->rows, m_nOfNeigh);
   m_index->knnSearch(*m_query, outIndices, outDists, m_nOfNeigh, flann::SearchParams(m_nOfCheck));

   // 3 - converting the results in vec
   m_indices = fromFlannIntToVec(outIndices);
   m_dists = fromFlannFloatToVec(outDists);

   delete[] outIndices.ptr();
   delete[] outDists.ptr();
};


/* Computing the k-nearest neighbors
  - input: set of points
  - output: the nearest neighbors
*/
void nearest::computeHierarchical(vector<vector<double>* >& pts){
  flann::KMeansIndexParams kmean_params(5, 1000, flann::FLANN_CENTERS_KMEANSPP, 0.001);

  // 1 - convert pts into m_query:
  m_dataset = new flann::Matrix<float>;
  *m_dataset = fromVecDoubleToFlann(pts);

  const int nOfPt = pts.size();
  const int dim = pts[0]->size();

  flann::Matrix<float> centers(new float[nOfPt*dim], nOfPt,dim);

  int nOfClusters = flann::hierarchicalClustering<flann::L2<float> >(*m_dataset, centers, kmean_params);
  cout << "N of Clusters = " << nOfClusters << endl;
  cout << centers[0][0] << " " << centers[0][1] << endl;
}


/* Returns the coordinates of the k-nearest neighbors of the point p
  - input: the index of the target point
  - output: the list of the k neighbors coordinates
*/
vector<vector<double>* > nearest::neighCoords(int p){
  if(p >= m_query->rows){
    puts("Out of range of the target points!");
    exit(1);
  };

  vector<vector<double>* > outVec; outVec.resize(m_nOfNeigh);
  for(size_t h=0; h<m_nOfNeigh; h++){
    vector<double>* hThRow;
    hThRow = new vector<double>;
    hThRow->resize(m_dataset->cols);
    for(size_t k=0; k<m_dataset->cols; k++){
      (*hThRow)[k] = *(m_dataset->ptr() + (*m_indices[p])[h] * m_dataset->cols  + k);
    }
    outVec[h] = hThRow;
  };
  return outVec;
};

// overloaded function, without allocating memory inside:
void nearest::neighCoords(int p, vector<vector<double>* >& outVec){
  if(p >= m_query->rows){
    puts("Out of range of the target points!");
    exit(1);
  };
  for(size_t h=0; h<m_nOfNeigh; h++){
    for(size_t k=0; k<m_dataset->cols; k++){
      (*outVec[h])[k] = *(m_dataset->ptr() + (*m_indices[p])[h] * m_dataset->cols  + k);
    }
  }
}




// Defining Conversions:

// vec --> Flann<float>
flann::Matrix<float> nearest::fromVecDoubleToFlann(vector<vector<double>* >& inVec){
  size_t rows = inVec.size();
  size_t cols = inVec[0]->size();
  size_t size = rows*cols;
  flann::Matrix<float> outMat(new float[size], rows, cols);
  for(size_t n = 0; n < size; ++n){
      *(outMat.ptr()+n) = (*inVec[n/cols])[n%cols];
  };
  return outMat;
};


// vec --> Flann<int>
flann::Matrix<int>   nearest::fromVecIntToFlann(vector<vector<int>* >& inVec){
  size_t rows = inVec.size();
  size_t cols = inVec[0]->size();
  size_t size = rows*cols;
  flann::Matrix<int> outMat(new int[size], rows, cols);
  for(size_t n = 0; n < size; ++n){
      *(outMat.ptr()+n) = (*inVec[n/cols])[n%cols];
  };
  return outMat;
};


// Flann<float> --> vec
vector<vector<double>* >  nearest::fromFlannFloatToVec(flann::Matrix<float>& inMat ){
  vector<vector<double>* > outVec;
  outVec.resize(inMat.rows);
  size_t counter = 0;
  for(size_t h=0; h<inMat.rows; h++){
    vector<double>* hThRow;
    hThRow = new vector<double>;
    hThRow->resize(inMat.cols);
    for(size_t k=0; k<inMat.cols; k++){
      (*hThRow)[k] = *(inMat.ptr() + counter);
      counter = counter + 1;
    }
    outVec[h] = hThRow;
  };
  return outVec;
};



// Flann<int> --> vec
vector<vector<int>* >  nearest::fromFlannIntToVec(flann::Matrix<int>& inMat ){
  vector<vector<int>* > outVec;
  outVec.resize(inMat.rows);
  size_t counter = 0;
  for(size_t h=0; h<inMat.rows; h++){
    vector<int>* hThRow;
    hThRow = new vector<int>;
    hThRow->resize(inMat.cols);
    for(size_t k=0; k<inMat.cols; k++){
      (*hThRow)[k] = *(inMat.ptr() + counter);
      counter = counter + 1;
    }
    outVec[h] = hThRow;
  };
  return outVec;
};
