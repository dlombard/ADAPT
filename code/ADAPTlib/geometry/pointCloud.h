// Header file for unstructured points

#ifndef pointCloud_h
#define pointCloud_h


#include "../genericInclude.h"
#include "../linAlg/vec.h"
#include "../linAlg/mat.h"
#include "../linAlg/linearAlgebraOperations.h"
#include "../linAlg/eigenSolver.h"


class pointCloud{
private:
  vector<vector<double> > m_pts;
  unsigned int m_nOfPt;
  unsigned int m_dim;

public:
  pointCloud(){};
  ~pointCloud(){};

  // overloaded contructors and destructors:
  pointCloud(vector<vector<double> >& ptsSet){
    m_pts = ptsSet;
    m_nOfPt = ptsSet.size();
    m_dim = ptsSet[0].size();
  }

  // init empty cloud point:
  pointCloud(unsigned int N, unsigned int d){
    m_nOfPt = N;
    m_dim = d;
    m_pts.resize(m_nOfPt);
    for(unsigned int iPt=0; iPt<m_nOfPt; iPt++){
      m_pts[iPt].resize(m_dim);
    }
  }

  // init in case of empty constructor initialization:
  inline void init(unsigned int N, unsigned int d){
    m_nOfPt = N;
    m_dim = d;
    m_pts.resize(m_nOfPt);
    for(unsigned int iPt=0; iPt<m_nOfPt; iPt++){
      m_pts[iPt].resize(m_dim);
    }
  }

  inline void init(const pointCloud& ptsSet) {
    m_pts = ptsSet.m_pts;
    m_nOfPt = ptsSet.m_nOfPt;
    m_dim = ptsSet.m_dim;
  }

  // clear:
  inline void clear(){
    for (unsigned int iPt=0; iPt<m_nOfPt; ++iPt) {
      m_pts[iPt].clear();
    }
    m_pts.clear();
    m_nOfPt = 0;
    m_dim = 0;
  }

  // ACCESS FUNCTIONS:
  inline vector<vector<double> > pts(){return m_pts;}
  inline vector<double> pts(unsigned int iPt){return m_pts[iPt];}
  inline const double pts(const unsigned int iPt, const unsigned int iComp) const {return m_pts[iPt][iComp];}
  inline double pts(const unsigned int iPt, const unsigned int iComp) {return m_pts[iPt][iComp];}
  inline const unsigned int nOfPt() const {return m_nOfPt;}
  inline const unsigned int dim() const {return m_dim;}


  // METHODS:

  // 1 -  provide the list of the iComp coordinates of the points;
  inline vector<double> listOfCoords(unsigned int iComp){
    vector<double> list(m_nOfPt);
    for(unsigned int iPt=0; iPt<m_nOfPt; iPt++){
      list[iPt] = m_pts[iPt][iComp];
    }
    return list;
  }

  // 2 - erase one point:
  inline void erasePoint(unsigned int iPt){
    assert(iPt<m_nOfPt);
    vector<vector<double> >::iterator it = m_pts.begin() + iPt;
    m_pts.erase(it);
    m_nOfPt = m_nOfPt - 1;
  }

  // 3 - add one point:
  inline void addPoint(vector<double> pt){
    assert(pt.size() == m_dim);
    m_pts.push_back(pt);
    m_nOfPt = m_nOfPt + 1;
  }

  // 4 - set point:
  inline void set_point(const unsigned int iPt, const std::vector<double>& coord){
    for(unsigned int iComp=0; iComp<m_dim; ++iComp) {
      m_pts[iPt][iComp] = coord[iComp];
    }
  }

  inline void set_point(const unsigned int iPt, const unsigned int iComp, const double value) {
    assert(iPt<m_nOfPt && iComp<m_dim);
    m_pts[iPt][iComp] = value;
  }

  // 5 - shift by a constant vector
  inline void operator += (vector<double> shift){
    assert(shift.size() == m_dim);
    for(unsigned int iPt=0; iPt<m_nOfPt; iPt++){
      for(unsigned int iComp=0; iComp<m_dim; iComp++){
        m_pts[iPt][iComp] += shift[iComp];
      }
    }
  }

  // 6 - multiply by scalar (usefull to rescale)
  inline void operator *= (double alpha){
    for(unsigned int iPt=0; iPt<m_nOfPt; iPt++){
      for(unsigned int iComp=0; iComp<m_dim; iComp++){
        m_pts[iPt][iComp] *= alpha;
      }
    }
  }

  // 7 - random assignment with uniform distribution in [0,1]^d
  inline void rand(unsigned int N, unsigned int d){
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(0.0, 1.0);
    for(unsigned int iPt=0; iPt<m_nOfPt; iPt++){
      for(unsigned int iComp=0; iComp<m_dim; iComp++){
        m_pts[iPt][iComp] = distribution(generator);
      }
    }
  }

  // 7.a) overloaded to take bounds into account:
  inline void rand(unsigned int N, unsigned int d, vector<double> bounds){
    assert(bounds.size() == 2*m_dim);
    std::default_random_engine generator;
    for(unsigned int iComp=0; iComp<m_dim; iComp++){
      const double lb = bounds[2*iComp];
      const double ub = bounds[2*iComp + 1];
      std::uniform_real_distribution<double> distribution(lb, ub);
      for(unsigned int iPt=0; iPt<m_nOfPt; iPt++){
        m_pts[iPt][iComp] = distribution(generator);
      }
    }
  }

  // 8 - random with normal distribution zero mean unit variance:
  inline void randn(unsigned int N, unsigned int d){
    std::default_random_engine generator;
    std::normal_distribution<double> distribution(0.0, 1.0);
    for(unsigned int iPt=0; iPt<m_nOfPt; iPt++){
      for(unsigned int iComp=0; iComp<m_dim; iComp++){
        m_pts[iPt][iComp] = distribution(generator);
      }
    }
  }

  // 8.a) random with normal, mean and variance assigned:
  inline void randn(unsigned int N, unsigned int d, vec mean, mat covariance){
    std::default_random_engine generator;
    eigenSolver eigDecCov(covariance);
    vector<vec> V = eigDecCov.eigenVecs();
    vector<double> lambda = eigDecCov.eigenVals();
    // distributions:
    vector<std::normal_distribution<double> > distribVec(m_dim);
    for(unsigned int iComp=0; iComp<m_dim; iComp++){
      std::normal_distribution<double> distrib(0.0, sqrt(lambda[iComp]));
      distribVec[iComp] = distrib;
    }
    // generate points:
    for(unsigned int iPt=0; iPt<m_nOfPt; iPt++){
      vec pt(m_dim, mean.comm());
      for(unsigned int iComp=0; iComp<m_dim; iComp++){
        double val = distribVec[iComp](generator);
        pt.setVecEl(iComp,val);
      }
      for(unsigned int iComp=0; iComp<m_dim; iComp++){
        m_pts[iPt][iComp] = 0.0;
        for(unsigned int jComp=0; jComp<m_dim; jComp++){
          m_pts[iPt][iComp] += V[jComp].getVecEl(iComp) * pt.getVecEl(jComp);
        }
      }
    }
    eigDecCov.clear();
  }

};

#endif
