// Implementation of the Node class:

#include "Node.h"

/* overloaded constructor:
  - input: a table of indices
  - output: the indices table is passed by reference
*/
Node::Node(vector< vector<unsigned int> >& vec_ind){
	m_nVar = vec_ind.size();
	m_indices = vec_ind;
	m_parent = NULL;
	m_children= vector<Node*>(0);
}

/* overloaded constructor:
  - input: a table of indices, number of variables;
  - output: the indices table is passed by reference
*/
Node::Node(vector< vector<unsigned int> >& vec_ind, const int& numvar){
	m_nVar = numvar;
	m_indices = vec_ind;
	m_parent = NULL;
	m_children = vector<Node*>(0);
}


/* function to count
  - input: the root, count:
  - output: count
*/
unsigned int Node::countNodesRec(Node* root, unsigned int& count){
    Node* child = NULL;
    for(unsigned int iChild = 0; iChild < root->childrenNumber(); iChild++){
        child = root->children(iChild);
        count++;
        if(child->childrenNumber() > 0){
            countNodesRec(child, count);
        }
    }
    return count;
}


/* function to append a Child:
  - input: child pointer
  - output: the child is appended
*/
void Node::appendChild(Node *child){
    child->setParent(this);
    m_children.push_back(child);
}


/* function to set parent
  - input: parent pointer
  - output: the parent is set
*/
void Node::setParent(Node *theParent){
    m_parent = theParent;
}


/* pop back the child
  - input: none
  - output: pop back
*/
void Node::popBackChild(){
    m_children.pop_back();
}


/* remove children
  - input: none
  - output: remove all the children
*/
void Node::removeChildren(){
    vector<vector<unsigned int> > vec_ind = getIndicesFromLeafs();
    m_indices = vec_ind;
    m_children= vector<Node*>(0);
}


/* remove a given child
  - input: the id of the child
  - output: the child is removed
*/
void Node::removeChild(unsigned int iChild){
    if(m_children.size() > 0) {
        m_children.erase(m_children.begin()+ iChild);
    }
    else {
        m_children.pop_back();
    }
}


/* check if a node has children
  - input: none
  - output: a bool
*/
bool Node::hasChildren(){
    if(m_children.size() > 0)
        return true;
    else
        return false;
}


/* check if a node has a parent
  - input: none
  - output: a bool
*/
bool Node::hasParent(){
    if(m_parent != NULL)
        return true;
    else
        return false;
}


/* return the children number:
  - input: none
  - output: return the children number
*/
unsigned int Node::childrenNumber(){
    return m_children.size();
}


/* return the grand-children number
  - input: none
  - output: the grandchildren number
*/
unsigned int Node::grandChildrenNum(){
    unsigned int t = 0;

    if(m_children.size() < 1){
        return 0;
    }
    countNodesRec(this, t);
    return t;
}


/* add leafs
  - input: a list of leafs
  - output: these are added to a tree
*/
void Node::addLeafs(vector<Node* >& leafsToAdd){
	if(m_children.size() == 0){
		leafsToAdd.push_back(this);
	}
	else{
		for (int jChild=0; jChild<m_children.size(); jChild++){
			Node* childj = m_children[jChild];
			if (!(childj->hasChildren())){
				leafsToAdd.push_back(childj);
			}
			else{
				childj->addLeafs(leafsToAdd);
			}
		}
	}
}


/* function that returns the leafs:
  - input: none
  - output: the leafs
*/
vector<Node*> Node::getLeafs(){
	vector<Node *> vec(0);
	addLeafs(vec);
	return vec;
}


/* function that creates a dyadic tree
  - input: the width
  - output: the tree is created
*/
void Node::createDyadicTree(const unsigned int& N){
	unsigned int count = 0;

	// Remove children if it has
	if (hasChildren()){
		removeChildren();
	}

	while (count < N){
		vector<Node* > leafs = getLeafs();

		for (int iLeaf =0; iLeaf<leafs.size(); iLeaf++){
			Node* leafi = leafs[iLeaf];
			vector< vector<unsigned int> > veci = leafi->indices();
			assert(veci.size()==m_nVar);

			vector<unsigned int> iterator(m_nVar,0);
			unsigned int it = 0;
			while (iterator[m_nVar-1] < 2){ // 1.5 in the original version

				vector< vector<unsigned int> > ind(m_nVar);
				for (unsigned int jVar=0; jVar<m_nVar; jVar++){
					unsigned int Nj = veci[jVar].size();

					if (iterator[jVar] == 0){

						unsigned int Mj = Nj/2;
						vector<unsigned int> subvecj = subvector(0, Mj, veci[jVar]);
						ind[jVar] = subvecj;
					}
					else{
						unsigned int Mj = Nj/2;
						vector<unsigned int> subvecj = subvector(Mj,(Nj-Mj),veci[jVar]);
						ind[jVar] = subvecj;
					}
				}
				Node* nodeIt = new Node(ind);
				leafi->appendChild(nodeIt);

				//On update iterator
				iterator[0] = iterator[0] + 1;
				unsigned int cour = 0;
				while( (iterator[cour] >= 2) && (cour < m_nVar-1)){ // >1.5 in the original
					iterator[cour] = 0;
					iterator[cour+1] = iterator[cour+1] +1;
					cour = cour+1;
				}
			}
			// free the memory
			leafi->eraseIndices();
		}
		count++;
	}
}


//Split a leaf into 2^d children
void Node::splitLeaf(Node* leafi){
	vector<Node* > leafs = getLeafs();
	int i = 0;
  isIn(leafi, leafs, i);
  /* index of leaf in the list*/
	//Node* leafi = leafs[i];

	vector< vector<unsigned int> > veci = leafi->indices();
	assert(veci.size()==m_nVar);

	vector<unsigned int> iterator(m_nVar,0);
	unsigned int it = 0;
	while (iterator[m_nVar-1] < 2){
		vector< vector<unsigned int> > ind(m_nVar);
		for (unsigned int jVar=0; jVar< m_nVar; jVar++){
			unsigned int Nj = veci[jVar].size();
			if (iterator[jVar] == 0){
				unsigned int Mj = Nj/2;
				vector<unsigned int> subvecj = subvector(0, Mj, veci[jVar]);
				ind[jVar] = subvecj;
			}
			else{
				int Mj = Nj/2;
				vector<unsigned int> subvecj = subvector(Mj,(Nj-Mj),veci[jVar]);
				ind[jVar] = subvecj;
			}

		}
		Node* nodeIt = new Node(ind);
		leafi->appendChild(nodeIt);

		//Update iterator
		iterator[0] = iterator[0] + 1;
		unsigned int cour = 0;
		while( (iterator[cour] >= 2) && (cour < m_nVar-1)){
			iterator[cour] = 0;
			iterator[cour+1] = iterator[cour+1] + 1;
			cour = cour+1;
		}
	}

	//free the memory
	leafi->eraseIndices();
}






/* function that extract a subvector
  - input: beginning, length, list of terms
  - output: a subvector
*/
vector<unsigned int> Node::subvector(const int& beg, const int& len, const vector<unsigned int>& list){
	/*vector<unsigned int> subvec(0);
	for (int iDof=beg; iDof<beg+len; iDof++){
		subvec.push_back(list[iDof]);
	}*/
  vector<unsigned int> subvec(len);
  for(unsigned int iDof=0; iDof<len; iDof++){
    unsigned int globInd = iDof + beg;
    subvec[iDof] = list[globInd];
  }
	return subvec;
}


/* get indices from leafs
  - input: none
  - output: list
*/
vector<vector<unsigned int> > Node::getIndicesFromLeafs(){
	vector<Node* > leafs = getLeafs();
	vector< vector<unsigned int> > ind(m_nVar);

	for (unsigned int jVar=0; jVar<m_nVar; jVar++){
		vector<unsigned int> vecj(0);

		for (unsigned int iLeaf =0; iLeaf<leafs.size(); iLeaf++){
			vector<unsigned int> oldvecj = vecj;
			vector<unsigned int> vecij = leafs[iLeaf]->indices(jVar);
			mergeSortInt(oldvecj, vecij, vecj);
		}
		sort(vecj.begin(), vecj.end());

		ind[jVar] = vecj;
	}
	return ind;
}


/* function that gets the parent of leafs
  - input leaf_list
  - output: a vector of node pointers
*/
vector<Node* > Node::getParentsOfLeafs(vector<unsigned int>& leaf_list){
	vector<Node* > leafs = getLeafs();
	vector<Node*> parents(0);
	leaf_list.resize(0);
	for (unsigned int iLeaf =0; iLeaf<leafs.size(); iLeaf++){
		if (leafs[iLeaf]->hasParent()){
			Node* parenti = leafs[iLeaf]->parent();
			if (parenti ==NULL){
				cout << "Error! There is no parent." << endl;
			}
			int k;
			isIn(parenti, parents, k);
			if (k<0){
				parents.push_back(parenti);
				leaf_list.push_back(iLeaf);
			}
		}
	}
	return parents;
}


/* create a copy of the current node
  - input: none
  - output: a copy of the current object
*/
Node* Node::createNodeCopy(){
	vector< vector<unsigned int> > ind = indices();
	unsigned int numvar = m_nVar;
	Node* newnode = new Node(ind, numvar);
	if (hasChildren()){
		for (unsigned int iChild=0; iChild<m_children.size(); iChild++){
			Node* newchildi = m_children[iChild]->createNodeCopy();
			newnode->appendChild(newchildi);
		}
	}
	return newnode;
}


/* check if an index is in
  - inputs:
  - output:
*/
void Node::isIn(const Node* node, const vector<Node*> vecnode,  int& k){
	k = -1;
	for (int j=0; j<vecnode.size(); j++){
		if (vecnode[j] == node){
			k=j;
		}
	}
}


/* visualise a node
  - input: a string with the filename
  - output: a file is written
*/
void Node::visualize(const string& path){
	vector<Node*> leafs = getLeafs();

	ofstream myfile;
	string name = path+"_leafs.txt";
	const char* name2 = name.c_str();
  myfile.open(name2);

	for (unsigned int iLeaf=0; iLeaf<leafs.size(); iLeaf++){
		Node* nodei = leafs[iLeaf];
		vector<vector<unsigned int> > veci = nodei->indices();
		myfile << iLeaf << " " ;
		for (int jVar=0; jVar<m_nVar; jVar++){
			vector<unsigned int> vecj = veci[jVar];
			myfile << vecj[0] << " " << vecj[vecj.size()-1] << " ";
		}
		myfile << endl;
	}
	myfile.close();
}

/* function that...

*/
void Node::whichLeaf(const vector<unsigned int>& indvec, Node*& leafind, vector<unsigned int>& new_vec){
	Node* current_node = this;
	new_vec = indvec;
	while(current_node->hasChildren()){
		int numchild = current_node->childrenNumber();
		bool IsFound = false;
		for (unsigned int iChild=0; iChild<numchild; iChild++){
			Node* nodei = current_node->children(iChild);
			vector< vector<unsigned int> > vecind = nodei->getIndicesFromLeafs();
			bool isInThisOne = true;
			for (unsigned int jVar=0; jVar<m_nVar; jVar++){
				isInThisOne  = (isInThisOne) && (indvec[jVar] >=vecind[jVar][0]) && (indvec[jVar] <=vecind[jVar][vecind[jVar].size()-1]);
			}
			if (isInThisOne){
				current_node = nodei;
				iChild = numchild;
				IsFound= true;
				for (int jVar=0; jVar<m_nVar; jVar++){
					new_vec[jVar] = indvec[jVar] - vecind[jVar][0];
				}
			}
		}
		if (IsFound==false) {cout << "There is a big problem!" << endl;}
	}
	leafind = current_node;
}
