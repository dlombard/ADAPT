// Header file of the class clustering to perform hierarchical clustering of
// unstructured point sets

#ifndef clustering_h
#define clustering_h

#include "Node.h"
#include "genericInclude.h"

class clustering{
private:
  vector<vector<double> >* m_points;
  unsigned int m_dim;
  unsigned int m_nOfPt;
  Node* m_node;
  bool m_use_lInfty;
  bool m_use_centroid;
  int m_which_linkage = -1; // 0 = single linkage, 1 = max (or complete) linkage, 2 = average linkage;

public:
  clustering(){};
  ~clustering(){};
  inline void clear(){
    delete m_node;
  }

  clustering(vector<vector<double> >&, string, string);

  // ACCESS FUNCTIONS:
  inline Node* node(){return m_node;}
  inline vector<vector<double> > points(){return *m_points;}
  inline vector<double> points(unsigned int iPt){return (*m_points)[iPt];}
  inline double points(unsigned int iPt, unsigned int iComp){return (*m_points)[iPt][iComp];}
  inline unsigned int dim(){return m_dim;}
  inline unsigned int nOfPt(){return m_nOfPt;}

  // SETTERS:
  inline void set_points(vector<vector<double> >& pts){m_points = &pts;}
  inline void set_use_linfty(bool& useIt){m_use_lInfty = useIt;}
  inline void set_use_centroid(bool& useIt){m_use_centroid = useIt;}
  inline void set_linkage(unsigned int linkType){m_which_linkage = linkType;}



  // METHODS:

  // inline compute l2 dist:
  inline double compute_l2_dist(vector<double> pt1, vector<double> pt2){
    assert(pt1.size() == pt2.size());
    double result = 0.0;
    for(unsigned int iComp=0; iComp<pt1.size(); iComp++){
      result += (pt1[iComp]-pt2[iComp]) * (pt1[iComp]-pt2[iComp]);
    }
    result = sqrt(result);
    return result;
  }

  // inline compute l-infty dist:
  inline double compute_linfty_dist(vector<double> pt1, vector<double> pt2){
    assert(pt1.size() == pt2.size());
    double result = 0.0;
    for(unsigned int iComp=0; iComp<pt1.size(); iComp++){
      double candidate = fabs(pt1[iComp] - pt2[iComp]);
      if(candidate > result){
        result = candidate;
      }
    }
    return result;
  }

  // inline compute a centroid of a set of points:
  inline vector<double> compute_centroid(vector<vector<double> >& pts){
    const unsigned int numPoints = pts.size();
    const unsigned int vecSize = pts[0].size();
    vector<double> centroid(vecSize);

    for(unsigned int iComp=0; iComp<vecSize; iComp++){
      centroid[iComp] = 0.0;
      for(unsigned int iPt=0; iPt<numPoints; iPt++){
        centroid[iComp] += pts[iPt][iComp];
      }
      centroid[iComp] = centroid[iComp]/(1.0*numPoints);
    }
    return centroid;
  }

  // compute single-linkage distance between two groups:
  inline double compute_single_linkage(vector<vector<double> >& ptSet1, vector<vector<double> >& ptSet2){
    assert(ptSet1[0].size() == ptSet2[0].size());
    const unsigned int numPoints_1 = ptSet1.size();
    const unsigned int numPoints_2 = ptSet2.size();
    const unsigned int vecSize = ptSet1[0].size();
    double result = DBL_MAX;
    for(unsigned int iPt=0; iPt<numPoints_1; iPt++){
      for(unsigned int jPt=0; jPt<numPoints_2; jPt++){
        double pairDist = m_use_lInfty ? compute_linfty_dist(ptSet1[iPt], ptSet2[jPt]) : compute_l2_dist(ptSet1[iPt], ptSet2[jPt]);
        if(pairDist<result){
          result = pairDist;
        }
      }
    }
    return result;
  }

  // compute max-linkage distance between two groups:
  inline double compute_max_linkage(vector<vector<double> >& ptSet1, vector<vector<double> >& ptSet2){
    assert(ptSet1[0].size() == ptSet2[0].size());
    const unsigned int numPoints_1 = ptSet1.size();
    const unsigned int numPoints_2 = ptSet2.size();
    const unsigned int vecSize = ptSet1[0].size();
    double result = 0.0;
    for(unsigned int iPt=0; iPt<numPoints_1; iPt++){
      for(unsigned int jPt=0; jPt<numPoints_2; jPt++){
        double pairDist = m_use_lInfty ? compute_linfty_dist(ptSet1[iPt], ptSet2[jPt]) : compute_l2_dist(ptSet1[iPt], ptSet2[jPt]);
        if(pairDist>result){
          result = pairDist;
        }
      }
    }
    return result;
  }

  // compute average-linkage distance between two groups:
  inline double compute_average_linkage(vector<vector<double> >& ptSet1, vector<vector<double> >& ptSet2){
    assert(ptSet1[0].size() == ptSet2[0].size());
    const unsigned int numPoints_1 = ptSet1.size();
    const unsigned int numPoints_2 = ptSet2.size();
    const unsigned int vecSize = ptSet1[0].size();
    double result = 0.0;
    for(unsigned int iPt=0; iPt<numPoints_1; iPt++){
      for(unsigned int jPt=0; jPt<numPoints_2; jPt++){
        double pairDist = m_use_lInfty ? compute_linfty_dist(ptSet1[iPt], ptSet2[jPt]) : compute_l2_dist(ptSet1[iPt], ptSet2[jPt]);
        result += pairDist;
      }
    }
    result = result / (1.0 *numPoints_1*numPoints_2);
    return result;
  }


  // compute vector of index table at a certain depth
  vector< vector<vector<unsigned int> > > indTableAtDepth(unsigned int);

  // run over the tree and compute the indices:
  void runOverTree(Node*&, vector<vector<vector<unsigned int> > >&, unsigned int);


  // compute clustering (init by an empty constructor)
  void compute(vector<vector<double> >&);

};

#endif
