/* Header file for: Class describing the nearest neighbor
 - deal with unstructure sets of points.
*/

#ifndef NEAREST_H
#define NEAREST_H


#include "genericInclude.h"
#include <flann/flann.hpp>

using namespace std;

class nearest {
private:
  flann::Matrix<float>* m_dataset;
  flann::Matrix<float>* m_query;
  vector<vector<int>* > m_indices;
  vector<vector<double>* > m_dists;
  int m_nOfNeigh;
  int m_nOfTrees;
  int m_nOfCheck;

  flann::Index<flann::L2<float> >* m_index;

public:
  nearest(){};
  ~nearest(){
    delete[] m_dataset->ptr();
    delete[] m_query->ptr();
    delete m_dataset;
    delete m_query;
    delete m_index;
    for(size_t h = 0; h<m_indices.size(); h++){
      delete m_indices[h];
      delete m_dists[h];
    }
  };

  void initialize(vector<vector<double>* >&, int, int, int);
  void computeKNN(vector<vector<double>* >&);
  void computeHierarchical(vector<vector<double>* >&);
  vector<vector<double>* > neighCoords(int);
  void neighCoords(int, vector<vector<double>* >& );

  // AUXILIARY CONVERSIONS:
  flann::Matrix<float>  fromVecDoubleToFlann(vector<vector<double>* >&);
  flann::Matrix<int>   fromVecIntToFlann(vector<vector<int>* >&);
  vector<vector<double>* >  fromFlannFloatToVec(flann::Matrix<float>& );
  vector<vector<int>* >   fromFlannIntToVec(flann::Matrix<int>& );

  // ACCESS FUNCTIONS:
  inline flann::Matrix<float> dataset(){return *m_dataset; }
  inline flann::Matrix<float> query(){return *m_query; }

  inline vector<vector<int>* > indices(){return m_indices; }
  inline vector<int> indices(int p){return *(m_indices[p]);}
  inline vector<vector<double>* > dists(){return m_dists; }
  inline vector<double> dists(int p){return *(m_dists[p]); }

  inline int nOfNeigh(){return m_nOfNeigh; };
  inline int nOfTrees(){return m_nOfTrees; };
  inline int nOfCheck(){return m_nOfCheck; };
  inline flann::Index<flann::L2<float> > index(){return *m_index; };
};

#endif
