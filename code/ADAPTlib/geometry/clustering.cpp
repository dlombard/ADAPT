// Implementation of the clustering class:

#include "clustering.h"



/* overloaded constructor
  - input: the set of points, distToUse={l2, linfty}, method = {centroid, single}
  - output: permforms agglomerative clustering on the set of points
*/
clustering::clustering(vector<vector<double> >& data, string distToUse, string method){
  m_use_lInfty = !(distToUse == "l2");
  m_use_centroid = (method == "centroid");
  if(m_which_linkage==-1){
    m_which_linkage = 1; // set default = 1;
  }

  m_nOfPt = data.size();
  m_dim = data[0].size();
  m_points = &data;

  m_node = new Node();

  // starting point: the groups are the leafs, every point is a leaf.
  //vector<Node*> leafs(m_nOfPt);
  vector<Node*> active(m_nOfPt);
  for(unsigned int iPt=0; iPt<m_nOfPt; iPt++){
    vector<vector<unsigned int> > indLeaf(1);
    indLeaf[0].resize(1);
    indLeaf[0][0] = iPt;
    Node* thisLeaf;
    thisLeaf = new Node(indLeaf);
    //leafs[iPt] = thisLeaf;
    active[iPt] = thisLeaf;
  }

  // number of active leafs:
  unsigned int nOfActive = m_nOfPt;


  // computing pairwise distances:
  vector<vector<double> >* distTable;
  distTable = new vector<vector<double> >;
  distTable->resize(nOfActive);

  for(unsigned int iGroup=0; iGroup<nOfActive; iGroup++){
    Node* active_i = active[iGroup];
    vector<vector<unsigned int> > id_i = active_i->indices();
    unsigned int n_i = id_i[0].size();
    vector<vector<double> > set_i(n_i);
    for(unsigned int iEl=0; iEl<n_i; iEl++){
      unsigned int idEl = id_i[0][iEl];
      set_i[iEl] = data[idEl];
    }

    (*distTable)[iGroup].resize(iGroup);

    for(unsigned int jGroup=0; jGroup<iGroup; jGroup++){
      Node* active_j = active[jGroup];
      vector<vector<unsigned int> > id_j = active_j->indices();
      unsigned int n_j = id_j[0].size();
      vector<vector<double> > set_j(n_j);
      for(unsigned int iEl=0; iEl<n_j; iEl++){
        unsigned int idEl = id_j[0][iEl];
        set_j[iEl] = data[idEl];
      }
      // compute distance:
      double pair_dist = 0.0;
      if(m_use_centroid){
        vector<double> cent_i = compute_centroid(set_i);
        vector<double> cent_j = compute_centroid(set_j);
        if(m_use_lInfty){
          pair_dist = compute_linfty_dist(cent_i, cent_j);
        }else{
          pair_dist = compute_l2_dist(cent_i, cent_j);
        }
      }
      else{
        switch (m_which_linkage) {
          case 0:{pair_dist = compute_single_linkage(set_i, set_j);}
          case 1:{pair_dist = compute_max_linkage(set_i, set_j);}
          case 2:{pair_dist = compute_average_linkage(set_i, set_j);}
          default:
            break;
        }

      }

      // filling the Table:
      (*distTable)[iGroup][jGroup] = pair_dist;
    }
  }



  while(nOfActive > 1){

    // test the pairwise distances and select the minimum:
    double minDist = DBL_MAX;
    vector<unsigned int> best_pair = {0,0};
    for(unsigned int iGroup=0; iGroup<nOfActive; iGroup++){

        for(unsigned int jGroup=0; jGroup<iGroup; jGroup++){
          double pair_dist = (*distTable)[iGroup][jGroup];

          // update the best:
          if(pair_dist<minDist){
            minDist = pair_dist;
            best_pair[0] = iGroup;
            best_pair[1] = jGroup;
          }
       }
    }

    cout << "A = " << best_pair[0] << " and " <<  best_pair[1] << endl;


    // set parent node:
    unsigned int n_1 = active[best_pair[0]]->indices(0).size();
    unsigned int n_2 = active[best_pair[1]]->indices(0).size();
    unsigned int n_parent = n_1 + n_2;
    vector<vector<unsigned int> > ind_parent(1);
    ind_parent[0].resize(n_parent);
    unsigned int cc = 0;
    for(unsigned int i_1 = 0; i_1<n_1; i_1++){
      ind_parent[0][cc] = active[best_pair[0]]->indices(0,i_1);
      cc += 1;
    }
    for(unsigned int i_2 = 0; i_2<n_2; i_2++){
      ind_parent[0][cc] = active[best_pair[1]]->indices(0,i_2);
      cc += 1;
    }
    Node* parent;
    parent = new Node(ind_parent);
    parent->appendChild(active[best_pair[0]]);
    parent->appendChild(active[best_pair[1]]);

    // erase from active the children, add the parent: best_pair[1] < best_pair[0] by construction
    vector<Node*>::iterator it_2 = active.begin() + best_pair[1];
    active.erase(it_2);
    vector<Node*>::iterator it_1 = active.begin() + best_pair[0] - 1;
    active.erase(it_1);

    active.push_back(parent);
    nOfActive = nOfActive - 1;

    // update the distance table:

    // erase inside each entry first:
    for(unsigned int iGroup=best_pair[1]+1; iGroup<nOfActive+1; iGroup++){
      if(iGroup<=best_pair[0]){
        vector<double>::iterator inner_2 = (*distTable)[iGroup].begin() + best_pair[1];
        (*distTable)[iGroup].erase(inner_2);
      }
      else{
        vector<double>::iterator inner_2 = (*distTable)[iGroup].begin() + best_pair[1];
        (*distTable)[iGroup].erase(inner_2);
        vector<double>::iterator inner_1 = (*distTable)[iGroup].begin() + best_pair[0]-1;
        (*distTable)[iGroup].erase(inner_1);
      }
    }

    // update the global table by adding the distances w.r.t. parent:
    vector<vector<double> >::iterator it_d_2 = distTable->begin() + best_pair[1];
    distTable->erase(it_d_2);
    vector<vector<double> >::iterator it_d_1 = distTable->begin() + best_pair[0]-1;
    distTable->erase(it_d_1);

    vector<double> distFromLast(nOfActive-1);
    Node* active_i = active[nOfActive-1];
    vector<vector<unsigned int> > id_i = active_i->indices();
    unsigned int n_i = id_i[0].size();
    vector<vector<double> > set_i(n_i);
    for(unsigned int iEl=0; iEl<n_i; iEl++){
      unsigned int idEl = id_i[0][iEl];
      set_i[iEl] = data[idEl];
    }

    for(unsigned int jGroup=0; jGroup<nOfActive-1; jGroup++){
      Node* active_j = active[jGroup];
      vector<vector<unsigned int> > id_j = active_j->indices();
      unsigned int n_j = id_j[0].size();
      vector<vector<double> > set_j(n_j);
      for(unsigned int iEl=0; iEl<n_j; iEl++){
        unsigned int idEl = id_j[0][iEl];
        set_j[iEl] = data[idEl];
      }
      // compute distance:
      double pair_dist = 0.0;
      if(m_use_centroid){
        vector<double> cent_i = compute_centroid(set_i);
        vector<double> cent_j = compute_centroid(set_j);
        if(m_use_lInfty){
          pair_dist = compute_linfty_dist(cent_i, cent_j);
        }else{
          pair_dist = compute_l2_dist(cent_i, cent_j);
        }
      }
      else{
        switch (m_which_linkage) {
          case 0:{pair_dist = compute_single_linkage(set_i, set_j);}
          case 1:{pair_dist = compute_max_linkage(set_i, set_j);}
          case 2:{pair_dist = compute_average_linkage(set_i, set_j);}
          default:
            break;
        }
      }
      // filling the Table:
      distFromLast[jGroup] =  pair_dist;
    }
    distTable->push_back(distFromLast);
    // end of while loop;
  }


  // assigning the node to the root:
  m_node = active[0];

  // clean the memory:
  distTable->clear();
  delete distTable;
}




/* Compute index table at a given depth:
  - input: the depth
  - output: the vector of index table for each sub-domain
*/
vector<vector<vector<unsigned int> > > clustering::indTableAtDepth(unsigned int depth){
  // check that the depth is not too large:
  vector<vector<unsigned int> > indTab_0 = m_node->indices();
  unsigned int N_0 = indTab_0[0].size();
  if( pow(2.0,depth) > N_0){
    PetscPrintf(PETSC_COMM_WORLD, "Depth is too large.\n");
    exit(1);
  }

  vector<vector<vector<unsigned int> > > toBeReturned;
  runOverTree(m_node, toBeReturned, depth);

  return toBeReturned;
}

// auxiliary recursive method:
void clustering::runOverTree(Node*& treeNode, vector<vector<vector<unsigned int> > >& tableToFill, unsigned int depth){
  vector<Node*> childVec = treeNode->children();
  if(depth==1){
    for(unsigned int iChild=0; iChild<childVec.size(); iChild++){
      vector<vector<unsigned int> > indTab_child = childVec[iChild]->indices();
      tableToFill.push_back(indTab_child);
    }
  }
  else{
    for(unsigned int iChild=0; iChild<childVec.size(); iChild++){
      runOverTree(childVec[iChild], tableToFill, depth-1);
    }
  }
}




/* overloaded constructor
  - input: the set of points, distToUse={l2, linfty}, method = {centroid, single}
  - output: permforms agglomerative clustering on the set of points
*/
void clustering::compute(vector<vector<double> >& data){

  // if the kind of linkage has not been set, put it to maximum linkage:
  if(m_which_linkage==-1){
    m_which_linkage = 1; // set default = 1;
  }

  m_nOfPt = data.size();
  m_dim = data[0].size();
  m_points = &data;

  m_node = new Node();

  // starting point: the groups are the leafs, every point is a leaf.
  //vector<Node*> leafs(m_nOfPt);
  vector<Node*> active(m_nOfPt);
  for(unsigned int iPt=0; iPt<m_nOfPt; iPt++){
    vector<vector<unsigned int> > indLeaf(1);
    indLeaf[0].resize(1);
    indLeaf[0][0] = iPt;
    Node* thisLeaf;
    thisLeaf = new Node(indLeaf);
    //leafs[iPt] = thisLeaf;
    active[iPt] = thisLeaf;
  }

  // number of active leafs:
  unsigned int nOfActive = m_nOfPt;


  // computing pairwise distances:
  vector<vector<double> >* distTable;
  distTable = new vector<vector<double> >;
  distTable->resize(nOfActive);

  for(unsigned int iGroup=0; iGroup<nOfActive; iGroup++){
    Node* active_i = active[iGroup];
    vector<vector<unsigned int> > id_i = active_i->indices();
    unsigned int n_i = id_i[0].size();
    vector<vector<double> > set_i(n_i);
    for(unsigned int iEl=0; iEl<n_i; iEl++){
      unsigned int idEl = id_i[0][iEl];
      set_i[iEl] = data[idEl];
    }

    (*distTable)[iGroup].resize(iGroup);

    for(unsigned int jGroup=0; jGroup<iGroup; jGroup++){
      Node* active_j = active[jGroup];
      vector<vector<unsigned int> > id_j = active_j->indices();
      unsigned int n_j = id_j[0].size();
      vector<vector<double> > set_j(n_j);
      for(unsigned int iEl=0; iEl<n_j; iEl++){
        unsigned int idEl = id_j[0][iEl];
        set_j[iEl] = data[idEl];
      }
      // compute distance:
      double pair_dist = 0.0;
      if(m_use_centroid){
        vector<double> cent_i = compute_centroid(set_i);
        vector<double> cent_j = compute_centroid(set_j);
        if(m_use_lInfty){
          pair_dist = compute_linfty_dist(cent_i, cent_j);
        }else{
          pair_dist = compute_l2_dist(cent_i, cent_j);
        }
      }
      else{
        switch (m_which_linkage) {
          case 0:{pair_dist = compute_single_linkage(set_i, set_j);}
          case 1:{pair_dist = compute_max_linkage(set_i, set_j);}
          case 2:{pair_dist = compute_average_linkage(set_i, set_j);}
          default:
            break;
        }

      }

      // filling the Table:
      (*distTable)[iGroup][jGroup] = pair_dist;
    }
  }



  while(nOfActive > 1){

    // test the pairwise distances and select the minimum:
    double minDist = DBL_MAX;
    vector<unsigned int> best_pair = {0,0};
    for(unsigned int iGroup=0; iGroup<nOfActive; iGroup++){

        for(unsigned int jGroup=0; jGroup<iGroup; jGroup++){
          double pair_dist = (*distTable)[iGroup][jGroup];

          // update the best:
          if(pair_dist<minDist){
            minDist = pair_dist;
            best_pair[0] = iGroup;
            best_pair[1] = jGroup;
          }
       }
    }

    cout << "A = " << best_pair[0] << " and " <<  best_pair[1] << endl;


    // set parent node:
    unsigned int n_1 = active[best_pair[0]]->indices(0).size();
    unsigned int n_2 = active[best_pair[1]]->indices(0).size();
    unsigned int n_parent = n_1 + n_2;
    vector<vector<unsigned int> > ind_parent(1);
    ind_parent[0].resize(n_parent);
    unsigned int cc = 0;
    for(unsigned int i_1 = 0; i_1<n_1; i_1++){
      ind_parent[0][cc] = active[best_pair[0]]->indices(0,i_1);
      cc += 1;
    }
    for(unsigned int i_2 = 0; i_2<n_2; i_2++){
      ind_parent[0][cc] = active[best_pair[1]]->indices(0,i_2);
      cc += 1;
    }
    Node* parent;
    parent = new Node(ind_parent);
    parent->appendChild(active[best_pair[0]]);
    parent->appendChild(active[best_pair[1]]);

    // erase from active the children, add the parent: best_pair[1] < best_pair[0] by construction
    vector<Node*>::iterator it_2 = active.begin() + best_pair[1];
    active.erase(it_2);
    vector<Node*>::iterator it_1 = active.begin() + best_pair[0] - 1;
    active.erase(it_1);

    active.push_back(parent);
    nOfActive = nOfActive - 1;

    // update the distance table:

    // erase inside each entry first:
    for(unsigned int iGroup=best_pair[1]+1; iGroup<nOfActive+1; iGroup++){
      if(iGroup<=best_pair[0]){
        vector<double>::iterator inner_2 = (*distTable)[iGroup].begin() + best_pair[1];
        (*distTable)[iGroup].erase(inner_2);
      }
      else{
        vector<double>::iterator inner_2 = (*distTable)[iGroup].begin() + best_pair[1];
        (*distTable)[iGroup].erase(inner_2);
        vector<double>::iterator inner_1 = (*distTable)[iGroup].begin() + best_pair[0]-1;
        (*distTable)[iGroup].erase(inner_1);
      }
    }

    // update the global table by adding the distances w.r.t. parent:
    vector<vector<double> >::iterator it_d_2 = distTable->begin() + best_pair[1];
    distTable->erase(it_d_2);
    vector<vector<double> >::iterator it_d_1 = distTable->begin() + best_pair[0]-1;
    distTable->erase(it_d_1);

    vector<double> distFromLast(nOfActive-1);
    Node* active_i = active[nOfActive-1];
    vector<vector<unsigned int> > id_i = active_i->indices();
    unsigned int n_i = id_i[0].size();
    vector<vector<double> > set_i(n_i);
    for(unsigned int iEl=0; iEl<n_i; iEl++){
      unsigned int idEl = id_i[0][iEl];
      set_i[iEl] = data[idEl];
    }

    for(unsigned int jGroup=0; jGroup<nOfActive-1; jGroup++){
      Node* active_j = active[jGroup];
      vector<vector<unsigned int> > id_j = active_j->indices();
      unsigned int n_j = id_j[0].size();
      vector<vector<double> > set_j(n_j);
      for(unsigned int iEl=0; iEl<n_j; iEl++){
        unsigned int idEl = id_j[0][iEl];
        set_j[iEl] = data[idEl];
      }
      // compute distance:
      double pair_dist = 0.0;
      if(m_use_centroid){
        vector<double> cent_i = compute_centroid(set_i);
        vector<double> cent_j = compute_centroid(set_j);
        if(m_use_lInfty){
          pair_dist = compute_linfty_dist(cent_i, cent_j);
        }else{
          pair_dist = compute_l2_dist(cent_i, cent_j);
        }
      }
      else{
        switch (m_which_linkage) {
          case 0:{pair_dist = compute_single_linkage(set_i, set_j);}
          case 1:{pair_dist = compute_max_linkage(set_i, set_j);}
          case 2:{pair_dist = compute_average_linkage(set_i, set_j);}
          default:
            break;
        }
      }
      // filling the Table:
      distFromLast[jGroup] =  pair_dist;
    }
    distTable->push_back(distFromLast);
    // end of while loop;
  }


  // assigning the node to the root:
  m_node = active[0];

  // clean the memory:
  distTable->clear();
  delete distTable;
}
