// header file to read unstructured meshes from different formats

#ifndef mesh_h
#define mesh_h

#include "genericInclude.h"
#include "pointCloud.h"

/*  LIST of the FIELDS:
  - m_mesh_pts => the coordinates x y z of the dofs.
  - m_elemTable[iEl] => list of the dofs in the iEl-th element
  - m_elemType[iEl] => the type of the iEl-th element (point, line, tria, tetra...)
  - m_dof_belongsTo[iDof] => the elements the iDof-th dof belongs to
*/

/* NOTES ON GMSH TYPES:
  - 1 : 2-nodes line
  - 2 : 3-nodes triangle (P1 2d)
  - 3 : 4-nodes quadrangle (Q1 2d)
  - 4 : 4-nodes tetra (P1 3d)
  - ...
  - 9 : 6-nodes triangle (P2 2d)
  - 11: 10-nodes tetra (P2 3d)
  - 15: 1 point 
*/


class mesh{
private:
  pointCloud m_mesh_pts;
  vector< vector<unsigned int> >* m_elemTable;
  vector<unsigned int>* m_elemType;
  vector< vector<unsigned int> >* m_dof_belongsTo;
  map<string, int> m_type;
  string m_format;

public:
  mesh(){};
  ~mesh(){};
  mesh(string fileName, string meshFormat){
    // initialising pointers:
    m_elemTable = new vector<vector<unsigned int> >;
    m_elemType = new vector<unsigned int>;
    m_dof_belongsTo = new vector< vector<unsigned int> >;
    // reading the format:
    m_format = meshFormat;
    int formatId = useMapType(meshFormat);
    switch(formatId){
      case 0:{
        load_gmsh(fileName);
      }
      case 1:{
        load_freefem(fileName);
      }
      case 2:{
        load_felisce(fileName);
      }
      default:
        break;
    }
    // filling inverse map:
    fill_dofBelongsTo_table();
  };

  // clear the mesh
  inline void clear(){
    m_mesh_pts.clear();
    m_elemTable->clear();
    m_dof_belongsTo->clear();
    m_elemType->clear();
    delete m_elemTable;
    delete m_elemType;
    delete m_dof_belongsTo;
  }


  // ACCESS FUNCTIONS:
  inline pointCloud mesh_pts(){return m_mesh_pts;}
  inline vector<vector<double> > pts(){return m_mesh_pts.pts();}
  inline vector<double> pts(unsigned int iPt){return m_mesh_pts.pts(iPt);}
  inline double pts(unsigned int iPt, unsigned int iComp){return m_mesh_pts.pts(iPt, iComp);}
  inline string format(){return m_format;}
  inline vector<vector<unsigned int> > elemTable(){return *m_elemTable;}
  inline vector<unsigned int> elemTable( unsigned int iEl){return (*m_elemTable)[iEl];}
  inline unsigned int elemTable(unsigned int iEl, unsigned int iDof){return (*m_elemTable)[iEl][iDof];}
  inline vector<vector<unsigned int> > dof_belongsTo(){return *m_dof_belongsTo;}
  inline vector<unsigned int> dof_belongsTo(unsigned int iDof){return (*m_dof_belongsTo)[iDof];}
  inline unsigned int dof_belongsTo(unsigned int iDof, unsigned int iEl){assert(iEl<(*m_dof_belongsTo)[iDof].size()); return (*m_dof_belongsTo)[iDof][iEl];}
  inline vector<unsigned int> elemType(){return *m_elemType;}
  inline unsigned int elemType(unsigned int iEl){return (*m_elemType)[iEl];}

  // METHODS:

  // map to determine the format:
  inline int useMapType(string pType){
      m_type["gmsh"] = 0;
      m_type["freefem"] = 1;
      m_type["felisce"] = 2;
      return m_type.find(pType)->second;
  }


  /* fill the table m_dof_belongsTo:
  - dof are indexed from 0.
  - elements are indexed with their id, indexed from 0
  - iTh entry: m_dof_belongsTo[i] = list of elements the i-th dof belongs to.
  */
  inline void fill_dofBelongsTo_table(){
    const unsigned int nOfDof = m_mesh_pts.nOfPt();
    const unsigned int nOfEl = m_elemTable->size();
    m_dof_belongsTo->resize(nOfDof);
    for(unsigned int iEl=0; iEl < nOfEl; iEl++){
      vector<unsigned int> dofList = (*m_elemTable)[iEl];
      unsigned int dofInThisEl = dofList.size();
      for(unsigned int iDof=0; iDof<dofInThisEl; iDof++){
        unsigned int id = dofList[iDof];
        (*m_dof_belongsTo)[id].push_back(iEl);
      }
    }
  }


  /* provide a list of elements by type:
    - input: the type,
    - output: the id of the elements of that type
  */
  inline vector<unsigned int> list_ofType(unsigned int iType){
    vector<unsigned int> list;
    const unsigned int nOfEl = m_elemType->size();
    for(unsigned int iEl=0; iEl<nOfEl; iEl++){
      if( (*m_elemType)[iEl] == iType ){
        list.push_back(iEl);
      }
    }
    return list;
  }



  /*
    LOADING METHODS, different formats
  */


  // load from gmsh and fill the data:
  inline void load_gmsh(string fileName){
    ifstream meshFile;
    meshFile.open(fileName.c_str());
    if (!meshFile){
      cerr << "\n";
      cerr << "Wrong file!\n";
      exit (1);
    }

    // start parsing the mesh file:
    bool continueReading = true;
    bool flag_nPts = false;
    bool flag_nEl = false;
    bool start_reading_pts = false;
    bool start_reading_ele = false;
    unsigned int counter = 0;
    while(continueReading){
      if(meshFile.eof()){
        continueReading = false;
        break;
      }
      else{
        string text;
        getline(meshFile, text);
        //cout << text << endl;

        if(text == "$EndElements"){
          start_reading_ele = false;
        }

        if(start_reading_ele){
          stringstream elStream(text);
          vector<unsigned int> tmp;
          unsigned int value;
          while(elStream >> value) {
            tmp.push_back(value);
          }
          unsigned int iEl = tmp[0] - 1; // we are starting from 0;
          unsigned int iType = tmp[1];
          // tmp[2] = n of tag; tmp[3] = phyEnt; tmp[4] = elemEntity;
          vector<unsigned int> listOfVertices(tmp.size()-5);
          for(unsigned int iDof=0; iDof<tmp.size()-5; iDof++){
            listOfVertices[iDof] = tmp[iDof+5] -1; // also verts start from 0;
          }
          (*m_elemTable)[iEl] = listOfVertices;
          (*m_elemType)[iEl] = iType;
        }

        if(flag_nEl){
          stringstream convert(text);
          unsigned int nEl;
          convert >> nEl;

          m_elemTable->resize(nEl);
          m_elemType->resize(nEl);

          flag_nEl = false;
          start_reading_ele = true;
        }

        if(text=="$Elements"){
          flag_nEl = true;
        }

        // points section:
        if(text=="$EndNodes"){
          start_reading_pts = false;
        }

        if(start_reading_pts){
          stringstream ptStream(text);
          vector<double> tmp(4);
          double value;
          int cc = 0;
          while(ptStream >> value) {
            tmp[cc] = value;
            cc += 1;
          }
          vector<double> ptCoord(3);
          for(unsigned int iComp=0; iComp<3; iComp++){
            ptCoord[iComp] = tmp[iComp+1];
          }
          unsigned int iPt = static_cast<int>(tmp[0]) - 1; // westart from 0;
          m_mesh_pts.set_point(iPt, ptCoord); // often iPt = counter;
          counter += 1;
        }

        // read n points, init the pointCloud:
        if(flag_nPts){
          stringstream convert(text);
          unsigned int nPts;
          convert >> nPts;
          m_mesh_pts.init(nPts, 3);
          //cout << "N = " << m_mesh_pts.nOfPt() << " dim = " << m_mesh_pts.dim() << "\n";
          flag_nPts = false;
          start_reading_pts = true;
        }

        // find the beginning of nodes:
        if(text == "$Nodes"){
          flag_nPts = true;
        }

      }
    }
    meshFile.close();
  }

  // load file from freefem generated mesh:
  inline void load_freefem(string fileName){

  }

  // load mesh from felisce:
  inline void load_felisce(string fileName){

  }


  // print mesh:
  inline void print(){
    cout << "Mesh type: " << m_format << endl;
    cout << "N of dofs: " << m_mesh_pts.nOfPt() << endl;
    cout << "N of elements: " << m_elemType->size() << endl;
  }

};


#endif
