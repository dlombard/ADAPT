// Auxiliary class Node to deal with trees:

#ifndef Node_h
#define Node_h

#include "../genericInclude.h"
#include "../linAlg/vec.h"
#include "../linAlg/mat.h"




using namespace std;


class Node{
    private:
	     unsigned int m_nVar;
       vector< vector<unsigned int> > m_indices;
       Node* m_parent;
       std::vector<Node*> m_children;
       unsigned int countNodesRec(Node*, unsigned int&);
       unsigned int m_nOfLeafs;

    public:
        Node(){};
	      ~Node(){};
        Node(vector< vector<unsigned int> >&);
	      Node(vector< vector<unsigned int> >&, const int&);
        inline void eraseIndices(){vector< vector<unsigned int> > vec(0); m_indices = vec;}

        // -- ACCES FUNCTIONS --
        inline unsigned int nVar(){return m_nVar;}
        inline vector<vector<unsigned int> > indices(){return m_indices;}
        inline vector<unsigned int> indices(unsigned int iVar){return m_indices[iVar];}
        inline unsigned int indices(unsigned int iVar, unsigned int jInd){return m_indices[iVar][jInd];}
        inline Node* parent(){return m_parent;}
        inline vector<Node*> children(){return m_children;}
        inline Node* children(unsigned int iChild){return m_children[iChild];}
        inline unsigned int nOfLeafs(){
          vector<Node*> leafs = getLeafs();
          m_nOfLeafs = leafs.size();
          return m_nOfLeafs;
        }

        // get sizes and auxiliary quantities:
        inline unsigned int getDim(unsigned int j){ return m_indices[j].size();}

        // -- METHODS for the class Node: --
        void appendChild(Node* child);
        void setParent(Node* parent);

        void popBackChild();
        void removeChild(unsigned int);
	      void removeChildren();

        bool hasChildren();
        bool hasParent();

        unsigned int childrenNumber();
        unsigned int grandChildrenNum();
        vector< vector<unsigned int> > getIndicesFromLeafs();
	      void addLeafs(vector< Node*>&);
	      vector< Node*> getLeafs();
        vector< Node*> getParentsOfLeafs(vector<unsigned int>&);
        void addLeafsandParentsofLeafs(vector< Node*>&, vector<Node*>&, vector< vector<unsigned int>>&);
	      void getLeafsandParentsofLeafs(vector<Node*>&, vector<Node*>&, vector< vector<unsigned int>>&);
	      void createDyadicTree(const unsigned int&);
        vector<unsigned int> subvector(const int&, const int&, const vector<unsigned int>&);
	      Node* createNodeCopy();
        void splitLeaf(Node*);
	      void isIn(const Node*, const vector<Node*>, int&);
        void visualize(const string&);
        void whichLeaf(const vector<unsigned int>&, Node*&, vector<unsigned int>&);


        // -- AUXILIARY METHODS --
        inline unsigned int findIdLeaf(Node* theLeaf){
          unsigned int idToReturn = 0;
          vector<Node*> leafs = getLeafs();
          for(unsigned int iLeaf = 0; iLeaf<leafs.size(); iLeaf++){
            if(leafs[iLeaf] == theLeaf){
              idToReturn = iLeaf;
              break;
            }
          }
          return idToReturn;
        }



        inline void mergeSortInt(vector<unsigned int>& a, vector<unsigned int>& b, vector<unsigned int>& c){
        	c.resize(0);
        	sort( a.begin(), a.end());
        	sort( b.begin(), b.end());

        	int i = 0, j = 0;
        	while( i < a.size() && j < b.size()){
        		if( a[ i ] == b[j] ){
           			c.push_back( a[i] );
           			i++;
        			j++;
        		}
        		else if( a[ i ] < b[j] ){
           			c.push_back( a[i] );
        			i++;
        		}
        		else{
           			c.push_back( b[j] );
        			j++;
        		}
        	}
        	while( i < a.size()){
        		c.push_back( a[i] );
        		i++;
        	}
        	while( j < b.size()){
        		c.push_back( b[j] );
        		j++;
        	}
        }

};

#endif
