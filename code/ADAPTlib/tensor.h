// Header of the mother class tensor:
#ifndef tensor_h
#define tensor_h

#include "genericInclude.h"


using namespace std;

class tensor{
protected:
MPI_Comm m_comm;
unsigned int m_nVar; // Number of variables = the tensor dimension * num_var_
vector<unsigned int> m_nDof_var; //Dimension of the discretised spaces in each variable * dim_var_
vector<unsigned int> m_inc; // auxiliary vector for linear indexing

public:
  tensor(){};
  ~tensor(){};

  // overloaded constructors:
  tensor(unsigned int dim, vector<unsigned int> dofPerDim, MPI_Comm theComm){
    assert(dofPerDim.size() == dim);
    m_nVar = dim;
    m_nDof_var.resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_nDof_var[iVar] = dofPerDim[iVar];
    }
    m_comm = theComm;
    compute_minc();
  };

  // overloaded constructor: dimension is inferred from dofPerDim:
  tensor(vector<unsigned int> dofPerDim, MPI_Comm theComm){
    m_nVar = dofPerDim.size();
    m_nDof_var.resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_nDof_var[iVar] = dofPerDim[iVar];
    }
    m_comm = theComm;
    compute_minc();
  }


  // ACCESS FUNCTIONS:
  inline MPI_Comm comm(){return m_comm;}
  inline unsigned int nVar(){return m_nVar;}
  inline vector<unsigned int> nDof_var(){return m_nDof_var;}
  inline unsigned int nDof_var(unsigned int j){return m_nDof_var[j];}
  inline vector<unsigned int> inc(){return m_inc;}
  inline unsigned int inc(unsigned int j){return m_inc[j];}


  // SETTERS:
  inline void set_comm(MPI_Comm theComm){m_comm = theComm;}
  inline void set_nVar(unsigned int dim){m_nVar = dim;}
  inline void set_nDof_var(vector<unsigned int> dofPerDim){
    assert(dofPerDim.size() == m_nVar);
    m_nDof_var.resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_nDof_var[iVar] = dofPerDim[iVar];
    }
  };

  inline void change_nDof_var(unsigned int iVar, unsigned int nDof){
    assert(iVar<m_nVar);
    m_nDof_var[iVar] = nDof;
    compute_minc();
  }


  // Auxiliary methods for linear indexing:
  inline void compute_minc(){
    m_inc.resize(m_nVar);
    for(unsigned int idVar = 0; idVar < m_nVar; idVar++){
      m_inc[idVar] = 1;
      for (unsigned int n=0; n<idVar; n++){
        m_inc[idVar] = m_inc[idVar] * m_nDof_var[n];
      }
    }
  }
  // given a vector of integer containing the global indices i,j,k.., return the linear index
	inline unsigned int sub2lin(vector<unsigned int> indices){
			unsigned int linInd = 0;
			for (unsigned int h=0; h<m_nVar; h++) {
					linInd = linInd + m_inc[h] * indices[h];
			};
			return linInd;
	}
	// given the linear index return a vector containing the indices i,j,k,...
	inline vector<unsigned int> lin2sub(unsigned int linInd){
			vector<unsigned int> indices;
			indices.resize(m_nVar);
			unsigned int reminder = linInd;
			for (int h=m_nVar-1; h>=0; h--) {
					indices[h] = reminder/m_inc[h];
					reminder = reminder - indices[h] * m_inc[h];
			};
			return indices;
	}


  // printing:
  inline void print(){
  		PetscPrintf(m_comm,"Tensor dimension: %d \n", m_nVar);
      PetscPrintf(m_comm,"n Dof per dimension:\n");
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        PetscPrintf(m_comm,"dof for var_%d  = %d \n", idVar, m_nDof_var[idVar]);
      }
  }



  // VIRTUAL FUNCTIONS:



};

// end of file:
#endif
