// Implemenetation of the class sparse tensor

#include "sparseTensor.h"
//#include "linearAlgebraOperations.h"



// - I - CONSTRUCTORS:

/* overloaded constructor
  - inputs: dimension, degrees of freedom per dimension, communicator.
  - output: construction of a sparse tensor
*/
sparseTensor::sparseTensor(unsigned int dim, vector<unsigned int> dofPerDim, MPI_Comm theComm){
    m_nVar = dim;
    m_nDof_var.resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_nDof_var[iVar] = dofPerDim[iVar];
    }
    m_comm = theComm;
    compute_minc();
    m_isInitialised = true;
    m_tensorEntries = new vector<double>;
    m_indices = new vector<vector<unsigned int> >;
};


/* overloaded constructor
  - inputs: degrees of freedom per dimension, communicator.
  - output: construction of a sparse tensor
*/
sparseTensor::sparseTensor(vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_nVar = dofPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  m_comm = theComm;
  compute_minc();
  m_isInitialised = true;
  m_tensorEntries = new vector<double>;
  m_indices = new vector<vector<unsigned int> >;
};


/* overloaded constructor
  - inputs: the table of Active indices, the entries
  - output: construction of a sparse tensor
*/
sparseTensor::sparseTensor(vector<vector<unsigned int> >& indTab, vector<double>& entries){
  m_nVar = indTab[0].size();
  m_nonZeros = indTab.size();
  *m_indices = indTab;
  *m_tensorEntries = entries;
  m_isInitialised = false; // ! missing the tensor degrees of freedom per direction and communicator
};


/* overloaded constructor
  - inputs: dof per dim, the table of Active indices, the entries, the comm;
  - output: construction of a sparse tensor
*/
sparseTensor::sparseTensor(vector<unsigned int> dofPerDim, vector<vector<unsigned int> >& indTab, vector<double>& entries, MPI_Comm theComm){
  m_nVar = dofPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  m_comm = theComm;
  compute_minc();
  m_isInitialised = true;
  *m_indices = indTab;
  *m_tensorEntries = entries;
}


/* init: just a constructor variation in case of empty construction */
void sparseTensor::init(vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_nVar = dofPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  m_comm = theComm;
  compute_minc();
  m_tensorEntries = new vector<double>;
  m_indices = new vector<vector<unsigned int> >;
  m_isInitialised = true;
}


/*  copy tensor structure from a given tensor:
  - input: the tensor whose structure is copied (same number of non-zeros)
  - output: the structure of this tensor is shaped
*/
void sparseTensor::copyTensorStructFrom(sparseTensor& source){

  if(m_isInitialised){clear();}

  m_comm = source.comm();
  m_nVar = source.nVar();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = source.nDof_var(iVar);
  }
  compute_minc();
  m_isInitialised = true;
}


/* Copy the tensor from a given fullTensor:
  - input: the tensor to be copied
  - output: the tensor (structure + copy of the entries)
*/
void sparseTensor::copyTensorFrom(sparseTensor& source){
  copyTensorStructFrom(source);
  m_nonZeros = source.nonZeros();
  m_tensorEntries = new vector<double>;
  m_tensorEntries->resize(m_nonZeros);
  m_indices = new vector<vector<unsigned int> >;
  m_indices->resize(m_nonZeros);
  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    vector<unsigned int> ind(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      ind[iVar] = source.indices(iEntry, iVar);
    }
    (*m_indices)[iEntry] = ind;
    (*m_tensorEntries)[iEntry] = source.tensorEntries(iEntry);
  }
  finalize();
}


/* check if this tensor has the same structure of a given sparse tensor
  - input: the tensor to compare with
  - output: true if if has the same structure, false otherwise
*/

bool sparseTensor::hasSameStructure(sparseTensor& toBeComparedTo){
    bool toBeReturned = true;
    if(m_nVar != toBeComparedTo.nVar()){
      return false;
    }
    else{
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        if(m_nDof_var[idVar] != toBeComparedTo.nDof_var(idVar)){
          return false;
        }
      }
    }
    return toBeReturned;
}


/* check if this tensor is equal a given sparse tensor
  - input: the tensor to compare with
  - output: true if it is equal, false otherwise
*/
bool sparseTensor::isEqualTo(sparseTensor& toBeComparedTo){
    bool toBeReturned = hasSameStructure(toBeComparedTo);
    if(toBeReturned){

      if(m_nonZeros == toBeComparedTo.nonZeros()){

        for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
          // look for indices tuples:
          vector<unsigned int> thisIndex = (*m_indices)[iEntry];
          bool isThereOneEqual = false;
          unsigned int iElEqual = 0;
          for(unsigned int jEntry=0; jEntry< toBeComparedTo.nonZeros(); jEntry++){
            vector<unsigned int> toCheck = toBeComparedTo.indices(jEntry);
            bool checkThis = true;
            for(unsigned int iVar=0; iVar<m_nVar; iVar++){
              if(thisIndex[iVar] != toCheck[iVar]){
                checkThis = false;
                break;
              }
            }
            if(checkThis){
              isThereOneEqual = true;
              iElEqual = jEntry;
              break;
            }
          }

          if(isThereOneEqual){
            double iEntryVal = (*m_tensorEntries)[iEntry];
            double jEntryVal = toBeComparedTo.tensorEntries(iElEqual);
            if(iEntryVal != jEntryVal){
              toBeReturned = false;
            }
          }
        }
      }
      else{
        toBeReturned = false;
      }
    }
    return toBeReturned;
}


/* check if this tensor is equal a given sparse tensor (overloaded operator)
  - input: the tensor to compare with
  - output: true if it is equal, false otherwise
*/
bool sparseTensor::operator == (sparseTensor& toBeCompared){
  bool isItEqual = isEqualTo(toBeCompared);
  return isItEqual;
}


/* sum a given tensor times alpha to the current tensor
  - input: the tensor to be summed
  - output: the present tensor T <- T + alpha S
*/

void sparseTensor::sum(sparseTensor& toBeAdded, double alpha){
  if(hasSameStructure(toBeAdded)){
    for(unsigned int iEntry=0; iEntry<toBeAdded.nonZeros(); iEntry++){
      vector<unsigned int> ind = toBeAdded.indices(iEntry);
      if(isIndexActive(ind)){
        (*m_tensorEntries)[m_currentActive] = (*m_tensorEntries)[m_currentActive] + alpha * toBeAdded.tensorEntries(iEntry);
      }
      else{
        m_nonZeros += 1;
        m_indices->push_back(ind);
        m_tensorEntries->push_back(alpha * toBeAdded.tensorEntries(iEntry));
      }
    }
  }
  else{
    PetscPrintf(m_comm, "Tensor cannot be summed!\n");
  }
}


/* overloaded operator +=
  - input: the tensor to be summed
  - output:  T <- T + toBeAdded;
*/
void sparseTensor::operator += (sparseTensor& toBeAdded){
  sum(toBeAdded, 1.0);
}


/* multiply this tensor by a scalar
  - input: the scalar
  - output: the present tensor T <- alpha T
*/
void sparseTensor::multiplyByScalar(double alpha){
  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    (*m_tensorEntries)[iEntry] *= alpha;
  }
}


/* overloaded operator *=
  - input: the scalar
  - output: the present tensor T <- alpha T
*/
void sparseTensor::operator *= (double alpha){
  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    (*m_tensorEntries)[iEntry] *= alpha;
  }
}


/* shift the non-zero entries of the tensor by a scalar
  - input: the scalar
  - output: the present tensor T <- alpha + T
*/
void sparseTensor::shiftByScalar(double alpha){
  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    (*m_tensorEntries)[iEntry] += alpha;
  }
}


/* overloaded operator to shift while preserving the pattern
  - input: the scalar
  - output: the present tensor T <- alpha + T
*/
void sparseTensor::operator += (double alpha){
  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    (*m_tensorEntries)[iEntry] += alpha;
  }
}


/* Extract a subtensor and put it into a given full tensor
  - input: the indices bounds (C notation [lower_1,upper_1, lower2, upper_2,...]) and an empty full tensor
  - output: the subtensor
*/
void sparseTensor::extractSubtensor(vector<unsigned int> indicesBounds, sparseTensor& target){
  assert(indicesBounds.size()==2*m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  unsigned int N = 1;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    resPerDim[idVar] = indicesBounds[2*idVar+1] - indicesBounds[2*idVar];
    N = N * resPerDim[idVar];
  }
  target.init(resPerDim, m_comm);

  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    bool toBeTaken = true;
    vector<unsigned int> iInd = (*m_indices)[iEntry];
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if( (iInd[iVar] < indicesBounds[2*iVar]) || (iInd[iVar] >= indicesBounds[2*iVar+1]) ){
        toBeTaken = false;
        break;
      }
    }
    if(toBeTaken){
      vector<unsigned int> tgInd(m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        tgInd[iVar] = iInd[iVar] - indicesBounds[2*iVar];
      }
      double value = (*m_tensorEntries)[iEntry];
      target.set_tensorElement(iInd,value);
    }
  }
  target.finalize();
}


/* Extract a subtensor: overloaded to provide a sparse tensor
  - input: the indices bounds (C notation [lower_1,upper_1, lower2, upper_2,...]) and an empty full tensor
  - output: the subtensor
*/
sparseTensor sparseTensor::extractSubtensor(vector<unsigned int> indicesBounds){
  assert(indicesBounds.size()==2*m_nVar);
  vector<unsigned int> resPerDim(m_nVar);
  unsigned int N = 1;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    resPerDim[idVar] = indicesBounds[2*idVar+1] - indicesBounds[2*idVar];
    N = N * resPerDim[idVar];
  }
  sparseTensor target(resPerDim, m_comm);

  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    bool toBeTaken = true;
    vector<unsigned int> iInd = (*m_indices)[iEntry];
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if( (iInd[iVar] < indicesBounds[2*iVar]) || (iInd[iVar] >= indicesBounds[2*iVar+1]) ){
        toBeTaken = false;
        break;
      }
    }
    if(toBeTaken){
      vector<unsigned int> tgInd(m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        tgInd[iVar] = iInd[iVar] - indicesBounds[2*iVar];
      }
      double value = (*m_tensorEntries)[iEntry];
      target.set_tensorElement(iInd,value);
    }
  }
  target.finalize();
  return target;
}


/* Put a given subtensor into this sparse tensor, at specified positions
  - input: the indices bounds (C notation [lower_1,upper_1, lower2, upper_2,...]) and the sparse subtensor
  - output: the subtensor is put into the current tensor
*/
void sparseTensor::assignSubtensor(vector<unsigned int> indicesBounds, sparseTensor& source){
  assert(indicesBounds.size()==2*m_nVar);
  for(unsigned int iEntry=0; iEntry<source.nonZeros(); iEntry++){
    vector<unsigned int> iInd = source.indices(iEntry);
    double val = source.tensorEntries(iEntry);
    vector<unsigned int> ind(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      ind[iVar] = iInd[iVar] + indicesBounds[2*iVar];
    }
    set_tensorElement(ind,val);
  }
  finalize();
};


/* reshape the current tensor into a new one (pointer given)
  - input: the set of new resolution per variables, COPY flag (to actually copy the entries)
  - output: result is the tensor reshaped.
  REMARK: could be use to have unfoldings as sparseTensors ( without copy ) => fast
*/
void sparseTensor::reshape(vector<unsigned int> nDofPerVar, sparseTensor& result, bool COPY = true){
  assert(nDofPerVar.size()==m_nVar);
  result.init(nDofPerVar, m_comm);
  if(!COPY){
    result.set_tensorEntries(*m_tensorEntries);
    vector<vector<unsigned int> >* indTab;
    indTab = new vector<vector<unsigned int> >;
    indTab->resize(m_nonZeros);
    for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
      unsigned int lin = sub2lin((*m_indices)[iEntry]);
      vector<unsigned int> indReshaped = result.lin2sub(lin);
      (*indTab)[iEntry] = indReshaped;
    }
    result.set_indices(*indTab);
  }
  else{
    for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
      unsigned int lin = sub2lin((*m_indices)[iEntry]);
      vector<unsigned int> indReshaped = result.lin2sub(lin);
      double value = (*m_tensorEntries)[iEntry];
      result.set_tensorElement(indReshaped, value);
    }
  }
  result.finalize();
}


/* reshape the current tensor into a new one
  - input: the set of new resolution per variables, COPY flag (to actually copy the entries)
  - output: result is the tensor reshaped.
  REMARK: could be use to have unfoldings as fullTensors ( without copy ) => fast
*/
sparseTensor sparseTensor::reshape(vector<unsigned int> nDofPerVar, bool COPY = true){
  assert(nDofPerVar.size()==m_nVar);
  sparseTensor result(nDofPerVar, m_comm);
  if(!COPY){
    result.set_tensorEntries(*m_tensorEntries);
    vector<vector<unsigned int> >* indTab;
    indTab = new vector<vector<unsigned int> >;
    indTab->resize(m_nonZeros);
    for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
      unsigned int lin = sub2lin((*m_indices)[iEntry]);
      vector<unsigned int> indReshaped = result.lin2sub(lin);
      (*indTab)[iEntry] = indReshaped;
    }
    result.set_indices(*indTab);
  }
  else{
    for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
      unsigned int lin = sub2lin((*m_indices)[iEntry]);
      vector<unsigned int> indReshaped = result.lin2sub(lin);
      double value = (*m_tensorEntries)[iEntry];
      result.set_tensorElement(indReshaped, value);
    }
  }
  result.finalize();
  return result;
}


/* mode I contraction of a tensor and a given vector
  - input: the mode along which to contract, the vector b
  - output: tensor contracted by b to provide result
*/
sparseTensor sparseTensor::modeIContraction(unsigned int modeI, vec& b){
  assert(b.size() == m_nDof_var[modeI]);

  vector<unsigned int> dofPerVar(m_nVar-1);
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar != modeI){
      dofPerVar[cc] = m_nDof_var[idVar];
      cc = cc + 1;
    }
  }
  sparseTensor result(dofPerVar, m_comm);

  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    unsigned int l = (*m_indices)[iEntry][modeI];
    double value = (*m_tensorEntries)[iEntry] * b.getVecEl(l);
    vector<unsigned int> ind(m_nVar-1);
    unsigned int cc=0;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != modeI){
        ind[cc] = (*m_indices)[iEntry][iVar];
        cc += 1;
      }
    }
    if(result.isIndexActive(ind)){
      unsigned int resEntry = result.currentActive();
      result.add_toTensorEntry(resEntry, value);
    }
    else{
      result.set_tensorElement(ind, value);
    }
  }

  result.finalize();
  return result;
};


/* Compute unfolding and put it into a given matrix
  - input: the index along which the unfolding has to be computed
  - output: the unfolding into the Matrix
*/
void sparseTensor::computeUnfolding(const unsigned int index, mat& M){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  M.init(nRows, nCols, m_comm);

  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    double value = (*m_tensorEntries)[iEntry];
    vector<unsigned int> ind = (*m_indices)[iEntry];
    unsigned int iRow = ind[index];
    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(idVar != index){
        jCol = jCol + unfoldInc*ind[idVar];
        unfoldInc = unfoldInc * m_nDof_var[idVar];
      }
    }
    M.setMatEl(iRow,jCol, value);
  }
  M.finalize();
}

/*void sparseTensor::computeUnfolding(const unsigned int index, mat& M){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  M.init(nRows, nCols, m_comm);

  // compute the pattern:
  vector<int> I(m_nonZeros);
  vector<int> J(m_nonZeros);

  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    vector<unsigned int> ind = (*m_indices)[iEntry];
    // computing row and column in the unfolding:
    unsigned int iRow = ind[index];
    I[iEntry] = iRow;

    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(idVar != index){
        jCol = jCol + unfoldInc*ind[idVar];
        unfoldInc = unfoldInc * m_nDof_var[idVar];
      }
    }
    J[iEntry] = jCol;
  }
  cout << "Setting elements" << endl;
  M.setElements(I, J, *m_tensorEntries);
  cout << "done." << endl;
  M.finalize();
}*/


/* Compute unfolding and put it into a given matrix
  - input: the index along which the unfolding has to be computed
  - output: the unfolding into the Matrix
*/
/*void sparseTensor::computeUnfolding(const unsigned int index, mat& M){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  M.init(nRows, nCols, m_comm);

  // compute the pattern:
  vector<int> I;
  vector<vector<int> > J;
  vector<vector<double> > entries;

  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    vector<unsigned int> ind = (*m_indices)[iEntry];
    // computing row and column in the unfolding:
    unsigned int iRow = ind[index];

    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(idVar != index){
        jCol = jCol + unfoldInc*ind[idVar];
        unfoldInc = unfoldInc * m_nDof_var[idVar];
      }
    }
    // value of the entry:
    double val = (*m_tensorEntries)[iEntry];

    // check if iRow is in I:
    bool isIn = false;
    for(unsigned int iI=0; iI<I.size(); iI++){
      if(iRow==I[iI]){
        isIn=true;
        J[iI].push_back(jCol);
        entries[iI].push_back(val);
        break;
      }
    }
    // if it is a new row:
    if(!isIn){
      I.push_back(iRow);
      vector<int> newRow(1);
      newRow[0] = jCol;
      J.push_back(newRow);
      vector<double> newRowVals(1);
      newRowVals[0] = val;
      entries.push_back(newRowVals);
    }
  }
  // filling the matrix row-wise:
  for(unsigned int iI=0; iI<I.size(); iI++){
    if(iI%100==0){cout << "Row " << I[iI] << " number of active cols = "<< J[iI].size() << endl;}
    vector<int> rowList = {I[iI]};
    vector<int> colList = J[iI];
    vector<double> rowVals = entries[iI];
    M.setElements(rowList, colList, rowVals);
  }
  M.finalize();
}*/


/* Compute unfolding (overloaded to provide matrix)
  - input: the index along which the unfolding has to be computed
  - output: the unfolding into the Matrix
*/
mat sparseTensor::computeUnfolding(const unsigned int index){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  mat M(nRows, nCols, m_comm);

  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    double value = (*m_tensorEntries)[iEntry];
    vector<unsigned int> ind = (*m_indices)[iEntry];
    unsigned int iRow = ind[index];
    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(idVar != index){
        jCol = jCol + unfoldInc*ind[idVar];
        unfoldInc = unfoldInc * m_nDof_var[idVar];
      }
    }
    M.setMatEl(iRow,jCol, value);
  }
  M.finalize();
  return M;
}


/* Compute unfolding and put it into a set of vectors (get the fibers)
  - input: the index along which the unfolding has to be computed
  - output: the set of fibers
*/
void sparseTensor::computeFibers(const unsigned int index, vector<vec>& fibers){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  fibers.resize(nCols);
  for(unsigned int iCol=0; iCol<nCols; iCol++){
    fibers[iCol].init(nRows, m_comm);
  }

  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    double value = (*m_tensorEntries)[iEntry];
    vector<unsigned int> ind = (*m_indices)[iEntry];
    unsigned int iRow = ind[index];
    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(idVar != index){
        jCol = jCol + unfoldInc*ind[idVar];
        unfoldInc = unfoldInc * m_nDof_var[idVar];
      }
    }
    fibers[jCol].setVecEl(iRow, value);
  }
  for(unsigned int iCol=0; iCol<nCols; iCol++){
    fibers[iCol].finalize();
  }
}


/* Compute unfolding and put it into a set of vectors (get the fibers)
  - input: the index along which the unfolding has to be computed
  - output: the set of fibers
*/
vector<vec> sparseTensor::computeFibers(const unsigned int index){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  vector<vec> fibers(nCols);
  for(unsigned int iCol=0; iCol<nCols; iCol++){
    fibers[iCol].init(nRows, m_comm);
  }

  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    double value = (*m_tensorEntries)[iEntry];
    vector<unsigned int> ind = (*m_indices)[iEntry];
    unsigned int iRow = ind[index];
    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(idVar != index){
        jCol = jCol + unfoldInc*ind[idVar];
        unfoldInc = unfoldInc * m_nDof_var[idVar];
      }
    }
    fibers[jCol].setVecEl(iRow, value);
  }
  for(unsigned int iCol=0; iCol<nCols; iCol++){
    fibers[iCol].finalize();
  }
  return fibers;
}


/* compute Unfolding SVD:
  - input:
  - output:
*/
void sparseTensor::computeUnfoldingSVD(const unsigned int index){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}
  // initialising covariance:
  mat C(nRows, nRows, m_comm);

  // compute the pattern:
  vector<vector<unsigned int> > J(nRows);
  vector<vector<unsigned int> > back_map(nRows);
  for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
    vector<unsigned int> ind = (*m_indices)[iEntry];
    unsigned int iRow = ind[index];
    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      if(idVar != index){
        jCol = jCol + unfoldInc*ind[idVar];
        unfoldInc = unfoldInc * m_nDof_var[idVar];
      }
    }
    J[iRow].push_back(jCol);
    back_map[iRow].push_back(iEntry);
  }

  // compute C:
  for(unsigned int iRow=0; iRow<nRows; iRow++){
    cout << iRow << endl;
    vector<double> scalar(iRow+1);
    for(unsigned int kPat=0; kPat<J[iRow].size(); kPat++){
      unsigned int kCol = J[iRow][kPat];
      unsigned int linI = back_map[iRow][kPat];
      double val_I = (*m_tensorEntries)[linI];
      for(unsigned int jRow=0; jRow<=iRow; jRow++){
          // find kCol in the pattern:
          for(unsigned int jPat=0; jPat<J[jRow].size(); jPat++){
            if(J[jRow][jPat]==kCol){
              unsigned int linJ = back_map[jRow][jPat];
              double val_J = (*m_tensorEntries)[linJ];
              scalar[jRow] += val_I * val_J;
              break;
            }
          }
      }
    }
    for(unsigned int jRow=0; jRow<=iRow; jRow++){
      C.setMatEl(iRow, jRow, scalar[jRow]);
      if(iRow != jRow){C.setMatEl(jRow, iRow, scalar[jRow]);}
    }
    scalar.clear();
  }
  C.finalize();


  // extract the modes:
  eigenSolver eps;
  eps.init(C, EPS_HEP, EPSKRYLOVSCHUR);
  eps.solveHermEPS(C);
  vector<vec> vv = eps.eigenVecs();
  vector<double> lambda = eps.eigenVals();
  // setting the singular values:
  //cout << "n of non-zero lambda = " << lambda.size() << endl;
  vec S;
  S.init(lambda.size(), m_comm);
  for(unsigned int iS=0; iS<lambda.size(); iS++){
    S(iS) = sqrt(fabs(lambda[iS]));
  }
  S.finalize();

  // set U:

  // free the memory:
  eps.clear();
  C.clear();
}




/* Evaluate unfolding without assembling it!
  - input: the index along which the unfolding has to be computed, (I,J) of the unfolding
  - output: the value of the unfolding_[index] in I,J
*/
void sparseTensor::evalUnfolding(const unsigned int index, const unsigned int I, const unsigned int J, double& value){
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}

  vector<unsigned int> ind(m_nVar);
  ind[index] = I;
  //define the unfolding increments
  vector<unsigned int> unfoldInc(m_nVar-1);
  unsigned int unfInc = 1;
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar!=index){
      unfoldInc[cc] = unfInc;
      unfInc = unfInc * m_nDof_var[idVar];
      cc = cc + 1;
    }
  }
  // compute the tensor indices
  unsigned int reminder = J;
  cc = m_nVar-2;
  for (int h=m_nVar-1; h>=0; h--){
      if(h != index){
        ind[h] = reminder/unfoldInc[cc];
        reminder = reminder - ind[h] * unfoldInc[cc];
        cc = cc - 1;
      }
  };
  if(isIndexActive(ind)){
    value = (*m_tensorEntries)[m_currentActive];
  }
  else{
    value = 0.0;
  }
}


/* Evaluate unfolding without assembling it!
  - input: the index along which the unfolding has to be computed, (I,J) of the unfolding
  - output: the value of the unfolding_[index] in I,J
*/
double sparseTensor::evalUnfolding(const unsigned int index, const unsigned int I, const unsigned int J){
  double toBeReturned = 0.0;
  assert(index<m_nVar);
  const unsigned int nRows = m_nDof_var[index];
	unsigned int nCols = 1;
	for (unsigned int idVar=0; idVar<m_nVar; idVar++){
		if (idVar != index){
			nCols = nCols*m_nDof_var[idVar];
		}
	}

  vector<unsigned int> ind(m_nVar);
  ind[index] = I;
  //define the unfolding increments
  vector<unsigned int> unfoldInc(m_nVar-1);
  unsigned int unfInc = 1;
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    if(idVar!=index){
      unfoldInc[cc] = unfInc;
      unfInc = unfInc * m_nDof_var[idVar];
      cc = cc + 1;
    }
  }
  // compute the tensor indices
  unsigned int reminder = J;
  cc = m_nVar-2;
  for (int h=m_nVar-1; h>=0; h--){
      if(h != index){
        ind[h] = reminder/unfoldInc[cc];
        reminder = reminder - ind[h] * unfoldInc[cc];
        cc = cc - 1;
      }
  };
  if(isIndexActive(ind)){
    toBeReturned = (*m_tensorEntries)[m_currentActive];
  }
  return toBeReturned;
}


/* in place matrix tensor product
  - input: the variable along which to compute the product
  - output: this tensor is transformed into the result
*/
void sparseTensor::inPlaceMatTensProd(unsigned int Jvar, mat& A){
  assert(m_nDof_var[Jvar]== A.nCols());

  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != Jvar){
      resPerDim[iVar] = m_nDof_var[iVar];
    }
    else{
      resPerDim[iVar] = A.nRows();
    }
  }

  // compute the Jvar unfolding of the result:
  mat jUnfold = computeUnfolding(Jvar);
  mat resUnfold = A * jUnfold;

  // clear the current tensor and re-init:
  MPI_Comm currentComm = m_comm;
  clear();
  init(resPerDim, currentComm);

  // compute the tensor entries through mapping:
  unsigned int nEnt = resUnfold.nRows()*resUnfold.nCols();
  m_tensorEntries = new vector<double>;
  m_indices = new vector<vector<unsigned int> >;
  for(unsigned int l=0; l<nEnt; l++){
    vector<unsigned int> ind = lin2sub(l);
    unsigned int iRow = ind[Jvar];
    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != Jvar){
        jCol = jCol + unfoldInc*ind[iVar];
        unfoldInc = unfoldInc * resPerDim[iVar];
      }
    }
    double value = resUnfold.getMatEl(iRow, jCol);
    if(value != 0.0){
      m_tensorEntries->push_back(value);
      m_indices->push_back(ind);
      m_nonZeros += 1;
    }
  }

  // free the memory;
  resUnfold.clear();
  jUnfold.clear();
}


/* matrix tensor product
  - input: the variable along which to compute the product
  - output: this tensor is transformed into the result
*/
sparseTensor sparseTensor::matTensProd(unsigned int Jvar, mat& A){
  assert(m_nDof_var[Jvar]== A.nCols());

  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(iVar != Jvar){
      resPerDim[iVar] = m_nDof_var[iVar];
    }
    else{
      resPerDim[iVar] = A.nRows();
    }
  }

  sparseTensor result(resPerDim, m_comm);

  // compute the Jvar unfolding of the result:
  mat jUnfold = computeUnfolding(Jvar);
  mat resUnfold = A * jUnfold;

  // compute the tensor entries through mapping:
  unsigned int nEnt = resUnfold.nRows()*resUnfold.nCols();
  for(unsigned int l=0; l<nEnt; l++){
    vector<unsigned int> ind = result.lin2sub(l);
    unsigned int iRow = ind[Jvar];
    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != Jvar){
        jCol = jCol + unfoldInc*ind[iVar];
        unfoldInc = unfoldInc * resPerDim[iVar];
      }
    }
    double value = resUnfold.getMatEl(iRow, jCol);
    if(value != 0.0){
      result.set_tensorElement(ind, value);
    }
  }
  result.finalize();

  // free the memory;
  resUnfold.clear();
  jUnfold.clear();

  return result;
}



// I/O functions



/* Importing full tensor from FROSTT repository.
  - Input: file name, reference to an empty full tensor, number of entries per line
  - Output: initialise and fill the full tensor.
*/
void sparseTensor::importFrostt(string fName, unsigned int nInLine, MPI_Comm theComm){
  m_comm = theComm;
  unsigned int dim = nInLine - 1;

  ifstream inputFile;
  inputFile.open(fName.c_str());
  char output[128];
  unsigned int IthMode;
  double tensorValue;

  unsigned int cc = 0;


  m_tensorEntries = new vector<double>;
  m_indices = new vector<vector<unsigned int> >;

  vector<int> maxModes(nInLine-1, 0); // assuming modes are positive in the tensor!!
  vector<int> minModes(nInLine-1, 16777216);  // 2^24
  vector<unsigned int> thisLine;
  if (inputFile.is_open()) {
    cout << "Reading...";
    while (!inputFile.eof()) {
      inputFile >> output;
      stringstream str;
      str << output;
      //vector<int> thisLine(nInLine-1);
      if(cc%nInLine<nInLine-1){
        unsigned int iVar = cc%nInLine;
        if(iVar==0){thisLine.clear(); thisLine.resize(nInLine-1);}
        // convert it to unsigned int
        str >> IthMode;
        thisLine[iVar] = int(IthMode);
        if(IthMode>maxModes[iVar]){
          maxModes[iVar] = IthMode;
        }
        if(IthMode<minModes[iVar]){
          minModes[iVar] = IthMode;
        }
        if(iVar == nInLine-2){
          m_indices->push_back(thisLine);
        }
      }
      else{
        // it is the tensor value, convert it to double
        str >> tensorValue;
        m_tensorEntries->push_back(tensorValue);
      }
      cc +=1;
     }
     cout << "done.\n";
  }
  else{
    puts("Unable to open file!");
    exit(1);
  }
  inputFile.close();


  /* analize the tab to get the resolution.
  assuming implicitly that the step in each mode is 1 (otherwise a map is required!)
  */
  vector<unsigned int> resPerDim(nInLine-1);
  for(unsigned int iVar = 0; iVar< nInLine-1; iVar++){
    assert(maxModes[iVar] > minModes[iVar]);
    resPerDim[iVar] = maxModes[iVar] - minModes[iVar] + 1;
  }
  for(unsigned int iEntry=0; iEntry<m_tensorEntries->size(); iEntry++){
    for(unsigned int iVar=0; iVar<nInLine-1; iVar++){
      (*m_indices)[iEntry][iVar] = (*m_indices)[iEntry][iVar] - minModes[iVar];
    }
  }
  m_nonZeros = m_tensorEntries->size();
  m_nVar = nInLine-1;
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = resPerDim[iVar];
  }
  compute_minc();
  m_isInitialised = true;
}
