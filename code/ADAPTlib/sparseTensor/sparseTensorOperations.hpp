// Header file for sparse tensor operations

#ifndef sparseTensorOperations_hpp
#define sparseTensorOperations_hpp

#include "sparseTensor.h"


/* Left Kronecker product of tensors
  - input: tensors A and B
  - output: a tensor which is the Kronecker product of the given tensors
*/
sparseTensor leftKronecker(sparseTensor& A, sparseTensor& B);


/* Right Kronecker product of tensors
  - input: tensors A and B
  - output: this tensor (empty) is the right Kronecker product of the tensors
*/
sparseTensor rightKronecker(sparseTensor& A, sparseTensor& B);

/* mode I Khatri-Rao product of two given sparse Tensors.
  - input: the sparse tensors A and B, the mode I
  - output: sparse tensor which is the mode I Khatri-Rao product of A and B
*/
sparseTensor modeIKhatriRao(unsigned int modeI, sparseTensor& A, sparseTensor& B);


/* mode I concatenation of two full order tensors
  - input: tensor A and tensor B in full tensor format
  - output: this tensor is the concatenation
*/
sparseTensor modeIConcatenate(unsigned int modeI, sparseTensor& A, sparseTensor& B);


/* outer Product of two given tensors A and B
  - input: the tensors A and B (sparse tensors)
  - output: the tensor which is the outer product of the two
*/
sparseTensor outerProduct(sparseTensor& A, sparseTensor& B);


/* mode I contraction of a tensor and a given vector
  - input: the mode along which to contract, the vector b
  - output: tensor contracted by b to provide result
*/
sparseTensor modeIContraction(unsigned int modeI, sparseTensor &A, vec& b);


/* operator +, to sum two Tensors
  - input: tensor A and B
  - output A + B
*/
sparseTensor operator + (sparseTensor& A, sparseTensor& B);


/* scalar Product of Full Tensors:
  - input: two Full tensors
  - output: A:B
*/
double scalProd(sparseTensor& A, sparseTensor& B);


/* Multiply a full tensor by a matrix:
  - input: Full tensor, a matrix, the mode along which to compute.
  - output: T*M
*/
sparseTensor matTensProd(unsigned int, sparseTensor&, mat&);

#endif
