// Implementation of sparse tensor operations

#include "sparseTensorOperations.hpp"
#include "linearAlgebraOperations.h"


/* Left Kronecker product of tensors
  - input: tensors A and B
  - output: a tensor which is the Kronecker product of the given tensors
*/
sparseTensor leftKronecker(sparseTensor& A, sparseTensor& B){
  assert(A.nVar()==B.nVar());
  assert(A.comm()==B.comm());
  vector<unsigned int>resPerDim(A.nVar());
  for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
    resPerDim[idVar] = A.nDof_var(idVar) * B.nDof_var(idVar);
  }
  sparseTensor toBeReturned(resPerDim, A.comm());
  for(unsigned int lB=0; lB<B.nonZeros(); lB++){
    vector<unsigned int> indicesB = B.indices(lB);
    double valB = B.tensorEntries(lB);
    for(unsigned int lA=0; lA<A.nonZeros(); lA++){
      vector<unsigned int> indicesA = A.indices(lA);
      double valA = A.tensorEntries(lA);
      vector<unsigned int> ind(A.nVar());
      for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
        ind[idVar] = indicesB[idVar]*A.nDof_var(idVar) + indicesA[idVar];
      }
      double val = valA*valB;
      if(val != 0.0){
        toBeReturned.set_tensorElement(ind, val);
      }
    }
  }
  toBeReturned.finalize();
  return toBeReturned;
}


/* Right Kronecker product of tensors
  - input: tensors A and B
  - output: this tensor (empty) is the right Kronecker product of the tensors
*/
sparseTensor rightKronecker(sparseTensor& A, sparseTensor& B){
  assert(A.nVar()==B.nVar());
  assert(A.comm()==B.comm());
  vector<unsigned int> resPerDim(A.nVar());
  for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
    resPerDim[idVar] = A.nDof_var(idVar) * B.nDof_var(idVar);
  }
  sparseTensor toBeReturned(resPerDim, A.comm());
  for(unsigned int lB=0; lB<B.nonZeros(); lB++){
    vector<unsigned int> indicesB = B.indices(lB);
    double valB = B.tensorEntries(lB);
    for(unsigned int lA=0; lA<A.nonZeros(); lA++){
      vector<unsigned int> indicesA = A.indices(lA);
      double valA = A.tensorEntries(lA);
      vector<unsigned int> ind(A.nVar());
      for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
        ind[idVar] = indicesA[idVar]*B.nDof_var(idVar) + indicesB[idVar];
      }
      double val = valA*valB;
      if(val != 0.0){
        toBeReturned.set_tensorElement(ind, val);
      }
    }
  }
  toBeReturned.finalize();
  return toBeReturned;
}


/* mode I Khatri-Rao product of two given sparse Tensors.
  - input: the sparse tensors A and B, the mode I
  - output: sparse tensor which is the mode I Khatri-Rao product of A and B
*/
sparseTensor modeIKhatriRao(unsigned int modeI, sparseTensor& A, sparseTensor& B){
  assert(A.nVar()==B.nVar());
  assert(A.comm()==B.comm());
  assert(A.nDof_var(modeI) == B.nDof_var(modeI));
  assert(modeI < A.nVar());

  // Init the resolution
  vector<unsigned int> dofPerDim(A.nVar());

  vector<unsigned int>indicesBoundsA(2*A.nVar());
  vector<unsigned int>indicesBoundsB(2*A.nVar());
  vector<unsigned int>indicesBoundsThis(2*A.nVar());
  for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
    if(idVar != modeI){
      dofPerDim[idVar] = A.nDof_var(idVar) * B.nDof_var(idVar);
      indicesBoundsA[2*idVar] = 0;
      indicesBoundsA[2*idVar+1] = A.nDof_var(idVar);
      indicesBoundsB[2*idVar] = 0;
      indicesBoundsB[2*idVar+1] = B.nDof_var(idVar);
      indicesBoundsThis[2*idVar] = 0;
      indicesBoundsThis[2*idVar+1] = dofPerDim[idVar];
    }
    else{
      dofPerDim[idVar] = A.nDof_var(modeI);
    }
  }

  sparseTensor toBeReturned(dofPerDim, A.comm());

  for(unsigned int idDof=0; idDof<dofPerDim[modeI]; idDof++){
    sparseTensor subA;
    indicesBoundsA[2*modeI] = idDof;
    indicesBoundsA[2*modeI+1] = idDof+1;
    A.extractSubtensor(indicesBoundsA,subA);
    sparseTensor subB;
    indicesBoundsB[2*modeI] = idDof;
    indicesBoundsB[2*modeI+1] = idDof+1;
    B.extractSubtensor(indicesBoundsB,subB);

    sparseTensor subKronProd = rightKronecker(subA,subB);
    indicesBoundsThis[2*modeI] = idDof;
    indicesBoundsThis[2*modeI+1] = idDof+1;
    toBeReturned.assignSubtensor(indicesBoundsThis,subKronProd);
  }
  toBeReturned.finalize();
  return toBeReturned;
}


/* mode I concatenation of two full order tensors
  - input: tensor A and tensor B in full tensor format
  - output: this tensor is the concatenation
*/
sparseTensor modeIConcatenate(unsigned int modeI, sparseTensor& A, sparseTensor& B){
  assert(A.nVar()==B.nVar());
  assert(A.comm()==B.comm());
  assert(modeI < A.nVar());
  for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
    if(idVar!=modeI){
      assert(A.nDof_var(idVar)==B.nDof_var(idVar));
    }
  }

  vector<unsigned int> dofPerDim(A.nVar());

  vector<unsigned int>indicesBoundsA(2*A.nVar());
  vector<unsigned int>indicesBoundsB(2*A.nVar());
  for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
    if(idVar != modeI){
      dofPerDim[idVar] = A.nDof_var(idVar);
      indicesBoundsA[2*idVar] = 0;
      indicesBoundsA[2*idVar+1] = A.nDof_var(idVar);
      indicesBoundsB[2*idVar] = 0;
      indicesBoundsB[2*idVar+1] = B.nDof_var(idVar);
    }
    else{
      dofPerDim[idVar] = A.nDof_var(idVar) + B.nDof_var(idVar);
      indicesBoundsA[2*idVar] = 0;
      indicesBoundsA[2*idVar+1] = A.nDof_var(idVar);
      indicesBoundsB[2*idVar] = A.nDof_var(idVar);
      indicesBoundsB[2*idVar+1] = dofPerDim[idVar];
    }
  }
  sparseTensor toBeReturned(dofPerDim, A.comm());
  toBeReturned.assignSubtensor(indicesBoundsA,A);
  toBeReturned.assignSubtensor(indicesBoundsB,B);
  toBeReturned.finalize();
  return toBeReturned;
}


/* outer Product of two given tensors A and B
  - input: the tensors A and B (sparse tensors)
  - output: the tensor which is the outer product of the two
*/
sparseTensor outerProduct(sparseTensor& A, sparseTensor& B){
  assert(A.comm() == B.comm());

  const unsigned int dim = A.nVar() + B.nVar();
  vector<unsigned int> dofPerDim(dim);
  for(unsigned int idVar=0; idVar<dim; idVar++){
    if(idVar<A.nVar()){
      dofPerDim[idVar] = A.nDof_var(idVar);
    }
    else{
      unsigned int idB = idVar - A.nVar();
      dofPerDim[idVar] = B.nDof_var(idB);
    }
  }
  sparseTensor toBeReturned(dofPerDim, A.comm());

  for(unsigned int lA=0; lA<A.nonZeros(); lA++){
    vector<unsigned int> indA = A.indices(lA);
    double valA = A.tensorEntries(lA);
    vector<unsigned int> ind(dim);
    for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
      ind[idVar] = indA[idVar];
    }
    for(unsigned int lB=0; lB<B.nonZeros(); lB++){
      vector<unsigned int> indB = B.indices(lB);
      double valB = B.tensorEntries(lB);
      double val = valA*valB;
      for(unsigned int idVar=0; idVar<B.nVar(); idVar++){
          unsigned int idThis = idVar + A.nVar();
          ind[idThis] = indB[idVar];
      }
      toBeReturned.set_tensorElement(ind,val);
    }
  }
  toBeReturned.finalize();
  return toBeReturned;
}


/* mode I contraction of a tensor and a given vector
  - input: the mode along which to contract, the vector b
  - output: tensor contracted by b to provide result
*/
sparseTensor modeIContraction(unsigned int modeI, sparseTensor &A, vec& b){
  assert(b.size() == A.nDof_var(modeI));

  vector<unsigned int> dofPerVar(A.nVar()-1);
  unsigned int cc = 0;
  for(unsigned int idVar=0; idVar<A.nVar(); idVar++){
    if(idVar != modeI){
      dofPerVar[cc] = A.nDof_var(idVar);
      cc = cc + 1;
    }
  }
  sparseTensor result(dofPerVar, A.comm());

  for(unsigned int iEntry=0; iEntry<A.nonZeros(); iEntry++){
    unsigned int l = A.indices(iEntry,modeI);
    double value = A.tensorEntries(iEntry) * b.getVecEl(l);
    vector<unsigned int> ind(A.nVar()-1);
    unsigned int cc=0;
    for(unsigned int iVar=0; iVar<A.nVar(); iVar++){
      if(iVar != modeI){
        ind[cc] = A.indices(iEntry,iVar);
        cc += 1;
      }
    }
    if(result.isIndexActive(ind)){
      unsigned int resEntry = result.currentActive();
      result.add_toTensorEntry(resEntry, value);
    }
    else{
      result.set_tensorElement(ind, value);
    }
  }

  result.finalize();
  return result;
};


/* operator +, to sum two Tensors
  - input: tensor A and B
  - output A + B
*/
sparseTensor operator + (sparseTensor& A, sparseTensor& B){
  assert(A.hasSameStructure(B));
  sparseTensor result;
  result.copyTensorFrom(A);
  result += B;
  return result;
}


/* scalar Product of Full Tensors:
  - input: two Full tensors
  - output: A:B
*/
double scalProd(sparseTensor& A, sparseTensor& B){
  assert(A.hasSameStructure(B));
  double result = 0.0;

  for(unsigned int iAEntry=0; iAEntry<A.nonZeros(); iAEntry++){
    vector<unsigned int> indA = A.indices(iAEntry);
    // check if there is an equal tuple of indices in B:
    bool isThereOneEqual = false;
    unsigned int iElEqual = 0;
    for(unsigned int jBEntry=0; jBEntry< B.nonZeros(); jBEntry++){
      vector<unsigned int> toCheck = B.indices(jBEntry);
      bool checkThis = true;
      for(unsigned int iVar=0; iVar<B.nVar(); iVar++){
        if(indA[iVar] != toCheck[iVar]){
          checkThis = false;
          break;
        }
      }
      if(checkThis){
        isThereOneEqual = true;
        iElEqual = jBEntry;
        break;
      }
    }
    // if there is (pattern intersection), add the scalar product
    if(isThereOneEqual){
      result = result + A.tensorEntries(iAEntry) * B.tensorEntries(iElEqual);
    }
  }

  return result;
}


/* Multiply a full tensor by a matrix:
  - input: Full tensor, a matrix, the mode along which to compute.
  - output: T*M
*/
sparseTensor matTensProd(unsigned int Jvar, sparseTensor& T, mat& A){
  assert(T.nDof_var(Jvar)== A.nCols());

  vector<unsigned int> resPerDim(T.nVar());
  for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
    if(iVar != Jvar){
      resPerDim[iVar] = T.nDof_var(iVar);
    }
    else{
      resPerDim[iVar] = A.nRows();
    }
  }

  sparseTensor result(resPerDim, T.comm());

  // compute the Jvar unfolding of the result:
  mat jUnfold = T.computeUnfolding(Jvar);
  mat resUnfold = A * jUnfold;

  // compute the tensor entries through mapping:
  unsigned int nEnt = resUnfold.nRows()*resUnfold.nCols();
  for(unsigned int l=0; l<nEnt; l++){
    vector<unsigned int> ind = result.lin2sub(l);
    unsigned int iRow = ind[Jvar];
    unsigned int jCol = 0;
    unsigned int unfoldInc = 1;
    for(unsigned int iVar=0; iVar<T.nVar(); iVar++){
      if(iVar != Jvar){
        jCol = jCol + unfoldInc*ind[iVar];
        unfoldInc = unfoldInc * resPerDim[iVar];
      }
    }
    double value = resUnfold.getMatEl(iRow, jCol);
    if(value != 0.0){
      result.set_tensorElement(ind, value);
    }
  }
  result.finalize();

  // free the memory;
  resUnfold.clear();
  jUnfold.clear();

  return result;
}
