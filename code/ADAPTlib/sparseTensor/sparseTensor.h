// Header file for Sparse Tensor Class

#ifndef sparseTensor_h
#define sparseTensor_h

#include "../tensor.h"
#include "../linAlg/linAlg.h"

class sparseTensor:public tensor{
private:
  vector<vector<unsigned int> >* m_indices;
  vector<double>* m_tensorEntries;
  unsigned int m_nonZeros;
  unsigned int m_currentActive; // auxiliary variable;
  bool m_isInitialised;

public:
  sparseTensor(){};
  ~sparseTensor(){};
  void clear(){
    m_tensorEntries->clear();
    m_indices->clear();
    delete m_indices;
    delete m_tensorEntries;
  }
  sparseTensor(unsigned int, vector<unsigned int>, MPI_Comm);
  sparseTensor(vector<unsigned int>, MPI_Comm);
  sparseTensor(vector<vector<unsigned int> >&, vector<double>&);
  sparseTensor(vector<unsigned int>, vector<vector<unsigned int> >&, vector<double>&, MPI_Comm);

  void init(vector<unsigned int>, MPI_Comm);

  // finalize:
  inline void finalize(){
    getNonZeros();
  }

  // - get the nonZeros()
  inline unsigned int getNonZeros(){m_nonZeros = m_indices->size(); return m_indices->size();}

  // check if indices are active (i.e.: the entry is non-zero):
  // m_currentActive contains the position iEntry is the index is active
  inline bool isIndexActive(vector<unsigned int> indToCheck){
    bool isIt = false;
    for(unsigned int iEntry=0; iEntry<m_indices->size(); iEntry++){
      vector<unsigned int> indEntry = (*m_indices)[iEntry];
      bool isThisEqual = true;
      for(unsigned int iVar=0; iVar< m_nVar; iVar++){
        if(indEntry[iVar] != indToCheck[iVar]){
          isThisEqual = false;
          break;
        }
      }
      if(isThisEqual){
        m_currentActive = iEntry;
        isIt = true;
        break;
      }
    }
    return isIt;
  }

  // -- ACCESS FUNCTIONS: --
  inline vector<vector<unsigned int> > indices(){return *m_indices;}
  inline vector<unsigned int> indices(unsigned int iEntry){return (*m_indices)[iEntry];}
  inline unsigned int indices(unsigned int iEntry, unsigned int iVar){return (*m_indices)[iEntry][iVar];}
  inline vector<double> tensorEntriesVector(){return *m_tensorEntries;}
  inline double tensorEntries(unsigned int iEntry){return (*m_tensorEntries)[iEntry];}
  inline double tensorEntries(vector<unsigned int> ind){
    if(isIndexActive(ind)){
      return (*m_tensorEntries)[m_currentActive];
    }
    else{
      return 0.0;
    }
  }
  inline vec tensorEntries(){
    m_nonZeros = m_tensorEntries->size();
    vec toBeReturned(m_nonZeros, m_comm);
    for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
      toBeReturned.setVecEl(iEntry, (*m_tensorEntries)[iEntry]);
    }
    toBeReturned.finalize();
    return toBeReturned;
  }

  inline unsigned int nonZeros(){return m_nonZeros;}
  inline bool isInitialised(){return m_isInitialised;}
  inline unsigned int currentActive(){return m_currentActive;}

  // set and get tensor elements;
  void set_tensorElement(vector<unsigned int> ind, double val){
    if(isIndexActive(ind)){
      (*m_tensorEntries)[m_currentActive] = val;
    }
    else{
      m_indices->push_back(ind);
      m_nonZeros += 1;
      m_tensorEntries->push_back(val);
    }
  }

  // set tensor Entries:
  void set_tensorEntries(vector<double>& entries){
    (*m_tensorEntries) = entries;
  }

  void set_indices(vector<vector<unsigned int> >& indTable){
    (*m_indices) = indTable;
  }

  void add_toTensorEntry(unsigned int l, double value){
    (*m_tensorEntries)[l] += value;
  }

  // compute the l-2 norm of a sparse tensor:
  inline double norm(){
    double toBeReturned = 0.0;
    for(unsigned int iEntry=0; iEntry<m_nonZeros; iEntry++){
      toBeReturned += (*m_tensorEntries)[iEntry] * (*m_tensorEntries)[iEntry];
    }
    toBeReturned = sqrt(toBeReturned);
    return toBeReturned;
  }

  // -- EVAL function for Sparse tensors --

  // basic void version:
  void eval(const vector<unsigned int>& ind, double& val){
    if(isIndexActive(ind)){
      val = (*m_tensorEntries)[m_currentActive];
    }
    else{
      val =  0.0;
    }
	};

  // overloaded to return a double:
  double eval(const vector<unsigned int>& ind){
    double val = 0.0;
    if(isIndexActive(ind)){
      val =  (*m_tensorEntries)[m_currentActive];
    }
    return val;
  }

  // overloaded with variadic list of indices:
  double eval(unsigned int iComp,...){
    vector<unsigned int> indices(m_nVar);
    va_list ap;
    va_start(ap, iComp);
    indices[0] = iComp;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      indices[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);
    double val = 0.0;
    if(isIndexActive(indices)){
      val =  (*m_tensorEntries)[m_currentActive];
    }
    return val;
  }

  // Nested proxy class for accessing and manipulating tensor elements
  // overloading operator () in assignement through a proxy class:
  class Proxy : public tensor
  {
    vector<vector<unsigned int> >* xInd;
    vector<double>* x;
    vector<unsigned int> idx;
    unsigned int m_currentActive;
    unsigned int xNonZeros;
  public:
      // m_currentActive contains the position iEntry is the index is active
      inline bool isIndexActive(vector<unsigned int> indToCheck){
        bool isIt = false;
        for(unsigned int iEntry=0; iEntry<xInd->size(); iEntry++){
          vector<unsigned int> indEntry = (*xInd)[iEntry];
          bool isThisEqual = true;
          for(unsigned int iVar=0; iVar< m_nVar; iVar++){
            if(indEntry[iVar] != indToCheck[iVar]){
              isThisEqual = false;
              break;
            }
          }
          if(isThisEqual){
            m_currentActive = iEntry;
            isIt = true;
            break;
          }
        }
        return isIt;
      }

      // Set Tensor Elements:
      inline void set_tensorElement(vector<unsigned int> ind, double val){
        if(isIndexActive(ind)){
          (*x)[m_currentActive] = val;
        }
        else{
          xInd->push_back(ind);
          x->push_back(val);
          xNonZeros += 1;
        }
      }

      // overloaded to return a double:
      double eval(const vector<unsigned int>& ind){
        double val = 0.0;
        if(isIndexActive(ind)){
          val =  (*x)[m_currentActive];
        }
        return val;
      }

      // Constructor of the proxy class:
      Proxy(vector<unsigned int> idx, vector<vector<unsigned int> >* xInd, vector<double>* x, unsigned int xNonZeros) : idx(idx), xInd(xInd), x(x), xNonZeros(xNonZeros){
        m_nVar = idx.size();
      }

      // equal operator overload:
      inline double operator= (double value) {
        set_tensorElement(idx, value);
        return value;
      }

      // Overloading the double operator for assignement:
      operator double(){
        double toBeReturned = eval(idx);
        return toBeReturned;
      }
  };

  // Calling the proxy object through variadic:
  Proxy operator() (unsigned int I,...) {

    vector<unsigned int> ind(m_nVar);
    va_list ap;
    va_start(ap, I);
    ind[0] = I;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      ind[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);
    return Proxy(ind, m_indices, m_tensorEntries, m_nonZeros);
  }


  // -- METHODS for sparse tensor: --

  // COPY Tensor Structure:
	void copyTensorStructFrom(sparseTensor&);

	// COPY the sparse tensor:
	void copyTensorFrom(sparseTensor&);

  // OPERATIONS on sparse tensor:
	bool hasSameStructure(sparseTensor&);
	bool isEqualTo(sparseTensor&);
  bool operator == (sparseTensor&);
	void sum(sparseTensor&, double);
  void operator += (sparseTensor&);
	void multiplyByScalar(double);
  void operator *= (double);
	void shiftByScalar(double);
  void operator += (double);
	void extractSubtensor(vector<unsigned int>, sparseTensor&);
  sparseTensor extractSubtensor(vector<unsigned int>);
	void assignSubtensor(vector<unsigned int>, sparseTensor&);
	void reshape(vector<unsigned int>, sparseTensor&, bool);
  sparseTensor reshape(vector<unsigned int>, bool);
  sparseTensor modeIContraction(unsigned int, vec&);

  // specific to sparseTensor:
  void inPlaceMatTensProd(unsigned int, mat&);
  sparseTensor matTensProd(unsigned int, mat&);

	// - Compute unfolding -
	void computeUnfolding(const unsigned int, mat&);
  mat computeUnfolding(const unsigned int);
	void computeFibers(const unsigned int, vector<vec>&);
  vector<vec> computeFibers(const unsigned int);
	void evalUnfolding(const unsigned int,const unsigned int,const unsigned int,double&);
  double evalUnfolding(const unsigned int,const unsigned int,const unsigned int);
  void computeUnfoldingSVD(const unsigned int);

  // i\o:
  void importFrostt(string, unsigned int, MPI_Comm);

};

// end of file
#endif
