// Header file for CPTensor compression routines


#ifndef CPTensorCompression_hpp
#define CPTensorCompression_hpp

#include "../CP/CPTensor.h"
#include "../CP/CPTensorOperations.hpp"
#include "../fullTensor/fullTensor.h"
#include "../fullTensor/fullTensorOperations.hpp"
#include "../sparseTensor/sparseTensor.h"
#include "../sparseTensor/sparseTensorOperations.hpp"
#include "../linAlg/linAlg.h"


class CPTensorCompression{
private:
  double m_tol;
  double m_relaxation;
  tensor* m_target;
  double m_resNorm;
  unsigned int m_verbosity;
  string m_saveName;

public:
  CPTensorCompression(){};
  ~CPTensorCompression(){};
  CPTensorCompression(tensor&, double);

  //-- ACCES FUNCTIONS: --
  inline double tol(){return m_tol;}
  inline double relaxation(){return m_relaxation;}
  inline tensor target(){return *m_target;}
  inline double resNorm(){return m_resNorm;}
  inline unsigned int verbosity(){return m_verbosity;}
  inline string saveName(){return m_saveName;}

  // -- SETTERS: --
  inline void set_tol(const double tolerance){m_tol = tolerance;}
  inline void set_relaxation(const double rel_par){m_relaxation = rel_par;}
  inline void set_target(tensor& tg){m_target = &tg;}
  inline void set_verbosity(unsigned int verb){m_verbosity = verb;}
  inline void set_saveName(string fileName){m_saveName = fileName;}

  // -- Methods of tensor compression: --

  /* greedy approximation */
  CPTensor greedy_ApproxOf(CPTensor& toBeApprox, vector<mat>& mass, const double tol, const double tolFP, const double alpha);


  /* methods for CP-TT */
  void compute_CPTT_Term(CPTensor&, vector<vec>&, double&);
  void compute_CPTT_Term(fullTensor&, vector<vec>&, double&);
  void compute_CPTT_Term(sparseTensor&, vector<vec>&, double&);
  CPTensor CPTT_ApproxOf(CPTensor&, double);
  CPTensor CPTT_ApproxOf(CPTensor&, double, bool);
  CPTensor CPTT_ApproxOf(fullTensor&, double);
  CPTensor CPTT_ApproxOf(sparseTensor&, double);

  /* from full to CP with ALS: */
  void compute_term(fullTensor&, vector<vec>&, unsigned int);
  void fix_point_ALS(fullTensor&, vector<vec>&, double);
  void update_residual(fullTensor&, vector<vec>&);
  CPTensor ALS(fullTensor&, double);

};






#endif
