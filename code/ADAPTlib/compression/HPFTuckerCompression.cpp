// Implementation of the class HPFTuckerCompression


#include "HPFTuckerCompression.hpp"


/* Constructor
  - input: a tensor to be set as the target and the tolerance
*/
HPFTuckerCompression::HPFTuckerCompression(tensor& tg, double tolerance){
  m_target = &tg;
  m_tol = tolerance;
}

/* constructor: given a full tensor and a node, construct the object:
  - input: full tensor, node = intial tree (with maximal partitioning)
  - output: construct
*/
HPFTuckerCompression::HPFTuckerCompression(fullTensor& tg, Node*& tree, double tolerance){
  m_target = &tg;
  m_node = tree;
  m_tol = tolerance;
  m_targetNorm = tg.tensorEntries().norm();


  // computing the indices map:
  m_leafs = m_node->getLeafs();
  const unsigned int nSub = m_leafs.size();
  m_subIndices.resize(nSub);
  for(unsigned int iSub=0; iSub<nSub; iSub++){
    vector<vector<unsigned int> > indLeaf = m_leafs[iSub]->indices();
    m_subIndices[iSub] = indLeaf;
  }

  m_subApprox.resize(nSub);

  // computing the local HOSVD compressions:
  for(unsigned int iSub=0; iSub<nSub; iSub++){
    cout << "ISUB = " << iSub << endl;
    // set to extract is tensorised, extract bounds:
    vector<unsigned int> indBounds(2*tg.nVar());
    for(unsigned int iVar=0; iVar<tg.nVar(); iVar++){
      unsigned int nOfInd = m_subIndices[iSub][iVar].size();
      indBounds[2*iVar] = m_subIndices[iSub][iVar][0];
      indBounds[2*iVar+1] = m_subIndices[iSub][iVar][nOfInd-1] + 1; // C convention
    }
    // extract subTensor:
    fullTensor subTensor = tg.extractSubtensor(indBounds);

    // compute the local approximation:
    m_subApprox[iSub].HOSVD(subTensor, true);
    subTensor.clear(); // the subtensor is cleared (not accessible)
  }

}


/* compute greedy repartition of the errors
  - input: the local approximation have been already computed (!!!)
  - output: list of terms to retain in each sub domain and the error in the leafs
*/
void HPFTuckerCompression::compute_greedy(){
  const unsigned int nSub = m_subApprox.size();
  const unsigned int m_nVar = m_subApprox[0].result().nVar();

  // table to check:
  m_tableToCheck.resize(nSub);
  for(unsigned int iSub=0; iSub<nSub; iSub++){
    m_tableToCheck[iSub].resize(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      m_tableToCheck[iSub][iVar] = 0;
    }
  }


  // compute the leafs error as HOSVD bound:
  m_leafsErrors.resize(nSub);
  double testSum = 0.0;
  for(unsigned int iSub=0; iSub<nSub; iSub++){
    m_leafsErrors[iSub] = 0.0;
    vector<vector<double> > singVals = m_subApprox[iSub].HOSVD_sigmas();
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      const unsigned int nMod = singVals[iVar].size();
      for(unsigned int iMod=0; iMod<nMod; iMod++){
        m_leafsErrors[iSub] += singVals[iVar][iMod] * singVals[iVar][iMod];
      }
    }
    testSum += m_leafsErrors[iSub];
    //cout << "iSub = " << iSub << " i_err = " << sqrt(m_leafsErrors[iSub]) << endl;
    m_leafsErrors[iSub] = sqrt(m_leafsErrors[iSub]);
  }


  // cycling and determine which terms must be retained:
  double err = sqrt(testSum);
  const double errTarget = m_tol * err;

  while(err > errTarget){
    // locate the largest singular value:
    unsigned int i_sub_star;
    unsigned int i_var_star;
    unsigned int i_mod_star;
    double sigma_star = 0.0;
    for(unsigned int iSub=0; iSub<nSub; iSub++){
      vector<vector<double> > singVals = m_subApprox[iSub].HOSVD_sigmas();
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        unsigned int iMod = m_tableToCheck[iSub][iVar];
        double sigma = 0.0;
        if( iMod < singVals[iVar].size() ){
          sigma = singVals[iVar][iMod];
        }
        if(sigma > sigma_star){
          sigma_star = sigma;
          i_sub_star = iSub;
          i_var_star = iVar;
        }
      }
    }

    // update the table toCheck:
    bool hasToInitTable = false;
    m_tableToCheck[i_sub_star][i_var_star] += 1;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(iVar != i_var_star){
        if(m_tableToCheck[i_sub_star][iVar] == 0){
          hasToInitTable = true;
          m_tableToCheck[i_sub_star][iVar] = 1;
        }
      }
    }

    // contribution of the retained sigmas on the error bound:
    /*double contrib = 0.0;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      for(unsigned int iMod=0; iMod<m_tableToCheck[i_sub_star][iVar]; iMod++){
        contrib += m_subApprox[i_sub_star].HOSVD_sigmas()[iVar][iMod] * m_subApprox[i_sub_star].HOSVD_sigmas()[iVar][iMod];
      }
    }*/

    // update the leafs error and total error:
    double toSubtract = sigma_star*sigma_star;
    if(hasToInitTable){
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        if(iVar != i_var_star){
          toSubtract += m_subApprox[i_sub_star].HOSVD_sigmas(iVar,0) * m_subApprox[i_sub_star].HOSVD_sigmas(iVar,0);
        }
      }
    }

    // check for roundoffs errors:
    if(m_leafsErrors[i_sub_star]*m_leafsErrors[i_sub_star] - toSubtract < 0.0){
      if(fabs(m_leafsErrors[i_sub_star]*m_leafsErrors[i_sub_star] - toSubtract)<1.0e-14){
        m_leafsErrors[i_sub_star] = 0.0;
      }
    }
    else{
      m_leafsErrors[i_sub_star] = sqrt(m_leafsErrors[i_sub_star]*m_leafsErrors[i_sub_star] - toSubtract);
    }

    err = sqrt(err*err - toSubtract);

    if (m_verbosity>1) {
      cout << "err = " <<  err << endl;
    }
  }

  if(m_verbosity>1){
    PetscPrintf(m_target->comm(), "Plotting ranks retained per subdomain:\n");
    for(unsigned int iSub=0; iSub< nSub; iSub++){
      PetscPrintf(m_target->comm(), "subdomain %d : ", iSub);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        PetscPrintf(m_target->comm(), " %d ", m_tableToCheck[iSub][iVar]);
      }
      PetscPrintf(m_target->comm(), "\n");
    }
  }

  // computing the retained approximations after greedy:
  for(unsigned int iSub=0; iSub<nSub; iSub++){
    vector<unsigned int> localRanks = m_tableToCheck[iSub];
    m_subApprox[iSub].compute_resultApproximation(localRanks);
  }


  // assigning total error:
  m_totalError = err;

  // computing the memory:
  m_totalMemory = 0;
  for(unsigned int iSub=0; iSub<nSub; iSub++){
    //cout << "iSub = " << iSub << " Leaf error = " << m_leafsErrors[iSub] << endl;
    // compute the memory of the modes:
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      // number of modes to retain:
      const unsigned int nRetained = m_tableToCheck[iSub][iVar];
      m_totalMemory += nRetained * m_subApprox[iSub].result().nDof_var(iVar);
    }
    // adding the core memory:
    unsigned int coreMemory = 1;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      coreMemory = coreMemory * m_tableToCheck[iSub][iVar];
    }
    m_totalMemory = m_totalMemory + coreMemory;
  }
}


/* optimise the hierarchical tree
  - input: the greedy algorithm has already been computed.
  - output: the tree is optimised
*/
void HPFTuckerCompression::optimise_tree(){

  m_leafs = m_node->getLeafs();
  // compile a list of parents:
  vector<Node*> parentList;
  for(unsigned int iSub=0; iSub<m_leafs.size(); iSub++){
    if(m_leafs[iSub]->hasParent()){
      Node* candidate = m_leafs[iSub]->parent();
      bool isAlreadyIn = false;
      for(unsigned int iPar=0; iPar<parentList.size(); iPar++){
        if(candidate == parentList[iPar]){
          isAlreadyIn = true;
          break;
        }
      }
      if(!isAlreadyIn){
        parentList.push_back(candidate);
      }
    }
  }


  unsigned int nOfPar = parentList.size();

  while(nOfPar>0){
    // compute the copy of the parents list:
    vector<Node*> parentsCopy(nOfPar);
    for(unsigned int iPar=0; iPar<nOfPar; iPar++){
      parentsCopy[iPar] = parentList[iPar];
    }

    // test the merging for all the parents in the list:
    vector<bool> answers(nOfPar);
    vector<TuckerCompression> listApprox(nOfPar);
    for(unsigned int iPar=0; iPar<nOfPar; iPar++){
      TuckerCompression parentApp;
      bool doMerge = shouldMerge(parentList[iPar], parentApp);
      answers[iPar] = doMerge;
      listApprox[iPar] = parentApp;
    }
    // remove the approximations in the list:
    for(unsigned int iPar=0; iPar<nOfPar; iPar++){
      if(answers[iPar]){
        vector<Node*> theseLeafs = parentList[iPar]->children();
        for(unsigned int iLeaf=0; iLeaf<theseLeafs.size(); iLeaf++){
          unsigned int iSub = m_node->findIdLeaf(theseLeafs[iLeaf]);
          m_subApprox[iSub].clear();
        }
      }
    }
    // compile the new list of Tucker approximations:
    vector<TuckerCompression> modifiedApprox;
    vector<double> modifiedLeafsErr;
    for(unsigned int iPar=0; iPar<nOfPar; iPar++){
      if(answers[iPar]){
        modifiedApprox.push_back(listApprox[iPar]);
        // computing the parent leaf error:
        double leafErr = 0.0;
        vector<Node*> theseLeafs = parentList[iPar]->children();
        for(unsigned int iLeaf=0; iLeaf<theseLeafs.size(); iLeaf++){
          unsigned int iSub = m_node->findIdLeaf(theseLeafs[iLeaf]);
          leafErr += m_leafsErrors[iSub] * m_leafsErrors[iSub];
        }
        modifiedLeafsErr.push_back(sqrt(leafErr));
      }
      else{
        vector<Node*> theseLeafs = parentList[iPar]->children();
        for(unsigned int iLeaf=0; iLeaf<theseLeafs.size(); iLeaf++){
          unsigned int iSub = m_node->findIdLeaf(theseLeafs[iLeaf]);
          modifiedApprox.push_back(m_subApprox[iSub]);
          modifiedLeafsErr.push_back(m_leafsErrors[iSub]);
        }
      }
    }
    m_subApprox.clear();
    m_subApprox.resize(0);
    m_subApprox = modifiedApprox;
    m_leafsErrors.clear();
    m_leafsErrors = modifiedLeafsErr;

    // computing the new error leafs:
    /*m_leafsErrors.clear();
    m_leafsErrors.resize(m_subApprox.size());
    for(unsigned int iSub=0; iSub<m_subApprox.size(); iSub++){
      vector<unsigned int> tuckerRanks = m_subApprox[iSub].resultApproximation().ranks();
      m_leafsErrors[iSub] = 0.0;
      for(unsigned int iVar=0; iVar<m_subApprox[iSub].result().nVar(); iVar++){
        for(unsigned int iMod=0; iMod<tuckerRanks[iVar]; iMod++){
          m_leafsErrors[iSub] += m_subApprox[iSub].HOSVD_sigmas(iVar,iMod) * m_subApprox[iSub].HOSVD_sigmas(iVar,iMod);
        }
      }
      m_leafsErrors[iSub] = sqrt(m_leafsErrors[iSub]);
    }
    */

    // remove the children from the parents to be merged:
    for(unsigned int iPar=0; iPar<nOfPar; iPar++){
      if(answers[iPar]){
        parentList[iPar]->removeChildren();
      }
    }
    m_leafs = m_node->getLeafs();
    const unsigned int nOfLeafs = m_leafs.size();

    // destroy the list of ancient parents.
    parentList.clear();
    parentList.resize(0);
    // compile a list of the parents to be tested:
    for(unsigned int iSub=0; iSub<nOfLeafs; iSub++){
      if(m_leafs[iSub]->hasParent()){
        Node* candidate = m_leafs[iSub]->parent();
        bool isAlreadyIn = false;
        for(unsigned int iPar=0; iPar<parentList.size(); iPar++){
          if(candidate == parentList[iPar]){
            isAlreadyIn = true;
            break;
          }
        }
        bool hasAlreadyBeenTested = false;
        for(unsigned int iOld=0; iOld<parentsCopy.size(); iOld++){
          if(candidate == parentsCopy[iOld]){
            hasAlreadyBeenTested = true;
            break;
          }
        }
        // if it has not grandchildren is optional
        if( (!isAlreadyIn) && (!hasAlreadyBeenTested) && (!candidate->hasGrandChildren() ) ){
          parentList.push_back(candidate);
        }
      }
    }
    nOfPar = parentList.size();
    if(m_verbosity>0){
      PetscPrintf(m_subApprox[0].result().comm(), "Number of parents to be tested: %d\n", nOfPar);
    }
  }

  //m_node->print_leafsTable();
}




/* Compute total error:
- square root of the sum of the squares of the leaf error
*/
void HPFTuckerCompression::compute_totalError(){
  m_totalError = 0.0;
  for(unsigned int iLeaf=0; iLeaf<m_leafsErrors.size(); iLeaf++){
    m_totalError += m_leafsErrors[iLeaf] * m_leafsErrors[iLeaf];
  }
  m_totalError = sqrt(m_totalError);
}


/* check if the approximations should be merged
  - input: the parent node
  - output: a bool
*/
bool HPFTuckerCompression::shouldMerge(Node* parent, TuckerCompression& mergedCompression){
  const unsigned int m_nVar = parent->nVar();
  MPI_Comm m_comm = m_target->comm();
  bool shouldIt = true;
  if(m_verbosity>1){parent->print_leafsTable();}

  // defining the list of the leafs
  vector<Node*> listOfLeafs = parent->getLeafs();
  const unsigned nOfLeafs = listOfLeafs.size();

  // computing error and error budget:
  double errLeafs = 0.0;
  double errorBudget = 0.0;
  unsigned int memoryLeafs = 0;
  for(unsigned int iLeaf = 0; iLeaf<nOfLeafs; iLeaf++){
    // determining the subdomain:
    unsigned int iSub = m_node->findIdLeaf(listOfLeafs[iLeaf]);
    if(m_verbosity>0){
      MPI_Comm localComm = m_subApprox[iSub].result().comm();
      PetscPrintf(localComm, "iLeaf = %d  err = %f \n", iLeaf, m_leafsErrors[iSub]);
    }
    //cout << "iLeaf = " << iLeaf << " err = " << m_leafsErrors[iSub] << endl;
    errLeafs += m_leafsErrors[iSub] * m_leafsErrors[iSub];
    //cout << "In the function: iSub = " << iSub << endl;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      unsigned int iMod = m_tableToCheck[iSub][iVar];
      if(!m_subApprox[iSub].resultApproximation().isEmpty()){
        vector<double> sigmas = m_subApprox[iSub].HOSVD_sigmas(iVar);
        if(iMod<sigmas.size()){
          errorBudget += m_subApprox[iSub].HOSVD_sigmas(iVar,iMod)*m_subApprox[iSub].HOSVD_sigmas(iVar,iMod);
        }
      }
    }
    if(!m_subApprox[iSub].resultApproximation().isEmpty()){
      unsigned int coreMem = 1;
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        coreMem *= m_subApprox[iSub].result().ranks(iVar);
        memoryLeafs += m_subApprox[iSub].resultApproximation().ranks(iVar) * m_subApprox[iSub].resultApproximation().nDof_var(iVar);
      }
      memoryLeafs += coreMem;
    }
  }
  errorBudget = sqrt(errorBudget);
  errLeafs = sqrt(errLeafs);
  if(m_verbosity>1){
    cout << "errLeafs = " << errLeafs << endl;
    cout << "errorBudget = " << errorBudget << endl;
    cout << endl;
  }


  // check if it is useful to merge or not:

  // 1 - compute the Tucker tensor containing the sub-approximations
  vector<unsigned int> indBounds(2*m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    indBounds[2*iVar] = UINT_MAX;
    indBounds[2*iVar+1] = 0;
  }
  // determining the bounds:
  vector<unsigned int> resPerDim(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    for(unsigned int iLeaf = 0; iLeaf<nOfLeafs; iLeaf++){
      vector<unsigned int> indLeaf_Var = listOfLeafs[iLeaf]->indices(iVar);
      const unsigned int nOfInd = indLeaf_Var.size();
      for(unsigned int iInd=0; iInd<nOfInd; iInd++){
        if(indLeaf_Var[iInd] < indBounds[2*iVar]){
          indBounds[2*iVar] = indLeaf_Var[iInd];
        }
        if(indLeaf_Var[iInd] >= indBounds[2*iVar+1]){
          indBounds[2*iVar+1] = indLeaf_Var[iInd] + 1; // C convention
        }
      }
    }
    resPerDim[iVar] = indBounds[2*iVar+1] - indBounds[2*iVar];
  }

  cout << "I'M HERE line 410\n";

  // Compute a Tucker rounding up to the error budget:
  //Tucker mergedApprox(resPerDim, m_comm);
  Tucker mergedApprox(resPerDim, m_comm);
  vector<vector<vec> > subModes(m_nVar);
  for(unsigned int iLeaf=0; iLeaf<nOfLeafs; iLeaf++){
    // identify the leaf globally:
    unsigned int iSub = m_node->findIdLeaf(listOfLeafs[iLeaf]);
    Tucker subTens = m_subApprox[iSub].resultApproximation();
    cout << "iLeaf = " << iLeaf << " corresponding to iSub = " << iSub;
    if(subTens.isEmpty()){
      cout << " ... and it is empty\n";
    }
    else{
      cout << "  ranks = " <<subTens.ranks(0) << " x " <<subTens.ranks(1) << " x " << subTens.ranks(2) << endl;
    }
    // if the approximation is not the zero function:
    if(!subTens.isEmpty()){

      vector<unsigned int> relBounds(2*m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        relBounds[2*iVar] = UINT_MAX;
        relBounds[2*iVar+1] = 0;
      }

      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        vector<unsigned int> globInd = listOfLeafs[iLeaf]->indices(iVar);
        for(unsigned int iInd=0; iInd<globInd.size(); iInd++){
          unsigned int relInd = globInd[iInd] - indBounds[2*iVar];
          if(relInd<relBounds[2*iVar]){
            relBounds[2*iVar] = relInd;
          }
          if(relInd>=relBounds[2*iVar+1]){
            relBounds[2*iVar+1] = relInd + 1;
          }
        }
      }

      // computing the useful approximation to get the error found by the greedy + budget:
      vector<unsigned int> boundsCore(2*m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        unsigned int nToRetain = subTens.ranks(iVar) > m_tableToCheck[iSub][iVar] ? m_tableToCheck[iSub][iVar] : subTens.ranks(iVar);
        boundsCore[2*iVar] = 0;
        boundsCore[2*iVar+1] = nToRetain; //C notation
        subModes[iVar].resize(nToRetain);
        for(unsigned int iMod = 0; iMod<nToRetain; iMod++){
          subModes[iVar][iMod] = subTens.modes(iVar,iMod);
        }
      }
      fullTensor subCore = subTens.core().extractSubtensor(boundsCore);
      Tucker toMerge(subCore, subModes);
      mergedApprox.appendSubtensor(relBounds, toMerge);
    }
  }
  //mergedApprox.print();
  //mergedApprox.core().print();
  //mergedApprox.core().tensorEntries().print();

  if(!mergedApprox.isEmpty()){
    mergedApprox.recompress(errorBudget);
    //mergedApprox.core().print();
    //mergedApprox.core().tensorEntries().print();

    // compute the memory:
    unsigned int mergedMemory = 0;
    unsigned int coreMem = 1;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      coreMem *= mergedApprox.ranks(iVar);
      mergedMemory += mergedApprox.ranks(iVar) * mergedApprox.nDof_var(iVar);
    }
    mergedMemory += coreMem;

    // compare:
    cout << "Leafs momory = " << memoryLeafs << " Merged memory = " << mergedMemory << endl;
    if(memoryLeafs < mergedMemory){
      shouldIt = false;
    }
  }

  // provide the parent approximation if it is advantageous
  if(!shouldIt){
    mergedApprox.clear();
  }else{
    mergedCompression.set_result(mergedApprox);
    mergedCompression.set_resultApproximation(mergedApprox);
    mergedCompression.set_HOSVD_modes(subModes);
    vector<vector<double> > sigTable = mergedApprox.sigmaTable();
    mergedCompression.set_HOSVD_sigmas(sigTable);
    mergedCompression.set_tol(errLeafs);
  }

  return shouldIt;
}
