// Header file for Tucker compression routines


#ifndef TuckerCompression_hpp
#define TuckerCompression_hpp

#include "CPTensor.h"
#include "CPTensorOperations.hpp"
#include "fullTensor.h"
#include "fullTensorOperations.hpp"
#include "sparseTensor.h"
#include "Tucker.h"
#include "sparseTensorOperations.hpp"
#include "svd.h"
#include "eigenSolver.h"
#include "linearSolver.h"


class TuckerCompression{
private:
  double m_tol;
  double m_relaxation;
  tensor* m_target;
  double m_resNorm;
  unsigned int m_verbosity;
  string m_saveName;
  Tucker m_result;
  Tucker m_resultApproximation;
  vector<vector<vec> > m_HOSVD_modes;
  vector<vector<double> > m_HOSVD_sigmas;

public:
  TuckerCompression(){};
  ~TuckerCompression(){};
  TuckerCompression(tensor&, double);

  void clear(){
    m_HOSVD_sigmas.clear();
    for(unsigned int iVar=0; iVar<m_HOSVD_modes.size(); iVar++){
      for(unsigned int iMod=0; iMod<m_HOSVD_modes[0].size(); iMod++){
        m_HOSVD_modes.clear();
      }
    }
    if(!m_result.isEmpty()){
      m_result.clear();
    }
  }


  //-- ACCES FUNCTIONS: --
  inline double tol(){return m_tol;}
  inline double relaxation(){return m_relaxation;}
  inline tensor target(){return *m_target;}
  inline double resNorm(){return m_resNorm;}
  inline unsigned int verbosity(){return m_verbosity;}
  inline string saveName(){return m_saveName;}
  inline Tucker result(){return m_result;}
  inline vector<vector<vec> > HOSVD_modes(){return m_HOSVD_modes;}
  inline vector<vec> HOSVD_modes(unsigned int iVar){return m_HOSVD_modes[iVar];}
  inline vec HOSVD_modes(unsigned int iVar, unsigned int iMod){return m_HOSVD_modes[iVar][iMod];}
  inline vector<vector<double> > HOSVD_sigmas(){return m_HOSVD_sigmas;}
  inline vector<double> HOSVD_sigmas(unsigned int iVar){return m_HOSVD_sigmas[iVar];}
  inline double HOSVD_sigmas(unsigned int iVar, unsigned int iMod){return m_HOSVD_sigmas[iVar][iMod];}
  inline Tucker resultApproximation(){return m_resultApproximation;}

  // -- SETTERS: --
  inline void set_tol(const double tolerance){m_tol = tolerance;}
  inline void set_relaxation(const double rel_par){m_relaxation = rel_par;}
  inline void set_target(tensor& tg){m_target = &tg;}
  inline void set_verbosity(unsigned int verb){m_verbosity = verb;}
  inline void set_saveName(string fileName){m_saveName = fileName;}
  inline void set_result(Tucker& tens){m_result = tens;}
  inline void set_HOSVD_sigmas(vector<vector<double> >& singValsTable){m_HOSVD_sigmas = singValsTable;}
  inline void set_HOSVD_modes(vector<vector<vec> >& modesTable){m_HOSVD_modes = modesTable;}
  inline void set_resultApproximation(Tucker& tens){m_resultApproximation = tens;}

  // -- Methods of tensor compression: --

  /* HOSVD */
  Tucker HOSVD(CPTensor&, double tol);
  void HOSVD(CPTensor&, bool);
  Tucker HOSVD(fullTensor&, double tol);
  void HOSVD(fullTensor&, bool);
  Tucker HOSVD(sparseTensor&, double tol);
  void HOSVD(sparseTensor&, bool);
  void HOSVD(tensor&);
  void compute_resultApproximation(vector<unsigned int>);

};






#endif
