// Header file for HPFTucker compression routines


#ifndef HPFTuckerCompression_hpp
#define HPFTuckerCompression_hpp

#include "Tucker.h"
#include "HPFTucker.h"
#include "sparseTensorOperations.hpp"
#include "TuckerCompression.hpp"
#include "svd.h"
#include "eigenSolver.h"
#include "linearSolver.h"


class HPFTuckerCompression{
  double m_tol;
  double m_relaxation;
  tensor* m_target;
  double m_resNorm;
  unsigned int m_verbosity;
  string m_saveName;
  HPFTucker m_result;
  double m_targetNorm;

  vector<double> m_leafsErrors;
  vector<TuckerCompression> m_subApprox;
  vector<vector<vector<unsigned int> > > m_subIndices;
  vector<vector<unsigned int> > m_tableToCheck;
  double m_totalError;
  unsigned int m_totalMemory;
  Node* m_node;
  vector<Node*> m_leafs;

public:
  HPFTuckerCompression(){};
  ~HPFTuckerCompression(){};
  HPFTuckerCompression(tensor&, double);
  HPFTuckerCompression(fullTensor&, Node*&, double);
  void clear(){
    for(unsigned int iSub=0; iSub<m_subApprox.size(); iSub++){
      m_subApprox[iSub].clear();
    }
  }


  //-- ACCES FUNCTIONS: --
  inline double tol(){return m_tol;}
  inline double relaxation(){return m_relaxation;}
  inline tensor target(){return *m_target;}
  inline double resNorm(){return m_resNorm;}
  inline unsigned int verbosity(){return m_verbosity;}
  inline string saveName(){return m_saveName;}
  inline HPFTucker result(){return m_result;}
  inline vector<double> leafsError(){return m_leafsErrors;}
  inline double leafsError(unsigned int iLeaf){return m_leafsErrors[iLeaf];}
  inline double totalError(){return m_totalError;}
  inline Node* node(){return m_node;}
  inline vector<Node*> leafs(){return m_leafs;}
  inline Node* leafs(unsigned int iSub){return m_leafs[iSub];}
  inline vector<vector<vector<unsigned int> > > subIndices(){return m_subIndices;}
  inline vector<vector<unsigned int > > subIndices(unsigned int iSub){return m_subIndices[iSub];}
  inline vector<unsigned int> subIndices(unsigned int iSub, unsigned int iVar){return m_subIndices[iSub][iVar];}
  inline unsigned int subIndices(unsigned int iSub, unsigned int iVar, unsigned int iInd){return m_subIndices[iSub][iVar][iInd];}
  inline double targetNorm(){return m_targetNorm;}
  inline unsigned int totalMemory(){return m_totalMemory;}
  inline vector<TuckerCompression> subApprox(){return m_subApprox;}
  inline TuckerCompression subApprox(unsigned int iSub){return m_subApprox[iSub];}


  // -- SETTERS: --
  inline void set_tol(const double tolerance){m_tol = tolerance;}
  inline void set_relaxation(const double rel_par){m_relaxation = rel_par;}
  inline void set_target(tensor& tg){m_target = &tg;}
  inline void set_verbosity(unsigned int verb){m_verbosity = verb;}
  inline void set_saveName(string fileName){m_saveName = fileName;}

  // -- Methods of tensor compression: --
  void compute_greedy();
  void optimise_tree();

  // -- Auxiliary Methods --
  void compute_totalError();
  bool shouldMerge(Node*, TuckerCompression&); 


};

#endif
