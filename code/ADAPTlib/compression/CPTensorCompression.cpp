// Implementation of the class CPTensorCompression

#include "CPTensorCompression.hpp"


/* Constructor
  - input: a tensor to be set as the target and the tolerance
*/
CPTensorCompression::CPTensorCompression(tensor& tg, double tolerance){
  m_target = &tg;
  m_tol = tolerance;
}



/* greedy to perform approximation of a given CP up to tol, relaxed FP (!)
  - inputs: the tensor to be approximated, the mass matrices, the error, the relaxation parameter
  - output: this tensor is the greedy approximation of the given target CP
*/
/*CPTensor CPTensorCompression::greedy_ApproxOf(CPTensor& toBeApprox, vector<mat>& mass, const double tol, const double tolFP = 1.0e-2, const double alpha=1.0){

  MPI_Comm actualComm = toBeApprox.comm();

  // define tolerance for Fix Point
  const double errFP = tolFP * tol;
  const unsigned int m_nVar = toBeApprox.nVar();

  vector<unsigned int> dofPerDim(toBeApprox.nVar());
  for(unsigned int idVar=0; idVar<toBeApprox.nVar(); idVar++){
    dofPerDim[idVar] = toBeApprox.nDof_var(idVar);
  }
  CPTensor result(dofPerDim, actualComm);
  unsigned int m_rank = 0;

  // init linear solvers:
  vector<linearSolver>linSol(m_nVar);
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    linSol[idVar].init(mass[idVar], KSPGMRES);
  }
  // init residual
  CPTensor residual;
  residual.copyTensorFrom(toBeApprox);
  double normRes = residual.norm2CP(mass);

  while(sqrt(normRes) > tol){
    PetscPrintf(actualComm,"greedy residual rank = %d\n", residual.rank());
    PetscPrintf(actualComm,"greedy residual norm = %f\n", residual.norm2CP(mass));

    // init random terms
    vector<vec> termsToAdd(m_nVar);
    PetscRandom rctx;
    PetscRandomCreate(actualComm,&rctx);
    PetscRandomSetSeed(rctx,std::chrono::system_clock::now().time_since_epoch().count());
    PetscRandomSeed(rctx);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      termsToAdd[idVar].init(dofPerDim[idVar], actualComm);
      termsToAdd[idVar].finalize();
      VecSetRandom(termsToAdd[idVar].x(),rctx);
    }

    // copy for fix point check
    vector<vec> termsToAdd_old(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      vec thisTerm;
      thisTerm.copyVecFrom(termsToAdd[idVar]);
      termsToAdd_old[idVar] = thisTerm;
    }

    // starting Fix-Point for ALS iteration
    double testFP = 1e12*errFP;
    while(testFP > errFP){
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        double prod = 1.0;
        for(unsigned int jVar=0; jVar<m_nVar; jVar++){
          if(jVar != iVar){
            vec prodJ = mass[jVar] * termsToAdd[jVar];
            double quadForm = scalProd(prodJ, termsToAdd[jVar]);
            prod = prod * quadForm;
            prodJ.clear();
          }
        }

        vec rhs(dofPerDim[iVar], actualComm);
        rhs.finalize();
        for(unsigned int kTerm=0; kTerm<residual.rank(); kTerm++){
          double prodK = 1.0;
          for(unsigned int jVar=0; jVar<m_nVar; jVar++){
            if(jVar != iVar){
              vec prodJ = mass[jVar] * termsToAdd[jVar];
              vec resT_kj = residual.terms(kTerm,jVar);
              double quadForm = scalProd(prodJ, resT_kj);
              prodK = prodK * quadForm;
              prodJ.clear();
            }
          }
          vec prodVecI = mass[iVar] * residual.terms(kTerm,iVar);
          prodK = prodK * residual.coeffs(kTerm);
          rhs.sum(prodVecI, prodK);
          prodVecI.clear();
        }
        rhs *= (1.0/prod);

        // solve linear System
        linSol[iVar].solve(rhs);
        vec tmpSol = linSol[iVar].sol();
        tmpSol *= alpha;
        termsToAdd[iVar] *= (1.0 - alpha);
        termsToAdd[iVar] += tmpSol;

        rhs.clear();
        tmpSol.clear();
      }
      CPTensor thisPureTens(termsToAdd, actualComm);
      CPTensor oldPureTens(termsToAdd_old, actualComm);
      oldPureTens.sum(thisPureTens, -1.0, true);
      double diffNorm2 = oldPureTens.norm2CP(mass);

      // copy termsToAdd
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        termsToAdd_old[idVar].clear();
        termsToAdd_old[idVar].copyVecFrom(termsToAdd[idVar]);
        termsToAdd_old[idVar].finalize();
      }
      testFP = sqrt(diffNorm2);
      PetscPrintf(actualComm, "testFP = %e \n", testFP);
    }
    PetscPrintf(actualComm, "\n", testFP);

    // update the result:
    result.addPureTensorTerm(termsToAdd, 1.0);
    PetscPrintf(actualComm,"Sol rank = %d\n", result.rank());

    // update residual:

    residual.addPureTensorTerm(termsToAdd, -1.0);

    PetscRandomDestroy(&rctx);

    normRes = residual.norm2CP(mass);
    PetscPrintf(actualComm, "normRes = %e \n", normRes);
    PetscPrintf(actualComm, "\n", testFP);
    PetscPrintf(actualComm, "\n", testFP);

    m_rank = result.rank();

    // optimise the coefficients:
    mat scalMat(m_rank, m_rank, actualComm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      vector<vec> I_terms = result.terms(iTerm);
      for(unsigned int jTerm=iTerm; jTerm<m_rank; jTerm++){
        vector<vec> J_terms = result.terms(jTerm);
        double val = result.scalarProdTerms(I_terms, J_terms, mass);
        scalMat.setMatEl(iTerm, jTerm, val);
        if(iTerm != jTerm){
          scalMat.setMatEl(jTerm, iTerm, val);
        }
      }
    }
    scalMat.finalize();

    vec scalVec(m_rank, actualComm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double entry = 0.0;
      vector<vec> I_terms = result.terms(iTerm);
      for(unsigned int kTerm=0; kTerm<toBeApprox.rank(); kTerm++){
        vector<vec> kTermVec = toBeApprox.terms(kTerm);
        double val = result.scalarProdTerms(I_terms, kTermVec, mass);
        val = val * toBeApprox.coeffs(kTerm);
        entry = entry + val;
      }
      scalVec.setVecEl(iTerm, entry);
    }
    scalVec.finalize();

    linearSolver coeffOptim(scalMat, scalVec, KSPGMRES);
    vec updateCoeffs = coeffOptim.sol();

    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      result.set_coeffs(iTerm, updateCoeffs.getVecEl(iTerm));
      residual.set_coeffs(residual.rank() - m_rank + iTerm, -1.0*updateCoeffs.getVecEl(iTerm));
    }

    // free the memory:
    scalVec.clear();
    updateCoeffs.clear();
    scalMat.clear();
    coeffOptim.clear();

    normRes = residual.norm2CP(mass);
    PetscPrintf(actualComm, "Corrected normRes = %e \n", normRes);
    PetscPrintf(actualComm, "\n", testFP);
    PetscPrintf(actualComm, "\n", testFP);
  }
  // destroy solvers
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    linSol[idVar].clear();
  }

  result.finalize();
  return result;
}*/


/* greedy to perform approximation of a given CP up to tol, relaxed FP (!)
  - inputs: the tensor to be approximated, the mass matrices, the error, the relaxation parameter
  - output: this tensor is the greedy approximation of the given target CP
*/
CPTensor CPTensorCompression::greedy_ApproxOf(CPTensor& toBeApprox, vector<mat>& mass, const double tol, const double tolFP = 1.0e-2, const double alpha=1.0){
  unsigned int m_nVar = toBeApprox.nVar();
  vector<unsigned int> m_nDof_var = toBeApprox.nDof_var();
  MPI_Comm m_comm = toBeApprox.comm();

  CPTensor sol(m_nDof_var, m_comm);

  // define tolerance for Fix Point and set the rank;
  const double errFP = tolFP * tol;
  unsigned int m_rank = 0;

  // init linear solvers:
  vector<linearSolver>linSol(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    linSol[iVar].init(mass[iVar], KSPGMRES);
  }

  // init residual:
  CPTensor residual;
  residual.copyTensorFrom(toBeApprox);
  double normRes = sqrt(residual.norm2CP(mass));

  // Start iterating:
  unsigned int it = 0;
  while(normRes > tol){
    // printing the residual rank:
    if(m_verbosity > 1){
      PetscPrintf(m_comm,"residual rank = %d\n", residual.rank());
      PetscPrintf(m_comm,"residual norm = %f\n", residual.norm2CP(mass));
    }

    // init random terms:
    vector<vec> termsToAdd(m_nVar);
    PetscRandom rctx;
    PetscRandomCreate(m_comm,&rctx);
    PetscRandomSetSeed(rctx,std::chrono::system_clock::now().time_since_epoch().count());
    PetscRandomSeed(rctx);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      Vec thisTerm;
      initVec(thisTerm, m_nDof_var[idVar], m_comm);
      finalizeVec(thisTerm);
      VecSetRandom(thisTerm,rctx);
      termsToAdd[idVar].setVector(thisTerm);
      termsToAdd[idVar].setComm(m_comm);
    }


    // copy for fix point check
    vector<vec> termsToAdd_old(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      termsToAdd_old[iVar].copyVecFrom(termsToAdd[iVar]);

    }
    // starting Fix-Point for ALS iteration:
    double testFP = 1e12*errFP;
    while(testFP > errFP){
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        double prod = 1.0;
        for(unsigned int jVar=0; jVar<m_nVar; jVar++){
          if(jVar != iVar){
            vec prodJ = mass[jVar] * termsToAdd[jVar];
            double quadForm = scalProd(prodJ, termsToAdd[jVar]);
            prod = prod * quadForm;
            prodJ.clear();
          }
        }
        // define the rhs:
        vec rhs(m_nDof_var[iVar], m_comm);
        rhs.finalize();
        for(unsigned int kTerm=0; kTerm<residual.rank(); kTerm++){
          double prodK = 1.0;
          for(unsigned int jVar=0; jVar<m_nVar; jVar++){
            if(jVar != iVar){
              vec prodJ = mass[jVar] *  termsToAdd[jVar];
              vec T_kj = residual.terms(kTerm,jVar);
              double quadForm = scalProd(prodJ, T_kj);
              prodK = prodK * quadForm;
              prodJ.clear();
            }
          }
          vec prodVecI = mass[iVar] * residual.terms(kTerm,iVar);

          prodK = prodK * residual.coeffs(kTerm);
          rhs.sum(prodVecI, prodK);
          prodVecI.clear();
        }
        // rescale the rhs:
        rhs *= (1.0/prod);

        // solve linear System
        linSol[iVar].solve(rhs);
        vec tmpSol = linSol[iVar].sol();

        // update:
        termsToAdd[iVar] *= (1.0 - alpha);
        termsToAdd[iVar].sum(tmpSol, alpha);

        // free the memory:
        rhs.clear();
        tmpSol.clear();
      }

      // Computing the error in the norm induced by mass:
      CPTensor thisPureTens(termsToAdd, m_comm);
      CPTensor oldPureTens(termsToAdd_old, m_comm);
      oldPureTens.sum(thisPureTens, -1.0, false);
      testFP = sqrt(oldPureTens.norm2CP(mass));


      // Copy termsToAdd into termsToAdd_old:
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        termsToAdd_old[idVar].clear();
        termsToAdd_old[idVar].copyVecFrom(termsToAdd[idVar]);
      }

      // Print fix point error:
      if(m_verbosity > 1){
        PetscPrintf(m_comm, "testFP = %e \n", testFP);
      }
    }

    if(m_verbosity > 1){PetscPrintf(m_comm, "\n", testFP);}

    // update the result:
    sol.addPureTensorTerm(termsToAdd, 1.0);
    m_rank += 1;
    PetscPrintf(m_comm,"Sol rank = %d\n", sol.rank());

    residual.addPureTensorTerm(termsToAdd, -1.0);

    PetscRandomDestroy(&rctx);

    normRes = sqrt(residual.norm2CP(mass));

    if(m_verbosity>1){
      PetscPrintf(m_comm, "It: %d, normRes = %e \n", it, normRes);
      PetscPrintf(m_comm, "\n", testFP);
      PetscPrintf(m_comm, "\n", testFP);
    }

    // optimise the coefficients:
    mat scalMat(m_rank, m_rank, m_comm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      vector<vec> I_term = sol.terms(iTerm);
      for(unsigned int jTerm=iTerm; jTerm<m_rank; jTerm++){
        vector<vec> J_term = sol.terms(jTerm);
        double val = scalarProdTerms(I_term, J_term, mass);
        scalMat.setMatEl(iTerm,jTerm,val);
        if(iTerm != jTerm){
          scalMat.setMatEl(jTerm,iTerm,val);
        }
      }
    }
    scalMat.finalize();

    // rhs for the optimisation of the coefficients:
    vec scalVec(m_rank, m_comm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double entry = 0.0;
      vector<vec> I_term = sol.terms(iTerm);
      for(unsigned int kTerm=0; kTerm<toBeApprox.rank(); kTerm++){
        vector<vec> kTermVec = toBeApprox.terms(kTerm);
        double val = scalarProdTerms(I_term, kTermVec, mass);
        val = val * toBeApprox.coeffs(kTerm);
        entry = entry + val;
      }
      scalVec.setVecEl(iTerm, entry);
    }
    scalVec.finalize();

    // solve the linear system and update:
    vec updateCoeffs = scalMat / scalVec;
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double thisCoeff = updateCoeffs(iTerm);
      sol.set_coeffs(iTerm, thisCoeff);
      residual.set_coeffs(residual.rank() - m_rank + iTerm, -1.0*thisCoeff);
    }

    // free the memory:
    scalVec.clear();
    updateCoeffs.clear();
    scalMat.clear();


    normRes = sqrt(residual.norm2CP(mass));

    if(m_verbosity > 0){
      PetscPrintf(m_comm, "It: %d, norm of the Residual = %e \n", it, normRes);
      PetscPrintf(m_comm, "\n", testFP);
      PetscPrintf(m_comm, "\n", testFP);
    }

    it += 1;
  }
  // destroy solvers
  for(unsigned int idVar=0; idVar<m_nVar; idVar++){
    linSol[idVar].clear();
  }

  return sol;
}




/* -- METHODS FOR CP-TT compression -- */


/* Compute one CP-TT term
  - input: the residual in CP format
  - output: the term (unitary norm), and its associated coefficient
*/
void CPTensorCompression::compute_CPTT_Term(CPTensor& residual, vector<vec>& theTerm, double& coeff){
  const unsigned int m_nVar = residual.nVar();
  MPI_Comm m_comm = residual.comm();
  const vector<unsigned int> m_nDof_var = residual.nDof_var();

  theTerm.resize(m_nVar);
  vector<unsigned int>indicesUnfolding(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    indicesUnfolding[iVar] = iVar;
  }

  CPTensor W;
  for(unsigned int iFun=0; iFun<m_nVar-2; iFun++){
    // compute 1-term svd for all the unfoldings m_nVar-iFun == indicesUnfolding.size();
    vector<double> singValues(m_nVar-iFun);
    vector<vec> modes(m_nVar-iFun);

    for(unsigned int count=0; count<m_nVar-iFun; count++){
       unsigned int iUnfold = indicesUnfolding[count];

       double sigma;
       vec u;
       if(iFun==0){
         residual.compute_Unfolding_OneSvdTerm(iUnfold, sigma, u);
       }
       else{
         W.compute_Unfolding_OneSvdTerm(count, sigma, u);
       }

       modes[count] = u;
       singValues[count] = sigma;
    }

    // ranking of singular values:
    unsigned int iBest=0;
    double largestSingVal = singValues[0];
    for(unsigned int iVal=1; iVal<singValues.size(); iVal++){
      if(singValues[iVal]>largestSingVal){
        iBest = iVal;
        largestSingVal = singValues[iVal];
      }
    }
    unsigned int bestIndex = indicesUnfolding[iBest];
    theTerm[bestIndex] = modes[iBest];


    // update the set of indices
    vector<unsigned int> oldIndices(m_nVar-iFun);
    for(unsigned int iInd=0; iInd<m_nVar-iFun; iInd++){
      oldIndices[iInd] = indicesUnfolding[iInd];
    }
    indicesUnfolding.clear();
    indicesUnfolding.resize(m_nVar-iFun-1);
    unsigned int cc = 0;
    for(unsigned int iInd=0; iInd<m_nVar-iFun; iInd++){
      if(oldIndices[iInd] != bestIndex){
        indicesUnfolding[cc] = oldIndices[iInd];
        cc = cc + 1;
      }
    }

    // free the memory;
    for(unsigned int iInd=0;iInd<m_nVar-iFun; iInd++){
      if(iInd != iBest){
        modes[iInd].clear();
      }
    }

    /*  Compute W = int_i residual u(x_i) dx_i
      - inputs: the vector w = sigma*v^T
      - output: the pure tensor term
    */
    if(iFun==0){
      residual.modeIContraction(iBest, theTerm[bestIndex], W);
    }
    else{
      W.inplaceModeIContraction(iBest, theTerm[bestIndex]);
    }
  }
  // the last term:

  double sigma;
  vec u;
  W.compute_Unfolding_OneSvdTerm(0, sigma, u);
  unsigned int bestIndex_0=indicesUnfolding[0];
  unsigned int bestIndex_1=indicesUnfolding[1];

  // computing v:
  vec v(m_nDof_var[bestIndex_1], m_comm);
  v.finalize();
  for(unsigned int iTerm=0; iTerm<W.rank(); iTerm++){
    vec term = W.terms(iTerm,0);
    double scalar = scalProd(term, u);
    double a_i = W.coeffs(iTerm) / sigma;
    a_i = a_i * scalar;
    v.sum(W.terms(iTerm,1), a_i);
  }

  theTerm[bestIndex_0]=u;
  theTerm[bestIndex_1]=v;

  coeff=sigma;
  W.clear();
}


/*  Compute one term of the CPTT algorithm
  - inputs: the residual in full tensor format
  - output: the pure tensor term
*/
void CPTensorCompression::compute_CPTT_Term(fullTensor& residual, vector<vec>& theTerm, double& coeff){
  unsigned int m_nVar = residual.nVar();
  MPI_Comm m_comm = residual.comm();
  vector<unsigned int> m_nDof_var = residual.nDof_var();

  theTerm.resize(m_nVar);
  vector<unsigned int>indicesUnfolding(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    indicesUnfolding[iVar] = iVar;
  }

  fullTensor W;
  for(unsigned int iFun=0; iFun<m_nVar-2; iFun++){
    // compute 1-term svd for all the unfoldings m_nVar-iFun == indicesUnfolding.size();
    vector<double> singValues(m_nVar-iFun);
    vector<vec> modes(m_nVar-iFun);
    vector<vec> v_modes(m_nVar-iFun);

    for(unsigned int count=0; count<m_nVar-iFun; count++){
       unsigned int iUnfold = indicesUnfolding[count];
       mat theIthUnfold;
       if(iFun==0){
         residual.computeUnfolding(iUnfold, theIthUnfold);
       }
       else{
         //Reshape w=sigma*v
         W.computeUnfolding(count, theIthUnfold);
       }

       svd firstTriplet;
       firstTriplet.oneSVDTerm(theIthUnfold);
       double sigma = firstTriplet.sigma();
       modes[count].copyVecFrom(firstTriplet.u());
       v_modes[count].copyVecFrom(firstTriplet.v());
       singValues[count] = sigma;

       theIthUnfold.clear();
       firstTriplet.clear();
    }
    // ranking of singular values:
    unsigned int iBest=0;
    double largestSingVal = singValues[0];
    for(unsigned int iVal=1; iVal<singValues.size(); iVal++){
      if(singValues[iVal]>largestSingVal){
        iBest = iVal;
        largestSingVal = singValues[iVal];
      }
    }
    unsigned int bestIndex = indicesUnfolding[iBest];
    theTerm[bestIndex] = modes[iBest];

    // update the set of indices
    vector<unsigned int> oldIndices(m_nVar-iFun);
    for(unsigned int iInd=0; iInd<m_nVar-iFun; iInd++){
      oldIndices[iInd] = indicesUnfolding[iInd];
    }
    indicesUnfolding.clear();
    indicesUnfolding.resize(m_nVar-iFun-1);
    unsigned int cc = 0;
    for(unsigned int iInd=0; iInd<m_nVar-iFun; iInd++){
      if(oldIndices[iInd] != bestIndex){
        indicesUnfolding[cc] = oldIndices[iInd];
        cc = cc + 1;
      }
    }

    // free the memory;
    for(unsigned int iInd=0;iInd<m_nVar-iFun; iInd++){
      if(iInd != iBest){
        modes[iInd].clear();
        v_modes[iInd].clear();
      }
    }

    /*  Reshape w = sigma*v^T of the CPTT algorithm
      - inputs: the vector w = sigma*v^T
      - output: the pure tensor term
    */
    vec w = v_modes[iBest];
    w *= largestSingVal;

    std::vector<unsigned int> resPerDim(indicesUnfolding.size());
    for (unsigned int iVar = 0; iVar < indicesUnfolding.size(); iVar++) {
      resPerDim[iVar]=m_nDof_var[indicesUnfolding[iVar]];
    }

    // tensor W:
    W.init(resPerDim, m_comm);
    W.set_tensorEntries(w);
    W.finalize();

 }
  // the last term:

mat lastUnfold;
W.computeUnfolding(0,lastUnfold);

svd firstTriplet;
firstTriplet.oneSVDTerm(lastUnfold);
double sigma = firstTriplet.sigma();
unsigned int bestIndex_0=indicesUnfolding[0];
unsigned int bestIndex_1=indicesUnfolding[1];

theTerm[bestIndex_0].copyVecFrom(firstTriplet.u());
theTerm[bestIndex_1].copyVecFrom(firstTriplet.v());
coeff=sigma;
firstTriplet.clear();
}


/*  Compute one term of the CPTT algorithm
  - inputs: the residual in sparse tensor format
  - output: the pure tensor term
*/
void CPTensorCompression::compute_CPTT_Term(sparseTensor& residual, vector<vec>& theTerm, double& coeff){
  unsigned int m_nVar = residual.nVar();
  MPI_Comm m_comm = residual.comm();
  vector<unsigned int> m_nDof_var = residual.nDof_var();

  theTerm.resize(m_nVar);
  vector<unsigned int>indicesUnfolding(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    indicesUnfolding[iVar] = iVar;
  }

  sparseTensor W;
  for(unsigned int iFun=0; iFun<m_nVar-2; iFun++){
    // compute 1-term svd for all the unfoldings m_nVar-iFun == indicesUnfolding.size();
    vector<double> singValues(m_nVar-iFun);
    vector<vec> modes(m_nVar-iFun);
    vector<vec> v_modes(m_nVar-iFun);

    for(unsigned int count=0; count<m_nVar-iFun; count++){
       unsigned int iUnfold = indicesUnfolding[count];
       mat theIthUnfold;
       if(iFun==0){
         residual.computeUnfolding(iUnfold, theIthUnfold);
       }
       else{
         //Reshape w=sigma*v
         W.computeUnfolding(count, theIthUnfold);
       }

       svd firstTriplet;
       firstTriplet.oneSVDTerm(theIthUnfold);
       double sigma = firstTriplet.sigma();
       modes[count].copyVecFrom(firstTriplet.u());
       v_modes[count].copyVecFrom(firstTriplet.v());
       singValues[count] = sigma;

       theIthUnfold.clear();
       firstTriplet.clear();
    }
    // ranking of singular values:
    unsigned int iBest=0;
    double largestSingVal = singValues[0];
    for(unsigned int iVal=1; iVal<singValues.size(); iVal++){
      if(singValues[iVal]>largestSingVal){
        iBest = iVal;
        largestSingVal = singValues[iVal];
      }
    }
    unsigned int bestIndex = indicesUnfolding[iBest];
    theTerm[bestIndex] = modes[iBest];

    // update the set of indices
    vector<unsigned int> oldIndices(m_nVar-iFun);
    for(unsigned int iInd=0; iInd<m_nVar-iFun; iInd++){
      oldIndices[iInd] = indicesUnfolding[iInd];
    }
    indicesUnfolding.clear();
    indicesUnfolding.resize(m_nVar-iFun-1);
    unsigned int cc = 0;
    for(unsigned int iInd=0; iInd<m_nVar-iFun; iInd++){
      if(oldIndices[iInd] != bestIndex){
        indicesUnfolding[cc] = oldIndices[iInd];
        cc = cc + 1;
      }
    }

    // free the memory;
    for(unsigned int iInd=0;iInd<m_nVar-iFun; iInd++){
      if(iInd != iBest){
        modes[iInd].clear();
        v_modes[iInd].clear();
      }
    }

    /*  Reshape w = sigma*v^T of the CPTT algorithm
      - inputs: the vector w = sigma*v^T
      - output: the pure tensor term
    */
    vec w = v_modes[iBest];
    w *= largestSingVal;

    std::vector<unsigned int> resPerDim(indicesUnfolding.size());
    for (unsigned int iVar = 0; iVar < indicesUnfolding.size(); iVar++) {
      resPerDim[iVar]=m_nDof_var[indicesUnfolding[iVar]];
    }

    // tensor W:
    W.init(resPerDim, m_comm);
    for(unsigned int iDof=0; iDof<w.size(); iDof++){
      double value = w.getVecEl(iDof);
      if(value != 0.0){
        vector<unsigned int> ind = W.lin2sub(iDof);
        W.set_tensorElement(ind, value);
      }
    }
    W.finalize();

 }
  // the last term:

mat lastUnfold;
W.computeUnfolding(0,lastUnfold);

svd firstTriplet;
firstTriplet.oneSVDTerm(lastUnfold);
double sigma = firstTriplet.sigma();
unsigned int bestIndex_0=indicesUnfolding[0];
unsigned int bestIndex_1=indicesUnfolding[1];

theTerm[bestIndex_0].copyVecFrom(firstTriplet.u());
theTerm[bestIndex_1].copyVecFrom(firstTriplet.v());
coeff=sigma;
firstTriplet.clear();
}



/* CPTT Approximation of a CP without optimisation of the coefficients
  - input: the CP tensor to be approximated, the tolerance
  - output: this CP tensor is the CPTT aproximation of the tensor
*/
CPTensor CPTensorCompression::CPTT_ApproxOf(CPTensor& toBeApprox, double tol){
  MPI_Comm m_comm = toBeApprox.comm();
  unsigned int m_nVar = toBeApprox.nVar();
  vector<unsigned int> m_nDof_var(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = toBeApprox.nDof_var(iVar);
  }

  ofstream outfile;
  if(!m_saveName.empty()){
    outfile.open(m_saveName.c_str());
  }

  CPTensor sol(m_nDof_var, m_comm);

  CPTensor residual;
  residual.copyTensorFrom(toBeApprox);

  double residualNorm = sqrt(residual.norm2CP());
  if(m_verbosity>0){
    PetscPrintf(m_comm,"Residual l2 norm = %f \n", residualNorm);
  }
  if(!m_saveName.empty()){
    outfile << residualNorm << flush << endl;
  }

  while(residualNorm > tol){
    vector<vec> kThTerm;
    double kThCoeff;
    compute_CPTT_Term(residual, kThTerm, kThCoeff);

    // update solution (this CP)
    sol.addPureTensorTerm(kThTerm, kThCoeff);

    // update residual:

    double resCoeff = -1.0*kThCoeff;
    residual.addPureTensorTerm(kThTerm, resCoeff);
    residualNorm = sqrt(residual.norm2CP());
    if(m_verbosity>0){
      PetscPrintf(m_comm,"Residual l2 norm = %f \n", residualNorm);
    }
    if(!m_saveName.empty()){
      outfile << residualNorm << flush << endl;
    }

  }

  if(!m_saveName.empty()){
    outfile.close();
  }
  return sol;
}


/* CPTT Approximation of a CP with optimisation of the coefficients
  - input: the CP tensor to be approximated, the tolerance
  - output: this CP tensor is the CPTT aproximation of the tensor
*/
CPTensor CPTensorCompression::CPTT_ApproxOf(CPTensor& toBeApprox, double tol, bool USE_CORRECTION=false){
  ofstream outfile;
  if(!m_saveName.empty()){
    outfile.open(m_saveName.c_str());
  }

  MPI_Comm m_comm = toBeApprox.comm();
  unsigned int m_nVar = toBeApprox.nVar();
  vector<unsigned int> m_nDof_var(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = toBeApprox.nDof_var(iVar);
  }

  CPTensor sol(m_nDof_var, m_comm);

  CPTensor residual;
  residual.copyTensorFrom(toBeApprox);

  double residualNorm = sqrt(residual.norm2CP());

  if(!m_saveName.empty()){
    outfile << residualNorm << flush << endl;
  }
  if(m_verbosity>0){
    PetscPrintf(m_comm,"Residual l2 norm = %f \n", residualNorm);
  }

  while(residualNorm > tol){
    vector<vec> kThTerm;
    double kThCoeff;
    compute_CPTT_Term(residual, kThTerm, kThCoeff);

    // update solution (this CP)
    sol.addPureTensorTerm(kThTerm, kThCoeff);

    // update residual:
    double resCoeff = -kThCoeff;
    residual.addPureTensorTerm(kThTerm, resCoeff);
    residualNorm = sqrt(residual.norm2CP());

    if(m_verbosity>0){
      PetscPrintf(m_comm,"Residual l2 norm = %f \n", residualNorm);
    }


    if(USE_CORRECTION){
      // optimise the coefficients:
      mat scalMat(sol.rank(), sol.rank(), m_comm);
      for(unsigned int iTerm=0; iTerm<sol.rank(); iTerm++){
        vector<vec> I_term = sol.terms(iTerm);
        for(unsigned int jTerm=iTerm; jTerm<sol.rank(); jTerm++){
          vector<vec> J_term = sol.terms(jTerm);
          double val = scalarProdTerms(I_term, J_term);
          scalMat.setMatEl(iTerm, jTerm, val);
          if(iTerm != jTerm){
            scalMat.setMatEl(jTerm,iTerm,val);
          }
        }
      }
      scalMat.finalize();

      vec scalVec(sol.rank(), m_comm);
      for(unsigned int iTerm=0; iTerm<sol.rank(); iTerm++){
        double entry = 0.0;
        vector<vec> I_term = sol.terms(iTerm);
        for(unsigned int kTerm=0; kTerm<toBeApprox.rank(); kTerm++){
          vector<vec> kTermVec = toBeApprox.terms(kTerm);
          double val = scalarProdTerms(I_term, kTermVec);
          val = val * toBeApprox.coeffs(kTerm);
          entry = entry + val;
        }
        scalVec.setVecEl(iTerm, entry, m_comm);
      }
      scalVec.finalize();

      // solving the linear system:
      vec updateCoeffs = scalMat / scalVec;

      for(unsigned int iTerm=0; iTerm<sol.rank(); iTerm++){
        double thisCoeff = updateCoeffs.getVecEl(iTerm);
        sol.set_coeffs(iTerm, thisCoeff);
        residual.set_coeffs(residual.rank() - sol.rank() + iTerm, -1.0*thisCoeff);
      }

      // free the memory:
      scalVec.clear();
      updateCoeffs.clear();
      scalMat.clear();


      // updating the residual norm:
      residualNorm = sqrt(residual.norm2CP());
    }


    if(m_verbosity>0){
      PetscPrintf(m_comm, "Corrected Norm2res = %f \n\n", residualNorm);
    }
    if(!m_saveName.empty()){
      outfile << residualNorm << flush << endl;
    }

  }
  if(!m_saveName.empty()){
    outfile.close();
  }
  return sol;
}


/*CPTT Approximation of full tensor
  - input: the full tensor to be approximated, the tolerance
  - output: this CP tensor is the CPTT aproximation of the tensor
*/
CPTensor CPTensorCompression::CPTT_ApproxOf(fullTensor& toBeApprox, double tol){
  ofstream outfile;
  if(!m_saveName.empty()){
    outfile.open(m_saveName.c_str());
  }

  MPI_Comm m_comm = toBeApprox.comm();
  unsigned int m_nVar = toBeApprox.nVar();
  vector<unsigned int> m_nDof_var(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = toBeApprox.nDof_var(iVar);
  }

  CPTensor sol(m_nDof_var, m_comm);

  fullTensor residual;
  residual.copyTensorFrom(toBeApprox);

  double residualNorm = residual.tensorEntries().norm();
  if(m_verbosity>0){
    PetscPrintf(m_comm,"Residual l2 norm = %f \n", residualNorm);
  }
  if(!m_saveName.empty()){
    outfile << residualNorm << flush << endl;
  }

  while(residualNorm > tol){
    vector<vec> kThTerm;
    double kThCoeff;
    compute_CPTT_Term(residual, kThTerm, kThCoeff);

    // update solution (this CP)
    sol.addPureTensorTerm(kThTerm, kThCoeff);

    // update residual:
    fullTensor thisTerm(m_nVar, m_nDof_var, m_comm);
    for(unsigned int l=0;l<thisTerm.nEntries(); l++){
      vector<unsigned int> ind = thisTerm.lin2sub(l);
      double value = kThCoeff;
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        double toMultiply = kThTerm[iVar].getVecEl(ind[iVar]);
        value = value * toMultiply;
      }
      thisTerm.set_tensorElement(l, value);
    }

    residual.sum(thisTerm, -1.0);
    residualNorm = residual.tensorEntries().norm();
    if(m_verbosity>1){
      PetscPrintf(m_comm, "Residual l2 norm = %f \n", residualNorm);
    }
    if(!m_saveName.empty()){
      outfile << residualNorm << flush << endl;
    }
  }
  if(!m_saveName.empty()){
    outfile.close();
  }
  return sol;
}


/*CPTT Approximation of a sparse tensor
  - input: the sparse tensor to be approximated, the tolerance
  - output: a CP tensor which is the CPTT aproximation of the tensor
*/
CPTensor CPTensorCompression::CPTT_ApproxOf(sparseTensor& toBeApprox, double tol){
  ofstream outfile;
  if(!m_saveName.empty()){
    outfile.open(m_saveName.c_str());
  }

  MPI_Comm m_comm = toBeApprox.comm();
  unsigned int m_nVar = toBeApprox.nVar();
  vector<unsigned int> m_nDof_var(m_nVar);
  unsigned int nDof = 1;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = toBeApprox.nDof_var(iVar);
    nDof *= m_nDof_var[iVar];
  }

  CPTensor sol(m_nDof_var, m_comm);

  sparseTensor residual;
  residual.copyTensorFrom(toBeApprox);

  double residualNorm = residual.norm();
  if(m_verbosity>0){
    PetscPrintf(m_comm,"Residual l2 norm = %f \n", residualNorm);
  }
  if(!m_saveName.empty()){
    outfile << residualNorm << flush << endl;
  }

  while(residualNorm > tol){
    vector<vec> kThTerm;
    double kThCoeff;
    compute_CPTT_Term(residual, kThTerm, kThCoeff);

    // update solution (this CP)
    sol.addPureTensorTerm(kThTerm, kThCoeff);

    // update residual:
    sparseTensor thisTerm(m_nDof_var, m_comm);
    for(unsigned int l=0; l<nDof; l++){
      vector<unsigned int> ind = thisTerm.lin2sub(l);
      double value = kThCoeff;
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        double toMultiply = kThTerm[iVar].getVecEl(ind[iVar]);
        value = value * toMultiply;
      }
      if(value != 0.0){
        thisTerm.set_tensorElement(ind, value);
      }
    }

    residual.sum(thisTerm, -1.0);
    residualNorm = residual.norm();
    if(m_verbosity>1){
      PetscPrintf(m_comm, "Residual l2 norm = %f \n", residualNorm);
    }
    if(!m_saveName.empty()){
      outfile << residualNorm << flush << endl;
    }
  }
  if(!m_saveName.empty()){
    outfile.close();
  }
  return sol;
}




/*CP Approximation of a full tensor via ALS method
  - input: the full tensor to be approximated
  - output: a CP tensor which is the ALS aproximation of the tensor
*/
CPTensor CPTensorCompression::ALS(fullTensor& toCompress, double tol){

  // init the solution:
  CPTensor toBeReturned(toCompress.nVar(), toCompress.nDof_var(), toCompress.comm());

  // init the residual:
  fullTensor residual;
  residual.copyTensorFrom(toCompress);

  double normRes = toCompress.tensorEntries().norm();
  unsigned int nTerms = 0;
  while(normRes>tol){
    // initialise a random term:
    vector<vec> termTest(residual.nVar());
    for(unsigned int iVar=0; iVar<residual.nVar(); iVar++){
      vec iVec = rand(residual.nDof_var(iVar), residual.comm());
      termTest[iVar] = iVec;
    }
    // compute an ALS fix point iteration to update the term
    double tolFP = 1.0e-2 * tol;
    fix_point_ALS(residual, termTest, tolFP);

    // add term Test to the solution:
    toBeReturned.addPureTensorTerm(termTest, 1.0);
    nTerms += 1;

    // update the residual:
    update_residual(residual, termTest);

    // update normRes:
    normRes = residual.tensorEntries().norm();
    cout << endl;
    cout << "Residual norm with " << nTerms << " terms: " << normRes << endl;

  }

  //clear the memory:
  residual.clear();

  // return the solution:
  return toBeReturned;
}


/* update_residual
  - input: the residual to be updated as a full tensor, the term to be subtracted
  - output: the pure tensor term is subtracted from the residual
*/
void CPTensorCompression::update_residual(fullTensor& currentRes, vector<vec>& term){

  for(unsigned int l=0; l<currentRes.nEntries(); l++){
    vector<unsigned int> ind = currentRes.lin2sub(l);
    double currentValue = currentRes.tensorEntries().getVecEl(l);
    double termVal = 1.0;
    for(unsigned int iVar=0; iVar<currentRes.nVar(); iVar++){
      unsigned int iDof = ind[iVar];
      termVal *= term[iVar].getVecEl(iDof);
    }
    currentValue -= termVal;
    currentRes.tensorEntries().setVecEl(l, currentValue);
  }
  currentRes.save("residual_ALS");
}



/*  compute_term
  - input: the full tensor to contract, the set of modes, the term i to update
  - output: update the i-th term
*/
void CPTensorCompression::compute_term(fullTensor& toContract, vector<vec>& terms, unsigned int iMod){

  // contract the fullTensor:
  fullTensor rhs;
  rhs.copyTensorFrom(toContract);

  unsigned int index = 0;
  for(unsigned int iVar=0; iVar<toContract.nVar(); iVar++){
    if(iVar != iMod){
      rhs.inplaceModeIContraction(index, terms[iVar]);
    }
    else{
      index = 1;
    }
    //rhs.print();
  }

  // compute normalisation constant:
  double normConst = 1.0;
  for(unsigned int iVar=0; iVar<toContract.nVar(); iVar++){
    if(iVar != iMod){
      double norm2term = terms[iVar].norm();
      norm2term = norm2term * norm2term;
      normConst *= norm2term;
    }
  }
  normConst = 1.0/ normConst;

  // updating the term:
  for(unsigned int iDof=0; iDof<terms[iMod].size(); iDof++){
    double value = rhs(iDof) * normConst;
    terms[iMod].setVecEl(iDof, value);
  }
  // clear the memory:
  rhs.clear();
}


/*  fix_point_ALS function:
  - input: the term to be computed
  - output: the ALS term to be added to the CP
*/
void CPTensorCompression::fix_point_ALS(fullTensor& toContract, vector<vec>& term, double tolFP){

  vector<vec> old_term(term.size());
  for(unsigned int iVar=0; iVar<term.size(); iVar++){
    old_term[iVar].copyVecFrom(term[iVar]);
  }

  double normDiscrepancy = 1.0e12;
  unsigned int nIter = 0;
  while( (normDiscrepancy>tolFP) && (nIter<100) ){
      // full cycle of update:
      for(unsigned int iVar=0; iVar<term.size(); iVar++){
        compute_term(toContract, term, iVar);
      }
      // compute the discrepancy:
      normDiscrepancy = 0.0;
      for(unsigned int iVar=0; iVar<term.size(); iVar++){
        vec diff = term[iVar] - old_term[iVar];
        double normDiff = diff.norm()/sqrt(diff.size()); // renormalised by the number of dofs
        normDiscrepancy += normDiff;
        diff.clear();
      }
      // update old_term:
      for(unsigned int iVar=0; iVar<term.size(); iVar++){
        old_term[iVar].clear();
        old_term[iVar].copyVecFrom(term[iVar]);
      }
      cout << "norm FP = " << normDiscrepancy << endl;
      nIter += 1;
  }

  // clear the memory:
  for(unsigned int iVar=0; iVar<term.size(); iVar++){
    old_term[iVar].clear();
  }

}
