// Implementation of Tucker compression routines:

#include "TuckerCompression.hpp"

/* HOSVD of a CPTensor
  - input: a CPTensor
  - output: its Tucker compression computed by HOSVD (up to machine precision)
*/
void TuckerCompression::HOSVD(CPTensor& toCompress, bool COMPUTE_APPROX){

  unsigned int m_nVar = toCompress.nVar();
  vector<unsigned int> m_nDof_var = toCompress.nDof_var();
  MPI_Comm m_comm = toCompress.comm();

  vector<unsigned int> tuckerRanks(m_nVar);
  m_HOSVD_modes.resize(m_nVar);
  for(unsigned int iVar = 0; iVar<m_nVar; iVar++){
    vector<vec> modesSet(toCompress.rank());
    for(unsigned int iFun=0; iFun<toCompress.rank(); iFun++){
      modesSet[iFun] = toCompress.terms(iFun, iVar);
    }
    // compute the svd of the modes:
    svd iVar_SVD(modesSet);

    if(!iVar_SVD.is_zero_rank()){
      mat toRetain = iVar_SVD.Umat();

      // setting the modes:
      m_HOSVD_modes[iVar].resize(toRetain.nCols());
      for(unsigned int iMod=0; iMod<toRetain.nCols(); iMod++){
        vec iViMmode(m_nDof_var[iVar], m_comm);
        for(unsigned int iDof=0; iDof<m_nDof_var[iVar]; iDof++){
          double val = toRetain.getMatEl(iDof, iMod);
          iViMmode.setVecEl(iMod, val);
        }
        iViMmode.finalize();
        m_HOSVD_modes[iVar][iMod] = iViMmode;
      }

      tuckerRanks[iVar] = toRetain.nCols();

      toRetain.clear();
    }
    iVar_SVD.clear();
  }


  if(COMPUTE_APPROX){
    // computing the core:
    fullTensor resCore(tuckerRanks, m_comm);
    for(unsigned int l=0; l<resCore.nEntries(); l++){
      vector<unsigned int> ind = resCore.lin2sub(l);
      double val = 0.0;
      vector<vec> testModes(m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        unsigned int iMod = ind[iVar];
        testModes[iVar] = m_HOSVD_modes[iVar][iMod];
      }

      for(unsigned int iTerm=0; iTerm<toCompress.rank(); iTerm++){
        double toAdd = toCompress.coeffs(iTerm);
        for(unsigned int iVar=0; iVar<m_nVar; iVar++){
          vec cpToTest = toCompress.terms(iTerm, iVar);
          toAdd = toAdd + scalProd(testModes[iVar], cpToTest);
        }
        val = val + toAdd;
      }
      resCore.set_tensorElement(l, val);
    }
    resCore.finalize();

    m_result.init(m_nDof_var, m_comm);
    m_result.set_core(resCore);
    m_result.set_modes(m_HOSVD_modes);
    m_result.get_ranks();
  }
}


/* HOSVD of a fullTensor
  - input: a fullTensor
  - output: its Tucker compression computed by HOSVD (up to machine precision)
*/
void TuckerCompression::HOSVD(fullTensor& toCompress, bool COMPUTE_APPROX){
  unsigned int m_nVar = toCompress.nVar();
  vector<unsigned int> m_nDof_var = toCompress.nDof_var();
  MPI_Comm m_comm = toCompress.comm();


  vector<unsigned int> tuckerRanks(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    tuckerRanks[iVar] = 0;
  }

  if(COMPUTE_APPROX){
    fullTensor resCore;
    resCore.copyTensorFrom(toCompress);

    m_HOSVD_modes.resize(m_nVar);
    m_HOSVD_sigmas.resize(m_nVar);
    for(unsigned int iVar = 0; iVar<m_nVar; iVar++){
      mat iUnfold = toCompress.computeUnfolding(iVar);

      // compute the svd of the modes:
      svd iVar_SVD(iUnfold);

      if(!iVar_SVD.is_zero_rank()){

        mat toRetain = iVar_SVD.Umat();
        mat toContract = transpose(toRetain);
        vec sigmas = iVar_SVD.Svec();

        // singular values table:
        m_HOSVD_sigmas[iVar].resize(sigmas.size());
        for(unsigned int iSig=0; iSig<sigmas.size(); iSig++){
          m_HOSVD_sigmas[iVar][iSig] = sigmas.getVecEl(iSig);
        }

        // setting the modes:
        m_HOSVD_modes[iVar].resize(toRetain.nCols());
        for(unsigned int iMod=0; iMod<toRetain.nCols(); iMod++){
          vec iViMmode(m_nDof_var[iVar], m_comm);
          for(unsigned int iDof=0; iDof<m_nDof_var[iVar]; iDof++){
            double val = toRetain.getMatEl(iDof, iMod);
            iViMmode.setVecEl(iDof, val);
          }
          iViMmode.finalize();
          m_HOSVD_modes[iVar][iMod] = iViMmode;
        }

        // setting the ranks:
        tuckerRanks[iVar] = toRetain.nCols();


        // contracting to compute the core:
        resCore.inPlaceMatTensProd(iVar, toContract);
        toContract.clear();

      }
      iVar_SVD.clear();
    }

    // computing the core:
    resCore.finalize();
    // check if the approximation is 0:

    bool isZero = true;
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(tuckerRanks[iVar] != 0){
        isZero = false;
        break;
      }
    }

    if(isZero){
      m_result.init(m_nDof_var, m_comm);
    }
    else{
      // putting the approximation into result:
      m_result.init(m_nDof_var, m_comm);
      m_result.set_core(resCore);
      m_result.set_modes(m_HOSVD_modes);
      m_result.get_ranks();
    }
  }
  else{
    m_HOSVD_modes.resize(m_nVar);
    m_HOSVD_sigmas.resize(m_nVar);
    for(unsigned int iVar = 0; iVar<m_nVar; iVar++){
      mat iUnfold = toCompress.computeUnfolding(iVar);

      // compute the svd of the modes:
      svd iVar_SVD(iUnfold);

      if(!iVar_SVD.is_zero_rank()){
        mat toRetain = iVar_SVD.Umat();
        vec sigmas = iVar_SVD.Svec();

        // singular values table:
        m_HOSVD_sigmas[iVar].resize(sigmas.size());
        for(unsigned int iSig=0; iSig<sigmas.size(); iSig++){
          m_HOSVD_sigmas[iVar][iSig] = sigmas.getVecEl(iSig);
        }

        // setting the modes:
        m_HOSVD_modes[iVar].resize(toRetain.nCols());
        for(unsigned int iMod=0; iMod<toRetain.nCols(); iMod++){
          vec iViMmode(m_nDof_var[iVar], m_comm);
          for(unsigned int iDof=0; iDof<m_nDof_var[iVar]; iDof++){
            double val = toRetain.getMatEl(iDof, iMod);
            iViMmode.setVecEl(iDof, val);
          }
          iViMmode.finalize();
          m_HOSVD_modes[iVar][iMod] = iViMmode;
        }

        // setting the ranks:
        tuckerRanks[iVar] = toRetain.nCols();
      }
      iVar_SVD.clear();
    }
  }

}


/* HOSVD of a sparseTensor
  - input: a fullTensor
  - output: its Tucker compression computed by HOSVD (up to machine precision)
*/
void TuckerCompression::HOSVD(sparseTensor& toCompress, bool COMPUTE_APPROX){

  unsigned int m_nVar = toCompress.nVar();
  vector<unsigned int> m_nDof_var = toCompress.nDof_var();
  MPI_Comm m_comm = toCompress.comm();

  vector<unsigned int> tuckerRanks(m_nVar);

  if(COMPUTE_APPROX){
    sparseTensor resCore;
    resCore.copyTensorFrom(toCompress);

    m_HOSVD_modes.resize(m_nVar);
    m_HOSVD_sigmas.resize(m_nVar);
    for(unsigned int iVar = 0; iVar<m_nVar; iVar++){

      mat iUnfold = toCompress.computeUnfolding(iVar);

      // compute the svd of the modes:
      svd iVar_SVD(iUnfold);

      if(!iVar_SVD.is_zero_rank()){
        mat toRetain = iVar_SVD.Umat();
        mat toContract = transpose(toRetain);
        vec sigmas = iVar_SVD.Svec();

        // setting the sigma table:
        m_HOSVD_sigmas[iVar].resize(sigmas.size());
        for(unsigned int iSig=0; iSig<sigmas.size(); iSig++){
          m_HOSVD_sigmas[iVar][iSig] = sigmas.getVecEl(iSig);
        }

        // setting the modes:
        m_HOSVD_modes[iVar].resize(toRetain.nCols());
        for(unsigned int iMod=0; iMod<toRetain.nCols(); iMod++){
          vec iViMmode(m_nDof_var[iVar], m_comm);
          for(unsigned int iDof=0; iDof<m_nDof_var[iVar]; iDof++){
            double val = toRetain.getMatEl(iDof, iMod);
            iViMmode.setVecEl(iDof, val);
          }
          iViMmode.finalize();
          m_HOSVD_modes[iVar][iMod] = iViMmode;
        }

        tuckerRanks[iVar] = toRetain.nCols();

        // contracting to compute the core:
        resCore.inPlaceMatTensProd(iVar, toContract);
        toContract.clear();

      }
      iVar_SVD.clear();
    }


    // computing the core:
    resCore.finalize();

    // convert the core into a full tensor:
    fullTensor convCore(m_nDof_var, m_comm);
    for(unsigned int iEntry=0; iEntry<resCore.nonZeros(); iEntry++){
      vector<unsigned int> ind = resCore.indices(iEntry);
      double value = resCore.tensorEntries(iEntry);
      convCore.set_tensorElement(ind, value);
    }
    convCore.finalize();
    resCore.clear();

    // putting the approximation into result:
    m_result.init(m_nDof_var, m_comm);
    m_result.set_core(convCore);
    m_result.set_modes(m_HOSVD_modes);
    m_result.get_ranks();
  }
  else{
    m_HOSVD_modes.resize(m_nVar);
    m_HOSVD_sigmas.resize(m_nVar);
    for(unsigned int iVar = 0; iVar<m_nVar; iVar++){

      mat iUnfold = toCompress.computeUnfolding(iVar);

      // compute the svd of the modes:
      svd iVar_SVD(iUnfold);

      if(!iVar_SVD.is_zero_rank()){
        mat toRetain = iVar_SVD.Umat();
        vec sigmas = iVar_SVD.Svec();

        // setting the sigma table:
        m_HOSVD_sigmas[iVar].resize(sigmas.size());
        for(unsigned int iSig=0; iSig<sigmas.size(); iSig++){
          m_HOSVD_sigmas[iVar][iSig] = sigmas.getVecEl(iSig);
        }

        // setting the modes:
        m_HOSVD_modes[iVar].resize(toRetain.nCols());
        for(unsigned int iMod=0; iMod<toRetain.nCols(); iMod++){
          vec iViMmode(m_nDof_var[iVar], m_comm);
          for(unsigned int iDof=0; iDof<m_nDof_var[iVar]; iDof++){
            double val = toRetain.getMatEl(iDof, iMod);
            iViMmode.setVecEl(iDof, val);
          }
          iViMmode.finalize();
          m_HOSVD_modes[iVar][iMod] = iViMmode;
        }

        tuckerRanks[iVar] = toRetain.nCols();
      }
      iVar_SVD.clear();
    }
  }
}


/* computing HOSVD of a generic tensor
  - input: a generic tensor (does not exploit a particular format)
  - output: modes table and singular values filled;
*/
void TuckerCompression::HOSVD(tensor& toApprox){
  m_target = &toApprox;
  m_HOSVD_modes.resize(m_target->nVar());
  m_HOSVD_sigmas.resize(m_target->nVar());
  for(unsigned int iVar=0; iVar<m_target->nVar(); iVar++){
    mat iUnfold = m_target->computeUnfolding(iVar);
    svd iVar_SVD(iUnfold);

    if(!iVar_SVD.is_zero_rank()){
      // singular values table:
      vec iThSigma = iVar_SVD.Svec();
      m_HOSVD_sigmas[iVar].resize(iThSigma.size());
      for(unsigned int iMod=0; iMod<iThSigma.size(); iMod++){
        double val = iThSigma.getVecEl(iMod);
        m_HOSVD_sigmas[iVar][iMod] = val;
      }
      // modes table:
      mat iThModeTable = iVar_SVD.Umat();
      m_HOSVD_modes[iVar].resize(iThModeTable.nCols());
      for(unsigned int iMod=0; iMod<iThModeTable.nCols(); iMod++){
        vec iViMmode(m_target->nDof_var(iVar), m_target->comm());
        for(unsigned int iDof=0; iDof<m_target->nDof_var(iVar); iDof++){
          double val = iThModeTable.getMatEl(iDof, iMod);
          iViMmode.setVecEl(iDof, val);
        }
        iViMmode.finalize();
        m_HOSVD_modes[iVar][iMod] = iViMmode;
      }
    }
  }
}


/* result approximation:
  - given the ranks to be retrieved, change result.
  - input: Tucker ranks
*/
void TuckerCompression::compute_resultApproximation(vector<unsigned int> tuckerRanks){
  // check for bounds:
  const unsigned int m_nVar = m_HOSVD_modes.size();
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(tuckerRanks[iVar]>m_HOSVD_modes[iVar].size()){
      tuckerRanks[iVar] = m_HOSVD_modes[iVar].size();
    }
  }

  // check if the requested approximation is empty:
  bool zeroApp = false;
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    if(tuckerRanks[iVar] == 0){
      zeroApp = true;
      break;
    }
  }

  // if it is empty:
  if(zeroApp){
    m_resultApproximation.init(tuckerRanks, m_result.comm());
  }
  else{
    // computing the core approximation:
    vector<unsigned int> indBounds(2*m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      indBounds[2*iVar] = 0;
      indBounds[2*iVar+1] = tuckerRanks[iVar];
    }

    // vector of the modes:
    vector<vector<vec> > appModes(m_nVar);
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      appModes[iVar].resize(tuckerRanks[iVar]);
      for(unsigned int iMod=0; iMod<tuckerRanks[iVar]; iMod++){
        appModes[iVar][iMod] = m_HOSVD_modes[iVar][iMod];
      }
    }

    // computing the core:
    fullTensor appCore = m_result.core().extractSubtensor(indBounds);

    // assembling the result approximation:
    m_resultApproximation.init(tuckerRanks, m_result.comm());
    m_resultApproximation.set_core(appCore);
    m_resultApproximation.set_modes(appModes);
    m_resultApproximation.get_ranks();
  }

}
