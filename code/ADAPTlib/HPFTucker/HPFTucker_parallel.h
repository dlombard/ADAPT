// class HPFTucker_parallel: subcommunicators

#ifndef HPFTucker_parallel_h
#define HPFTucker_parallel_h

#include "tensor.h"
#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"
#include "Tucker.h"
#include "Node.h"

class HPFTucker_parallel : public tensor{
private:
  Node* m_node;
  vector<Tucker> m_subTensors;
  unsigned int m_nOfSub;
  bool m_isInitialised;
  vector<Node*> m_leafs;

  // MPI_Comm m_comm is part of tensor
  unsigned int m_nOfTeams;
  MPI_Group m_worldGroup;
  vector<MPI_Group> m_teams;
  vector<MPI_Comm> m_comm_teams;
  vector<unsigned int> m_ownership_table;
  vector<unsigned int> m_local_map_iSub;

public:
  HPFTucker_parallel(){m_isInitialised = false;};
  ~HPFTucker_parallel(){};
  // overloaded contructors:
  HPFTucker_parallel(vector<unsigned int>, MPI_Comm);
  HPFTucker_parallel(unsigned int, vector<unsigned int>, MPI_Comm);
  HPFTucker_parallel(vector<unsigned int>, unsigned int, unsigned int, MPI_Comm);
  void init(vector<unsigned int>, MPI_Comm);


  // -- ACCESS FUNCTIONS --
  inline Node* node(){return m_node;}
  inline vector<Tucker> subTensors(){return m_subTensors;}
  inline Tucker subTensors(unsigned int iSub){return m_subTensors[iSub];}
  inline unsigned int nOfSub(){return m_nOfSub;}
  inline vector<Node*> leafs(){return m_leafs;}
  inline Node* leafs(unsigned int iSub){return m_leafs[iSub];}
  inline unsigned int nOfTeams(){return m_nOfTeams;}
  inline MPI_Group worldGroup(){return m_worldGroup;}
  inline vector<MPI_Group> teams(){return m_teams;}
  inline MPI_Group teams(unsigned int iTeam){return m_teams[iTeam];}
  inline vector<MPI_Comm> comm_teams(){return m_comm_teams;}
  inline MPI_Comm comm_teams(unsigned int iTeam){return m_comm_teams[iTeam];}
  inline vector<unsigned int> ownership_table(){return m_ownership_table;}
  inline unsigned int ownership_table(unsigned int iSub){return m_ownership_table[iSub];}

  // -- SETTERS --
  // setting a node:
  inline void set_node(Node* tree){
    m_node = tree;
    m_nOfSub = m_node->nOfLeafs();
    m_ownership_table.resize(m_nOfSub);
  }

  // setting one subTensor:
  inline void set_subTensors(unsigned int iSub, Tucker& subTens){
    unsigned int iTeam = iSub % m_nOfTeams;
    if(m_comm_teams[iTeam] != MPI_COMM_NULL){
      unsigned int iLoc = localSubId(iSub);
      m_subTensors[iLoc] = subTens;
    }
  }

  // setting just one tensor element:
  void set_tensorElement(vector<unsigned int> ind, double val){
    // first, find in which leaf:
    vector<unsigned int> shift_ind(m_nVar);
    Node* activeLeaf;
    m_node->whichLeaf(ind, activeLeaf, shift_ind);
    unsigned int iSub = m_node->findIdLeaf(activeLeaf);
    // setting the corresponding subtensor in the shifted index
    unsigned int iTeam = iSub % m_nOfTeams;
    if(m_comm_teams[iTeam] != MPI_COMM_NULL){
      unsigned int iLoc = localSubId(iSub);
      m_subTensors[iLoc].set_tensorElement(shift_ind, val);
    }
  }


  // EVALUATION


  /* eval function (base version)
    - input: the multi-index
    - output: the value
  */
  void eval(const vector<unsigned int>& ind, double& val){
    // first, find in which leaf:
    int hasVal = 0;
    vector<unsigned int> shift_ind(m_nVar);
    Node* activeLeaf;
    m_node->whichLeaf(ind, activeLeaf, shift_ind);
    unsigned int iSub = m_node->findIdLeaf(activeLeaf);
    // evaluate the corresponding subtensor in the shifted index
    unsigned int iTeam = iSub % m_nOfTeams;
    if(m_comm_teams[iTeam] != MPI_COMM_NULL){
      unsigned int iLoc = localSubId(iSub);
      m_subTensors[iLoc].eval(shift_ind, val);
      int idLocalProc;
      MPI_Comm_rank(m_comm_teams[iTeam], &idLocalProc);
      if(idLocalProc == 0){
        hasVal = 1;
      }
    }
    int idProc;
    MPI_Comm_rank(m_comm, &idProc);
    int root = idProc * hasVal;
    MPI_Bcast(&val, 1, MPI_DOUBLE, root, m_comm);
  }


  /* eval function (overloaded to return double)
    - input: the multi-index
    - output: the value
  */
  double eval(const vector<unsigned int>& ind){
    // first, find in which leaf:
    double val;
    int hasVal = 0;
    vector<unsigned int> shift_ind(m_nVar);
    Node* activeLeaf;
    m_node->whichLeaf(ind, activeLeaf, shift_ind);
    unsigned int iSub = m_node->findIdLeaf(activeLeaf);
    // evaluate the corresponding subtensor in the shifted index
    unsigned int iTeam = iSub % m_nOfTeams;
    if(m_comm_teams[iTeam] != MPI_COMM_NULL){
      unsigned int iLoc = localSubId(iSub);
      val = m_subTensors[iSub].eval(shift_ind);
      // root is the proc 0 of the team (there is always a 0)
      int idLocalProc;
      MPI_Comm_rank(m_comm_teams[iTeam], &idLocalProc);
      if(idLocalProc == 0){
        hasVal = 1;
      }
    }
    int idProc;
    MPI_Comm_rank(m_comm, &idProc);
    int root = idProc * hasVal;
    MPI_Bcast(&val, 1, MPI_DOUBLE, root, m_comm);
    return val;
  }

  /* eval function (overloaded to be variadic)
    - input: the multi-index
    - output: the value
  */
  double eval(unsigned int iComp,...){
    vector<unsigned int> ind(m_nVar);
    va_list ap;
    va_start(ap, iComp);
    ind[0] = iComp;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      ind[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);

    // first, find in which leaf:
    double val;
    int hasVal = 0;
    vector<unsigned int> shift_ind(m_nVar);
    Node* activeLeaf;
    m_node->whichLeaf(ind, activeLeaf, shift_ind);
    unsigned int iSub = m_node->findIdLeaf(activeLeaf);
    // evaluate the corresponding subtensor in the shifted index
    unsigned int iTeam = iSub % m_nOfTeams;
    if(m_comm_teams[iTeam] != MPI_COMM_NULL){
      unsigned int iLoc = localSubId(iSub);
      val = m_subTensors[iSub].eval(shift_ind);
      // root is the proc 0 of the team (there is always a 0)
      int idLocalProc;
      MPI_Comm_rank(m_comm_teams[iTeam], &idLocalProc);
      if(idLocalProc == 0){
        hasVal = 1;
      }
    }
    int idProc;
    MPI_Comm_rank(m_comm, &idProc);
    int root = idProc * hasVal;
    MPI_Bcast(&val, 1, MPI_DOUBLE, root, m_comm);
    return val;
  }

  // Nested proxy class for accessing and manipulating tensor elements
  // overloading operator () in assignement through a proxy class:
  class Proxy : public tensor
  {
    vector<unsigned int> idx;
    Node* x_node;
    vector<Tucker>* x_subTensors;

    unsigned int x_nOfTeams;
    MPI_Group x_worldGroup;
    vector<MPI_Comm>* x_comm_teams;
    vector<unsigned int>* x_ownership_table;
    vector<unsigned int>* x_local_map_iSub;

  public:

    inline void set_tensorElement(vector<unsigned int> ind, double val){
      // first, find in which leaf:
      vector<unsigned int> shift_ind(m_nVar);
      Node* activeLeaf;
      x_node->whichLeaf(ind, activeLeaf, shift_ind);
      unsigned int iSub = x_node->findIdLeaf(activeLeaf);
      // setting the corresponding subtensor in the shifted index
      unsigned int iTeam = iSub % x_nOfTeams;
      if((*x_comm_teams)[iTeam] != MPI_COMM_NULL){
        unsigned int iLoc = localSubId(iSub);
        (*x_subTensors)[iLoc].set_tensorElement(shift_ind, val);
      }
    }

    double eval(const vector<unsigned int>& ind){
      // first, find in which leaf:
      double val;
      int hasVal = 0;
      vector<unsigned int> shift_ind(m_nVar);
      Node* activeLeaf;
      x_node->whichLeaf(ind, activeLeaf, shift_ind);
      unsigned int iSub = x_node->findIdLeaf(activeLeaf);
      // evaluate the corresponding subtensor in the shifted index
      unsigned int iTeam = iSub % x_nOfTeams;
      if((*x_comm_teams)[iTeam] != MPI_COMM_NULL){
        unsigned int iLoc = localSubId(iSub);
        val = (*x_subTensors)[iSub].eval(shift_ind);
        // root is the proc 0 of the team (there is always a 0)
        int idLocalProc;
        MPI_Comm_rank((*x_comm_teams)[iTeam], &idLocalProc);
        if(idLocalProc == 0){
          hasVal = 1;
        }
      }
      int idProc;
      MPI_Comm_rank(m_comm, &idProc);
      int root = idProc * hasVal;
      MPI_Bcast(&val, 1, MPI_DOUBLE, root, m_comm);
      return val;
    }

    // constructor:
    Proxy(vector<unsigned int> idx, vector<unsigned int> dofPerDim, Node* x_node, vector<Tucker>* x_subTensors, unsigned int x_nOfTeams, vector<MPI_Comm>* x_comm_teams, vector<unsigned int>* x_local_map_iSub, MPI_Comm theComm) : idx(idx), x_node(x_node), x_subTensors(x_subTensors), x_nOfTeams(x_nOfTeams), x_comm_teams(x_comm_teams), x_local_map_iSub(x_local_map_iSub){
      m_comm = theComm;
      m_nVar = idx.size();
      m_nDof_var.resize(m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        m_nDof_var[iVar] = dofPerDim[iVar];
      }
      compute_minc();
    }

    // equal operator overload:
    inline double operator= (double value) {
      set_tensorElement(idx, value);
      return value;
    }

    // Overloading the double operator for assignement:
    operator double(){
      double toBeReturned = eval(idx);
      return toBeReturned;
    }

    // auxiliary:
    inline unsigned int localSubId(unsigned int iSub){
      unsigned int toBeReturned;
      for(unsigned int iLoc=0; iLoc<(*x_local_map_iSub).size(); iLoc++){
        if((*x_local_map_iSub)[iLoc] == iSub){
          toBeReturned = iSub;
          break;
        }
      }
      return toBeReturned;
    }

  };
  // end of Proxy
  // Variadic operator ():
  Proxy operator() (unsigned int I,...) {

    vector<unsigned int> ind(m_nVar);
    va_list ap;
    va_start(ap, I);
    ind[0] = I;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      ind[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);
    return Proxy(ind, m_nDof_var, m_node, &m_subTensors, m_nOfTeams, &m_comm_teams, &m_local_map_iSub, m_comm);
  }



  // -- II -- specific to HPFTucker_parallel

  // Create Teams:
  inline void createTeams(unsigned int nOfTeams){
    m_nOfTeams = nOfTeams;
    int nOfProcs;
    MPI_Comm_size(m_comm, &nOfProcs);
    int idProc;
    MPI_Comm_rank(m_comm, &idProc);
    MPI_Comm_group(m_comm, &m_worldGroup);

    m_teams.resize(m_nOfTeams);
    m_comm_teams.resize(m_nOfTeams);

    // Create m_nOfTeams groups, based on the proc ranks:
    vector<vector<int> > idProcs_inTeams(m_nOfTeams);
    for(int iProc=0; iProc<nOfProcs; iProc++){
      int iTeam = iProc%m_nOfTeams;
      idProcs_inTeams[iTeam].push_back(iProc);
    }

    // Creating the teams:
    for(unsigned int iTeam=0; iTeam< m_nOfTeams; iTeam++){
      int nThis = idProcs_inTeams[iTeam].size();
      int thisRanks[nThis];
      for(unsigned int iP=0; iP<nThis; iP++){
        thisRanks[iP] = idProcs_inTeams[iTeam][iP];
      }

      MPI_Group thisGroup;
      MPI_Group_incl(m_worldGroup, nThis, thisRanks, &thisGroup);
      m_teams[iTeam] = thisGroup;

      // Create a new communicator based on the groups ( 0 is the TAG!)
      MPI_Comm thisComm;
      MPI_Comm_create_group(m_comm, thisGroup, 0, &thisComm);
      m_comm_teams[iTeam] = thisComm;
    }
  }

  // checking teams by printing:
  inline void checkTeams(){
    int nOfProcs;
    MPI_Comm_size(m_comm, &nOfProcs);
    int idProc;
    MPI_Comm_rank(m_comm, &idProc);

    for(unsigned int iTeam=0; iTeam<m_nOfTeams; iTeam++){
      int locSize, locProc;
      MPI_Comm_size(m_comm_teams[iTeam], &locSize);
      MPI_Comm_rank(m_comm_teams[iTeam], &locProc);
      if(m_comm_teams[iTeam] != MPI_COMM_NULL){
          cout << "Local rank: " << locProc << "  Global rank: " << idProc << "  Team: " << iTeam << endl;
      }
    }
  }

  // print the tree structure:
  inline void print_leafsTable(){
    for(unsigned int iLeaf=0; iLeaf< m_leafs.size(); iLeaf++){
      vector<vector<unsigned int> > i_vec_ind = m_leafs[iLeaf]->indices();
      PetscPrintf(m_comm, "Printing the index table for Leaf: %d \n", iLeaf);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        for(unsigned int jDof=0; jDof<i_vec_ind[iVar].size(); jDof++){
          PetscPrintf(m_comm, "%d  ", i_vec_ind[iVar][jDof]);
        }
        PetscPrintf(m_comm, "\n");
      }
      PetscPrintf(m_comm, "\n");
    }
  }

// given the global iSub, find the local
inline unsigned int localSubId(unsigned int iSub){
  unsigned int toBeReturned;
  for(unsigned int iLoc=0; iLoc<m_local_map_iSub.size(); iLoc++){
    if(m_local_map_iSub[iLoc] == iSub){
      toBeReturned = iSub;
      break;
    }
  }
  return toBeReturned;
}

// end of class:
};

#endif
