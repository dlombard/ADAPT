// implementation of HPFTucker_parallel

#include "HPFTucker_parallel.h"


// -- CONSTRUCTORS --

/* constructor:
  - input: the degrees of freedom per direction, the communicator
  - output: tensor is init
*/
HPFTucker_parallel::HPFTucker_parallel(vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = dofPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  compute_minc();
}


/* constructor:
  - input: number of variables, the degrees of freedom per direction, the communicator
  - output: tensor is init
*/
HPFTucker_parallel::HPFTucker_parallel(unsigned int dim, vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = dim;
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  compute_minc();
}


/* constructor:
  - input:  the degrees of freedom per direction, the tree width, the number of teams the communicator
  - output: tensor is init, uniform dyadic tree
*/
HPFTucker_parallel::HPFTucker_parallel(vector<unsigned int> dofPerDim, unsigned int width, unsigned int nTeams, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = dofPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  compute_minc();

  // compute the teams:
  createTeams(nTeams);

  // creating the Node:
  vector<vector<unsigned int> > indexTable(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    indexTable[iVar].resize(m_nDof_var[iVar]);
    for(unsigned int jDof=0; jDof<m_nDof_var[iVar]; jDof++){
      indexTable[iVar][jDof] = jDof;
    }
  }
  m_node = new Node(indexTable);
  m_node->createDyadicTree(width);
  m_leafs = m_node->getLeafs();
  m_nOfSub = m_leafs.size();
  m_ownership_table.resize(m_nOfSub);
  //distribute m_subTensors on teams: iSub = GLOBAL INDEX of subtensor
  for(unsigned int iSub=0; iSub<m_nOfSub; iSub++){
    const unsigned int iTeam = iSub % m_nOfTeams;
    m_ownership_table[iSub] = iTeam;
    if(m_comm_teams[iTeam] != MPI_COMM_NULL){
      vector<vector<unsigned int> > vec_iSub = m_leafs[iSub]->indices();
      vector<unsigned int> resPerDim_iSub(m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        resPerDim_iSub[iVar] = vec_iSub[iVar].size();
      }
      Tucker subTens(resPerDim_iSub, m_comm_teams[iTeam]);
      m_subTensors.push_back(subTens);
      m_local_map_iSub.push_back(iSub);
    }
  }
}


/* constructor: init for an empty object
  - input: the degrees of freedom per direction, the communicator
  - output: tensor is init
*/
void HPFTucker_parallel::init(vector<unsigned int> dofPerDim, MPI_Comm theComm){
  m_comm = theComm;
  m_nVar = dofPerDim.size();
  m_nDof_var.resize(m_nVar);
  for(unsigned int iVar=0; iVar<m_nVar; iVar++){
    m_nDof_var[iVar] = dofPerDim[iVar];
  }
  compute_minc();
}
