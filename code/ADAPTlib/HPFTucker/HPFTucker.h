// Header for HPFTucker tensor format:

#ifndef HPFTucker_h
#define HPFTucker_h

#include "../tensor.h"
#include "../linAlg/linAlg.h"
#include "../Tucker/Tucker.h"
#include "../geometry/Node.h"

class HPFTucker : public tensor{
private:
  Node* m_node;
  vector<Tucker> m_subTensors;
  unsigned int m_nOfSub;
  bool m_isInitialised;
  vector<Node*> m_leafs;

public:
  HPFTucker(){m_isInitialised = false;};
  ~HPFTucker(){};
  // overloaded contructors:
  HPFTucker(vector<unsigned int>, MPI_Comm);
  HPFTucker(unsigned int, vector<unsigned int>, MPI_Comm);
  HPFTucker(vector<unsigned int>, unsigned int, MPI_Comm);
  HPFTucker(vector<unsigned int> dofPerDim, Node*&, vector<Tucker>&);
  void init(vector<unsigned int>, MPI_Comm);
  // clear function:
  void clear(){
    m_node->eraseIndices();
    for(unsigned int iSub=0; iSub<m_nOfSub; iSub++){
      m_subTensors[iSub].clear();
      delete m_leafs[iSub];
    }
  }

  // -- ACCESS FUNCTIONS --
  inline Node* node(){return m_node;}
  inline vector<Tucker> subTensors(){return m_subTensors;}
  inline Tucker subTensors(unsigned int iSub){return m_subTensors[iSub];}
  inline unsigned int nOfSub(){return m_nOfSub;}
  inline vector<Node*> leafs(){return m_leafs;}
  inline Node* leafs(unsigned int iSub){return m_leafs[iSub];}

  // -- SETTERS --
  // setting a node:
  inline void set_node(Node* tree){
    m_node = tree;
    m_nOfSub = m_node->nOfLeafs();
    m_subTensors.resize(m_node->nOfLeafs());
  }
  // setting subTensors:
  inline void set_subTensors(vector<Tucker>& subTens){
    m_nOfSub = subTens.size();
    m_subTensors = subTens;
  }
  // setting one subTensor:
  inline void set_subTensors(unsigned int iSub, Tucker& subTens){
    m_subTensors[iSub] = subTens;
  }
  // setting just one tensor element:
  void set_tensorElement(vector<unsigned int> ind, double val){
    // first, find in which leaf:
    vector<unsigned int> shift_ind(m_nVar);
    Node* activeLeaf;
    m_node->whichLeaf(ind, activeLeaf, shift_ind);
    unsigned int iSub = m_node->findIdLeaf(activeLeaf);
    // evaluate the corresponding subtensor in the shifted index
    m_subTensors[iSub].set_tensorElement(shift_ind, val);
  }

  // -- Evaluate --
  inline void getNumSub(){m_nOfSub = m_subTensors.size();}


  /* eval function (base version)
    - input: the multi-index
    - output: the value
  */
  void eval(const vector<unsigned int>& ind, double& val){
    // first, find in which leaf:
    vector<unsigned int> shift_ind(m_nVar);
    Node* activeLeaf;
    m_node->whichLeaf(ind, activeLeaf, shift_ind);
    unsigned int iSub = m_node->findIdLeaf(activeLeaf);
    // evaluate the corresponding subtensor in the shifted index
    m_subTensors[iSub].eval(shift_ind, val);
  }


  /* eval function (overloaded to return double)
    - input: the multi-index
    - output: the value
  */
  double eval(const vector<unsigned int>& ind){
    // first, find in which leaf:
    vector<unsigned int> shift_ind(m_nVar);
    Node* activeLeaf;
    m_node->whichLeaf(ind, activeLeaf, shift_ind);
    unsigned int iSub = m_node->findIdLeaf(activeLeaf);
    // evaluate the corresponding subtensor in the shifted index
    return m_subTensors[iSub].eval(shift_ind);
  }


  /* eval function (overloaded to be variadic)
    - input: the multi-index
    - output: the value
  */
  double eval(unsigned int iComp,...){
    vector<unsigned int> ind(m_nVar);
    va_list ap;
    va_start(ap, iComp);
    ind[0] = iComp;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      ind[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);

    // first, find in which leaf:
    vector<unsigned int> shift_ind(m_nVar);
    Node* activeLeaf;
    m_node->whichLeaf(ind, activeLeaf, shift_ind);
    unsigned int iSub = m_node->findIdLeaf(activeLeaf);
    // evaluate the corresponding subtensor in the shifted index
    return m_subTensors[iSub].eval(shift_ind);
  }


  // Nested proxy class for accessing and manipulating tensor elements
  // overloading operator () in assignement through a proxy class:
  class Proxy : public tensor
  {
    vector<unsigned int> idx;
    Node* x_node;
    vector<Tucker>* x_subTensors;

  public:

    inline void set_tensorElement(vector<unsigned int> ind, double val){
      // first, find in which leaf:
      vector<unsigned int> shift_ind(m_nVar);
      Node* activeLeaf;
      x_node->whichLeaf(ind, activeLeaf, shift_ind);
      unsigned int iSub = x_node->findIdLeaf(activeLeaf);
      // evaluate the corresponding subtensor in the shifted index
      (*x_subTensors)[iSub].set_tensorElement(shift_ind, val);
    }

    double eval(const vector<unsigned int>& ind){
      // first, find in which leaf:
      vector<unsigned int> shift_ind(m_nVar);
      Node* activeLeaf;
      x_node->whichLeaf(ind, activeLeaf, shift_ind);
      unsigned int iSub = x_node->findIdLeaf(activeLeaf);
      // evaluate the corresponding subtensor in the shifted index
      return (*x_subTensors)[iSub].eval(shift_ind);
    }

    // constructor:
    Proxy(vector<unsigned int> idx, vector<unsigned int> dofPerDim, Node* x_node, vector<Tucker>* x_subTensors, MPI_Comm theComm) : idx(idx), x_node(x_node), x_subTensors(x_subTensors){
      m_comm = theComm;
      m_nVar = idx.size();
      m_nDof_var.resize(m_nVar);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        m_nDof_var[iVar] = dofPerDim[iVar];
      }
      compute_minc();
    }

    // equal operator overload:
    inline double operator= (double value) {
      set_tensorElement(idx, value);
      return value;
    }

    // Overloading the double operator for assignement:
    operator double(){
      double toBeReturned = eval(idx);
      return toBeReturned;
    }
  };
  // end of Proxy
  // Variadic operator ():
  Proxy operator() (unsigned int I,...) {

    vector<unsigned int> ind(m_nVar);
    va_list ap;
    va_start(ap, I);
    ind[0] = I;
    for(unsigned int iVar=1; iVar<m_nVar; iVar++){
      ind[iVar] = va_arg(ap, unsigned int);
    }
    va_end(ap);
    return Proxy(ind, m_nDof_var, m_node, &m_subTensors, m_comm);
  }


  // Methods and operations for Tensor Train:
  // COPY Tensor Structure:
	void copyTensorStructFrom(HPFTucker&);

	// COPY the full tensor:
	void copyTensorFrom(HPFTucker&);

  // OPERATIONS on Tensor Train (generic):
	bool hasSameStructure(HPFTucker&);
	bool isEqualTo(HPFTucker&);
  bool operator == (HPFTucker&);
	void sum(HPFTucker&, double, bool);
  void operator += (HPFTucker&);
	void multiplyByScalar(double);
  void operator *= (double);
	void shiftByScalar(double);
  void operator += (double);
	void extractSubtensor(vector<unsigned int>, HPFTucker&);
  HPFTucker extractSubtensor(vector<unsigned int>);
  void extractSubtensor(vector<vector<unsigned int> >, HPFTucker&);
  HPFTucker extractSubtensor(vector<vector<unsigned int> >);
	void assignSubtensor(vector<unsigned int>, HPFTucker&);


  // specific to HPFTucker:
  bool hasSamePartition(HPFTucker&);
  inline void print_leafsTable(){
    for(unsigned int iLeaf=0; iLeaf< m_leafs.size(); iLeaf++){
      vector<vector<unsigned int> > i_vec_ind = m_leafs[iLeaf]->indices();
      PetscPrintf(m_comm, "Printing the index table for Leaf: %d \n", iLeaf);
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){
        for(unsigned int jDof=0; jDof<i_vec_ind[iVar].size(); jDof++){
          PetscPrintf(m_comm, "%d  ", i_vec_ind[iVar][jDof]);
        }
        PetscPrintf(m_comm, "\n");
      }
      PetscPrintf(m_comm, "\n");
    }
  }

  // check if a Tucker subtensor is empty:
  inline bool isEmpty(unsigned int iSub){
    bool isIt = false;
    vector<unsigned int> subRanks = m_subTensors[iSub].ranks();
    for(unsigned int iVar=0; iVar<m_nVar; iVar++){
      if(subRanks[iVar] == 0){
        isIt = true;
        break;
      }
    }
    return isIt;
  }

};


#endif
