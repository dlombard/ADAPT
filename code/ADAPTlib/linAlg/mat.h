// header class for matrix wrapper
/*
  - Implementation of part of the auxiliary functions
*/

#ifndef mat_h
#define mat_h

#include "../genericInclude.h"
#include "vec.h"


using namespace std;

class mat{
private:
  Mat m_M;
  MPI_Comm m_comm;
  unsigned int m_nRows;
  unsigned int m_nCols;
public:
  mat(){};
  ~mat(){};
  void clear(){MatDestroy(&m_M);}


  /* overloaded Constructor:
    - Inputs: number of rows and cols, Communicator
    - Output: init the matrix
  */
  mat(unsigned int nRows, unsigned int nCols, MPI_Comm theComm){
    m_nRows = nRows;
    m_nCols = nCols;
    m_comm = theComm;
    PetscErrorCode ierr;
    ierr = MatCreate(theComm, &m_M);
    ierr = MatSetSizes(m_M, PETSC_DECIDE, PETSC_DECIDE, nRows, nCols);
    ierr = MatSetFromOptions(m_M);
    ierr = MatSetUp(m_M);
  }

  /* overloaded Constructor:
    - Inputs: number of rows and cols
    - Output: init the matrix
    (!) m_comm = PETSC_COMM_WORLD
  */
  mat(unsigned int nRows, unsigned int nCols){
    m_nRows = nRows;
    m_nCols = nCols;
    m_comm = PETSC_COMM_WORLD;
    PetscErrorCode ierr;
    ierr = MatCreate(PETSC_COMM_WORLD, &m_M);
    ierr = MatSetSizes(m_M, PETSC_DECIDE, PETSC_DECIDE, nRows, nCols);
    ierr = MatSetFromOptions(m_M);
    ierr = MatSetUp(m_M);
  }

  // init an object created with empty constructor:
  void init(unsigned int nRows, unsigned int nCols, MPI_Comm theComm){
    m_nRows = nRows;
    m_nCols = nCols;
    m_comm = theComm;
    PetscErrorCode ierr;
    ierr = MatCreate(theComm, &m_M);
    ierr = MatSetSizes(m_M, PETSC_DECIDE, PETSC_DECIDE, nRows, nCols);
    ierr = MatSetFromOptions(m_M);
    ierr = MatSetUp(m_M);
  }

  // init an object created with empty constructor (m_comm=PETSC_COMM_WORLD):
  void init(unsigned int nRows, unsigned int nCols){
    m_nRows = nRows;
    m_nCols = nCols;
    m_comm = PETSC_COMM_WORLD;
    PetscErrorCode ierr;
    ierr = MatCreate(PETSC_COMM_WORLD, &m_M);
    ierr = MatSetSizes(m_M, PETSC_DECIDE, PETSC_DECIDE, nRows, nCols);
    ierr = MatSetFromOptions(m_M);
    ierr = MatSetUp(m_M);
  }

  // overloaded init to compy a given matrix structure (and its non-zero pattern)
  void init(Mat A){
      PetscErrorCode ierr;
      ierr = MatDuplicate(A, MAT_SHARE_NONZERO_PATTERN, &m_M);
  }

  /* set matrix element
  - input: i,j, value
  - output: [m_M]_{i,j} = val
  */
  void setMatEl(int indRow, int indCol, double val, bool PAR=true){
    if(!PAR){
      int rank;
      MPI_Comm_rank (m_comm, &rank);
      if(rank == 0){ // Proc 0 is doing it and communicating it
        MatSetValues(m_M, 1, &indRow, 1, &indCol, &val, INSERT_VALUES);
      }
    }
    else{ //Parallel non-communicating way
      int startInd, endInd;
      MatGetOwnershipRange(m_M, &startInd, &endInd);
      if( (indRow>=startInd) &&(indRow<endInd) ){
        MatSetValues(m_M, 1, &indRow, 1, &indCol, &val, INSERT_VALUES);
      }
    }
    // Need to Finalize the Assembly outside !
  }

  /* set matrix element
  - input: i,j, value
  - output: [m_M]_{i,j} = val
  */
  void addMatEl(int indRow, int indCol, double val, bool PAR=true){
    if(!PAR){
      int rank;
      MPI_Comm_rank (m_comm, &rank);
      if(rank == 0){ // Proc 0 is doing it and communicating it
        MatSetValues(m_M, 1, &indRow, 1, &indCol, &val, ADD_VALUES);
      }
    }
    else{ //Parallel non-communicating way
      int startInd, endInd;
      MatGetOwnershipRange(m_M, &startInd, &endInd);
      if( (indRow>=startInd) &&(indRow<endInd) ){
        MatSetValues(m_M, 1, &indRow, 1, &indCol, &val, ADD_VALUES);
      }
    }
    // Need to Finalize the Assembly outside !
  }


  /* set multiple matrix elements, finalize outside!
  - input: i,j, value
  - output: [m_M]_{i,j} = val
  */
  void setElements(vector<int> indRow, vector<int> indCol, vector<double> val){
    const PetscInt nActiveRows = indRow.size();
    const PetscInt nActiveCols = indCol.size();
    assert(nActiveRows==nActiveCols);

    PetscInt *idxm, *idxn;
    // putting the indRow:
    PetscMalloc1(nActiveRows, &idxm);
    for (unsigned int iR=0; iR<nActiveRows; iR++) {idxm[iR] = indRow[iR];}
    // putting the indCol:
    PetscMalloc1(nActiveCols, &idxn);
    for (unsigned int iC=0; iC<nActiveCols; iC++) {idxn[iC] = indCol[iC];}

    PetscScalar *values;
    PetscMalloc1(nActiveRows, &values);
    for (unsigned int iR=0; iR<nActiveRows; iR++) {values[iR] = val[iR];}

    MatSetValues(m_M, nActiveRows, idxm, nActiveCols, idxn, values, INSERT_VALUES);

    PetscFree(idxm);
    PetscFree(idxn);
    PetscFree(values);
  }


  /* set multiple matrix elements, parallel efficient way, finalize outside!
  - input: [i],[j], value
  - output: [m_M]_{i,j} = val
  */
  void setElements(unsigned int nActiveRows, PetscInt *idxm, unsigned int nActiveCols, PetscInt *idxn, PetscScalar *values){
    MatSetValues(m_M, nActiveRows, idxm, nActiveCols, idxn, values, INSERT_VALUES);
  }

  /* set multiple matrix elements in a Row, parallel efficient way.
  - input: [i],[j], value
  - output: [m_M]_{i,j} = val
  */
  void setRowElements(const PetscInt iRow, unsigned int nActiveCols, PetscInt *idxn, PetscScalar *values){
    PetscInt start, end;
    MatGetOwnershipRange(m_M, &start, &end);
    // just the proc which has this Row in memory assembles it
    if( (iRow>=start) && (iRow<end) ){
      MatSetValues(m_M, 1, &iRow, nActiveCols, idxn, values, INSERT_VALUES);
    }
  }


  /* get matrix element:
   - Get just one element, specified by idRow and idCol, global indices:
   */
  double getMatEl(int idRow, int idCol) const {
    double toBeReturned = 0.0;
    int rank;
    MPI_Comm_rank (m_comm, &rank);
    int startInd, endInd;
    MatGetOwnershipRange( m_M , &startInd , &endInd );
    int amIRoot = 0;
    if( (idRow >=startInd ) && (idRow<endInd) ){
      MatGetValues(m_M, 1, &idRow, 1, &idCol, &toBeReturned);
      amIRoot = 1;
    }
    int consensus = amIRoot * rank;
    int root = 0;
    MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, m_comm);
    MPI_Bcast(&toBeReturned, 1, MPI_DOUBLE, root, m_comm);

    return toBeReturned;
  }

  // copy a Matrix:
  void copyMatFrom(const mat& A){
    init(A.nRows(), A.nCols(), A.comm());
    MatCopy(A.M(), m_M, DIFFERENT_NONZERO_PATTERN);
    finalize();
  }

  // copy an assembled Matrix:
  void copy(const mat& A) {
    assert(m_nRows==A.m_nRows && m_nCols==A.m_nCols && m_comm==A.m_comm);
    MatCopy(A.m_M, m_M, DIFFERENT_NONZERO_PATTERN);
  }

  // copy an unassembled Matrix:
  void convert(const mat& A) {
    m_nRows = A.m_nRows;
    m_nCols = A.m_nCols;
    m_comm = A.m_comm;
    PetscErrorCode ierr;
    ierr = MatConvert(A.m_M, MATSAME, MAT_INITIAL_MATRIX, &m_M);
  }

  // check if a matrix is assembled or not:
  bool isAssembled() {
    PetscBool assembled;
    PetscErrorCode ierr = MatAssembled(m_M, &assembled);
    return assembled;
  }

  // get a column vector:
  vec getCol(const int iCol) const {
    Vec theCol;
    PetscErrorCode ierr;
    ierr = VecCreate(m_comm, &theCol);
    ierr = VecSetSizes(theCol, PETSC_DECIDE, m_nRows);
    ierr = VecSetFromOptions(theCol);


    MatGetColumnVector(m_M, theCol, iCol);
    vec toBeReturned;
    toBeReturned.setComm(m_comm);
    toBeReturned.setVector(theCol);
    toBeReturned.finalize();
    return toBeReturned;
    /*vec toBeReturned(m_nRows, m_comm);
    for(unsigned int iRow=0; iRow<m_nRows; iRow++){
      double val = getMatEl(iRow, iCol);
      toBeReturned.setVecEl(iRow, val);
    }
    toBeReturned.finalize();
    return toBeReturned;*/
  }



  // set a column vector:
  void setCol(int iCol, const vec& col){
    assert(col.size() == m_nRows);
    assert(m_comm == col.comm());

    int lowVec, highVec;
    VecGetOwnershipRange(col.x(), &lowVec, &highVec);
    int lowMat, highMat;
    MatGetOwnershipRange(m_M, &lowMat, &highMat);

    for(int i=lowVec; i<highVec; i++){
        if( (i>=lowMat) && (i<highMat) ){ // no communication needed
          PetscScalar val;
          VecGetValues(col.x(),1,&i,&val);
          MatSetValue(m_M,i,iCol,val,INSERT_VALUES);
        }
        else{ // collective call to share the value
          double globVal = getMatEl(i, iCol);
          setMatEl(i, iCol, globVal);
        }
    }
    finalize();
  }



  // finalize the Assembly:
  void finalize() {
      MatAssemblyBegin(m_M, MAT_FINAL_ASSEMBLY);
      MatAssemblyEnd(m_M, MAT_FINAL_ASSEMBLY);
  };

  // print:
  void print() {
    finalize();
    MatView(m_M, PETSC_VIEWER_STDOUT_WORLD);
  }

  // -- SETTERS --

  // Set communicator:
  inline void setComm(MPI_Comm theComm){
    m_comm = theComm;
  }

  // Set Mat:
  inline void setMatrix(Mat& theMat){
    m_M = theMat;
    int nRows, nCols;
    MatGetSize(m_M, &nRows, &nCols);
    m_nRows = nRows;
    m_nCols = nCols;
  }


  // -- ACCESS FUNCTIONS --
  inline const Mat M() const {return m_M;}
  inline Mat M(){return m_M;}
  inline Mat& Mpt(){return m_M;}
  inline unsigned int nRows() const {return m_nRows;}
  inline unsigned int nCols() const {return m_nCols;}
  inline MPI_Comm comm() const {return m_comm;}

  // getSize
  inline void getSize(){
    int nRows, nCols;
    MatGetSize(m_M, &nRows, &nCols);
    m_nRows = nRows;
    m_nCols = nCols;
  }


  // -- OPERATORS --

  // Proxy class (nested) to access elements in assignement:
  class Proxy{
    Mat x;
    unsigned int id_i;
    unsigned int id_j;
    MPI_Comm b_comm;
  public:

     // Set one matrix element:
     void setMatEl(Mat& A, int indRow, int indCol, double val, MPI_Comm theComm, bool PAR=true){
       if(!PAR){
         int rank;
         MPI_Comm_rank (theComm, &rank);
         if(rank == 0){ // Proc 0 is doing it and communicating it
           MatSetValues(A, 1, &indRow, 1, &indCol, &val, INSERT_VALUES);
         }
       }
       else{ //Parallel non-communicating way
         int startInd, endInd;
         MatGetOwnershipRange(A, &startInd, &endInd);
         if( (indRow>=startInd) &&(indRow<endInd) ){
           MatSetValues(A, 1, &indRow, 1, &indCol, &val, INSERT_VALUES);
         }
       }
       // Need to Finalize the Assembly outside !
     }

     // Get matrix elements:
     double getMatEl(Mat A, int idRow, int idCol, MPI_Comm theComm){
       double toBeReturned = 0.0;
       int rank;
       MPI_Comm_rank (theComm, &rank);
       int startInd, endInd;
       MatGetOwnershipRange( A , &startInd , &endInd );
       int amIRoot = 0;
       if( (idRow >=startInd ) && (idRow<endInd) ){
         MatGetValues(A, 1, &idRow, 1, &idCol, &toBeReturned);
         amIRoot = 1;
       }
       int consensus = amIRoot * rank;
       int root = 0;
       MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, theComm);
       MPI_Bcast(&toBeReturned, 1, MPI_DOUBLE, root, theComm);

       return toBeReturned;
     }

     Proxy(Mat x, unsigned int id_i, unsigned int id_j, MPI_Comm b_comm) : x(x), id_i(id_i), id_j(id_j), b_comm(b_comm) {}
     inline double operator= (double value) {
       setMatEl(x, id_i, id_j, value, b_comm);
       return value;
     }
     operator double(){
       double toBeReturned = getMatEl(x, id_i, id_j, b_comm);
       return toBeReturned;
     }
  };

  Proxy operator() (unsigned int i, unsigned int j) { return Proxy(m_M, i,j, m_comm); }


  // check if a matrix is empty:
  bool isEmpty(){
    bool toBeReturned = true;
    getSize();
    if(m_nRows==0 || m_nCols==0){
      return toBeReturned;
    }
    else{
      for(unsigned int iRow=0; iRow<m_nRows; iRow++){
        for(unsigned int iCol=0; iCol<m_nCols; iCol++){
          if(fabs(getMatEl(iRow,iCol)) > DBL_EPSILON){
            toBeReturned = false;
            break;
          }
        }
      }
    }
    return toBeReturned;
  }

  // multiply all the elements by a scalar:
  inline void operator *= (const double alpha){
    finalize();
    MatScale(m_M, alpha);
  }

  // sum to all elements a scalar:
  inline void operator += (const double alpha){
    finalize();
    MatShift(m_M, alpha);
  }

  // sum a matrix to the current one:
  inline void operator += (const mat& toBeAdded){
    finalize();
    MatAXPY(m_M, 1.0, toBeAdded.M(), DIFFERENT_NONZERO_PATTERN);
  }

  // sum to the current matrix:
  inline void sum(const mat& toBeAdded, const double alpha){
    finalize();
    MatAXPY(m_M, alpha, toBeAdded.M(), DIFFERENT_NONZERO_PATTERN);
  }

  // sum to the current matrix:
  inline void scale(const double alpha) {
    MatScale(m_M, alpha);
  }

  // scale and sum to the current matrix:
  inline void add(const mat& toBeAdded, const double alpha) {
    MatAXPY(m_M, alpha, toBeAdded.m_M, DIFFERENT_NONZERO_PATTERN);
  }

  // checking if the matrices are equal:
  inline bool operator == (mat toCheck){
    PetscBool isIt;
    MatEqual(m_M, toCheck.M(), &isIt);
    return isIt;
  }

  // computing the Frobenious norm:
  inline double norm(){
    double toReturn;
    MatNorm(m_M, NORM_FROBENIUS,&toReturn);
    return toReturn;
  }

  // Assign block to a given position
  inline void assignBlock(mat A, unsigned int start_row, unsigned int end_row, unsigned int start_col, unsigned int end_col) {
    assert(A.nRows()==end_row-start_row);
    assert(A.nCols()==end_col-start_col);

    for (unsigned int iRow=start_row; iRow<end_row; iRow++) {
      unsigned int local_row = iRow-start_row;
      for (unsigned int iCol=start_col; iCol<end_col; iCol++) {
        unsigned int local_col = iCol-start_col;

        double value = A.getMatEl(local_row, local_col);
        if (fabs(value)!=0.) {
          setMatEl(iRow, iCol, value);
        }
      }
    }
    finalize();
  }

  // Extract sub-matrix: index C convention ! (upper index excluded).
  mat extractSubmatrix(unsigned int iRow_low, unsigned int iRow_up, unsigned int iCol_low, unsigned int iCol_up){
    const unsigned int n_rows = iRow_up - iRow_low;
    const unsigned int n_cols = iCol_up - iCol_low;
    mat output(n_rows, n_cols, m_comm);
    for(unsigned int i=0; i<n_rows; i++){
      unsigned int iRow = i + iRow_low;
      for(unsigned int j=0; j<n_cols; j++){
        unsigned int iCol = j + iCol_low;
        double val = this->getMatEl(iRow,iCol);
        output.setMatEl(i,j,val);
      }
    }
    output.finalize();
    return output;
  }


  // I/O operations:

  // saving a matrix in binary format:
  inline void save(string fileName){
    PetscViewer viewer;
    PetscViewerBinaryOpen(m_comm, fileName.c_str(), FILE_MODE_WRITE, &viewer);
    MatView(m_M, viewer);
    PetscViewerDestroy(&viewer);
  }

  // loading a matrix in binary format:
  inline void load(string fileName){
    PetscViewer viewer;
    PetscViewerBinaryOpen(m_comm, fileName.c_str(), FILE_MODE_READ, &viewer);
    MatCreate(m_comm, &m_M);
    MatLoad(m_M, viewer);
    PetscViewerDestroy(&viewer);
  }

  // loading a matrix in binary format:
  inline void load(const string& filename, const MPI_Comm& comm) {
    m_comm = comm;
    PetscViewer viewer;
    PetscViewerBinaryOpen(m_comm, filename.c_str(), FILE_MODE_READ, &viewer);
    MatCreate(m_comm, &m_M);
    MatLoad(m_M, viewer);
    PetscViewerDestroy(&viewer);
    int nRows, nCols;
    MatGetSize(m_M, &nRows, &nCols);
    m_nRows = nRows;
    m_nCols = nCols;
    this->finalize();
  }

};



#endif
