// Header of linear algebra operations:

#ifndef linearAlgebraOperations_h
#define linearAlgebraOperations_h

#include "mat.h"
#include "vec.h"


// 0 -- MPI and CONVERSIONS --

// 0.1 -- findNonZero indices of a vector:
vector<int> findNonZero(vector<double>& v);


// 0.2 -- reduce a vector to its non-zero elements:
vector<double> nonZero(vector<double>& v);


//0.3 converting vector<int> into PetscInt array:
void vector2PetscInt(vector<int> vIn, PetscInt* array);

// 0.4 converting vector<double> into PetscScalar array:
void vector2PetscScalar(vector<double> vIn, PetscScalar* array);

// 0.5 converting a PetscInt array into vector<int>
void PetscInt2vector(const PetscInt* array, vector<int>& vOut );

// 0.6 converting a PetscScalar array into a vector<double>
void PetscScalar2vector(const PetscScalar* array, vector<double>& vOut );

// convert a vector<vec> into mat:
mat vectorVec2Mat(const vector<vec>&);

// convert a mat into a vector<vec>:
vector<vec> mat2vectorVec(const mat&);

// 0.7 print std vector on proc 0:
void printStdVec(vector<double>& toBePrinted, MPI_Comm theComm);

// 0.7 print std vector on proc 0:
void printStdVecInt(vector<int>& toBePrinted, MPI_Comm theComm);

// get the largest element (local on the proc)
void getLargestElem(const vector<double>& vec, int& jmax, double& sigmamax);


//  merge two indices vectors, local on the proc
void mergeSortInt(vector<int>& a, vector<int>& b, vector<int>& c);






// I -- OPERATIONS WITH VECTORS --


// I.1 - Initialize a Petsc Vec, given its pointer and its size:
void initVec(Vec& x, int itsSize, MPI_Comm theComm);


// I.1.1 - Overloaded, copying the structure of another vector, b:
void initVec(Vec& toInit, Vec& b);


/* setVecEl : setting one or more Elements
  external elements MUST be correctly available by the Procs !
*/

// I.2.1 Vec set values: just one element Proc 0 is doing the operation if PAR=false
void setVecEl(Vec& x, int ind, double val, MPI_Comm theComm, bool PAR);


// I.2.2 Vec set values: multiple values either in parallel or not:
void setVecEl(Vec& x, vector<double> val, MPI_Comm theComm,  bool PAR);


// I.2.3 set Vector from index low to high included equal to a given vector<double>
void setVecEl(Vec& x, int low, int high, vector<double> val, MPI_Comm theComm,  bool FIN, bool PAR);


// I.2.4 Set Elements with local vectors:
void setVecLocEl(Vec& x, vector<double> val, MPI_Comm theComm);


// I.3 Finalize the Assembly
void finalizeVec(Vec& x);


/* Global indices in 1D ordering to be passed! But VecGet is only local to proc!
   In these functions, result is broadcast to all the Procs if BROADCAST=true! Collective call;
*/

// I.4.0 generic and complete getVecEl
inline void getVecEl(Vec x, int ind, double& val, int& theRank, MPI_Comm theComm, bool BROADCAST);


// I.4.0.b Get just one value and keep it local to the proc, collective call:
/* explicitly no broadcasting, faster
  - output: val and theRank (id of the proc having val)
*/
inline void getVecElNoBroadcast(Vec x, int ind, double& val, int& theRank, MPI_Comm theComm);


// I.4.1 Get just one value and pass it to all Procs!:
/* overloaded, output is directly the double, all the procs have it */
double getVecEl(Vec x, int ind, MPI_Comm theComm);


// overloaded:
void getVecEl(Vec x, int ind, double& val, MPI_Comm theComm);


// I.4.2 Get a subset of the vector: from low to high included and pass it to all
void getVecEl(Vec x, int low, int high, vector<double> val, MPI_Comm theComm);

// overloaded, multiple values
vector<double> getVecEl(Vec x, int low, int high, MPI_Comm theComm);



/* transforming shared vector<double> to Vec and viceversa */

// I.5.1 Vec2vector converter
void Vec2vector(Vec& In, vector<double>& Out, MPI_Comm theComm);

// I.5.2 vector2Vec => passing just the pointer
void vector2Vec(vector<double> In, Vec& Out, MPI_Comm theComm);

vec vector2vec(const std::vector<double>& v, const MPI_Comm& comm);

std::vector<double> vec2vector(const vec& v);

mat vector2mat(const std::vector<double>& v, const MPI_Comm& comm);

mat vector2mat(const std::vector<vec>& v, const MPI_Comm& comm);

std::vector<vec> mat2vector(const mat& v);


// 0 -- copyVecFrom:
vec operator ~ (const vec&);

/* 1 -- sum of two vecs
  - overloaded operator
*/
vec operator + (vec &v1, vec &v2);

/* 1.b -- difference of two vecs
  - overloaded operator
*/
vec operator - (vec &v1, vec &v2);

vec lin_comb(const vector<vec>& U, const vec& a);
vec lin_comb(vector<vec>, vector<double>);
vec lin_comb(const vector<vec>&, const mat&, const unsigned int);
vector<vec> lin_comb(vector<vec>, mat);
vector<vec> lin_comb(vector<vec>, vector<vec>);
vec lin_comb(const unsigned int nDof_x, const unsigned int nDof_y, const vector<vec>& terms_x, const vector<double>& coeffs, const vector<vec>& terms_y, const MPI_Comm& comm, const unsigned int jTerm);
vector<vec> lin_comb(const unsigned int nDof_x, const unsigned int nDof_y, const vector<vec>& terms_x, const vector<double>& coeffs, const vector<vec>& terms_y, const MPI_Comm& comm);


/* 2 -- scalar product
  - inputs: two vec
  - output: their l-2 scalar product
*/
double scalProd(const vec& v1, const vec& v2);


/* 2.1 -- scalar product, overloaded
  - inputs: two vec, a mass matrix
  - output: their scalar product
*/
double scalProd(vec& v1, vec& v2, mat mass);

double norm(const vec& v);


/* 3 -- pointwise multiplication
  - function:
  - Remark, alternative syntaxes:
  for(unsigned int iDof=0; iDof<v1.size(); iDof++){

    double val1 = v1.getVecEl(iDof);
    double val2 = v2.getVecEl(iDof);
    double val = val1 * val2;
    toBeReturned.setVecEl(iDof, val);

    // or through the Proxy:
    toBeReturned(iDof) = v1(iDof) * v2(iDof);
  }
*/
vec pointWiseMult(vec& v1, vec&v2);


/* Matrix vector Product
  - input: mat A, vec v;
  - output: y = A v
*/
vec operator * (mat A, vec v);

/* Matrix scaling by a factor
  - input: mat A, vec v;
  - output: y = A v
*/
mat operator * (double alpha, mat A);


/* plain function */
void matVecProd(const mat& A, const vec& x, vec& y);

// Perform v = A*u
void MatMult(const mat& A, const vec& u, vec& v);

// Perform v = A*u
void MatMult(const std::vector<vec>& A, const vec& u, vec& v);

// Perform v = A'*u
void MatMultTranspose(const std::vector<vec>& A, const vec& u, vec& v);

// Perform w = v+A*u
void MatMultAdd(const mat& A, const vec& u, const vec& v, vec& w);

// Perform C = A*B
void MatMatMult(const mat& A, const std::vector<vec>& B, std::vector<vec>& C);

// Perform C = A*B
std::vector<vec> MatMatMult(const mat& A, const std::vector<vec>& B);

// Perform C = A'*B
void MatTransposeMatMult(const std::vector<vec>& A, const std::vector<vec>& B, mat& C);

// Perform C = A'*B
mat MatTransposeMatMult(const std::vector<vec>& A, const std::vector<vec>& B);

/* get matrix column */
vec getCol(mat A, unsigned int iCol);



/* create a vector, fill it with 0 */
vec zeros(unsigned int, MPI_Comm);

/* create a vector, fill it with 1 */
vec ones(unsigned int, MPI_Comm);


/* initialise a vector with random entries
  - input: the size, the communication
  - output: the random vector
*/
vec rand(unsigned int, MPI_Comm);



// II - OPERATIONS for MATRICES:

// II.1 - Initialize a Matrix, given its pointer and its sizes:
void initMat(Mat& A, int nRows, int nCols, MPI_Comm theComm);

// II.1.2 overloaded share structure and non-zero pattern:
void initMat(Mat& matToInit, Mat A);



/* set Matrix Elements or rows or cols */

// II.2.1 Setting just one element:
void setMatEl(Mat& A, int indRow, int indCol, double val, MPI_Comm theComm, bool PAR);


// II.2.2 Setting a whole matrix by providing a dense full Table
void setMatEl(Mat& A, vector<vector<double>* > theMat, MPI_Comm theComm,  bool PAR);



// II.2.3 Assemble the Matrix by providing a table containing the pattern and a table containing the non-zero entries
void setMatEl(Mat& A, vector<vector<int>* > pattern, vector<vector<double>* > theEntries, MPI_Comm theComm,  bool PAR);


// II.2.4 Set Matrix by providing local dense submatrices of the right size.
void setMatLocEl(Mat& A, vector<vector<double>* > theMat, MPI_Comm theComm);


// II.2.5 Set Matrix by providing local sparse submatrices of the right size.
void setMatLocEl(Mat& A, vector<vector<int>*> pattern, vector<vector<double>* > theEntries, MPI_Comm theComm);



// II.2.6 Set a Matrix Row by providing a dense vector: FINALIZE OUTSIDE!
void setMatRow(Mat&A, int idRow, vector<double>& theRow, MPI_Comm theComm, bool PAR);



// II.2.7 Set a matrix row by providing a sparse vector: FINALIZE OUTSIDE!
void setMatRow(Mat&A, int idRow, vector<int>& colInd, vector<double>& theRow, MPI_Comm theComm, bool PAR);


// II.3 Finalizing the Assembly
void finalizeMat(Mat& A);




/* Routines to get matrix elements with a collective call */

// II.4.0 get matrix element and decide if broadcasting or not:
void getMatEl(Mat A, int idRow, int idCol, double& val, int& theRank, MPI_Comm theComm, bool BROADCAST);

// II.4.0.b collective call, explicitly no broadcast, faster
void getMatEl(Mat A, int idRow, int idCol, double& val, int& theRank, MPI_Comm theComm);



// II.4 Get just one element, specified by idRow and idCol, global indices:
double getMatEl(Mat A, int idRow, int idCol, MPI_Comm theComm);


/* Generic Routines for matrix manipulation */


// Setting a column equal to a vec. Have to finalize outside!
void setMatCol(Mat& A, int idCol, Vec v, MPI_Comm theComm);


// Computing the transpose of a matrix, returning a finalized matrix
Mat transpose(Mat A, MPI_Comm theComm);

// overloaded to take existing pointer:
void transpose(Mat A, Mat& theTranspose, MPI_Comm theComm);

// function that returns mat:
mat transpose(mat A);



// Computing the hermitian of a matrix, returning a finalized matrix
Mat hermitian(Mat A, MPI_Comm theComm);

// overloaded to take existing pointer:
void hermitian(Mat A, Mat& theTranspose, MPI_Comm theComm);

// function that returns the hermitian of a mat:
mat hermitian(mat A);


// Identity:

// overloaded to take an existing pointer:
void eye(Mat& I, int n, MPI_Comm theComm);

// function that returns a mat:
mat eye(unsigned int n, MPI_Comm theComm);


/* reshape function for Petsc matrices
  - inputs: the matrix to be reshaped, the dimensions
  - output: a matrix
*/
inline Mat reshape(Mat& matToReshape, unsigned int newRows, unsigned int newCols, MPI_Comm theComm);

// reshape function for a mat object, that returns a mat object:
mat reshape(mat A, unsigned int newRows, unsigned int newCols);


/* vectorise a Petsc matrix
  - inputs: the matrix to be reshaped, the dimensions
  - output: a vector
*/
inline Vec vectoriseMat(Mat& toBeVectorised, MPI_Comm theComm);


// function that returns a vec
vec vectoriseMat(mat& toBeVectorised);



// produce a diagonal matrix given a vector:
void diag(Mat& D, Vec d, MPI_Comm theComm);

// overloaded to provide a matrix:
Mat diag(Vec d, MPI_Comm theComm);

// function that returns a mat object:
mat diag(const vec& d);

// function that, given a mat, returns a vec whose entries are the diagonal
vec diag(const mat& A);

// summing two mat to provide a mat:
mat operator + (mat m1, mat m2);

// multiplying two matrices to get a mat:
mat operator * (mat m1, mat m2);

// function:
void matMatMult(mat& m1, mat& m2, mat& result);


// I/O:

// saving a vector in binary format:
void saveVec(const vec& v, const string& fileName);

// loading a vector in binary format:
vec loadVec(const string& fileName, const MPI_Comm& theComm);

// saving a vector in ASCII format:
void saveVecASCII(vec& v, string fileName);

// loading a vector in ASCII format:
vec loadVecASCII(string fileName, MPI_Comm theComm);

// saving a matrix in binary format:
void saveMat(const mat& toBeSaved, const string& fileName);

// loading a matrix in binary format:
mat loadMat(const string& fileName, const MPI_Comm& theComm);

// loading a matrix saved from freefem++:
mat loadFreefemMatrix(string fileName, MPI_Comm theComm);

// loading a matrix saved from freefem++:
mat loadFreefemCOOMatrix(string fileName, MPI_Comm theComm);

// saving a matrix in freefem COO format:
void saveFreefemCOOMatrix(mat A, string filename);


// end of file:
#endif
