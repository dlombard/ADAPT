// Header file for the solution of eigenvalue problems.

#ifndef eigenSolver_h
#define eigenSolver_h

#include "mat.h"
#include "vec.h"
#include "linearAlgebraOperations.h"


class eigenSolver{
private:
  EPS m_eps;
  vec m_v;
  double m_lambda;
  vector<vec> m_eigenVecs;
  vector<double> m_eigenVals;
  vector<vec> m_imagVecs;
  vector<double> m_imagVals;
  bool m_is_v_Computed;
  bool m_is_HEP;

public:
  eigenSolver(){};
  ~eigenSolver(){};
  inline void clear(){
    /*if(m_is_v_Computed){
      m_v.clear();
    }
    for(unsigned int jEig=0; jEig<m_eigenVecs.size(); jEig++){
      //m_eigenVecs[jEig].clear();
    }
    if(!m_is_HEP){
      for(unsigned int jEig=0; jEig<m_imagVecs.size(); jEig++){
        m_imagVecs[jEig].clear();
      }
      m_imagVals.clear();
    }
    m_eigenVals.clear();*/

    EPSDestroy(&m_eps);
  }

  // Overlaoded constructor, generic problem, default tolerances:
  eigenSolver(mat A){
    m_is_v_Computed = false; // compute all of them, not just the first
    PetscErrorCode ierr;
    ierr = EPSCreate(A.comm(),&m_eps);
    ierr = EPSSetOperators(m_eps, A.M(), NULL);
    ierr = EPSSetProblemType(m_eps, EPS_NHEP);
    ierr = EPSSetType(m_eps, EPSKRYLOVSCHUR);
    ierr = EPSSetFromOptions(m_eps);

    EPSSolve(m_eps);
    int nconv;
    EPSGetConverged(m_eps, &nconv);

    int nOfEig = (A.nCols() < nconv) ? A.nCols() : nconv;
    if(nOfEig==0){PetscPrintf(PETSC_COMM_WORLD,"0 Eig converged!\n");}

    m_eigenVecs.resize(nOfEig);
    m_eigenVals.resize(nOfEig);
    m_imagVecs.resize(nOfEig);
    m_imagVals.resize(nOfEig);
    for(int j=0; j<nOfEig; j++) {
      double realPart;
      double imagPart;
      Vec realEV; initVec(realEV, A.nRows(), A.comm()); finalizeVec(realEV);
      Vec imagEV; initVec(imagEV, A.nRows(), A.comm()); finalizeVec(imagEV);

      EPSGetEigenpair(m_eps, j, &realPart, &imagPart, realEV, imagEV);
      m_eigenVals[j] = realPart;
      m_imagVals[j] = imagPart;

      /*vec jThEigenVec;
      jThEigenVec.setComm(A.comm());
      jThEigenVec.setVector(realEV);
      jThEigenVec.finalize();
      m_eigenVecs[j] = jThEigenVec;*/
      m_eigenVecs[j].setComm(A.comm());
      m_eigenVecs[j].setVector(realEV);
      m_eigenVecs[j].finalize();


      /*vec jThImagVec;
      jThImagVec.setComm(A.comm());
      jThImagVec.setVector(imagEV);
      jThImagVec.finalize();
      m_imagVecs[j] = jThImagVec;*/
      m_imagVecs[j].setComm(A.comm());
      m_imagVecs[j].setVector(imagEV);
      m_imagVecs[j].finalize();
    }
  }

  // overloaded for a generalised non-hermitian eigenvalue problem:
  eigenSolver(mat A, mat mass){
    PetscErrorCode ierr;
    ierr = EPSCreate(A.comm(), &m_eps);
    ierr = EPSSetOperators(m_eps, A.M(), mass.M());
    ierr = EPSSetProblemType(m_eps, EPS_GNHEP);
    ierr = EPSSetType(m_eps, EPSKRYLOVSCHUR);
    ierr = EPSSetFromOptions(m_eps);

    EPSSolve(m_eps);
    int nconv;
    EPSGetConverged(m_eps, &nconv);

    int nOfEig = (A.nCols() < nconv) ? A.nCols() : nconv;
    if(nOfEig==0){PetscPrintf(PETSC_COMM_WORLD,"0 Eig converged!\n");}

    m_eigenVecs.resize(nOfEig);
    m_eigenVals.resize(nOfEig);
    m_imagVecs.resize(nOfEig);
    m_imagVals.resize(nOfEig);
    for(int j=0; j<nOfEig; j++) {
      double realPart;
      double imagPart;
      Vec realEV; initVec(realEV, A.nRows(), A.comm()); finalizeVec(realEV);
      Vec imagEV; initVec(imagEV, A.nRows(), A.comm()); finalizeVec(imagEV);

      EPSGetEigenpair(m_eps, j, &realPart, &imagPart, realEV, imagEV);
      m_eigenVals[j] = realPart;
      m_imagVals[j] = imagPart;

      vec jThEigenVec;
      jThEigenVec.setComm(A.comm());
      jThEigenVec.setVector(realEV);
      m_eigenVecs[j] = jThEigenVec;

      vec jThImagVec;
      jThImagVec.setComm(A.comm());
      jThImagVec.setVector(imagEV);
      m_imagVecs[j] = jThImagVec;
    }
  }


  // -- INIT --

  // Using Default Tolerances:
  void init(mat A, EPSProblemType type){
    if (type == EPS_HEP){
      m_is_HEP = true;
    }
    PetscErrorCode ierr;
    ierr = EPSCreate(A.comm(),&m_eps);
    ierr = EPSSetOperators(m_eps, A.M(), NULL);
    ierr = EPSSetProblemType(m_eps,type);
    ierr = EPSSetType(m_eps, EPSKRYLOVSCHUR);
    ierr = EPSSetFromOptions(m_eps);
  }

  // overloading by specifying Tolerances and number of eigenvalues:
  void init(mat A, EPSProblemType type, int nev, double tol){
    if (type == EPS_HEP){
      m_is_HEP = true;
    }
    PetscErrorCode ierr;
    ierr = EPSCreate(A.comm(), &m_eps);
    ierr = EPSSetOperators(m_eps, A.M(), NULL);
    ierr = EPSSetProblemType(m_eps, type);
    ierr = EPSSetType(m_eps, EPSKRYLOVSCHUR);
    ierr = EPSSetTolerances(m_eps, tol, PETSC_DEFAULT);
    ierr = EPSSetDimensions(m_eps, nev, PETSC_DEFAULT, PETSC_DEFAULT);
    ierr = EPSSetFromOptions(m_eps);
  }

  // overloading by adding mass: type = EPS_GHEP, EPS_GHIEP, EPS_GNHEP, EPS_PGNHEP
  void init(mat A, Mat Mass, EPSProblemType type){
    if (type == EPS_HEP){
      m_is_HEP = true;
    }
    PetscErrorCode ierr;
    ierr = EPSCreate(A.comm(), &m_eps);
    ierr = EPSSetOperators(m_eps, A.M(), Mass);
    ierr = EPSSetProblemType(m_eps, type);
    ierr = EPSSetType(m_eps, EPSKRYLOVSCHUR);
    ierr = EPSSetFromOptions(m_eps);
  }

  // overloading by specifying Tolerances and number of eigenvalues:
  void init(mat A, mat mass, EPSProblemType type, int nev, double tol){
    if (type == EPS_HEP){
      m_is_HEP = true;
    }
    PetscErrorCode ierr;
    ierr = EPSCreate(A.comm(), &m_eps);
    ierr = EPSSetOperators(m_eps, A.M(), mass.M());
    ierr = EPSSetProblemType(m_eps, type);
    ierr = EPSSetType(m_eps, EPSKRYLOVSCHUR);
    ierr = EPSSetTolerances(m_eps, tol, PETSC_DEFAULT);
    ierr = EPSSetDimensions(m_eps, nev, PETSC_DEFAULT, PETSC_DEFAULT);
    ierr = EPSSetFromOptions(m_eps);
  }

  // overloading with solvertype
  void init(mat A, EPSProblemType pbType, EPSType solverType){
    if (pbType == EPS_HEP){
      m_is_HEP = true;
    }
    PetscErrorCode ierr;
    ierr = EPSCreate(A.comm(), &m_eps);
    ierr = EPSSetOperators(m_eps, A.M(), NULL);
    ierr = EPSSetProblemType(m_eps, pbType);
    ierr = EPSSetType(m_eps, solverType);
    ierr = EPSSetFromOptions(m_eps);
  }


  // -- SOLVERS: --

  // solving one term hermitian EPS :
  void solveFirstPairHermEPS(mat A){

    EPSSolve(m_eps);
    int nconv;
    EPSGetConverged(m_eps, &nconv);

    int nOfEig = (A.nCols() < nconv) ? A.nCols() : nconv;
    if(nOfEig==0){PetscPrintf(PETSC_COMM_WORLD,"0 Eig converged!\n");}

    m_v.setComm(A.comm());

    Vec v;
    initVec(v, A.nRows(), A.comm());
    finalizeVec(v);
    double imagPart;
    Vec imagEV;
    initVec(imagEV, A.nRows(), A.comm());
    finalizeVec(imagEV);

    EPSGetEigenpair(m_eps, 0, &m_lambda, &imagPart, v, imagEV);
    m_v.setVector(v);
  }

  // synthetic function to compute the largest singular value and its eigenvector:
  void solveOneTermHermitian(mat A){
    m_is_v_Computed = true;
    PetscErrorCode ierr;
    ierr = EPSCreate(A.comm(),&m_eps);
    ierr = EPSSetOperators(m_eps, A.M(), NULL);
    ierr = EPSSetProblemType(m_eps, EPS_HEP);
    ierr = EPSSetType(m_eps, EPSKRYLOVSCHUR);
    ierr = EPSSetFromOptions(m_eps);
    EPSSolve(m_eps);
    int nconv;
    EPSGetConverged(m_eps, &nconv);

    int nOfEig = (A.nCols() < nconv) ? A.nCols() : nconv;
    if(nOfEig==0){PetscPrintf(PETSC_COMM_WORLD,"0 Eig converged!\n");}

    m_v.setComm(A.comm());

    Vec v;
    initVec(v, A.nRows(), A.comm());
    finalizeVec(v);
    double imagPart;
    Vec imagEV;
    initVec(imagEV, A.nRows(), A.comm());
    finalizeVec(imagEV);

    EPSGetEigenpair(m_eps, 0, &m_lambda, &imagPart, v, imagEV);
    m_v.setVector(v);
  }


  // Solving Hermitian eigenvalue problem:
  void solveHermEPS(mat A){
    EPSSolve(m_eps);
    int nconv;
    EPSGetConverged(m_eps, &nconv);

    int nOfEig = (A.nCols() < nconv) ? A.nCols() : nconv;
    if(nOfEig==0){PetscPrintf(PETSC_COMM_WORLD,"0 Eig converged!\n");}

    m_eigenVecs.resize(nOfEig);
    m_eigenVals.resize(nOfEig);
    for(int j=0; j<nOfEig; j++) {
      double realPart;
      double imagPart;
      Vec realEV; initVec(realEV, A.nRows(), A.comm()); finalizeVec(realEV);
      Vec imagEV; initVec(imagEV, A.nRows(), A.comm()); finalizeVec(imagEV);

      EPSGetEigenpair(m_eps, j, &realPart, &imagPart, realEV, imagEV);
      m_eigenVals[j] = realPart;

      vec jThEigenVec;
      jThEigenVec.setComm(A.comm());
      jThEigenVec.setVector(realEV);
      m_eigenVecs[j] = jThEigenVec;
    }
  }

  // Solving Generic eigenvalue problem:
  void solveEPS(mat A){
    EPSSolve(m_eps);
    int nconv;
    EPSGetConverged(m_eps, &nconv);

    int nOfEig = (A.nCols() < nconv) ? A.nCols() : nconv;
    if(nOfEig==0){PetscPrintf(PETSC_COMM_WORLD,"0 Eig converged!\n");}

    m_eigenVecs.resize(nOfEig);
    m_eigenVals.resize(nOfEig);
    m_imagVecs.resize(nOfEig);
    m_imagVals.resize(nOfEig);
    for(int j=0; j<nOfEig; j++) {
      double realPart;
      double imagPart;
      Vec realEV; initVec(realEV, A.nRows(), A.comm()); finalizeVec(realEV);
      Vec imagEV; initVec(imagEV, A.nRows(), A.comm()); finalizeVec(imagEV);

      EPSGetEigenpair(m_eps, j, &realPart, &imagPart, realEV, imagEV);
      m_eigenVals[j] = realPart;
      m_imagVals[j] = imagPart;

      vec jThEigenVec;
      jThEigenVec.setComm(A.comm());
      jThEigenVec.setVector(realEV);
      m_eigenVecs[j] = jThEigenVec;

      vec jThImagVec;
      jThImagVec.setComm(A.comm());
      jThImagVec.setVector(imagEV);
      m_imagVecs[j] = jThImagVec;
    }
  }


  // -- ACCESS FUNCTIONS: --
  inline vec v(){return m_v;}
  inline double lambda(){return m_lambda;}

  inline vector<vec> eigenVecs(){return m_eigenVecs;}
  inline vec eigenVecs(unsigned int j){return m_eigenVecs[j];}
  inline vector<double> eigenVals(){return m_eigenVals;}
  inline double eigenVals(unsigned int j){return m_eigenVals[j];}

  inline vector<vec> imagVecs(){return m_imagVecs;}
  inline vec imagVecs(unsigned int j){return m_imagVecs[j];}
  inline vector<double> imagVals(){return m_imagVals;}
  inline double imagVals(unsigned int j){return m_imagVals[j];}

};




// end of file
#endif
