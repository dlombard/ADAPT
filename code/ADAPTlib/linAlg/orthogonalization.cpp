#include "orthogonalization.h"


/* overloaded constructor:
  - input: a set of vec;
  - output: a set of orthonormal vec m_Q and the matrix m_R
*/
qr::qr(vector<vec> V) {
  MPI_Comm theComm = V[0].comm();
  unsigned int nRows = V[0].size();
  unsigned int nCols = V.size();
  m_R.init(nCols, nCols, theComm);
  m_Q.resize(nCols);

  // Create a set of vectors, copy the vecs V:
  vector<vec> theCols;
  theCols.resize(V.size());
  for(int iVec=0; iVec<nCols; iVec++){
    vec iThVec;
    iThVec.copyVecFrom(V[iVec]);
    theCols[iVec] = iThVec;
  }

  //Starting the MGS algorithm:
  for(unsigned int i=0; i<nCols; i++){
    double r_ii = theCols[i].norm();
    double coeff = 0.0;
    if(r_ii>m_tolQR){
      m_toRetrieve.push_back(i);
      m_R.setMatEl(i, i, r_ii);

      coeff = 1.0/r_ii;
      vec q_i;
      q_i.copyVecFrom(theCols[i]);
      q_i *= coeff;
      m_Q[i] = q_i;
    }
    else{
      //setMatEl(R, i, i, 0.0, theComm); //uncomment to obtain dense R
    }
    // Computing the off-diagonal part of R
    for(int j=i+1; j<nCols; j++){
      double r_ij;
      if(coeff>0){
        double r_ij = scalProd(theCols[i], theCols[j]);
        r_ij = r_ij*coeff;
        m_R.setMatEl(i,j,r_ij);
        double scale = -1.0*r_ij*coeff;
        theCols[j].sum(theCols[i], scale);
      }
      else{
        //setMatEl(R,i,j,0.0,theComm); //uncomment to obtain a dense R
      }
    }
  }
  m_R.finalize();

  // free the memory:
  for(unsigned int iVec=0; iVec<nCols; iVec++){
    theCols[iVec].clear();
  }

}


/* overloaded constructor:
  - input: a matrix mat;
  - output: the matrix m_Qmat and the matrix m_R
*/
qr::qr(mat A){
  m_has_Computed_Qmat = true;
  m_R.init(A.nCols(), A.nCols(), A.comm());
  m_Qmat.init(A.nRows(), A.nCols(), A.comm());

  // Create a mat, copy of A:
  vector<vec> theCopy(A.nCols());
  for(unsigned int iCol=0; iCol<A.nCols(); iCol++){
    vec theIthCol = A.getCol(iCol);
    theCopy[iCol] = theIthCol;
  }

  //Starting the MGS algorithm:
  for(unsigned int i=0; i<A.nCols(); i++){
    double r_ii = theCopy[i].norm();
    // cout << r_ii << endl;
    double coeff = 0.0;
    if(r_ii>m_tolQR){
      m_toRetrieve.push_back(i);
      m_R.setMatEl(i, i, r_ii);

      coeff = 1.0/r_ii;
      vec q_i;
      q_i.copyVecFrom(theCopy[i]);
      // q_i.print();
      q_i *= coeff;
      // q_i.print();
      for(unsigned int iRow=0; iRow<A.nRows(); iRow++){
        double val = q_i.getVecEl(iRow);
        m_Qmat.setMatEl(iRow, i, val);
      }
      // m_Qmat.print();
      q_i.clear();
    }
    else{
      //setMatEl(R, i, i, 0.0, theComm); //uncomment to obtain dense R
    }
    // Computing the off-diagonal part of R
    for(int j=i+1; j<A.nCols(); j++){
      double r_ij;
      if(coeff>0){
        double r_ij = scalProd(theCopy[i], theCopy[j]);
        r_ij = r_ij*coeff;
        m_R.setMatEl(i,j,r_ij);
        double scale = -1.0*r_ij*coeff;
        theCopy[j].sum(theCopy[i], scale);
      }
      else{
        //setMatEl(R,i,j,0.0,theComm); //uncomment to obtain a dense R
      }
    }
  }
  m_R.finalize();
  m_Qmat.finalize();

  // free the memory:
  for(unsigned int iCol=0; iCol<A.nCols(); iCol++){
    theCopy[iCol].clear();
  }

}


/* Thin QR factorization
  - input: a set of vec V
  - output: the set of orthonormal vec m_Q and the matrix m_R
*/
qr::qr(const vector<vec>& V, const MPI_Comm& theComm) {
  MGS(V, m_Q, m_R, m_tolQR, theComm);
}


/* Thin modified Gram-Schmid algorithm
  - input: a set of vec V, a set of orthonormal vec Q, an unassembled matrix R, a tolerance tol, a communicator theComm
*/
void MGS(const vector<vec>& V, vector<vec>& Q, mat& R, const double tol, const MPI_Comm& theComm) {

  assert(0.0<tol);

  unsigned int k=0;
  double tmp;
  std::vector<bool> isZero(V.size(), false);
  std::vector<unsigned int> I, J;
  std::vector<double> val;

  Q.resize(V.size());
  for(unsigned int i=0; i<Q.size(); ++i) {
    Q[i].copyVecFrom(V[i]);
  }

  //Starting the MGS algorithm:
  for (unsigned int i=0; i<Q.size(); ++i) {
    tmp = Q[k].norm();
    if (tmp>tol) {
      I.push_back(k);
      J.push_back(i);
      val.push_back(tmp);
      Q[i] *= (1.0/tmp);
      for (unsigned int j=i+1; j<Q.size(); ++j) {
        tmp = scalProd(Q[i], Q[j]);
        if (fabs(tmp)!=0.0) {
          I.push_back(k);
          J.push_back(j);
          val.push_back(tmp);
          Q[j].add(Q[i], -tmp);
        }
      }
      ++k;
    } else {
      isZero[i] = true;
    }
  }

  for (unsigned int i=Q.size()-1; i<Q.size(); --i) {
    if (isZero[i]) {
      Q.erase(std::next(Q.begin(), i));
    }
  }

  R.init(Q.size(), V.size(), theComm);
  for (unsigned int i=0; i<val.size(); ++i) {
    R.setMatEl(I[i], J[i], val[i]);
  }
  R.finalize();

}


// -- II -- Applications of QR


/* least square approximation
 - inputs: a target, a basis of vectors
 - outputs: the best reconstruction, the error in l^2 norm.
*/
void ls_approx(const vec& target, const vector<vec>& basis, vec& approx, double& err){
  // orthonormalise the basis:
  vector<vec> Q;
  mat R;
  MGS(basis, Q, R, 1.0e-12, basis[0].comm());

  // compute the rhs:
  const unsigned int n_basis = Q.size();
  vec rhs(n_basis, basis[0].comm());
  for(unsigned int iComp=0; iComp<n_basis; iComp++){
    double val = scalProd(target, Q[iComp]);
    rhs.setVecEl(iComp,val);
  }
  rhs.finalize();
  // compute the coefficients:
  vec sol = R/rhs;
  approx = lin_comb(basis, sol);

  vec diff;
  diff.copyVecFrom(target);
  diff *= -1.0;
  diff += approx;
  err = diff.norm();
  diff.clear();

  // free the memory:
  R.clear();
  for(unsigned int iComp=0; iComp<n_basis; iComp++){
    Q[iComp].clear();
  }
  Q.clear();
  sol.clear();
  rhs.clear();
}
