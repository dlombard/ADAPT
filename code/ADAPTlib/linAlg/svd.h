// Header class for svd
#ifndef svd_h
#define svd_h

#include "orthogonalization.h"
#include "eigenSolver.h"

// Implementing external functions => computation of the svd out of class (without an object)
mat compute_cov_mat(const mat&);
mat compute_cov_mat(const vector<vec>&);
unsigned int estimate_truncation(const vector<double>&, double);

void compute_svd(const mat&, mat&, mat&, mat&, double);
void compute_svd(const vector<vec>&, vector<vec>&, vector<double>&, vector<vec>&, double);


// class svd:
class svd{
private:
  SVD m_svd;
  vec m_u,m_v;
  double m_sigma;
  // set of vectors:
  vector<double> m_S;
  vec m_Svec;
  // set of matrices:
  mat m_Umat, m_Vmat, m_Smat;
  // set of vecs:
  vector<vec> m_Umod;
  vector<vec> m_Vmod;

  bool m_use_svd_pt = true;
  bool m_has_Computed_mat = false;
  bool m_is_zero_rank = false;
  double tolQR = 2.22e-16;

public:
  svd(){};
  ~svd(){};
  void clear(){
    if(!m_is_zero_rank){
      if(!m_has_Computed_mat){
        m_S.clear();
      }else{
        m_Umat.clear();
        m_Vmat.clear();
        m_Smat.clear();
        m_Svec.clear();
      }

      if(m_Umod.size()>0){
        for(unsigned int iMod=0; iMod<m_Umod.size(); iMod++){
          m_Umod[iMod].clear();
        }
        m_Umod.clear();
      }

      if(m_Vmod.size()>0){
        for(unsigned int iMod=0; iMod<m_Vmod.size(); iMod++){
          m_Vmod[iMod].clear();
        }
        m_Vmod.clear();
      }

      if(m_use_svd_pt){
        SVDDestroy(&m_svd);
      }
    }
  }


  /* overloaded constructor
    - input: set of vec
    - output: sets of right and left singular vecs, singular values
  */
  svd(vector<vec> vectorSet){

    if(vectorSet.size()==0){
      m_is_zero_rank = true;
      return;
    }
    bool isTheSetEmpty = true;
    for(unsigned int iVec = 0; iVec<vectorSet.size(); iVec++){
      if(!vectorSet[iVec].isEmpty()){
        isTheSetEmpty = false;
      }
    }
    if(isTheSetEmpty){
      m_is_zero_rank = true;
      return;
    }


    m_has_Computed_mat = true;
    unsigned int nRows = vectorSet[0].size();
    unsigned int nCols = vectorSet.size();
    MPI_Comm theComm = vectorSet[0].comm();

    if(nRows>=nCols){
      // First step of LHC: compute a QR factorisation
      Mat Q,R;
      initMat(Q,nRows,nCols,theComm);
      finalizeMat(Q);
      initMat(R,nCols,nCols,theComm);
      finalizeMat(R);

      // Create a set of vectors, copy the elements of vectorSet:
      vector<Vec> theCols;
      theCols.resize(nCols);
      for(int idCol=0; idCol<nCols; idCol++){
        Vec iThCopy;
        initVec(iThCopy, nRows, theComm);
        VecCopy(vectorSet[idCol].x(), iThCopy);
        finalizeVec(iThCopy);
        theCols[idCol] = iThCopy;
      }

      //Starting the MGS algorithm:
      vector<int> toRetrieve;
      for(int i=0; i<nCols; i++){
        double r_ii;
        VecNorm(theCols[i],NORM_2,&r_ii);
        double coeff = 0.0;
        if(r_ii>tolQR){
          toRetrieve.push_back(i);
          setMatEl(R, i, i, r_ii, theComm, true);
          coeff = 1.0/r_ii;
          for(int idRow=0; idRow<nRows; idRow++){
            double qValue = getVecEl(theCols[i],idRow,theComm);
            qValue = qValue*coeff;
            setMatEl(Q, idRow, i, qValue,theComm, true);
          }
        }
        else{
          //setMatEl(R, i, i, 0.0, theComm); //uncomment to obtain dense R
        }
        // Computing the off-diagonal part of R
        for(int j=i+1;j<nCols;j++){
          double r_ij;
          if(coeff>0){
            VecDot(theCols[i],theCols[j],&r_ij);
            r_ij = r_ij*coeff;
            setMatEl(R,i,j,r_ij,theComm, true);
            double scale = -1.0*r_ij*coeff;
            VecAXPY(theCols[j],scale,theCols[i]);
          }
          else{
            //setMatEl(R,i,j,0.0,theComm); //uncomment to obtain a dense R
          }
        }
      }
      finalizeMat(Q);
      finalizeMat(R);

      int rank = toRetrieve.size();
      /* if the matrix is close to the zero matrix, return a
         zero rank in the form of a null pointer.
      */
      if (rank == 0){
        m_is_zero_rank = true;
        return;
      }

      // toRetrieve contains the Ids of the Columns of Q and the rows of R
      Mat redR;
      initMat(redR, rank, nCols, theComm);
      for(int idRow=0; idRow<rank; idRow++){
        int theRowIndex = toRetrieve[idRow];
        for(int idCol=idRow; idCol<nCols;idCol++){
          double valueToInsert = getMatEl(R, theRowIndex, idCol, theComm);
          setMatEl(redR, idRow, idCol, valueToInsert, theComm, true);
        }
      }
      finalizeMat(redR);
      // bidiagonalization of the reduced R
      //SVD svd;
      Mat Ur;
      Vec singVals;
      m_Vmat.setComm(theComm);
      initSVD(m_svd, redR, rank, 1.0e-18, theComm, SVDTRLANCZOS);
      solveSVD(m_svd, redR, Ur, m_Vmat.Mpt(), singVals, theComm);
      m_Vmat.getSize();
      m_Vmat.finalize();

      int nOfTriplets;
      VecGetSize(singVals, &nOfTriplets);
      m_S.resize(nOfTriplets);
      for(unsigned int iS=0; iS<nOfTriplets; iS++){
        m_S[iS] = getVecEl(singVals, iS, theComm);
      }

      // the right vectors:
      Mat redQ;
      initMat(redQ, nRows, rank, theComm);
      for(int idCol=0; idCol<rank; idCol++){
        Vec tmpL,tmpR;
        MatCreateVecs(Q,&tmpL,&tmpR);
        int toTake = toRetrieve[idCol];
        MatGetColumnVector(Q,tmpR,toTake);
        setMatCol(redQ, idCol, tmpR, theComm);
        VecDestroy(&tmpL);
      }
      finalizeMat(redQ);

      m_Umat.setComm(theComm);
      Mat theResult;
      initMat(theResult, nRows, nOfTriplets, theComm);
      finalizeMat(theResult);
      MatMatMult(redQ,Ur,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&theResult);
      m_Umat.setMatrix(theResult);

      m_Svec.init(nOfTriplets, theComm);
      for(unsigned int iS=0; iS<nOfTriplets; iS++){
        m_Svec.setVecEl( iS, m_S[iS], true);
      }
      m_Svec.finalize();
      m_Smat = diag(m_Svec);

      for(int i=0; i<nCols; i++){
        VecDestroy(&theCols[i]);
      }
      MatDestroy(&Q);
      MatDestroy(&R);
      MatDestroy(&redR);
      MatDestroy(&redQ);
    }
    // the case in which nCols > nRows
    else{
      // First step of LHC: compute a QR factorisation of the transposed
      Mat Q,R;
      initMat(Q,nCols,nRows,theComm);
      finalizeMat(Q);
      initMat(R,nRows,nRows,theComm);
      finalizeMat(R);

      // Create a set of vectors, copy the rows of A:
      vector<Vec> theRows;
      theRows.resize(nRows);
      for(int idRow=0; idRow<nRows; idRow++){
        Vec theIthRow;
        initVec(theIthRow, nCols, theComm);
        for(int iCol=0; iCol<nCols; iCol++){
          double val = vectorSet[iCol].getVecEl(idRow);
          setVecEl(theIthRow, iCol, val, theComm, true);
        }
        finalizeVec(theIthRow);
        theRows[idRow] = theIthRow;
      }


      //Starting the MGS algorithm:
      vector<int> toRetrieve;
      for(int i=0; i<nRows; i++){
        double r_ii;
        VecNorm(theRows[i],NORM_2,&r_ii);
        double coeff = 0.0;
        if(r_ii>tolQR){
          toRetrieve.push_back(i);
          setMatEl(R, i, i, r_ii, theComm, true);
          coeff = 1.0/r_ii;
          for(int idCol=0; idCol<nCols; idCol++){
            double qValue = getVecEl(theRows[i],idCol,theComm);
            qValue = qValue*coeff;
            setMatEl(Q, idCol, i, qValue,theComm, true);
          }
        }
        else{
          //setMatEl(R, i, i, 0.0, theComm); //uncomment to obtain dense R
        }
        // Computing the off-diagonal part of R
        for(int j=i+1;j<nRows;j++){
          double r_ij;
          if(coeff>0){
            VecDot(theRows[i],theRows[j],&r_ij);
            r_ij = r_ij*coeff;
            setMatEl(R,i,j,r_ij,theComm, true);
            double scale = -1.0*r_ij*coeff;
            VecAXPY(theRows[j],scale,theRows[i]);
          }
          else{
            //setMatEl(R,i,j,0.0,theComm); //uncomment to obtain a dense R
          }
        }
      }
      finalizeMat(Q);
      finalizeMat(R);


      // Computing the transposed redR:
      int rank = toRetrieve.size();
      /* if the matrix is close to the zero matrix, return a
         zero rank in the form of a null pointer.
      */
      if (rank == 0){
        m_is_zero_rank = true;
        return;
      }

      // toRetrieve contains the Ids of the Columns of Q^T and the rows of R^T
      // compute the reduced entries of R^T
      Mat redRT;
      initMat(redRT, nRows, rank, theComm);
      for(int idRow=0; idRow<rank; idRow++){
        int theRowIndex = toRetrieve[idRow];
        for(int idCol=idRow; idCol<nRows;idCol++){
          double valueToInsert = getMatEl(R, theRowIndex, idCol, theComm);
          setMatEl(redRT, idCol, idRow, valueToInsert, theComm, true);
        }
      }
      finalizeMat(redRT);

      // compute the SVD of RT: outputs U,S directly obtained.
      //SVD svd;
      Mat Vr;
      Vec singVals;
      initSVD(m_svd, redRT, rank, 1.0e-18, theComm, SVDTRLANCZOS);
      solveSVD(m_svd, redRT, m_Umat.Mpt(), Vr, singVals , theComm);
      m_Umat.getSize();
      m_Umat.finalize();

      int nOfTriplets;
      VecGetSize(singVals, &nOfTriplets);
      m_S.resize(nOfTriplets);
      for(unsigned int iS=0; iS<nOfTriplets; iS++){
        m_S[iS] = getVecEl(singVals, iS, theComm);
      }

      Mat redQ;
      initMat(redQ, nCols, rank, theComm);
      for(int idCol=0; idCol<rank; idCol++){
        Vec tmpL,tmpR;
        MatCreateVecs(Q,&tmpL,&tmpR);
        int toTake = toRetrieve[idCol];
        MatGetColumnVector(Q,tmpR,toTake);
        setMatCol(redQ, idCol, tmpR, theComm);
        VecDestroy(&tmpL);
      }
      finalizeMat(redQ);

      m_Vmat.setComm(theComm);
      Mat theResult;
      initMat(theResult, nCols, nOfTriplets, theComm);
      finalizeMat(theResult);
      MatMatMult(redQ,Vr,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&theResult);
      m_Vmat.setMatrix(theResult);


      m_Svec.init(nOfTriplets, theComm);
      for(unsigned int iS=0; iS<nOfTriplets; iS++){
        m_Svec.setVecEl( iS, m_S[iS], true);
      }
      m_Svec.finalize();
      m_Smat = diag(m_Svec);

      MatDestroy(&Q);
      MatDestroy(&R);
      MatDestroy(&redRT);
      MatDestroy(&redQ);
    }
  }


  // overloaded to take a matrix and provide matrices as output
  svd(mat A){
    if(A.isEmpty()){
      m_is_zero_rank=true;
      return;
    }

    //SVDCreate(A.comm(), &m_svd); => use m_svd directly in LHC
    m_has_Computed_mat = true;
    Mat Umatrix,Vmatrix;
    Vec Svector;
    svdLHC(A.Mpt(), Umatrix, Vmatrix, Svector, A.comm());
    m_Umat.setComm(A.comm());
    m_Vmat.setComm(A.comm());
    m_Svec.setComm(A.comm());
    m_Umat.setMatrix(Umatrix);
    m_Vmat.setMatrix(Vmatrix);
    m_Svec.setVector(Svector);
    m_Smat = diag(m_Svec);
  }



  // compute the first triplet:
  void oneSVDTerm(mat A){
    if(A.isEmpty()){
      m_is_zero_rank=true;
      return;
    }
    Vec uVec, vVec;
    computeOneSVDTerm(A.Mpt(), m_sigma, uVec, vVec, A.comm());
    m_u.setComm(A.comm());
    m_v.setComm(A.comm());
    m_u.setVector(uVec);
    m_v.setVector(vVec);
  }



  // FUNCTIONS TO COMPUTE SVD after empty contructor

  // Functions to be applied after an empty constructor:
  void svdVecSet(vector<vec> vectorSet){
    if(vectorSet.size()==0){
      m_is_zero_rank = true;
      return;
    }
    bool isTheSetEmpty = true;
    for(unsigned int iVec = 0; iVec<vectorSet.size(); iVec++){
      if(!vectorSet[iVec].isEmpty()){
        isTheSetEmpty = false;
      }
    }
    if(isTheSetEmpty){
      m_is_zero_rank = true;
      return;
    }


    m_has_Computed_mat = true;
    unsigned int nRows = vectorSet[0].size();
    unsigned int nCols = vectorSet.size();
    MPI_Comm theComm = vectorSet[0].comm();

    if(nRows>=nCols){
      // First step of LHC: compute a QR factorisation
      Mat Q,R;
      initMat(Q,nRows,nCols,theComm);
      finalizeMat(Q);
      initMat(R,nCols,nCols,theComm);
      finalizeMat(R);

      // Create a set of vectors, copy the elements of vectorSet:
      vector<Vec> theCols;
      theCols.resize(nCols);
      for(int idCol=0; idCol<nCols; idCol++){
        Vec iThCopy;
        initVec(iThCopy, nRows, theComm);
        VecCopy(vectorSet[idCol].x(), iThCopy);
        finalizeVec(iThCopy);
        theCols[idCol] = iThCopy;
      }

      //Starting the MGS algorithm:
      vector<int> toRetrieve;
      for(int i=0; i<nCols; i++){
        double r_ii;
        VecNorm(theCols[i],NORM_2,&r_ii);
        double coeff = 0.0;
        if(r_ii>tolQR){
          toRetrieve.push_back(i);
          setMatEl(R, i, i, r_ii, theComm, true);
          coeff = 1.0/r_ii;
          for(int idRow=0; idRow<nRows; idRow++){
            double qValue = getVecEl(theCols[i],idRow,theComm);
            qValue = qValue*coeff;
            setMatEl(Q, idRow, i, qValue,theComm, true);
          }
        }
        else{
          //setMatEl(R, i, i, 0.0, theComm); //uncomment to obtain dense R
        }
        // Computing the off-diagonal part of R
        for(int j=i+1;j<nCols;j++){
          double r_ij;
          if(coeff>0){
            VecDot(theCols[i],theCols[j],&r_ij);
            r_ij = r_ij*coeff;
            setMatEl(R,i,j,r_ij,theComm, true);
            double scale = -1.0*r_ij*coeff;
            VecAXPY(theCols[j],scale,theCols[i]);
          }
          else{
            //setMatEl(R,i,j,0.0,theComm); //uncomment to obtain a dense R
          }
        }
      }
      finalizeMat(Q);
      finalizeMat(R);

      int rank = toRetrieve.size();
      /* if the matrix is close to the zero matrix, return a
         zero rank in the form of a null pointer.
      */
      if (rank == 0){
        m_is_zero_rank = true;
        return;
      }

      // toRetrieve contains the Ids of the Columns of Q and the rows of R
      Mat redR;
      initMat(redR, rank, nCols, theComm);
      for(int idRow=0; idRow<rank; idRow++){
        int theRowIndex = toRetrieve[idRow];
        for(int idCol=idRow; idCol<nCols;idCol++){
          double valueToInsert = getMatEl(R, theRowIndex, idCol, theComm);
          setMatEl(redR, idRow, idCol, valueToInsert, theComm, true);
        }
      }
      finalizeMat(redR);
      // bidiagonalization of the reduced R
      //SVD svd;
      Mat Ur;
      Vec singVals;
      m_Vmat.setComm(theComm);
      initSVD(m_svd, redR, rank, 1.0e-18, theComm, SVDTRLANCZOS);
      solveSVD(m_svd, redR, Ur, m_Vmat.Mpt(), singVals, theComm);
      m_Vmat.getSize();
      m_Vmat.finalize();

      int nOfTriplets;
      VecGetSize(singVals, &nOfTriplets);
      m_S.resize(nOfTriplets);
      for(unsigned int iS=0; iS<nOfTriplets; iS++){
        m_S[iS] = getVecEl(singVals, iS, theComm);
      }

      // the right vectors:
      Mat redQ;
      initMat(redQ, nRows, rank, theComm);
      for(int idCol=0; idCol<rank; idCol++){
        Vec tmpL,tmpR;
        MatCreateVecs(Q,&tmpL,&tmpR);
        int toTake = toRetrieve[idCol];
        MatGetColumnVector(Q,tmpR,toTake);
        setMatCol(redQ, idCol, tmpR, theComm);
        VecDestroy(&tmpL);
      }
      finalizeMat(redQ);

      m_Umat.setComm(theComm);
      Mat theResult;
      initMat(theResult, nRows, nOfTriplets, theComm);
      finalizeMat(theResult);
      MatMatMult(redQ,Ur,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&theResult);
      m_Umat.setMatrix(theResult);

      m_Svec.init(nOfTriplets, theComm);
      for(unsigned int iS=0; iS<nOfTriplets; iS++){
        m_Svec.setVecEl( iS, m_S[iS], true);
      }
      m_Svec.finalize();
      m_Smat = diag(m_Svec);

      for(int i=0; i<nCols; i++){
        VecDestroy(&theCols[i]);
      }
      MatDestroy(&Q);
      MatDestroy(&R);
      MatDestroy(&redR);
      MatDestroy(&redQ);
    }
    // the case in which nCols > nRows
    else{
      // First step of LHC: compute a QR factorisation of the transposed
      Mat Q,R;
      initMat(Q,nCols,nRows,theComm);
      finalizeMat(Q);
      initMat(R,nRows,nRows,theComm);
      finalizeMat(R);

      // Create a set of vectors, copy the rows of A:
      vector<Vec> theRows;
      theRows.resize(nRows);
      for(int idRow=0; idRow<nRows; idRow++){
        Vec theIthRow;
        initVec(theIthRow, nCols, theComm);
        for(int iCol=0; iCol<nCols; iCol++){
          double val = vectorSet[iCol].getVecEl(idRow);
          setVecEl(theIthRow, iCol, val, theComm, true);
        }
        finalizeVec(theIthRow);
        theRows[idRow] = theIthRow;
      }


      //Starting the MGS algorithm:
      vector<int> toRetrieve;
      for(int i=0; i<nRows; i++){
        double r_ii;
        VecNorm(theRows[i],NORM_2,&r_ii);
        double coeff = 0.0;
        if(r_ii>tolQR){
          toRetrieve.push_back(i);
          setMatEl(R, i, i, r_ii, theComm, true);
          coeff = 1.0/r_ii;
          for(int idCol=0; idCol<nCols; idCol++){
            double qValue = getVecEl(theRows[i],idCol,theComm);
            qValue = qValue*coeff;
            setMatEl(Q, idCol, i, qValue,theComm, true);
          }
        }
        else{
          //setMatEl(R, i, i, 0.0, theComm); //uncomment to obtain dense R
        }
        // Computing the off-diagonal part of R
        for(int j=i+1;j<nRows;j++){
          double r_ij;
          if(coeff>0){
            VecDot(theRows[i],theRows[j],&r_ij);
            r_ij = r_ij*coeff;
            setMatEl(R,i,j,r_ij,theComm, true);
            double scale = -1.0*r_ij*coeff;
            VecAXPY(theRows[j],scale,theRows[i]);
          }
          else{
            //setMatEl(R,i,j,0.0,theComm); //uncomment to obtain a dense R
          }
        }
      }
      finalizeMat(Q);
      finalizeMat(R);


      // Computing the transposed redR:
      int rank = toRetrieve.size();
      /* if the matrix is close to the zero matrix, return a
         zero rank in the form of a null pointer.
      */
      if (rank == 0){
        m_is_zero_rank = true;
        return;
      }

      // toRetrieve contains the Ids of the Columns of Q^T and the rows of R^T
      // compute the reduced entries of R^T
      Mat redRT;
      initMat(redRT, nRows, rank, theComm);
      for(int idRow=0; idRow<rank; idRow++){
        int theRowIndex = toRetrieve[idRow];
        for(int idCol=idRow; idCol<nRows;idCol++){
          double valueToInsert = getMatEl(R, theRowIndex, idCol, theComm);
          setMatEl(redRT, idCol, idRow, valueToInsert, theComm, true);
        }
      }
      finalizeMat(redRT);

      // compute the SVD of RT: outputs U,S directly obtained.
      //SVD svd;
      Mat Vr;
      Vec singVals;
      initSVD(m_svd, redRT, rank, 1.0e-18, theComm, SVDTRLANCZOS);
      solveSVD(m_svd, redRT, m_Umat.Mpt(), Vr, singVals , theComm);
      m_Umat.getSize();
      m_Umat.finalize();

      int nOfTriplets;
      VecGetSize(singVals, &nOfTriplets);
      m_S.resize(nOfTriplets);
      for(unsigned int iS=0; iS<nOfTriplets; iS++){
        m_S[iS] = getVecEl(singVals, iS, theComm);
      }

      Mat redQ;
      initMat(redQ, nCols, rank, theComm);
      for(int idCol=0; idCol<rank; idCol++){
        Vec tmpL,tmpR;
        MatCreateVecs(Q,&tmpL,&tmpR);
        int toTake = toRetrieve[idCol];
        MatGetColumnVector(Q,tmpR,toTake);
        setMatCol(redQ, idCol, tmpR, theComm);
        VecDestroy(&tmpL);
      }
      finalizeMat(redQ);

      m_Vmat.setComm(theComm);
      Mat theResult;
      initMat(theResult, nCols, nOfTriplets, theComm);
      finalizeMat(theResult);
      MatMatMult(redQ,Vr,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&theResult);
      m_Vmat.setMatrix(theResult);


      m_Svec.init(nOfTriplets, theComm);
      for(unsigned int iS=0; iS<nOfTriplets; iS++){
        m_Svec.setVecEl( iS, m_S[iS], true);
      }
      m_Svec.finalize();
      m_Smat = diag(m_Svec);

      MatDestroy(&Q);
      MatDestroy(&R);
      MatDestroy(&redRT);
      MatDestroy(&redQ);
    }
  }




  // Init functions after empty contructor:
  void init(mat& A, bool useLHC = true, bool vec_output=false){
    if(A.isEmpty()){
      m_is_zero_rank=true;
      return;
    }
    if(!useLHC){
      initSVD(m_svd, A.M(), A.comm(), SVDTRLANCZOS);
      Mat Umatrix,Vmatrix;
      Vec Svector;
      solveSVD(m_svd, A.Mpt(), Umatrix, Vmatrix, Svector, A.comm());
      m_Umat.setComm(A.comm());
      m_Vmat.setComm(A.comm());
      m_Svec.setComm(A.comm());
      m_Umat.setMatrix(Umatrix);
      m_Vmat.setMatrix(Vmatrix);
      m_Svec.setVector(Svector);
      m_Smat = diag(m_Svec);
    }
    else{
      Mat Umatrix,Vmatrix;
      Vec Svector;
      svdLHC(A.Mpt(), Umatrix, Vmatrix, Svector, A.comm());
      m_Umat.setComm(A.comm());
      m_Vmat.setComm(A.comm());
      m_Svec.setComm(A.comm());
      m_Umat.setMatrix(Umatrix);
      m_Vmat.setMatrix(Vmatrix);
      m_Svec.setVector(Svector);
      m_Smat = diag(m_Svec);
    }
    if(vec_output){
      const unsigned int nMod = m_Umat.nCols();
      m_Umod.resize(nMod);
      for(unsigned int iMod=0; iMod<nMod; iMod++){
        vec u_mod(m_Umat.nRows(), A.comm());
        for(unsigned int iDof=0; iDof<m_Umat.nRows(); iDof++){
          double value = m_Umat.getMatEl(iDof,iMod);
          if(fabs(value)>DBL_EPSILON){
            u_mod.setVecEl(iDof,value);
          }
          u_mod.finalize();
          m_Umod[iMod] = u_mod;
        }
      }
      m_Vmod.resize(nMod);
      for(unsigned int iMod=0; iMod<nMod; iMod++){
        vec v_mod(m_Vmat.nRows(), A.comm());
        for(unsigned int iDof=0; iDof<m_Vmat.nRows(); iDof++){
          double value = m_Vmat.getMatEl(iDof,iMod);
          if(fabs(value)>DBL_EPSILON){
            v_mod.setVecEl(iDof,value);
          }
          v_mod.finalize();
          m_Vmod[iMod] = v_mod;
        }
      }
    }
  }


  /* compute svd by passing from covariance matrix and Lanczos.
    - input: mat A
    - output: singular vectors U and singular values S
  */
  void compute_svdCov(mat& A, vector<vec>& U, vec& S){
    m_use_svd_pt = false;
    const unsigned int nR = A.nRows();
    const unsigned int nC = A.nCols();

    if(nR > nC){
      mat AT = transpose(A);
      mat C = AT * A;
      eigenSolver eps;
      eps.init(C, EPS_HEP, EPSLANCZOS);
      eps.solveHermEPS(C);
      vector<vec> vv = eps.eigenVecs();
      vector<double> lambda = eps.eigenVals();
      // setting the singular values:
      //cout << "n of non-zero lambda = " << lambda.size() << endl;
      S.init(lambda.size(), A.comm());
      for(unsigned int iS=0; iS<lambda.size(); iS++){
        S(iS) = sqrt(fabs(lambda[iS]));
      }
      S.finalize();
      // multiply A vv:
      U.resize(lambda.size());
      for(unsigned int iL=0; iL<lambda.size(); iL++){
        vec a_vv = A * vv[iL];
        double scale = 1.0/S(iL);
        a_vv *= scale;
        U[iL] = a_vv;
      }

      eps.clear();
      AT.clear();
      C.clear();
    }else{

      mat AT = transpose(A);
      mat C = A * AT;
      eigenSolver eps;
      eps.init(C, EPS_HEP, EPSLANCZOS);
      eps.solveHermEPS(C);

      vector<vec> vv = eps.eigenVecs();
      vector<double> lambda = eps.eigenVals();
      // setting the singular values:
      //cout << "n of non-zero lambda = " << lambda.size() << endl;
      S.init(lambda.size(), A.comm());
      for(unsigned int iS=0; iS<lambda.size(); iS++){
        S(iS) = sqrt(fabs(lambda[iS]));
      }
      S.finalize();

      // U = vv, copy because all eps is destroyed:
      U.resize(lambda.size());
      for(unsigned int iL=0; iL<lambda.size(); iL++){
        vec mode;
        mode.copyVecFrom(vv[iL]);
        U[iL] = mode;
      }

      eps.clear();
      AT.clear();
      C.clear();

    }
  }


  /* compute svd by passing from covariance matrix and Lanczos, overloaded
    - input: mat A
    - output: singular vectors m_Umod, m_Vmod and singular values Svec are filled
  */
  void compute_svdCov(mat& A){
    m_use_svd_pt = false;
    const unsigned int nR = A.nRows();
    const unsigned int nC = A.nCols();

    if(nR > nC){
      mat AT = transpose(A);
      mat C = AT * A;
      eigenSolver eps;
      //eps.init(C, EPS_HEP, EPSLANCZOS);
      eps.init(C, EPS_HEP, EPSKRYLOVSCHUR);
      eps.solveHermEPS(C);
      vector<vec> vv = eps.eigenVecs();
      vector<double> lambda = eps.eigenVals();
      const unsigned int nOfSig = lambda.size();

      // setting the singular values:
      m_Svec.init(nOfSig, A.comm());
      for(unsigned int iS=0; iS<nOfSig; iS++){
        m_Svec(iS) = sqrt(fabs(lambda[iS]));
      }
      m_Svec.finalize();

      m_Vmod.resize(nOfSig);
      for(unsigned int iMod=0; iMod<nOfSig; iMod++){
        vec mode;
        mode.copyVecFrom(vv[iMod]);
        m_Vmod[iMod] = mode;
      }

      // multiply A vv:
      m_Umod.resize(nOfSig);
      for(unsigned int iL=0; iL<nOfSig; iL++){
        vec a_vv = A * vv[iL];
        double scale = 1.0/m_Svec(iL);
        a_vv *= scale;
        m_Umod[iL] = a_vv;
      }
      // free the memory:
      eps.clear();
      AT.clear();
      C.clear();
    }else{

      mat AT = transpose(A);
      mat C = A * AT;
      eigenSolver eps;
      //eps.init(C, EPS_HEP, EPSLANCZOS);
      eps.init(C, EPS_HEP, EPSKRYLOVSCHUR);
      eps.solveHermEPS(C);

      vector<vec> vv = eps.eigenVecs();
      vector<double> lambda = eps.eigenVals();

      const unsigned int nOfSig = lambda.size();
      // setting the singular values:
      m_Svec.init(nOfSig, A.comm());
      for(unsigned int iS=0; iS<nOfSig; iS++){
        m_Svec(iS) = sqrt(fabs(lambda[iS]));
      }
      m_Svec.finalize();

      // U = vv, copy because all eps is destroyed:
      m_Umod.resize(nOfSig);
      for(unsigned int iL=0; iL<nOfSig; iL++){
        vec mode;
        mode.copyVecFrom(vv[iL]);
        m_Umod[iL] = mode;
      }

      // Assembling the V modes:
      m_Vmod.resize(nOfSig);
      for(unsigned int iL=0; iL<nOfSig; iL++){
        vec mode = AT * m_Umod[iL];
        double scale = 1.0/m_Svec(iL);
        mode *= scale;
        m_Vmod[iL] = mode;
      }

      // free the memory
      eps.clear();
      AT.clear();
      C.clear();

    }
  }


  /* compute svd by passing from covariance matrix and Lanczos.
    - input: vector<vec> A
    - output: singular vectors U and singular values S
  */
  void compute_svdCov(vector<vec> A, mat& U, vec& S){
    m_use_svd_pt = false;
    const unsigned int nR = A[0].size();
    const unsigned int nC = A.size();

    if(nR > nC){
      mat C(nC,nC);
      for(unsigned int iMod=0; iMod<nC; iMod++){
        for(unsigned int jMod=0; jMod<=iMod; jMod++){
          double val = scalProd(A[iMod], A[jMod]);
          C.setMatEl(iMod,jMod, val);
          if(iMod != jMod){
            C.setMatEl(jMod,iMod,val);
          }
        }
      }
      C.finalize();
      eigenSolver eps;
      eps.init(C, EPS_HEP, nC, 1.0e-12);
      eps.solveHermEPS(C);
      vector<vec> vv = eps.eigenVecs();
      vector<double> lambda = eps.eigenVals();
      // setting the singular values:
      //cout << "n of non-zero lambda = " << lambda.size() << endl;
      S.init(lambda.size(), A[0].comm());
      for(unsigned int iS=0; iS<lambda.size(); iS++){
        S(iS) = sqrt(fabs(lambda[iS]));
      }
      S.finalize();
      // multiply A vv:

      U.init(nR, lambda.size(), A[0].comm());
      for(unsigned int iL=0; iL<lambda.size(); iL++){
        vec a_vv(A[0].size(), A[0].comm());
        for(unsigned int iDof=0; iDof<nR; iDof++){
          double value = 0.0;
          for(unsigned int i=0; i<lambda.size(); i++){
            value += A[i].getVecEl(iDof) * vv[iL].getVecEl(i);
          }
          a_vv(iDof) = value;
        }
        a_vv.finalize();

        double scale = 1.0/S(iL);
        a_vv *= scale;
        U.setCol(iL,a_vv);
        a_vv.clear();
      }
      U.finalize();

      eps.clear();
      C.clear();
    }else{

      mat C(nR,nR);
      for(unsigned int iMod=0; iMod<nR; iMod++){
        for(unsigned int jMod=0; jMod<=iMod; jMod++){
          double val = 0.0;
          for(unsigned int iDof=0; iDof<nC; iDof++){
            val += A[iDof].getVecEl(iMod) * A[iDof].getVecEl(jMod);
          }
          C.setMatEl(iMod,jMod, val);
          if(iMod != jMod){
            C.setMatEl(jMod,iMod,val);
          }
        }
      }
      C.finalize();
      eigenSolver eps;
      eps.init(C, EPS_HEP, nR, 1.0e-12);
      eps.solveHermEPS(C);

      vector<vec> vv = eps.eigenVecs();
      vector<double> lambda = eps.eigenVals();
      // setting the singular values:
      //cout << "n of non-zero lambda = " << lambda.size() << endl;
      S.init(lambda.size(), A[0].comm());
      for(unsigned int iS=0; iS<lambda.size(); iS++){
        S(iS) = sqrt(fabs(lambda[iS]));
      }
      S.finalize();

      // U = vv, copy because all eps is destroyed:
      U.init(nR, lambda.size(), A[0].comm());
      for(unsigned int iL=0; iL<lambda.size(); iL++){
        vec mode;
        mode.copyVecFrom(vv[iL]);
        U.setCol(iL,mode);
      }
      U.finalize();

      eps.clear();
      C.clear();

    }
  }




  // V -- SVD PETSC IMPLEMENTATIONS --

  // V.1 -- Init an SVD context --
  void initSVD(SVD& svd, Mat A, MPI_Comm theComm, SVDType type=SVDCYCLIC){
      SVDCreate(theComm, &svd);
      SVDSetType(svd,type);
      SVDSetOperator(svd, A);
      SVDSetFromOptions(svd);

  }


  // overloading by specifying the number of singular values:
  void initSVD(SVD& svd, Mat A, int nsv, MPI_Comm theComm, SVDType type=SVDCYCLIC, bool LARGEST=true){
      SVDCreate(theComm, &svd);
      SVDSetOperator(svd, A);
      SVDSetType(svd,type);
      SVDSetDimensions(svd, nsv, PETSC_DEFAULT, PETSC_DEFAULT);
      if(!LARGEST){
          SVDSetWhichSingularTriplets(svd, SVD_SMALLEST);
      };
      SVDSetFromOptions(svd);
  }


  // overloading by specifying Tolerances and number of singular values: to compute the smallest singular values, put a false when calling
  void initSVD(SVD& svd, Mat A, int nsv, double tol, MPI_Comm theComm, SVDType type=SVDCYCLIC,bool LARGEST=true){
      SVDCreate(theComm, &svd);
      SVDSetOperator(svd, A);
      SVDSetType(svd,type);
      SVDSetDimensions(svd, nsv, PETSC_DEFAULT, PETSC_DEFAULT);
      SVDSetTolerances(svd, tol, PETSC_DECIDE);
      if(!LARGEST){
          SVDSetWhichSingularTriplets(svd, SVD_SMALLEST);
      };
      SVDSetFromOptions(svd);
  }


  // overloading by specifying Tolerances and number of singular values: to compute the smallest singular values, put a false when calling
  void initSVD(SVD& svd, Mat A, double tol, MPI_Comm theComm, SVDType type=SVDCYCLIC,bool LARGEST=true){
      SVDCreate(theComm, &svd);
      SVDSetOperator(svd, A);
      SVDSetType(svd,type);
      //SVDSetDimensions(svd,PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);
      SVDSetTolerances(svd, tol, PETSC_DECIDE);
      if(!LARGEST){
          SVDSetWhichSingularTriplets(svd, SVD_SMALLEST);
      };
      SVDSetFromOptions(svd);
  }


  /* compute one SVD term
    compute the largest singular value and its associated singular vectors
    - input: the matrix,
    - output: the singular value, the singular vectors
  */
  void computeOneSVDTerm(Mat& A, double& sigma, Vec& u, Vec& v, MPI_Comm theComm, SVDType type=SVDTRLANCZOS, bool LARGEST=true){
    SVDCreate(theComm, &m_svd);
    SVDSetOperator(m_svd, A);
    SVDSetType(m_svd,type);
    PetscInt nsv = 1;
    SVDSetDimensions(m_svd, nsv, PETSC_DEFAULT, PETSC_DEFAULT);
    SVDSetTolerances(m_svd, 1.0e-12, PETSC_DECIDE);
    if(!LARGEST){
        SVDSetWhichSingularTriplets(m_svd, SVD_SMALLEST);
    };
    SVDSetFromOptions(m_svd);

    SVDSolve(m_svd);
    int nconv;
    SVDGetConverged(m_svd,&nconv);
    MatCreateVecs(A,&v,&u);
    SVDGetSingularTriplet(m_svd, 0, &sigma, u, v );
  }


  // V.2 Solving SVD, putting the result into two parallel matrices and one Vec;
  // U, V and S are just pointers, matrices are created inside according to the converged
  void solveSVD(SVD& svd, Mat& A, Mat& U, Mat& V, Vec& S, MPI_Comm theComm){

      Vec u,v;
      PetscReal sigma;
      int nconv;
      SVDSolve(svd);
      SVDGetConverged(svd,&nconv);
      MatCreateVecs(A,&v,&u);
      int nRowA, nColA;
      MatGetSize(A,&nRowA, &nColA);
      int nRowU = nRowA;
      int nRowV = nColA;

      //Deciding the dimensions
      int nWanted, nCSP, mpd;
      SVDGetDimensions(svd, &nWanted, &nCSP, &mpd);
      //int nSingular = ( (nconv < nWanted) ? nconv : nWanted );
      int nSingular = min(nRowA, nColA);
      nSingular = min(nconv, nSingular);

      //initialize the matrices:
      initMat(U, nRowU, nSingular, theComm);
      initMat(V, nRowV, nSingular, theComm);
      initVec(S, nSingular, theComm);


      // to get error information add SVDComputeError.
      for (int j=0; j<nSingular; j++) {
          SVDGetSingularTriplet( svd, j, &sigma, u, v );
          setMatCol(U, j, u, theComm);
          setMatCol(V, j, v, theComm);
          setVecEl(S, j, sigma, theComm, true);
      }
      finalizeMat(U);
      finalizeMat(V);
      finalizeVec(S);
      VecDestroy(&u);
      VecDestroy(&v);
  }




  /* SOLVE SVD by naive LHC like algorithm
    - Inputs: the matrix A
      the matrices U,V
      the vector S
    - Outputs thin SVD: U,S,V
    - Default tolerances are used (both for QR and SVD bidiagonalization)
  */

  void svdLHC(Mat& A, Mat& U, Mat& V, Vec& S, MPI_Comm theComm){

    int nRows, nCols;
    MatGetSize(A, &nRows, &nCols);
    const double tolQR = 2.22e-16;

    if(nRows>=nCols){
      // First step of LHC: compute a QR factorisation
      Mat Q,R;
      initMat(Q,nRows,nCols,theComm);
      finalizeMat(Q);
      initMat(R,nCols,nCols,theComm);
      finalizeMat(R);

      // Create a set of vectors, copy the columns of A:
      vector<Vec> theCols;
      theCols.resize(nCols);
      Vec left;
      for(int idCol=0; idCol<nCols; idCol++){
        MatCreateVecs(A,&left,&theCols[idCol]);
        MatGetColumnVector(A,theCols[idCol],idCol);
      }
      VecDestroy(&left);

      //Starting the MGS algorithm:
      vector<int> toRetrieve;
      for(int i=0; i<nCols; i++){
        double r_ii;
        VecNorm(theCols[i],NORM_2,&r_ii);
        double coeff = 0.0;
        if(r_ii>tolQR){
          toRetrieve.push_back(i);
          setMatEl(R, i, i, r_ii, theComm, true);
          coeff = 1.0/r_ii;
          for(int idRow=0; idRow<nRows; idRow++){
            double qValue = getVecEl(theCols[i],idRow,theComm);
            qValue = qValue*coeff;
            setMatEl(Q, idRow, i, qValue,theComm, true);
          }
        }
        else{
          //setMatEl(R, i, i, 0.0, theComm); //uncomment to obtain dense R
        }
        // Computing the off-diagonal part of R
        for(int j=i+1;j<nCols;j++){
          double r_ij;
          if(coeff>0){
            VecDot(theCols[i],theCols[j],&r_ij);
            r_ij = r_ij*coeff;
            setMatEl(R,i,j,r_ij,theComm, true);
            double scale = -1.0*r_ij*coeff;
            VecAXPY(theCols[j],scale,theCols[i]);
          }
          else{
            //setMatEl(R,i,j,0.0,theComm); //uncomment to obtain a dense R
          }
        }
      }
      finalizeMat(Q);
      finalizeMat(R);

      int rank = toRetrieve.size();

      /* if the matrix is close to the zero matrix, return a
         zero rank in the form of a null pointer.
      */
      if (rank == 0){
        m_is_zero_rank = true;
        U = PETSC_NULL;
        V = PETSC_NULL;
        S = PETSC_NULL;
        return;
      }

      // toRetrieve contains the Ids of the Columns of Q and the rows of R
      Mat redR;
      initMat(redR, rank, nCols, theComm);
      for(int idRow=0; idRow<rank; idRow++){
        int theRowIndex = toRetrieve[idRow];
        for(int idCol=idRow; idCol<nCols;idCol++){
          double valueToInsert = getMatEl(R, theRowIndex, idCol, theComm);
          setMatEl(redR, idRow, idCol, valueToInsert, theComm, true);
        }
      }
      finalizeMat(redR);
      // bidiagonalization of the reduced R
      //SVD svd;
      Mat Ur;
      initSVD(m_svd, redR, rank, 1.0e-18, theComm, SVDTRLANCZOS);
      solveSVD(m_svd, redR, Ur, V, S, theComm);

      int nOfTriplets;
      VecGetSize(S, &nOfTriplets);

      // the right vectors:
      Mat redQ;
      initMat(redQ, nRows, rank, theComm);
      for(int idCol=0; idCol<rank; idCol++){
        Vec tmpL,tmpR;
        MatCreateVecs(Q,&tmpL,&tmpR);
        int toTake = toRetrieve[idCol];
        MatGetColumnVector(Q,tmpR,toTake);
        setMatCol(redQ, idCol, tmpR, theComm);
        VecDestroy(&tmpL);
      }
      finalizeMat(redQ);

      initMat(U, nRows, nOfTriplets, theComm);
      finalizeMat(U);
      MatMatMult(redQ,Ur,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&U);

      for(int i=0; i<nCols; i++){
        VecDestroy(&theCols[i]);
      }
      // free the memory:
      MatDestroy(&Q);
      MatDestroy(&R);
      MatDestroy(&redR);
      MatDestroy(&redQ);
    }
    // the case in which nCols > nRows
    else{
      // First step of LHC: compute a QR factorisation of the transposed
      Mat Q,R;
      initMat(Q,nCols,nRows,theComm);
      finalizeMat(Q);
      initMat(R,nRows,nRows,theComm);
      finalizeMat(R);

      // Create a set of vectors, copy the rows of A:
      vector<Vec> theRows;
      theRows.resize(nRows);
      Vec right;
      for(int idRow=0; idRow<nRows; idRow++){
        initVec(theRows[idRow], nCols, theComm);
        for(unsigned int jCol=0; jCol<nCols; jCol++){
          double val = getMatEl(A, idRow, jCol, theComm);
          setVecEl(theRows[idRow], jCol, val, theComm, true);
        }
        finalizeVec(theRows[idRow]);
        /*MatCreateVecs(A,&theRows[idRow],&right);
        const PetscInt* nonZeroColsA;
        const PetscScalar* theRowOfA;
        MatGetRow(A, idRow, &nCols , &nonZeroColsA, &theRowOfA);
        VecSetValues(theRows[idRow],nCols,nonZeroColsA,theRowOfA,INSERT_VALUES);
        finalizeVec(theRows[idRow]);*/
      }
      //VecDestroy(&right);

      //Starting the MGS algorithm:
      vector<int> toRetrieve;
      for(int i=0; i<nRows; i++){
        double r_ii;
        VecNorm(theRows[i],NORM_2,&r_ii);
        double coeff = 0.0;
        if(r_ii>tolQR){
          toRetrieve.push_back(i);
          setMatEl(R, i, i, r_ii, theComm, true);
          coeff = 1.0/r_ii;
          for(int idCol=0; idCol<nCols; idCol++){
            double qValue = getVecEl(theRows[i],idCol,theComm);
            qValue = qValue*coeff;
            setMatEl(Q, idCol, i, qValue,theComm, true);
          }
        }
        else{
          //setMatEl(R, i, i, 0.0, theComm); //uncomment to obtain dense R
        }
        // Computing the off-diagonal part of R
        for(int j=i+1;j<nRows;j++){
          double r_ij;
          if(coeff>0){
            VecDot(theRows[i],theRows[j],&r_ij);
            r_ij = r_ij*coeff;
            setMatEl(R,i,j,r_ij,theComm, true);
            double scale = -1.0*r_ij*coeff;
            VecAXPY(theRows[j],scale,theRows[i]);
          }
          else{
            //setMatEl(R,i,j,0.0,theComm); //uncomment to obtain a dense R
          }
        }
      }
      finalizeMat(Q);
      finalizeMat(R);


      // Computing the transposed redR:
      int rank = toRetrieve.size();

      /* if the matrix is close to the zero matrix, return a
         zero rank in the form of a null pointer.
      */
      if (rank == 0){
        m_is_zero_rank = true;
        U = PETSC_NULL;
        V = PETSC_NULL;
        S = PETSC_NULL;
        return;
      }

      // toRetrieve contains the Ids of the Columns of Q^T and the rows of R^T
      // compute the reduced entries of R^T
      Mat redRT;
      initMat(redRT, nRows, rank, theComm);
      for(int idRow=0; idRow<rank; idRow++){
        int theRowIndex = toRetrieve[idRow];
        for(int idCol=idRow; idCol<nRows;idCol++){
          double valueToInsert = getMatEl(R, theRowIndex, idCol, theComm);
          setMatEl(redRT, idCol, idRow, valueToInsert, theComm, true);
        }
      }
      finalizeMat(redRT);

      // compute the SVD of RT: outputs U,S directly obtained.
      //SVD svd;
      Mat Vr;
      initSVD(m_svd, redRT, rank, 1.0e-18, theComm, SVDTRLANCZOS);
      solveSVD(m_svd, redRT, U, Vr, S, theComm);

      int nOfTriplets;
      VecGetSize(S, &nOfTriplets);

      Mat redQ;
      initMat(redQ, nCols, rank, theComm);
      for(int idCol=0; idCol<rank; idCol++){
        Vec tmpL,tmpR;
        MatCreateVecs(Q,&tmpL,&tmpR);
        int toTake = toRetrieve[idCol];
        MatGetColumnVector(Q,tmpR,toTake);
        setMatCol(redQ, idCol, tmpR, theComm);
        VecDestroy(&tmpL);
      }
      finalizeMat(redQ);

      initMat(V, nCols, nOfTriplets, theComm);
      finalizeMat(V);
      MatMatMult(redQ,Vr,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&V);

      // free the memory:
      MatDestroy(&Q);
      MatDestroy(&R);
      MatDestroy(&redRT);
      MatDestroy(&redQ);
    }

  }



  // -- ACCESS FUNCTIONS --
  inline vector<double> S(){return m_S;}
  inline double S(unsigned int i){return m_S[i];}
  inline vec Svec(){return m_Svec;}
  inline mat Umat(){return m_Umat;}
  inline mat Vmat(){return m_Vmat;}
  inline mat Smat(){return m_Smat;}
  inline double sigma(){return m_sigma;}
  inline vec u(){return m_u;}
  inline vec v(){return m_v;}
  inline bool is_zero_rank(){return m_is_zero_rank;}
  inline unsigned int nOfSigma(){return m_Svec.size();}
  inline vector<vec> Umod(){return m_Umod;}
  inline vec Umod(unsigned int iMod){return m_Umod[iMod];}
  inline vector<vec> Vmod(){return m_Vmod;}
  inline vec Vmod(unsigned int iMod){return m_Vmod[iMod];}

  // -- SETTERS --
  inline void set_is_zero_rank(bool isIt){m_is_zero_rank = isIt;}
};

// end of file:
#endif
