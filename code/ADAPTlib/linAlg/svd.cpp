// Implementation of the svd external functions:

#include "svd.h"


/* 1 - computing covariance matrix:
  - overloaded: input is either a matrix or a vector vec
  - output: the covariance matrix
*/
mat compute_cov_mat(const mat& A){
  mat A_t = transpose(A);
  mat C = A_t*A;
  mat I = eye(C.nRows(),C.comm());
  I *= DBL_EPSILON;
  C += I;
  I.clear();
  return C;
};

mat compute_cov_mat(const vector<vec>& A){
  const unsigned int nMod = A.size();
  mat D(nMod,nMod, A[0].comm());
  for(unsigned int i=0; i<nMod; i++){
    vec fibre_i = A[i];
    for(unsigned int j=0; j<=i; j++){
      vec fibre_j = A[j];
      double entry = scalProd(fibre_i, fibre_j);
      D(i,j) = entry;
      if(i != j){
        D(j,i) = entry;
      }
    }
  }
  D.finalize();
  mat I = eye(D.nRows(),D.comm());
  I *= DBL_EPSILON;
  D += I;
  I.clear();
  return D;
}

/* 1.c -- estimate the truncation --
  - input vector<vec> lambda, tolerance;
  - output: how many terms to be retained.
*/
unsigned int estimate_truncation(const vector<double>& lambda, double tol){
  const double tol2 = tol * tol;

  unsigned int nT = lambda.size();
  unsigned int toRetain = nT;
  double tail = fabs(lambda[nT-1]);
  unsigned int dc = 1;
  while ( (tail < tol2) && (dc<nT) ){
    dc += 1;
    tail += fabs(lambda[nT-dc]);
    toRetain -= 1;
  }
  if(toRetain==0){toRetain=1;}
  return toRetain;
}


/* 2 - computing svd of a matrix:
  - overloaded: input is either a matrix or a vector vec
  - output: the svd
*/
void compute_svd(const mat& A, mat& U, mat&S, mat& V, double tol){
  mat C = compute_cov_mat(A);

  eigenSolver eps_0;
  eps_0.init(C, EPS_HEP, C.nCols(), 1.0e-12);
  eps_0.solveHermEPS(C);
  vector<vec> vv_0 = eps_0.eigenVecs();
  vector<double> lambda_0 = eps_0.eigenVals();

  // truncating:
  unsigned int toRetain = estimate_truncation(lambda_0, tol);

  // computing the output:
  U.init(A.nRows(), toRetain, A.comm());
  for(unsigned int iCol=0; iCol<toRetain; iCol++){
    // computing i-th mode:
    for(unsigned int iRow = 0; iRow<A.nRows(); iRow++){
      double val = 0.0;
      for(unsigned int iComp = 0; iComp<vv_0[iCol].size(); iComp++){
        val += A.getMatEl(iRow,iComp) * vv_0[iCol].getVecEl(iComp);
      }
      val = val/sqrt(fabs(lambda_0[iCol]));
      U.setMatEl(iRow, iCol, val);
    }
  }
  U.finalize();

  S.init(toRetain, toRetain, A.comm());
  for(unsigned int iMod=0; iMod<toRetain; iMod++){
    double val = sqrt(fabs(lambda_0[iMod]));
    S.setMatEl(iMod, iMod, val);
  }
  S.finalize();

  V.init(A.nCols(), toRetain, A.comm());
  for(unsigned int iRow=0; iRow<A.nCols(); iRow++){
    for(unsigned int iCol=0; iCol<toRetain; iCol++){
      double val = vv_0[iCol].getVecEl(iRow);
      V.setMatEl(iRow, iCol, val);
    }
  }
  V.finalize();

  // free the memory:
  C.clear();
  eps_0.clear();
}


void compute_svd(const vector<vec>& A, vector<vec>& U, vector<double>& S, vector<vec>& V, double tol){
  const unsigned int nDof = A[0].size();
  mat C = compute_cov_mat(A);

  eigenSolver eps_0;
  eps_0.init(C, EPS_HEP, C.nCols(), 1.0e-12);
  eps_0.solveHermEPS(C);
  vector<vec> vv_0 = eps_0.eigenVecs();
  vector<double> lambda_0 = eps_0.eigenVals();

  // truncating:
  unsigned int toRetain = estimate_truncation(lambda_0, tol);

  // computing the output:
  U.resize(toRetain);
  for(unsigned int iCol=0; iCol<toRetain; iCol++){
    U[iCol].init(nDof, A[0].comm());
    // computing i-th mode:
    for(unsigned int iRow = 0; iRow<nDof; iRow++){
      double val = 0.0;
      for(unsigned int iComp = 0; iComp<vv_0[iCol].size(); iComp++){
        val += A[iComp].getVecEl(iRow) * vv_0[iCol].getVecEl(iComp);
      }
      val = val/sqrt(fabs(lambda_0[iCol]));
      U[iCol].setVecEl(iRow, val);
    }
    U[iCol].finalize();
  }

  S.resize(toRetain);
  for(unsigned int iMod=0; iMod<toRetain; iMod++){
    S[iMod] = sqrt(fabs(lambda_0[iMod]));
  }

  // copy v:
  V.resize(toRetain);
  for(unsigned int iCol=0; iCol<toRetain; iCol++){
    V[iCol].copyVecFrom(vv_0[iCol]);
  }

  // free the memory:
  C.clear();
  eps_0.clear();
}
