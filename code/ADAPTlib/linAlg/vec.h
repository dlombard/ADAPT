// header class for vector wrapper
/*
  - Implementation of part of the auxiliary functions
*/

#ifndef vec_h
#define vec_h

#include "../genericInclude.h"

using namespace std;

class vec{

private:
  Vec m_x;
  unsigned int m_size;
  MPI_Comm m_comm;

public:

  // I - CONSTRUCTORS and DESTRUCTORS
  vec(){};
  ~vec(){};
  void clear(){VecDestroy(&m_x);}

  /* I.1 - overloaded constructors:
    - inputs: vector size, MPI communicator
    - output: the fields m_x, m_comm, m_size are initialised
  */
  vec(int itsSize, MPI_Comm theComm){
    PetscErrorCode ierr;
    ierr = VecCreate(theComm, &m_x);
    ierr = VecSetSizes(m_x,PETSC_DECIDE, itsSize);
    ierr = VecSetFromOptions(m_x);
    m_comm = theComm;
    m_size = itsSize;
  }

  /* I.1.1 - overloaded constructors:
    - input: vector size
    - output: the fields m_x, m_comm, m_size are initialised
    (!) m_comm = PETSC_COMM_WORLD by default
  */
  vec(int itsSize){
    m_comm = PETSC_COMM_WORLD;
    PetscErrorCode ierr;
    ierr = VecCreate(m_comm, &m_x);
    ierr = VecSetSizes(m_x, PETSC_DECIDE, itsSize);
    ierr = VecSetFromOptions(m_x);
    m_size = itsSize;
  }

  // -- INIT, in case of existing object --
  // I.2 - Initialize a Petsc Vec given its size and a communicator:
  void init(unsigned int itsSize, MPI_Comm theComm){
      PetscErrorCode ierr;
      ierr = VecCreate(theComm, &m_x);
      ierr = VecSetSizes(m_x,PETSC_DECIDE, itsSize);
      ierr = VecSetFromOptions(m_x);
      m_comm = theComm;
      m_size = itsSize;
  }

  // I.2 - Initialize a Petsc Vec given its size (m_comm= PETSC_COMM_WORLD):
  void init(unsigned int itsSize){
      m_comm = PETSC_COMM_WORLD;
      PetscErrorCode ierr;
      ierr = VecCreate(m_comm, &m_x);
      ierr = VecSetSizes(m_x,PETSC_DECIDE, itsSize);
      ierr = VecSetFromOptions(m_x);
      m_size = itsSize;
  }

  // I.2 - Overloaded, copying the structure of another vector, b:
  void init(vec& b) {
      PetscErrorCode ierr;
      ierr = VecDuplicate(b.x(), &m_x);
  }

  // II - Setting the element of the vector
  void setVecEl(int ind, double val, bool PAR = true){
    if(!PAR){
      int rank;
      MPI_Comm_rank (m_comm, &rank);
      if(rank == 0){
        VecSetValues( m_x , 1 , &ind , &val , INSERT_VALUES );
      }
    }
    else{ // Parallel non-communicating way
      int startInd, endInd;
      VecGetOwnershipRange( m_x , &startInd , &endInd );
      if((ind>=startInd) && (ind<endInd)){
        VecSetValues( m_x , 1 , &ind , &val , INSERT_VALUES );
      }
    }
  }

  // adding values to existing entries for unassembled objects
  void addVecEl(int ind, double val, bool PAR = true){
    if(!PAR){
      int rank;
      MPI_Comm_rank (m_comm, &rank);
      if(rank == 0){
        VecSetValues( m_x , 1 , &ind , &val , ADD_VALUES );
      }
    }
    else{ // Parallel non-communicating way
      int startInd, endInd;
      VecGetOwnershipRange( m_x , &startInd , &endInd );
      if((ind>=startInd) && (ind<endInd)){
        VecSetValues( m_x , 1 , &ind , &val , ADD_VALUES );
      }
    }
  }

  // III Finalize the Assembly
  void finalize(){
    VecAssemblyBegin(m_x);
    VecAssemblyEnd(m_x);
  }



  // IV Get just one value and pass it to all Procs!:
  /* overloaded, output is directly the double, all the procs have it */
  double getVecEl(int ind) const {
    double toBeReturned = 0.0;
    int rank;
    MPI_Comm_rank (m_comm, &rank);
    int startInd, endInd;
    VecGetOwnershipRange( m_x , &startInd , &endInd );
    int amIRoot = 0;
    if( (ind >=startInd ) && (ind<endInd) ){
      VecGetValues(m_x, 1 , &ind, &toBeReturned);
      amIRoot = 1;
    }
    int consensus = amIRoot * rank;
    int root = 0;
    MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, m_comm);
    MPI_Bcast(&toBeReturned, 1, MPI_DOUBLE, root, m_comm);

    return toBeReturned;
  }

  // Get the size (in case of resizing...)
  inline unsigned int getSize(){
    PetscInt toBeReturned;
    VecGetSize(m_x, &toBeReturned);
    m_size = toBeReturned;
    return toBeReturned;
  }

  // norm-2 of a vector:
  inline double norm() const {
    double toBeReturned = 0.0;
    VecNorm(m_x, NORM_2, &toBeReturned);
    return toBeReturned;
  }

  // infty-norm of a vector:
  inline double normInfty(){
    double toBeReturned = 0.0;
    VecNorm(m_x, NORM_INFINITY, &toBeReturned);
    return toBeReturned;
  }

  // ones() to set all the vector elements to 1.0:
  inline void ones(){
    VecSet(m_x, 1.0);
    finalize();
  }

  // copy vector from a given vec:
  void copyVecFrom(const vec& vToCopy){
    m_comm = vToCopy.comm();
    m_size = vToCopy.size();
    init(m_size, m_comm);
    VecCopy(vToCopy.x(), m_x);
    finalize();
  }

  // copy an assembled vector:
  void copy(const vec& v) {
    assert(m_size==v.m_size && m_comm==v.m_comm);
    VecCopy(v.m_x, m_x);
  }

  // copy an unassembled vector
  void convert(const vec& v) {
    m_comm = v.m_comm;
    m_size = v.m_size;
    this->init(m_size, m_comm);
    this->finalize();
    VecCopy(v.m_x, m_x);
  }

  void scale(const double alpha) {
    VecScale(m_x, alpha);
  }

  // VecAXPY
  void add(const vec& v, double alpha){
    assert(v.m_comm==m_comm);
    VecAXPY(m_x, alpha, v.m_x);
  }

  // VecAXPY
  void sum(vec toSum, double alpha){
    assert(toSum.comm()==m_comm);
    VecAXPY(m_x, alpha, toSum.x());
  }

  // -- SETTERS --
  // set Vec
  inline void setVector(Vec& theVec){
    m_x = theVec;
    PetscInt toBeReturned;
    VecGetSize(m_x, &toBeReturned);
    m_size = toBeReturned;
  }

  // set Communicator
  inline void setComm(MPI_Comm theComm){
    m_comm = theComm;
  }

  // ACCESS FUNCTIONS:
  inline const Vec x() const {return m_x;}
  inline Vec x() {return m_x;}
  inline Vec& xPt() {return m_x;}
  inline unsigned int size() const {
    return m_size;
  }
  inline MPI_Comm comm() const {return m_comm;}
  inline void print(){
    VecView(m_x, PETSC_VIEWER_STDOUT_WORLD);
  }


  // OPERATORS:

  // overloading operator () in assignement through a proxy class:
  class Proxy
  {
    Vec x;
    unsigned int idx;
    MPI_Comm b_comm;
  public:

    // setVecEl and getVecEl for the nested class Proxy
    void setVecEl(Vec& x, int ind, double val, MPI_Comm theComm, bool PAR = true){
        if(!PAR){
          int rank;
          MPI_Comm_rank (theComm, &rank);
          if(rank == 0){
            VecSetValues( x , 1 , &ind , &val , INSERT_VALUES );
          }
        }
        else{ // Parallel non-communicating way
          int startInd, endInd;
          VecGetOwnershipRange( x , &startInd , &endInd );
          if((ind>=startInd) && (ind<endInd)){
            VecSetValues( x , 1 , &ind , &val , INSERT_VALUES );
          }
        }
      }

      // getVecEl for the proxy:
      double getVecEl(Vec x, int ind, MPI_Comm theComm){
        double toBeReturned = 0.0;
        int rank;
        MPI_Comm_rank (theComm, &rank);
        int startInd, endInd;
        VecGetOwnershipRange( x , &startInd , &endInd );
        int amIRoot = 0;
        if( (ind >=startInd ) && (ind<endInd) ){
          VecGetValues(x, 1 , &ind, &toBeReturned);
          amIRoot = 1;
        }
        int consensus = amIRoot * rank;
        int root = 0;
        MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, theComm);
        MPI_Bcast(&toBeReturned, 1, MPI_DOUBLE, root, theComm);

        return toBeReturned;
      }

      // Constructor of the proxy class:
      Proxy(Vec x, unsigned int idx, MPI_Comm b_comm) : x(x), idx(idx), b_comm(b_comm) {}

      // equal operator overload:
      inline double operator= (double value) {
        setVecEl(x, idx, value, b_comm);
        return value;
      }

      // Overloading the double operator for assignement:
      operator double(){
        double toBeReturned = getVecEl(x, idx, b_comm);
        return toBeReturned;
      }
  };

  // Calling the proxy object:
  Proxy operator() (unsigned int index) { return Proxy(m_x, index, m_comm); }

  // check if a vector is empty
  bool isEmpty(){
    bool toBeReturned = true;
    getSize();
    if(m_size==0){
      return toBeReturned;
    }
    else{
      for(unsigned int iRow=0; iRow<m_size; iRow++){
        if(fabs(getVecEl(iRow)) > DBL_EPSILON){
          toBeReturned = false;
          break;
        }
      }
    }
    return toBeReturned;
  }


  // multiply by scalar:
  inline void operator *= (double alpha){
    finalize();
    VecScale(m_x, alpha);
  }

  // add to all the elements a constant:
  inline void operator += (double alpha){
    finalize();
    VecShift(m_x, alpha);
  }

  // summing another vec:
  inline void operator += (vec toBeAdded){
    finalize();
    VecAXPY(m_x, 1.0, toBeAdded.x());
    finalize();
  }

  // subtracting another vector:
  inline void operator -= (vec toBeAdded){
    finalize();
    VecAXPY(m_x, -1.0, toBeAdded.x());
    finalize();
  }

  // checking equalities between vecs:
  inline bool operator == (vec toCheck){
    PetscBool isIt;
    VecEqual(m_x, toCheck.x(), &isIt);
    return isIt;
  }

  // extract sub-vector and put it into a vec: indices C convention
  vec extractSubvector(unsigned int iLow, unsigned int iUp) const {
    const unsigned int vecSize = iUp-iLow;
    vec output(vecSize, m_comm);
    for(unsigned int i=0; i<vecSize; i++){
      unsigned int index = i + iLow;
      double val = this->getVecEl(index);
      output.setVecEl(i, val);
    }
    output.finalize();
    return output;
  }

  // I/O operations:

  // saving a vector in binary format:
  inline void save(string fileName){
    PetscViewer viewer;
    PetscViewerBinaryOpen(m_comm, fileName.c_str(), FILE_MODE_WRITE, &viewer);
    VecView(m_x, viewer);
    PetscViewerDestroy(&viewer);
  }

  // loading a vector in binary format:
  inline void load(string fileName){
    PetscViewer viewer;
    PetscViewerBinaryOpen(m_comm, fileName.c_str(), FILE_MODE_READ, &viewer);
    VecCreate(m_comm, &m_x);
    VecLoad(m_x, viewer);
    PetscViewerDestroy(&viewer);
  }

  // loading a vector in binary format:
  inline void load(const string& filename, const MPI_Comm& comm) {
    m_comm = comm;
    PetscViewer viewer;
    PetscViewerBinaryOpen(m_comm, filename.c_str(), FILE_MODE_READ, &viewer);
    VecCreate(m_comm, &m_x);
    VecLoad(m_x, viewer);
    PetscViewerDestroy(&viewer);
    PetscInt toBeReturned;
    VecGetSize(m_x, &toBeReturned);
    m_size = toBeReturned;
    this->finalize();
  }

  // save vector in ASCII:
  inline void saveASCII(string fileName){
    ofstream outfile;
    outfile.open(fileName.c_str());
    for(unsigned int iDof=0; iDof<m_size; iDof++){
      outfile << getVecEl(iDof) << endl;
    }
    outfile.close();
  }

  // load from ASCII: -- useful for freefem++
  inline void loadASCII(string fileName){
      vector<double> nonZerosList;
      vector<unsigned int> idList;
      double thisEntry = 0.0;

      ifstream inputFile;
      inputFile.open(fileName.c_str());
      char output[128];
      unsigned int cc = 0;
      if (inputFile.is_open()) {
        while (!inputFile.eof()) {
          inputFile >> output;
          stringstream str;
          str << output;
          str >> thisEntry;
          if(fabs(thisEntry)!=0.){
            idList.push_back(cc);
            nonZerosList.push_back(thisEntry);
          }
          cc += 1;
        }
      }
      else{
        puts("Unable to read file!");
        exit(1);
      }
      cc = cc -1;
      init(cc, m_comm);
      for(unsigned int iEl=0; iEl<nonZerosList.size(); iEl++){
        setVecEl(idList[iEl], nonZerosList[iEl]);
      }
      finalize();
      getSize();
  }

};





#endif
