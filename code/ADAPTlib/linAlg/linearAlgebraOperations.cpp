// Header of linear algebra operations:



#include "linearAlgebraOperations.h"




// 0 -- MPI and CONVERSIONS --

// 0.1 -- findNonZero indices of a vector:
vector<int> findNonZero(vector<double>& v){
    const unsigned int vecSize = v.size();
    vector<int> pattern;
    for(unsigned int h=0; h<vecSize; h++){
        if(v[h] != 0.0){
            pattern.push_back(h);
        }
    }
    return pattern;
}


// 0.2 -- reduce a vector to its non-zero elements:
vector<double> nonZero(vector<double>& v){
    const unsigned int vecSize = v.size();
    vector<double> toBeReturned;
    for(unsigned int h=0; h<vecSize; h++){
        if(v[h] != 0.0){
            toBeReturned.push_back(v[h]);
        }
    }
    return toBeReturned;
}


//0.3 converting vector<int> into PetscInt array:
void vector2PetscInt(vector<int> vIn, PetscInt* array){
    for(unsigned int j=0; j<vIn.size(); j++){
        array[j] = vIn[j];
    }
}

// 0.4 converting vector<double> into PetscScalar array:
void vector2PetscScalar(vector<double> vIn, PetscScalar* array){
    for(unsigned int j=0; j<vIn.size(); j++){
        array[j] = vIn[j];
    }
}

// 0.5 converting a PetscInt array into vector<int>
void PetscInt2vector(const PetscInt* array, vector<int>& vOut ){
  for(unsigned int j=0; j<vOut.size(); j++){
      vOut[j] = array[j];
  }
}

// 0.6 converting a PetscScalar array into a vector<double>
void PetscScalar2vector(const PetscScalar* array, vector<double>& vOut ){
  for(unsigned int j=0; j<vOut.size(); j++){
      vOut[j] = array[j];
  }
}

// convert a vector<vec> into a mat:
mat vectorVec2Mat(const vector<vec>& U){
  const unsigned int n_rows = U[0].size();
  const unsigned int n_cols = U.size();
  mat output(n_rows, n_cols, U[0].comm());
  for(unsigned int iCol=0; iCol<U.size(); iCol++){
    for(unsigned int iRow=0; iRow<U[0].size(); iRow++){
      double value = U[iCol].getVecEl(iRow); // if comm is the same use getVecElNoBroadcast
      output.setMatEl(iRow,iCol,value);
    }
  }
  output.finalize();
  return output;
}

// convert a mat into a vector<vec>:
vector<vec> mat2vectorVec(const mat& U){
  const unsigned int nVec = U.nCols();
  const unsigned int vSize = U.nRows();
  vector<vec> output(nVec);
  for(unsigned int iVec=0; iVec<nVec; iVec++){
    output[iVec].init(vSize, U.comm());
    for(unsigned int iDof=0; iDof<vSize; iDof++){
      double value = U.getMatEl(iDof,iVec);
      output[iVec].setVecEl(iDof,value);
    }
    output[iVec].finalize();
  }
  return output;
}


// 0.7 print std vector on proc 0:
void printStdVec(vector<double>& toBePrinted, MPI_Comm theComm){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  if(rank==0){
    unsigned int theVecSize = toBePrinted.size();
    for(unsigned int j=0; j<theVecSize; j++){
      std::cout << "v(" << j << ") = " << toBePrinted[j] << endl;
    }
    std::cout << endl;
  }
}

// 0.7 print std vector on proc 0:
void printStdVecInt(vector<int>& toBePrinted, MPI_Comm theComm){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  if(rank==0){
    unsigned int theVecSize = toBePrinted.size();
    for(unsigned int j=0; j<theVecSize; j++){
      std::cout << "v(" << j << ") = " << toBePrinted[j] << endl;
    }
    std::cout << endl;
  }
}

// get the largest element (local on the proc)
void getLargestElem(const vector<double>& vec, int& jmax, double& sigmamax){
	sigmamax = 0;
	jmax = -1;
	for (int j=0; j<vec.size(); j++){
		if (vec[j]>sigmamax){
			jmax = j;
			sigmamax = vec[j];
		}
	}
}


//  merge two indices vectors, local on the proc
void mergeSortInt(vector<int>& a, vector<int>& b, vector<int>& c){

	c.resize(0);

	sort( a.begin(), a.end());
	sort( b.begin(), b.end());

	int i = 0, j = 0;
	while( i < a.size() && j < b.size()){
		if( a[ i ] == b[ j ] ){
   			c.push_back( a[ i ] );
   			i++;
			j++;
		}
		else if( a[ i ] < b[ j ] ){
   			c.push_back( a[ i ] );
			i++;
		}
		else{
   			c.push_back( b[j ] );
			j++;
		}
	}
	while( i < a.size()){
		c.push_back( a[i] );
		i++;
	}
	while( j < b.size()){
		c.push_back( b[j] );
		j++;
	}
}






// I -- OPERATIONS WITH VECTORS --


// I.1 - Initialize a Petsc Vec, given its pointer and its size:
void initVec(Vec& x, int itsSize, MPI_Comm theComm){
    PetscErrorCode ierr;
    ierr = VecCreate(theComm, &x);
    ierr = VecSetSizes(x,PETSC_DECIDE, itsSize);
    ierr = VecSetFromOptions(x);
}


// I.1.1 - Overloaded, copying the structure of another vector, b:
void initVec(Vec& toInit, Vec& b){
    PetscErrorCode ierr;
    ierr = VecDuplicate(b, &toInit);
}


/* setVecEl : setting one or more Elements
  external elements MUST be correctly available by the Procs !
*/

// I.2.1 Vec set values: just one element Proc 0 is doing the operation if PAR=false
void setVecEl(Vec& x, int ind, double val, MPI_Comm theComm, bool PAR = true){
  if(!PAR){
    int rank;
    MPI_Comm_rank (theComm, &rank);
    if(rank == 0){
      VecSetValues( x , 1 , &ind , &val , INSERT_VALUES );
    }
  }
  else{ // Parallel non-communicating way
    int startInd, endInd;
    VecGetOwnershipRange( x , &startInd , &endInd );
    if((ind>=startInd) && (ind<endInd)){
      VecSetValues( x , 1 , &ind , &val , INSERT_VALUES );
    }
  }
  // Need to finilize outsite!
}

// I.2.2 Vec set values: multiple values either in parallel or not:
void setVecEl(Vec& x, vector<double> val, MPI_Comm theComm,  bool PAR = true){
    int vecSize;
    VecGetSize(x, &vecSize);

    if(!PAR){ // Proc 0 is doing it and communicating it:
        int rank;
        MPI_Comm_rank (theComm, &rank);
        if(rank == 0){
           for(int h=0; h<vecSize; h++){
             VecSetValues( x , 1 , &h , &val[h] , INSERT_VALUES );
           }
        }
    }
    else{
       int startInd, endInd;
       VecGetOwnershipRange( x , &startInd , &endInd );
       for(int h=startInd; h<endInd; h++){
           VecSetValues( x , 1 , &h , &val[h] , INSERT_VALUES );
       }
    }
    VecAssemblyBegin(x);
    VecAssemblyEnd(x);
}


// I.2.3 set Vector from index low to high included equal to a given vector<double>
void setVecEl(Vec& x, int low, int high, vector<double> val, MPI_Comm theComm,  bool FIN = true, bool PAR = true){

  if(!PAR){ // Proc 0 is doing it and communicating it:
      int rank;
      MPI_Comm_rank (theComm, &rank);
      if(rank == 0){
         for(int h=low; h<high+1; h++){
           VecSetValues( x , 1 , &h , &val[h-low] , INSERT_VALUES );
         }
      }
  }
  else{
     int startInd, endInd;
     VecGetOwnershipRange( x , &startInd , &endInd );
     if( (startInd <= high) && (endInd >= low) ){ // intersection
       for(int h=low; h<high+1; h++){
          VecSetValues( x , 1 , &h , &val[h-low] , INSERT_VALUES );
       }
     }
  }
  if(FIN){
    VecAssemblyBegin(x);
    VecAssemblyEnd(x);
  }
}


// I.2.4 Set Elements with local vectors:
void setVecLocEl(Vec& x, vector<double> val, MPI_Comm theComm){
  int startInd, endInd;
  VecGetOwnershipRange( x , &startInd , &endInd );
  if(val.size() != endInd - startInd){puts("Wrong size of local vec!");};
  for(int h=startInd; h<endInd; h++){
      VecSetValues( x , 1 , &h , &val[h-startInd] , INSERT_VALUES );
  }
}


// I.3 Finalize the Assembly
void finalizeVec(Vec& x){
  VecAssemblyBegin(x);
  VecAssemblyEnd(x);
}


/* Global indices in 1D ordering to be passed! But VecGet is only local to proc!
   In these functions, result is broadcast to all the Procs if BROADCAST=true! Collective call;
*/

// I.4.0 generic and complete getVecEl
inline void getVecEl(Vec x, int ind, double& val, int& theRank, MPI_Comm theComm, bool BROADCAST=true){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  VecGetOwnershipRange( x , &startInd , &endInd );
  if(!BROADCAST){
    if( (ind >=startInd ) && (ind<endInd) ){
      VecGetValues(x, 1 , &ind, &val);
      theRank = rank;
    }
  }
  else{
    int amIRoot = 0;
    if( (ind >=startInd ) && (ind<endInd) ){
      VecGetValues(x, 1 , &ind, &val);
      amIRoot = 1;
    }
    int consensus = amIRoot * rank;
    int root = 0;
    MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, theComm);
    MPI_Bcast(&val, 1, MPI_DOUBLE, root, theComm);
  }
}


// I.4.0.b Get just one value and keep it local to the proc, collective call:
/* explicitly no broadcasting, faster
  - output: val and theRank (id of the proc having val)
*/

inline void getVecElNoBroadcast(Vec x, int ind, double& val, int& theRank, MPI_Comm theComm){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  VecGetOwnershipRange( x , &startInd , &endInd );
  if( (ind >=startInd ) && (ind<endInd) ){
    VecGetValues(x, 1 , &ind, &val);
    theRank = rank;
  }
}


// I.4.1 Get just one value and pass it to all Procs!:
/* overloaded, output is directly the double, all the procs have it */
double getVecEl(Vec x, int ind, MPI_Comm theComm){
  double toBeReturned = 0.0;
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  VecGetOwnershipRange( x , &startInd , &endInd );
  int amIRoot = 0;
  if( (ind >=startInd ) && (ind<endInd) ){
    VecGetValues(x, 1 , &ind, &toBeReturned);
    amIRoot = 1;
  }
  int consensus = amIRoot * rank;
  int root = 0;
  MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, theComm);
  MPI_Bcast(&toBeReturned, 1, MPI_DOUBLE, root, theComm);

  return toBeReturned;
}


// overloaded:
void getVecEl(Vec x, int ind, double& val, MPI_Comm theComm){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  VecGetOwnershipRange( x , &startInd , &endInd );
  int amIRoot = 0;
  if( (ind >=startInd ) && (ind<endInd) ){
    VecGetValues(x, 1 , &ind, &val);
    amIRoot = 1;
  }
  int consensus = amIRoot * rank;
  int root = 0;
  MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, theComm);
  MPI_Bcast(&val, 1, MPI_DOUBLE, root, theComm);
}


// I.4.2 Get a subset of the vector: from low to high included and pass it to all
void getVecEl(Vec x, int low, int high, vector<double> val, MPI_Comm theComm){
  if(val.size() != high - low + 1){puts("Wrong size of Vec!");};
  const int n = high-low+1;
  PetscInt ind[n];
  for(unsigned int h=0; h<n; h++){
    ind[h] = low + h;
  }
  PetscScalar y[n];
  for(int j=0; j<n; j++){
      getVecEl(x, ind[j], y[j], theComm);
  }
  for(unsigned int h=0; h<n; h++){
    val[h] = y[h];
  }
}

// overloaded, multiple values
vector<double> getVecEl(Vec x, int low, int high, MPI_Comm theComm){
    const int n = high-low+1;
    vector<double> toBeReturned;
    toBeReturned.resize(n);
    PetscInt ind[n];
    for(unsigned int h=0; h<n; h++){
      ind[h] = low + h;
    }
    PetscScalar y[n];
    for(int j=0; j<n; j++){
        getVecEl(x, ind[j], y[j], theComm);
    }
    for(unsigned int h=0; h<n; h++){
      toBeReturned[h] = y[h];
    }
    return toBeReturned;
}



/* transforming shared vector<double> to Vec and viceversa */

// I.5.1 Vec2vector converter
void Vec2vector(Vec& In, vector<double>& Out, MPI_Comm theComm){
  int vecSize;
  VecGetSize(In, &vecSize);
  if(Out.size()!=vecSize){Out.resize(vecSize);}
  getVecEl(In, 0, vecSize, Out, theComm);
}

// I.5.2 vector2Vec => passing just the pointer
void vector2Vec(vector<double> In, Vec& Out, MPI_Comm theComm){
  int vecSize = In.size();
  initVec(Out, vecSize, theComm);
  setVecEl(Out, In, theComm);
}

vec vector2vec(const std::vector<double>& v, const MPI_Comm& comm) {
  vec toBeReturned(v.size(), comm);
  for (unsigned int i=0; i<v.size(); ++i) {
    toBeReturned.setVecEl(i, v[i]);
  }
  toBeReturned.finalize();
  return toBeReturned;
}

std::vector<double> vec2vector(const vec& v) {
  std::vector<double> toBeReturned(v.size());
  for (unsigned int i=0; i<toBeReturned.size(); ++i) {
    toBeReturned[i] = v.getVecEl(i);
  }
  return toBeReturned;
}

mat vector2mat(const std::vector<double>& v, const MPI_Comm& comm) {
  mat toBeReturned(v.size(), v.size(), comm);
  for (unsigned int i=0; i<v.size(); ++i) {
    toBeReturned.setMatEl(i, i, v[i]);
  }
  toBeReturned.finalize();
  return toBeReturned;
}

mat vector2mat(const std::vector<vec>& v, const MPI_Comm& comm) {
  mat toBeReturned((v.size()==0) ? 0 : v[0].size(), v.size(), comm);
  for (unsigned int i=0; i<v.size(); ++i) {
    assert(v[i].size()==toBeReturned.nRows());
    toBeReturned.setCol(i, v[i]);
  }
  toBeReturned.finalize();
  return toBeReturned;
}

std::vector<vec> mat2vector(const mat& v) {
  std::vector<vec> toBeReturned(v.nCols());
  for (unsigned int i=0; i<toBeReturned.size(); ++i) {
    toBeReturned[i].copyVecFrom(v.getCol(i));
  }
  return toBeReturned;
}


// 0 -- copy vec from --
vec operator ~ (const vec& toBeCopied){
  vec toBeReturned;
  toBeReturned.copyVecFrom(toBeCopied);
  return toBeReturned;
}


/* 1 -- sum of two vecs
  - overloaded operator
*/
vec operator + (vec &v1, vec &v2){
     assert(v1.size()==v2.size());
     assert(v1.comm()==v2.comm());
     vec toBeReturned(v1.size(), v1.comm());
     toBeReturned.finalize();
     VecAXPY(toBeReturned.x(),1.0,v1.x());
     VecAXPY(toBeReturned.x(),1.0,v2.x());
     toBeReturned.finalize();
     return toBeReturned;
}


/* 1.b -- difference of two vecs
  - overloaded operator
*/
vec operator - (vec &v1, vec &v2){
     assert(v1.size()==v2.size());
     assert(v1.comm()==v2.comm());
     vec toBeReturned(v1.size(), v1.comm());
     toBeReturned.finalize();
     VecAXPY(toBeReturned.x(),+1.0,v1.x());
     VecAXPY(toBeReturned.x(),-1.0,v2.x());
     toBeReturned.finalize();
     return toBeReturned;
}


/* 1.c -- linear combination of vectors
  - input: vector<vec> U, the coeffcients vec a
  - output: linear combination
*/
vec lin_comb(const vector<vec>& U, const vec& a){
  assert(U.size()==a.size());
  const unsigned int nDof = U[0].size();
  vec output;
  output.copyVecFrom(U[0]);
  double first_coeff = a.getVecEl(0);
  output *= first_coeff;
  for(unsigned int jVec=1; jVec<U.size(); jVec++){
    double alpha = a.getVecEl(jVec);
    VecAXPY(output.x(), alpha, U[jVec].x());
  }
  return output;
}

// overloaded to have a vector<double>
vec lin_comb(vector<vec> U, vector<double> a){
  assert(U.size()==a.size());
  const unsigned int nDof = U[0].size();
  vec output;
  output.copyVecFrom(U[0]);
  double first_coeff = a[0];
  output *= first_coeff;
  for(unsigned int jVec=1; jVec<U.size(); jVec++){
    double alpha = a[jVec];
    VecAXPY(output.x(), alpha, U[jVec].x());
  }
  return output;
}
// overloaded to have the k-th column of the matrix A
vec lin_comb(const vector<vec>& U, const mat& A, const unsigned int kth_col){
  assert(U.size()==A.nRows());
  const unsigned int nDof = U[0].size();
  vec output;
  output.copyVecFrom(U[0]);
  double first_coeff = A.getMatEl(0,kth_col);
  output *= first_coeff;
  for(unsigned int jVec=1; jVec<U.size(); jVec++){
    double alpha = A.getMatEl(jVec,kth_col);
    VecAXPY(output.x(), alpha, U[jVec].x());
  }
  return output;
}
// overloaded to have more than one vec as output:
vector<vec> lin_comb(vector<vec> U, mat A){
  assert(U.size()==A.nRows());
  const unsigned int nDof = U[0].size();
  vector<vec> output(A.nCols());
  for(unsigned int iVec=0; iVec<A.nCols(); iVec++){
    output[iVec].copyVecFrom(U[0]);
    double alpha = A.getMatEl(0,iVec);
    output[iVec] *= alpha;
  }
  // compute the outputs:
  for(unsigned int iVec=0; iVec<A.nCols(); iVec++){
    for(unsigned int jVec=1; jVec<U.size(); jVec++){
      double alpha = A.getMatEl(jVec,iVec);
      VecAXPY(output[iVec].x(), alpha, U[jVec].x());
    }
  }
  return output;
}
// overloaded to have more than one vec as output and vector<vec> instead of mat:
vector<vec> lin_comb(vector<vec> U, vector<vec> A){
  assert(U.size()==A[0].size());
  const unsigned int nDof = U[0].size();
  vector<vec> output(A.size());
  for(unsigned int iVec=0; iVec<A.size(); iVec++){
    output[iVec].copyVecFrom(U[0]);
    double alpha = A[iVec].getVecEl(0);
    output[iVec] *= alpha;
  }
  // compute the outputs:
  for(unsigned int iVec=0; iVec<A.size(); iVec++){
    for(unsigned int jVec=1; jVec<U.size(); jVec++){
      double alpha = A[iVec].getVecEl(jVec);
      VecAXPY(output[iVec].x(), alpha, U[jVec].x());
    }
  }
  return output;
}

vec lin_comb(const unsigned int nDof_x, const unsigned int nDof_y, const vector<vec>& terms_x, const vector<double>& coeffs, const vector<vec>& terms_y, const MPI_Comm& comm, const unsigned int jTerm) {
  assert(coeffs.size()==terms_x.size() && coeffs.size()==terms_y.size() && jTerm<nDof_y);

  vec output(nDof_x, comm);
  for (unsigned int iTerm=0; iTerm<terms_x.size(); ++iTerm) {
    VecAXPY(output.x(), coeffs[iTerm]*terms_y[iTerm].getVecEl(jTerm), terms_x[iTerm].x());
  }
  return output;
}

vector<vec> lin_comb(const unsigned int nDof_x, const unsigned int nDof_y, const vector<vec>& terms_x, const vector<double>& coeffs, const vector<vec>& terms_y, const MPI_Comm& comm) {
  assert(coeffs.size()==terms_x.size() && coeffs.size()==terms_y.size());

  vector<vec> output(nDof_y);
  for (unsigned int iTerm=0; iTerm<output.size(); ++iTerm) {
    output[iTerm] = lin_comb(nDof_x, nDof_y, terms_x, coeffs, terms_y, comm, iTerm);
  }
  return output;
}





/* 2 -- scalar product
  - inputs: two vec
  - output: their l-2 scalar product
*/
double scalProd(const vec& v1, const vec& v2) {
  assert(v1.size()==v2.size());
  assert(v1.comm()==v2.comm());
  double toBeReturned = 0.0;
  VecDot(v1.x(), v2.x(), &toBeReturned);
  return toBeReturned;
}


/* 2.1 -- scalar product, overloaded
  - inputs: two vec, a mass matrix
  - output: their scalar product
*/
double scalProd(vec& v1, vec& v2, mat& mass){
  assert(v1.size()==v2.size());
  assert(v1.comm()==v2.comm());
  assert(mass.nRows()==v1.size());
  assert(mass.nCols()==v2.size());
  assert(mass.comm()==v1.comm());

  double toBeReturned = 0.0;
  Vec tmp;
  initVec(tmp, v1.size(), v1.comm());
  finalizeVec(tmp);
  MatMult(mass.M(), v2.x(), tmp);
  VecDot(v1.x(), tmp, &toBeReturned);
  VecDestroy(&tmp);
  return toBeReturned;
}

double norm(const vec& v) {
  return v.norm();
}



/* 3 -- pointwise multiplication
  - function:
  - Remark, alternative syntaxes:
  for(unsigned int iDof=0; iDof<v1.size(); iDof++){

    double val1 = v1.getVecEl(iDof);
    double val2 = v2.getVecEl(iDof);
    double val = val1 * val2;
    toBeReturned.setVecEl(iDof, val);

    // or through the Proxy:
    toBeReturned(iDof) = v1(iDof) * v2(iDof);
  }
*/
vec pointWiseMult(vec& v1, vec&v2){
  assert(v1.size()==v2.size());
  assert(v1.comm()==v2.comm());
  vec toBeReturned(v1.size(), v1.comm());
  toBeReturned.finalize();
  VecPointwiseMult(toBeReturned.x(), v1.x() , v2.x());
  return toBeReturned;
}


/* Matrix vector Product
  - input: mat A, vec v;
  - output: y = A v
*/
vec operator * (mat A, vec v){
  assert(A.nCols() == v.size());
  assert(A.comm() == v.comm());
  vec toBeReturned(A.nRows(), A.comm());
  toBeReturned.finalize();
  MatMult(A.M(), v.x(), toBeReturned.x());
  return toBeReturned;
}


/* Matrix vector product
  - implicit arguments
  - output: w
*/
void matVecProd(const mat& A, const vec& v, vec& w) {
  assert(A.nCols() == v.size());
  assert(A.comm() == v.comm());
  w.init(A.nRows(), A.comm());
  w.finalize();
  MatMult(A.M(), v.x(), w.x());
}

// Perform v = A*u
void MatMult(const mat& A, const vec& u, vec& v) {
  assert(A.nRows()==v.size() && A.nCols()==u.size());
  MatMult(A.M(), u.x(), v.x());
}

// Perform v = A*u
void MatMult(const std::vector<vec>& A, const vec& u, vec& v) {
  assert(A.size()==u.size());
  PetscScalar zero = 0.0;
  if (u.size()==0) {
    VecSet(v.x(), zero);
    v.finalize();
  } else {
    assert(A[0].size()==v.size());
    VecAXPBY(v.x(), u.getVecEl(0), zero, A[0].x());
    for (unsigned int i=1; i<u.size(); ++i) {
      assert(A[i].size()==v.size());
      VecAXPY(v.x(), u.getVecEl(i), A[i].x());
    }
  }
}

// Perform v = A'*u
void MatMultTranspose(const std::vector<vec>& A, const vec& u, vec& v) {
  assert(A.size()==v.size());
  double val;
  for (unsigned int i=0; i<A.size(); ++i) {
    assert(A[i].size()==u.size());
    VecDot(A[i].x(), u.x(), &val);
    v.setVecEl(i, val);
  }
  v.finalize();
}

// Perform w = v+A*u
void MatMultAdd(const mat& A, const vec& u, const vec& v, vec& w) {
  assert(v.size()==w.size() && A.nRows()==w.size() && A.nCols()==u.size());
  MatMultAdd(A.M(), u.x(), v.x(), w.x());
}

// Perform C = A*B
void MatMatMult(const mat& A, const std::vector<vec>& B, std::vector<vec>& C) {
  assert(B.size()==C.size());
  for (unsigned int j=0; j<C.size(); ++j) {
    assert(A.nRows()==C[j].size() && A.nCols()==B[j].size());
    MatMult(A.M(), B[j].x(), C[j].x());
  }
}

// Perform C = A*B
std::vector<vec> MatMatMult(const mat& A, const std::vector<vec>& B) {
  std::vector<vec> C(B.size());
  for (unsigned int j=0; j<C.size(); ++j) {
    C[j].init(A.nRows(), B[j].comm());
    C[j].finalize();
  }
  MatMatMult(A, B, C);
  return C;
}

// Perform C = A'*B
void MatTransposeMatMult(const std::vector<vec>& A, const std::vector<vec>& B, mat& C) {
  assert(A.size()==C.nRows() && B.size()==C.nCols());
  double val;
  for (unsigned int i=0; i<C.nRows(); ++i) {
    for (unsigned int j=0; j<C.nCols(); ++j) {
      assert(A[i].size()==B[j].size());
      VecDot(A[i].x(), B[j].x(), &val);
      C.setMatEl(i, j, val);
    }
  }
  C.finalize();
}

// Perform C = A'*B
mat MatTransposeMatMult(const std::vector<vec>& A, const std::vector<vec>& B) {
  mat C;
  if (A.size()>0) {
    C.init(A.size(), B.size(), A[0].comm());
  } else {
    C.init(A.size(), B.size());
  }
  MatTransposeMatMult(A, B, C);
  return C;
}

/* Matrix scaling by a factor
  - input: double alpha, mat A;
  - output: A = alpha * A
*/
mat operator * (double alpha, mat A){
  mat toBeReturned;
  toBeReturned.copyMatFrom(A);
  toBeReturned *= alpha;
  return toBeReturned;
}



/* create the vector filled with 0's, not sparse */
vec zeros(unsigned int nDof, MPI_Comm theComm){
  vec toBeReturned(nDof, theComm);
  for(unsigned int iDof=0; iDof<nDof; iDof++){
    toBeReturned(iDof) = 0.0;
  }
  toBeReturned.finalize();
  return toBeReturned;
}

/* create the vector filled with 1's,  */
vec ones(unsigned int nDof, MPI_Comm theComm){
  vec toBeReturned(nDof, theComm);
  toBeReturned.ones();
  return toBeReturned;
}

/* random vector implementation */
vec rand(unsigned int dim, MPI_Comm theComm){
  PetscRandom rctx;
  PetscRandomCreate(theComm,&rctx);
  PetscRandomSetSeed(rctx,std::chrono::system_clock::now().time_since_epoch().count());
  PetscRandomSeed(rctx);

  Vec entries;
  initVec(entries, dim, theComm);
  finalizeVec(entries);
  VecSetRandom(entries,rctx);

  vec toBeReturned;
  toBeReturned.setComm(theComm);
  toBeReturned.setVector(entries);
  toBeReturned.getSize();
  toBeReturned.finalize();
  PetscRandomDestroy(&rctx);

  return toBeReturned;
}


/* get matrix column
  - inputs a mat and the index of the column
  - output the vector which is the matrix column
*/
vec getCol(mat A, unsigned int iCol){
  const unsigned int nRows = A.nRows();
  const unsigned int nCols = A.nCols();
  assert(iCol<nCols);
  vec out(nRows,A.comm());
  for(unsigned int iRow=0; iRow<nRows; iRow++){
    double value = A.getMatEl(iRow,iCol);
    if(fabs(value)>DBL_EPSILON){
      out.setVecEl(iRow, value);
    }
  }
  out.finalize();
  return out;
}



// II - OPERATIONS for MATRICES:

// II.1 - Initialize a Matrix, given its pointer and its sizes:
void initMat(Mat& A, int nRows, int nCols, MPI_Comm theComm){
   PetscErrorCode ierr;
   ierr = MatCreate(theComm, &A);
   ierr = MatSetSizes(A, PETSC_DECIDE, PETSC_DECIDE, nRows, nCols);
   ierr = MatSetFromOptions(A);
   ierr = MatSetUp(A);
}

// II.1.2 overloaded share structure and non-zero pattern:
void initMat(Mat& matToInit, Mat A){
    PetscErrorCode ierr;
    ierr = MatDuplicate(A, MAT_SHARE_NONZERO_PATTERN, &matToInit);
}



/* set Matrix Elements or rows or cols */

// II.2.1 Setting just one element:
void setMatEl(Mat& A, int indRow, int indCol, double val, MPI_Comm theComm, bool PAR=true){
  if(!PAR){
    int rank;
    MPI_Comm_rank (theComm, &rank);
    if(rank == 0){ // Proc 0 is doing it and communicating it
      MatSetValues(A, 1, &indRow, 1, &indCol, &val, INSERT_VALUES);
    }
  }
  else{ //Parallel non-communicating way
    int startInd, endInd;
    MatGetOwnershipRange(A, &startInd, &endInd);
    if( (indRow>=startInd) &&(indRow<endInd) ){
      MatSetValues(A, 1, &indRow, 1, &indCol, &val, INSERT_VALUES);
    }
  }
  // Need to Finalize the Assembly outside !
}


// II.2.2 Setting a whole matrix by providing a dense full Table
void setMatEl(Mat& A, vector<vector<double>* > theMat, MPI_Comm theComm,  bool PAR = true){
    int nRows, nCols;
    MatGetSize(A, &nRows, &nCols);
    if(!PAR){ // Proc 0 is doing it row by row and communicates it.
      int rank;
      MPI_Comm_rank(theComm, &rank);
      if(rank == 0){
          for(int i=0; i<nRows; i++){
            vector<int> pattern = findNonZero(*theMat[i]);
            if(pattern.size()>0){
              PetscInt cols[pattern.size()];
              vector2PetscInt(pattern, cols);
              vector<double> theRow = nonZero(*theMat[i]);
              PetscScalar val[pattern.size()];
              vector2PetscScalar(theRow, val);
              MatSetValues(A,1,&i, pattern.size(), cols , val , INSERT_VALUES);
            }
          }
      }
    }
    else{ // Parallel non-communicating way.
      int startInd, endInd;
      MatGetOwnershipRange(A, &startInd, &endInd);
      for(int i=startInd; i<endInd; i++){
        vector<int> pattern = findNonZero(*theMat[i]);
        if(pattern.size()>0){
          PetscInt cols[pattern.size()];
          vector2PetscInt(pattern, cols);
          vector<double> theRow = nonZero(*theMat[i]);
          PetscScalar val[pattern.size()];
          vector2PetscScalar(theRow, val);
          MatSetValues(A,1,&i, pattern.size(), cols , val , INSERT_VALUES);
        }
      }
    }
    MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
}


// II.2.3 Assemble the Matrix by providing a table containing the pattern and a table containing the non-zero entries
void setMatEl(Mat& A, vector<vector<int>* > pattern, vector<vector<double>* > theEntries, MPI_Comm theComm,  bool PAR = true){
  int nRows, nCols;
  MatGetSize(A, &nRows, &nCols);
  if(!PAR){ // Proc 0 is doing it row by row and communicates it.
    int rank;
    MPI_Comm_rank(theComm, &rank);
    if(rank == 0){
        for(int i=0; i<nRows; i++){
          if(pattern[i]->size()>0){
            PetscInt cols[pattern[i]->size()];
            vector2PetscInt(*pattern[i], cols);
            PetscScalar val[pattern[i]->size()];
            vector2PetscScalar(*theEntries[i], val);
            MatSetValues(A,1,&i, pattern[i]->size(), cols , val , INSERT_VALUES);
          }
        }
    }
  }
  else{ // Parallel non-communicating way.
    int startInd, endInd;
    MatGetOwnershipRange(A, &startInd, &endInd);
    for(int i=startInd; i<endInd; i++){
      if(pattern[i]->size()>0){
        PetscInt cols[pattern[i]->size()];
        vector2PetscInt(*pattern[i], cols);
        PetscScalar val[pattern[i]->size()];
        vector2PetscScalar(*theEntries[i], val);
        MatSetValues(A,1,&i, pattern[i]->size(), cols , val , INSERT_VALUES);
      }
    }
  }
  MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
}



// II.2.4 Set Matrix by providing local dense submatrices of the right size.
void setMatLocEl(Mat& A, vector<vector<double>* > theMat, MPI_Comm theComm){
  int startInd, endInd;
  MatGetOwnershipRange(A, &startInd, &endInd);
  for(int i=startInd; i<endInd; i++){ // i is the global index = local + startInd
    vector<int> pattern = findNonZero(*theMat[i-startInd]);
    if(pattern.size()>0){
      PetscInt cols[pattern.size()];
      vector2PetscInt(pattern, cols);
      vector<double> theRow = nonZero(*theMat[i-startInd]);
      PetscScalar val[pattern.size()];
      vector2PetscScalar(theRow, val);
      MatSetValues(A,1,&i, pattern.size(), cols , val , INSERT_VALUES);
    }
  }
  MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
}


// II.2.5 Set Matrix by providing local sparse submatrices of the right size.
void setMatLocEl(Mat& A, vector<vector<int>*> pattern, vector<vector<double>* > theEntries, MPI_Comm theComm){
  int startInd, endInd;
  MatGetOwnershipRange(A, &startInd, &endInd);
  for(int i=startInd; i<endInd; i++){
    int local = i-startInd;
    const int n = pattern[local]->size();
    if(n>0){
      PetscInt cols[n];
      vector2PetscInt(*pattern[local], cols);
      PetscScalar val[n];
      vector2PetscScalar(*theEntries[local], val);
      MatSetValues(A,1,&i, n, cols , val , INSERT_VALUES);
    }
  }
  MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
}



// II.2.6 Set a Matrix Row by providing a dense vector: FINALIZE OUTSIDE!
void setMatRow(Mat&A, int idRow, vector<double>& theRow, MPI_Comm theComm, bool PAR=true){
  if(!PAR){ // Proc 0 is doing it and communicates it.
    int rank;
    MPI_Comm_rank(theComm, &rank);
    if(rank == 0){
        vector<int> pattern = findNonZero(theRow);
        if(pattern.size()>0){
            PetscInt cols[pattern.size()];
            vector2PetscInt(pattern, cols);
            vector<double> toInsert = nonZero(theRow);
            PetscScalar val[pattern.size()];
            vector2PetscScalar(toInsert, val);
            MatSetValues(A,1,&idRow, pattern.size(), cols , val , INSERT_VALUES);
        }
    }
  }
  else{ // Parallel non-communicating way: just the Proc that has the row does it
    int startInd, endInd;
    MatGetOwnershipRange(A, &startInd, &endInd);
    vector<int> pattern = findNonZero(theRow);
    if( (idRow>=startInd) && (idRow<endInd)){
      if(pattern.size()>0){
        PetscInt cols[pattern.size()];
        vector2PetscInt(pattern, cols);
        vector<double> toInsert = nonZero(theRow);
        PetscScalar val[pattern.size()];
        vector2PetscScalar(toInsert, val);
        MatSetValues(A,1,&idRow, pattern.size(), cols , val , INSERT_VALUES);
      }
    }
  }
  // Have to Finalize Outside !!
}




// II.2.7 Set a matrix row by providing a sparse vector: FINALIZE OUTSIDE!
void setMatRow(Mat&A, int idRow, vector<int>& colInd, vector<double>& theRow, MPI_Comm theComm, bool PAR=true){
  if(!PAR){ // Proc 0 is doing it and communicates it.
    int rank;
    MPI_Comm_rank(theComm, &rank);
    if(rank == 0){
        const int n = colInd.size();
        if(n>0){
            PetscInt cols[n];
            vector2PetscInt(colInd, cols);
            vector<double> toInsert = nonZero(theRow);
            PetscScalar val[n];
            vector2PetscScalar(toInsert, val);
            MatSetValues(A,1,&idRow, n, cols , val , INSERT_VALUES);
        }
    }
  }
  else{ // Parallel non-communicating way: just the Proc that has the row do it
    int startInd, endInd;
    MatGetOwnershipRange(A, &startInd, &endInd);
    if( (idRow>=startInd) && (idRow<endInd)){
      const int n = colInd.size();
      if(n>0){
        PetscInt cols[n];
        vector2PetscInt(colInd, cols);
        vector<double> toInsert = nonZero(theRow);
        PetscScalar val[n];
        vector2PetscScalar(toInsert, val);
        MatSetValues(A,1,&idRow, n, cols , val , INSERT_VALUES);
      }
    }
  }
  // Have to Finalize Outside !!
}


// II.3 Finalizing the Assembly
void finalizeMat(Mat& A){
  MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
}




/* Routines to get matrix elements with a collective call */

// II.4.0 get matrix element and decide if broadcasting or not:
inline void getMatEl(Mat A, int idRow, int idCol, double& val, int& theRank, MPI_Comm theComm, bool BROADCAST=true){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  MatGetOwnershipRange( A , &startInd , &endInd );
  if(!BROADCAST){
    if( (idRow >=startInd ) && (idRow<endInd) ){
      MatGetValues(A, 1, &idRow, 1, &idCol, &val);
      theRank = rank;
    }
  }
  else{
    int amIRoot = 0;
    if( (idRow >=startInd ) && (idRow<endInd) ){
      MatGetValues(A, 1, &idRow, 1, &idCol, &val);
      amIRoot = 1;
    }
    int consensus = amIRoot * rank;
    int root = 0;
    MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, theComm);
    MPI_Bcast(&val, 1, MPI_DOUBLE, root, theComm);
  }
}

// II.4.0.b collective call, explicitly no broadcast, faster
inline void getMatEl(Mat A, int idRow, int idCol, double& val, int& theRank, MPI_Comm theComm){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  MatGetOwnershipRange( A , &startInd , &endInd );
  if( (idRow >=startInd ) && (idRow<endInd) ){
    MatGetValues(A, 1, &idRow, 1, &idCol, &val);
    theRank = rank;
  }
}


// II.4 Get just one element, specified by idRow and idCol, global indices:
double getMatEl(Mat A, int idRow, int idCol, MPI_Comm theComm){
  double toBeReturned = 0.0;
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  MatGetOwnershipRange( A , &startInd , &endInd );
  int amIRoot = 0;
  if( (idRow >=startInd ) && (idRow<endInd) ){
    MatGetValues(A, 1, &idRow, 1, &idCol, &toBeReturned);
    amIRoot = 1;
  }
  int consensus = amIRoot * rank;
  int root = 0;
  MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, theComm);
  MPI_Bcast(&toBeReturned, 1, MPI_DOUBLE, root, theComm);

  return toBeReturned;
}


/* Generic Routines for matrix manipulation */


// Setting a column equal to a vec. Have to finalize outside!
void setMatCol(Mat& A, int idCol, Vec v, MPI_Comm theComm){
    int nRows, nCols;
    MatGetSize(A, &nRows, &nCols);
    int vecSize;
    VecGetSize(v, &vecSize);
    if(vecSize != nRows){puts("Wrong size of column!");}
    int lowVec, highVec;
    VecGetOwnershipRange(v, &lowVec, &highVec);
    int lowMat, highMat;
    MatGetOwnershipRange(A, &lowMat, &highMat);

    for(int i=lowVec; i<highVec; i++){
        if( (i>=lowMat) && (i<highMat) ){ // no communication needed
          PetscScalar val;
          VecGetValues(v,1,&i,&val);
          MatSetValue(A,i,idCol,val,INSERT_VALUES);
        }
        else{ // collective call to share the value
          double globVal = getMatEl(A, i, idCol, theComm);
          setMatEl(A, i, idCol, globVal, theComm);
        }
    }
}


// Computing the transpose of a matrix, returning a finalized matrix
Mat transpose(Mat A, MPI_Comm theComm){
    int nRowA, nColA;
    MatGetSize(A, &nRowA, &nColA);
    int nRow = nColA;
    int nCol = nRowA;
    Mat theTranspose;
    initMat(theTranspose, nRow, nCol, theComm);
    MatTranspose(A, MAT_INITIAL_MATRIX, &theTranspose);
    finalizeMat(theTranspose);
    return theTranspose;
}

// overloaded to take existing pointer:
void transpose(Mat A, Mat& theTranspose, MPI_Comm theComm){
    int nRowA, nColA;
    MatGetSize(A, &nRowA, &nColA);
    int nRow = nColA;
    int nCol = nRowA;
    initMat(theTranspose, nRow, nCol, theComm);
    MatTranspose(A, MAT_INITIAL_MATRIX, &theTranspose);
    finalizeMat(theTranspose);
}

// function that returns mat:
mat transpose(mat A){
  mat toBeReturned;
  Mat theTranspose;
  initMat(theTranspose, A.nRows(), A.nCols(), A.comm());
  MatTranspose(A.M(), MAT_INITIAL_MATRIX, &theTranspose);
  finalizeMat(theTranspose);
  toBeReturned.setComm(A.comm());
  toBeReturned.setMatrix(theTranspose);
  return toBeReturned;
}



// Computing the hermitian of a matrix, returning a finalized matrix
Mat hermitian(Mat A, MPI_Comm theComm){
    int nRowA, nColA;
    MatGetSize(A, &nRowA, &nColA);
    int nRow = nColA;
    int nCol = nRowA;
    Mat theTranspose;
    initMat(theTranspose, nRow, nCol, theComm);
    MatHermitianTranspose(A, MAT_INITIAL_MATRIX, &theTranspose);
    finalizeMat(theTranspose);
    return theTranspose;
}

// overloaded to take existing pointer:
void hermitian(Mat A, Mat& theTranspose, MPI_Comm theComm){
    int nRowA, nColA;
    MatGetSize(A, &nRowA, &nColA);
    int nRow = nColA;
    int nCol = nRowA;
    initMat(theTranspose, nRow, nCol, theComm);
    MatHermitianTranspose(A, MAT_INITIAL_MATRIX, &theTranspose);
    finalizeMat(theTranspose);
}

// function that returns the hermitian of a mat:
mat hermitian(mat A){
  mat toBeReturned;
  Mat theHermitian;
  initMat(theHermitian, A.nRows(), A.nCols(), A.comm());
  MatHermitianTranspose(A.M(), MAT_INITIAL_MATRIX, &theHermitian);
  finalizeMat(theHermitian);
  toBeReturned.setComm(A.comm());
  toBeReturned.setMatrix(theHermitian);
  return toBeReturned;
}


// Identity:

// overloaded to take an existing pointer:
void eye(Mat& I, int n, MPI_Comm theComm){
    Vec d;
    initVec(d, n, theComm);
    VecSet(d, 1.0);
    finalizeVec(d);
    initMat(I, n, n, theComm);
    MatDiagonalSet(I, d, INSERT_VALUES);
    finalizeMat(I);
}

// function that returns a mat:
mat eye(unsigned int n, MPI_Comm theComm){
  Vec d;
  Mat I;
  initVec(d, n, theComm);
  VecSet(d, 1.0);
  finalizeVec(d);
  initMat(I, n, n, theComm);
  MatDiagonalSet(I, d, INSERT_VALUES);
  finalizeMat(I);
  VecDestroy(&d);
  mat toBeReturned;
  toBeReturned.setMatrix(I);
  toBeReturned.setComm(theComm);
  return toBeReturned;
}


/* reshape function for Petsc matrices
  - inputs: the matrix to be reshaped, the dimensions
  - output: a matrix
*/
inline Mat reshape(Mat& matToReshape, unsigned int newRows, unsigned int newCols, MPI_Comm theComm){
  PetscInt nRows, nCols;
  MatGetSize(matToReshape, &nRows, &nCols);
  assert(newRows * newCols == nRows * nCols);
  assert( (newRows % nRows == 0) || (nRows % newRows == 0) );
  assert( (newCols % nCols == 0) || (nCols % newCols == 0) );
  Mat toBeReturned;
  initMat(toBeReturned, newRows, newCols, theComm);
  for(unsigned int idRow=0; idRow<nRows; idRow++){
    for(unsigned int idCol=0; idCol<nCols; idCol++){
      unsigned int linInd = idCol * nRows + idRow;
      unsigned int J = linInd / newRows;
      unsigned int I = linInd % newRows;
      double value = getMatEl(matToReshape, idRow, idCol, theComm);
      setMatEl(toBeReturned, I , J , value, theComm);
    }
  }
  finalizeMat(toBeReturned);
  return toBeReturned;
}

// reshape function for a mat object, that returns a mat object:
mat reshape(mat A, unsigned int newRows, unsigned int newCols){
  assert(newRows * newCols == A.nRows() * A.nCols());
  assert( (newRows % A.nRows() == 0) || (A.nRows() % newRows == 0) );
  assert( (newCols % A.nCols() == 0) || (A.nCols() % newCols == 0) );

  mat toBeReturned(newRows, newCols, A.comm());

  for(unsigned int idRow=0; idRow<A.nRows(); idRow++){
    for(unsigned int idCol=0; idCol<A.nCols(); idCol++){
      unsigned int linInd = idCol * A.nRows() + idRow;
      unsigned int J = linInd / newRows;
      unsigned int I = linInd % newRows;
      double value = A.getMatEl(idRow, idCol);
      if(value != 0.0){
        toBeReturned.setMatEl(I , J , value);
      }
    }
  }
  toBeReturned.finalize();

  return toBeReturned;
};


/* vectorise a Petsc matrix
  - inputs: the matrix to be reshaped, the dimensions
  - output: a vector
*/
inline Vec vectoriseMat(Mat& toBeVectorised, MPI_Comm theComm){
  PetscInt nRows, nCols;
  MatGetSize(toBeVectorised, &nRows, &nCols);
  const unsigned int vecSize = nRows * nCols;
  Vec toBeReturned;
  initVec(toBeReturned, vecSize, theComm);
  for(unsigned int idRow=0; idRow<nRows; idRow++){
    for(unsigned int idCol=0; idCol<nCols; idCol++){
      unsigned int linInd = idCol * nRows + idRow;
      double value = getMatEl(toBeVectorised, idRow, idCol, theComm);
      setVecEl(toBeReturned, linInd, value, theComm);
    }
  }
  finalizeVec(toBeReturned);
  return toBeReturned;
}


// function that returns a vec
vec vectoriseMat(mat& toBeVectorised){
  unsigned int vecSize = toBeVectorised.nRows() * toBeVectorised.nCols();
  vec toBeReturned(vecSize, toBeVectorised.comm());
  for(unsigned int idRow=0; idRow<toBeVectorised.nRows(); idRow++){
    for(unsigned int idCol=0; idCol<toBeVectorised.nCols(); idCol++){
      unsigned int linInd = idCol * toBeVectorised.nRows() + idRow;
      double value = toBeVectorised.getMatEl(idRow, idCol);
      if(value != 0.0){
        toBeReturned.setVecEl(linInd, value);
      }
    }
  }
  toBeReturned.finalize();
  return toBeReturned;
}



// produce a diagonal matrix given a vector:
void diag(Mat& D, Vec d, MPI_Comm theComm){
    int n;
    VecGetSize(d, &n);
    initMat(D, n, n, theComm);
    MatDiagonalSet(D, d, INSERT_VALUES);
    finalizeMat(D);
}

// overloaded to provide a matrix:
Mat diag(Vec d, MPI_Comm theComm){
    int n;
    VecGetSize(d, &n);
    Mat D;
    initMat(D, n, n, theComm);
    MatDiagonalSet(D, d, INSERT_VALUES);
    finalizeMat(D);
    return D;
}

// function that returns a mat object:
mat diag(const vec& d){
  mat toBeReturned;

  Mat D;
  initMat(D, d.size(), d.size(), d.comm());
  MatDiagonalSet(D, d.x(), INSERT_VALUES);
  finalizeMat(D);

  toBeReturned.setComm(d.comm());
  toBeReturned.setMatrix(D);
  return toBeReturned;
}

// function that, given a mat, returns a vec whose entries are the diagonal
vec diag(const mat& A){
  assert(A.nRows()==A.nCols());
  vec toBeReturned(A.nRows(), A.comm());
  for(unsigned int iDof=0; iDof<A.nRows(); iDof++){
    double value = A.getMatEl(iDof,iDof);
    if(value != 0.0){
      toBeReturned.setVecEl(iDof, value);
    }
  }
  toBeReturned.finalize();
  return toBeReturned;
}

// summing two mat to provide a mat:
mat operator + (mat m1, mat m2){
  assert(m1.nRows()==m2.nRows());
  assert(m1.nCols()==m2.nCols());
  assert(m1.comm()==m2.comm());
  mat toBeReturned(m1.nRows(),m1.nCols(), m1.comm());
  toBeReturned.finalize();
  MatAXPY(toBeReturned.M(), 1.0, m1.M(), DIFFERENT_NONZERO_PATTERN);
  MatAXPY(toBeReturned.M(), 1.0, m2.M(), DIFFERENT_NONZERO_PATTERN);
  toBeReturned.finalize();
  return toBeReturned;
}

// multiplying two matrices to get a mat:
mat operator * (mat m1, mat m2){
  assert(m1.nCols()==m2.nRows());
  assert(m1.comm()==m2.comm());
  Mat theResult;
  initMat(theResult, m1.nRows(), m2.nCols(), m1.comm());
  finalizeMat(theResult);
  MatMatMult(m1.M(), m2.M(), MAT_INITIAL_MATRIX, PETSC_DEFAULT, &theResult);
  mat toBeReturned;
  toBeReturned.setComm(m1.comm());
  toBeReturned.setMatrix(theResult);
  return toBeReturned;
}

// plain function
void matMatMult(mat& m1, mat& m2, mat& result){
  assert(m1.nCols()==m2.nRows());
  assert(m1.comm()==m2.comm());
  Mat theResult;
  initMat(theResult, m1.nRows(), m2.nCols(), m1.comm());
  finalizeMat(theResult);
  MatMatMult(m1.M(), m2.M(), MAT_INITIAL_MATRIX, PETSC_DEFAULT, &theResult);
  result.setComm(m1.comm());
  result.setMatrix(theResult);
  result.finalize();
}


// -- I/O operations: --

// saving a vector in binary format:
void saveVec(const vec& v, const string& fileName){
  PetscViewer viewer;
  PetscViewerBinaryOpen(v.comm(), fileName.c_str(), FILE_MODE_WRITE, &viewer);
  VecView(v.x(), viewer);
  PetscViewerDestroy(&viewer);
}

// loading a vector in binary format:
vec loadVec(const string& fileName, const MPI_Comm& m_comm){
  Vec m_x;
  PetscViewer viewer;
  PetscViewerBinaryOpen(m_comm, fileName.c_str(), FILE_MODE_READ, &viewer);
  VecCreate(m_comm, &m_x);
  VecLoad(m_x, viewer);
  PetscViewerDestroy(&viewer);
  vec toBeReturned;
  toBeReturned.setComm(m_comm);
  toBeReturned.setVector(m_x);
  toBeReturned.getSize();
  toBeReturned.finalize();
  return toBeReturned;
}

// save vector in ASCII:
void saveVecASCII(vec& toBeSaved, string fileName){
  ofstream outfile;
  outfile.open(fileName.c_str());
  for(unsigned int iDof=0; iDof<toBeSaved.size(); iDof++){
    outfile << toBeSaved.getVecEl(iDof) << endl;
  }
  outfile.close();
}

// load from ASCII: -- useful for freefem++
vec loadVecASCII(string fileName, MPI_Comm m_comm){
    vector<double> nonZerosList;
    vector<unsigned int> idList;
    double thisEntry = 0.0;

    ifstream inputFile;
    inputFile.open(fileName.c_str());
    char output[128];
    unsigned int cc = 0;
    if (inputFile.is_open()) {
      while (!inputFile.eof()) {
        inputFile >> output;
        stringstream str;
        str << output;
        str >> thisEntry;
        if(fabs(thisEntry)!=0.){
          idList.push_back(cc);
          nonZerosList.push_back(thisEntry);
        }
        cc += 1;
      }
    }
    else{
      puts("Unable to read file!");
      exit(1);
    }
    cc = cc -1;
    vec toBeReturned(cc, m_comm);
    for(unsigned int iEl=0; iEl<nonZerosList.size(); iEl++){
      toBeReturned.setVecEl(idList[iEl], nonZerosList[iEl]);
    }
    toBeReturned.finalize();
    return toBeReturned;
}


// saving a matrix in binary format:
void saveMat(const mat& toBeSaved, const string& fileName){
  PetscViewer viewer;
  PetscViewerBinaryOpen(toBeSaved.comm(), fileName.c_str(), FILE_MODE_WRITE, &viewer);
  MatView(toBeSaved.M(), viewer);
  PetscViewerDestroy(&viewer);
}

// loading a matrix in binary format:
mat loadMat(const string& fileName, const MPI_Comm& m_comm) {
  Mat loadedMat;
  PetscViewer viewer;
  PetscViewerBinaryOpen(m_comm, fileName.c_str(), FILE_MODE_READ, &viewer);
  MatCreate(m_comm, &loadedMat);
  MatLoad(loadedMat, viewer);
  PetscViewerDestroy(&viewer);
  mat toBeReturned;
  toBeReturned.setComm(m_comm);
  toBeReturned.setMatrix(loadedMat);
  toBeReturned.finalize();
  return toBeReturned;
}

// loading a square matrix saved by freefem++
/* load matrix from freefem++
  - format I J VAL
  - rows are in increasing order
*/
mat loadFreefemMatrix(string fileName, MPI_Comm theComm){
    ifstream inputFile;
    inputFile.open(fileName.c_str());
    char output[128];

    unsigned int iThRow;
    unsigned int jThCol;
    double val;

    vector<unsigned int> I;
    vector<unsigned int> J;
    vector<double> values;

    if (inputFile.is_open()) {
      cout << "Reading..." << endl;
      unsigned int cc = 0;
      while (!inputFile.eof()) {
        inputFile >> output;
        stringstream str;
        str << output;

        if(cc%3==0){
          str >> iThRow;
          I.push_back(iThRow);
        }
        else if(cc%3==1){
          str >> jThCol;
          J.push_back(jThCol);
        }
        else if(cc%3==2){
          str >> val;
          values.push_back(val);
        }
        cc += 1;
      }
      cout << "done.\n";
    }
    else{
      puts("Unable to read file!");
      exit(1);
    }

    I.pop_back();
    unsigned int nRows = I[I.size()-1] + 1;
    mat toBeReturned(nRows, nRows, theComm);
    for(unsigned int iEl=0; iEl<I.size(); iEl++){
      toBeReturned.setMatEl(I[iEl], J[iEl], values[iEl]);
    }
    toBeReturned.finalize();

    return toBeReturned;
}

// loading a square matrix saved by freefem++
/* load matrix from freefem++
  - format I J VAL
  - rows are in increasing order
*/
mat loadFreefemCOOMatrix(string filename, MPI_Comm theComm){

  std::ifstream file(filename.c_str(), std::ifstream::in);
  if (!file) {
    std::cerr << "Error opening file: " << filename << std::endl;
    exit(0);
  }
  std::string line;
  unsigned int nRows, nCols, nVals, tmp;

  std::getline(file, line);
  std::getline(file, line);

  file >> nRows;
  file >> nCols;
  file >> nVals;
  file >> tmp;
  file >> tmp;
  file >> tmp;
  file >> tmp;

  unsigned int i = 0;
  vector<unsigned int> I(nVals), J(nVals);
  vector<double> V(nVals);
  while (!file.eof() && i<nVals) {
    file >> I[i];
    file >> J[i];
    file >> V[i];
    ++i;
  }
  if (i<nVals) {
    std::cerr << "Error reading file : " << filename << std::endl;
    exit(0);
  }
  file.close();

  mat toBeReturned(nRows, nCols, theComm);
  for (unsigned int i=0; i<nVals; ++i) {
    toBeReturned.setMatEl(I[i], J[i], V[i]);
  }
  toBeReturned.finalize();

  return toBeReturned;

    // ifstream inputFile;
    // inputFile.open(fileName.c_str());
    // char output[128];

    // unsigned int iThRow;
    // unsigned int jThCol;
    // double val;

    // unsigned int nRows;
    // unsigned int nCols;
    // unsigned int nVals;

    // vector<unsigned int> I;
    // vector<unsigned int> J;
    // vector<double> values;

    // if (inputFile.is_open()) {
    //   cout << "Reading..." << endl;

    //   unsigned int cl = 0;
    //   unsigned int cc = 0;
    //   unsigned int idof = 0;

    //   while (!inputFile.eof()) {
    //     if (cl<2) {

    //       std::string line;
    //       std::getline(inputFile, line);
    //       //cout << line << endl;
    //       cl += 1;

    //     } else if (cc<7) {
    //       switch (cc){
    //         case 0: {
    //           inputFile >> output;
    //           stringstream str;
    //           str << output;
    //           str >> nRows;
    //           cc += 1;
    //         }
    //         case 1: {
    //           inputFile >> output;
    //           stringstream str;
    //           str << output;
    //           str >> nCols;
    //           cc += 1;
    //         }
    //         case 2: {
    //           inputFile >> output;
    //           stringstream str;
    //           str << output;
    //           str >> nVals;
    //           cc += 1;
    //           I.resize(nVals);
    //           J.resize(nVals);
    //           values.resize(nVals);
    //         }
    //         default:{
    //           inputFile >> output;
    //           cc += 1;
    //         }
    //       }
    //     } else {

    //       inputFile >> output;
    //       stringstream str;
    //       str << output;


    //       //cout << cc << endl;
    //       //cout << str.str() << endl;


    //       if(cc%3==1){
    //         str >> iThRow;
    //         //cout << iThRow << endl;
    //         I[idof] = iThRow;
    //       }
    //       else if(cc%3==2){
    //         str >> jThCol;
    //         J[idof] = jThCol;
    //       }
    //       else if(cc%3==0){
    //         str >> val;
    //         values[idof] = val;
    //         idof += 1;
    //       }
    //       cc += 1;
    //     }
    //   }
    //   cout << "done.\n";
    // } else{
    //   puts("Unable to read file!");
    //   exit(1);
    // }

    // mat toBeReturned(nRows, nCols, theComm);
    // for(unsigned int iEl=0; iEl<I.size(); iEl++){
    //   toBeReturned.setMatEl(I[iEl], J[iEl], values[iEl]);
    // }
    // toBeReturned.finalize();

    // return toBeReturned;
}


// saving a matrix in freefem COO format
void saveFreefemCOOMatrix(mat A, string filename) {

  std::ofstream file(filename.c_str(), std::ofstream::out);
  if (!file) {
    std::cerr << "Error opening file: " << filename << std::endl;
    exit(0);
  }

  file << "#  HashMatrix Matrix (COO)" << std::endl;
  file << "#    n       m        nnz     half     fortran   state" << std::endl;
  file << A.nRows() << " " << A.nCols() << " " << A.nRows()*A.nCols() << " 0 0 0 0" << std::endl;

  for (unsigned int i=0; i<A.nRows(); ++i) {
    for (unsigned int j=0; j<A.nCols(); ++j) {
      file << i << " " << j << " " << setprecision(15) << scientific << A.getMatEl(i, j) << std::endl;
    }
  }

}
