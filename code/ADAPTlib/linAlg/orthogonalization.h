#ifndef orthogonalization_h
#define orthogonalization_h

#include "mat.h"
#include "vec.h"
#include "linearAlgebraOperations.h"
#include "linearSolver.h"

class qr {

private:
  vector<vec> m_Q;
  mat m_Qmat;
  mat m_R;
  bool m_has_Computed_Qmat = false;
  vector<unsigned int> m_toRetrieve;
  double m_tolQR = 1.0e-16;

public:

  qr() {}
  qr(vector<vec> V);
  qr(mat A);
  qr(const vector<vec>& V, const MPI_Comm& theComm);
  ~qr() {}

  void clear(){
    m_R.clear();
    for(unsigned int jVec=0; jVec<m_Q.size(); jVec++){
      m_Q[jVec].clear();
    }
    if(m_has_Computed_Qmat){
      m_Qmat.clear();
    }
  }

  // -- ACCESS FUNCTION: --
  vector<vec> Q() {return m_Q;}
  vec Q(unsigned int j) {return m_Q[j];}
  mat Qmat() {return m_Qmat;}
  mat R() {return m_R;}
  vector<unsigned int> toRetrieve() {return m_toRetrieve;}

};

void MGS(const vector<vec>& V, vector<vec>& Q, mat& R, const double tol, const MPI_Comm& theComm);

// Applications of QR
void ls_approx(const vec&, const vector<vec>&, vec&, double&);

#endif
