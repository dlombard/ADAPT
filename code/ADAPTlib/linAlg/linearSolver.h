// header class linearSolver.h

#ifndef linearSolver_h
#define linearSolver_h

#include "vec.h"
#include "mat.h"
#include "linearAlgebraOperations.h"




/* KSP ROUTINES */

class linearSolver{
private:
  KSP m_ksp;
  vec m_sol;

public:
  linearSolver(){};
  ~linearSolver(){};
  inline void clear(){KSPDestroy(&m_ksp);}

  /* Overloaded constructor with system solution included:
    - the field m_sol contains the solution.
  */
  linearSolver(mat A, vec& b, KSPType type = KSPGMRES){
    assert(A.comm()==b.comm());
    assert(A.nRows()==b.size());

    KSPCreate(A.comm(),&m_ksp);
    KSPSetOperators(m_ksp, A.M(), A.M()); // => use A to build preconditioner
    KSPSetType(m_ksp, type);
    KSPSetFromOptions(m_ksp);
    KSPSetUp(m_ksp);
    m_sol.init(b.size(), b.comm());
    m_sol.finalize();
    KSPSolve(m_ksp, b.x(), m_sol.x());
  }


  // generic init, Krilov method: put  KSPCG for CG method. Initial guess is zero
  void init(mat A, KSPType type = KSPGMRES){
      KSPCreate(A.comm(),&m_ksp);
      KSPSetOperators(m_ksp, A.M(), A.M()); // => use A to build preconditioner
      KSPSetType(m_ksp, type);
      KSPSetFromOptions(m_ksp);
      KSPSetUp(m_ksp);
  }

  // overloaded to take tolerances:
  void init(mat A, double relTol, double absTol, KSPType type = KSPGMRES){
      KSPCreate(A.comm(),&m_ksp);
      KSPSetOperators(m_ksp, A.M(), A.M()); // => use A to build preconditioner
      KSPSetType(m_ksp, type);
      KSPSetTolerances(m_ksp, relTol, absTol, PETSC_DEFAULT, PETSC_DEFAULT);
      KSPSetFromOptions(m_ksp);
      KSPSetUp(m_ksp);
  }

  // Solve Ax = b;
  void solve(vec& b){
      m_sol.init(b.size(), b.comm());
      m_sol.finalize();
      KSPSolve(m_ksp, b.x(), m_sol.x());
  }

  // Solve Ax = b and output is the solution:
  vec solve(vec& b, bool UPDATE_SOL){
    vec output(b.size(), b.comm());
    output.finalize();
    KSPSolve(m_ksp, b.x(), output.x());
    if(UPDATE_SOL){
      m_sol = output;
    }
    return output;
  }

  // ACCESS Function:
  inline KSP ksp(){return m_ksp;}
  inline vec sol(){return m_sol;}

};

// operator
inline vec operator / (mat A, vec b){
  linearSolver linSys;
  linSys.init(A, KSPGMRES);
  linSys.solve(b);
  linSys.clear();
  return linSys.sol();
}

// function to solve in a least squares sense:
inline vec solve_least_squares(mat A, vec b){
  linearSolver linSys;
  linSys.init(A, KSPLSQR);
  linSys.solve(b);
  linSys.clear();
  return linSys.sol();
}

// solve with direct LU
inline vec solve_LU(mat A, vec b){
  KSP ksp;
  PC pc;
  KSPCreate(A.comm(),&ksp);
  KSPSetOperators(ksp, A.M(), A.M());
  KSPSetType(ksp,KSPPREONLY);
  KSPGetPC(ksp,&pc);
  PCSetType(pc,PCLU);
  KSPSetFromOptions(ksp);
  KSPSetUp(ksp);
  
  vec output(b.size(), b.comm());
  output.finalize();
  KSPSolve(ksp, b.x(), output.x());

  return output;
}


// end of file:
#endif
