// Parallel tensor implementation
#include "genericInclude.h"
#include "linAlg/linAlg.h"
#include "CP/CP.h"
#include "fullTensor/fullTens.h"
#include "TensorTrain/SoTT.h"
#include "compression/CPTensorCompression.hpp"
#include "Applications/Network/Network.h"
#include "Applications/Network/locHOSVD.h"
#include <string>

using namespace std;


/* Linspace function: */
vector<double> linspace(double start, double end, int num){

  vector<double> linspaced(num);


  if (num == 0) { return linspaced; }
  if (num == 1) {
      linspaced.push_back(start);
      return linspaced;
    }

  double delta = (end - start) / (num - 1.0);

  for(unsigned int i=0; i < num; ++i){
    linspaced[i] = start + delta * i;
  }
  //linspaced.push_back(end);  I want to ensure that start and end are exactly the same as the input
  return linspaced;
}


void saveField_2d_txt(vec& sol, unsigned int Nx, unsigned int Ny, string& fName){
 string saveName = fName + ".txt";
 unsigned int nx = Nx;
 unsigned int ny = Ny;
 unsigned int nz = 1;
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

  for(unsigned int iDof=0; iDof<sol.size(); iDof++){
     double value = sol(iDof);
     if(fabs(value) < 1.0e-9){value = 0.0;}
     outfile << value << endl;
  }
  outfile.close();
}


// Simple test on vectors

int main(int argc, char **args){

  static char help[] = "Testing\n\n";

  SlepcInitialize(&argc,&args,(char*)0,help);

  if(argc>1){cout << "Performing test: " << args[1] << endl<< endl;}

  // Number of procs, and the ranks
  int nOfProcs;
  MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
  int idProc;
  MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

  // Use the class vec
  vector<double> nt = {400};//linspace(1000.0, 1000.0, 1);

  vector<double> x = linspace(0.0, 1.0, 500);

  vector<vector<double>> t(nt.size());
  for (unsigned int iT = 0; iT < nt.size(); iT++) {
    t[iT]= linspace(0.0, 0.5, nt[iT]);

    vector<vector<double>*> u(t[iT].size());
    double normData = 0.0;
    for (unsigned int j = 0; j < t[iT].size(); j++) {
      u[j] = new vector<double>(x.size());
      for (unsigned int i = 0; i < x.size(); i++) {
        double val = exp(- 500 * (x[i] - 0.25 - t[iT][j]) * (x[i] - 0.25 - t[iT][j]));
        (*u[j])[i] = val;
        normData += val * val;
      }
    }
    normData = sqrt(normData);



    ofstream outfile;
    string solName = "Fexact_" +to_string(t[iT].size()) +".txt";
    outfile.open(solName.c_str());
    for (unsigned int j = 0; j < t[iT].size(); j++) {
      for (size_t i = 0; i < x.size(); i++) {
        outfile << (*u[j])[i] << flush << endl;
      }
    }
    outfile.close();

    //To extract the information in t:
    vector<vector<double>*> u_t(x.size());
    for (unsigned int i = 0; i < x.size(); i++) {
      u_t[i] = new vector<double>(t[iT].size());
      for (unsigned int j = 0; j < t[iT].size(); j++) {
        double val= exp(- 500 * (x[i] - 0.25 - t[iT][j]) * (x[i] - 0.25 - t[iT][j]));
        (*u_t[i])[j] = val;
      }
    }

    cout<< "Changed the format of our fibers base" << endl;
    cout << "the number of columns of u = " << u.size() << " And of u_t = " << u_t.size() << endl;
    cout << "the number of rows of u = " << u[0]->size() << " And of u_t = " << u_t[0]->size() << endl;


//Clustering method

    vector<mat> Basis;
    vector<vector<unsigned int>> indOnBasis;
    vector<double> locErrBudget;
    vector<double> memory;
    double globalBudget;
    Network net;
    string x_name = "x";
    double error = 5.0e-2 * normData; // * sqrt(normalization);

    //Obtaining the basis with respect to the x
    net.lowMem_ComputeBases(u, error, Basis, indOnBasis, locErrBudget, globalBudget);
    cout << "Basis size " << Basis.size() << endl;
    unsigned int nfiles_x = indOnBasis.size();
    if (Basis.size() > 1) {
      net.lowMem_Merge2dBases(x_name, u, Basis, indOnBasis, locErrBudget, globalBudget, memory);
      cout << "Basis merged" << endl;
      cout << memory.size() << endl;
    }

    /*
    //Save the ind on basis in a vector
    string outFileName = string("indOnBasis_x.txt") ;
    net.save_indOnBasis(indOnBasis, outFileName);
    cout << "iob saved" << endl;


    //Load the ind on basis
    unsigned int iob0;
    vector<unsigned int> iobList;
    vector<vector<unsigned int>> inds;
    net.load_indOnBasis(outFileName, inds, iobList, iob0);

    cout << "number of clusters " << iob0 << endl;
    cout << "of sizes "<< endl;
    for (size_t i = 0; i < iobList.size(); i++) {
      cout << iobList[i] << endl;
    }

    cout << "and of elements: " << endl;
    for (size_t iInd = 0; iInd < iob0; iInd++) {
      for (size_t iEl = 0; iEl < iobList[iInd]; iEl++) {
        cout << inds[iInd][iEl] << endl;
      }
    }*/


    /*
    string outFileName = string("Applications/Network/memory_x_")+ to_string(t.size())+string("_") + string(".txt") ;
    outfile.open(outFileName.c_str());
    for (size_t iBas = 0; iBas < memory.size(); iBas++) {
      outfile << memory[iBas] << flush << endl;
    }
    outfile.close();*/



    vector<mat> Basis_t;
    vector<vector<unsigned int>> indOnBasis_t;
    vector<double> locErrBudget_t;
    vector<double> memory_t;
    double globalBudget_t;
    string t_name = "t";

    //Obtaining the basis with respect to the t
    net.lowMem_ComputeBases(u_t, error, Basis_t, indOnBasis_t, locErrBudget_t, globalBudget_t);
    cout << "Basis_t computed and its size is " << Basis_t.size() << endl;
    unsigned int nfiles_t = indOnBasis_t.size();
    if (Basis_t.size() > 1) {
      net.lowMem_Merge2dBases(t_name, u_t, Basis_t, indOnBasis_t, locErrBudget_t, globalBudget_t, memory_t);
      cout << "Basis_t merged "<< endl;
    }
    cout << "number of files in t = " << nfiles_t << endl;
    cout << "number of files in x = " << nfiles_x << endl;


    cout << "Number of subdomains in x = " << indOnBasis_t.size() << endl;
    cout << "Number of subdomains in t = " << indOnBasis.size() << endl;

    indOnBasis.clear();
    indOnBasis_t.clear();

    /*
    outFileName = string("Applications/Network/memory_t_")+ to_string(t.size())+string("_") + string(".txt") ;
    outfile.open(outFileName.c_str());
    for (size_t iBas = 0; iBas < memory_t.size(); iBas++) {
      outfile << memory_t[iBas] << flush << endl;
    }
    outfile.close();*/

    unsigned int total_memory = 0;
    unsigned int total_memory_t = 0;

    /*
    //Computing basis and core for the HOSVD:
    mat Ux=lowMem_computeHOSVDbasis(u, error * error);
    mat Ut=lowMem_computeHOSVDbasis(u_t, error * error);

    vector<mat> basisHOSVD(2);
    basisHOSVD[0] = Ux;
    basisHOSVD[1] = Ut;

    mat core = lowMem_computeHOSVDcore(u, basisHOSVD);
    cout << "HOSVD core computed" << endl;
    unsigned int memory_pod = Ux.nCols() * (x.size() + t[iT].size());
    cout << "memory pod = " << memory_pod << endl;
    double compression_ratio_POD = (memory_pod * 1.0)/(x.size()*t[iT].size());

    double error_sum_HOSVD = 0.0;
    double error_sum_localHOSVD = 0.0;*/

    //unsigned int memory_locpod = 0;
    unsigned int best_memory = 1.0e8;
    vector<unsigned int> best_pair_clusters(2);

    double compression_ratio_locPOD = 0.0;


    for (size_t ix = 0; ix < nfiles_x; ix++) {
      unsigned int iob0_x;
      vector<unsigned int> iobList_x;
      vector<vector<unsigned int>> indOnBasis;
      string outFileName_x = string("indOnBasis_x_") + to_string(ix) + (".txt") ;
      net.load_indOnBasis(outFileName_x, indOnBasis, iobList_x, iob0_x);
      //cout << "x loaded " << endl;

      for (size_t it = 0; it < nfiles_t; it++) {
        unsigned int memory_locpod = 0;
        //cout << "ix = " << ix << " it = " << it << endl;
        unsigned int iob0_t;
        vector<unsigned int> iobList_t;
        vector<vector<unsigned int>> indOnBasis_t;
        string outFileName_t = string("indOnBasis_t_") + to_string(it) + (".txt") ;
        net.load_indOnBasis(outFileName_t, indOnBasis_t, iobList_t, iob0_t);
        //cout << "t loaded " << endl;

        cout << indOnBasis.size() << endl;
        cout << indOnBasis_t.size() << endl;

        //vector<vector<mat>> localCoresVec(indOnBasis.size());
        //vector<vector<vector<mat>>> locBasisHOSVDVec(indOnBasis.size());
        vector<vector<bool>> isItNon0(indOnBasis.size());

        for (unsigned int iBase = 0; iBase < indOnBasis.size(); iBase++) {
          //localCoresVec[iBase].resize(indOnBasis_t.size());
          //locBasisHOSVDVec[iBase].resize(indOnBasis_t.size());
          isItNon0[iBase].resize(indOnBasis_t.size());
          for (unsigned int jBase = 0; jBase < indOnBasis_t.size(); jBase++) {

              vector<vec> theFibers(indOnBasis[iBase].size());
              //vector<vec> theFibers_t(indOnBasis_t[jBase].size());

              //cout << "iBase " << iBase << "and jBase " << jBase << endl;
              //cout << theFibers.size() << endl;
              //cout << theFibers_t.size() << endl;

              for (unsigned int iFib = 0; iFib < indOnBasis[iBase].size(); iFib++) {
                //vec insideVec(indOnBasis_t[jBase].size(), PETSC_COMM_WORLD);
                theFibers[iFib].init(indOnBasis_t[jBase].size(), PETSC_COMM_WORLD);
                for (unsigned int jFib = 0; jFib < indOnBasis_t[jBase].size(); jFib++) {
                  //cout << "the index in t " << indOnBasis_t[jBase][jFib] << endl;
                  //cout << "the index in x " << indOnBasis[iBase][iFib] << endl;
                  double val = (*u[indOnBasis[iBase][iFib]])[indOnBasis_t[jBase][jFib]];

                  //insideVec(jFib) = val;
                  theFibers[iFib].setVecEl(jFib, val);
                }
                //theFibers[iFib] = insideVec;
                theFibers[iFib].finalize();
              }
              //cout << "the fibers computed" << endl;

              /*
              for (unsigned int jFib = 0; jFib < indOnBasis_t[jBase].size(); jFib++) {
                //vec insideVec_t(indOnBasis[iBase].size(), PETSC_COMM_WORLD);
                theFibers_t[jFib].init(indOnBasis[iBase].size(), PETSC_COMM_WORLD);
                for (unsigned int iFib = 0; iFib < indOnBasis[iBase].size(); iFib++) {
                  //cout << "the index in t " << indOnBasis_t[jBase][jFib] << endl;
                  //cout << "the index in x " << indOnBasis[iBase][iFib] << endl;
                  double val = (*u_t[indOnBasis_t[jBase][jFib]])[indOnBasis[iBase][iFib]];

                  //insideVec_t(iFib) = val;
                  theFibers_t[jFib].setVecEl(iFib, val);
                }
                //theFibers_t[jFib] = insideVec_t;
                theFibers_t[jFib].finalize();
              }*/
              //cout << "computed fibers" << endl;
              double l2 = 0.0;
              for (unsigned int iFib = 0; iFib < theFibers.size(); iFib++) {
                for (unsigned int jFib = 0; jFib < theFibers[0].size(); jFib++) {
                  double val = theFibers[iFib](jFib)*theFibers[iFib](jFib);
                  l2 += val;
                }
              }

              //cout << "l2 computed = " << l2 << endl;
              if (l2 > (error * error)/(indOnBasis.size() * indOnBasis_t.size())) {

                isItNon0[iBase][jBase] = true;
                unsigned int rank = estimate_basisRank(theFibers, (error * error)/(indOnBasis.size() * indOnBasis_t.size()));
                //mat Uxi=computeHOSVDbasis(theFibers, error * error/(indOnBasis.size() * indOnBasis_t.size()));
                //cout << "basis x computed " << endl;

                //mat Utj=computeHOSVDbasis(theFibers_t, error * error/(indOnBasis.size() * indOnBasis_t.size()));
                //cout << "basis t computed " << endl;
                /*locBasisHOSVDVec[iBase][jBase].resize(2);
                locBasisHOSVDVec[iBase][jBase][0]=Uxi;
                locBasisHOSVDVec[iBase][jBase][1]=Utj;*/

                //mat lCore = computeHOSVDcore(theFibers, locBasisHOSVDVec[iBase][jBase]);

                //localCoresVec[iBase][jBase]=lCore;

                //memory_locpod += Uxi.nCols() * (theFibers.size() + theFibers_t.size());
                memory_locpod += rank * (theFibers.size() + theFibers[0].size());


                //Uxi.clear();
                //Utj.clear();
                //cout << "clear matrices done" << endl;

              } else{isItNon0[iBase][jBase] = false;}

              //cout << "indOnBasis = " << indOnBasis.size() << endl;
              //cout << "indOnBasis t = " << indOnBasis_t.size() << endl;

              for (size_t iFib = 0; iFib < indOnBasis[iBase].size(); iFib++) {
                theFibers[iFib].clear();
              }
            //  for (size_t iFib = 0; iFib < indOnBasis_t[jBase].size(); iFib++) {
              //  theFibers_t[iFib].clear();
              //}
              theFibers.clear();
            //  theFibers_t.clear();

              //cout << "clear fibs done" << endl;

          }
        }
        cout << "ix = " << ix << " and it = " << it << "and its memory = " << memory_locpod << endl;
        cout << endl;

        if (memory_locpod < best_memory) {
          best_memory = memory_locpod;
          best_pair_clusters[0] = ix;
          best_pair_clusters[1]= it;
        }
        indOnBasis_t.clear();
      }
      indOnBasis.clear();
    }
    cout << "the best memory = " << best_memory << " and the best pair " << best_pair_clusters[0] << " and " << best_pair_clusters[1] << endl;
    /*
    cout << "Entering in the reconstruction part" << endl;
    vector<vec> u_rec_HOSVD(t[iT].size());
    vector<vec> u_rec_localHOSVD(t[iT].size());


    vector<unsigned int> theGrid(2);

    unsigned int position = 0;
    unsigned int position_j = 0;


    for (unsigned int jGrid = 0; jGrid < t[iT].size(); jGrid++) {
      vec u_j_rec_HOSVD(x.size(), PETSC_COMM_WORLD);
      vec u_j_rec_localHOSVD(x.size(), PETSC_COMM_WORLD);
      for (unsigned int iGrid = 0; iGrid < x.size(); iGrid++) {

        theGrid[0]=iGrid;
        theGrid[1]=jGrid;

        double trueval = exp(- 500 * (x[iGrid] - 0.25 - t[iT][jGrid]) * (x[iGrid] - 0.25 - t[iT][jGrid]));

        //Reconstruction of the function with HOSVD
        double recVal_HOSVD = reconstructionHOSVD(core, basisHOSVD, theGrid);
        u_j_rec_HOSVD(iGrid)= recVal_HOSVD;

        double the_error_HOSVD = trueval - recVal_HOSVD;
        error_sum_HOSVD += the_error_HOSVD * the_error_HOSVD;


        //Reconstruction of the function with local HOSVD
        //The basis
        unsigned int i = findBasis(Basis, indOnBasis, jGrid);
        unsigned int j = findBasis(Basis_t, indOnBasis_t, iGrid);

        //The mapping
        unsigned int ind = 0;
        for (unsigned int iInd = 0; iInd < indOnBasis[i].size(); iInd++) {
          if(indOnBasis[i][iInd] == jGrid){
            ind = iInd;
            break;
          }
        }
        position = indOnBasis[i][ind];


        unsigned int ind_t = 0;
        for (unsigned int jInd = 0; jInd < indOnBasis_t[j].size(); jInd++) {
          if(indOnBasis_t[j][jInd] == iGrid){
            ind_t = jInd;
            break;
          }
        }
        position_j = indOnBasis_t[j][ind_t];

        vector<unsigned int> theLocalGrid(2);
        theLocalGrid[0] = ind_t;
        theLocalGrid[1] = ind;


        if(isItNon0[i][j] == true){
          double recVal_localHOSVD = reconstructionHOSVD(localCoresVec[i][j], locBasisHOSVDVec[i][j], theLocalGrid);
          //cout<< "reconstructed function on the local domain" << endl;

          u_j_rec_localHOSVD(iGrid)= recVal_localHOSVD;
          double the_error_localHOSVD = trueval - recVal_localHOSVD;
          error_sum_localHOSVD += the_error_localHOSVD * the_error_localHOSVD;
        } else{
          //cout << "zero function" << endl;
          u_j_rec_localHOSVD(iGrid)= 0.0;
          double the_error_localHOSVD = trueval;
        }
      }
      u_j_rec_HOSVD.finalize();
      u_j_rec_localHOSVD.finalize();

      u_rec_HOSVD[jGrid].copyVecFrom(u_j_rec_HOSVD);
      u_rec_localHOSVD[jGrid].copyVecFrom(u_j_rec_localHOSVD);

      u_j_rec_HOSVD.clear();
      u_j_rec_localHOSVD.clear();
    }


    compression_ratio_locPOD = (memory_locpod * 1.0)/(x.size() * t[iT].size());

    error_sum_HOSVD = sqrt(error_sum_HOSVD);
    error_sum_HOSVD *= 1/(sqrt(x.size() * t[iT].size()));
    cout << "the global error of the HOSVD = " << error_sum_HOSVD << endl;

    error_sum_localHOSVD = sqrt(error_sum_localHOSVD);
    error_sum_localHOSVD *= 1/(sqrt(x.size() * t[iT].size()));
    cout << "the global error of the new alternative for the local HOSVD = " << error_sum_localHOSVD << endl;



    string recName = "FrecHOSVD_" + to_string(t[iT].size()) +".txt";
    outfile.open(recName.c_str());
    for (unsigned int j = 0; j < t[iT].size(); j++) {
      for (size_t i = 0; i < x.size(); i++) {
        outfile << u_rec_HOSVD[j](i) << flush << endl;
      }
    }
    outfile.close();

    recName = "FrecLocHOSVD_" + to_string(t[iT].size()) +".txt";
    outfile.open(recName.c_str());
    for (unsigned int j = 0; j < t[iT].size(); j++) {
      for (size_t i = 0; i < x.size(); i++) {
        outfile << u_rec_localHOSVD[j](i) << flush << endl;
      }
    }
    outfile.close();*/

  }
  SlepcFinalize();
  return 0;
}
