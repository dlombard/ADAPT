// Header for conversions

#ifndef conversions_hpp
#define conversions_hpp

#include "../linAlg/vec.h"
#include "../linAlg/mat.h"
#include "../CP/CPTensor.h"
#include "../fullTensor/fullTensor.h"
#include "../sparseTensor/sparseTensor.h"
#include "../TensorTrain/TensorTrain.h"
#include "../Tucker/Tucker.h"

using namespace std;


// Functions that perform the conversions between several formats //


/* I -- CONVERSIONS to CP format -- */

CPTensor fromTuckerToCP(Tucker&, bool);
CPTensor fromTensorTrainToCP(TensorTrain&, bool);

/* II -- CONVERSIONS to Tucker format -- */
Tucker fromCPToTucker(CPTensor&, bool);


#endif
