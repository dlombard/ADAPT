// Implementation of conversions

#include "conversions.hpp"




/* I -- CONVERSIONS to CP format -- */


/* conversion from Tucker to CP
  - input: a Tucker tensor
  - output: the corresponding CP
*/
CPTensor fromTuckerToCP(Tucker& input, bool COPY = true){
  CPTensor output(input.nDof_var(), input.comm());
  const unsigned int CP_rank = input.core().nEntries();
  output.set_rank(CP_rank);
  for(unsigned int iTerm=0; iTerm<CP_rank; iTerm++){
    vector<unsigned int> indCore = input.core().lin2sub(iTerm);
    double coeff = input.core().tensorEntries(iTerm);
    output.set_coeffs(iTerm, coeff);
    for(unsigned int iVar=0; iVar<input.nVar(); iVar++){
      unsigned int iMod = indCore[iVar];
      vec theMode = input.modes(iVar, iMod);
      if(!COPY){
        output.set_terms(iTerm, iVar, theMode);
      }
      else{
        vec toInsert;
        toInsert.copyVecFrom(theMode);
        output.set_terms(iTerm, iVar, toInsert);
      }
    }
  }
  return output;
};


/* conversion from TensorTrain to CP
  - input: a Tensor Train tensor
  - output: the corresponding CP
*/
CPTensor fromTensorTrainToCP(TensorTrain& input, bool COPY = true){
  CPTensor output(input.nDof_var(), input.comm());
  unsigned int CP_rank = 1;
  for(unsigned int iVar=0; iVar<input.nVar(); iVar++){
    CP_rank *= (input.trainSize(iVar,0) * input.trainSize(iVar,1));
  }
  output.set_rank(CP_rank);

  unsigned int cc = 0;
  for(unsigned int iVar=0; iVar<input.nVar(); iVar++){
    for(unsigned int i=0; i<input.trainSize(iVar,0); i++){
      for(unsigned int j=0; j<input.trainSize(iVar,1); j++){
        vec tt_fiber = input.train(iVar, i, j);
        if(!COPY){
          output.set_terms(cc, iVar, tt_fiber);
        }
        else{
          vec fiberCopy;
          fiberCopy.copyVecFrom(tt_fiber);
          output.set_terms(cc, iVar, fiberCopy);
        }
        output.set_coeffs(cc, 1.0);
        cc += 1;
      }
    }
  }
  return output;
}



/* II -- CONVERSIONS to Tucker format -- */


/* conversion from CP to Tucker (trivial conversion without rounding)
  - input: a CP tensor
  - output: the corresponding Tucker
*/
Tucker fromCPToTucker(CPTensor& input, bool COPY = true){
  Tucker output(input.nDof_var(), input.comm());
  vector<unsigned int> tucker_ranks(input.nVar());
  for(unsigned int iVar=0; iVar<input.nVar(); iVar++){
    tucker_ranks[iVar] = input.rank();
  }
  output.set_ranks(tucker_ranks);

  for(unsigned int iTerm=0; iTerm<input.rank(); iTerm++){
    for(unsigned int iVar=0; iVar<input.nVar(); iVar++){
      vec CP_fiber = input.terms(iTerm, iVar);
      if(!COPY){
        output.set_modes(iVar, iTerm, CP_fiber);
      }
      else{
        vec fiberCopy;
        fiberCopy.copyVecFrom(CP_fiber);
        output.set_modes(iVar, iTerm, CP_fiber);
      }
    }
    // setting the diagonal core entries:
    vector<unsigned int> coreInd(input.nVar());
    for(unsigned int iVar=0; iVar<input.nVar(); iVar++){
      coreInd[iVar] = iTerm;
    }
    output.core().set_tensorElement(coreInd, input.coeffs(iTerm));
  }
  output.core().finalize();

  return output;
}
