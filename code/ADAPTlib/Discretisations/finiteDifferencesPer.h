// Header of the mother class tensor:
#ifndef finiteDifferencesPer_h
#define finiteDifferencesPer_h

#include "../genericInclude.h"
#include "../linAlg/linAlg.h"
#include "../tensor.h"

using namespace std;

class finiteDifferencesPer{
private:
  tensor m_struct;
  vector<double> m_bounds;
  unsigned int m_nDof;
  vector<double> m_dx;
  mat m_mass;

public:
  finiteDifferencesPer(){};
  ~finiteDifferencesPer(){};
  finiteDifferencesPer(unsigned int, vector<unsigned int>, MPI_Comm);
  finiteDifferencesPer(unsigned int, vector<unsigned int>, vector<double>, MPI_Comm);

  // Functions for computation:
  void compute_mass();
  vector<double> compute_point(vector<unsigned int>);
  vector<unsigned int> compute_ll(vector<double>);
  vector<vec> compute_gradient(vec&);
 // bool isItOnBoundary(unsigned int);
 // tuple<unsigned int, unsigned int> whichBoundaryIsOn(unsigned int);
  unsigned int linIndIncrement(unsigned int, vector<int>);
  unsigned int linIndIncrementPer(unsigned int, vector<int>);
  //bool isInBox(unsigned int);


  // ACCESS FUNCTIONS:
  inline tensor struc(){return m_struct;}
  inline unsigned int dim(){return m_struct.nVar();}
  inline vector<unsigned int> dofPerDim(){return m_struct.nDof_var();}
  inline unsigned int nDof(){return m_nDof;}
  inline vector<double> dx(){return m_dx;}
  inline double dx(int iVar){return m_dx[iVar];}
  inline mat mass(){return m_mass;}
  inline vector<double> bounds(){return m_bounds;}
  inline double bounds(unsigned int iB){return m_bounds[iB];}

  // Print function:
  inline void print(){
    m_struct.print();
    PetscPrintf(m_struct.comm(),"Total number of dofs: %d\n", m_nDof);
    PetscPrintf(m_struct.comm(),"SpaceBox:\n");
    for(unsigned int iVar=0; iVar<m_struct.nVar(); iVar++){
      PetscPrintf(m_struct.comm(),"[%f , %f]\n",m_bounds[2*iVar], m_bounds[2*iVar+1]);
    }
  }

};

#endif
