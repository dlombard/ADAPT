// class Full tensor, daughter of tensor, header file.
#ifndef fullTensor_h
#define fullTensor_h

#include "tensor.h"

using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:

class fullTensor : public tensor{

private:
	unsigned int m_nEntries;
	Vec m_tensorEntries;
	//vector<unsigned int> m_inc; // auxiliary vector for indexing
	bool m_isInitialised;

	public:

	// -- CONSTRUCTORS and DESTRUCTOR --
	fullTensor(){m_isInitialised=false;};
	~fullTensor(){};

	// overloaded to initialise an empty tensor
	fullTensor(unsigned int,vector<unsigned int>&, MPI_Comm);
        fullTensor(const int&, const vector<unsigned int>&, const vector< vector<unsigned int> >&, const Vec&, MPI_Comm);


	// - initialise fields:
	void init();


	// -- ACCESS FUNCTIONS --
	inline bool isInitialised(){return m_isInitialised;};
	inline unsigned int nEntries(){return m_nEntries;};
	inline Vec tensorEntries(){return m_tensorEntries;};
	inline double tensorEntries(unsigned int linInd){return a.getVecEl(m_tensorEntries, linInd, m_theComm);};
	inline double operator[] (vector<unsigned int> ind){
		unsigned int linInd = sub2lin(ind);
		return a.getVecEl(m_tensorEntries, linInd, m_theComm);
	}


	// -- METHODS --

	

  // 2 - PRINTING the tensor structure:
	void printTensorStruct();



	// SETTERS:
	// one the insert of the elements is done, finilise the Vec of entries
	inline void finalizeTensor(){
		a.finalizeVec(m_tensorEntries);
	}
	// set an element given the vector of indices i,j,k,...
	inline void setTensorElement(vector<unsigned int> indices, double value){
	  int linInd = sub2lin(indices);
	  a.setVecEl(m_tensorEntries, linInd, value, m_theComm);
	}

	//set an element given a linear index
	inline void setTensorElement(unsigned int linInd, double value){
		assert(linInd<m_nEntries);
		a.setVecEl(m_tensorEntries, linInd, value, m_theComm);
	}

  // set the whole data stream:
	inline void setTensorEntries(Vec& theEntries){
		m_tensorEntries = theEntries;
	}
  // potentially change the initialisation flag (useful in copies).
	inline void setIsInitialised(bool state){
		m_isInitialised = state;
	}


	// COPY Tensor Structure:
	void copyTensorStructFrom(fullTensor&);
	void copyTensorStructTo(fullTensor&);

	// COPY the full tensor:
	void copyTensorFrom(fullTensor&);
	void copyTensorTo(fullTensor&);


  // OPERATIONS on full tensor
	bool hasSameStructure(fullTensor&);
	bool isEqualTo(fullTensor&);
	void sum(fullTensor&,double);
	void multiplyByScalar(double);
	void shiftByScalar(double);
	void extractSubtensor(vector<unsigned int>&, fullTensor&);
	void assignSubtensor(vector<unsigned int>&, fullTensor&);
	void reshape(vector<unsigned int>&, fullTensor&, bool);
	void leftKronecker(fullTensor&, fullTensor&);
	void rightKronecker(fullTensor&, fullTensor&);
	void implicitLeftKronecker(fullTensor&, vector<unsigned int>&, vector<unsigned int>&, vector<unsigned int>&, double&);
	void implicitRightKronecker(fullTensor&, vector<unsigned int>&, vector<unsigned int>&, vector<unsigned int>&, double&);
	void modeIKhatriRao(unsigned int, fullTensor&, fullTensor&);
	void modeIConcatenate(unsigned int, fullTensor&, fullTensor&);
	void modeIContraction(unsigned int, Vec&, fullTensor&);
	void modeIMatMult(unsigned int, Mat&, fullTensor&);
	void outerProduct(fullTensor&,fullTensor&);

	// - Compute unfolding -
	void computeUnfolding(const unsigned int, Mat&, int&);
	void computeUnfolding(const unsigned int, vector<Vec>&);
	void fromUnfold2full(const unsigned int, vector<unsigned int>&, Mat&, MPI_Comm);
	void evalUnfolding(const unsigned int,const unsigned int,const unsigned int,double&);

	//-- Evaluation of the tensor entries for a full tensor --
	void eval(const vector<unsigned int>& ind, double& val){
		unsigned int lin = sub2lin(ind);
		val = a.getVecEl(m_tensorEntries,lin, m_theComm);
	};

};


#endif
