// Created by: Damiano & Virginie on ...
#include "SubTensor.h"

using namespace std;


// -- Constructors --

// GenTensor
SubTensor::SubTensor(Node* node, tensor* tensor){
	node_ = node;
	tensor_ = tensor;

        m_theComm = tensor->theComm();
	indices_ = node->getIndicesFromLeafs();

	m_nVar = tensor->nVar();

	vector<unsigned int> vec_dim(m_nVar);
	for (int j = 0; j<m_nVar; j++)
	{
		vec_dim[j] = indices_[j].size();
	}
	m_nDof_var = vec_dim;
	compute_minc(); 
}

//-- Evaluation of the tensor --
void SubTensor::eval(const vector<unsigned int>& vecind, double& res)
{ 
	vector<unsigned int> true_ind(m_nVar); 
	for (int j=0; j<m_nVar; j++)
	{
		true_ind[j] = indices_[j][vecind[j]];
	}
	tensor_->eval(true_ind, res);
}

