// header file for CP format

#ifndef CPTensor_h
#define CPTensor_h

#include "tensor.h"
#include "fullTensor.h"

using namespace std;

class CPTensor : public tensor{
private:
  vector<vector<Vec> > m_terms;
  vector<double> m_coeffs;
  unsigned int m_rank;
  bool m_isInitialised;
  string m_CPName;
public:

  // Constructor and Destructor
  CPTensor(){};
  CPTensor(unsigned int, vector<unsigned int>, MPI_Comm);
  CPTensor(vector<vector<Vec> >&, vector<double>&, MPI_Comm);
  CPTensor(vector<vector<Vec> >&, MPI_Comm);
  CPTensor(vector<Vec>&, double, MPI_Comm);
  CPTensor(vector<Vec>&, MPI_Comm);
  ~CPTensor(){};
  void clear();


  // ACCESS FUNCTIONS:
  inline vector<vector<Vec> > terms(){return m_terms;};
  inline vector<Vec> terms(unsigned int iTerm){
    assert(iTerm<m_rank);
    return m_terms[iTerm];
  };
  inline Vec terms(unsigned int iTerm, unsigned int idVar){
    assert(iTerm<m_rank);
    assert(idVar<m_nVar);
    return (m_terms[iTerm])[idVar];
  };
  inline unsigned int rank(){return m_rank;};
  inline vector<double> coeffs(){return m_coeffs;};
  inline double coeffs(unsigned int iTerm){return m_coeffs[iTerm];};
  inline string CPName(){return m_CPName;};


  // SETTERS:
  inline void set_rank(unsigned int theRank){m_rank = theRank;};
  inline void set_terms(vector<vector<Vec> >& theTerms){
    m_terms = theTerms;
  }
  // if already initialised!
  inline void set_terms(unsigned int iTerm, vector<Vec>& iThPureTensProd){
    assert(iTerm<m_rank);
    m_terms[iTerm] = iThPureTensProd;
  }
  inline void set_terms(unsigned int iTerm, unsigned int idVar, Vec& theTerm){
    assert(iTerm<m_rank);
    assert(idVar<m_nVar);
    (m_terms[iTerm])[idVar] = theTerm;
  }
  inline void set_coeffs(vector<double>& theCoeffs){m_coeffs = theCoeffs;};
  inline void set_coeffs(unsigned int iTerm, double val){m_coeffs[iTerm] = val;};
  inline void set_CPName(string& name){m_CPName = name;};
  inline void setIsInitialised(bool state){
		m_isInitialised = state;
	}


  // COPY Tensor Structure:
	void copyTensorStructFrom(CPTensor&);
	void copyTensorStructTo(CPTensor&);

	// COPY the full tensor:
	void copyTensorFrom(CPTensor&);
	void copyTensorTo(CPTensor&);


  // OPERATIONS:
  bool hasSameStructure(CPTensor&);
  void multiplyByScalar(double);
  double scalarProdTerms(vector<Vec>&, vector<Vec>&, vector<Mat>&);
  double scalarProdTerms(vector<Vec>&, vector<Vec>&);
  double scalarProd(CPTensor&);
  double norm2CP(vector<Mat>&);
  double norm2CP();
  void rescaleTerm(unsigned int, unsigned int, double);

  void compute_CPTT_Term(fullTensor&, vector<Vec>&, double&);

  void compute_CPTT_Term(CPTensor&, vector<Vec>&, double&);
  void compute_Unfolding_OneSvdTerm(unsigned int, double&, Vec&);
  void addPureTensorTerm(vector<Vec>&, double);
  void sum(CPTensor&, double, bool);
  void modeIContraction(unsigned int, Vec&, CPTensor&);
  void inplaceModeIContraction(unsigned int, Vec&);
  void contractEverythingExceptI(unsigned int, vector<Vec>&, Vec&);

  void greedy_ApproxOf(fullTensor&);
  void greedy_ApproxOf(CPTensor&, vector<Mat>&, const double, const double);
  void greedy_ApproxOf(CPTensor&, vector<Mat>&, const double, const double, const double);

  void CPTT_ApproxOf(fullTensor&, double);
  void CPTT_ApproxOf(CPTensor&, double, bool);



  // Eval function and compute unfolding
  void computeUnfolding(const unsigned int, Mat&, int&);
	void computeUnfolding(const unsigned int, vector<Vec>&);
  void eval(const vector<unsigned int>& ind, double& val);



};

#endif
