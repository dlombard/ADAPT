/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/

#ifndef HPFCPTensor_h
#define HPFCPTensor_h

#include "genericInclude.h"
#include "auxiliary.h"
#include "tensor.h"
#include "CPTensor.h"
#include "Node.h"
#include "ApproxTensor.h"



using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:


class HPFCPTensor: public tensor{

	private:

        Node* m_node;
        vector<CPTensor> m_CPvec;

	public: 

        HPFCPTensor(){};
  	~HPFCPTensor(){};

      	//Overloaded constructors
	HPFCPTensor(Node*, const vector<CPTensor>&, MPI_Comm);
	HPFCPTensor(ApproxTensor*, bool);

	inline Node* getNode(){return m_node; }
	inline vector<CPTensor> getTens(){return m_CPvec;}

	void eval(const vector<unsigned int>& vec_ind, double& res);
	CPTensor CP_From_HPFCP();

};


#endif



