/*
    CLASS PARAMETERS: header class to read param.dat file
*/

#include <string>
#include <iostream>
#include <map>
#include "ToolLib/getpot-c++/GetPot"
#include "spaceDisc.h"
#include "paramDisc.h"

using namespace std;


class codeData {

private:
    /*SPACE VARIABLES:*/

    // specific to FD:

    // specific to FE:

    /* PARAMETERS VARIABLES: */

    /* TIME */

    /* SOLVERS TYPE and TOLERANCES: */
    string m_linSolverType;
    map <string,int> linSolveT;
    double m_tol;
    double m_svd;

    /* I/O VARIABLES */
    string m_dirToSave;
    string m_dataType;
    map <string,int> dataT;

public:
    codeData(){};
    ~codeData(){};


    // METHODS:

    // read from file:
    void read(char* );


    // MAP DEFINITIONS:

    // define space disc map;

    // define space disc Boundary Conditions:


    // Define conversion of m_vPar and m_mPar into vector<Eigen:: >


    // define Linear Solver map;
    int useMapLinearSolver(string& linType){
        checkLinSolveType(linType);
        linT["Dir"] = 0;
        linT["Ite"] = 1;
        return linT.find(linTypeType)->second;
    }

    void checkLinSolveType(string& linType){
        if( (linType.compare("Dir")!=0) && ((linType.compare("Ite")!=0))){cerr<< "Error in Linear Solver type!"<<endl; exit(1);};
    }


    // define Linear System solver in Greedy;



    // define Data Type Map;
    int useMapDataType(string& dataType){
        checkDataType(dataType);
        dataT["ASCII"] = 0;
        dataT["VTK"] = 1;
        dataT["BINARY"] = 2;
        dataT["ALL"] = 3;
        return dataT.find(dataType)->second;
    }

    void checkDataType(string& dataType){
        if( (dataType.compare("ASCII")!=0) && ((dataType.compare("VTK")!=0)) && ((dataType.compare("BINARY")!=0)) && ((dataType.compare("ALL")!=0))){cerr<< "Error in Data type!"<<endl; exit(1);};
    }



    // ACCESS FUNCTIONS:

    // 1 -- Space Discretization --

    // 2 -- Parameters Discretization --

    // 3 -- Time Discretization --


    // 4 -- Solvers Type and Tolerances --
    inline int linearSolverType(){return useMapLinearSolver(m_linSolverType); }
    inline double tol(){return m_tol; }
    inline double svd(){return m_svd; }

    // 5 -- I/O
    inline int dataType(){return useMapDataType(m_dataType); }
    inline string dirToSave(){return m_dirToSave; }
};





// IMPLEMENTATION:


// Function that reads the parameters file:
void codeData::read(char* fileName){
    cout << "Reading file: " << fileName << endl;
    GetPot paramFile(fileName);

    // I - SPACE DISCRETIZATION:


    // I.1) for FINITE DIFFERENCES-FOURIER:


    // I.2) for FINITE ELEMENTS:


    // II - PARAMETERS DISCRETIZATION:

    // III - TIME DISCRETIZATION:

    // IV - SOLVERS AND TOLERANCES:
    m_linSolverType = paramFile("Solvers/Poisson","nil");
    m_tol = paramFile("Solvers/tol",0.);
    m_svd = paramFile("Solvers/svd",0.);

    // V - I/O:
    m_dataType = paramFile("IO/type","nil");
    m_dirToSave = paramFile("IO/outputDir","nil");
}
