// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>

#include "auxiliary.h"
#include "GenTensor.h"
#include "CoulombTensor.h"
#include "GenTabTensor.h"
#include "TuckerTensor.h"
#include "Node.h"
#include "SubTensor.h"
#include "ApproxTensor.h"

using namespace std;
int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";
    PetscErrorCode ierr;

    SlepcInitialize(&argc,&args,(char*)0,help);
    //PetscInitialize(&argc,&args,(char*)0,help);

    //ierr = SlepcInitialize(&argc,&args,(char*)0,help);

    int numProc;
    ierr = MPI_Comm_size(PETSC_COMM_WORLD,&numProc);
    //MPI_Comm_size(PETSC_COMM_WORLD,&numProc);
    int myId;
    MPI_Comm_rank (PETSC_COMM_WORLD, &myId);
  

    auxiliary z;


    int num = 2;  
    int n0 = 1024;
    int n1 = 1024;

    /*int num = 3;  
    int n0 = 512;
    int n1 = 512; 
    int n2 = 512; */
    vector<int> vec_dim(2); 
    vec_dim[0] = n0; 
    vec_dim[1] = n1; 
    //vec_dim[2] = n2; 

    CoulombTensor* CT = new CoulombTensor(num, vec_dim); 

    vector<double> box_inf(num, -100.0); 
    vector<double> box_sup(num, 100.0); 

    double epsilon = 1.0; 

    CT->init(box_inf, box_sup, epsilon); 

    vector<int> index(2); 
    index[0] = 19; 
    index[1] = 4; 
    //index[2] = 6;

    double res = 0; 
    CT->eval(index, res, PETSC_COMM_WORLD);

    cout << "res = " << res << endl; 

    /*

	
    Mat M;
    int N;
    z.initMat(M, n1, n0*n2, PETSC_COMM_WORLD);
    z.finalizeMat(M);

    cout << "  Before matricization   " << endl; 

    MatView(M, PETSC_VIEWER_STDOUT_WORLD);


    CT.computeUnfolding(1, M, N,PETSC_COMM_WORLD); 

    cout << "  After matricization   " << endl;

    MatView(M, PETSC_VIEWER_STDOUT_WORLD);

    cout << CT.getNumVar() << endl;

    CT.compute_HOSVD(PETSC_COMM_WORLD);
    
    cout << "Done!!!!!!!!!!" << endl;

    Vec S;
    S = CT.getSingVals(0);

    VecView(S, PETSC_VIEWER_STDOUT_WORLD);*/


    /*vector<int> vec_dim2(3); 
    vec_dim[0] = 2; 
    vec_dim[1] = 2; 
    vec_dim[2] = 2; 


    Vec coeff; 
    int m = 8; 
    z.initVec(coeff,m, PETSC_COMM_WORLD);
    z.setVecEl(coeff,0, 0.5, PETSC_COMM_WORLD);
 z.setVecEl(coeff,1, 5, PETSC_COMM_WORLD);
 z.setVecEl(coeff,2, 1, PETSC_COMM_WORLD);
 z.setVecEl(coeff,3, 2.5, PETSC_COMM_WORLD);
 z.setVecEl(coeff,4, 3.5, PETSC_COMM_WORLD);
 z.setVecEl(coeff,5, -6, PETSC_COMM_WORLD);
 z.setVecEl(coeff,6, -1.2, PETSC_COMM_WORLD);
 z.setVecEl(coeff,7, 0.5, PETSC_COMM_WORLD);
    z.finalizeVec(coeff); 

  cout << "Coeff finalized " << endl; */

  vector< vector<int> > indices(2);

  for (int j=0; j<2; j++)
  {
	vector<int> vecnj(vec_dim[j]); 
	for (int k=0; k<vec_dim[j]; k++)
	{
		vecnj[k] = k; 
	}

	indices[j] = vecnj; 
  }

    Node* itr = NULL;

    Node* node = new Node(indices);

    node->createDyadicTree(7);

    Node* newnode = node->createNodeCopy(); 


    ApproxTensor approx2(newnode, CT, PETSC_COMM_WORLD);
	
   //	cout << "before" << endl; 
   // approx2.compute_local_HOSVD(PETSC_COMM_WORLD);
   //	cout << "compute_local_SVD" << endl; 

   vector<double> errtab2 = approx2.getLeafErrors();
   cout << errtab2[0] << endl; 

   approx2.compute_greedy_errmem(0.001,PETSC_COMM_WORLD);

   vector<double> errtabnew2 = approx2.getLeafErrors();
   cout << errtabnew2[0] << endl;

   //double memtot2 = n0*n1*n2;
   double memtot2 = n0*n1;
   double memgreed2 = approx2.compute_memtot();

   cout << "Mem Tot 2 = " << memtot2 << endl;
 
   cout << "Mem greedy 2 = " << memgreed2 << endl; 

   cout << "Gain 2 = " << memgreed2/memtot2 << endl;

   bool merge, merge2;

   /*cout << "Nombre de feuilles avant toute chose: " << endl; 
   cout << approx2.getNode()->getLeafs().size() << endl; 

   ApproxTensor* approx3 = approx2.testMerging(100,PETSC_COMM_WORLD,merge);

   cout << "Nombre de feuilles après test : " << endl; 
   cout << approx2.getNode()->getLeafs().size() << endl; 

   cout << "Nombre de feuilles de approx3 : " << endl; 
   cout << approx3->getNode()->getLeafs().size() << endl; 

   approx2.tryAndDoMerging(100,PETSC_COMM_WORLD,merge2);
   cout << "Nombre de feuilles après 2ème test : " << endl; 
   cout << approx2.getNode()->getLeafs().size() << endl; */

 
   int countmerge, mem, meminit;
   approx2.MergeAsPossibleSecondStrategy(countmerge, meminit, mem, PETSC_COMM_WORLD);

   double memd = mem; 
   double memdd = meminit;

   cout << memdd << " " << memgreed2 << endl; 

    cout << "Old gain = " << memgreed2/memtot2 << endl;
   cout << mem << "  New gain = " << mem/memtot2 << endl;
	cout << mem << "  Relative gain = " << mem/memgreed2 << endl;

   string path = "/libre/ehrlachv/Tensor/Results/ResCoulomb_7_dim2_N1024";


   ofstream myfile;
   string name = path+"_memorygain.txt";
   const char* name2 = name.c_str();  
    myfile.open(name2);

    myfile << "Memories: Total = " << memtot2 << "  Greedy = " << memgreed2 << "  Greedy Tree = " << endl; 

    myfile << "Old gain = " << memgreed2/memtot2 << endl;
    myfile << mem << "  New gain = " << mem/memtot2 << endl;
    myfile << mem << "  Relative gain = " << mem/memgreed2 << endl;


  myfile.close();	

   approx2.getNode()->visualize(path);




/*
   vector<vector<int> > vecindnew2 = leafs2[5]->getIndicesFromLeafs();

   for (int j=0; j<3; j++)
   {
	cout << "j  = " << j << endl; 
   z.printStdVecInt(vecindnew2[j], PETSC_COMM_WORLD);

    }

   vector<vector<int> > vecindnew3 = (leafs2[5]->getParent())->getIndicesFromLeafs();

   for (int j=0; j<3; j++)
   {
	cout << "j  = " << j << endl; 
   z.printStdVecInt(vecindnew3[j], PETSC_COMM_WORLD);

    }*/

    


    ierr = PetscFinalize();
    return 0;
}
