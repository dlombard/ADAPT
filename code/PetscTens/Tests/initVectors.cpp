// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include "genericInclude.h"
#include "auxiliary.h"

using namespace std;

// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

    // Initialize the auxiliary class:
    auxiliary a;

    // init vector:
    int vecSize = 8;
    Vec v;
    a.initVec(v,vecSize,PETSC_COMM_WORLD);
    // put the first element to 5.0
    a.setVecEl(v,0,5.0,PETSC_COMM_WORLD);
    //finalise and print
    a.finalizeVec(v);
    VecView(v,PETSC_VIEWER_STDOUT_WORLD);

    MPI_Barrier(MPI_COMM_WORLD);
    PetscPrintf(PETSC_COMM_WORLD, "\nBroadcast of the value...\n");
    MPI_Barrier(MPI_COMM_WORLD);

    // global getVecEl:
    double theVal = a.getVecEl(v,0,PETSC_COMM_WORLD);
    cout << "I am proc " << idProc << ": v[0] = " << theVal << endl;

    MPI_Barrier(MPI_COMM_WORLD);
    PetscPrintf(PETSC_COMM_WORLD, "\nNo broadcast of the value...\n");
    MPI_Barrier(MPI_COMM_WORLD);

    // getVecEl, collective call but without broadcasting:
    int theRank;
    double val;
    a.getVecEl(v,0,val,theRank,PETSC_COMM_WORLD,false);
    if(idProc==theRank){
      cout << "I am proc " << idProc << ": v[0] = " << val << endl;
    }
    else{
      cout << "I am proc " << idProc << ": v[0] = " << val << endl;
    }



    // Finalize the MPI environment.
    SlepcFinalize();
    return 0;
}
