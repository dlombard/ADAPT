// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include "genericInclude.h"
#include "auxiliary.h"
#include "fullTensor.h"
#include "CPTensor.h"

using namespace std;

// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


    // define auxiliary
    auxiliary a;

    // init a Full Tensor given the dimension and the resolution:
    unsigned int dim = 4;
    vector<unsigned int> resPerDim(dim);
    resPerDim[0] = 2;
    resPerDim[1] = 4;
    resPerDim[2] = 3;
    resPerDim[3] = 6;


    fullTensor B(dim, resPerDim, PETSC_COMM_WORLD);
    B.printTensorStruct();

    // set tensor elements:
    vector<unsigned int> I = {0,0,0,0};
    B.setTensorElement(I,57.0);
    I = {0,1,0,0};
    B.setTensorElement(I,1.0);

    I = {1,1,1,0};
    B.setTensorElement(I,-6.0);
    I = {0,1,1,1};
    B.setTensorElement(I,-7.0);
    I = {1,2,1,1};
    B.setTensorElement(I,-8.0);

    I = {0,2,2,4};
    B.setTensorElement(I,3.14);
    I = {1,3,2,5};
    B.setTensorElement(I,2.71);

    B.finalizeTensor();

    /*CPTensor A(dim, resPerDim, PETSC_COMM_WORLD);
    vector<Vec> theTerm;
    A.compute_CPTT_Term(B, theTerm);*/

    Mat Unfolding_0;
    B.computeUnfolding(0, Unfolding_0);

    double sigma;
    Vec u,v;
    a.computeOneSVDTerm(Unfolding_0, sigma, u, v, PETSC_COMM_WORLD);

    PetscPrintf(PETSC_COMM_WORLD, "sigma = %f \n", sigma);

    Mat U,V;
    Vec singVec;
    a.svdLHC(Unfolding_0, U, V, singVec, PETSC_COMM_WORLD);
    VecView(singVec, PETSC_VIEWER_STDOUT_WORLD);

    // Finalize the MPI environment.
    SlepcFinalize();
    return 0;
}
