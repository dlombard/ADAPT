// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include "genericInclude.h"
#include "auxiliary.h"
#include "fullTensor.h"
#include "CPTensor.h"

using namespace std;

// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);


    // define auxiliary
    auxiliary a;

    // init a Full Tensor given the dimension and the resolution:
    unsigned int dim = 3;
    vector<unsigned int> resPerDim(dim);
    resPerDim[0] = 2;
    resPerDim[1] = 4;
    resPerDim[2] = 3;

    CPTensor f(dim, resPerDim, PETSC_COMM_WORLD);
    unsigned int rank = 3;
    vector<vector<Vec> > cpTerms(rank);
    PetscRandom rctx;
    PetscRandomCreate(PETSC_COMM_WORLD,&rctx);
    f.set_rank(rank);
    vector<double> coTab(rank);
    for(unsigned int iTerm=0; iTerm<rank; iTerm++){
        cpTerms[iTerm].resize(dim);
        coTab[iTerm] = 1.0;
        for(unsigned int idVar=0; idVar<dim; idVar++){
          a.initVec(cpTerms[iTerm][idVar], resPerDim[idVar], PETSC_COMM_WORLD);
          a.finalizeVec(cpTerms[iTerm][idVar]);
          VecSetRandom(cpTerms[iTerm][idVar],rctx);
          //VecView(cpTerms[iTerm][idVar], PETSC_VIEWER_STDOUT_WORLD);
        }
    }
    f.set_terms(cpTerms);
    f.set_coeffs(coTab);
    PetscRandomDestroy(&rctx);



    double norm2 = f.norm2CP();
    PetscPrintf(PETSC_COMM_WORLD, "norm f = %e \n", norm2);

    // define identity mass matrices:
    vector<Mat> massMat(dim);
    for(unsigned int idVar=0; idVar<dim; idVar++){
      massMat[idVar] = a.eye(resPerDim[idVar], PETSC_COMM_WORLD);
    }

    CPTensor appTens;
    appTens.greedy_ApproxOf(f, massMat, 1.0e-2, 1.0e-5);



    // Finalize the MPI environment.
    SlepcFinalize();
    return 0;
}
