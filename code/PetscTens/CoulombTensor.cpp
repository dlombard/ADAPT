// Created by: Damiano & Virginie on ...
#include "CoulombTensor.h"

using namespace std;

// Constructor
CoulombTensor::CoulombTensor(const int& num_var, const std::vector<unsigned int>& dim_var, MPI_Comm theComm){
        m_theComm = theComm;
	m_nVar = num_var;
	m_nDof_var = dim_var;
	compute_minc(); 
}




void CoulombTensor::init(const vector<double>& box_inf, const vector<double>& box_sup, const double& epsilon, const double& alpha){
		

	//Safety checks
	assert(box_inf.size() == m_nVar);
	assert(box_sup.size() == m_nVar);
	for (int i=0; i<m_nVar; i++)
	{
		assert(box_inf[i]<box_sup[i]);
	}
		

	box_inf_ = box_inf; 
	box_sup_ = box_sup;

	box_delta_ = vector<double>(m_nVar, 0.0);

	for (int i=0; i<m_nVar; i++)
	{
		box_delta_[i] = (box_sup_[i] - box_inf_[i])/m_nDof_var[i]; 
	}


	epsilon_ =epsilon;
	alpha_  = alpha; 

}; 


void CoulombTensor::init(const vector<double>& box_inf, const vector<double>& box_sup, const double& epsilon){

	//Safety checks
	assert(box_inf.size() == m_nVar);
	assert(box_sup.size() == m_nVar);
	for (int i=0; i<m_nVar; i++)
	{
		assert(box_inf[i]<box_sup[i]);
	}
		

	box_inf_ = box_inf; 
	box_sup_ = box_sup;

	box_delta_ = vector<double>(m_nVar, 0.0);

	for (int i=0; i<m_nVar; i++)
	{
		box_delta_[i] = (box_sup_[i] - box_inf_[i])/m_nDof_var[i]; 
	}


	epsilon_ =epsilon;
	alpha_  = 1.0; 

}; 



void CoulombTensor::eval(const vector<unsigned int>& vec_int, double& res){


	//Safety checks		
	assert(vec_int.size()==m_nVar); 
	for (int i=0; i<m_nVar; i++)
	{
		assert(vec_int[i] < m_nDof_var[i]); 
	}	
	

	res = 0; 
	for (int i=0; i< m_nVar; i++)
	{
		double xi = box_inf_[i] + (vec_int[i]+0.5)*box_delta_[i];

		for (int j=i+1; j<m_nVar; j++)
		{

			double xj = box_inf_[j] + (vec_int[j]+0.5)*box_delta_[j];
			res = res + 1.0/(epsilon_ + pow(abs(xi -xj), alpha_));
		}
	}
		

}



