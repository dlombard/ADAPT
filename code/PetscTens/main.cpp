// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>

#include "auxiliary.h"
#include "tensor.h"
#include "CoulombTensor.h"
#include "GibbsTensor.h"
#include "GaussianTensor.h"
#include "fullTensor.h"
#include "Node.h"
#include "SubTensor.h"
#include "ApproxTensor.h"
#include "HPFCPTensor.h"
#include "CPTensor.h"
#include "operatorTensor.h"
#include "multilinearSolver.h"

using namespace std;
int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";
    PetscErrorCode ierr;

    SlepcInitialize(&argc,&args,(char*)0,help);
    //PetscInitialize(&argc,&args,(char*)0,help);

    //ierr = SlepcInitialize(&argc,&args,(char*)0,help);

    int numProc;
    ierr = MPI_Comm_size(PETSC_COMM_WORLD,&numProc);
    //MPI_Comm_size(PETSC_COMM_WORLD,&numProc);
    int myId;
    MPI_Comm_rank (PETSC_COMM_WORLD, &myId);


    auxiliary z;

    // Init Problem:
    int num = 2;
    int n0 = 64;
    int n1 = 64;


    double valinf  = 0; 
    double valsup = 1; 
    double L = valsup - valinf; 
    vector<double> box_inf(num, valinf); 
    vector<double> box_sup(num, valsup); 

    const double h0 = L/static_cast<double>(n0-1);
    const double h1 = L/static_cast<double>(n0-1);

    cout << "h0 = " << h0 << "h1 = " << h1 << endl; 

    

    vector<unsigned int> vec_dim(2);
    vec_dim[0] = n0;
    vec_dim[1] = n1;

    Mat K_x, K_y, M_x, M_y;
    z.initMat(K_x, n0, n0, PETSC_COMM_WORLD);
    z.setMatEl(K_x, 0, 0, 2.0/h0, PETSC_COMM_WORLD);
    z.setMatEl(K_x, 0, 1,-1.0/h0, PETSC_COMM_WORLD);
    z.setMatEl(K_x, n0-1, n0-1, 2.0/h0, PETSC_COMM_WORLD);
    z.setMatEl(K_x, n0-1, n0-2,-1.0/h0, PETSC_COMM_WORLD);
    for(unsigned int iRow=1; iRow < n0-1; iRow++){
      z.setMatEl(K_x, iRow, iRow-1, -1.0/h0, PETSC_COMM_WORLD);
      z.setMatEl(K_x, iRow, iRow, 2.0/h0, PETSC_COMM_WORLD);
      z.setMatEl(K_x, iRow, iRow+1, -1.0/h0, PETSC_COMM_WORLD);
    }
    z.finalizeMat(K_x);
    MatView(K_x , PETSC_VIEWER_STDOUT_WORLD);

    z.initMat(K_y, n1, n1, PETSC_COMM_WORLD);
    z.setMatEl(K_y, 0, 0, 2.0/h1, PETSC_COMM_WORLD);
    z.setMatEl(K_y, 0, 1,-1.0/h1, PETSC_COMM_WORLD);
    z.setMatEl(K_y, n1-1, n1-1, 2.0/h1, PETSC_COMM_WORLD);
    z.setMatEl(K_y, n1-1, n1-2,-1.0/h1, PETSC_COMM_WORLD);
    for(unsigned int iRow=1; iRow < n1-1; iRow++){
      z.setMatEl(K_y, iRow, iRow-1, -1.0/h1, PETSC_COMM_WORLD);
      z.setMatEl(K_y, iRow, iRow, 2.0/h1, PETSC_COMM_WORLD);
      z.setMatEl(K_y, iRow, iRow+1, -1.0/h1, PETSC_COMM_WORLD);
    }
    z.finalizeMat(K_y);
    MatView(K_y , PETSC_VIEWER_STDOUT_WORLD);

    z.initMat(M_x, n0, n0, PETSC_COMM_WORLD);
    z.setMatEl(M_x, 0, 0, 2.0*h0/3.0, PETSC_COMM_WORLD);
    z.setMatEl(M_x, 0, 1, h0/6, PETSC_COMM_WORLD);
    z.setMatEl(M_x, n0-1, n0-1, 2.0*h0/3.0, PETSC_COMM_WORLD);
    z.setMatEl(M_x, n0-1, n0-2, h0/6.0, PETSC_COMM_WORLD);
    for(unsigned int iRow=1; iRow < n0-1; iRow++){
      z.setMatEl(M_x, iRow, iRow-1, h0/6.0, PETSC_COMM_WORLD);
      z.setMatEl(M_x, iRow, iRow, 2.0*h0/3.0, PETSC_COMM_WORLD);
      z.setMatEl(M_x, iRow, iRow+1, h0/6.0, PETSC_COMM_WORLD);
    }
    z.finalizeMat(M_x);
    MatView(M_x , PETSC_VIEWER_STDOUT_WORLD);

    z.initMat(M_y, n1, n1, PETSC_COMM_WORLD);
    z.setMatEl(M_y, 0, 0, 2.0*h1/3.0, PETSC_COMM_WORLD);
    z.setMatEl(M_y, 0, 1, h1/6, PETSC_COMM_WORLD);
    z.setMatEl(M_y, n1-1, n1-1, 2.0*h1/3.0, PETSC_COMM_WORLD);
    z.setMatEl(M_y, n1-1, n1-2, h1/6.0, PETSC_COMM_WORLD);
    for(unsigned int iRow=1; iRow < n1-1; iRow++){
      z.setMatEl(M_y, iRow, iRow-1, h1/6.0, PETSC_COMM_WORLD);
      z.setMatEl(M_y, iRow, iRow, 2.0*h1/3.0, PETSC_COMM_WORLD);
      z.setMatEl(M_y, iRow, iRow+1, h1/6.0, PETSC_COMM_WORLD);
    }
    z.finalizeMat(M_y);
    MatView(M_y , PETSC_VIEWER_STDOUT_WORLD);


    // mass operator to compute the rhs.
    vector<vector <Mat> > massOp(1);
    vector<Mat> theMass(2);
    theMass[0] = M_x;
    theMass[1] = M_y;
    massOp[0] = theMass;


    // -Delta operator, stiffness:
    vector<vector <Mat> > stiffOp(2);
    vector<Mat> stiff_1(2);
    stiff_1[0] = K_x;
    stiff_1[1] = M_y;
    stiffOp[0] = stiff_1;
    vector<Mat> stiff_2(2);
    stiff_2[0] = M_x;
    stiff_2[1] = K_y;
    stiffOp[1] = stiff_2;

operatorTensor Delta(stiffOp, PETSC_COMM_WORLD);
operatorTensor Mass(massOp, PETSC_COMM_WORLD);

    double alpha = 0.1; 
   double beta = 0.0001; 

   vector<double> mu(2,0.75);
    vector<vector<double> > inv_cov(2); 
   vector<double> col1(2);
   col1[0] = (alpha+beta)/(4*alpha*beta); 
   col1[1] = (beta-alpha)/(4*alpha*beta); 
 
   vector<double> col2(2);
   col2[1] = (alpha+beta)/(4*alpha*beta); 
   col2[0] = (beta-alpha)/(4*alpha*beta); 

	inv_cov[0] = col1; 
        inv_cov[1] = col2; 

    GaussianTensor* f = new GaussianTensor(num, vec_dim, PETSC_COMM_WORLD); 

    f->init(box_inf, box_sup, mu, inv_cov); 

 
    vector<unsigned int> index(2); 
    index[0] = 5; 
    index[1] = 60; 

    double res =0; 
    f->eval(index, res);

    cout << "res = " << res << endl;

    index[0] = 32; 
    index[1] = 32; 

    f->eval(index, res);

    cout << "res = " << res << endl;


    index[0] = 96; 
    index[1] = 96; 

    f->eval(index, res);

    cout << "res = " << res << endl;

  vector< vector < unsigned int> > indices(2);

  for (int j=0; j<2; j++)
  {
	vector < unsigned int> vecnj(vec_dim[j]); 
	for (int k=0; k<vec_dim[j]; k++)
	{
		vecnj[k] = k; 
	}

	indices[j] = vecnj; 
  }

    Node* itr = NULL;
 cout << "here " << endl; 
    Node* node = new Node(indices);
  cout << "node created" << endl;
 
    node->createDyadicTree(3);
  cout << "new node " << endl; 

    Node* newnode = node->createNodeCopy(); 

cout << "done" << endl; 

	   cout << "First number of subdomains = " << newnode->getLeafs().size() << endl;


   ApproxTensor* approx2 = new ApproxTensor(newnode, f);

   vector<double> errtab2 = approx2->getLeafErrors();
   cout << errtab2[0] << endl; 

   approx2->compute_greedy_errmem(1e-5);

   vector<double> errtabnew2 = approx2->getLeafErrors();
   cout << errtabnew2[0] << endl;

   //double memtot2 = n0*n1*n2;
   double memtot2 = n0*n1;
 
   double memgreed2 = approx2->compute_memtot();

   cout << "Mem Tot 2 = " << memtot2 << endl;
 
   cout << "Mem greedy 2 = " << memgreed2 << endl; 

   cout << "Gain 2 = " << memgreed2/memtot2 << endl;

   bool merge, merge2;



 string path = "./results/test";
   string

 path2 = "./results/test";
      approx2->getNode()->visualize(path2);
     approx2->writeErrors(path2);
  approx2->writeMemory(path2);
  approx2->writeSVDindices(path2);

string path3 = "./results/test_valuetens.txt";
   ofstream myfile3;
const char* name3 = path3.c_str();  
    myfile3.open(name3);

for (int i = 0; i<n0; i++)
{
	for (int j=0; j<n1; j++)
	{
		vector<unsigned int> index_ij(2); 
   		 index_ij[0] = i; 
    		index_ij[1] = j; 
		f->eval(index_ij,res);
		myfile3 << i << " " << j << " " << res << endl ;
	}

}


 
   int countmerge, mem, meminit;
   approx2->MergeAsPossibleSecondStrategy(countmerge, meminit, mem);

   
   	vector<Node*> leafs = approx2->getNode()->getLeafs(); 
	cout << leafs.size() << endl;
	vector<vector<unsigned int> > leafs_SVD_indices = approx2->getLeafSVDIndices(); 
	cout << leafs_SVD_indices.size() << endl;

        cout << approx2->getLeafSVDIndices()[0][0] << endl;
	for (int k=0; k<leafs.size(); k++)
	{
		cout << "k = " << k << " " <<   leafs_SVD_indices[k][0] << " " <<  leafs_SVD_indices[k][1] << endl; 
	}

   cout << approx2->getLeafSVDIndices()[0][0] << endl;

  HPFCPTensor F_HPF(approx2, true);

	vector<Node*> leafs2 = F_HPF.getNode()->getLeafs(); 
	vector<CPTensor> vectens  = F_HPF.getTens();
	int r =0; 
	for (int j=0; j<leafs2.size(); j++)
	{
		r = r + vectens[j].rank();
	}

	cout << "r = " << r << endl; 

 

//First method
CPTensor rhs_tot = F_HPF.CP_From_HPFCP();

	cout << "rank = " << rhs_tot.rank() << endl; 


// Cout matrice

cout << Delta.numTerms() << " "  <<  Delta.nVar() << endl; 

    MatView(Delta.op(0,0) , PETSC_VIEWER_STDOUT_WORLD);
    MatView(Delta.op(0,1) , PETSC_VIEWER_STDOUT_WORLD);
    MatView(Delta.op(1,0) , PETSC_VIEWER_STDOUT_WORLD);
    MatView(Delta.op(1,1) , PETSC_VIEWER_STDOUT_WORLD); 

multilinearSolver mySolver; 


CPTensor sol(2, vec_dim, PETSC_COMM_WORLD); 

cout << "rank_tot = " << rhs_tot.rank() << endl; 

for (int i=0; i<rhs_tot.rank(); i++)
{
 
	cout << "i = " << i << endl; 
	vector<Vec> thisterm = rhs_tot.terms(i);
        VecView(thisterm[0], PETSC_VIEWER_STDOUT_WORLD);
	VecView(thisterm[1], PETSC_VIEWER_STDOUT_WORLD); 
	CPTensor rhsi(thisterm,PETSC_COMM_WORLD);

	CPTensor soli(2, vec_dim, PETSC_COMM_WORLD);
        
	mySolver.greedy_ALS(Delta, rhsi, soli, 1e-4, 1e-1);
	

	//mySolver.CG_compress(Delta, rhsi,soli,1e-4);


        //

        //mySolver.CG(Delta, rhsi,soli,1e-4);

 	cout << "solveri done" << endl; 
        cout << endl;
        cout << endl;  

        sol.sum(soli, 1.0,true);
        soli.clear();  
  	cout << "i = done " << i << endl; 
}

	
     Node* newnode_sol = node->createNodeCopy(); 
     tensor* mytens = &sol; 
     ApproxTensor* approx_sol = new ApproxTensor(newnode_sol, mytens);
     approx_sol->compute_greedy_errmem(1e-5);

       int countmerge_sol, mem_sol, meminit_sol;
   approx_sol->MergeAsPossibleSecondStrategy(countmerge_sol, meminit_sol, mem_sol);





    ierr = PetscFinalize();
    return 0;
}
