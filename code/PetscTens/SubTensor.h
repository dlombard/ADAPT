/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/

#ifndef SubTensor_h
#define SubTensor_h

#include "genericInclude.h"
#include "auxiliary.h"
#include "Node.h"
#include "tensor.h"


using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:

class SubTensor: public tensor{

	protected:

	Node* node_; // Node which determines the subset of indices
	tensor* tensor_; //the "original" tensor

	vector< vector<unsigned int> > indices_; //set of indices related to the original tensor 


	public:

	// -- CONSTRUCTORS and DESTRUCTOR --
	SubTensor(){};
	~SubTensor(){};
 
	//Overloaded constructors
	SubTensor(Node*, tensor*);


	//-- Evaluation of the tensor --
	void eval(const vector<unsigned int>&, double&); 



	

};


#endif
