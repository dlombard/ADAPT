/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/

#ifndef ApproxTensor_h
#define ApproxTensor_h

#include "genericInclude.h"
#include "auxiliary.h"
#include "Node.h"
#include  "tensor.h"
#include  "SubTensor.h"


using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:

class ApproxTensor{

	protected:

	Node* node_;
	tensor* tensor_;

	vector<SubTensor*> leaf_subtensors_; 

	vector<double> leaf_errors_; 
	vector< vector<unsigned int> > leaf_SVDindices_; 
	vector<unsigned int> leaf_memory_; 
	double err_tot_; 

	public:
	//vector< vector<unsigned int> > copy_leaf_SVDindices_; 

	// -- CONSTRUCTORS and DESTRUCTOR --
	ApproxTensor(){};
	~ApproxTensor(){};
 
	//Overloaded constructors
	ApproxTensor(Node*, tensor*);
	ApproxTensor(Node*, tensor*, vector<SubTensor*>, vector< vector<unsigned int>>, vector<double>, vector<unsigned int>);

	inline Node* getNode(){return node_;}
	inline tensor* getTensor(){return tensor_;}

	inline vector<SubTensor*> getSubTensors(){return leaf_subtensors_;}

	inline vector<double> getLeafErrors(){return leaf_errors_;}
	inline vector< vector<unsigned int> > getLeafSVDIndices(){ return leaf_SVDindices_; }
	inline vector<unsigned int> getLeafMemory(){return leaf_memory_;} 

	inline double getErrTot(){return err_tot_;}

     

	//Approximation functions
	void compute_local_HOSVD(); 

	void compute_greedy_errmem(const double&); 

	void compute_errtot();

	void compute_errandmemNode(Node*, double&, int&);

	int compute_memtot();

	void isIn(const Node*, const vector<Node*>, int&);

	ApproxTensor* testMerging(const int&, bool&);
	void tryAndDoMerging(const int&,  bool&);

	ApproxTensor* createCopy();

	void setData(vector<SubTensor*> , vector< vector<unsigned int>>, vector<double>, vector<unsigned int>);
	void copyDataFrom(ApproxTensor*);

	void  MergeAsPossibleFirstStrategy(int&, int&, int&);
	void  MergeAsPossibleSecondStrategy(int&, int& , int&);

        void writeErrors(const string&);
        void writeSVDindices(const string&); 
	void writeMemory(const string&); 

        void SplitAsPossible(int&, int&, int&);
	void tryAndDoSplitting(const int&,bool&);




};


#endif
