/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/


#ifndef operatorTensor_h
#define operatorTensor_h

#include "genericInclude.h"
#include "auxiliary.h"
#include "CPTensor.h"


using namespace std;

class operatorTensor{

private:
  MPI_Comm m_theComm;
  unsigned int m_numTerms;
  unsigned int m_nVar;
  vector<vector<Mat> > m_op;
  auxiliary a;

public:

  operatorTensor(){};
  ~operatorTensor(){};

  // overloaded CONSTRUCTOR:
  operatorTensor(unsigned int, unsigned int, MPI_Comm);
  operatorTensor(vector<Mat>&, MPI_Comm);
  operatorTensor(vector<vector<Mat> >&, MPI_Comm);


  // SETTERS
  inline void set_numTerms(unsigned int nTerms){m_numTerms = nTerms;};
  inline void set_nVar(unsigned int nVar){m_nVar = nVar;};
  inline void set_Comm(MPI_Comm theComm){m_theComm = theComm;};
  inline void set_term(unsigned int iTerm, unsigned int iVar, Mat A){m_op[iTerm][iVar] = A;};


  // ACCES FUNCTIONS:
  unsigned int numTerms(){return m_numTerms;};
  unsigned int nVar(){return m_nVar;};
  vector<vector<Mat> > op(){return m_op;};
  vector<Mat> op(unsigned int iTerm){return m_op[iTerm];};
  Mat op(unsigned int iTerm, unsigned int jVar){return m_op[iTerm][jVar];};


  // Operations:
  void applyOperator(CPTensor&, CPTensor&);
  CPTensor applyOperator(CPTensor&);
  operatorTensor adjoint();
};
#endif
