// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>

#include "auxiliary.h"
#include "tensor.h"

using namespace std;
int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    PetscInitialize(&argc,&args,(char*)0,help);
    PetscErrorCode ierr;
    int numProc;
    ierr = MPI_Comm_size(PETSC_COMM_WORLD,&numProc);
    int myId;
    MPI_Comm_rank (PETSC_COMM_WORLD, &myId);

    auxiliary z;
    Vec a,b,c,d;
    const int vecSize = 4;

    // vector a: initialize, set one element at a time, finalize:
    if(myId == 0){cout << "Vector a: " << endl;}
    z.initVec(a, vecSize, PETSC_COMM_WORLD);
    VecView(a, PETSC_VIEWER_STDOUT_WORLD);
    z.setVecEl(a, 0, 2.5, PETSC_COMM_WORLD);
    z.setVecEl(a, 2, 3.1, PETSC_COMM_WORLD);
    z.finalizeVec(a);
    VecView(a, PETSC_VIEWER_STDOUT_WORLD);
    if(myId == 0){cout << endl;}

    // vector b: initialize, set the whole vector to a globally shared vector<double>:
    /*if(myId == 0){cout << "Vector b: " << endl;}
    vector<double> sharedVec; sharedVec.resize(vecSize);
    sharedVec[0] = 1.5; sharedVec[2] = 9.0;
    initVec(b, vecSize, PETSC_COMM_WORLD);
    setVecEl(b, sharedVec, PETSC_COMM_WORLD);
    VecView(b, PETSC_VIEWER_STDOUT_WORLD);

    // getting the third element of b:
    double el = getVecEl(b, 2, PETSC_COMM_WORLD);
    if(myId == 0){
      cout << "the third element is : " << el << endl;
      cout << endl;
    }

    // vector c: intialize, set the vector equal to a vector known only by 0
    if(myId==0){cout << "Vector c" << endl;}
    initVec(c, a); // it has the same structure than a.
    vector<double> myVec;
    if(myId == 0){
        myVec.resize(vecSize);
        myVec[1] = 5.0; myVec[3] = 7.0;
    }
    // global call with a false, to say that only the master knows it:
    setVecEl(c, myVec, PETSC_COMM_WORLD, false);
    VecView(c, PETSC_VIEWER_STDOUT_WORLD);
    if(myId==0){cout << endl;}

    // vector d: each proc has its local vector and fill the global vec in parallel:
    if(myId==0){cout << "Vector d" << endl;}
    initVec(d, a);
    vector<double> locVec; locVec.resize(4/numProc);
    locVec[0] = myId; locVec[1] = 2*myId; // done for numProc=2 ...
    setVecLocEl(d, locVec, PETSC_COMM_WORLD);
    VecView(d, PETSC_VIEWER_STDOUT_WORLD);
    if(myId==0){cout << endl;}


    // take the first 3 elements of the vector b:
    vector<double> e = getVecEl(b, 0, 2, PETSC_COMM_WORLD);
    if(myId==0){cout << "e = [" << e[0] << ", " << e[1] << ", " << e[2] << " ]\n";}



    if(myId == 0){std::cout << "Normal End\n";};*/

    ierr = PetscFinalize();
    return 0;
}
