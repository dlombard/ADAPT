/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/

#ifndef GaussianTensor_h
#define GaussianTensor_h

#include "genericInclude.h"
#include "auxiliary.h"
#include "tensor.h"


using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:


class GaussianTensor: public tensor{

	private:

	vector<double> box_inf_; 
	vector<double> box_sup_;
	vector<double> box_delta_;

	vector<double> mu_; 
	vector<vector<double> > inv_cov_; 


	public: 

        GaussianTensor(){};
  	~GaussianTensor(){};

      	//Overloaded constructors
	GaussianTensor(const int&, const vector<unsigned int>&, MPI_Comm); 

	void init(const vector<double>&, const vector<double>&, const vector<double>&, const vector< vector<double> >&);

	void eval(const vector<unsigned int>& vec_int, double&);

};


#endif



