// Created by: Damiano & Virginie on ...
#include "GibbsTensor.h"

using namespace std;

// Constructor
GibbsTensor::GibbsTensor(const int& num_var, const std::vector<unsigned int>& dim_var, MPI_Comm theComm){
        m_theComm = theComm;
	m_nVar = num_var;
	m_nDof_var = dim_var;
	compute_minc(); 
}




void GibbsTensor::init(const vector<double>& box_inf, const vector<double>& box_sup, const string& type, vector< vector<double> >& params, const double& beta){
	
	//Safety checks
	assert(box_inf.size() == m_nVar);
	assert(box_sup.size() == m_nVar);
	for (int i=0; i<m_nVar; i++)
	{
		assert(box_inf[i]<box_sup[i]);
	}
		

	box_inf_ = box_inf; 
	box_sup_ = box_sup;

	box_delta_ = vector<double>(m_nVar, 0.0);

	for (int i=0; i<m_nVar; i++)
	{
		box_delta_[i] = (box_sup_[i] - box_inf_[i])/m_nDof_var[i]; 
	}


	type_potential_ = type;
	params_pairs_atoms_ = params;
	beta_  = beta; 

}; 


void GibbsTensor::init(const vector<double>& box_inf, const vector<double>& box_sup, const vector<string>& type_atoms, const double& beta)
{
		

	//Safety checks
	assert(box_inf.size() == m_nVar);
	assert(box_sup.size() == m_nVar);
	assert(type_atoms.size() == m_nVar);
	for (int i=0; i<m_nVar; i++)
	{
		assert(box_inf[i]<box_sup[i]);
	}
		

	box_inf_ = box_inf; 
	box_sup_ = box_sup;

	box_delta_ = vector<double>(m_nVar, 0.0);

	for (int i=0; i<m_nVar; i++)
	{
		box_delta_[i] = (box_sup_[i] - box_inf_[i])/m_nDof_var[i]; 
	}


	type_potential_ = "Lennard-Jones";



	vector<string> possible_atoms(5); 
	possible_atoms[0]= "A"; //Argon
	possible_atoms[1] = "K"; //Krypton
	possible_atoms[2]= "N"; //Neon
	possible_atoms[3] = "O"; //Oxygène
	possible_atoms[4] = "C"; //Carbone


	vector<double> possible_d(5); //en nm 
	possible_d[0] = 0.34; //Argon
	possible_d[1] = 0.36; //Krypton
	possible_d[2] = 0.278; //Neon
	possible_d[3] = 0.346; //Oxygène
	possible_d[4] = 0.4486; //Carbone


	vector<double> possible_E(5); //en K 
	possible_E[0] = 119.8; //Argon
	possible_E[1] = 171; //Krypton
	possible_E[2] = 34.9; //Neon
	possible_E[3] = 118; //Oxygène
	possible_E[4] = 189; //Carbone


	int pair_index = 0; 
	vector< vector<double> > params( (m_nVar*(m_nVar-1)/2) );
	for (int i=0; i<m_nVar; i++)
	{
		string typei = type_atoms[i]; 	
		int whichi;
		isIn(typei, possible_atoms, whichi); 

		double di = possible_d[whichi]; 
		double Ei = possible_E[whichi];  

		for (int j=i+1; j<m_nVar; j++)
		{
			string typej = type_atoms[j]; 
			int whichj;
			isIn(typej, possible_atoms, whichi); 

			double dj = possible_d[whichj]; 
			double Ej = possible_E[whichj];  

			double d = (di+dj)/2; 
			double E = sqrt(Ei*Ej);  

		
			vector<double> paramij(2); 
			paramij[0] = d; 
			paramij[1] = E; 
			params[pair_index] = paramij; 

			pair_index = pair_index+1; 

		}
	}



	params_pairs_atoms_ = params;
	beta_  = beta; 

};


double GibbsTensor::Potential(const double& r, const string& type, const vector<double>& param)
{
	if (type=="Quadratic")
	{
		return param[0]*r*r; 
	}
	if (type=="Lennard-Jones")
	{
		//param[0] -> d
		//param[1] -> E0
		double t1 = 12*(log(param[0]) - log(r+1e-16));
		double t2 = 6*(log(param[0]) - log(r+1e-16));
		return 4*param[1]*(exp(t1) - exp(t2)); 
	}

} 



void GibbsTensor::eval(const vector<unsigned int>& vec_int, double& res){


	//Safety checks		
	assert(vec_int.size()==m_nVar); 
	for (int i=0; i<m_nVar; i++)
	{
		assert(vec_int[i] < m_nDof_var[i]); 
	}	
	

	double V = 0; 
	res = 1.0; 
	int pair_index = 0; 
	for (int i=0; i< m_nVar; i++)
	{
		double xi = box_inf_[i] + (vec_int[i]+0.5)*box_delta_[i];

		for (int j=i+1; j<m_nVar; j++)
		{

			double xj = box_inf_[j] + (vec_int[j]+0.5)*box_delta_[j];

			double rij = abs(xi-xj);  
			V = V + Potential(rij, type_potential_, params_pairs_atoms_[pair_index]);
			//cout << "V = " << V << endl; 
			//res = res*(exp(-beta_*Potential(rij, type_potential_, params_pairs_atoms_[pair_index])));
			pair_index = pair_index+1; 
		}

	}
	
	double s = -beta_*V;
	//res = s;
	res = exp(s);
	/*if (res < 1e-16)
	{
		res = 0.;
	}*/
	res = res +1.0; 
		

}


void GibbsTensor::isIn(const string s, const vector<string> vec, int& k)
{
	k = -1; 
	for (int j=0; j<vec.size(); j++)
	{
		if (vec[j] == s)
		{
			k=j;
		}
	}
}



