/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/


#ifndef GenTabTensor_h
#define GenTabTensor_h

#include "genericInclude.h"
#include "auxiliary.h"
#include "GenTensor.h"



using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:


class GenTabTensor: public GenTensor{

	private:

	vector<vector<int> > indices_;
	Vec coeffs_;

	public: 

        GenTabTensor(){};
  	~GenTabTensor(){};

      	//Overloaded constructors
	GenTabTensor(const int&, const vector<int>&, const vector< vector<int> >&, const Vec&);


	//eval

	void eval(const vector<int>&, double&, MPI_Comm);

};


#endif



