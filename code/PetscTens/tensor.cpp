// Created by: Damiano & Virginie on ...
#include "tensor.h"


using namespace std;


// -- Constructors --

// tensor
tensor::tensor(const int& num_var, const vector<unsigned int>& dim_var){
	m_nVar = num_var;
	m_nDof_var = dim_var;
	compute_minc(); 
}


// Compute Unfolding
vector<unsigned int> tensor::compute_multi_index(const int& index, const int& i, const int& j)
{

	int jcour = j;

	vector<unsigned int> vec_ind(m_nVar);
	for (int k=0; k<m_nVar; k++)
	{
		if (k==index)
		{
			vec_ind[index] = i; 
		}
		else
		{
			vec_ind[k] = jcour % m_nDof_var[k]; 
			jcour = (jcour - vec_ind[k])/m_nDof_var[k];
		}

	}

	return vec_ind; 
}


void tensor::computeUnfolding(const unsigned int index, Mat& M, int& N)
{

	int N1 = m_nDof_var[index]; 
	int N2 = 1; 

	for (int k=0; k<m_nVar; k++)
	{
		if (k!=index)
		{
			N2 = N2*m_nDof_var[k]; 
		}
	}
	N = N2;

	a.initMat(M, N1, N2, m_theComm);

	for (int i=0; i<N1; i++)
	{

		for (int j=0; j<N2; j++)
		{
			double coeff = 0; 
			vector<unsigned int> vec_ind = compute_multi_index(index, i, j);
			eval(vec_ind, coeff);
			a.setMatEl(M,i,j, coeff, m_theComm);

		}
	}
	a.finalizeMat(M);

         

}

void tensor::compute_HOSVD()
{

	singular_vals_.resize(m_nVar); 
	singular_vecs_.resize(m_nVar);

	//SVD svd;
	

	int N;

	if (m_nVar >2)
	{

		for (int i=0; i<m_nVar; i++)
		{



			Mat M; 
			Vec S;
			Mat U;
			Mat V;

			computeUnfolding(i,M,N);

			//MatView(M, PETSC_VIEWER_STDOUT_WORLD);

                	//a.svdGK(M,U,V,S,theComm, EPSLAPACK);
			a.svdLHC(M,U,V,S,1e-10,m_theComm);

			//VecView(S, PETSC_VIEWER_STDOUT_WORLD);

			singular_vals_[i] = S; 
			singular_vecs_[i] = U; 

			//SVDDestroy(&svd);
			MatDestroy(&M);
			MatDestroy(&V);

		}
	}
	else
	{

		Mat M;
		Vec S;
		Mat U;
		Mat V;

		computeUnfolding(0,M,N);

		//MatView(M, PETSC_VIEWER_STDOUT_WORLD);

                //a.svdGK(M,U,V,S,theComm, EPSLAPACK);
		a.svdLHC(M,U,V,S,1e-10,m_theComm);

		//VecView(S, PETSC_VIEWER_STDOUT_WORLD);

		int vecSize;
  		VecGetSize(S, &vecSize);

		singular_vals_[0] = S; 
		singular_vals_[1] = S;
		singular_vecs_[0] = U; 
		singular_vecs_[1] = V; 


		MatDestroy(&M);
		//MatDestroy(&V);


	}


}

void tensor::compute_HOSVD(const double& tol)
{

	singular_vals_.resize(m_nVar); 
	singular_vecs_.resize(m_nVar);
	
	int N;

	if (m_nVar >2)
	{
		for (int i=0; i<m_nVar; i++)
		{

			Mat M;
			Vec S;
			Mat U;
			Mat V;

			computeUnfolding(i,M,N);

			//MatView(M, PETSC_VIEWER_STDOUT_WORLD);

                	//a.svdGK(M,U,V,S,theComm, EPSLAPACK);
			a.svdLHC(M,U,V,S,0.25*tol,m_theComm);

			//VecView(S, PETSC_VIEWER_STDOUT_WORLD);

			int vecSize;
  			VecGetSize(S, &vecSize); 

			singular_vals_[i] = S; 
			singular_vecs_[i] = U; 
			

			//SVDDestroy(&svd);
			MatDestroy(&M);
			MatDestroy(&V);
			//VecDestroy(&u);
   			//VecDestroy(&v);
			//MatDestroy(&U);
			//VecDestroy(&S);

		}
	}
	else
	{
		Mat M;
		Vec S;
		Mat U;
		Mat V;

		computeUnfolding(0,M,N);

		//MatView(M, PETSC_VIEWER_STDOUT_WORLD);

                //a.svdGK(M,U,V,S,theComm, EPSLAPACK);
		a.svdLHC(M,U,V,S,0.25*tol,m_theComm);

		//VecView(S, PETSC_VIEWER_STDOUT_WORLD);

		int vecSize;
  		VecGetSize(S, &vecSize);


		singular_vals_[0] = S; 
		singular_vals_[1] = S;
		singular_vecs_[0] = U; 
		singular_vecs_[1] = V; 


		MatDestroy(&M);
		//MatDestroy(&V);
	}


}



void tensor::getLargestSingVal(const vector<unsigned int>& vecind, int& jmax, double& sigmamax)
{

	sigmamax = 0; 
	jmax = -1; 


	for (int j=0; j<m_nVar; j++)
	{
		
		
		int ind = vecind[j];
		int nRow; 
		Vec S  = singular_vals_[j];
		//cout << "j = " << j << " ind = " << ind << endl; 
		//VecView(S, PETSC_VIEWER_STDOUT_WORLD);

		VecGetSize(S,&nRow);
		if ( (ind) < nRow)
		{
			if (a.getVecEl(S,ind,m_theComm)>sigmamax)
			{
				jmax = j;
				sigmamax = a.getVecEl(S,ind,m_theComm);  
			}

		}
		

	}	

	//cout << "sigmamax = " << endl;
}



void tensor::getError(const vector<unsigned int>& vecind, double& err)
{
 
	err = 0; 

	for (int j=0; j<m_nVar; j++)
	{
		
		int ind = vecind[j];
		int nRow; 
		VecGetSize(singular_vals_[j],&nRow);
	
		for (int l=ind; l<nRow; l++)
		{
			err = err + a.getVecEl(singular_vals_[j],l,m_theComm)*a.getVecEl(singular_vals_[j],l,m_theComm);
		}	

	}	

	err = sqrt(err);
}




void tensor::compute_greedy_errmem(const double& err_tol, double& err, int& mem, vector<unsigned int>& vals)
{

	
	vector<unsigned int> vecind(m_nVar,0);
	double errtot;
	getError(vecind, errtot);
	mem = 0;

	//cout << "err_tol = " << err_tol << " errtot in greedy init= " << errtot << endl;

	auxiliary z; 


	while (errtot> err_tol)
	{

		int kmax;
		double sigmamax;

		getLargestSingVal(vecind,kmax, sigmamax);
			
		if (vecind[kmax]==0)
		{

			mem = 0; 
			for (int j=0; j<m_nVar; j++)
			{	
				vecind[j] = 1;
				mem = mem + nDof_var(j);
			} 

			if (m_nVar>2)
			{
				mem = mem +1;

			}
				
							
		}
		else
		{


			if (m_nVar>2)
			{
				vecind[kmax] = vecind[kmax] +1; 

				mem = mem + nDof_var(kmax);
				int prod = 1;
				for (int j=0; j<m_nVar; j++)
				{
					
					if (j!=kmax)
					{
						prod = prod*vecind[j]; 
					}
				}
				mem = mem + prod;

			}
			else
			{
				vecind[0] = vecind[0]+1; 
				vecind[1] = vecind[1]+1;

				mem = mem + nDof_var(0) + nDof_var(1); 
			}

		}	


		double errnew;
		getError(vecind, errnew);
		//cout << "errnew = " << errnew << endl;
		//z.printStdVecInt(vecind, theComm);
		errtot = errnew;
	

	} 
	vals.resize(m_nVar); 
	for (int j=0; j<m_nVar; j++)
	{
		vals[j] = vecind[j];
	} 
	err = errtot;

	//cout << "err_tol = " << err_tol << " errtot in greedy = " << err << endl;


}

void tensor::scalarProdPureTens(const vector<Vec>& Rtab, double& scal)
{
	scal =0; 
        int Ntot = m_inc[m_nVar-1];
	for (int l = 0; l < Ntot; l++)
	{
		vector<unsigned int> indl = lin2sub(l); 
		double prod = 1; 
		for (int idVar = 0; idVar < m_nVar; idVar++)
		{
			prod = prod*a.getVecEl(Rtab[idVar], indl[idVar], m_theComm);
		}
		double res;
		eval(indl, res);
		scal = scal + prod*res;
	 }
}


