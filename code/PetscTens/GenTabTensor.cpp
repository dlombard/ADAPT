// Created by: Damiano & Virginie on ...
#include "GenTabTensor.h"

using namespace std;

// Constructor
GenTabTensor::GenTabTensor(const int& dim, const vector<int>& dim_vec, const vector< vector<int> >& indices, const Vec& coeff){

	num_var_ = dim;
	dim_var_ = dim_vec; 

	indices_  = indices; 
	coeffs_ = coeff; 
}

void GenTabTensor::eval(const vector<int>& vec_ind, double& res, MPI_Comm theComm){
	res = 0; 

	bool found_index = false; 
	int j=0; 

	while ( (!found_index) && ( j < indices_.size() ) )
	{
		bool equality = true; 
		for (int k=0; k<num_var_; k++)
		{
			equality = equality && (vec_ind[k] == indices_[j][k]);
		}
		if (equality){
			found_index = true;
		}
		j = j+1; 
	}
	j = j-1; 

	if (found_index) res = a.getVecEl(coeffs_, j, theComm);
	
}

