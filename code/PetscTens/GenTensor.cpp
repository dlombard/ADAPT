// Created by: Damiano & Virginie on ...
#include "GenTensor.h"


using namespace std;


// -- Constructors --

// GenTensor
GenTensor::GenTensor(const int& num_var, const vector<int>& dim_var){
	num_var_ = num_var;
	dim_var_ = dim_var;
}


// Compute Unfolding
vector<int> GenTensor::compute_multi_index(const int& index, const int& i, const int& j)
{

	int jcour = j;

	vector<int> vec_ind(num_var_);
	for (int k=0; k<num_var_; k++)
	{
		if (k==index)
		{
			vec_ind[index] = i; 
		}
		else
		{
			vec_ind[k] = jcour % dim_var_[k]; 
			jcour = (jcour - vec_ind[k])/dim_var_[k];
		}

	}

	return vec_ind; 
}


void GenTensor::computeUnfolding(const int& index, Mat& M, int& N, MPI_Comm theComm)
{

	int N1 = dim_var_[index]; 
	int N2 = 1; 

	for (int k=0; k<num_var_; k++)
	{
		if (k!=index)
		{
			N2 = N2*dim_var_[k]; 
		}
	}
	N = N2;

	a.initMat(M, N1, N2, theComm);

	for (int i=0; i<N1; i++)
	{

		for (int j=0; j<N2; j++)
		{
			double coeff = 0; 
			vector<int> vec_ind = compute_multi_index(index, i, j);
			eval(vec_ind, coeff, theComm);
			a.setMatEl(M,i,j, coeff, theComm);

		}
	}
	a.finalizeMat(M);

}

void GenTensor::compute_HOSVD(MPI_Comm theComm)
{

	singular_vals_.resize(num_var_); 
	singular_vecs_.resize(num_var_);

	//SVD svd;
	

	int N;

	for (int i=0; i<num_var_; i++)
	{



		Mat M; 
		Vec S;
		Mat U;
		Mat V;

		computeUnfolding(i,M,N,theComm);

		//MatView(M, PETSC_VIEWER_STDOUT_WORLD);

                //a.svdGK(M,U,V,S,theComm, EPSLAPACK);
		a.svdLHC(M,U,V,S,1e-10,theComm);

		//VecView(S, PETSC_VIEWER_STDOUT_WORLD);

		singular_vals_[i] = S; 
		singular_vecs_[i] = U; 

		//SVDDestroy(&svd);
		MatDestroy(&M);
		MatDestroy(&V);
		//VecDestroy(&u);
   		//VecDestroy(&v);
		//MatDestroy(&U);
		//VecDestroy(&S);

	}


}

void GenTensor::compute_HOSVD(const double& tol, MPI_Comm theComm)
{

	singular_vals_.resize(num_var_); 
	singular_vecs_.resize(num_var_);
	
	 
	
	
	int N;

	for (int i=0; i<num_var_; i++)
	{

		Mat M;
		Vec S;
		Mat U;
		Mat V;

		computeUnfolding(i,M,N,theComm);

		//MatView(M, PETSC_VIEWER_STDOUT_WORLD);

                //a.svdGK(M,U,V,S,theComm, EPSLAPACK);
		a.svdLHC(M,U,V,S,0.25*tol,theComm);

		//VecView(S, PETSC_VIEWER_STDOUT_WORLD);

		int vecSize;
  		VecGetSize(S, &vecSize);

		cout << "vecSize = " << vecSize << endl;

		singular_vals_[i] = S; 
		singular_vecs_[i] = U; 

		//SVDDestroy(&svd);
		MatDestroy(&M);
		MatDestroy(&V);
		//VecDestroy(&u);
   		//VecDestroy(&v);
		//MatDestroy(&U);
		//VecDestroy(&S);

	}


}



void GenTensor::getLargestSingVal(const vector<int>& vecind, int& jmax, double& sigmamax, MPI_Comm theComm)
{

	sigmamax = 0; 
	jmax = -1; 


	for (int j=0; j<num_var_; j++)
	{
		
		
		int ind = vecind[j];
		int nRow; 
		Vec S  = singular_vals_[j];
		//cout << "j = " << j << " ind = " << ind << endl; 
		//VecView(S, PETSC_VIEWER_STDOUT_WORLD);

		VecGetSize(S,&nRow);
		if ( (ind) < nRow)
		{
			if (a.getVecEl(S,ind,theComm)>sigmamax)
			{
				jmax = j;
				sigmamax = a.getVecEl(S,ind,theComm);  
			}

		}
		

	}	

	//cout << "sigmamax = " << endl;
}



void GenTensor::getError(const vector<int>& vecind, double& err, MPI_Comm theComm)
{
 
	err = 0; 

	for (int j=0; j<num_var_; j++)
	{
		
		int ind = vecind[j];
		int nRow; 
		VecGetSize(singular_vals_[j],&nRow);
	
		for (int l=ind; l<nRow; l++)
		{
			err = err + a.getVecEl(singular_vals_[j],l,theComm)*a.getVecEl(singular_vals_[j],l,theComm);
		}	

	}	

	err = sqrt(err);
}




void GenTensor::compute_greedy_errmem(const double& err_tol, double& err, int& mem, vector<int> vals, MPI_Comm theComm)
{

	
	vector<int> vecind(num_var_,0);
	double errtot;
	getError(vecind, errtot, theComm);
	mem = 0;

	//cout << "err_tol = " << err_tol << " errtot in greedy init= " << errtot << endl;

	auxiliary z; 


	while (errtot> err_tol)
	{

		int kmax;
		double sigmamax;

		getLargestSingVal(vecind,kmax, sigmamax, theComm);
			
		if (vecind[kmax]==0)
		{

			mem = 0; 
			for (int j=0; j<num_var_; j++)
			{	
				vecind[j] = 1;
				mem = mem + getVarDim(j);
			} 

			if (num_var_>2)
			{
				mem = mem +1;

			}
				
							
		}
		else
		{


			if (num_var_>2)
			{
				vecind[kmax] = vecind[kmax] +1; 

				mem = mem + getVarDim(kmax);
				int prod = 1;
				for (int j=0; j<num_var_; j++)
				{
					
					if (j!=kmax)
					{
						prod = prod*vecind[j]; 
					}
				}
				mem = mem + prod;

			}
			else
			{
				vecind[0] = vecind[0]+1; 
				vecind[1] = vecind[1]+1;

				mem = mem + getVarDim(0) + getVarDim(1); 
			}

		}	


		double errnew;
		getError(vecind, errnew, theComm);
		//cout << "errnew = " << errnew << endl;
		//z.printStdVecInt(vecind, theComm);
		errtot = errnew;
	

	}

	vals = vecind; 
	err = errtot;

	//cout << "err_tol = " << err_tol << " errtot in greedy = " << err << endl;


}

