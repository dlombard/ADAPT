// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>

#include "auxiliary.h"
#include "GenTensor.h"
#include "CoulombTensor.h"
#include "GibbsTensor.h"
#include "GenTabTensor.h"
#include "TuckerTensor.h"
#include "Node.h"
#include "SubTensor.h"
#include "ApproxTensor.h"

using namespace std;
int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";
    PetscErrorCode ierr;

    SlepcInitialize(&argc,&args,(char*)0,help);
    //PetscInitialize(&argc,&args,(char*)0,help);

    //ierr = SlepcInitialize(&argc,&args,(char*)0,help);

    int numProc;
    ierr = MPI_Comm_size(PETSC_COMM_WORLD,&numProc);
    //MPI_Comm_size(PETSC_COMM_WORLD,&numProc);
    int myId;
    MPI_Comm_rank (PETSC_COMM_WORLD, &myId);
  

    auxiliary z;


    int num = 2;  
    int n0 = 2048;
    int n1 = 2048;
    //int n2 = 256; 

    vector<int> vec_dim(2); 
    vec_dim[0] = n0; 
    vec_dim[1] = n1; 
    //vec_dim[2] = n2; 

    GibbsTensor* CT = new GibbsTensor(num, vec_dim); 

    vector<double> box_inf(num, -2); 
    vector<double> box_sup(num, 2); 

  
    double beta = 0.01; 
    vector<string> type_atoms(2); 

    type_atoms[0] = "N";
    type_atoms[1] = "O"; 
    //type_atoms[2] = "C"; 

    CT->init(box_inf, box_sup, type_atoms, beta); 


   /*double beta = 1; 
   vector<double> params(1); 
   params[0] = 1.0;

   vector< vector<double> > paramvec(1); 
   paramvec[0] = params;
    CT->init(box_inf, box_sup, "Quadratic", paramvec, beta); */

 

    vector<int> index(2); 
    index[0] = 0; 
    index[1] = 254; 
    //index[2] = 254;

    double res = 0; 
    CT->eval(index, res, PETSC_COMM_WORLD);

    cout << "res = " << res << endl; 

    index[0] = 0; 
    index[1] = 0; 
    //index[2] = 254;

    CT->eval(index, res, PETSC_COMM_WORLD);

    cout << "res = " << res << endl; 

    index[0] = 0; 
    index[1] = 253; 
    //index[2] = 254;

    CT->eval(index, res, PETSC_COMM_WORLD);

    cout << "res = " << res << endl; 
   
  vector< vector<int> > indices(2);

  for (int j=0; j<2; j++)
  {
	vector<int> vecnj(vec_dim[j]); 
	for (int k=0; k<vec_dim[j]; k++)
	{
		vecnj[k] = k; 
	}

	indices[j] = vecnj; 
  }

    Node* itr = NULL;

    Node* node = new Node(indices);

    node->createDyadicTree(7);

    Node* newnode = node->createNodeCopy(); 


    ApproxTensor approx2(newnode, CT, PETSC_COMM_WORLD);

   vector<double> errtab2 = approx2.getLeafErrors();
   cout << errtab2[0] << endl; 

   approx2.compute_greedy_errmem(1e-5,PETSC_COMM_WORLD);

   vector<double> errtabnew2 = approx2.getLeafErrors();
   cout << errtabnew2[0] << endl;

   //double memtot2 = n0*n1*n2;
   double memtot2 = n0*n1;
 
   double memgreed2 = approx2.compute_memtot();

   cout << "Mem Tot 2 = " << memtot2 << endl;
 
   cout << "Mem greedy 2 = " << memgreed2 << endl; 

   cout << "Gain 2 = " << memgreed2/memtot2 << endl;

   bool merge, merge2;



 string path = "/libre/ehrlachv/Tensor/ResGibbs_7_dim2_N2048";
   string path2 = "/libre/ehrlachv/Tensor/ResGibbs_7_dim2_N2048_beforemerge";
      approx2.getNode()->visualize(path2);
     approx2.writeErrors(path2);
  approx2.writeMemory(path2);
  approx2.writeSVDindices(path2);

 
   int countmerge, mem, meminit;
   approx2.MergeAsPossibleSecondStrategy(countmerge, meminit, mem, PETSC_COMM_WORLD);

   double memd = mem; 
   double memdd = meminit;

   cout << memdd << " " << memgreed2 << endl; 

    cout << "Old gain = " << memgreed2/memtot2 << endl;
   cout << mem << "  New gain = " << mem/memtot2 << endl;
	cout << mem << "  Relative gain = " << mem/memgreed2 << endl;





   ofstream myfile;
   string name = path+"_memorygain.txt";
   const char* name2 = name.c_str();  
    myfile.open(name2);

    myfile << "Memories: Total = " << memtot2 << "  Greedy = " << memgreed2 << "  Greedy Tree = " << endl; 

    myfile << "Old gain = " << memgreed2/memtot2 << endl;
    myfile << mem << "  New gain = " << mem/memtot2 << endl;
    myfile << mem << "  Relative gain = " << mem/memgreed2 << endl;


  myfile.close();	

   approx2.getNode()->visualize(path);
     approx2.writeErrors(path);
  approx2.writeMemory(path);
  approx2.writeSVDindices(path);


/*

 
   int countmerge, mem, meminit;
   approx2.MergeAsPossibleSecondStrategy(countmerge, meminit, mem, PETSC_COMM_WORLD);

   double memd = mem; 
   double memdd = meminit;

   cout << memdd << " " << memgreed2 << endl; 

    cout << "Old gain = " << memgreed2/memtot2 << endl;
   cout << mem << "  New gain = " << mem/memtot2 << endl;
	cout << mem << "  Relative gain = " << mem/memgreed2 << endl;

   string path = "/home/ehrlache/ADAPT/code/Results/ResGibbs_4_dim2_N1024";


   ofstream myfile;
   string name = path+"_memorygain.txt";
   const char* name2 = name.c_str();  
    myfile.open(name2);

    myfile << "Memories: Total = " << memtot2 << "  Greedy = " << memgreed2 << "  Greedy Tree = " << endl; 

    myfile << "Old gain = " << memgreed2/memtot2 << endl;
    myfile << mem << "  New gain = " << mem/memtot2 << endl;
    myfile << mem << "  Relative gain = " << mem/memgreed2 << endl;


  myfile.close();	

   approx2.getNode()->visualize(path);
     approx2.writeErrors(path);
  approx2.writeMemory(path);
  approx2.writeSVDindices(path);*/

    ierr = PetscFinalize();
    return 0;
}
