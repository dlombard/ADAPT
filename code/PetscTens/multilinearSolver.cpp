// Implementation of the class multilinearSolver


#include "multilinearSolver.h"
#include<chrono>
#include<ctime>
using namespace std;



/* Conjugate Gradient solvers
  - Input: operator tensor, rhs, tolerance
  - Output: solution
    REMARK: in this function there is no initial guess: implicitly set to 0.
*/

void multilinearSolver::CG(operatorTensor& A, CPTensor& rhs, CPTensor& sol, double tol){

  unsigned int nVar = rhs.nVar();
  vector<unsigned int> dofPerDim = rhs.nDof_var();
  MPI_Comm theComm = rhs.theComm();


  // Initialising the solution fields:
  sol.set_nVar(nVar);
  sol.set_nDof_var(dofPerDim);
  sol.setTensorComm(theComm);


  // Creating residual andconjugate direction tensors:
  CPTensor residual;
  CPTensor conjDir;
  residual.copyTensorFrom(rhs);
  conjDir.copyTensorFrom(rhs);

  // compute the norm of the residual
  double norm2Res = residual.norm2CP();
  PetscPrintf(rhs.theComm(), "Residual norm = %e \n", sqrt(norm2Res));

  while(sqrt(norm2Res)>tol){

    double alphaNum = norm2Res;
    CPTensor krilovTens;
    A.applyOperator(conjDir, krilovTens);
    double alphaDen = conjDir.scalarProd(krilovTens); 
    double alpha = alphaNum/alphaDen;


    // solution update (copy the terms conjDir will be destroyed);
    sol.sum(conjDir, alpha, true); 
    residual.sum(krilovTens, -alpha, true); 

    norm2Res = residual.norm2CP();
    PetscPrintf(rhs.theComm(), "Residual norm = %e \n", sqrt(norm2Res));

    // updating the conjugate direction
    double betaDen = alphaNum;
    double betaNum = norm2Res;
    double beta = betaNum/betaDen;

    conjDir.multiplyByScalar(beta);
    conjDir.sum(residual, 1.0, true);
    krilovTens.clear();

  }

  conjDir.clear();
  residual.clear();
}


void multilinearSolver::CG_compress(operatorTensor& A, CPTensor& rhs, CPTensor& sol, double tol){

  unsigned int nVar = rhs.nVar();
  vector<unsigned int> dofPerDim = rhs.nDof_var();
  MPI_Comm theComm = rhs.theComm();

    vector<Mat> Idvec(nVar);
    for (int k=0; k<nVar; k++)
    {
	Mat Idk = a.eye(dofPerDim[k],theComm); 
	Idvec[k] = Idk;
	
    }

  // Initialising the solution fields:
  sol.set_nVar(nVar);
  sol.set_nDof_var(dofPerDim);
  sol.setTensorComm(theComm);


  // Creating residual andconjugate direction tensors:
  CPTensor residual;
  CPTensor conjDir;
  residual.copyTensorFrom(rhs);
  conjDir.copyTensorFrom(rhs);

  // compute the norm of the residual
  double norm2Res = residual.norm2CP();
  PetscPrintf(rhs.theComm(), "Residual norm = %e \n", sqrt(norm2Res));

  while(sqrt(norm2Res)>tol){

    double alphaNum = norm2Res;
    CPTensor krilovTens;
    A.applyOperator(conjDir, krilovTens);
    double alphaDen = conjDir.scalarProd(krilovTens); 
    double alpha = alphaNum/alphaDen;


    // solution update (copy the terms conjDir will be destroyed);
    sol.sum(conjDir, alpha, true); 


    //Attention, on a rajouté le greedy
    CPTensor sol2(nVar, dofPerDim, theComm);
    sol2.greedy_ApproxOf(sol, Idvec, 0.1*tol, 0.01*tol);
    sol.clear(); 
    sol.copyTensorFrom(sol2); 
    sol2.clear(); 
    
    CPTensor temp; 
    A.applyOperator(sol, temp);
    residual.clear(); 
    residual.copyTensorFrom(rhs); 
    residual.sum(temp,-1.0,true);
    temp.clear();


    /////////////////////
    //residual.sum(krilovTens, -alpha, true); 

    norm2Res = residual.norm2CP();
    PetscPrintf(rhs.theComm(), "Residual norm = %e \n", sqrt(norm2Res));



    // updating the conjugate direction
    double betaDen = alphaNum;
    double betaNum = norm2Res;
    double beta = betaNum/betaDen;

    conjDir.multiplyByScalar(beta);
    conjDir.sum(residual, 1.0, true);
    krilovTens.clear();

  }

  conjDir.clear();
  residual.clear();
}





/* greedy ALS to solve a linear problem
  - inputs: the tensor to be approximated, the mass matrices, the error
  - output: this tensor is the greedy approximation
*/
void multilinearSolver::greedy_ALS(operatorTensor& A, CPTensor& rhs, CPTensor& sol, const double tol, const double tolFP = 1.0e-2){


   
  unsigned int m_nVar = rhs.nVar();
  vector<unsigned int> m_nDof_var = rhs.nDof_var();
  MPI_Comm m_theComm = rhs.theComm();


  // Initialising the solution fields:
  sol.set_nVar(m_nVar);
  sol.set_nDof_var(m_nDof_var);
  sol.setTensorComm(m_theComm);

  // define tolerance for Fix Point
  const double errFP = tolFP * tol;



  // init residual
  CPTensor residual;
  residual.copyTensorFrom(rhs);
  double norm2Res = residual.norm2CP();

  while(sqrt(norm2Res) > tol){




    PetscPrintf(m_theComm,"residual rank = %d\n", residual.rank());
    PetscPrintf(m_theComm,"residual norm = %f\n", residual.norm2CP());


    // init random terms
    vector<Vec> termsToAdd(m_nVar);

    PetscRandom rctx;
    PetscRandomCreate(m_theComm,&rctx);
 PetscRandomSetSeed(rctx,std::chrono::system_clock::now().time_since_epoch().count());
    PetscRandomSeed(rctx);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      a.initVec(termsToAdd[idVar], m_nDof_var[idVar], m_theComm);
      a.finalizeVec(termsToAdd[idVar]);
      VecSetRandom(termsToAdd[idVar],rctx);
    }

    // copy for fix point check
    vector<Vec> termsToAdd_old(m_nVar);
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      Vec thisTerm;
      a.initVec(thisTerm, m_nDof_var[idVar], m_theComm);
      VecCopy(termsToAdd[idVar], thisTerm);
      a.finalizeVec(thisTerm);
      termsToAdd_old[idVar] = thisTerm;
    }
    // starting Fix-Point for ALS iteration
    double testFP = 1e12*errFP;
    while(testFP > errFP){

 
      for(unsigned int iVar=0; iVar<m_nVar; iVar++){

	Mat Ai; 
        a.initMat(Ai, m_nDof_var[iVar], m_nDof_var[iVar], m_theComm);
        a.finalizeMat(Ai); 
        
      for (unsigned int kTerm=0; kTerm<A.numTerms(); kTerm++)
      {
	double prod = 1.0;
        for(unsigned int jVar=0; jVar<m_nVar; jVar++){
          if(jVar != iVar){
            Vec prodJ;
            a.initVec(prodJ, m_nDof_var[jVar], m_theComm);
            a.finalizeVec(prodJ);
            MatMult(A.op(kTerm,jVar), termsToAdd[jVar], prodJ);
            double quadForm;
            VecDot(prodJ, termsToAdd[jVar], &quadForm);
            prod = prod * quadForm;
            VecDestroy(&prodJ);
          }
        }
	MatAXPY(Ai, prod, A.op(kTerm,iVar), DIFFERENT_NONZERO_PATTERN); 
      }
      a.finalizeMat(Ai); 
       KSP iKsp;
       a.initLinSolve(iKsp, Ai, m_theComm);


        Vec rhsi;
        a.initVec(rhsi, m_nDof_var[iVar], m_theComm);
        a.finalizeVec(rhsi);

        for(unsigned int kTerm=0; kTerm<residual.rank(); kTerm++){
          double prodK = 1.0;
          for(unsigned int jVar=0; jVar<m_nVar; jVar++){
            if(jVar != iVar){
              double quadForm;
              VecDot(termsToAdd[jVar], residual.terms(kTerm,jVar), &quadForm);
              prodK = prodK * quadForm;
            }
          }
          prodK = prodK * residual.coeffs(kTerm);
          VecAXPY(rhsi, prodK, residual.terms(kTerm,iVar));
        }

        // solve linear System
        a.solveLin(iKsp, rhsi, termsToAdd[iVar], m_theComm, false);
        VecDestroy(&rhsi);
        MatDestroy(&Ai);
        KSPDestroy(&iKsp);
      }
      CPTensor thisPureTens(termsToAdd, m_theComm);
      CPTensor oldPureTens(termsToAdd_old, m_theComm);
      oldPureTens.sum(thisPureTens,-1.0, true);
      double diffNorm2 = oldPureTens.norm2CP();


      // copy termsToAdd
      for(unsigned int idVar=0; idVar<m_nVar; idVar++){
        VecDestroy(&termsToAdd_old[idVar]);
        Vec thisTerm;
        a.initVec(thisTerm, m_nDof_var[idVar], m_theComm);
        VecCopy(termsToAdd[idVar], thisTerm);
        a.finalizeVec(thisTerm);
        termsToAdd_old[idVar] = thisTerm;
      }
      testFP = sqrt(diffNorm2);
      PetscPrintf(m_theComm, "testFP = %e \n", testFP);
    }
    PetscPrintf(m_theComm, "\n", testFP);

    // update the result:
    sol.addPureTensorTerm(termsToAdd, 1.0);
    PetscPrintf(m_theComm,"Sol rank = %d\n",sol.rank());

    // update residual:
    CPTensor temp(termsToAdd, m_theComm); 
    CPTensor Atemp; 
    A.applyOperator(temp, Atemp);  
    residual.sum(Atemp, -1.0, true);
    temp.clear(); 
    Atemp.clear(); 

    PetscRandomDestroy(&rctx);

    norm2Res = residual.norm2CP();
    PetscPrintf(m_theComm, "Norm2res = %e \n", norm2Res);
    PetscPrintf(m_theComm, "\n", testFP);
    PetscPrintf(m_theComm, "\n", testFP);

    // optimise the coefficients:
    /*Mat scalMat;
    a.initMat(scalMat,m_rank,m_rank,m_theComm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      for(unsigned int jTerm=iTerm; jTerm<m_rank; jTerm++){
        double val = scalarProdTerms(m_terms[iTerm], m_terms[jTerm], mass);
        a.setMatEl(scalMat,iTerm,jTerm,val,m_theComm);
        if(iTerm != jTerm){
          a.setMatEl(scalMat,jTerm,iTerm,val,m_theComm);
        }
      }
    }
    a.finalizeMat(scalMat);

    Vec scalVec;
    a.initVec(scalVec,m_rank, m_theComm);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double entry = 0.0;
      for(unsigned int kTerm=0; kTerm<toBeApprox.rank(); kTerm++){
        vector<Vec> kTermVec = toBeApprox.terms(kTerm);
        double val = scalarProdTerms(m_terms[iTerm], kTermVec, mass);
        val = val * toBeApprox.coeffs(kTerm);
        entry = entry + val;
      }
      a.setVecEl(scalVec, iTerm, entry, m_theComm);
    }
    a.finalizeVec(scalVec);

    Vec updateCoeffs;
    a.initVec(updateCoeffs, m_rank, m_theComm);
    a.finalizeVec(updateCoeffs);

    KSP scalSolver;
    a.initLinSolve(scalSolver, scalMat, m_theComm);
    a.solveLin(scalSolver, scalVec, updateCoeffs, m_theComm, false);
    for(unsigned int iTerm=0; iTerm<m_rank; iTerm++){
      double thisCoeff = a.getVecEl(updateCoeffs, iTerm, m_theComm);
      m_coeffs[iTerm] = thisCoeff;
      residual.set_coeffs(residual.rank() - m_rank + iTerm, -1.0*thisCoeff);
    }

    VecDestroy(&scalVec);
    VecDestroy(&updateCoeffs);
    MatDestroy(&scalMat);
    KSPDestroy(&scalSolver);

    norm2Res = residual.norm2CP(mass);
    PetscPrintf(m_theComm, "Corrected Norm2res = %e \n", norm2Res);
    PetscPrintf(m_theComm, "\n", testFP);
    PetscPrintf(m_theComm, "\n", testFP);
    if(!m_CPName.empty()){
      outfile << norm2Res << flush << endl;
    }*/
  }

}






// Solving with CPTT

/*void multilinearSolver::solvewithCPTT(operatorTensor& A, CPTensor& F, CPTensor& sol, double tol){
// Initialise residual R=F:
CPTensor R;
R.copyTensorFrom(F);
ofstream outfile;
if(!sol.CPName().empty()){
  string fileName 
= string("residual_") + sol.CPName() + string(".txt");
  outfile.open(fileName.c_str());
}

// Computing the Adjoint of the operator:
operatorTensor adjA = A.adjoint();

// Initialising an empty solution:
sol.setTensorComm(F.theComm());
sol.set_nVar(F.nVar());
vector<unsigned int> resPerDim = F.nDof_var();
sol.set_nDof_var(resPerDim);

// start looping
double residualNorm = sqrt(R.norm2CP());

PetscPrintf(PETSC_COMM_WORLD, "Residual norm %f \n", residualNorm);


unsigned int it = 0;
while ( residualNorm > tol) {

  CPTensor t(R.nVar(), R.nDof_var(), R.theComm());
  t.compute_CPTT_Term(R);
  cout << "t computed" << endl;
  cout << t.coeffs(0) << endl;


  CPTensor y = A.applyOperator(R);

  CPTensor w = A.applyOperator(t);

  double T1= y.scalarProd(w);
  cout << "T1 = " << T1 << endl;


  CPTensor RtoBeApprox;
  if(T1<0){
    RtoBeApprox.copyTensorFrom(R);
    RtoBeApprox.sum(t,-1.0, true);
  }
  while (T1 < 0) {
    cout << "entering the loop" << endl;
    CPTensor tToAdd(R.nVar(), R.nDof_var(), R.theComm());
    tToAdd.compute_CPTT_Term(RtoBeApprox);

    CPTensor wToAdd = A.applyOperator(tToAdd);
    w.sum(wToAdd, 1.0, true);
    t.sum(tToAdd, 1.0, false);
    T1 = T1 + y.scalarProd(wToAdd);
    RtoBeApprox.sum(tToAdd,-1.0, true);
  }
  // compute T2 =  <AA^* t, AA^* t>;
  //CPTensor update = adjA.applyOperator(t);
  CPTensor update;
  adjA.applyOperator(t, update);

  //CPTensor z=A.applyOperator(update);
  CPTensor z;
  A.applyOperator(update, z);

  double T2 = z.norm2CP();
  cout << "T2 = " << T2 << endl;
  cout << "alpha = " << T1/T2 << endl;

  // free the memory:
  w.clear();
  y.clear();
  z.clear();

  // exact line search:
  double alpha = T1 / T2;

  cout << "BEFORE UPDATE" << endl;
  for (unsigned int iTerm=0; iTerm < sol.rank(); iTerm++) {
    //for (unsigned int iVar = 0; iVar < sol.nVar(); iVar++) {
    //  VecView(sol.terms(iTerm, iVar), PETSC_VIEWER_STDOUT_WORLD);
    //}
    //cout << sol.coeffs(iTerm)<<endl;
  }
  cout << endl;




  // update the solution:
  sol.sum(update, alpha, false);
  //update.clear();

  cout << "AFTER UPDATE" << endl;

  for (unsigned int iTerm=0; iTerm < sol.rank(); iTerm++) {
    //for (unsigned int iVar = 0; iVar < sol.nVar(); iVar++) {
    //  VecView(sol.terms(iTerm, iVar), PETSC_VIEWER_STDOUT_WORLD);
    //}
    //cout << sol.coeffs(iTerm) << endl;
  }
  cout << endl;

  //if(it == 5){exit(1);}


  // renormalise the terms
  double exponent = 1.0/static_cast<double>(sol.nVar());
  for(unsigned int iTerm=0; iTerm<sol.rank(); iTerm++){
    double c_i = sol.coeffs(iTerm);
    double sign = 1;
    if(c_i<0.0){
      c_i = -1.0*c_i;
      sign = -1.0;
    }
    double scaling = pow(c_i, exponent);
    for(unsigned int iVar=0; iVar<sol.nVar(); iVar++){
      sol.rescaleTerm(iTerm, iVar, scaling);
    }
    sol.set_coeffs(iTerm, sign);
  }



  cout << "Sol computed" << endl;



  //CPTensor AsolUpdated = A.applyOperator(sol);
  CPTensor AsolUpdated;
  A.applyOperator(sol, AsolUpdated);

  R.clear();
  R.copyTensorFrom(F);
  R.sum(AsolUpdated,-1.0, false);
  residualNorm = sqrt(R.norm2CP());
  PetscPrintf(PETSC_COMM_WORLD, "Residual norm %f \n", residualNorm);
  if(!sol.CPName().empty()){
    outfile << residualNorm << flush << endl;
  }
  it = it + 1;


}
if(!sol.CPName().empty()){
  outfile.close();
}

}*/


/* CPTT and conjugate gradient method
  - Input: operator tensor, rhs, tolerance
  - Output: solution
    REMARK: in this function there is no initial guess: implicitly set to 0.
  - 2 inner iterations of CG
*/
/*void multilinearSolver::solveCPTT_CG(operatorTensor& A, CPTensor& F, CPTensor& sol, double tol){
    unsigned int nVar = F.nVar();
    vector<unsigned int> dofPerDim = F.nDof_var();
    MPI_Comm theComm = F.theComm();

    // Initialising the solution fields:
    sol.set_nVar(nVar);
    sol.set_nDof_var(dofPerDim);
    sol.setTensorComm(theComm);

    // Copy the right hand side:
    // Initialise residual R=F:
    CPTensor R;
    R.copyTensorFrom(F);

    // compute the residual norm:
    double norm2Res = R.norm2CP();
    double residualNorm = sqrt(norm2Res);
    PetscPrintf(F.theComm(), "Intial Residual norm = %e \n\n", residualNorm);
    if(residualNorm<tol){
      PetscPrintf(F.theComm(), "Best solution is 0, nothing to be done \n");
      return;
    }


    while(residualNorm>tol){
      // First step: compress R and retrieve one CPTT term
      CPTensor compF;
      compF.CPTT_ApproxOf(R,residualNorm*0.99, false);
      R.sum(compF, -1.0, true);
      PetscPrintf(F.theComm(), "Rank of compF = %d \n", compF.rank());

      // Second step: compute the CG approximation.
      PetscPrintf(F.theComm(), "Entering CG step...\n");
      CPTensor CGsol(nVar, dofPerDim, theComm);
      CPTensor CGResidual;
      CPTensor conjDir;
      CGResidual.copyTensorFrom(compF);
      conjDir.copyTensorFrom(compF);

      // compute the norm of the residual
      double CGnorm2Res = CGResidual.norm2CP();
      const double initialNorm = sqrt(CGnorm2Res);
      PetscPrintf(F.theComm(), "CG Residual norm = %e \n", sqrt(CGnorm2Res));

      unsigned int it=0;
      while(sqrt(CGnorm2Res)>1.0e-4*initialNorm && (it<2)){
        double alphaNum = CGnorm2Res;
        CPTensor krilovTens;
        A.applyOperator(conjDir, krilovTens);
        double alphaDen = conjDir.scalarProd(krilovTens);
        double alpha = alphaNum/alphaDen;


        // solution update (copy the terms conjDir will be destroyed);
        CGsol.sum(conjDir, alpha, true);
        CGResidual.sum(krilovTens, -alpha, true);

        CGnorm2Res = CGResidual.norm2CP();
        PetscPrintf(F.theComm(), "CG Residual norm = %e \n", sqrt(CGnorm2Res));

        // updating the conjugate direction
        double betaDen = alphaNum;
        double betaNum = norm2Res;
        double beta = betaNum/betaDen;

        conjDir.multiplyByScalar(beta);
        conjDir.sum(CGResidual, 1.0, true);
        krilovTens.clear();
        it +=1;
      }

      conjDir.clear();
      CGResidual.clear();



      PetscPrintf(F.theComm(), "Done, solution rank = %d\n", CGsol.rank());

      // sum this sol to CGSol;
      sol.sum(CGsol,1.0,true);
      CGsol.clear();
      compF.clear();

      // compute the new residual:
      CPTensor AsolUpdated;
      A.applyOperator(sol, AsolUpdated);

      R.clear();
      R.copyTensorFrom(F);
      R.sum(AsolUpdated,-1.0, false);
      residualNorm = sqrt(R.norm2CP());
      PetscPrintf(F.theComm(), "Residual norm at the end of the Iteration  = %e \n\n", residualNorm);
    }

}*/



/* CPTT and bi-conjugate gradient stabilized method
  - Input: operator tensor, rhs, tolerance
  - Output: solution
    REMARK: in this function there is no initial guess: implicitly set to 0.
  - 2 inner iterations of BiCGStab (unpreconditionned)
*/
/*void multilinearSolver::solveCPTT_BiCGStab(operatorTensor& A, CPTensor& F, CPTensor& sol, double tol){
    unsigned int nVar = F.nVar();
    vector<unsigned int> dofPerDim = F.nDof_var();
    MPI_Comm theComm = F.theComm();

    // Initialising the solution fields:
    sol.set_nVar(nVar);
    sol.set_nDof_var(dofPerDim);
    sol.setTensorComm(theComm);

    // Copy the right hand side:
    // Initialise residual R=F:
    CPTensor R;
    R.copyTensorFrom(F);

    // compute the residual norm:
    double norm2Res = R.norm2CP();
    double residualNorm = sqrt(norm2Res);
    PetscPrintf(F.theComm(), "Intial Residual norm = %e \n\n", residualNorm);
    if(residualNorm<tol){
      PetscPrintf(F.theComm(), "Best solution is 0, nothing to be done \n");
      return;
    }


    while(residualNorm>tol){
      // First step: compress R and retrieve one CPTT term
      CPTensor compF;
      compF.CPTT_ApproxOf(R,residualNorm*0.99, false);
      R.sum(compF, -1.0, true);
      PetscPrintf(F.theComm(), "Rank of compF = %d \n", compF.rank());

      // Second step: compute the Bi-CGStab approximation.
      PetscPrintf(F.theComm(), "Entering BiCG step...\n");
      CPTensor CGsol(nVar, dofPerDim, theComm);
      CPTensor CGResidual;
      CPTensor conjDir;
      CPTensor krilovTens;
      CPTensor initRes;
      CGResidual.copyTensorFrom(compF);
      conjDir.copyTensorFrom(compF);
      krilovTens.copyTensorFrom(compF);
      initRes.copyTensorFrom(compF);
      double alpha = 1.0;
      double omega = 1.0;
      double rho_0 = 1.0;
      double rho_i = 1.0;

      // compute the norm of the residual
      double CGnorm2Res = CGResidual.norm2CP();
      const double initialNorm = sqrt(CGnorm2Res);

      PetscPrintf(F.theComm(), "BiCG Residual norm = %e \n", sqrt(CGnorm2Res));

      unsigned int it=0;
      while(sqrt(CGnorm2Res)>1.0e-4*initialNorm && (it<2)){
        rho_0 = rho_i;
        rho_i = initRes.scalarProd(CGResidual);
        double beta = (rho_i/rho_0) * (alpha/omega);

        if(it>0){
          conjDir.sum(krilovTens,-omega,true);
          conjDir.multiplyByScalar(beta);
          conjDir.sum(CGResidual, 1.0, true);
        }
        krilovTens.clear();
        A.applyOperator(conjDir, krilovTens);

        double alphaDen = initRes.scalarProd(krilovTens);
        alpha = rho_i / alphaDen;
        CGsol.sum(conjDir, alpha, true);

        CPTensor sTens;
        sTens.copyTensorFrom(CGResidual);
        sTens.sum(krilovTens, -alpha, true);

        CPTensor tTens;
        A.applyOperator(sTens);

        double omega_num = tTens.scalarProd(sTens);
        double omega_den = tTens.norm2CP();
        if(omega_den<1.0e-12){
          omega = 0.01;
        }
        else{
          omega = omega_num/omega_den;
        }


        CGsol.sum(sTens, omega, true);
        CGResidual.clear();
        CGResidual.copyTensorFrom(sTens);
        CGResidual.sum(tTens, -omega, true);

        CGnorm2Res = CGResidual.norm2CP();
        PetscPrintf(F.theComm(), "BiCG Residual norm = %e \n", sqrt(CGnorm2Res));

        sTens.clear();
        tTens.clear();

        it +=1;
      }
      initRes.clear();
      conjDir.clear();
      CGResidual.clear();



      PetscPrintf(F.theComm(), "Done, solution rank = %d\n", CGsol.rank());

      // sum this sol to CGSol;
      sol.sum(CGsol,1.0,true);
      CGsol.clear();
      compF.clear();

      // compute the new residual:
      CPTensor AsolUpdated;
      A.applyOperator(sol, AsolUpdated);

      R.clear();
      R.copyTensorFrom(F);
      R.sum(AsolUpdated,-1.0, false);
      residualNorm = sqrt(R.norm2CP());
      PetscPrintf(F.theComm(), "Residual norm at the end of the Iteration  = %e \n\n", residualNorm);
    }

}*/
