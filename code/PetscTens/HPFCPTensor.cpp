// Created by: Damiano & Virginie on ...
#include "HPFCPTensor.h"

using namespace std;

// Constructor
HPFCPTensor::HPFCPTensor(Node* node, const vector<CPTensor>& vec, MPI_Comm theComm){
  m_node = node;
  m_CPvec = vec;

  m_theComm = theComm;


  vector< vector<unsigned int> > indices  = m_node->getIndicesFromLeafs();
  m_nVar = indices.size();
  vector<unsigned int> dim(m_nVar);
  for (int idVar=0; idVar < m_nVar; idVar++)
  {
	dim[idVar] = indices[idVar].size();
  } 
   
  m_nDof_var =dim;
  compute_minc(); 
}


HPFCPTensor::HPFCPTensor(ApproxTensor* app, bool comp_HOSVD = true){

  cout << "Constructing HPFCP" << endl; 
  m_node = app->getNode();
  tensor* tens = app->getTensor();
  m_nVar = tens->nVar();
  m_nDof_var = tens->nDof_var();

     cout << m_nVar << endl; 

  auxiliary a;


  m_theComm = tens->theComm();
  compute_minc(); 

  vector<Node*> leafs = m_node->getLeafs();
  vector<CPTensor> vecCP(leafs.size());
  if (comp_HOSVD)
  { 
  	app->compute_local_HOSVD(); 
  }

  vector<SubTensor*> subtenslist = app->getSubTensors();
  vector< vector<unsigned int> > svd_indices = app->getLeafSVDIndices();

  cout << svd_indices.size() << endl; 
  cout << svd_indices[0].size() << endl; 
  cout << svd_indices[0][0] << endl; 

  for (int k=0; k<leafs.size(); k++)
  {
	cout << "k = " << k << endl; 
	SubTensor* subk = subtenslist[k];
        vector<unsigned int> indk = svd_indices[k];
	cout << "nVar = " << subk->nVar() << endl; 

	if (m_nVar>2)
	{
		 
	}
	else
	{
		// Pas optimal : à changer!!!
		unsigned int rank = indk[0];
		cout << "rank = " << rank << endl; 
		Mat vec0 = subk->getSingVecs(0); 
		cout << "Mat collected" << endl; 
		Mat vec1 = subk->getSingVecs(1);

		Vec S = subk->getSingVals(0);

		cout << "Mat and S collected" << endl; 

		vector <vector<Vec> > vecterms(rank); 
		vector<double> coeffs(rank); 

		for (int i=0; i<rank; i++)
		{ 
			cout << "i = " << i << endl; 	
			vector<Vec> vec;
			vec.resize(2); 
			Vec A,B; 
			a.initVec(A,subk->nDof_var(0), m_theComm);
			a.initVec(B,subk->nDof_var(1), m_theComm);

			MatGetColumnVector(vec0,A,i);
			MatGetColumnVector(vec1,B,i);
			vec[0] = A; 
			vec[1] = B;
			vecterms[i] = vec; 

			//Première possibilité
			//double res;
			//scalarProdPureTens(vec, res);
			//coeffs[i] = res;

			//Deuxième possibilité
			coeffs[i] = a.getVecEl(S,i, m_theComm);

			
		}

		CPTensor cptens(vecterms, coeffs, m_theComm);
		vecCP[k]= cptens;
               
	}
  }
  
  m_CPvec = vecCP;

}


void HPFCPTensor::eval(const vector<unsigned int>& vec_ind, double& res){

	Node* leaf;
	vector<unsigned int> new_vec_ind;
	m_node->whichLeaf(vec_ind, leaf, new_vec_ind);

	vector< Node*> leafs = m_node->getLeafs();

	int pos;
	m_node->isIn(leaf, leafs, pos);

	CPTensor localtens = m_CPvec[pos]; 

	localtens.eval(new_vec_ind, res);
}


CPTensor HPFCPTensor::CP_From_HPFCP(){

	vector<vector<Vec> > theTerms; 
	vector<double> theCoeffs; 

	//Calcul du rang
	int rank =0; 
	vector<Node*> leafs = m_node->getLeafs();
	for (int k=0; k<leafs.size(); k++)
	{
		CPTensor tensk = m_CPvec[k];
		rank = rank + tensk.rank();
	}
	theTerms.resize(rank); 
	theCoeffs.resize(rank); 
	int ind = 0; 
	for (int k=0; k<leafs.size(); k++)
	{
		CPTensor tensk = m_CPvec[k];
		vector< vector<unsigned int> > indicesk = leafs[k]->getIndices();
		for (int l=0; l<tensk.rank(); l++)
		{
			vector<Vec> vecindl(m_nVar);
			for (int idVar = 0; idVar<m_nVar; idVar++)
			{
				vector<unsigned int> indicesk_idVar = indicesk[idVar]; 
				unsigned int deb = indicesk_idVar[0]; 
				unsigned int len = indicesk_idVar.size();

				Vec term_l_idVar = tensk.terms(l,idVar);
				Vec newterm_l_idvar; 
				a.initVec(newterm_l_idvar, m_nDof_var[idVar] , m_theComm);
				for (int t=0; t<len;t++)
				{
					double val= a.getVecEl(term_l_idVar,t, m_theComm);		
					a.setVecEl(newterm_l_idvar, deb+t, val, m_theComm);
				}
				a.finalizeVec(newterm_l_idvar);
				vecindl[idVar] = newterm_l_idvar;

			}
			
			theTerms[ind+l] = vecindl;
			double coeffindl = tensk.coeffs(l);
			theCoeffs[ind+l] = coeffindl;
		}
		ind = ind + tensk.rank();
	}


	CPTensor CPtens(theTerms, theCoeffs,m_theComm); 

	return CPtens; 

	
}

