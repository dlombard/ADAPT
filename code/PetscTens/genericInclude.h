// generic include header file


#ifndef genericInclude_h
#define genericInclude_h

#include <stdio.h>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ostream>
#include <string>
#include <vector>
#include <map>
#include <complex>
#include <math.h>
#include <ios>
#include <algorithm>
#include <random>
#include <mpi.h>
#include <petscksp.h>
#include <slepceps.h>
#include <slepcsvd.h>
#include <slepc.h>

#endif
