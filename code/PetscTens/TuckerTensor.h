/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/

#ifndef TuckerTensor_h
#define TuckerTensor_h

#include "genericInclude.h"
#include "auxiliary.h"
#include "GenTensor.h"
#include "GenTabTensor.h"



using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:


class TuckerTensor: public GenTensor{

	private:

	GenTabTensor coeffs_;
	vector<Mat> basis_functions_;
	vector<int> ranks_;

	public: 

        TuckerTensor(){};
  	~TuckerTensor(){};

      	//Overloaded constructors
	TuckerTensor(const GenTabTensor&, const vector<Mat>&);

	void eval(const vector<int>&, double&, MPI_Comm);
	vector<int> compute_index_rank(const int&);

};


#endif



