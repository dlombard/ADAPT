// Auxiliary File implementing Matrix / Vector operations and converters for a basis usage

#ifndef auxiliary_h
#define auxiliary_h

#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include "genericInclude.h"

using namespace std;

class auxiliary{
private:

public:
  auxiliary(){};
  ~auxiliary(){};

/* 0 -- MPI, vector<double> generic routines -- */

// 0.1 -- findNonZero indices of a vector:
vector<int> findNonZero(vector<double>& v){
    const unsigned int vecSize = v.size();
    vector<int> pattern;
    for(unsigned int h=0; h<vecSize; h++){
        if(v[h] != 0.0){
            pattern.push_back(h);
        }
    }
    return pattern;
}


// 0.2 -- reduce a vector to its non-zero elements:
vector<double> nonZero(vector<double>& v){
    const unsigned int vecSize = v.size();
    vector<double> toBeReturned;
    for(unsigned int h=0; h<vecSize; h++){
        if(v[h] != 0.0){
            toBeReturned.push_back(v[h]);
        }
    }
    return toBeReturned;
}


//0.3 converting vector<int> into PetscInt array:
void vector2PetscInt(vector<int> vIn, PetscInt* array){
    for(unsigned int j=0; j<vIn.size(); j++){
        array[j] = vIn[j];
    }
}

// 0.4 converting vector<double> into PetscScalar array:
void vector2PetscScalar(vector<double> vIn, PetscScalar* array){
    for(unsigned int j=0; j<vIn.size(); j++){
        array[j] = vIn[j];
    }
}

// 0.5 converting a PetscInt array into vector<int>
void PetscInt2vector(const PetscInt* array, vector<int>& vOut ){
  for(unsigned int j=0; j<vOut.size(); j++){
      vOut[j] = array[j];
  }
}

// 0.6 converting a PetscScalar array into a vector<double>
void PetscScalar2vector(const PetscScalar* array, vector<double>& vOut ){
  for(unsigned int j=0; j<vOut.size(); j++){
      vOut[j] = array[j];
  }
}


// 0.7 print std vector on proc 0:
void printStdVec(vector<double>& toBePrinted, MPI_Comm theComm){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  if(rank==0){
    unsigned int theVecSize = toBePrinted.size();
    for(unsigned int j=0; j<theVecSize; j++){
      std::cout << "v(" << j << ") = " << toBePrinted[j] << endl;
    }
    std::cout << endl;
  }
}

// 0.7 print std vector on proc 0:
void printStdVecInt(vector<int>& toBePrinted, MPI_Comm theComm){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  if(rank==0){
    unsigned int theVecSize = toBePrinted.size();
    for(unsigned int j=0; j<theVecSize; j++){
      std::cout << "v(" << j << ") = " << toBePrinted[j] << endl;
    }
    std::cout << endl;
  }
}

// get the largest element (local on the proc)
void getLargestElem(const vector<double>& vec, int& jmax, double& sigmamax){
	sigmamax = 0;
	jmax = -1;
	for (int j=0; j<vec.size(); j++){
		if (vec[j]>sigmamax){
			jmax = j;
			sigmamax = vec[j];
		}
	}
}


//  merge two indices vectors, local on the proc
void mergeSortInt(vector<int>& a, vector<int>& b, vector<int>& c){

	c.resize(0);

	sort( a.begin(), a.end());
	sort( b.begin(), b.end());

	int i = 0, j = 0;
	while( i < a.size() && j < b.size()){
		if( a[ i ] == b[ j ] ){
   			c.push_back( a[ i ] );
   			i++;
			j++;
		}
		else if( a[ i ] < b[ j ] ){
   			c.push_back( a[ i ] );
			i++;
		}
		else{
   			c.push_back( b[j ] );
			j++;
		}
	}
	while( i < a.size()){
		c.push_back( a[i] );
		i++;
	}
	while( j < b.size()){
		c.push_back( b[j] );
		j++;
	}
}

void mergeSortInt(vector<unsigned int>& a, vector<unsigned int>& b, vector<unsigned int>& c){

	c.resize(0);

	sort( a.begin(), a.end());
	sort( b.begin(), b.end());

	int i = 0, j = 0;
	while( i < a.size() && j < b.size()){
		if( a[ i ] == b[ j ] ){
   			c.push_back( a[ i ] );
   			i++;
			j++;
		}
		else if( a[ i ] < b[ j ] ){
   			c.push_back( a[ i ] );
			i++;
		}
		else{
   			c.push_back( b[j ] );
			j++;
		}
	}
	while( i < a.size()){
		c.push_back( a[i] );
		i++;
	}
	while( j < b.size()){
		c.push_back( b[j] );
		j++;
	}
}



/* I --  VECTOR OPERATIONS --- */

// I.1 - Initialize a Petsc Vec, given its pointer and its size:
void initVec(Vec& x, int itsSize, MPI_Comm theComm){
    PetscErrorCode ierr;
    ierr = VecCreate(theComm, &x);
    ierr = VecSetSizes(x,PETSC_DECIDE, itsSize);
    ierr = VecSetFromOptions(x);
}


// I.1.1 - Overloaded, copying the structure of another vector, b:
void initVec(Vec& toInit, Vec& b){
    PetscErrorCode ierr;
    ierr = VecDuplicate(b, &toInit);
}


/* setVecEl : setting one or more Elements
  external elements MUST be correctly available by the Procs !
*/

// I.2.1 Vec set values: just one element Proc 0 is doing the operation if PAR=false
void setVecEl(Vec& x, int ind, double val, MPI_Comm theComm, bool PAR = true){
  if(!PAR){
    int rank;
    MPI_Comm_rank (theComm, &rank);
    if(rank == 0){
      VecSetValues( x , 1 , &ind , &val , INSERT_VALUES );
    }
  }
  else{ // Parallel non-communicating way
    int startInd, endInd;
    VecGetOwnershipRange( x , &startInd , &endInd );
    if((ind>=startInd) && (ind<endInd)){
      VecSetValues( x , 1 , &ind , &val , INSERT_VALUES );
    }
  }
  // Need to finilize outsite!
}

// I.2.2 Vec set values: multiple values either in parallel or not:
void setVecEl(Vec& x, vector<double> val, MPI_Comm theComm,  bool PAR = true){
    int vecSize;
    VecGetSize(x, &vecSize);

    if(!PAR){ // Proc 0 is doing it and communicating it:
        int rank;
        MPI_Comm_rank (theComm, &rank);
        if(rank == 0){
           for(int h=0; h<vecSize; h++){
             VecSetValues( x , 1 , &h , &val[h] , INSERT_VALUES );
           }
        }
    }
    else{
       int startInd, endInd;
       VecGetOwnershipRange( x , &startInd , &endInd );
       for(int h=startInd; h<endInd; h++){
           VecSetValues( x , 1 , &h , &val[h] , INSERT_VALUES );
       }
    }
    VecAssemblyBegin(x);
    VecAssemblyEnd(x);
}


// I.2.3 set Vector from index low to high included equal to a given vector<double>
void setVecEl(Vec& x, int low, int high, vector<double> val, MPI_Comm theComm,  bool FIN = true, bool PAR = true){

  if(!PAR){ // Proc 0 is doing it and communicating it:
      int rank;
      MPI_Comm_rank (theComm, &rank);
      if(rank == 0){
         for(int h=low; h<high+1; h++){
           VecSetValues( x , 1 , &h , &val[h-low] , INSERT_VALUES );
         }
      }
  }
  else{
     int startInd, endInd;
     VecGetOwnershipRange( x , &startInd , &endInd );
     if( (startInd <= high) && (endInd >= low) ){ // intersection
       for(int h=low; h<high+1; h++){
          VecSetValues( x , 1 , &h , &val[h-low] , INSERT_VALUES );
       }
     }
  }
  if(FIN){
    VecAssemblyBegin(x);
    VecAssemblyEnd(x);
  }
}


// I.2.4 Set Elements with local vectors:
void setVecLocEl(Vec& x, vector<double> val, MPI_Comm theComm){
  int startInd, endInd;
  VecGetOwnershipRange( x , &startInd , &endInd );
  if(val.size() != endInd - startInd){puts("Wrong size of local vec!");};
  for(int h=startInd; h<endInd; h++){
      VecSetValues( x , 1 , &h , &val[h-startInd] , INSERT_VALUES );
  }
}


// I.3 Finalize the Assembly
void finalizeVec(Vec& x){
  VecAssemblyBegin(x);
  VecAssemblyEnd(x);
}


/* Global indices in 1D ordering to be passed! But VecGet is only local to proc!
   In these functions, result is broadcast to all the Procs if BROADCAST=true! Collective call;
*/

// I.4.0 generic and complete getVecEl
inline void getVecEl(Vec x, int ind, double& val, int& theRank, MPI_Comm theComm, bool BROADCAST=true){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  VecGetOwnershipRange( x , &startInd , &endInd );
  if(!BROADCAST){
    if( (ind >=startInd ) && (ind<endInd) ){
      VecGetValues(x, 1 , &ind, &val);
      theRank = rank;
    }
  }
  else{
    int amIRoot = 0;
    if( (ind >=startInd ) && (ind<endInd) ){
      VecGetValues(x, 1 , &ind, &val);
      amIRoot = 1;
    }
    int consensus = amIRoot * rank;
    int root = 0;
    MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, theComm);
    MPI_Bcast(&val, 1, MPI_DOUBLE, root, theComm);
  }
}


// I.4.0.b Get just one value and keep it local to the proc, collective call:
/* explicitly no broadcasting, faster
  - output: val and theRank (id of the proc having val)
*/

inline void getVecElNoBroadcast(Vec x, int ind, double& val, int& theRank, MPI_Comm theComm){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  VecGetOwnershipRange( x , &startInd , &endInd );
  if( (ind >=startInd ) && (ind<endInd) ){
    VecGetValues(x, 1 , &ind, &val);
    theRank = rank;
  }
}


// I.4.1 Get just one value and pass it to all Procs!:
/* overloaded, output is directly the double, all the procs have it */
double getVecEl(Vec x, int ind, MPI_Comm theComm){
  double toBeReturned = 0.0;
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  VecGetOwnershipRange( x , &startInd , &endInd );
  int amIRoot = 0;
  if( (ind >=startInd ) && (ind<endInd) ){
    VecGetValues(x, 1 , &ind, &toBeReturned);
    amIRoot = 1;
  }
  int consensus = amIRoot * rank;
  int root = 0;
  MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, theComm);
  MPI_Bcast(&toBeReturned, 1, MPI_DOUBLE, root, theComm);

  return toBeReturned;
}

void getVecEl(Vec x, int ind, double& val, MPI_Comm theComm){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  VecGetOwnershipRange( x , &startInd , &endInd );
  int amIRoot = 0;
  if( (ind >=startInd ) && (ind<endInd) ){
    VecGetValues(x, 1 , &ind, &val);
    amIRoot = 1;
  }
  int consensus = amIRoot * rank;
  int root = 0;
  MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, theComm);
  MPI_Bcast(&val, 1, MPI_DOUBLE, root, theComm);
}


// I.4.2 Get a subset of the vector: from low to high included and pass it to all
void getVecEl(Vec x, int low, int high, vector<double> val, MPI_Comm theComm){
  if(val.size() != high - low + 1){puts("Wrong size of Vec!");};
  const int n = high-low+1;
  PetscInt ind[n];
  for(unsigned int h=0; h<n; h++){
    ind[h] = low + h;
  }
  PetscScalar y[n];
  for(int j=0; j<n; j++){
      getVecEl(x, ind[j], y[j], theComm);
  }
  for(unsigned int h=0; h<n; h++){
    val[h] = y[h];
  }
}

vector<double> getVecEl(Vec x, int low, int high, MPI_Comm theComm){
    const int n = high-low+1;
    vector<double> toBeReturned;
    toBeReturned.resize(n);
    PetscInt ind[n];
    for(unsigned int h=0; h<n; h++){
      ind[h] = low + h;
    }
    PetscScalar y[n];
    for(int j=0; j<n; j++){
        getVecEl(x, ind[j], y[j], theComm);
    }
    for(unsigned int h=0; h<n; h++){
      toBeReturned[h] = y[h];
    }
    return toBeReturned;
}



/* transforming shared vector<double> to Vec and viceversa */

// I.5.1 Vec2vector converter
void Vec2vector(Vec& In, vector<double>& Out, MPI_Comm theComm){
  int vecSize;
  VecGetSize(In, &vecSize);
  if(Out.size()!=vecSize){Out.resize(vecSize);}
  getVecEl(In, 0, vecSize, Out, theComm);
}

// I.5.2 vector2Vec => passing just the pointer
void vector2Vec(vector<double> In, Vec& Out, MPI_Comm theComm){
  int vecSize = In.size();
  initVec(Out, vecSize, theComm);
  setVecEl(Out, In, theComm);
}








/* II -- MATRIX OPERATIONS --- */


// II.1 - Initialize a Matrix, given its pointer and its sizes:
void initMat(Mat& A, int nRows, int nCols, MPI_Comm theComm){
   PetscErrorCode ierr;
   ierr = MatCreate(theComm, &A);
   ierr = MatSetSizes(A, PETSC_DECIDE, PETSC_DECIDE, nRows, nCols);
   ierr = MatSetFromOptions(A);
   ierr = MatSetUp(A);
}

// II.1.2 overloaded share structure and non-zero pattern:
void initMat(Mat& matToInit, Mat A){
    PetscErrorCode ierr;
    ierr = MatDuplicate(A, MAT_SHARE_NONZERO_PATTERN, &matToInit);
}



/* set Matrix Elements or rows or cols */

// II.2.1 Setting just one element:
void setMatEl(Mat& A, int indRow, int indCol, double val, MPI_Comm theComm, bool PAR=true){
  if(!PAR){
    int rank;
    MPI_Comm_rank (theComm, &rank);
    if(rank == 0){ // Proc 0 is doing it and communicating it
      MatSetValues(A, 1, &indRow, 1, &indCol, &val, INSERT_VALUES);
    }
  }
  else{ //Parallel non-communicating way
    int startInd, endInd;
    MatGetOwnershipRange(A, &startInd, &endInd);
    if( (indRow>=startInd) &&(indRow<endInd) ){
      MatSetValues(A, 1, &indRow, 1, &indCol, &val, INSERT_VALUES);
    }
  }
  // Need to Finalize the Assembly outside !
}


// II.2.2 Setting a whole matrix by providing a dense full Table
void setMatEl(Mat& A, vector<vector<double>* > theMat, MPI_Comm theComm,  bool PAR = true){
    int nRows, nCols;
    MatGetSize(A, &nRows, &nCols);
    if(!PAR){ // Proc 0 is doing it row by row and communicates it.
      int rank;
      MPI_Comm_rank(theComm, &rank);
      if(rank == 0){
          for(int i=0; i<nRows; i++){
            vector<int> pattern = findNonZero(*theMat[i]);
            if(pattern.size()>0){
              PetscInt cols[pattern.size()];
              vector2PetscInt(pattern, cols);
              vector<double> theRow = nonZero(*theMat[i]);
              PetscScalar val[pattern.size()];
              vector2PetscScalar(theRow, val);
              MatSetValues(A,1,&i, pattern.size(), cols , val , INSERT_VALUES);
            }
          }
      }
    }
    else{ // Parallel non-communicating way.
      int startInd, endInd;
      MatGetOwnershipRange(A, &startInd, &endInd);
      for(int i=startInd; i<endInd; i++){
        vector<int> pattern = findNonZero(*theMat[i]);
        if(pattern.size()>0){
          PetscInt cols[pattern.size()];
          vector2PetscInt(pattern, cols);
          vector<double> theRow = nonZero(*theMat[i]);
          PetscScalar val[pattern.size()];
          vector2PetscScalar(theRow, val);
          MatSetValues(A,1,&i, pattern.size(), cols , val , INSERT_VALUES);
        }
      }
    }
    MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
}


// II.2.3 Assemble the Matrix by providing a table containing the pattern and a table containing the non-zero entries
void setMatEl(Mat& A, vector<vector<int>* > pattern, vector<vector<double>* > theEntries, MPI_Comm theComm,  bool PAR = true){
  int nRows, nCols;
  MatGetSize(A, &nRows, &nCols);
  if(!PAR){ // Proc 0 is doing it row by row and communicates it.
    int rank;
    MPI_Comm_rank(theComm, &rank);
    if(rank == 0){
        for(int i=0; i<nRows; i++){
          if(pattern[i]->size()>0){
            PetscInt cols[pattern[i]->size()];
            vector2PetscInt(*pattern[i], cols);
            PetscScalar val[pattern[i]->size()];
            vector2PetscScalar(*theEntries[i], val);
            MatSetValues(A,1,&i, pattern[i]->size(), cols , val , INSERT_VALUES);
          }
        }
    }
  }
  else{ // Parallel non-communicating way.
    int startInd, endInd;
    MatGetOwnershipRange(A, &startInd, &endInd);
    for(int i=startInd; i<endInd; i++){
      if(pattern[i]->size()>0){
        PetscInt cols[pattern[i]->size()];
        vector2PetscInt(*pattern[i], cols);
        PetscScalar val[pattern[i]->size()];
        vector2PetscScalar(*theEntries[i], val);
        MatSetValues(A,1,&i, pattern[i]->size(), cols , val , INSERT_VALUES);
      }
    }
  }
  MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
}



// II.2.4 Set Matrix by providing local dense submatrices of the right size.
void setMatLocEl(Mat& A, vector<vector<double>* > theMat, MPI_Comm theComm){
  int startInd, endInd;
  MatGetOwnershipRange(A, &startInd, &endInd);
  for(int i=startInd; i<endInd; i++){ // i is the global index = local + startInd
    vector<int> pattern = findNonZero(*theMat[i-startInd]);
    if(pattern.size()>0){
      PetscInt cols[pattern.size()];
      vector2PetscInt(pattern, cols);
      vector<double> theRow = nonZero(*theMat[i-startInd]);
      PetscScalar val[pattern.size()];
      vector2PetscScalar(theRow, val);
      MatSetValues(A,1,&i, pattern.size(), cols , val , INSERT_VALUES);
    }
  }
  MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
}


// II.2.5 Set Matrix by providing local sparse submatrices of the right size.
void setMatLocEl(Mat& A, vector<vector<int>*> pattern, vector<vector<double>* > theEntries, MPI_Comm theComm){
  int startInd, endInd;
  MatGetOwnershipRange(A, &startInd, &endInd);
  for(int i=startInd; i<endInd; i++){
    int local = i-startInd;
    const int n = pattern[local]->size();
    if(n>0){
      PetscInt cols[n];
      vector2PetscInt(*pattern[local], cols);
      PetscScalar val[n];
      vector2PetscScalar(*theEntries[local], val);
      MatSetValues(A,1,&i, n, cols , val , INSERT_VALUES);
    }
  }
  MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
}



// II.2.6 Set a Matrix Row by providing a dense vector: FINALIZE OUTSIDE!
void setMatRow(Mat&A, int idRow, vector<double>& theRow, MPI_Comm theComm, bool PAR=true){
  if(!PAR){ // Proc 0 is doing it and communicates it.
    int rank;
    MPI_Comm_rank(theComm, &rank);
    if(rank == 0){
        vector<int> pattern = findNonZero(theRow);
        if(pattern.size()>0){
            PetscInt cols[pattern.size()];
            vector2PetscInt(pattern, cols);
            vector<double> toInsert = nonZero(theRow);
            PetscScalar val[pattern.size()];
            vector2PetscScalar(toInsert, val);
            MatSetValues(A,1,&idRow, pattern.size(), cols , val , INSERT_VALUES);
        }
    }
  }
  else{ // Parallel non-communicating way: just the Proc that has the row does it
    int startInd, endInd;
    MatGetOwnershipRange(A, &startInd, &endInd);
    vector<int> pattern = findNonZero(theRow);
    if( (idRow>=startInd) && (idRow<endInd)){
      if(pattern.size()>0){
        PetscInt cols[pattern.size()];
        vector2PetscInt(pattern, cols);
        vector<double> toInsert = nonZero(theRow);
        PetscScalar val[pattern.size()];
        vector2PetscScalar(toInsert, val);
        MatSetValues(A,1,&idRow, pattern.size(), cols , val , INSERT_VALUES);
      }
    }
  }
  // Have to Finalize Outside !!
}




// II.2.7 Set a matrix row by providing a sparse vector: FINALIZE OUTSIDE!
void setMatRow(Mat&A, int idRow, vector<int>& colInd, vector<double>& theRow, MPI_Comm theComm, bool PAR=true){
  if(!PAR){ // Proc 0 is doing it and communicates it.
    int rank;
    MPI_Comm_rank(theComm, &rank);
    if(rank == 0){
        const int n = colInd.size();
        if(n>0){
            PetscInt cols[n];
            vector2PetscInt(colInd, cols);
            vector<double> toInsert = nonZero(theRow);
            PetscScalar val[n];
            vector2PetscScalar(toInsert, val);
            MatSetValues(A,1,&idRow, n, cols , val , INSERT_VALUES);
        }
    }
  }
  else{ // Parallel non-communicating way: just the Proc that has the row do it
    int startInd, endInd;
    MatGetOwnershipRange(A, &startInd, &endInd);
    if( (idRow>=startInd) && (idRow<endInd)){
      const int n = colInd.size();
      if(n>0){
        PetscInt cols[n];
        vector2PetscInt(colInd, cols);
        vector<double> toInsert = nonZero(theRow);
        PetscScalar val[n];
        vector2PetscScalar(toInsert, val);
        MatSetValues(A,1,&idRow, n, cols , val , INSERT_VALUES);
      }
    }
  }
  // Have to Finalize Outside !!
}


// II.3 Finalizing the Assembly
void finalizeMat(Mat& A){
  MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
}




/* Routines to get matrix elements with a collective call */

// II.4.0 get matrix element and decide if broadcasting or not:
inline void getMatEl(Mat A, int idRow, int idCol, double& val, int& theRank, MPI_Comm theComm, bool BROADCAST=true){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  MatGetOwnershipRange( A , &startInd , &endInd );
  if(!BROADCAST){
    if( (idRow >=startInd ) && (idRow<endInd) ){
      MatGetValues(A, 1, &idRow, 1, &idCol, &val);
      theRank = rank;
    }
  }
  else{
    int amIRoot = 0;
    if( (idRow >=startInd ) && (idRow<endInd) ){
      MatGetValues(A, 1, &idRow, 1, &idCol, &val);
      amIRoot = 1;
    }
    int consensus = amIRoot * rank;
    int root = 0;
    MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, theComm);
    MPI_Bcast(&val, 1, MPI_DOUBLE, root, theComm);
  }
}

// II.4.0.b collective call, explicitly no broadcast, faster
inline void getMatEl(Mat A, int idRow, int idCol, double& val, int& theRank, MPI_Comm theComm){
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  MatGetOwnershipRange( A , &startInd , &endInd );
  if( (idRow >=startInd ) && (idRow<endInd) ){
    MatGetValues(A, 1, &idRow, 1, &idCol, &val);
    theRank = rank;
  }
}


// II.4 Get just one element, specified by idRow and idCol, global indices:
double getMatEl(Mat A, int idRow, int idCol, MPI_Comm theComm){
  double toBeReturned = 0.0;
  int rank;
  MPI_Comm_rank (theComm, &rank);
  int startInd, endInd;
  MatGetOwnershipRange( A , &startInd , &endInd );
  int amIRoot = 0;
  if( (idRow >=startInd ) && (idRow<endInd) ){
    MatGetValues(A, 1, &idRow, 1, &idCol, &toBeReturned);
    amIRoot = 1;
  }
  int consensus = amIRoot * rank;
  int root = 0;
  MPI_Allreduce(&consensus, &root, 1, MPI_INT, MPI_SUM, theComm);
  MPI_Bcast(&toBeReturned, 1, MPI_DOUBLE, root, theComm);

  return toBeReturned;
}




/* Generic Routines for matrix manipulation */


// Setting a column equal to a vec. Have to finalize outside!
void setMatCol(Mat& A, int idCol, Vec v, MPI_Comm theComm){
    int nRows, nCols;
    MatGetSize(A, &nRows, &nCols);
    int vecSize;
    VecGetSize(v, &vecSize);
    if(vecSize != nRows){puts("Wrong size of column!");}
    int lowVec, highVec;
    VecGetOwnershipRange(v, &lowVec, &highVec);
    int lowMat, highMat;
    MatGetOwnershipRange(A, &lowMat, &highMat);

    for(int i=lowVec; i<highVec; i++){
        if( (i>=lowMat) && (i<highMat) ){ // no communication needed
          PetscScalar val;
          VecGetValues(v,1,&i,&val);
          MatSetValue(A,i,idCol,val,INSERT_VALUES);
        }
        else{ // collective call to share the value
          double globVal = getMatEl(A, i, idCol, theComm);
          setMatEl(A, i, idCol, globVal, theComm);
        }
    }
}

// Computing the transpose of a matrix, returning a finalized matrix
Mat transpose(Mat A, MPI_Comm theComm){
    int nRowA, nColA;
    MatGetSize(A, &nRowA, &nColA);
    int nRow = nColA;
    int nCol = nRowA;
    Mat theTranspose;
    initMat(theTranspose, nRow, nCol, theComm);
    MatTranspose(A, MAT_INITIAL_MATRIX, &theTranspose);
    finalizeMat(theTranspose);
    return theTranspose;
}

// overloaded to take existing pointer:
void transpose(Mat A, Mat& theTranspose, MPI_Comm theComm){
    int nRowA, nColA;
    MatGetSize(A, &nRowA, &nColA);
    int nRow = nColA;
    int nCol = nRowA;
    initMat(theTranspose, nRow, nCol, theComm);
    MatTranspose(A, MAT_INITIAL_MATRIX, &theTranspose);
    finalizeMat(theTranspose);
}

// Computing the hermitian of a matrix, returning a finalized matrix
Mat hermitian(Mat A, MPI_Comm theComm){
    int nRowA, nColA;
    MatGetSize(A, &nRowA, &nColA);
    int nRow = nColA;
    int nCol = nRowA;
    Mat theTranspose;
    initMat(theTranspose, nRow, nCol, theComm);
    MatHermitianTranspose(A, MAT_INITIAL_MATRIX, &theTranspose);
    finalizeMat(theTranspose);
    return theTranspose;
}

// overloaded to take existing pointer:
void hermitian(Mat A, Mat& theTranspose, MPI_Comm theComm){
    int nRowA, nColA;
    MatGetSize(A, &nRowA, &nColA);
    int nRow = nColA;
    int nCol = nRowA;
    initMat(theTranspose, nRow, nCol, theComm);
    MatHermitianTranspose(A, MAT_INITIAL_MATRIX, &theTranspose);
    finalizeMat(theTranspose);
}



// Identity:
Mat eye(int n, MPI_Comm theComm){
    Vec d;
    initVec(d, n, theComm);
    VecSet(d, 1.0);
    finalizeVec(d);
    Mat I;
    initMat(I, n, n, theComm);
    MatDiagonalSet(I, d, INSERT_VALUES);
    finalizeMat(I);
    return I;
}

// overloaded to take an existing pointer:
void eye(Mat& I, int n, MPI_Comm theComm){
    Vec d;
    initVec(d, n, theComm);
    VecSet(d, 1.0);
    finalizeVec(d);
    initMat(I, n, n, theComm);
    MatDiagonalSet(I, d, INSERT_VALUES);
    finalizeMat(I);
}


/* reshape function for Petsc matrices
  - inputs: the matrix to be reshaped, the dimensions
  - output: a matrix
*/
inline Mat reshape(Mat& matToReshape, unsigned int newRows, unsigned int newCols, MPI_Comm theComm){
  PetscInt nRows, nCols;
  MatGetSize(matToReshape, &nRows, &nCols);
  assert(newRows * newCols == nRows * nCols);
  assert( (newRows % nRows == 0) || (nRows % newRows == 0) );
  assert( (newCols % nCols == 0) || (nCols % newCols == 0) );
  Mat toBeReturned;
  initMat(toBeReturned, newRows, newCols, theComm);
  for(unsigned int idRow=0; idRow<nRows; idRow++){
    for(unsigned int idCol=0; idCol<nCols; idCol++){
      unsigned int linInd = idCol * nRows + idRow;
      unsigned int J = linInd / newRows;
      unsigned int I = linInd % newRows;
      double value = getMatEl(matToReshape, idRow, idCol, theComm);
      setMatEl(toBeReturned, I , J , value, theComm);
    }
  }
  finalizeMat(toBeReturned);
  return toBeReturned;
}


/* vectorise a Petsc matrix
  - inputs: the matrix to be reshaped, the dimensions
  - output: a vector
*/
inline Vec vectoriseMat(Mat& toBeVectorised, MPI_Comm theComm){
  PetscInt nRows, nCols;
  MatGetSize(toBeVectorised, &nRows, &nCols);
  const unsigned int vecSize = nRows * nCols;
  Vec toBeReturned;
  initVec(toBeReturned, vecSize, theComm);
  for(unsigned int idRow=0; idRow<nRows; idRow++){
    for(unsigned int idCol=0; idCol<nCols; idCol++){
      unsigned int linInd = idCol * nRows + idRow;
      double value = getMatEl(toBeVectorised, idRow, idCol, theComm);
      setVecEl(toBeReturned, linInd, value, theComm);
    }
  }
  finalizeVec(toBeReturned);
  return toBeReturned;
}


// produce a diagonal matrix given a vector:
void diag(Mat& D, Vec d, MPI_Comm theComm){
    int n;
    VecGetSize(d, &n);
    initMat(D, n, n, theComm);
    MatDiagonalSet(D, d, INSERT_VALUES);
    finalizeMat(D);
}

// overloaded to provide a matrix:
Mat diag(Vec d, MPI_Comm theComm){
    int n;
    VecGetSize(d, &n);
    Mat D;
    initMat(D, n, n, theComm);
    MatDiagonalSet(D, d, INSERT_VALUES);
    finalizeMat(D);
    return D;
}



/* KSP ROUTINES */

// generic init, Krilov method: put  KSPCG for CG method. Initial guess is zero
void initLinSolve(KSP& ksp, Mat A, MPI_Comm theComm, KSPType type = KSPGMRES){
    KSPCreate(theComm,&ksp);
    KSPSetOperators(ksp, A, A); // => use A to build preconditioner
    KSPSetType(ksp, type);
    KSPSetFromOptions(ksp);
    KSPSetUp(ksp);
}


// overloaded to take tolerances:
void initLinSolve(KSP& ksp, Mat A, double relTol, double absTol, MPI_Comm theComm, KSPType type = KSPGMRES){
    KSPCreate(theComm,&ksp);
    KSPSetOperators(ksp, A, A); // => use A to build preconditioner
    KSPSetType(ksp, type);
    KSPSetTolerances(ksp, relTol, absTol, PETSC_DEFAULT, PETSC_DEFAULT);
    KSPSetFromOptions(ksp);
    KSPSetUp(ksp);
}



// Solve Ax = b;
void solveLin(KSP& ksp, Vec& b, Vec&x, MPI_Comm theComm, bool haveToInit=true){
    if(haveToInit){
        int vecSize;
        VecGetSize(b, &vecSize);
        initVec(x, vecSize, theComm);
    }
    KSPSolve(ksp,b,x);
}





/*  SLEPC ROUTINES: */


// III -- EIGENVALUE PROBLEMS --

// III.1 Creating an eigenvalue context: type = EPS_HEP, EPS_NHEP

// Using Default Tolerances:
void initEps(EPS& eps, Mat A, EPSProblemType type, MPI_Comm theComm){
  PetscErrorCode ierr;
  ierr = EPSCreate(theComm,&eps);
  ierr = EPSSetOperators(eps,A,NULL);
  ierr = EPSSetProblemType(eps,type);
  ierr = EPSSetType(eps, EPSKRYLOVSCHUR);
  ierr = EPSSetFromOptions(eps);
}

// overloading by specifying Tolerances and number of eigenvalues:
void initEps(EPS& eps, Mat A, EPSProblemType type, int nev, double tol, MPI_Comm theComm){
  PetscErrorCode ierr;
  ierr = EPSCreate(theComm,&eps);
  ierr = EPSSetOperators(eps, A, NULL);
  ierr = EPSSetProblemType(eps,type);
  ierr = EPSSetType(eps, EPSKRYLOVSCHUR);
  ierr = EPSSetTolerances(eps, tol, PETSC_DEFAULT);
  ierr = EPSSetDimensions(eps, nev, PETSC_DEFAULT, PETSC_DEFAULT);
  ierr = EPSSetFromOptions(eps);
}

// overloading by adding mass: type = EPS_GHEP, EPS_GHIEP, EPS_GNHEP, EPS_PGNHEP
void initEps(EPS& eps, Mat A, Mat Mass, EPSProblemType type, MPI_Comm theComm){
  PetscErrorCode ierr;
  ierr = EPSCreate(theComm,&eps);
  ierr = EPSSetOperators(eps, A, Mass);
  ierr = EPSSetProblemType(eps,type);
  ierr = EPSSetType(eps, EPSKRYLOVSCHUR);
  ierr = EPSSetFromOptions(eps);
}

// overloading by specifying Tolerances and number of eigenvalues:
void initEps(EPS& eps, Mat A, Mat Mass, EPSProblemType type, int nev, double tol, MPI_Comm theComm){
  PetscErrorCode ierr;
  ierr = EPSCreate(theComm,&eps);
  ierr = EPSSetOperators(eps, A, Mass);
  ierr = EPSSetProblemType(eps,type);
  ierr = EPSSetType(eps, EPSKRYLOVSCHUR);
  ierr = EPSSetTolerances(eps, tol, PETSC_DEFAULT);
  ierr = EPSSetDimensions(eps, nev, PETSC_DEFAULT, PETSC_DEFAULT);
  ierr = EPSSetFromOptions(eps);
}

// overloading with solvertype
void initEps(EPS& eps, Mat A, EPSProblemType pbType, EPSType solverType, MPI_Comm theComm){
  PetscErrorCode ierr;
  ierr = EPSCreate(theComm,&eps);
  ierr = EPSSetOperators(eps,A,NULL);
  ierr = EPSSetProblemType(eps, pbType);
  ierr = EPSSetType(eps, solverType);
  ierr = EPSSetFromOptions(eps);
}


// solving one term hermitian EPS :
void solveFirstPairHermEPS(EPS& eps, Mat A, Vec& v, double& lambda, MPI_Comm theComm){
  int nRows, nCols;
  MatGetSize(A, &nRows, &nCols);
  EPSSolve(eps);
  int nconv;
  EPSGetConverged(eps, &nconv);

  int nOfEig = (nCols < nconv) ? nCols : nconv;
  if(nOfEig==0){PetscPrintf(PETSC_COMM_WORLD,"0 Eig converged!\n");}

  initVec(v, nRows, theComm);
  finalizeVec(v);

  double imagPart;
  Vec imagEV; initVec(imagEV, nRows, theComm); finalizeVec(imagEV);

  EPSGetEigenpair(eps, 0, &lambda, &imagPart, v, imagEV);
  finalizeVec(v);
}



// solving EPS for Hermitian problems:
void solveHermEPS(EPS& eps, Mat A, Mat& V, Vec& lambda, MPI_Comm theComm){
  int nRows, nCols;
  MatGetSize(A, &nRows, &nCols);
  EPSSolve(eps);
  int nconv;
  EPSGetConverged(eps, &nconv);

  int nOfEig = (nCols < nconv) ? nCols : nconv;
  if(nOfEig==0){PetscPrintf(PETSC_COMM_WORLD,"0 Eig converged!\n");}

  initMat(V, nRows, nOfEig, theComm);
  initVec(lambda, nOfEig, theComm);

  double realPart;
  double imagPart;
  Vec realEV; initVec(realEV, nRows, theComm); finalizeVec(realEV);
  Vec imagEV; initVec(imagEV, nRows, theComm); finalizeVec(imagEV);
  for(int j=0; j<nOfEig; j++) {
      EPSGetEigenpair(eps, j, &realPart, &imagPart, realEV, imagEV);
      setVecEl(lambda, j, realPart, theComm);
      setMatCol(V, j, realEV, theComm);
  }
  finalizeMat(V);
  finalizeVec(lambda);
}

// overloaded to take a tolerance and compress: ABSOLUTE tol = false by default
void solveHermEPS(EPS& eps, Mat A, Mat& V, Vec& lambda, double tol, MPI_Comm theComm, bool ABSOLUTE=false){
  int nRows, nCols;
  MatGetSize(A, &nRows, &nCols);
  EPSSolve(eps);
  int nconv;
  EPSGetConverged(eps, &nconv);

  int nOfEig = (nCols < nconv) ? nCols : nconv;
  if(nOfEig==0){PetscPrintf(PETSC_COMM_WORLD,"0 Eig converged!\n");}

  int nToTake = 0;
  int start = 0;
  if(!ABSOLUTE){
    nToTake = 1;
    start = 1;
  };
  vector<double> theEigenvalues; theEigenvalues.resize(nOfEig);
  double realPart;
  double imagPart;
  Vec realEV; initVec(realEV, nRows, theComm); finalizeVec(realEV);
  Vec imagEV; initVec(imagEV, nRows, theComm); finalizeVec(imagEV);
  for(int j=start; j<nOfEig; j++) {
      EPSGetEigenpair(eps, j, &realPart, &imagPart, realEV, imagEV);
      theEigenvalues[j] = realPart;
      if(!ABSOLUTE){
        if(fabs(realPart) > tol * fabs(realPart) ){nToTake = nToTake + 1;};
      }
      else{
        if(fabs(realPart) > tol){nToTake = nToTake + 1;};
      }
  }

  initMat(V, nRows, nToTake, theComm);
  initVec(lambda, nToTake, theComm);

  for(int j=0; j<nToTake; j++) {
      EPSGetEigenpair(eps, j, &realPart, &imagPart, realEV, imagEV);
      setVecEl(lambda, j, realPart, theComm);
      setMatCol(V, j, realEV, theComm);
  }
  finalizeMat(V);
  finalizeVec(lambda);
}




// IV -- ORTHOGONALIZATION ROUTINES --

// IV.0 - Check the BV properties
void checkBV(BV& bv){
    int loc, glob, cols;
    BVGetSizes(bv, &loc, &glob, &cols);
    PetscPrintf(PETSC_COMM_WORLD, "Number of vectors: %D\n", cols);
    PetscPrintf(PETSC_COMM_WORLD, "Size of vectors: %D\n", glob);
    PetscPrintf(PETSC_COMM_WORLD, "\n");
    BVView(bv, PETSC_VIEWER_STDOUT_WORLD);
}

// IV.1 - QR decomposition, using BVOrthogonalize:
void initQR(BV& bv, Mat V, MPI_Comm theComm){
   PetscErrorCode ierr;
   ierr = BVCreate(theComm, &bv);
   int nRows,nCols;
   ierr = MatGetSize(V, &nRows, &nCols);
   Vec x;
   initVec(x, nRows, theComm);
   ierr = MatGetColumnVector(V, x, 0);
   ierr = BVSetSizesFromVec(bv, x, nCols);
   ierr = BVSetOrthogonalization(bv, BV_ORTHOG_MGS, BV_ORTHOG_REFINE_IFNEEDED, 0.5, BV_ORTHOG_BLOCK_GS);
   ierr = BVSetFromOptions(bv);

   ierr = BVInsertVec(bv, 0, x);
   for(unsigned int j=1; j<nCols; j++){
       ierr = MatGetColumnVector(V, x, j);
       ierr = BVInsertVec(bv, j, x);
   };
}

// IV.2 set QR with mass Mass: BVSetMatrix(BV bv,Mat B,PetscBool indef)
void initQR(BV& bv, Mat V, Mat Mass, MPI_Comm theComm){
   PetscErrorCode ierr;
   ierr = BVCreate(theComm, &bv);
   int nRows,nCols;
   ierr = MatGetSize(V, &nRows, &nCols);
   Vec x;
   initVec(x, nRows, theComm);
   ierr = MatGetColumnVector(V, x, 0);
   ierr = BVSetSizesFromVec(bv, x, nCols);
   ierr = BVSetMatrix(bv, Mass, PETSC_FALSE);
   ierr = BVSetOrthogonalization(bv, BV_ORTHOG_MGS, BV_ORTHOG_REFINE_IFNEEDED, 0.5, BV_ORTHOG_BLOCK_GS);
   ierr = BVSetFromOptions(bv);

   ierr = BVInsertVec(bv, 0, x);
   for(unsigned int j=1; j<nCols; j++){
       ierr = MatGetColumnVector(V, x, j);
       ierr = BVInsertVec(bv, j, x);
   };
}


// IV.3 solve the QR factorization: if Q and R are initialized outside put a false.
void solveQR(BV& bv, Mat& Q, Mat& R, MPI_Comm theComm, bool haveToInit = true){
    int loc, glob, nVec;
    BVGetSizes(bv, &loc, &glob, &nVec);
    int nRows,nCols;
    if(!haveToInit){
      MatGetSize(Q, &nRows, &nCols);
      if( (glob!=nRows) || (nVec!=nCols) ){puts("Wrong size of input args!");}
    }
    else{
       initMat(Q, glob, nVec, theComm);
       initMat(R, nVec, nVec, theComm);
       nRows = glob;
       nCols = nVec;
    }
    // Matrix Z => to be copied into R, the output.
    Mat Z;
    MatCreateSeqDense(PETSC_COMM_SELF, nCols, nCols, NULL, &Z);
    MatSetFromOptions(Z);

    // Performing the QR orthogonalization:
    BVOrthogonalize(bv, Z);

    // copying Z into R, to whatever matrix format it is:
    for(int i=0; i<nCols; i++){
        const PetscInt* colZ;
        const PetscScalar* theRowOfZ;
        MatGetRow(Z, i, &nCols , &colZ, &theRowOfZ);
        vector<double> toInsert; toInsert.resize(nCols);
        PetscScalar2vector(theRowOfZ, toInsert);
        setMatRow(R, i, toInsert, theComm);
    }
    finalizeMat(R);
    MatDestroy(&Z);

    // putting the column vectors of bv into Q:
    Vec theOrthCol;
    initVec(theOrthCol, nRows, theComm);
    for(int j=0; j<nCols; j++){
        BVGetColumn(bv, j, &theOrthCol);
        setMatCol(Q, j, theOrthCol, theComm);
        BVRestoreColumn(bv, j, &theOrthCol);
    };
    finalizeMat(Q);
}


// Modified Gram Schmid producing thin Q,R:
/* the columns of the matrix A are the vectors to be orthonormalised*/
void MGScols(Mat& A, Mat& Q, Mat& R, MPI_Comm theComm, vector<int>& idColToRetrieve, double tolQR=2.22e-16){
  int nRows, nCols;
  MatGetSize(A, &nRows, &nCols);
  initMat(Q,nRows,nCols,theComm);
  finalizeMat(Q);
  initMat(R,nCols,nCols,theComm);
  finalizeMat(R);

  // Create a set of vectors, copy the columns of A:
  vector<Vec> theCols;
  theCols.resize(nCols);
  //Vec left;
  for(int idCol=0; idCol<nCols; idCol++){
    initVec(theCols[idCol],nRows,theComm);
    finalizeVec(theCols[idCol]);
    // MatCreateVec(A,&theCols[idCol],&left) in alternative !!!
    MatGetColumnVector(A,theCols[idCol],idCol);
  }
  //VecDestroy(&left);

  //Starting the MGS algorithm:
  vector<int> toRetrieve;
  for(int i=0; i<nCols; i++){
    double r_ii;
    VecNorm(theCols[i],NORM_2,&r_ii);
    double coeff = 0.0;
    if(r_ii>tolQR){
      toRetrieve.push_back(i);
      setMatEl(R, i, i, r_ii, theComm);
      coeff = 1.0/r_ii;
      for(int idRow=0; idRow<nRows; idRow++){
        double qValue = getVecEl(theCols[i],idRow,theComm);
        qValue = qValue*coeff;
        setMatEl(Q, idRow, i, qValue,theComm);
      }
    }
    else{
      //setMatEl(fullR, i, i, 0.0, theComm); //uncomment to obtain dense R
    }
    // Computing the off-diagonal part of R
    for(int j=i+1;j<nCols;j++){
      double r_ij;
      if(coeff>0){
        VecDot(theCols[i],theCols[j],&r_ij);
        r_ij = r_ij*coeff;
        setMatEl(R,i,j,r_ij,theComm);
        double scale = -1.0*r_ij*coeff;
        VecAXPY(theCols[j],scale,theCols[i]);
      }
      else{
        //setMatEl(fullR,i,j,0.0,theComm); //uncomment to obtain a dense R
      }
    }
  }
  finalizeMat(Q);
  finalizeMat(R);

  // provide the columns id to be retrieve if a vector has been specified
  idColToRetrieve = toRetrieve;


  int rank = toRetrieve.size();

  /* if the matrix is close to the zero matrix, return a
     zero rank in the form of a null pointer.
  */
  if (rank == 0){
    Q = PETSC_NULL;
    R = PETSC_NULL;
    return;
  }

  for(int i=0; i<nCols; i++){
    VecDestroy(&theCols[i]);
  }
}


/* the rows of the matrix A are the vectors to be orthonormalised*/
void MGSrows(Mat& A, Mat& Q, Mat& R, MPI_Comm theComm, double tolQR=2.22e-16){
  // implementation in temporayRoutine.tmp to be optimised
}

// V -- SVD --

// V.1 -- Init an SVD context --
void initSVD(SVD& svd, Mat A, MPI_Comm theComm, SVDType type=SVDCYCLIC){
    SVDCreate(theComm, &svd);
    SVDSetType(svd,type);
    SVDSetOperator(svd, A);
    SVDSetFromOptions(svd);

}


// overloading by specifying the number of singular values:
void initSVD(SVD& svd, Mat A, int nsv, MPI_Comm theComm, SVDType type=SVDCYCLIC, bool LARGEST=true){
    SVDCreate(theComm, &svd);
    SVDSetOperator(svd, A);
    SVDSetType(svd,type);
    SVDSetDimensions(svd, nsv, PETSC_DEFAULT, PETSC_DEFAULT);
    if(!LARGEST){
        SVDSetWhichSingularTriplets(svd, SVD_SMALLEST);
    };
    SVDSetFromOptions(svd);
}


// overloading by specifying Tolerances and number of singular values: to compute the smallest singular values, put a false when calling
void initSVD(SVD& svd, Mat A, int nsv, double tol, MPI_Comm theComm, SVDType type=SVDCYCLIC,bool LARGEST=true){
    SVDCreate(theComm, &svd);
    SVDSetOperator(svd, A);
    SVDSetType(svd,type);
    SVDSetDimensions(svd, nsv, PETSC_DEFAULT, PETSC_DEFAULT);
    SVDSetTolerances(svd, tol, PETSC_DECIDE);
    if(!LARGEST){
        SVDSetWhichSingularTriplets(svd, SVD_SMALLEST);
    };
    SVDSetFromOptions(svd);
}


// overloading by specifying Tolerances and number of singular values: to compute the smallest singular values, put a false when calling
void initSVD(SVD& svd, Mat A, double tol, MPI_Comm theComm, SVDType type=SVDCYCLIC,bool LARGEST=true){
    SVDCreate(theComm, &svd);
    SVDSetOperator(svd, A);
    SVDSetType(svd,type);
    //SVDSetDimensions(svd,PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);
    SVDSetTolerances(svd, tol, PETSC_DECIDE);
    if(!LARGEST){
        SVDSetWhichSingularTriplets(svd, SVD_SMALLEST);
    };
    SVDSetFromOptions(svd);
}


/* compute one SVD term
  compute the largest singular value and its associated singular vectors
  - input: the matrix,
  - output: the singular value, the singular vectors
*/
void computeOneSVDTerm(Mat& A, double& sigma, Vec& u, Vec& v, MPI_Comm theComm, SVDType type=SVDTRLANCZOS, bool LARGEST=true){
  SVD svd;
  SVDCreate(theComm, &svd);
  SVDSetOperator(svd, A);
  SVDSetType(svd,type);
  PetscInt nsv = 1;
  SVDSetDimensions(svd, nsv, PETSC_DEFAULT, PETSC_DEFAULT);
  SVDSetTolerances(svd, 1.0e-12, PETSC_DECIDE);
  if(!LARGEST){
      SVDSetWhichSingularTriplets(svd, SVD_SMALLEST);
  };
  SVDSetFromOptions(svd);

  SVDSolve(svd);
  int nconv;
  SVDGetConverged(svd,&nconv);
  MatCreateVecs(A,&v,&u);
  SVDGetSingularTriplet( svd, 0, &sigma, u, v );

  SVDDestroy(&svd);
}


// V.2 Solving SVD, putting the result into two parallel matrices and one Vec;
// U, V and S are just pointers, matrices are created inside according to the converged
void solveSVD(SVD& svd, Mat& A, Mat& U, Mat& V, Vec& S, MPI_Comm theComm){

    Vec u,v;
    PetscReal sigma;
    int nconv;
    SVDSolve(svd);
    SVDGetConverged(svd,&nconv);
    MatCreateVecs(A,&v,&u);
    int nRowA, nColA;
    MatGetSize(A,&nRowA, &nColA);
    int nRowU = nRowA;
    int nRowV = nColA;

    //Deciding the dimensions
    int nWanted, nCSP, mpd;
    SVDGetDimensions(svd, &nWanted, &nCSP, &mpd);
    //int nSingular = ( (nconv < nWanted) ? nconv : nWanted );
    int nSingular = min(nRowA, nColA);
    nSingular = min(nconv, nSingular);

    //initialize the matrices:
    initMat(U, nRowU, nSingular, theComm);
    initMat(V, nRowV, nSingular, theComm);
    initVec(S, nSingular, theComm);


    // to get error information add SVDComputeError.
    for (int j=0; j<nSingular; j++) {
        SVDGetSingularTriplet( svd, j, &sigma, u, v );
        setMatCol(U, j, u, theComm);
        setMatCol(V, j, v, theComm);
        setVecEl(S, j, sigma, theComm);
    }
    finalizeMat(U);
    finalizeMat(V);
    finalizeVec(S);
    VecDestroy(&u);
    VecDestroy(&v);
}


// Overloaded: solve the SVD by retrieving only the part of the spectrum greater than tol, ABSOLUTE or !ABSOLUTE = RELATIVE to sigma 0
/* this is achieved by using the BV object */
void solveSVD(SVD& svd, Mat& A, Mat& U, Mat& V, Vec& S, double tol, MPI_Comm theComm, bool ABSOLUTE = false){

  Vec u,v;
  PetscReal sigma;
  int nconv;
  SVDSolve(svd);
  SVDGetConverged(svd,&nconv);
  MatCreateVecs(A,&v,&u);
  int nRowA, nColA;
  MatGetSize(A,&nRowA, &nColA);
  int nRowU = nRowA;
  int nRowV = nColA;

  //Deciding the dimensions
  int nWanted, nCSP, mpd;
  SVDGetDimensions(svd, &nWanted, &nCSP, &mpd);
  //int nSingular = min(nconv,nWanted);
  int nSingular = ( (nconv < nWanted) ? nconv : nWanted );

  //Create 2 BV to temporarily store the triplets:
  BV bvU, bvV;
  vector<double> sig; sig.resize(nSingular);

  // bvU:
  BVCreate(theComm, &bvU);
  BVSetSizes(bvU, PETSC_DECIDE, nRowU, nSingular);
  BVSetMatrix(bvU, NULL, PETSC_FALSE);
  BVSetFromOptions(bvU);

  // bvV:
  BVCreate(theComm, &bvV);
  BVSetSizes(bvV, PETSC_DECIDE, nRowV, nSingular);
  BVSetMatrix(bvV, NULL, PETSC_FALSE);
  BVSetFromOptions(bvV);


  for(int j=0; j<nSingular; j++) {
      SVDGetSingularTriplet( svd, j, &sigma, u, v );
      BVInsertVec(bvU, j, u);
      BVInsertVec(bvV, j, v);
      sig[j] = sigma;
  }

  // Checking how many singular values must be retained
  int nOfRetained = 0;
  if(!ABSOLUTE){
    nOfRetained = 1;
    for(int h=1; h<nSingular; h++){
        if(sig[h] > sig[0]*tol){
          nOfRetained = nOfRetained + 1;
        }
    }
  }
  else{
    for(int h=0; h<nSingular; h++){
      if(sig[h] > tol){
        nOfRetained = nOfRetained + 1;
      }
    }
  }

  // Computing the output matrices:
  initMat(U, nRowU, nOfRetained, theComm);
  initMat(V, nRowV, nOfRetained, theComm);
  initVec(S, nOfRetained, theComm);

  for (int j=0; j<nOfRetained; j++) {
      BVGetColumn(bvU, j, &u);
      setMatCol(U, j, u, theComm);
      BVRestoreColumn(bvU, j, &u);
      BVGetColumn(bvV, j, &v);
      setMatCol(V, j, v, theComm);
      BVRestoreColumn(bvV, j, &v);
      setVecEl(S, j, sig[j], theComm);
  }
  finalizeMat(U);
  finalizeMat(V);
  finalizeVec(S);

  VecDestroy(&u);
  VecDestroy(&v);
  BVDestroy(&bvU);
  BVDestroy(&bvV);
}


/* SOLVE SVD by naive CYCLIC Golub-Kahan
  - Inputs: the matrix A
    the matrices U,V
    the vector S
*/

void svdGK(Mat& A, Mat& U, Mat& V, Vec& S, MPI_Comm theComm, EPSType type=EPSKRYLOVSCHUR){
  int nRows, nCols;
  MatGetSize(A, &nRows, &nCols);
  Mat B;
  int sizeB = nRows+nCols;
  initMat(B, sizeB, sizeB, theComm);
  for(int i=0; i<nRows; i++){
    for(int j=0; j<nCols; j++){
      double value = getMatEl(A,i,j,theComm);
      setMatEl(B, i+nCols, j, value, theComm);
      setMatEl(B, j, i+nCols, value, theComm);
    }
  }
  finalizeMat(B);
  EPS epsGK;
  Mat F;
  Vec SigGK;
  initEps(epsGK, B, EPS_HEP, type, theComm);
  solveHermEPS(epsGK, B, F, SigGK, theComm);
  int nConv;
  VecGetSize(SigGK,&nConv);

  int nOfTriplets = 0;
  for(int idS=0; idS<nConv; idS++){
    double singVal = getVecEl(SigGK,idS,theComm);
    // if it is positive, retrieve its value and vectors
    if(singVal >= 1e-18){
      nOfTriplets += 1;
    }
  }

  initVec(S,nOfTriplets,theComm);
  initMat(U,nRows,nOfTriplets,theComm);
  initMat(V,nCols,nOfTriplets,theComm);

  int counter = 0;
  for(int idS=0; idS<nConv; idS++){
    double singVal = getVecEl(SigGK,idS,theComm);
    // if it is positive, retrieve its value and vectors
    if(singVal >= 1e-18){
      setVecEl(S,counter,singVal,theComm);
      for(int i=0; i<nRows; i++){
        double Uval = getMatEl(F,nCols+i,idS, theComm);
        setMatEl(U,i,counter,Uval,theComm);
      }
      for(int j=0; j<nCols; j++){
        double Vval = getMatEl(F,j,idS, theComm);
        setMatEl(V,j,counter,Vval,theComm);
      }
      counter += 1;
    }
  }
  //cout << "Counter = " << counter << " -- nOfT = " << nOfTriplets << endl;
  finalizeVec(S);
  finalizeMat(U);
  finalizeMat(V);
}



/* SOLVE SVD by naive LHC like algorithm
  - Inputs: the matrix A
    the matrices U,V
    the vector S
  - Outputs thin SVD: U,S,V
  - Default tolerances are used (both for QR and SVD bidiagonalization)
*/

void svdLHC(Mat& A, Mat& U, Mat& V, Vec& S, MPI_Comm theComm){

  int nRows, nCols;
  MatGetSize(A, &nRows, &nCols);
  const double tolQR = 2.22e-16;

  if(nRows>=nCols){
    // First step of LHC: compute a QR factorisation
    Mat Q,R;
    initMat(Q,nRows,nCols,theComm);
    finalizeMat(Q);
    initMat(R,nCols,nCols,theComm);
    finalizeMat(R);

    // Create a set of vectors, copy the columns of A:
    vector<Vec> theCols;
    theCols.resize(nCols);
    Vec left;
    for(int idCol=0; idCol<nCols; idCol++){
      MatCreateVecs(A,&left,&theCols[idCol]);
      MatGetColumnVector(A,theCols[idCol],idCol);
    }
    VecDestroy(&left);

    //Starting the MGS algorithm:
    vector<int> toRetrieve;
    for(int i=0; i<nCols; i++){
      double r_ii;
      VecNorm(theCols[i],NORM_2,&r_ii);
      double coeff = 0.0;
      if(r_ii>tolQR){
        toRetrieve.push_back(i);
        setMatEl(R, i, i, r_ii, theComm);
        coeff = 1.0/r_ii;
        for(int idRow=0; idRow<nRows; idRow++){
          double qValue = getVecEl(theCols[i],idRow,theComm);
          qValue = qValue*coeff;
          setMatEl(Q, idRow, i, qValue,theComm);
        }
      }
      else{
        //setMatEl(R, i, i, 0.0, theComm); //uncomment to obtain dense R
      }
      // Computing the off-diagonal part of R
      for(int j=i+1;j<nCols;j++){
        double r_ij;
        if(coeff>0){
          VecDot(theCols[i],theCols[j],&r_ij);
          r_ij = r_ij*coeff;
          setMatEl(R,i,j,r_ij,theComm);
          double scale = -1.0*r_ij*coeff;
          VecAXPY(theCols[j],scale,theCols[i]);
        }
        else{
          //setMatEl(R,i,j,0.0,theComm); //uncomment to obtain a dense R
        }
      }
    }
    finalizeMat(Q);
    finalizeMat(R);

    int rank = toRetrieve.size();
    /* if the matrix is close to the zero matrix, return a
       zero rank in the form of a null pointer.
    */
    if (rank == 0){
      U = PETSC_NULL;
      V = PETSC_NULL;
      S = PETSC_NULL;
      return;
    }

    // toRetrieve contains the Ids of the Columns of Q and the rows of R
    Mat redR;
    initMat(redR, rank, nCols, theComm);
    for(int idRow=0; idRow<rank; idRow++){
      int theRowIndex = toRetrieve[idRow];
      for(int idCol=idRow; idCol<nCols;idCol++){
        double valueToInsert = getMatEl(R, theRowIndex, idCol, theComm);
        setMatEl(redR, idRow, idCol, valueToInsert, theComm);
      }
    }
    finalizeMat(redR);
    // bidiagonalization of the reduced R
    SVD svd;
    Mat Ur;
    initSVD(svd, redR, rank, 1.0e-18, theComm, SVDTRLANCZOS);
    solveSVD(svd, redR, Ur, V, S, theComm);

    int nOfTriplets;
    VecGetSize(S, &nOfTriplets);

    // the right vectors:
    Mat redQ;
    initMat(redQ, nRows, rank, theComm);
    for(int idCol=0; idCol<rank; idCol++){
      Vec tmpL,tmpR;
      MatCreateVecs(Q,&tmpL,&tmpR);
      int toTake = toRetrieve[idCol];
      MatGetColumnVector(Q,tmpR,toTake);
      setMatCol(redQ, idCol, tmpR, theComm);
      VecDestroy(&tmpL);
    }
    finalizeMat(redQ);

    initMat(U, nRows, nOfTriplets, theComm);
    finalizeMat(U);
    MatMatMult(redQ,Ur,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&U);

    for(int i=0; i<nCols; i++){
      VecDestroy(&theCols[i]);
    }
  }
  // the case in which nCols > nRows
  else{
    // First step of LHC: compute a QR factorisation of the transposed
    Mat Q,R;
    initMat(Q,nCols,nRows,theComm);
    finalizeMat(Q);
    initMat(R,nRows,nRows,theComm);
    finalizeMat(R);

    // Create a set of vectors, copy the rows of A:
    vector<Vec> theRows;
    theRows.resize(nRows);
    Vec right;
    for(int idRow=0; idRow<nRows; idRow++){
      MatCreateVecs(A,&theRows[idRow],&right);
      const PetscInt* nonZeroColsA;
      const PetscScalar* theRowOfA;
      MatGetRow(A, idRow, &nCols , &nonZeroColsA, &theRowOfA);
      VecSetValues(theRows[idRow],nCols,nonZeroColsA,theRowOfA,INSERT_VALUES);
      finalizeVec(theRows[idRow]);
    }
    VecDestroy(&right);

    //Starting the MGS algorithm:
    vector<int> toRetrieve;
    for(int i=0; i<nRows; i++){
      double r_ii;
      VecNorm(theRows[i],NORM_2,&r_ii);
      double coeff = 0.0;
      if(r_ii>tolQR){
        toRetrieve.push_back(i);
        setMatEl(R, i, i, r_ii, theComm);
        coeff = 1.0/r_ii;
        for(int idCol=0; idCol<nCols; idCol++){
          double qValue = getVecEl(theRows[i],idCol,theComm);
          qValue = qValue*coeff;
          setMatEl(Q, idCol, i, qValue,theComm);
        }
      }
      else{
        //setMatEl(R, i, i, 0.0, theComm); //uncomment to obtain dense R
      }
      // Computing the off-diagonal part of R
      for(int j=i+1;j<nRows;j++){
        double r_ij;
        if(coeff>0){
          VecDot(theRows[i],theRows[j],&r_ij);
          r_ij = r_ij*coeff;
          setMatEl(R,i,j,r_ij,theComm);
          double scale = -1.0*r_ij*coeff;
          VecAXPY(theRows[j],scale,theRows[i]);
        }
        else{
          //setMatEl(R,i,j,0.0,theComm); //uncomment to obtain a dense R
        }
      }
    }
    finalizeMat(Q);
    finalizeMat(R);


    // Computing the transposed redR:
    int rank = toRetrieve.size();
    /* if the matrix is close to the zero matrix, return a
       zero rank in the form of a null pointer.
    */
    if (rank == 0){
      U = PETSC_NULL;
      V = PETSC_NULL;
      S = PETSC_NULL;
      return;
    }

    // toRetrieve contains the Ids of the Columns of Q^T and the rows of R^T
    // compute the reduced entries of R^T
    Mat redRT;
    initMat(redRT, nRows, rank, theComm);
    for(int idRow=0; idRow<rank; idRow++){
      int theRowIndex = toRetrieve[idRow];
      for(int idCol=idRow; idCol<nRows;idCol++){
        double valueToInsert = getMatEl(R, theRowIndex, idCol, theComm);
        setMatEl(redRT, idCol, idRow, valueToInsert, theComm);
      }
    }
    finalizeMat(redRT);

    // compute the SVD of RT: outputs U,S directly obtained.
    SVD svd;
    Mat Vr;
    initSVD(svd, redRT, rank, 1.0e-18, theComm, SVDTRLANCZOS);
    solveSVD(svd, redRT, U, Vr, S, theComm);

    int nOfTriplets;
    VecGetSize(S, &nOfTriplets);

    Mat redQ;
    initMat(redQ, nCols, rank, theComm);
    for(int idCol=0; idCol<rank; idCol++){
      Vec tmpL,tmpR;
      MatCreateVecs(Q,&tmpL,&tmpR);
      int toTake = toRetrieve[idCol];
      MatGetColumnVector(Q,tmpR,toTake);
      setMatCol(redQ, idCol, tmpR, theComm);
      VecDestroy(&tmpL);
    }
    finalizeMat(redQ);

    initMat(V, nCols, nOfTriplets, theComm);
    finalizeMat(V);
    MatMatMult(redQ,Vr,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&V);
  }

}



/* SOLVE SVD by naive LHC like algorithm
  - Inputs: the matrix A
    the matrices U,V
    the vector S
  - Outputs thin SVD: U,S,V
  - Specify tolerances
*/

void svdLHC(Mat& A, Mat& U, Mat& V, Vec& S, double tol, MPI_Comm theComm){

  int nRows, nCols;
  MatGetSize(A, &nRows, &nCols);
  const double tolQR = tol;

  if(nRows>=nCols){
    // First step of LHC: compute a QR factorisation
    Mat Q,R;
    initMat(Q,nRows,nCols,theComm);
    finalizeMat(Q);
    initMat(R,nCols,nCols,theComm);
    finalizeMat(R);

    // Create a set of vectors, copy the columns of A:
    vector<Vec> theCols;
    theCols.resize(nCols);
    Vec left;
    for(int idCol=0; idCol<nCols; idCol++){
      MatCreateVecs(A,&left,&theCols[idCol]);
      MatGetColumnVector(A,theCols[idCol],idCol);
    }
    VecDestroy(&left);

    //Starting the MGS algorithm:
    vector<int> toRetrieve;
    for(int i=0; i<nCols; i++){
      double r_ii;
      VecNorm(theCols[i],NORM_2,&r_ii);
      double coeff = 0.0;
      if(r_ii>tolQR){
        toRetrieve.push_back(i);
        setMatEl(R, i, i, r_ii, theComm);
        coeff = 1.0/r_ii;
        for(int idRow=0; idRow<nRows; idRow++){
          double qValue = getVecEl(theCols[i],idRow,theComm);
          qValue = qValue*coeff;
          setMatEl(Q, idRow, i, qValue,theComm);
        }
      }
      else{
        //setMatEl(R, i, i, 0.0, theComm); //uncomment to obtain dense R
      }
      // Computing the off-diagonal part of R
      for(int j=i+1;j<nCols;j++){
        double r_ij;
        if(coeff>0){
          VecDot(theCols[i],theCols[j],&r_ij);
          r_ij = r_ij*coeff;
          setMatEl(R,i,j,r_ij,theComm);
          double scale = -1.0*r_ij*coeff;
          VecAXPY(theCols[j],scale,theCols[i]);
        }
        else{
          //setMatEl(R,i,j,0.0,theComm); //uncomment to obtain a dense R
        }
      }
    }
    finalizeMat(Q);
    finalizeMat(R);

    int rank = toRetrieve.size();
    /* if the matrix is close to the zero matrix, return a
       zero rank in the form of a null pointer.
    */
    if (rank == 0){
      U = PETSC_NULL;
      V = PETSC_NULL;
      S = PETSC_NULL;
      return;
    }

    // toRetrieve contains the Ids of the Columns of Q and the rows of R
    Mat redR;
    initMat(redR, rank, nCols, theComm);
    for(int idRow=0; idRow<rank; idRow++){
      int theRowIndex = toRetrieve[idRow];
      for(int idCol=idRow; idCol<nCols;idCol++){
        double valueToInsert = getMatEl(R, theRowIndex, idCol, theComm);
        setMatEl(redR, idRow, idCol, valueToInsert, theComm);
      }
    }
    finalizeMat(redR);
    // bidiagonalization of the reduced R
    SVD svd;
    Mat Ur;
    initSVD(svd, redR, rank, tol, theComm, SVDTRLANCZOS);
    solveSVD(svd, redR, Ur, V, S, theComm);

    int nOfTriplets;
    VecGetSize(S, &nOfTriplets);

    // the right vectors:
    Mat redQ;
    initMat(redQ, nRows, rank, theComm);
    for(int idCol=0; idCol<rank; idCol++){
      Vec tmpL,tmpR;
      MatCreateVecs(Q,&tmpL,&tmpR);
      int toTake = toRetrieve[idCol];
      MatGetColumnVector(Q,tmpR,toTake);
      setMatCol(redQ, idCol, tmpR, theComm);
      VecDestroy(&tmpL);
    }
    finalizeMat(redQ);

    initMat(U, nRows, nOfTriplets, theComm);
    finalizeMat(U);
    MatMatMult(redQ,Ur,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&U);

    for(int i=0; i<nCols; i++){
      VecDestroy(&theCols[i]);
    }
  }
  // the case in which nCols > nRows
  else{
    // First step of LHC: compute a QR factorisation of the transposed
    Mat Q,R;
    initMat(Q,nCols,nRows,theComm);
    finalizeMat(Q);
    initMat(R,nRows,nRows,theComm);
    finalizeMat(R);

    // Create a set of vectors, copy the rows of A:
    vector<Vec> theRows;
    theRows.resize(nRows);
    Vec right;
    for(int idRow=0; idRow<nRows; idRow++){
      MatCreateVecs(A,&theRows[idRow],&right);
      const PetscInt* nonZeroColsA;
      const PetscScalar* theRowOfA;
      MatGetRow(A, idRow, &nCols , &nonZeroColsA, &theRowOfA);
      VecSetValues(theRows[idRow],nCols,nonZeroColsA,theRowOfA,INSERT_VALUES);
      finalizeVec(theRows[idRow]);
    }
    VecDestroy(&right);

    //Starting the MGS algorithm:
    vector<int> toRetrieve;
    for(int i=0; i<nRows; i++){
      double r_ii;
      VecNorm(theRows[i],NORM_2,&r_ii);
      double coeff = 0.0;
      if(r_ii>tolQR){
        toRetrieve.push_back(i);
        setMatEl(R, i, i, r_ii, theComm);
        coeff = 1.0/r_ii;
        for(int idCol=0; idCol<nCols; idCol++){
          double qValue = getVecEl(theRows[i],idCol,theComm);
          qValue = qValue*coeff;
          setMatEl(Q, idCol, i, qValue,theComm);
        }
      }
      else{
        //setMatEl(R, i, i, 0.0, theComm); //uncomment to obtain dense R
      }
      // Computing the off-diagonal part of R
      for(int j=i+1;j<nRows;j++){
        double r_ij;
        if(coeff>0){
          VecDot(theRows[i],theRows[j],&r_ij);
          r_ij = r_ij*coeff;
          setMatEl(R,i,j,r_ij,theComm);
          double scale = -1.0*r_ij*coeff;
          VecAXPY(theRows[j],scale,theRows[i]);
        }
        else{
          //setMatEl(R,i,j,0.0,theComm); //uncomment to obtain a dense R
        }
      }
    }
    finalizeMat(Q);
    finalizeMat(R);


    // Computing the transposed redR:
    int rank = toRetrieve.size();
    /* if the matrix is close to the zero matrix, return a
       zero rank in the form of a null pointer.
    */
    if (rank == 0){
      U = PETSC_NULL;
      V = PETSC_NULL;
      S = PETSC_NULL;
      return;
    }

    // toRetrieve contains the Ids of the Columns of Q^T and the rows of R^T
    // compute the reduced entries of R^T
    Mat redRT;
    initMat(redRT, nRows, rank, theComm);
    for(int idRow=0; idRow<rank; idRow++){
      int theRowIndex = toRetrieve[idRow];
      for(int idCol=idRow; idCol<nRows;idCol++){
        double valueToInsert = getMatEl(R, theRowIndex, idCol, theComm);
        setMatEl(redRT, idCol, idRow, valueToInsert, theComm);
      }
    }
    finalizeMat(redRT);

    // compute the SVD of RT: outputs U,S directly obtained.
    SVD svd;
    Mat Vr;
    initSVD(svd, redRT, rank, tol, theComm, SVDTRLANCZOS);
    solveSVD(svd, redRT, U, Vr, S, theComm);

    int nOfTriplets;
    VecGetSize(S, &nOfTriplets);

    Mat redQ;
    initMat(redQ, nCols, rank, theComm);
    for(int idCol=0; idCol<rank; idCol++){
      Vec tmpL,tmpR;
      MatCreateVecs(Q,&tmpL,&tmpR);
      int toTake = toRetrieve[idCol];
      MatGetColumnVector(Q,tmpR,toTake);
      setMatCol(redQ, idCol, tmpR, theComm);
      VecDestroy(&tmpL);
    }
    finalizeMat(redQ);

    initMat(V, nCols, nOfTriplets, theComm);
    finalizeMat(V);
    MatMatMult(redQ,Vr,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&V);
  }

}







// Solve SVD by EPS: to be avoided except for particular cases
/*
 It passes through A^T A
 It uses Lanczos method to compute the eigenvalue decomposition if not otherwise stated
 Threshold to 1.0e-15 (numerical propagation of errors)
*/
void solveSVDbyEPS(Mat& A, Mat& U, Mat& V, Vec& S, MPI_Comm theComm, EPSType solverType=EPSLANCZOS){
    int nRows, nCols;
    MatGetSize(A, &nRows, &nCols);

    EPS eps;
    if(nRows >= nCols){
      Mat C;
      initMat(C, nCols, nCols, theComm);
      finalizeMat(C);
      Mat AT = transpose(A, theComm);
      MatMatMult(AT, A, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &C);
      initEps(eps, C, EPS_HEP, solverType, theComm);
      Mat EV;
      Vec lambda;
      solveHermEPS(eps, C, EV, lambda, theComm);

      int nOfLambda;
      VecGetSize(lambda,&nOfLambda);

      // if V is computed through other methods it might be non-square.
      initMat(V, nCols, nOfLambda, theComm);
      MatCopy(EV, V, DIFFERENT_NONZERO_PATTERN);
      finalizeMat(V);

      initVec(S, nOfLambda, theComm);
      Vec invSigma;
      initVec(invSigma, nOfLambda, theComm);
      for(int j=0; j<nOfLambda; j++){
        double lam = getVecEl(lambda, j, theComm);
        // lambda>=0, consider few times the machine precision as threshold.
        if(lam<1.0e-15){
          lam=0.0;
        }
        lam = sqrt(lam);
        setVecEl(S, j, lam, theComm);
        if(lam >= 1.0e-15){
          setVecEl(invSigma, j, 1.0/lam, theComm);
        }
        else{
          setVecEl(invSigma, j, 0.0, theComm);
        }
      }
      finalizeVec(S);
      finalizeVec(invSigma);
      Mat invS = diag(invSigma, theComm);

      initMat(U, nRows, nOfLambda, theComm);
      MatMatMatMult(A, V, invS, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &U );
      finalizeMat(U);

      // cleanup:
      MatDestroy(&invS);
      VecDestroy(&invSigma);
      VecDestroy(&lambda);
      MatDestroy(&EV);
      MatDestroy(&AT);
      MatDestroy(&C);
    }
    else{
      Mat C;
      initMat(C, nRows, nRows, theComm);
      finalizeMat(C);
      Mat AT = transpose(A, theComm);
      MatMatMult(A, AT, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &C);
      initEps(eps, C, EPS_HEP, solverType, theComm);

      Mat EV;
      Vec lambda;
      solveHermEPS(eps, C, EV, lambda, theComm);

      int nOfLambda;
      VecGetSize(lambda,&nOfLambda);

      initMat(U, nRows, nOfLambda, theComm);
      MatCopy(EV, U, DIFFERENT_NONZERO_PATTERN);
      finalizeMat(U);


      initVec(S, nOfLambda, theComm);
      Vec invSigma;
      initVec(invSigma, nOfLambda, theComm);
      for(int j=0; j<nOfLambda; j++){
        double lam = getVecEl(lambda, j, theComm);
        if(lam<1.0e-15){
          lam=0.0;
        }
        lam = sqrt(lam);
        setVecEl(S, j, lam, theComm);
        if(lam >= 1.0e-15){
          setVecEl(invSigma, j, 1.0/lam, theComm);
        }
        else{
          setVecEl(invSigma, j, 0.0, theComm);
        }
      }
      finalizeVec(S);
      finalizeVec(invSigma);
      Mat invS = diag(invSigma, theComm);

      initMat(V, nCols, nOfLambda, theComm);
      MatMatMatMult(AT, U, invS, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &V );
      finalizeMat(V);

      // cleanup:
      MatDestroy(&invS);
      VecDestroy(&invSigma);
      VecDestroy(&lambda);
      MatDestroy(&EV);
      MatDestroy(&AT);
      MatDestroy(&C);
    }
    EPSDestroy(&eps);
}



/* overloaded to take a threshold and compress:
 It passes through A^T A
 It uses Lanczos method to compute the eigenvalue decomposition, if not otherwise stated
 compress up to tol
*/
void solveSVDbyEPS(Mat& A, Mat& U, Mat& V, Vec& S, double tol, MPI_Comm theComm, EPSType solverType=EPSLANCZOS){
    int nRows, nCols;
    MatGetSize(A, &nRows, &nCols);

    EPS eps;
    if(nRows >= nCols){
      Mat C;
      initMat(C, nCols, nCols, theComm);
      finalizeMat(C);
      Mat AT = transpose(A, theComm);
      MatMatMult(AT, A, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &C);
      initEps(eps, C, EPS_HEP, solverType, theComm);
      Mat EV;
      Vec lambda;
      solveHermEPS(eps, C, EV, lambda, theComm);

      int nOfLambda;
      VecGetSize(lambda,&nOfLambda);

      // compute how many eigevalues have to be retained:
      int toRetain = 0;
      for(unsigned int idLam=0; idLam<nOfLambda; idLam++){
        double lam = getVecEl(lambda, idLam, theComm);
        if(lam>tol*tol){
          toRetain += 1;
        }
      }

      // if V is computed through other methods it might be non-square.
      initMat(V, nCols, nOfLambda, theComm);
      MatCopy(EV, V, DIFFERENT_NONZERO_PATTERN);
      finalizeMat(V);

      initVec(S, nOfLambda, theComm);
      Vec invSigma;
      initVec(invSigma, nOfLambda, theComm);
      for(int j=0; j<nOfLambda; j++){
        double lam = getVecEl(lambda, j, theComm);
        // lambda>=0, consider few times the machine precision as threshold.
        if(lam<tol){
          lam=0.0;
        }
        lam = sqrt(lam);
        setVecEl(S, j, lam, theComm);
        if(lam >= tol){
          setVecEl(invSigma, j, 1.0/lam, theComm);
        }
        else{
          setVecEl(invSigma, j, 0.0, theComm);
        }
      }
      finalizeVec(S);
      finalizeVec(invSigma);
      Mat invS = diag(invSigma, theComm);

      initMat(U, nRows, nOfLambda, theComm);
      MatMatMatMult(A, V, invS, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &U );
      finalizeMat(U);

      // cleanup:
      MatDestroy(&invS);
      VecDestroy(&invSigma);
      VecDestroy(&lambda);
      MatDestroy(&EV);
      MatDestroy(&AT);
      MatDestroy(&C);
    }
    else{
      Mat C;
      initMat(C, nRows, nRows, theComm);
      finalizeMat(C);
      Mat AT = transpose(A, theComm);
      MatMatMult(A, AT, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &C);
      initEps(eps, C, EPS_HEP, solverType, theComm);

      Mat EV;
      Vec lambda;
      solveHermEPS(eps, C, EV, lambda, theComm);

      int nOfLambda;
      VecGetSize(lambda,&nOfLambda);

      initMat(U, nRows, nOfLambda, theComm);
      MatCopy(EV, U, DIFFERENT_NONZERO_PATTERN);
      finalizeMat(U);


      initVec(S, nOfLambda, theComm);
      Vec invSigma;
      initVec(invSigma, nOfLambda, theComm);
      for(int j=0; j<nOfLambda; j++){
        double lam = getVecEl(lambda, j, theComm);
        if(lam<tol){
          lam=0.0;
        }
        lam = sqrt(lam);
        setVecEl(S, j, lam, theComm);
        if(lam >= tol){
          setVecEl(invSigma, j, 1.0/lam, theComm);
        }
        else{
          setVecEl(invSigma, j, 0.0, theComm);
        }
      }
      finalizeVec(S);
      finalizeVec(invSigma);
      Mat invS = diag(invSigma, theComm);

      initMat(V, nCols, nOfLambda, theComm);
      MatMatMatMult(AT, U, invS, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &V );
      finalizeMat(V);

      // cleanup:
      MatDestroy(&invS);
      VecDestroy(&invSigma);
      VecDestroy(&lambda);
      MatDestroy(&EV);
      MatDestroy(&AT);
      MatDestroy(&C);
    }
    EPSDestroy(&eps);
}





// end of class
};


#endif
