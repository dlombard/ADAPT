#include "Node.h"
#include<algorithm>
#include"auxiliary.h"

using namespace std;


Node::Node(vector< vector<unsigned int> >& vec_ind)
{
	num_var_ = vec_ind.size();
	indices_ = vec_ind;
	parent_ = NULL;
	children_= vector<Node*>(0);
}

Node::Node(vector< vector<unsigned int> >& vec_ind, const int& numvar)
{
	num_var_ = numvar;
	indices_ = vec_ind;
	parent_ = NULL;
	children_= vector<Node*>(0);
}



int Node::countNodesRec(Node* root, int& count)
{   
    Node* parent = root;
    Node* child = NULL;


    for(int it = 0; it < parent->childrenNumber(); it++)
    {   
 
        child = parent->getChild(it);
        count++;


        if(child->childrenNumber() > 0)
        {   
            countNodesRec(child, count);
        } 
    }

    return count;
}

void Node::appendChild(Node *child)
{       
    child->setParent(this);
    children_.push_back(child);
}

void Node::setParent(Node *theParent)
{
    parent_ = theParent;
}

void Node::popBackChild()
{
    children_.pop_back();
}

void Node::removeChildren()
{
	
   auxiliary z;
    
	
    vector<vector<unsigned int> > vec_ind = getIndicesFromLeafs();

    indices_ = vec_ind; 

    children_= vector<Node*>(0);
}


void Node::removeChild(int pos)
{   
    if(children_.size() > 0) {
        children_.erase(children_.begin()+ pos);
    }
    else {
        children_.pop_back();
    }
}

bool Node::hasChildren()
{
    if(children_.size() > 0)
        return true;
    else
        return false;
}

bool Node::hasParent()
{
    if(parent_ != NULL)
        return true;
    else 
        return false;
}

Node* Node::getParent()
{
    return parent_;
}

Node* Node::getChild(int pos)
{   
    if(children_.size() < pos)
        return NULL;
    else
        return children_[pos];
}

int Node::childrenNumber()
{
    return children_.size();
}

int Node::grandChildrenNum()
{   
    int t = 0;

    if(children_.size() < 1)
    {
        return 0;
    }

    countNodesRec(this, t);

    return t;
}



void Node::addLeafs(vector<Node* >& vec)
{
		


	if(children_.size() == 0)
	{
		vec.push_back(this);
	}
	else
	{

		for (int j=0; j<children_.size(); j++)
		{
			Node* childj = children_[j]; 
			if (!(childj->hasChildren()))
			{
				vec.push_back(childj);	
			}
			else
			{
				childj->addLeafs(vec);
			}
		}
	}
}

vector<Node*> Node::getLeafs()
{

	vector<Node *> vec(0); 
	addLeafs(vec); 
	return vec; 
}


void Node::createDyadicTree(const int& N)
{
	//Construit un arbre dyadique de profondeur N a partir d'un noeud root.
	int count = 0; 
	
	// On commence par enlever les éventuels enfants
 
	if (hasChildren())
	{
		removeChildren();
	}

	while (count < N)
	{

		vector<Node* > leafs = getLeafs(); 

		for (int i =0; i<leafs.size(); i++)
		{
			Node* leafi = leafs[i]; 
			vector< vector<unsigned int> > veci = leafi->getIndices();

			assert(veci.size()==num_var_);

			vector<unsigned int> iterator(num_var_,0);
			int it = 0; 
			while (iterator[num_var_-1] < 1.5)
			{

				vector< vector<unsigned int> > indices(num_var_); 
				for (int j=0; j< num_var_; j++)
				{

					int Nj = veci[j].size(); 

					if (iterator[j] == 0)
					{

						int Mj = Nj/2; 
						vector<unsigned int> subvecj = subvector(0,Mj,veci[j]); 

						indices[j] = subvecj;

						
					}
					else
					{
						int Mj = Nj/2; 
						vector<unsigned int> subvecj = subvector(Mj,(Nj-Mj),veci[j]); 

						indices[j] = subvecj;
					}

				}
				Node* nit = new Node(indices); 

				leafi->appendChild(nit);


				//On update iterator
				iterator[0] = iterator[0] +1; 
				int cour = 0; 
				while( (iterator[cour] > 1.5) && (cour < num_var_-1))
				{	
					iterator[cour] = 0; 
					iterator[cour+1] = iterator[cour+1] +1; 
					cour = cour+1; 
					
				}	
				
				

			}

			//Pour gagner de la mémoire, on pourrait effacer les indices de leafi
			leafi->eraseIndices();

		}

		count++; 
	}	

}

//Split une feuille en 2^d enfants
void Node::splitLeaf(Node* leafi)
{

	vector<Node* > leafs = getLeafs(); 

	int i = 0; 
        isIn(leafi, leafs, i);
        /* indice de la feuille leaf dans la liste des feuilles*/
	//Node* leafi = leafs[i]; 
	
	vector< vector<unsigned int> > veci = leafi->getIndices();

	assert(veci.size()==num_var_);

	vector<unsigned int> iterator(num_var_,0);
	int it = 0; 
	while (iterator[num_var_-1] < 1.5)
	{
		
		vector< vector<unsigned int> > indices(num_var_); 
		for (int j=0; j< num_var_; j++)
		{

			int Nj = veci[j].size(); 

			if (iterator[j] == 0)
			{

				int Mj = Nj/2; 
				vector<unsigned int> subvecj = subvector(0,Mj,veci[j]); 

				indices[j] = subvecj;

						
			}
			else
			{
				int Mj = Nj/2; 
				vector<unsigned int> subvecj = subvector(Mj,(Nj-Mj),veci[j]); 

				indices[j] = subvecj;
			}

		}
		Node* nit = new Node(indices); 

		leafi->appendChild(nit);


		//On update iterator
		iterator[0] = iterator[0] +1; 
		int cour = 0; 
		while( (iterator[cour] > 1.5) && (cour < num_var_-1))
		{	
			iterator[cour] = 0; 
			iterator[cour+1] = iterator[cour+1] +1; 
			cour = cour+1; 				
		}	
				
				

	}

	//Pour gagner de la mémoire, on efface les indices de leafi
	leafi->eraseIndices();	

}









vector<unsigned int> Node::subvector(const int& beg, const int& len, const vector<unsigned int>& vec)
{
	
	vector<unsigned int> subvec(0); 

	for (int i=beg; i<beg+len; i++)
	{
		subvec.push_back(vec[i]);
	}
	
	return subvec;

}



vector<vector<unsigned int> > Node::getIndicesFromLeafs()
{
	auxiliary z;

	vector<Node* > leafs = getLeafs(); 


	vector< vector<unsigned int> > indices(num_var_);

	for (int j=0; j<num_var_; j++)
	{
		vector<unsigned int> vecj(0);  
		
		for (int i =0; i<leafs.size(); i++)
		{

			
			vector<unsigned int> oldvecj = vecj; 


			vector<unsigned int> vecij = leafs[i]->getVecInd(j);

			z.mergeSortInt(oldvecj, vecij, vecj);

		}

		sort(vecj.begin(), vecj.end());
		
		indices[j] = vecj; 

	}

	return indices;

}


vector<Node* > Node::getParentsOfLeafs(vector<unsigned int>& leaf_list)
{

	vector<Node* > leafs = getLeafs(); 

	vector<Node*> parents(0); 
	leaf_list.resize(0);

		
	for (int i =0; i<leafs.size(); i++)
	{

		if (leafs[i]->hasParent())
		{
			Node* parenti = leafs[i]->getParent(); 
			if (parenti ==NULL)
			{	
				cout << "There is a big problem" << endl;
			}			


			int k;
			isIn(parenti, parents, k);

			if (k<0)
			{
				parents.push_back(parenti); 
				leaf_list.push_back(i);
			}
		}

	}


	return parents;

}




Node* Node::createNodeCopy()
{

	vector< vector<unsigned int> > indices = getIndices(); 
	int numvar = num_var_;
	Node* newnode = new Node(indices, numvar);


	if (hasChildren())
	{

		for (int i=0; i<children_.size(); i++)
		{

			Node* newchildi = children_[i]->createNodeCopy();
			newnode->appendChild(newchildi);
						
		}
	}

	return newnode;
}


void Node::isIn(const Node* node, const vector<Node*> vecnode, int& k)
{
	k = -1; 
	for (int j=0; j<vecnode.size(); j++)
	{
		if (vecnode[j] == node)
		{
			k=j;
		}
	}
}


void Node::visualize(const string& path)
{
	vector<Node*> leafs = getLeafs(); 

	ofstream myfile;
	string name = path+"_leafs.txt";
	const char* name2 = name.c_str();  
  	myfile.open(name2);
	//myfilegx.precision(16); 

	for (int i=0; i<leafs.size(); i++)
	{
		Node* nodei = leafs[i]; 
		vector<vector<unsigned int> > veci = nodei->getIndices(); 

		myfile << i << " " ;

		for (int j=0; j<num_var_; j++)
		{
			vector<unsigned int> vecj = veci[j]; 
			myfile << vecj[0] << " " << vecj[vecj.size()-1] << " ";
		} 

		myfile << endl; 

	}

	myfile.close();	

}


void Node::whichLeaf(const vector<unsigned int>& indvec, Node* leafind, vector<unsigned int>& new_vec)
{

	Node* current_node = this; 
	new_vec = indvec;
	while(current_node->hasChildren())
	{
		int numchild = current_node->childrenNumber();
		bool IsFound = false; 
		for (int i=0; i<numchild; i++)
		{
			Node* nodei = current_node->getChild(i);
			vector< vector<unsigned int> > vecind = nodei->getIndicesFromLeafs();
			bool isInThisOne = true; 
			for (int j=0; j<num_var_; j++)
			{
				isInThisOne  = (isInThisOne) && (indvec[j] >=vecind[j][0]) && (indvec[j] <=vecind[j][vecind[j].size()-1]); 
			} 
			if (isInThisOne)
			{
				current_node = nodei;
				i= numchild;
				IsFound= true;
				for (int j=0; j<num_var_; j++)
				{
					new_vec[j] = indvec[j] - vecind[j][0];
				}
			}
		}
		if (IsFound==false) {cout << "There is a big problem!" << endl;}

	}
	leafind = current_node;

} 
