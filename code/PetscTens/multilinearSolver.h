// Class of multi-linear solvers

#ifndef multilinearSolver_h
#define multilinearSolver_h

#include "tensor.h"
#include "CPTensor.h"
#include "genericInclude.h"
#include "operatorTensor.h"
#include "fullTensor.h"

using namespace std;

class multilinearSolver{

private:
   auxiliary a;
public:
  multilinearSolver(){};
  ~multilinearSolver(){};

  // list of methods, actual solvers:
  void CG(operatorTensor&, CPTensor&, CPTensor&, double);
  void CG_compress(operatorTensor&, CPTensor&, CPTensor&, double);

  void greedy_ALS(operatorTensor&, CPTensor&, CPTensor&, const double, const double);


  // Solving with CPTT:
  void solvewithCPTT(operatorTensor&, CPTensor&, CPTensor&, double);
  void solveCPTT_CG(operatorTensor&, CPTensor&, CPTensor&, double);
  void solveCPTT_BiCGStab(operatorTensor&, CPTensor&, CPTensor&, double);

};



#endif
