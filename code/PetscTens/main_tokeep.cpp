// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>

#include "auxiliary.h"
#include "tensor.h"
#include "CoulombTensor.h"
#include "GibbsTensor.h"
#include "GenTabTensor.h"
#include "Node.h"
#include "SubTensor.h"
#include "ApproxTensor.h"
#include "CPTensor.h"
#include "operatorTensor.h"
#include "multilinearSolver.h"

using namespace std;
int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";
    PetscErrorCode ierr;

    SlepcInitialize(&argc,&args,(char*)0,help);
    //PetscInitialize(&argc,&args,(char*)0,help);

    //ierr = SlepcInitialize(&argc,&args,(char*)0,help);

    int numProc;
    ierr = MPI_Comm_size(PETSC_COMM_WORLD,&numProc);
    //MPI_Comm_size(PETSC_COMM_WORLD,&numProc);
    int myId;
    MPI_Comm_rank (PETSC_COMM_WORLD, &myId);


    auxiliary z;

    // Init Problem:
    int num = 2;
    int n0 = 64;
    int n1 = 64;
    const double h0 = 1.0/static_cast<double>(n0-1);
    const double h1 = 1.0/static_cast<double>(n0-1);

    vector<int> vec_dim(2);
    vec_dim[0] = n0;
    vec_dim[1] = n1;

    Mat K_x, K_y, M_x, M_y;
    z.initMat(K_x, n0, n0, PETSC_COMM_WORLD);
    z.setMatEl(K_x, 0, 0, 2.0/h0, PETSC_COMM_WORLD);
    z.setMatEl(K_x, 0, 1,-1.0/h0, PETSC_COMM_WORLD);
    z.setMatEl(K_x, n0-1, n0-1, 2.0/h0, PETSC_COMM_WORLD);
    z.setMatEl(K_x, n0-1, n0-2,-1.0/h0, PETSC_COMM_WORLD);
    for(unsigned int iRow=1; iRow < n0-1; iRow++){
      z.setMatEl(K_x, iRow, iRow-1, -1.0/h0, PETSC_COMM_WORLD);
      z.setMatEl(K_x, iRow, iRow, 2.0/h0, PETSC_COMM_WORLD);
      z.setMatEl(K_x, iRow, iRow+1, -1.0/h0, PETSC_COMM_WORLD);
    }
    z.finalizeMat(K_x);

    z.initMat(K_y, n1, n1, PETSC_COMM_WORLD);
    z.setMatEl(K_y, 0, 0, 2.0/h1, PETSC_COMM_WORLD);
    z.setMatEl(K_y, 0, 1,-1.0/h1, PETSC_COMM_WORLD);
    z.setMatEl(K_y, n1-1, n1-1, 2.0/h1, PETSC_COMM_WORLD);
    z.setMatEl(K_y, n1-1, n1-2,-1.0/h1, PETSC_COMM_WORLD);
    for(unsigned int iRow=1; iRow < n1-1; iRow++){
      z.setMatEl(K_y, iRow, iRow-1, -1.0/h1, PETSC_COMM_WORLD);
      z.setMatEl(K_y, iRow, iRow, 2.0/h1, PETSC_COMM_WORLD);
      z.setMatEl(K_y, iRow, iRow+1, -1.0/h1, PETSC_COMM_WORLD);
    }
    z.finalizeMat(K_y);

    z.initMat(M_x, n0, n0, PETSC_COMM_WORLD);
    z.setMatEl(M_x, 0, 0, 2.0*h0/3.0, PETSC_COMM_WORLD);
    z.setMatEl(M_x, 0, 1, h0/6, PETSC_COMM_WORLD);
    z.setMatEl(M_x, n0-1, n0-1, 2.0*h0/3.0, PETSC_COMM_WORLD);
    z.setMatEl(M_x, n0-1, n0-2, h0/6.0, PETSC_COMM_WORLD);
    for(unsigned int iRow=1; iRow < n0-1; iRow++){
      z.setMatEl(M_x, iRow, iRow-1, h0/6.0, PETSC_COMM_WORLD);
      z.setMatEl(M_x, iRow, iRow, 2.0*h0/3.0, PETSC_COMM_WORLD);
      z.setMatEl(M_x, iRow, iRow+1, h0/6.0, PETSC_COMM_WORLD);
    }
    z.finalizeMat(M_x);

    z.initMat(M_y, n1, n1, PETSC_COMM_WORLD);
    z.setMatEl(M_y, 0, 0, 2.0*h1/3.0, PETSC_COMM_WORLD);
    z.setMatEl(M_y, 0, 1, h1/6, PETSC_COMM_WORLD);
    z.setMatEl(M_y, n1-1, n1-1, 2.0*h1/3.0, PETSC_COMM_WORLD);
    z.setMatEl(M_y, n1-1, n1-2, h1/6.0, PETSC_COMM_WORLD);
    for(unsigned int iRow=1; iRow < n1-1; iRow++){
      z.setMatEl(M_y, iRow, iRow-1, h1/6.0, PETSC_COMM_WORLD);
      z.setMatEl(M_y, iRow, iRow, 2.0*h1/3.0, PETSC_COMM_WORLD);
      z.setMatEl(M_y, iRow, iRow+1, h1/6.0, PETSC_COMM_WORLD);
    }
    z.finalizeMat(M_y);


    // mass operator to compute the rhs.
    vector<vector <Mat> > massOp(1);
    vector<Mat> theMass(2);
    theMass[0] = M_x;
    theMass[1] = M_y;
    massOp[0] = theMass;


    // -Delta operator, stiffness:
    vector<vector <Mat> > stiffOp(2);
    vector<Mat> stiff_x(2);
    stiff_x[0] = K_x;
    stiff_x[1] = M_y;
    stiffOp[0] = stiff_x;
    vector<Mat> stiff_y(2);
    stiff_y[0] = M_x;
    stiff_x[0] = K_y;
    stiffOp[1] = stiff_y;




    ierr = PetscFinalize();
    return 0;
}
