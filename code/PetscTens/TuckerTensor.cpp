// Created by: Damiano & Virginie on ...
#include "TuckerTensor.h"

using namespace std;

// Constructor
TuckerTensor::TuckerTensor(const GenTabTensor& coeff, const vector<Mat>& mat_vec){

	num_var_ = mat_vec.size();
	vector<int> dim_var(num_var_);
	vector<int> ranks(num_var_);
	
	int nRow, nCol;

	for (int i=0; i<num_var_; i++)
	{
		MatGetSize(mat_vec[i],&nRow, &nCol);
		dim_var[i] = nRow;
		ranks[i] = nCol;
	}
	dim_var_ = dim_var;
	ranks_ = ranks;

	coeffs_ = coeff;

	basis_functions_ = mat_vec;


}



void TuckerTensor::eval(const vector<int>& vec_ind, double& res, MPI_Comm theComm){

	int Jtot = 1; 
	for (int i=0; i<num_var_; i++)
	{
		Jtot=Jtot*ranks_[i];
	}

	res = 0; 
	for (int j=0; j<Jtot; j++)
	{


		vector<int> vec_ind_Tu = compute_index_rank(j);

	
		double prod = 1.0;
		for (int i=0; i<num_var_; i++)
		{
			prod = prod*a.getMatEl(basis_functions_[i], vec_ind[i], vec_ind_Tu[i], theComm);

		}
		double mult; 
		coeffs_.eval(vec_ind_Tu, mult, theComm);

		res = res + mult*prod;

	} 
}

vector<int> TuckerTensor::compute_index_rank(const int& j){


	int jcour = j;

	vector<int> vec_ind(num_var_);

	for (int k=0; k<num_var_; k++)
	{
		vec_ind[k] = jcour % ranks_[k]; 
		jcour = (jcour - vec_ind[k])/ranks_[k];

	}

	return vec_ind; 

}
