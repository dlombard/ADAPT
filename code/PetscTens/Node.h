/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/

#ifndef Node_h
#define Node_h

#include "genericInclude.h"
#include "auxiliary.h"


using namespace std;


class Node
{
    private:
	int num_var_; 
        vector< vector<unsigned int> > indices_;
        Node* parent_;
        std::vector<Node*> children_;

        int countNodesRec(Node*, int&);

    public:
        Node(){};
	~Node(){};
        Node(vector< vector<unsigned int> >&);
	Node(vector< vector<unsigned int> >&, const int&);

        void appendChild(Node* child);
        void setParent(Node* parent);

        void popBackChild();
        void removeChild(int pos);
	void removeChildren();

        bool hasChildren();
        bool hasParent();

        Node* getParent();
        Node* getChild(int pos);

        int childrenNumber();
        int grandChildrenNum();

	vector<Node*> getChildren(){return children_;}

        vector< vector<unsigned int> > getIndices(){return indices_;}
	int getDim(const int& j){ return indices_[j].size();}
	vector<unsigned int> getVecInd(const int& j){ return indices_[j];}
	int getNumVar(){return num_var_;}
	vector< vector<unsigned int> > getIndicesFromLeafs();

	void eraseIndices(){vector< vector<unsigned int> > vec(0); indices_ = vec;}

	void addLeafs(vector< Node*>&); 
	vector< Node*> getLeafs();

	vector< Node*> getParentsOfLeafs(vector<unsigned int>&);

	void addLeafsandParentsofLeafs(vector< Node*>&, vector<Node*>&, vector< vector<unsigned int>>&); 
	void getLeafsandParentsofLeafs(vector<Node*>&, vector<Node*>&, vector< vector<unsigned int>>&); 

	void createDyadicTree(const int&);

	vector<unsigned int> subvector(const int&, const int&, const vector<unsigned int>&);

	Node* createNodeCopy();
        void splitLeaf(Node*);

	void isIn(const Node*, const vector<Node*>, int&);

	void visualize(const string&);

        void whichLeaf(const vector<unsigned int>&, Node*, vector<unsigned int>&); 


};

#endif
