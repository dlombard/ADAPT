// Created by: Damiano & Virginie on ...
#include "GaussianTensor.h"

using namespace std;

// Constructor
GaussianTensor::GaussianTensor(const int& num_var, const std::vector<unsigned int>& dim_var, MPI_Comm theComm){
        m_theComm = theComm;
	m_nVar = num_var;
	m_nDof_var = dim_var;
	compute_minc(); 
}




void GaussianTensor::init(const vector<double>& box_inf, const vector<double>& box_sup, const vector<double>& mu, const vector< vector<double> >& cinv){
	
	//Safety checks
	assert(box_inf.size() == m_nVar);
	assert(box_sup.size() == m_nVar);
	for (int i=0; i<m_nVar; i++)
	{
		assert(box_inf[i]<box_sup[i]);
	}
		

	box_inf_ = box_inf; 
	box_sup_ = box_sup;

	box_delta_ = vector<double>(m_nVar, 0.0);

	for (int i=0; i<m_nVar; i++)
	{
		box_delta_[i] = (box_sup_[i] - box_inf_[i])/m_nDof_var[i]; 
	}
	mu_ = mu; 
	inv_cov_ = cinv;

}; 




void GaussianTensor::eval(const vector<unsigned int>& vec_int, double& res){


	//Safety checks		
	assert(vec_int.size()==m_nVar); 
	for (int i=0; i<m_nVar; i++)
	{
		assert(vec_int[i] < m_nDof_var[i]); 
	}	
	
	double product = 0; 
	for (int i = 0; i< m_nVar; i++)
	{
		double xi = box_inf_[i] + (vec_int[i]+0.5)*box_delta_[i]; 
		
		for (int j=0; j<m_nVar; j++)
		{
			double xj = box_inf_[j] + (vec_int[j]+0.5)*box_delta_[j];
			product = product - 0.5*(xi - mu_[i])*inv_cov_[i][j]*(xj - mu_[j]);
		}	
	}
	res = 0.01 + 2*exp(product);
}



