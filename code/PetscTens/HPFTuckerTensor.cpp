// Created by: Damiano & Virginie on ...
#include "HPFTuckerTensor.h"

using namespace std;

// Constructor
HPFTuckerTensor::HPFTuckerTensor(Node* node, const vector<TuckerTensor>& vec){
  node_ = node;
  tens_ = vec;
}



void HPFTuckerTensor::eval(const vector<int>& vec_ind, double& res, MPI_Comm theComm){

	Node* leaf;
	vector<int> new_vec_ind;
	node_->whichLeaf(vec_ind, leaf, new_vec_ind);

	vector< Node*> leafs = node_->getLeafs();

	int pos;
	node_->isIn(leaf, leafs, pos);

	TuckerTensor Tuck = tens_[pos]; 

	Tuck.eval(new_vec_ind, res, theComm);
}

