// implementation of the mather class tensor. Header
// tensor is a generic class, containing the attributes of all kinds of tensors.

#ifndef tensor_h
#define tensor_h

#include "genericInclude.h"
#include "auxiliary.h"


using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:

class tensor{

	protected:

        MPI_Comm m_theComm;
	int m_nVar; // Number of variables = the tensor dimension * num_var_
	vector<unsigned int> m_nDof_var; //Dimension of the discretised spaces in each variable * dim_var_
	auxiliary a; // class containing all the auxiliary operations;
        vector<unsigned int> m_inc; // auxiliary vector for indexing

	// Data from HOSVD
	vector<Vec> singular_vals_; 
	vector<Mat> singular_vecs_;


	public:

	// -- CONSTRUCTORS and DESTRUCTOR --
	tensor(){};
	~tensor(){};

	//Overloaded constructors
	tensor(const int&, const vector<unsigned int>&);


	// -- ACCESS FUNCTIONS --
	inline MPI_Comm theComm(){return m_theComm;};
  	inline const int nVar(){return m_nVar;}; //getNumVar()
  	inline const vector<unsigned int> nDof_var(){return m_nDof_var;}; //getVarDimVec()
  	inline const int nDof_var(int j){assert(j<m_nVar); return m_nDof_var[j];}; // getVarDim
    
	inline Vec getSingVals(const int& d){return singular_vals_[d];}
	inline Mat getSingVecs(const int& d){return singular_vecs_[d];}

	inline vector<Vec> getSingVals(){return singular_vals_;}
	inline vector<Mat> getSingVecs(){return singular_vecs_;}


        inline vector<unsigned int> inc(){return m_inc;};
	inline unsigned int inc(int j){assert(j<m_nVar); return m_inc[j];};

// 1 - AUXILIARY inline methods for indexing:
	// given a vector of integer containing the global indices i,j,k.., return the linear index
	inline unsigned int sub2lin(vector<unsigned int> indices){
			unsigned int linInd = 0;
			for (unsigned int h=0; h<m_nVar; h++) {
					linInd = linInd + m_inc[h] * indices[h];
			};
			return linInd;
	}
	// given the linear index return a vector containing the indices i,j,k,...
	inline vector<unsigned int> lin2sub(unsigned int linInd){
			vector<unsigned int> indices;
			indices.resize(m_nVar);
			unsigned int reminder = linInd;
			for (int h=m_nVar-1; h>=0; h--) {
					indices[h] = reminder/m_inc[h];
					reminder = reminder - indices[h] * m_inc[h];
			};
			return indices;
	}


  // -- SETTERS --
        inline void compute_minc()
	{
	 	m_inc.resize(m_nVar);
  		for(unsigned int idVar = 0; idVar < m_nVar; idVar++){
    			m_inc[idVar] = 1;
    			for (unsigned int n=0; n<idVar; n++)
			{
	        		m_inc[idVar] = m_inc[idVar] * m_nDof_var[n];
			}
		}
    	};
	inline void setTensorComm(MPI_Comm theComm){m_theComm = theComm;};
        inline void set_nVar(int nVar){m_nVar = nVar;};
         inline void set_nDof_var(vector<unsigned int>& vec){assert(vec.size()==m_nVar);
    m_nDof_var.resize(vec.size());
    for(unsigned int idVar=0; idVar<m_nVar; idVar++){
      m_nDof_var[idVar] = vec[idVar];
    }
  }
	// if already initialised!
  inline void set_nDof_var(int j, unsigned int val){assert(j<m_nVar); m_nDof_var[j]=val;};

  // -- METHODS --

	// - Compute unfolding -
	virtual void computeUnfolding(const unsigned int, Mat&, int&);
        vector<unsigned int> compute_multi_index(const int&, const int&, const int&);

	virtual void compute_HOSVD();
	virtual void compute_HOSVD(const double&);
	//vector<int> compute_multi_index(const int&, const int&, const int&);

	//-- Evaluation of the tensor entries --
	virtual void eval(const vector<unsigned int>&, double&){cout << "something" << endl;};

       virtual void getLargestSingVal(const vector<unsigned int>&, int&, double&);
	virtual void getError(const vector<unsigned int>&, double&);

	virtual void compute_greedy_errmem(const double&, double&, int& , vector<unsigned int>&);

	virtual void scalarProdPureTens(const vector<Vec>&, double&);
};


#endif
