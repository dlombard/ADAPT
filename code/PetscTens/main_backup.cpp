// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>

#include "auxiliary.h"
#include "GenTensor.h"
#include "CoulombTensor.h"
#include "GenTabTensor.h"
#include "TuckerTensor.h"

using namespace std;
int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";
    PetscErrorCode ierr;

    SlepcInitialize(&argc,&args,(char*)0,help);
    //PetscInitialize(&argc,&args,(char*)0,help);

    //ierr = SlepcInitialize(&argc,&args,(char*)0,help);

    int numProc;
    ierr = MPI_Comm_size(PETSC_COMM_WORLD,&numProc);
    //MPI_Comm_size(PETSC_COMM_WORLD,&numProc);
    int myId;
    MPI_Comm_rank (PETSC_COMM_WORLD, &myId);
  

    auxiliary z;


    int num = 3; 
    int n0 = 4;
    int n1 = 5; 
    int n2 = 6; 
    vector<unsigned int> vec_dim(3); 
    vec_dim[0] = n0; 
    vec_dim[1] = n1; 
    vec_dim[2] = n2; 

   /* CoulombTensor CT(num, vec_dim); 

    vector<double> box_inf(3, -2.0); 
    vector<double> box_sup(3, 2.0); 

    double epsilon = 0.01; 

    CT.init(box_inf, box_sup, epsilon); 

    vector<int> index(3); 
    index[0] = 2; 
    index[1] = 3; 
    index[2] = 4;

    double res = 0; 
    CT.eval(index, res, PETSC_COMM_WORLD);

    cout << res << endl; 


	
    Mat M;
    int N;
    z.initMat(M, n1, n0*n2, PETSC_COMM_WORLD);
    z.finalizeMat(M);

    cout << "  Before matricization   " << endl; 

    MatView(M, PETSC_VIEWER_STDOUT_WORLD);


    CT.computeUnfolding(1, M, N,PETSC_COMM_WORLD); 

    cout << "  After matricization   " << endl;

    MatView(M, PETSC_VIEWER_STDOUT_WORLD);

    cout << CT.getNumVar() << endl;

    CT.compute_HOSVD(PETSC_COMM_WORLD);
    
    cout << "Done!!!!!!!!!!" << endl;

    Vec S;
    S = CT.getSingVals(0);

    VecView(S, PETSC_VIEWER_STDOUT_WORLD);*/


    vector<unsigned int> vec_dim2(3); 
    vec_dim[0] = 2; 
    vec_dim[1] = 2; 
    vec_dim[2] = 2; 


    Vec coeff; 
    int m = 8; 
    z.initVec(coeff,m, PETSC_COMM_WORLD);
    z.setVecEl(coeff,0, 0.5, PETSC_COMM_WORLD);
 z.setVecEl(coeff,1, 5, PETSC_COMM_WORLD);
 z.setVecEl(coeff,2, 1, PETSC_COMM_WORLD);
 z.setVecEl(coeff,3, 2.5, PETSC_COMM_WORLD);
 z.setVecEl(coeff,4, 3.5, PETSC_COMM_WORLD);
 z.setVecEl(coeff,5, -6, PETSC_COMM_WORLD);
 z.setVecEl(coeff,6, -1.2, PETSC_COMM_WORLD);
 z.setVecEl(coeff,7, 0.5, PETSC_COMM_WORLD);
    z.finalizeVec(coeff); 

  cout << "Coeff finalized " << endl; 

  vector< vector<unsigned int> > indices(10); 
  vector<unsigned int> ind0(3); 
   ind0[0] = 0; 
   ind0[1] = 0; 
   ind0[2] = 0;
   indices[0] = ind0; 

   ind0[0] = 0; 
   ind0[1] = 0; 
   ind0[2] = 1;
   indices[1] = ind0; 

   ind0[0] = 0; 
   ind0[1] = 1; 
   ind0[2] = 0;
   indices[2] = ind0; 

   ind0[0] = 0; 
   ind0[1] = 1; 
   ind0[2] = 1;
   indices[3] = ind0; 

   ind0[0] = 1; 
   ind0[1] = 0; 
   ind0[2] = 0;
   indices[4] = ind0; 

   ind0[0] = 1; 
   ind0[1] = 0; 
   ind0[2] = 1;
   indices[5] = ind0;  

   ind0[0] = 1; 
   ind0[1] = 1; 
   ind0[2] = 0;
   indices[6] = ind0; 

   ind0[0] = 1; 
   ind0[1] = 1; 
   ind0[2] = 1;
   indices[7] = ind0; 
 

cout << "indices finalized" << endl; 

  fullTensor GTT(3, vec_dim2, indices, coeff) ; 

cout << "GTT created" << endl; 

    vector<unsigned int> index(3); 
    index[0] = 1; 
    index[1] = 0; 
    index[2] = 1;

    double res = 0; 
    GTT.eval(index, res, PETSC_COMM_WORLD);

    cout << "res = " << res << endl; 

Mat M0,M1,M2; 

    z.initMat(M0,n0,2, PETSC_COMM_WORLD);
    z.setMatEl(M0,0, 0,0.5, PETSC_COMM_WORLD);
    z.setMatEl(M0,1, 1,1, PETSC_COMM_WORLD);
    z.setMatEl(M0,2, 0,-1, PETSC_COMM_WORLD);
    z.finalizeMat(M0); 

    z.initMat(M1,n1,2, PETSC_COMM_WORLD);
    z.setMatEl(M1,0, 0,5, PETSC_COMM_WORLD);
    z.setMatEl(M1,1, 1,-3, PETSC_COMM_WORLD);
    z.setMatEl(M1,4, 0,2, PETSC_COMM_WORLD);
    z.finalizeMat(M1); 

    z.initMat(M2,n2,2, PETSC_COMM_WORLD);
    z.setMatEl(M2,0, 0,1, PETSC_COMM_WORLD);
    z.setMatEl(M2,1, 1,1, PETSC_COMM_WORLD);
    z.setMatEl(M2,5, 0,-3, PETSC_COMM_WORLD);
    z.finalizeMat(M2); 

vector<Mat> vec_mat(3); 
vec_mat[0] = M0; 
vec_mat[1] = M1; 
vec_mat[2] = M2; 


TuckerTensor TT(GTT, vec_mat);


    vector<unsigned int> index2(3); 
    index2[0] = 2; 
    index2[1] = 4; 
    index2[2] = 5;


TT.eval(index2, res, PETSC_COMM_WORLD);

    cout << "res = " << res << endl;


    ierr = PetscFinalize();
    return 0;
}
