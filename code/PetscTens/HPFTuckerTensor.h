/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/

#ifndef HPFTuckerTensor_h
#define HPFTuckerTensor_h

#include "genericInclude.h"
#include "auxiliary.h"
#include "GenTensor.h"
#include "TuckerTensor.h"



using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:


class HPFTuckerTensor: public GenTensor{

	private:

        Node* node_;
        vector<TuckerTensor> tens_;

	public: 

        HPFTuckerTensor(){};
  	~HPFTuckerTensor(){};

      	//Overloaded constructors
	HPFTuckerTensor(Node*, const vector<TuckerTensor>&);

	void eval(const vector<int>&, double&, MPI_Comm);

};


#endif



