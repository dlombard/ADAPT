/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/

#ifndef GenTensor_h
#define GenTensor_h

#include "genericInclude.h"
#include "auxiliary.h"
#include "Node.h"
//#include "GenTabTensor.h"


using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:

class GenTensor{

	protected:

	MPI_Comm m_theComm;
	int num_var_; // Number of variables
	vector<int> dim_var_; //Dimension des espaces discrétisés dans chaque variable
	auxiliary a; // class containing all the auxiliary operations;


	// Data from HOSVD
	vector<Vec> singular_vals_; 
	vector<Mat> singular_vecs_;


	public:

	// -- CONSTRUCTORS and DESTRUCTOR --
	GenTensor(){};
	~GenTensor(){};
 
	//Overloaded constructors
	GenTensor(const int&, const vector<int>&);

	// -- ACCESS FUNCTIONS --
        //inline MPI_Comm theComm(){return m_theComm;};
	inline int getNumVar(){return num_var_;}
	inline int getVarDim(const int& d){return dim_var_[d];}
	inline vector<int> getVarDimVec(){return dim_var_;}
	inline Vec getSingVals(const int& d){return singular_vals_[d];}
	inline Mat getSingVecs(const int& d){return singular_vecs_[d];}

          // -- SETTERS --
	inline void setTensorComm(MPI_Comm theComm){m_theComm = theComm;};
        inline void setNumVar(int nVar){num_var_ = nVar;};
        inline void setVarDimVec(vector<unsigned int>& vec)
         {
         assert(vec.size()==num_var_);
         dim_var_.resize(vec.size());
         for(unsigned int idVar=0; idVar<num_var_; idVar++){dim_var_[idVar] = vec[idVar];}
         }
	 //if already initialised!
        inline void setVarDim(int j, unsigned int val){assert(j<num_var_); dim_var_[j]=val;};



	//-- Compute unfolding and HOSVD --
	virtual void computeUnfolding(const int&, Mat&, int&,MPI_Comm);
	vector<int> compute_multi_index(const int&, const int&, const int&);

	virtual void compute_HOSVD(MPI_Comm);
	virtual void compute_HOSVD(const double&, MPI_Comm);


	//-- Evaluation of the tensor --
	virtual void eval(const vector<int>&, double&, MPI_Comm){}; 

        virtual void getLargestSingVal(const vector<int>&, int&, double&, MPI_Comm);
	virtual void getError(const vector<int>&, double&, MPI_Comm);

	virtual void compute_greedy_errmem(const double&, double&, int& , vector<int>, MPI_Comm);

	//virtual void scalarProd(const vector<Vec>&, double&, MPI_Comm);

	//virtual void project(const vector<Mat>&, GenTabTensor&, MPI_Comm);
	

};


#endif
