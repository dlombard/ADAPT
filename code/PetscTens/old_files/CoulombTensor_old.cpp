// Created by: Damiano & Virginie on ...
#include "CoulombTensor.h"

using namespace std;

// Constructor
CoulombTensor::CoulombTensor(const int& num_var, const std::vector<int>& dim_var){
	num_var_ = num_var;
	dim_var_ = dim_var;
}




void CoulombTensor::init(const vector<double>& box_inf, const vector<double>& box_sup, const double& epsilon, const double& alpha){
		

	//Safety checks
	assert(box_inf.size() == num_var_);
	assert(box_sup.size() == num_var_);
	for (int i=0; i<num_var_; i++)
	{
		assert(box_inf[i]<box_sup[i]);
	}
		

	box_inf_ = box_inf; 
	box_sup_ = box_sup;

	box_delta_ = vector<double>(num_var_, 0.0);

	for (int i=0; i<num_var_; i++)
	{
		box_delta_[i] = (box_sup_[i] - box_inf_[i])/dim_var_[i]; 
	}


	epsilon_ =epsilon;
	alpha_  = alpha; 

}; 


void CoulombTensor::init(const vector<double>& box_inf, const vector<double>& box_sup, const double& epsilon){

	//Safety checks
	assert(box_inf.size() == num_var_);
	assert(box_sup.size() == num_var_);
	for (int i=0; i<num_var_; i++)
	{
		assert(box_inf[i]<box_sup[i]);
	}
		

	box_inf_ = box_inf; 
	box_sup_ = box_sup;

	box_delta_ = vector<double>(num_var_, 0.0);

	for (int i=0; i<num_var_; i++)
	{
		box_delta_[i] = (box_sup_[i] - box_inf_[i])/dim_var_[i]; 
	}


	epsilon_ =epsilon;
	alpha_  = 1.0; 

}; 



void CoulombTensor::eval(const vector<int>& vec_int, double& res, MPI_Comm theComm){


	//Safety checks		
	assert(vec_int.size()==num_var_); 
	for (int i=0; i<num_var_; i++)
	{
		assert(vec_int[i] < dim_var_[i]); 
	}	
	

	res = 0; 
	for (int i=0; i< num_var_; i++)
	{
		double xi = box_inf_[i] + (vec_int[i]+0.5)*box_delta_[i];

		for (int j=i+1; j<num_var_; j++)
		{

			double xj = box_inf_[j] + (vec_int[j]+0.5)*box_delta_[j];
			res = res + 1.0/(epsilon_ + pow(abs(xi -xj), alpha_));
		}
	}
		

}



