/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/

#ifndef ApproxTensor_h
#define ApproxTensor_h

#include "genericInclude.h"
#include "auxiliary.h"
#include "Node.h"
#include  "GenTensor.h"
#include  "SubTensor.h"


using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:

class ApproxTensor{

	protected:

	Node* node_;
	GenTensor* tensor_;

	vector<SubTensor*> leaf_subtensors_; 

	vector<double> leaf_errors_; 
	vector< vector<int> > leaf_SVDindices_; 
	vector<int> leaf_memory_; 

	double err_tot_; 

	public:

	// -- CONSTRUCTORS and DESTRUCTOR --
	ApproxTensor(){};
	~ApproxTensor(){};
 
	//Overloaded constructors
	ApproxTensor(Node*, GenTensor*, MPI_Comm);
	ApproxTensor(Node*, GenTensor*, vector<SubTensor*>, vector< vector<int>>, vector<double>, vector<int>);

	inline Node* getNode(){return node_;}
	inline GenTensor* getTensor(){return tensor_;}

	inline vector<SubTensor*> getSubTensors(){return leaf_subtensors_;}

	inline vector<double> getLeafErrors(){return leaf_errors_;}
	inline vector< vector<int> > getLeafSVDIndices(){ return leaf_SVDindices_; }
	inline vector<int> getLeafMemory(){return leaf_memory_;} 

	inline double getErrTot(){return err_tot_;}

	//Approximation functions
	void compute_local_HOSVD(MPI_Comm); 

	void compute_greedy_errmem(const double&,MPI_Comm); 

	void compute_errtot();

	void compute_errandmemNode(Node*, double&, int&);

	int compute_memtot();

	void isIn(const Node*, const vector<Node*>, int&);

	ApproxTensor* testMerging(const int&, MPI_Comm, bool&);
	void tryAndDoMerging(const int&, MPI_Comm, bool&);

	ApproxTensor* createCopy();

	void setData(vector<SubTensor*> , vector< vector<int>>, vector<double>, vector<int>);
	void copyDataFrom(ApproxTensor*);

	void  MergeAsPossibleFirstStrategy(int&, int&, int&, MPI_Comm);
	void  MergeAsPossibleSecondStrategy(int&, int& , int&, MPI_Comm);

        void writeErrors(const string&);
        void writeSVDindices(const string&); 
	void writeMemory(const string&); 

        void SplitAsPossible(int&, int&, int&, MPI_Comm);
	void tryAndDoSplitting(const int&, MPI_Comm, bool&);




};


#endif
