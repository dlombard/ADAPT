/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/

#ifndef GibbsTensor_h
#define GibbsTensor_h

#include "genericInclude.h"
#include "auxiliary.h"
#include "GenTensor.h"


using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:


class GibbsTensor: public GenTensor{

	private:

	vector<double> box_inf_; 
	vector<double> box_sup_;
	vector<double> box_delta_;

	int num_atoms_;
	
	string type_potential_; 

	vector< vector<double> > params_pairs_atoms_;

	double beta_;
	/*double alpha_;*/

	public: 

        GibbsTensor(){};
  	~GibbsTensor(){};

      	//Overloaded constructors
	GibbsTensor(const int&, const vector<int>&); 

	void init(const vector<double>&, const vector<double>&, const string&, vector< vector<double> >&, const double&);
	void init(const vector<double>&, const vector<double>&, const vector<string>&, const double&);

	//void init(const vector<double>&, const vector<double>&, const double&, const double&);
	//void init(const vector<double>&, const vector<double>&, const double&);

	void isIn(const string, const vector<string>, int&);
	double Potential(const double&, const string&, const vector<double>&);

	void eval(const vector<int>& vec_int, double&, MPI_Comm);

};


#endif



