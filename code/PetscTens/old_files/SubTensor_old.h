/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/

#ifndef SubTensor_h
#define SubTensor_h

#include "genericInclude.h"
#include "auxiliary.h"
#include "Node.h"
#include "GenTensor.h"


using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:

class SubTensor: public GenTensor{

	protected:

	Node* node_; // Node which determines the subset of indices
	GenTensor* tensor_; //the "original" tensor

	vector< vector<int> > indices_; //set of indices related to the original tensor 


	public:

	// -- CONSTRUCTORS and DESTRUCTOR --
	SubTensor(){};
	~SubTensor(){};
 
	//Overloaded constructors
	SubTensor(Node*, GenTensor*);


	//-- Evaluation of the tensor --
	void eval(const vector<int>&, double&, MPI_Comm); 



	

};


#endif
