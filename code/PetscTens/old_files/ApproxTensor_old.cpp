// Created by: Damiano & Virginie on ...
#include "ApproxTensor.h"

using namespace std;


// -- Constructors --

// GenTensor
ApproxTensor::ApproxTensor(Node* node, GenTensor* tensor, MPI_Comm theComm){
	node_ = node;
	tensor_ = tensor;

	int num_var = tensor_->getNumVar(); 

	vector<Node* > leafs = node_->getLeafs(); 

        vector<int> veczero(num_var,0);

	vector<SubTensor* > leaf_subten(leafs.size()); 
	vector< vector<int> > vec(leafs.size()); 
	vector<double> err(leafs.size(),-1);

	

	for (int i=0; i<leafs.size(); i++)
	{
		Node* leafi = leafs[i]; 
		SubTensor* subi = new SubTensor(leafi, tensor_); 
		subi->compute_HOSVD(theComm);
		leaf_subten[i] = subi; 
	
		vec[i] = veczero;

		double errval; 
		subi->getError(veczero, errval, theComm); 
		
		err[i] = errval;

		
	}	
	
	leaf_subtensors_ = leaf_subten; 
	leaf_SVDindices_ = vec; 
	leaf_errors_ = err; 

	vector<int> mem(leaf_subtensors_.size(),0);
	leaf_memory_ = mem;

	compute_errtot(); 
	

}


ApproxTensor::ApproxTensor(Node* node, GenTensor* tensor, vector<SubTensor*> leaf_subten, vector< vector<int>> vec, vector<double> err, vector<int> mem)
{
	node_ = node;
	tensor_ = tensor;

	leaf_subtensors_ = leaf_subten; 
	leaf_SVDindices_ = vec; 
	leaf_errors_ = err; 
	leaf_memory_ = mem;

	compute_errtot(); 
	
}


void ApproxTensor::setData(vector<SubTensor*> leaf_subten, vector< vector<int>> vec, vector<double> err, vector<int> mem)
{

	vector<Node*> leafs = node_->getLeafs();
	int L = leafs.size();  
	assert(leaf_subten.size() == L);
	assert(vec.size() == L);
	assert(mem.size() == L);

	leaf_subtensors_ = leaf_subten; 
	leaf_SVDindices_ = vec; 
	leaf_errors_ = err; 
	leaf_memory_ = mem;

	compute_errtot(); 
	
}

void ApproxTensor::copyDataFrom(ApproxTensor* other)
{

	node_ = other->getNode(); 
	tensor_ = other->getTensor(); 

	leaf_subtensors_ = other->getSubTensors(); 
	leaf_SVDindices_ = other->getLeafSVDIndices(); 
	leaf_errors_ = other->getLeafErrors(); 
	leaf_memory_ = other->getLeafMemory();

	compute_errtot(); 
	
}





ApproxTensor* ApproxTensor::createCopy()
{
 
	Node* newnode = node_->createNodeCopy(); 

	return new ApproxTensor(newnode, tensor_, leaf_subtensors_, leaf_SVDindices_, leaf_errors_, leaf_memory_); 
	
}

void ApproxTensor::compute_errtot()
{
	int L = leaf_subtensors_.size();
	double errtot = 0; 
	for (int k=0; k< L; k++)
	{
		errtot = errtot + (leaf_errors_[k])*(leaf_errors_[k]);
	}
	err_tot_ = sqrt(errtot);

	//cout << "err_tot_ = " << err_tot_ << endl;
}

int ApproxTensor::compute_memtot()
{
	int Mem = 0; 
	int L = leaf_subtensors_.size();
	for (int k=0; k< L; k++)
	{
		Mem = Mem + leaf_memory_[k];
	}
	return Mem;
}


void ApproxTensor::compute_errandmemNode(Node* parent, double& err, int& mem)
{
	vector<Node* > leafs = node_->getLeafs();

	vector<Node*> children = parent->getLeafs(); 

	vector<int> numChildInLeafs(children.size()); 
	for (int i=0; i<children.size(); i++)
	{ 
		int k = -1; 
		isIn(children[i], leafs, k);
		numChildInLeafs[i] = k;
	}

	err = 0;
	mem = 0;  

	for (int i=0; i<children.size(); i++)
	{
		err = err + leaf_errors_[numChildInLeafs[i]]*leaf_errors_[numChildInLeafs[i]];
		mem = mem + leaf_memory_[numChildInLeafs[i]];
	}
	err = sqrt(err);

}



void ApproxTensor::compute_local_HOSVD(MPI_Comm theComm){


	cout << "Computing HOSVDs......." << endl; 	

        cout << endl; 

	int num_var = tensor_->getNumVar();
	vector<int> veczero(num_var,0);

	for (int i=0; i<leaf_subtensors_.size(); i++)
	{

		cout << "Leaf i = " << i << endl; 
		
		SubTensor* subi = leaf_subtensors_[i]; 
		subi->compute_HOSVD(theComm);

		double erri;
		subi-> getError(veczero, erri, theComm);
		leaf_errors_[i]= erri;
		
	}
}




void ApproxTensor::compute_greedy_errmem(const double& eps, MPI_Comm theComm)
{

	
	compute_errtot();
	double err_tol = eps*err_tot_; 

	auxiliary z; 

	int numvar = tensor_->getNumVar();

	//cout << "entering greedy_errmem" << endl; 
	cout << "err_tol = " << err_tol << endl;  


	while (err_tot_>= err_tol)
	{

		int lchosen = -1;
		double errmax = 0; 

		z.getLargestElem(leaf_errors_, lchosen, errmax);

		if (lchosen >-1)
		{
			vector<int> vecl = leaf_SVDindices_[lchosen];
			SubTensor* subl = leaf_subtensors_[lchosen];

			int kmax;
			double sigmamax;

			subl->getLargestSingVal(vecl,kmax, sigmamax, theComm);
			
			int mem; 
			if (vecl[kmax]==0)
			{

			
				mem = 0; 
				for (int j=0; j<numvar; j++)
				{	
					vecl[j] = 1;
					mem = mem + subl->getVarDim(j);
				} 
				if (numvar>2)
				{
					mem = mem +1;
				}
				
							
			}
			else
			{

				if (numvar>2)
				{

					vecl[kmax] = vecl[kmax] +1; 

					mem = leaf_memory_[lchosen];
				

					mem = mem + subl->getVarDim(kmax);
					int prod = 1;
					for (int j=0; j<numvar; j++)
					{
					
						if (j!=kmax)
						{
							prod = prod*vecl[j]; 
						}
					}
					mem = mem + prod;
				}
				else
				{
					vecl[0] = vecl[0] +1; 
					vecl[1] = vecl[1] +1;

					mem = leaf_memory_[lchosen];
				

					mem = mem + subl->getVarDim(0) + subl->getVarDim(1);
				}

			}


			double errl;
			subl-> getError(vecl, errl, theComm);
			leaf_errors_[lchosen] = errl;
			leaf_SVDindices_[lchosen] = vecl;
			leaf_memory_[lchosen] = mem; 


			compute_errtot();

		}

	}

	
	//cout << "err_tot = " << err_tot_ << endl; 

	double sigma = err_tol*err_tol - err_tot_*err_tot_; 
	//cout << "sigma = " << sigma << endl;

	vector<Node*> leafs = node_->getLeafs();

	sigma = sigma/leafs.size(); 
	
	for (int l=0; l<leafs.size(); l++)
	{
		//leaf_errors_[l]  = (err_tol/err_tot_) * leaf_errors_[l];
		leaf_errors_[l]  = sqrt(leaf_errors_[l]*leaf_errors_[l] + sigma);
	}

	compute_errtot();
	cout << "err_tot = " << err_tot_ << " errtol = " << err_tol << endl; 

}


void ApproxTensor::isIn(const Node* node, const vector<Node*> vecnode, int& k)
{
	k = -1; 
	for (int j=0; j<vecnode.size(); j++)
	{
		if (vecnode[j] == node)
		{
			k=j;
		}
	}
}



ApproxTensor* ApproxTensor::testMerging(const int& k, MPI_Comm theComm, bool& merge)
{
	merge = false;

	auxiliary z;

	vector<Node*> leafs = node_->getLeafs(); 
	Node* nodek = leafs[k]; 
	vector< vector<int>> ind = nodek->getIndices(); 
	Node* nodeparent = nodek->getParent(); 

	double err;
	int mem;
	compute_errandmemNode(nodeparent, err, mem);
	cout << "err du noeud parent = " << err << endl; 

	ApproxTensor* merged = createCopy();

	Node* copy = merged->getNode(); 
	vector<Node*> newLeafsbeforeMerge = copy->getLeafs(); 
	
	Node* nodek2 = newLeafsbeforeMerge[k]; 
	vector< vector<int>> ind2 = nodek2->getIndices(); 


	Node* nodeparent2 = nodek2->getParent(); 
	merged->compute_errandmemNode(nodeparent2, err, mem);
	cout << "err du nouveau noeud = " << err << endl; 

	nodeparent2->removeChildren(); 
	//Il faut updater les erreurs


	vector<Node*> newLeafsafterMerge = copy->getLeafs(); 

	int newL = newLeafsafterMerge.size(); 

	int k0; 
	isIn(nodeparent2, newLeafsafterMerge, k0);

	//On vérifie un truc
	for (int l=0; l< newL; l++)
	{
		if (l!=k0)
		{

			int p;
			//On verifie que la feuille etait bien la avant
			Node* cl =  newLeafsafterMerge[l]; 
			isIn(cl, newLeafsbeforeMerge, p);
	
		}

	}	


	vector<SubTensor*> leaf_subten_tab(newL);
	vector< vector<int>> vec_tab (newL); 
	vector<double> err_tab(newL); 
	vector<int> mem_tab(newL); 

	int newmem = -1; 
	vector<int> sing_val; 


	for (int l=0; l<newL; l++)
	{

		Node* newchildl = newLeafsafterMerge[l]; 

		int k; 
		isIn(newchildl, newLeafsbeforeMerge, k);

		if (k>-1)
		{

			SubTensor* subl = new SubTensor(newchildl, tensor_);
			leaf_subten_tab[l] = subl; 

			vec_tab[l] = leaf_SVDindices_[k];
			err_tab[l] = leaf_errors_[k];
			mem_tab[l] = leaf_memory_[k]; 

		}
		else
		{

			assert(newchildl == nodeparent2);

			vector< vector<int> > vec_indices = newchildl->getIndices(); 

			SubTensor* new_sub = new SubTensor(newchildl, tensor_);

			new_sub->compute_HOSVD(theComm);

			leaf_subten_tab[l] = new_sub;

			
			double errtemp; 
			new_sub->compute_greedy_errmem(err, errtemp, newmem , sing_val, theComm);

			err_tab[l] = err; 
			mem_tab[l] = newmem; 
			vec_tab[l] = sing_val; 

			//cout << "new val vec" << endl; 
			//z.printStdVecInt(sing_val, theComm);

		}
			
	}

	assert(newmem>-1);
	merged->setData(leaf_subten_tab, vec_tab, err_tab, mem_tab);
	merged->compute_errtot();

	cout << "Erreurs totales = " << merged->getErrTot() << " " << err_tot_ << endl; 
	
	
	cout << "newmem = " << newmem << "  mem = " << mem << endl; 

	if (newmem <= mem)
	{
		merge = true;
		cout << "On merge!! yes!!" << endl; 

		//cout << "On verifie que tout est consistant" << endl;
		//copyDataFrom(merged);	

	}
	else
	{
		cout << "Non, on ne merge pas : bof..." << endl; 
	}

	return merged;

}


void ApproxTensor::tryAndDoMerging(const int& k, MPI_Comm theComm, bool& merge)
{
	merge = false;

	auxiliary z;

	vector<Node*> leafs = node_->getLeafs(); 
	Node* nodek = leafs[k]; 
	vector< vector<int>> ind = nodek->getIndices(); 
	Node* nodeparent = nodek->getParent(); 

	double err;
	int mem;
	compute_errandmemNode(nodeparent, err, mem);
	//cout << "err du noeud parent = " << err << endl; 


	ApproxTensor* merged = createCopy();

	Node* copy = merged->getNode(); 
	vector<Node*> newLeafsbeforeMerge = copy->getLeafs(); 
	
	Node* nodek2 = newLeafsbeforeMerge[k]; 
	vector< vector<int>> ind2 = nodek2->getIndices(); 


	Node* nodeparent2 = nodek2->getParent(); 
	merged->compute_errandmemNode(nodeparent2, err, mem);
	//cout << "err du nouveau noeud = " << err << endl; 

	nodeparent2->removeChildren(); 

	vector<Node*> newLeafsafterMerge = copy->getLeafs(); 

	int newL = newLeafsafterMerge.size(); 

	int k0; 
	isIn(nodeparent2, newLeafsafterMerge, k0);

	//On vérifie un truc
	for (int l=0; l< newL; l++)
	{
		if (l!=k0)
		{

			int p;
			//On verifie que la feuille etait bien la avant
			Node* cl =  newLeafsafterMerge[l]; 
			isIn(cl, newLeafsbeforeMerge, p);
	
		}

	}	


	vector<SubTensor*> leaf_subten_tab(newL);
	vector< vector<int>> vec_tab (newL); 
	vector<double> err_tab(newL); 
	vector<int> mem_tab(newL); 

	int newmem = -1; 
	vector<int> sing_val; 


	for (int l=0; l<newL; l++)
	{

		Node* newchildl = newLeafsafterMerge[l]; 

		int k; 
		isIn(newchildl, newLeafsbeforeMerge, k);

		if (k>-1)
		{

			SubTensor* subl = new SubTensor(newchildl, tensor_);
			leaf_subten_tab[l] = subl; 

			vec_tab[l] = leaf_SVDindices_[k];
			err_tab[l] = leaf_errors_[k];
			mem_tab[l] = leaf_memory_[k]; 

		}
		else
		{

			assert(newchildl == nodeparent2);

			vector< vector<int> > vec_indices = newchildl->getIndices(); 

			SubTensor* new_sub = new SubTensor(newchildl, tensor_);

			cout << "err = " << err << endl; 
			cout << "Computing HOSVD" << endl; 
			new_sub->compute_HOSVD(err, theComm);
			//new_sub->compute_HOSVD(theComm);

			leaf_subten_tab[l] = new_sub;

			
			double errtemp; 

			new_sub->compute_greedy_errmem(err, errtemp, newmem , sing_val, theComm);

			

			err_tab[l] = err;
			//cout << "err = " << err_tab[l] << endl;  
			mem_tab[l] = newmem; 
			vec_tab[l] = sing_val;

			//z.printStdVecInt(sing_val, theComm); 

		}
			
	}

	assert(newmem>-1);
	merged->setData(leaf_subten_tab, vec_tab, err_tab, mem_tab);
	merged->compute_errtot();
	cout << "Erreurs totales = " << merged->getErrTot() << " " << err_tot_ << endl; 
	
	cout << "newmem = " << newmem << "  mem = " << mem << endl; 

	if (newmem <= mem)
	{
		merge = true;
		cout << "On merge!! yes!!" << endl; 

		//cout << "On verifie que tout est consistant" << endl;
		//copyDataFrom(merged);	-> Non!

		nodeparent->removeChildren(); 
		setData(leaf_subten_tab, vec_tab, err_tab, mem_tab);
		compute_errtot();

		cout << "Erreurs totales after merging = " << merged->getErrTot() << " " << err_tot_ << endl; 
		

	}
	else
	{
		cout << "Non, on ne merge pas : bof..." << endl; 
		compute_errtot();
		cout << "Erreurs totales after merging = " << merged->getErrTot() << " " << err_tot_ << endl;
	}


}

void  ApproxTensor::MergeAsPossibleFirstStrategy(int& countmerge, int& meminit, int& memfin, MPI_Comm theComm)
{

	double errinit;
	compute_errandmemNode(node_, errinit, meminit);

	double err;


	int it = 0; 
	vector<int> leaf_list;
	vector<Node* > parents = node_->getParentsOfLeafs(leaf_list);
	vector<Node*> leafs = node_->getLeafs(); 

	int sizePar = parents.size(); 

	countmerge = 0; 

	while(it < sizePar)
	{
		
		Node* newpar = parents[it]; 
		Node* childofpar = leafs[leaf_list[it]]; 

		int k;
		isIn(childofpar, leafs, k);

		//cout << "k doit toujours etre positif " << k << endl; 

		bool merge;

		tryAndDoMerging(k,theComm, merge);
		
		if (merge)
		{
				leafs = node_->getLeafs(); 
				parents = node_->getParentsOfLeafs(leaf_list);
				sizePar = parents.size(); 

				//cout << "sizePar = " << sizePar << endl; 
				it = 0;
				countmerge = countmerge+1; 

	
				compute_errandmemNode(node_, err, memfin);
		}
		else
		{
			it = it+1;
		}

	}

	cout << "err = " << err << "  errinit = " << errinit << endl; 
	cout << "meminit = " << meminit << " memfin = " << memfin << endl;

}


void  ApproxTensor::MergeAsPossibleSecondStrategy(int& countmerge, int& meminit, int& memfin, MPI_Comm theComm)
{

	double errinit;
	compute_errandmemNode(node_, errinit, meminit);

	double err;


	int it = 0; 
	vector<int> leaf_list;
	vector<Node* > parents = node_->getParentsOfLeafs(leaf_list);
	vector<Node*> leafs = node_->getLeafs(); 
	vector<Node* > parents_old = parents;

	vector<Node*> parents_tested(0);

	int sizePar = parents.size(); 

	countmerge = 0; 

	bool mergethisturn = true;
	int numberofloop = 0; 

	while(mergethisturn)
	{

		mergethisturn = false;

		int itind = -1; 
		cout << "sizePar = " << sizePar << endl; 
		
		for (it = 0; it < sizePar ; it++)
		{


			cout << "it = " << it << endl; 
			cout << "sizePar = " << sizePar << endl;
			Node* newpar = parents_old[it]; 
			int l;
			isIn(newpar, parents, l);
			//cout << "l doit toujours etre positif " << l << endl; //Non, en fait, pas de raison

			cout << "parents size = " << parents.size() << endl;

			int tested; 
			isIn(newpar,parents_tested,tested);

			if (tested > -0.5)
			{
				cout << "Already tested: nothing to be done!" << endl; 
			}

			if ((l>-0.5) && (tested<-0.5))
			{
				Node* childofpar = leafs[leaf_list[l]]; 

				int k;
				isIn(childofpar, leafs, k);

				//cout << "k doit toujours etre positif " << k << endl; 

				bool merge;

				tryAndDoMerging(k,theComm, merge);
		
				if (merge)
				{
						leafs = node_->getLeafs(); 
						parents = node_->getParentsOfLeafs(leaf_list);
						countmerge = countmerge+1; 
	
						compute_errandmemNode(node_, err, memfin);

						mergethisturn = true; 
						itind = it; 
				}
				else
				{
					cout << "No merge : added to the list of tested parents" << endl; 
					parents_tested.push_back(newpar); 
				}
		
			}

		}

		cout << "mergethisturn = " << mergethisturn << endl; 

		cout << "size parents tested = " << parents_tested.size() << endl; 

		parents_old = parents;
		sizePar = parents.size(); 
		
		numberofloop = numberofloop+1; 

		compute_errandmemNode(node_, err, memfin);

		cout << "numberofloop = " << numberofloop << endl;  
		cout << "err = " << err << "  errinit = " << errinit << endl; 
		cout << "meminit = " << meminit << " memfin = " << memfin << endl;


		
	}

	cout << "Final result:" << endl; 
	cout << "err = " << err << "  errinit = " << errinit << endl; 
	cout << "meminit = " << meminit << " memfin = " << memfin << endl;

	cout << "Total number of merges = " << countmerge << endl; 

}



void ApproxTensor::writeErrors(const string& path)
{

	vector<Node*> leafs = node_->getLeafs(); 

	ofstream myfile;
	string name = path+"_errors.txt";
	const char* name2 = name.c_str();  
  	myfile.open(name2);
	//myfilegx.precision(16); 

	for (int i=0; i<leafs.size(); i++)
	{
		myfile << i << " " << leaf_errors_[i] << endl;
	}

	myfile.close();	
}


void ApproxTensor::writeSVDindices(const string& path)
{
	vector<Node*> leafs = node_->getLeafs(); 

	ofstream myfile;
	string name = path+"_ranks.txt";
	const char* name2 = name.c_str();  
  	myfile.open(name2);
	//myfilegx.precision(16); 

	int numvar = tensor_->getNumVar(); 

	for (int i=0; i<leafs.size(); i++)
	{

		myfile << i << " " ;

		for (int j=0; j<numvar; j++)
		{
			myfile << leaf_SVDindices_[i][j] << " ";
		} 

		myfile << endl; 

	}

	myfile.close();	

} 
	

void ApproxTensor::writeMemory(const string& path)
{

	vector<Node*> leafs = node_->getLeafs(); 

	ofstream myfile;
	string name = path+"_memory.txt";
	const char* name2 = name.c_str();  
  	myfile.open(name2);
	//myfilegx.precision(16); 

	for (int i=0; i<leafs.size(); i++)
	{

		myfile << i << " " << leaf_memory_[i] << endl;

	}

	myfile.close();	

} 




void ApproxTensor::tryAndDoSplitting(const int& k, MPI_Comm theComm, bool& merge)
{
	merge = false;

	auxiliary z;

	vector<Node*> leafs = node_->getLeafs(); 
	Node* nodek = leafs[k]; 
	vector< vector<int>> ind = nodek->getIndices(); 
	Node* nodeparent = nodek->getParent(); 

	double err;
	int mem;
	compute_errandmemNode(nodeparent, err, mem);
	//cout << "err du noeud parent = " << err << endl; 


	ApproxTensor* merged = createCopy();

	Node* copy = merged->getNode(); 
	vector<Node*> newLeafsbeforeMerge = copy->getLeafs(); 
	
	Node* nodek2 = newLeafsbeforeMerge[k]; 
	vector< vector<int>> ind2 = nodek2->getIndices(); 


	Node* nodeparent2 = nodek2->getParent(); 
	merged->compute_errandmemNode(nodeparent2, err, mem);
	//cout << "err du nouveau noeud = " << err << endl; 

	nodeparent2->removeChildren(); 

	vector<Node*> newLeafsafterMerge = copy->getLeafs(); 

	int newL = newLeafsafterMerge.size(); 

	int k0; 
	isIn(nodeparent2, newLeafsafterMerge, k0);

	//On vérifie un truc
	for (int l=0; l< newL; l++)
	{
		if (l!=k0)
		{

			int p;
			//On verifie que la feuille etait bien la avant
			Node* cl =  newLeafsafterMerge[l]; 
			isIn(cl, newLeafsbeforeMerge, p);
	
		}

	}	


	vector<SubTensor*> leaf_subten_tab(newL);
	vector< vector<int>> vec_tab (newL); 
	vector<double> err_tab(newL); 
	vector<int> mem_tab(newL); 

	int newmem = -1; 
	vector<int> sing_val; 


	for (int l=0; l<newL; l++)
	{

		Node* newchildl = newLeafsafterMerge[l]; 

		int k; 
		isIn(newchildl, newLeafsbeforeMerge, k);

		if (k>-1)
		{

			SubTensor* subl = new SubTensor(newchildl, tensor_);
			leaf_subten_tab[l] = subl; 

			vec_tab[l] = leaf_SVDindices_[k];
			err_tab[l] = leaf_errors_[k];
			mem_tab[l] = leaf_memory_[k]; 

		}
		else
		{

			assert(newchildl == nodeparent2);

			vector< vector<int> > vec_indices = newchildl->getIndices(); 

			SubTensor* new_sub = new SubTensor(newchildl, tensor_);

			cout << "err = " << err << endl; 
			cout << "Computing HOSVD" << endl; 
			new_sub->compute_HOSVD(err, theComm);
			//new_sub->compute_HOSVD(theComm);

			leaf_subten_tab[l] = new_sub;

			
			double errtemp; 

			new_sub->compute_greedy_errmem(err, errtemp, newmem , sing_val, theComm);

			

			err_tab[l] = err;
			//cout << "err = " << err_tab[l] << endl;  
			mem_tab[l] = newmem; 
			vec_tab[l] = sing_val;

			//z.printStdVecInt(sing_val, theComm); 

		}
			
	}

	assert(newmem>-1);
	merged->setData(leaf_subten_tab, vec_tab, err_tab, mem_tab);
	merged->compute_errtot();
	cout << "Erreurs totales = " << merged->getErrTot() << " " << err_tot_ << endl; 
	
	cout << "newmem = " << newmem << "  mem = " << mem << endl; 

	if (newmem <= mem)
	{
		merge = true;
		cout << "On merge!! yes!!" << endl; 

		//cout << "On verifie que tout est consistant" << endl;
		//copyDataFrom(merged);	-> Non!

		nodeparent->removeChildren(); 
		setData(leaf_subten_tab, vec_tab, err_tab, mem_tab);
		compute_errtot();

		cout << "Erreurs totales after merging = " << merged->getErrTot() << " " << err_tot_ << endl; 
		

	}
	else
	{
		cout << "Non, on ne merge pas : bof..." << endl; 
		compute_errtot();
		cout << "Erreurs totales after merging = " << merged->getErrTot() << " " << err_tot_ << endl;
	}


}





void ApproxTensor::SplitAsPossible(int& countsplit, int& meminit, int& memfin, MPI_Comm theComm)
{


}
