// Parallel tensor implementation
#include <stdio.h>
#include <iostream>
#include <mpi.h>
#include <petscksp.h>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include "genericInclude.h"
#include "auxiliary.h"
#include "fullTensor.h"
#include "CPTensor.h"
using namespace std;

/* 4 -- saving field in VTK:
  - input: a 3d tensor
  - output: a vtk file
*/
void saveField(fullTensor& T, string& fName){
 string saveName = fName + ".vtk";
 unsigned int nx = T.nDof_var(0);
 unsigned int ny = T.nDof_var(1);
 unsigned int nz = T.nDof_var(2);
 unsigned int numOfVal = nx*ny*nz;

 ofstream outfile (saveName.c_str());

 outfile << "# vtk DataFile Version 2.0 " <<endl;
 outfile << "Data animation" << endl;
 outfile << "ASCII" <<endl;
 outfile << "DATASET STRUCTURED_POINTS" <<endl;
 outfile << "DIMENSIONS " << nx << " " << ny << " " << nz << endl;
 outfile << "ASPECT_RATIO 1.0 1.0 1.0" <<endl;
 outfile << "ORIGIN 0.0 0.0 0.0" <<endl;
 outfile << "POINT_DATA " << numOfVal << endl;
 outfile << "SCALARS Field float" << endl;
 outfile << "LOOKUP_TABLE default" <<endl;
 for(unsigned int h=0; h<T.nEntries(); h++){
     double value = T.tensorEntries(h);
     if(fabs(value) < 1.0e-9){value = 0.0;};
     outfile << value << endl;
 }
 outfile.close();
}


// Simple test on vectors

int main(int argc, char **args){

    static char help[] = "Testing vector routines\n\n";

    SlepcInitialize(&argc,&args,(char*)0,help);

    // Number of procs, and the ranks
    int nOfProcs;
    MPI_Comm_size(PETSC_COMM_WORLD, &nOfProcs);
    int idProc;
    MPI_Comm_rank(PETSC_COMM_WORLD, &idProc);

    // define auxiliary Petsc operations
    auxiliary a;

    //Random numbers
    std::default_random_engine coeffGenerator;
    std::uniform_real_distribution<double> distribution(-1.0,1.0);
    srand(time(NULL));



    // init a Full Tensor given the dimension and the resolution:
/*    unsigned int dim = 4;
    vector<unsigned int> resPerDim(dim);
    resPerDim[0] = 20;
    resPerDim[1] = 20;
    resPerDim[2] = 20;
    resPerDim[3] = 20;

    vector<unsigned int> kappaMax(dim);
    kappaMax[0]=5;
    kappaMax[1]=5;
    kappaMax[2]=5;
    kappaMax[3]=5;

    const double beta = 1.5;
    const double pi = std::atan(1.0)*4.0;

    fullTensor F(dim, resPerDim, PETSC_COMM_WORLD);
    F.printTensorStruct();

    vector<double> waveAmplitudes(625);
    int count = 0;
    for (unsigned int kappa_i = 1; kappa_i <= kappaMax[0]; kappa_i++) {
      for (unsigned int kappa_j = 1; kappa_j <= kappaMax[1]; kappa_j++) {
        for (unsigned int kappa_k = 1; kappa_k <= kappaMax[2]; kappa_k++) {
          for (unsigned int kappa_l = 1; kappa_l <= kappaMax[3]; kappa_l++) {
            double lambda= sqrt(kappa_i*kappa_i + kappa_j*kappa_j + kappa_k*kappa_k + kappa_l*kappa_l);
            double alpha = distribution(coeffGenerator);
            double aCoef=alpha/(pow(lambda,beta));
            waveAmplitudes[count] = aCoef;
            count += 1;
          }
        }
      }
    }

    for (unsigned int i = 0; i < resPerDim[0]; i++) {
      for (unsigned int j = 0; j < resPerDim[1]; j++) {
        for (unsigned int k = 0; k < resPerDim[2]; k++) {
          for (unsigned int l = 0; l < resPerDim[3]; l++) {
            vector<unsigned int> indices={i,j,k,l};
            double x_i = 1.0*i/(resPerDim[0]-1);
            double x_j = 1.0*j/(resPerDim[1]-1);
            double x_k = 1.0*k/(resPerDim[2]-1);
            double x_l = 1.0*l/(resPerDim[3]-1);

            double value = 0.0;
            int cc = 0;
            for (unsigned int kappa_i = 1; kappa_i <= kappaMax[0]; kappa_i++) {
              for (unsigned int kappa_j = 1; kappa_j <= kappaMax[1]; kappa_j++) {
                for (unsigned int kappa_k = 1; kappa_k <= kappaMax[2]; kappa_k++) {
                  for (unsigned int kappa_l = 1; kappa_l <= kappaMax[3]; kappa_l++) {
                    double amplitude = waveAmplitudes[cc];
                    value +=  amplitude*sin(pi*kappa_i*x_i)*sin(pi*kappa_j*x_j)*sin(pi*kappa_k*x_k)*sin(pi*kappa_l*x_l);
                    cc += 1;
                  }
                }
              }
            }
            F.setTensorElement(indices,value);
          }
        }
      }
    }
    F.finalizeTensor();
 */

    // Compute a 3d restriction
/*    vector<unsigned int> resPerDimT(dim-1);
    resPerDimT[0]=resPerDim[0];
    resPerDimT[1]=resPerDim[1];T
    resPerDimT[2]=resPerDim[2];
    fullTensor T(dim-1, resPerDimT, PETSC_COMM_WORLD);

    Vec omega;
    auxiliary a;
    a.initVec(omega, resPerDim[3], PETSC_COMM_WORLD);
    double value = 1.0/(resPerDim[3]-1);
    VecShift(omega, value);
    a.finalizeVec(omega);
    F.modeIContraction(3,omega,T);

    string fName = "T";
    saveField(T,fName); */


    //Same example but with CP in 4 dimensions
    unsigned int dim = 4;
    vector<unsigned int> resPerDim(dim);
    resPerDim[0] = 20;
    resPerDim[1] = 20;
    resPerDim[2] = 20;
    resPerDim[3] = 20;

    vector<unsigned int> kappaMax(dim);
    kappaMax[0]=3;
    kappaMax[1]=3;
    kappaMax[2]=3;
    kappaMax[3]=3;

    unsigned int nOfTerms = 1;
    for(unsigned int iVar=0; iVar<dim; iVar++){
      nOfTerms = nOfTerms * kappaMax[iVar];
    }

    const double beta = 3.5;
    const double pi = std::atan(1.0)*4.0;

    CPTensor F(dim, resPerDim, PETSC_COMM_WORLD);

    vector<double> waveAmplitudes(nOfTerms);
    int count = 0;
    for (unsigned int kappa_i = 1; kappa_i <= kappaMax[0]; kappa_i++) {
      for (unsigned int kappa_j = 1; kappa_j <= kappaMax[1]; kappa_j++) {
        for (unsigned int kappa_k = 1; kappa_k <= kappaMax[2]; kappa_k++) {
          for (unsigned int kappa_l = 1; kappa_l <= kappaMax[3]; kappa_l++) {
            double lambda= sqrt(kappa_i*kappa_i + kappa_j*kappa_j + kappa_k*kappa_k + kappa_l*kappa_l);
            double alpha = distribution(coeffGenerator);
            double aCoef=alpha/(pow(lambda,beta));
            waveAmplitudes[count] = aCoef;
            count += 1;
          }
        }
      }
    }
    F.set_rank(nOfTerms);
    F.set_coeffs(waveAmplitudes);
    vector<vector<Vec> > terms(F.rank());
    unsigned int iTerm=0;
    for (unsigned int kappa_i = 1; kappa_i <= kappaMax[0]; kappa_i++) {
      for (unsigned int kappa_j = 1; kappa_j <= kappaMax[1]; kappa_j++) {
        for (unsigned int kappa_k = 1; kappa_k <= kappaMax[2]; kappa_k++) {
          for (unsigned int kappa_l = 1; kappa_l <= kappaMax[3]; kappa_l++) {
            vector<unsigned int> waveNb(dim);
            waveNb[0] = kappa_i;
            waveNb[1] = kappa_j;
            waveNb[2] = kappa_k;
            waveNb[3] = kappa_l;

            vector<Vec> thisTerm(dim);
            for (unsigned int iVar = 0; iVar < dim; iVar++){
              unsigned int kappa = waveNb[iVar];
              Vec discFun;
              a.initVec(discFun,resPerDim[iVar], PETSC_COMM_WORLD);
              for (unsigned int iDof = 0; iDof < resPerDim[iVar]; iDof++) {
                double x = 1.0*iDof/(resPerDim[iVar]-1);
                double value = sin(pi*kappa*x);
                a.setVecEl(discFun,iDof,value, PETSC_COMM_WORLD);
              }
              a.finalizeVec(discFun);
              thisTerm[iVar] = discFun;
            }
            terms[iTerm] = thisTerm;
            iTerm = iTerm + 1;
          }
        }
      }
    }
    F.set_terms(terms);
    PetscPrintf(PETSC_COMM_WORLD,"Tensor has been built \n\n");


    clock_t begin = clock();

    Mat unfolding;
    PetscPrintf(PETSC_COMM_WORLD,"Computing unfolding \n\n");
    F.computeUnfolding(0, unfolding);

    Vec u, v;
    double sigma;
    PetscPrintf(PETSC_COMM_WORLD,"Computing one term svd \n\n");
    a.computeOneSVDTerm(unfolding, sigma, u, v, PETSC_COMM_WORLD);

    cout << "sigma = " << sigma << endl;
    clock_t end = clock();
    cout << "elapsed time = " << double(end-begin)/CLOCKS_PER_SEC << endl;

    cout << "Computing unfolding svd without unfolding" <<endl;
    clock_t begin_1 = clock();
    Vec u_1;
    double sigma_1;
    F.compute_Unfolding_OneSvdTerm(0, sigma_1, u_1);
    cout << "sigma_1 = " << sigma_1 << endl;
    clock_t end_1 = clock();
    cout << "elapsed time = " << double(end_1-begin_1)/CLOCKS_PER_SEC << endl;

    // computing approximation by ALS:
    // define identity mass matrices:
    /*vector<Mat> massMat(dim);
    for(unsigned int idVar=0; idVar<dim; idVar++){
      massMat[idVar] = a.eye(resPerDim[idVar], PETSC_COMM_WORLD);
    }

    PetscPrintf(PETSC_COMM_WORLD,"Starting ALS: \n\n");
    CPTensor ALS_approx;
    ALS_approx.greedy_ApproxOf(F, massMat, 1.0e-2, 1.0e-3);
    */

    //Same example but in generic dimension
    /*  unsigned int dim;
    unsigned int points;
    int N;
    vector<int> x;
    vector<unsigned int> resPerDim(dim);

      for (int i = 0; i < dim-1; i++) {
        resPerDim[i] = points;
        N *=resPerDim[i];

        for ( l = 0; l < N; l++) {
          vector ip = F.lin2sub(l);
          double x[i] = 1.0*ip/(resPerDim[i]-1);

          for (m = 0; m <pow(L,dim); m++) {
            vector lp=A.lin2sub(m)+1;
            lambda;
            value=alpha/pow(lambda,beta);

            for (j=0; j < dim; j++) {
              value=value*sin(pi*lp[j]*x[j]);

            }
          }
        }
      }
*/





    //CPTensor approx;
    //approx.CPTT_ApproxOf(F, 1.0e-2);


    // Finalize the MPI environment.
    SlepcFinalize();
    return 0;
}
