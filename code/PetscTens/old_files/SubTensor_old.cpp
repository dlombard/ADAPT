// Created by: Damiano & Virginie on ...
#include "SubTensor.h"

using namespace std;


// -- Constructors --

// GenTensor
SubTensor::SubTensor(Node* node, GenTensor* tensor){
	node_ = node;
	tensor_ = tensor;

	indices_ = node->getIndicesFromLeafs();

	num_var_ = tensor->getNumVar();

	vector<int> vec_dim(num_var_);
	for (int j = 0; j<num_var_; j++)
	{
		vec_dim[j] = indices_[j].size();
	}
	dim_var_ = vec_dim;
}

//-- Evaluation of the tensor --
void SubTensor::eval(const vector<int>& vecind, double& res, MPI_Comm theComm)
{

	vector<int> true_ind(num_var_); 
	for (int j=0; j<num_var_; j++)
	{
		true_ind[j] = indices_[j][vecind[j]];
	}
	tensor_->eval(true_ind, res, theComm);

}

