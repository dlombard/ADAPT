/* Header file for: Class containing tensor operations
 Created by: Damiano & Virginie on 18 - 03 - 2015
 class tensor based on Petsc Matrices
*/

#ifndef CoulombTensor_h
#define CoulombTensor_h

#include "genericInclude.h"
#include "auxiliary.h"
#include "GenTensor.h"


using namespace std;

// 0 - DECLARATION of the MOTHER CLASS:


class CoulombTensor: public GenTensor{

	private:

	vector<double> box_inf_; 
	vector<double> box_sup_;
	vector<double> box_delta_;
	double epsilon_;
	double alpha_;

	public: 

        CoulombTensor(){};
  	~CoulombTensor(){};

      	//Overloaded constructors
	CoulombTensor(const int&, const vector<int>&);

	void init(const vector<double>&, const vector<double>&, const double&, const double&);
	void init(const vector<double>&, const vector<double>&, const double&);

	void eval(const vector<int>& vec_int, double&, MPI_Comm);

};


#endif



